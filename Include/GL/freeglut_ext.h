<!DOCTYPE html>
<html lang="en">
<head>
  <meta id="bb-bootstrap" data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="5a097ca786e7c03ff34850dd" />
  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta charset="utf-8">
  <title>
  Yanosik / Zapart 
  / source  / Include / GL / freeglut_ext.h
 &mdash; Bitbucket
</title>
  <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
  


<meta id="bb-canon-url" name="bb-canon-url" content="https://bitbucket.org">
<meta name="bb-api-canon-url" content="https://api.bitbucket.org">
<meta name="apitoken" content="{&quot;token&quot;: &quot;yREvvgFBo8_T7RZu5KeQUTlNGoU5T-fx1ZgDH5mjd52BtXQ0upejivLNulY9eE87OqDaMl5JbC1OZVRpDTkA4OkzahRbUG1UFwdnAFf1XH3u-Fgk&quot;, &quot;expiration&quot;: 1511781292.108443}">

<meta name="bb-commit-hash" content="370568ebc84f">
<meta name="bb-app-node" content="app-166">
<meta name="bb-view-name" content="bitbucket.apps.repo2.views.filebrowse">
<meta name="ignore-whitespace" content="False">
<meta name="tab-size" content="None">
<meta name="locale" content="en">

<meta name="application-name" content="Bitbucket">
<meta name="apple-mobile-web-app-title" content="Bitbucket">


<meta name="theme-color" content="#0049B0">
<meta name="msapplication-TileColor" content="#0052CC">
<meta name="msapplication-TileImage" content="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/mstile-150x150.png">
<link rel="apple-touch-icon" sizes="180x180" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/apple-touch-icon.png">
<link rel="icon" sizes="192x192" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/android-chrome-192x192.png">

<link rel="icon" sizes="16x16 24x24 32x32 64x64" type="image/x-icon" href="/favicon.ico?v=2">
<link rel="mask-icon" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/safari-pinned-tab.svg" color="#0052CC">

<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Bitbucket">

  <meta name="description" content="">
  
  
    
  



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/vendor.css" />
<link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/app.css" />



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/adg3.css">
  
  <script nonce="SOF0GG3ium4WyWCT">
  window.__sentry__ = {"dsn": "https://ea49358f525d4019945839a3d7a8292a@sentry.io/159509", "release": "370568ebc84f (production)", "tags": {"project": "bitbucket-core"}, "environment": "production"};
</script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/sentry.js" nonce="SOF0GG3ium4WyWCT"></script>
  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/early.js" nonce="SOF0GG3ium4WyWCT"></script>
  
  
    <link href="/Yanosik/zapart/rss?token=a07b0067199174cef445f1e8fa79b35f" rel="alternate nofollow" type="application/rss+xml" title="RSS feed for Zapart" />

</head>
<body class="production adg3  "
    data-static-url="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/"
data-base-url="https://bitbucket.org"
data-no-avatar-image="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg"
data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="{&quot;loginStatusUrl&quot;: &quot;https://id.atlassian.com/profile/rest/profile&quot;}"
data-settings="{&quot;MENTIONS_MIN_QUERY_LENGTH&quot;: 3}"

data-current-repo="{&quot;scm&quot;: &quot;git&quot;, &quot;readOnly&quot;: false, &quot;mainbranch&quot;: {&quot;name&quot;: &quot;master&quot;}, &quot;uuid&quot;: &quot;8f59423b-9494-4138-af6f-56e97d727ae4&quot;, &quot;language&quot;: &quot;&quot;, &quot;owner&quot;: {&quot;username&quot;: &quot;Yanosik&quot;, &quot;uuid&quot;: &quot;8aa0a974-0d88-41f1-b96a-c1ebd6c01af9&quot;, &quot;isTeam&quot;: false}, &quot;fullslug&quot;: &quot;Yanosik/zapart&quot;, &quot;slug&quot;: &quot;zapart&quot;, &quot;id&quot;: 29632644, &quot;pygmentsLanguage&quot;: null}"
data-current-cset="f71033cf7729d442b43ee6f67d340a04470f80ae"





data-browser-monitoring="true"
data-switch-create-pullrequest-commit-status="true"

data-track-js-errors="true"


>
<div id="page">
  
    <div id="adg3-navigation"
  
  
  >
  <nav class="skeleton-nav">
    <div class="skeleton-nav--left">
      <div class="skeleton-nav--left-top">
        <ul class="skeleton-nav--items">
          <li></li>
          <li></li>
          <li></li>
          <li class="skeleton--icon"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
        </ul>
      </div>
      <div class="skeleton-nav--left-bottom">
        <div class="skeleton-nav--left-bottom-wrapper">
          <div class="skeleton-nav--item-help"></div>
          <div class="skeleton-nav--item-profile"></div>
        </div>
      </div>
    </div>
    <div class="skeleton-nav--right">
      <ul class="skeleton-nav--items-wide">
        <li class="skeleton--icon-logo-container">
          <div class="skeleton--icon-container"></div>
          <div class="skeleton--icon-description"></div>
          <div class="skeleton--icon-logo"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
      </ul>
    </div>
  </nav>
</div>

    <div id="wrapper">
      
  


      
  <div id="nps-survey-container"></div>

 

      
  

<div id="account-warning" data-module="header/account-warning"
  data-unconfirmed-addresses="false"
  data-no-addresses="false"
  
></div>



      
  
<header id="aui-message-bar">
  
</header>


      <div id="content" role="main">

        
          <header class="app-header">
            <div class="app-header--primary">
              
                <div class="app-header--context">
                  <div class="app-header--breadcrumbs">
                    
  <ol class="aui-nav aui-nav-breadcrumbs">
    <li>
  <a href="/Yanosik/">Jan</a>
</li>

<li>
  <a href="/Yanosik/zapart">Zapart</a>
</li>
    
  <li>
    <a href="/Yanosik/zapart/src">
      Source
    </a>
  </li>

  </ol>

                  </div>
                  <h1 class="app-header--heading">
                    <span class="file-path">freeglut_ext.h</span>
                  </h1>
                </div>
              
            </div>

            <div class="app-header--secondary">
              
                
              
            </div>
          </header>
        

        
        
  <div class="aui-page-panel ">
    <div class="hidden">
  
  </div>
    <div class="aui-page-panel-inner">

      <div
        id="repo-content"
        class="aui-page-panel-content forks-enabled can-create"
        data-module="repo/index"
        
      >
        
        
  <div id="source-container" class="maskable" data-module="repo/source/index">
    



<header id="source-path">
  
    <div class="labels labels-csv">
      <div class="aui-buttons">
        <button data-branches-tags-url="/api/1.0/repositories/Yanosik/zapart/branches-tags"
                data-module="components/branch-dialog"
                
                class="aui-button branch-dialog-trigger" title="master">
          
            
              <span class="aui-icon aui-icon-small aui-iconfont-devtools-branch">Branch</span>
            
            <span class="name">master</span>
          
          <span class="aui-icon-dropdown"></span>
        </button>
        <button class="aui-button" id="checkout-branch-button"
                title="Check out this branch">
          <span class="aui-icon aui-icon-small aui-iconfont-devtools-clone">Check out branch</span>
          <span class="aui-icon-dropdown"></span>
        </button>
      </div>
      
    
    
  
    </div>
  
  
    <div class="secondary-actions">
      <div class="aui-buttons">
        
          <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/freeglut_ext.h?at=master"
            class="aui-button pjax-trigger source-toggle" aria-pressed="true">
            Source
          </a>
          <a href="/Yanosik/zapart/diff/Include/GL/freeglut_ext.h?diff2=f71033cf7729&at=master"
            class="aui-button pjax-trigger diff-toggle"
            title="Diff to previous change">
            Diff
          </a>
          <a href="/Yanosik/zapart/history-node/f71033cf7729/Include/GL/freeglut_ext.h?at=master"
            class="aui-button pjax-trigger history-toggle">
            History
          </a>
        
      </div>

      
        
        
      

    </div>
  
  <h1>
    
      
        <a href="/Yanosik/zapart/src/f71033cf7729?at=master"
          class="pjax-trigger root" title="Yanosik/zapart at f71033cf7729">Zapart</a> /
      
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/?at=master"
              class="pjax-trigger directory-name">Include</a> /
          
        
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/?at=master"
              class="pjax-trigger directory-name">GL</a> /
          
        
      
        
          
            <span class="file-name">freeglut_ext.h</span>
          
        
      
    
  </h1>
  
    
    
  
  <div class="clearfix"></div>
</header>


  
    
  

  <div id="editor-container" class="maskable"
       data-module="repo/source/editor"
       data-owner="Yanosik"
       data-slug="zapart"
       data-is-writer="true"
       data-has-push-access="true"
       data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"
       data-branch="master"
       data-path="Include/GL/freeglut_ext.h"
       data-source-url="/api/internal/repositories/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_ext.h">
    <div id="source-view" class="file-source-container" data-module="repo/source/view-file" data-is-lfs="false">
      <div class="toolbar">
        <div class="primary">
          <div class="aui-buttons">
            
              <button id="file-history-trigger" class="aui-button aui-button-light changeset-info"
                      data-changeset="f71033cf7729d442b43ee6f67d340a04470f80ae"
                      data-path="Include/GL/freeglut_ext.h"
                      data-current="f71033cf7729d442b43ee6f67d340a04470f80ae">
                 

  <div class="aui-avatar aui-avatar-xsmall">
    <div class="aui-avatar-inner">
      <img src="https://bitbucket.org/account/Yanosik/avatar/16/?ts=1511779772">
    </div>
  </div>
  <span class="changeset-hash">f71033c</span>
  <time datetime="2017-11-13T12:06:29+00:00" class="timestamp"></time>
  <span class="aui-icon-dropdown"></span>

              </button>
            
          </div>
          
          <a href="/Yanosik/zapart/full-commit/f71033cf7729/Include/GL/freeglut_ext.h" id="full-commit-link"
             title="View full commit f71033c">Full commit</a>
        </div>
        <div class="secondary">
          
          <div class="aui-buttons">
            
              <a href="/Yanosik/zapart/annotate/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_ext.h?at=master"
                 class="aui-button aui-button-light pjax-trigger blame-link">Blame</a>
              
            
            <a href="/Yanosik/zapart/raw/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_ext.h" class="aui-button aui-button-light raw-link">Raw</a>
          </div>
          
            

            <div class="aui-buttons">
              
              <button id="file-edit-button" class="edit-button aui-button aui-button-light aui-button-split-main"
                  

                  >
                Edit
                
              </button>
              <button id="file-more-actions-button" class="aui-button aui-button-light aui-dropdown2-trigger aui-button-split-more" aria-owns="split-container-dropdown" aria-haspopup="true"
                  >
                More file actions
              </button>
            </div>
            <div id="split-container-dropdown" class="aui-dropdown2 aui-style-default" data-container="#editor-container">
              <ul class="aui-list-truncate">
                <li><a href="#" data-module="repo/source/rename-file" class="rename-link">Rename</a></li>
                <li><a href="#" data-module="repo/source/delete-file" class="delete-link">Delete</a></li>
              </ul>
            </div>
          
        </div>

        <div id="fileview-dropdown"
            class="aui-dropdown2 aui-style-default"
            data-fileview-container="#fileview-container"
            
            
            data-fileview-button="#fileview-trigger"
            data-module="connect/fileview">
          <div class="aui-dropdown2-section">
            <ul>
              <li>
                <a class="aui-dropdown2-radio aui-dropdown2-checked"
                    data-fileview-id="-1"
                    data-fileview-loaded="true"
                    data-fileview-target="#fileview-original"
                    data-fileview-connection-key=""
                    data-fileview-module-key="file-view-default">
                  Default File Viewer
                </a>
              </li>
              
            </ul>
          </div>
        </div>

        <div class="clearfix"></div>
      </div>
      <div id="fileview-container">
        <div id="fileview-original"
            class="fileview"
            data-module="repo/source/highlight-lines"
            data-fileview-loaded="true">
          


  
    <div class="file-source">
      <table class="codehilite highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre><a href="#freeglut_ext.h-1">  1</a>
<a href="#freeglut_ext.h-2">  2</a>
<a href="#freeglut_ext.h-3">  3</a>
<a href="#freeglut_ext.h-4">  4</a>
<a href="#freeglut_ext.h-5">  5</a>
<a href="#freeglut_ext.h-6">  6</a>
<a href="#freeglut_ext.h-7">  7</a>
<a href="#freeglut_ext.h-8">  8</a>
<a href="#freeglut_ext.h-9">  9</a>
<a href="#freeglut_ext.h-10"> 10</a>
<a href="#freeglut_ext.h-11"> 11</a>
<a href="#freeglut_ext.h-12"> 12</a>
<a href="#freeglut_ext.h-13"> 13</a>
<a href="#freeglut_ext.h-14"> 14</a>
<a href="#freeglut_ext.h-15"> 15</a>
<a href="#freeglut_ext.h-16"> 16</a>
<a href="#freeglut_ext.h-17"> 17</a>
<a href="#freeglut_ext.h-18"> 18</a>
<a href="#freeglut_ext.h-19"> 19</a>
<a href="#freeglut_ext.h-20"> 20</a>
<a href="#freeglut_ext.h-21"> 21</a>
<a href="#freeglut_ext.h-22"> 22</a>
<a href="#freeglut_ext.h-23"> 23</a>
<a href="#freeglut_ext.h-24"> 24</a>
<a href="#freeglut_ext.h-25"> 25</a>
<a href="#freeglut_ext.h-26"> 26</a>
<a href="#freeglut_ext.h-27"> 27</a>
<a href="#freeglut_ext.h-28"> 28</a>
<a href="#freeglut_ext.h-29"> 29</a>
<a href="#freeglut_ext.h-30"> 30</a>
<a href="#freeglut_ext.h-31"> 31</a>
<a href="#freeglut_ext.h-32"> 32</a>
<a href="#freeglut_ext.h-33"> 33</a>
<a href="#freeglut_ext.h-34"> 34</a>
<a href="#freeglut_ext.h-35"> 35</a>
<a href="#freeglut_ext.h-36"> 36</a>
<a href="#freeglut_ext.h-37"> 37</a>
<a href="#freeglut_ext.h-38"> 38</a>
<a href="#freeglut_ext.h-39"> 39</a>
<a href="#freeglut_ext.h-40"> 40</a>
<a href="#freeglut_ext.h-41"> 41</a>
<a href="#freeglut_ext.h-42"> 42</a>
<a href="#freeglut_ext.h-43"> 43</a>
<a href="#freeglut_ext.h-44"> 44</a>
<a href="#freeglut_ext.h-45"> 45</a>
<a href="#freeglut_ext.h-46"> 46</a>
<a href="#freeglut_ext.h-47"> 47</a>
<a href="#freeglut_ext.h-48"> 48</a>
<a href="#freeglut_ext.h-49"> 49</a>
<a href="#freeglut_ext.h-50"> 50</a>
<a href="#freeglut_ext.h-51"> 51</a>
<a href="#freeglut_ext.h-52"> 52</a>
<a href="#freeglut_ext.h-53"> 53</a>
<a href="#freeglut_ext.h-54"> 54</a>
<a href="#freeglut_ext.h-55"> 55</a>
<a href="#freeglut_ext.h-56"> 56</a>
<a href="#freeglut_ext.h-57"> 57</a>
<a href="#freeglut_ext.h-58"> 58</a>
<a href="#freeglut_ext.h-59"> 59</a>
<a href="#freeglut_ext.h-60"> 60</a>
<a href="#freeglut_ext.h-61"> 61</a>
<a href="#freeglut_ext.h-62"> 62</a>
<a href="#freeglut_ext.h-63"> 63</a>
<a href="#freeglut_ext.h-64"> 64</a>
<a href="#freeglut_ext.h-65"> 65</a>
<a href="#freeglut_ext.h-66"> 66</a>
<a href="#freeglut_ext.h-67"> 67</a>
<a href="#freeglut_ext.h-68"> 68</a>
<a href="#freeglut_ext.h-69"> 69</a>
<a href="#freeglut_ext.h-70"> 70</a>
<a href="#freeglut_ext.h-71"> 71</a>
<a href="#freeglut_ext.h-72"> 72</a>
<a href="#freeglut_ext.h-73"> 73</a>
<a href="#freeglut_ext.h-74"> 74</a>
<a href="#freeglut_ext.h-75"> 75</a>
<a href="#freeglut_ext.h-76"> 76</a>
<a href="#freeglut_ext.h-77"> 77</a>
<a href="#freeglut_ext.h-78"> 78</a>
<a href="#freeglut_ext.h-79"> 79</a>
<a href="#freeglut_ext.h-80"> 80</a>
<a href="#freeglut_ext.h-81"> 81</a>
<a href="#freeglut_ext.h-82"> 82</a>
<a href="#freeglut_ext.h-83"> 83</a>
<a href="#freeglut_ext.h-84"> 84</a>
<a href="#freeglut_ext.h-85"> 85</a>
<a href="#freeglut_ext.h-86"> 86</a>
<a href="#freeglut_ext.h-87"> 87</a>
<a href="#freeglut_ext.h-88"> 88</a>
<a href="#freeglut_ext.h-89"> 89</a>
<a href="#freeglut_ext.h-90"> 90</a>
<a href="#freeglut_ext.h-91"> 91</a>
<a href="#freeglut_ext.h-92"> 92</a>
<a href="#freeglut_ext.h-93"> 93</a>
<a href="#freeglut_ext.h-94"> 94</a>
<a href="#freeglut_ext.h-95"> 95</a>
<a href="#freeglut_ext.h-96"> 96</a>
<a href="#freeglut_ext.h-97"> 97</a>
<a href="#freeglut_ext.h-98"> 98</a>
<a href="#freeglut_ext.h-99"> 99</a>
<a href="#freeglut_ext.h-100">100</a>
<a href="#freeglut_ext.h-101">101</a>
<a href="#freeglut_ext.h-102">102</a>
<a href="#freeglut_ext.h-103">103</a>
<a href="#freeglut_ext.h-104">104</a>
<a href="#freeglut_ext.h-105">105</a>
<a href="#freeglut_ext.h-106">106</a>
<a href="#freeglut_ext.h-107">107</a>
<a href="#freeglut_ext.h-108">108</a>
<a href="#freeglut_ext.h-109">109</a>
<a href="#freeglut_ext.h-110">110</a>
<a href="#freeglut_ext.h-111">111</a>
<a href="#freeglut_ext.h-112">112</a>
<a href="#freeglut_ext.h-113">113</a>
<a href="#freeglut_ext.h-114">114</a>
<a href="#freeglut_ext.h-115">115</a>
<a href="#freeglut_ext.h-116">116</a>
<a href="#freeglut_ext.h-117">117</a>
<a href="#freeglut_ext.h-118">118</a>
<a href="#freeglut_ext.h-119">119</a>
<a href="#freeglut_ext.h-120">120</a>
<a href="#freeglut_ext.h-121">121</a>
<a href="#freeglut_ext.h-122">122</a>
<a href="#freeglut_ext.h-123">123</a>
<a href="#freeglut_ext.h-124">124</a>
<a href="#freeglut_ext.h-125">125</a>
<a href="#freeglut_ext.h-126">126</a>
<a href="#freeglut_ext.h-127">127</a>
<a href="#freeglut_ext.h-128">128</a>
<a href="#freeglut_ext.h-129">129</a>
<a href="#freeglut_ext.h-130">130</a>
<a href="#freeglut_ext.h-131">131</a>
<a href="#freeglut_ext.h-132">132</a>
<a href="#freeglut_ext.h-133">133</a>
<a href="#freeglut_ext.h-134">134</a>
<a href="#freeglut_ext.h-135">135</a>
<a href="#freeglut_ext.h-136">136</a>
<a href="#freeglut_ext.h-137">137</a>
<a href="#freeglut_ext.h-138">138</a>
<a href="#freeglut_ext.h-139">139</a>
<a href="#freeglut_ext.h-140">140</a>
<a href="#freeglut_ext.h-141">141</a>
<a href="#freeglut_ext.h-142">142</a>
<a href="#freeglut_ext.h-143">143</a>
<a href="#freeglut_ext.h-144">144</a>
<a href="#freeglut_ext.h-145">145</a>
<a href="#freeglut_ext.h-146">146</a>
<a href="#freeglut_ext.h-147">147</a>
<a href="#freeglut_ext.h-148">148</a>
<a href="#freeglut_ext.h-149">149</a>
<a href="#freeglut_ext.h-150">150</a>
<a href="#freeglut_ext.h-151">151</a>
<a href="#freeglut_ext.h-152">152</a>
<a href="#freeglut_ext.h-153">153</a>
<a href="#freeglut_ext.h-154">154</a>
<a href="#freeglut_ext.h-155">155</a>
<a href="#freeglut_ext.h-156">156</a>
<a href="#freeglut_ext.h-157">157</a>
<a href="#freeglut_ext.h-158">158</a>
<a href="#freeglut_ext.h-159">159</a>
<a href="#freeglut_ext.h-160">160</a>
<a href="#freeglut_ext.h-161">161</a>
<a href="#freeglut_ext.h-162">162</a>
<a href="#freeglut_ext.h-163">163</a>
<a href="#freeglut_ext.h-164">164</a>
<a href="#freeglut_ext.h-165">165</a>
<a href="#freeglut_ext.h-166">166</a>
<a href="#freeglut_ext.h-167">167</a>
<a href="#freeglut_ext.h-168">168</a>
<a href="#freeglut_ext.h-169">169</a>
<a href="#freeglut_ext.h-170">170</a>
<a href="#freeglut_ext.h-171">171</a>
<a href="#freeglut_ext.h-172">172</a>
<a href="#freeglut_ext.h-173">173</a>
<a href="#freeglut_ext.h-174">174</a>
<a href="#freeglut_ext.h-175">175</a>
<a href="#freeglut_ext.h-176">176</a>
<a href="#freeglut_ext.h-177">177</a>
<a href="#freeglut_ext.h-178">178</a>
<a href="#freeglut_ext.h-179">179</a>
<a href="#freeglut_ext.h-180">180</a>
<a href="#freeglut_ext.h-181">181</a>
<a href="#freeglut_ext.h-182">182</a>
<a href="#freeglut_ext.h-183">183</a>
<a href="#freeglut_ext.h-184">184</a>
<a href="#freeglut_ext.h-185">185</a>
<a href="#freeglut_ext.h-186">186</a>
<a href="#freeglut_ext.h-187">187</a>
<a href="#freeglut_ext.h-188">188</a>
<a href="#freeglut_ext.h-189">189</a>
<a href="#freeglut_ext.h-190">190</a>
<a href="#freeglut_ext.h-191">191</a>
<a href="#freeglut_ext.h-192">192</a>
<a href="#freeglut_ext.h-193">193</a>
<a href="#freeglut_ext.h-194">194</a>
<a href="#freeglut_ext.h-195">195</a>
<a href="#freeglut_ext.h-196">196</a>
<a href="#freeglut_ext.h-197">197</a>
<a href="#freeglut_ext.h-198">198</a>
<a href="#freeglut_ext.h-199">199</a>
<a href="#freeglut_ext.h-200">200</a>
<a href="#freeglut_ext.h-201">201</a>
<a href="#freeglut_ext.h-202">202</a>
<a href="#freeglut_ext.h-203">203</a>
<a href="#freeglut_ext.h-204">204</a>
<a href="#freeglut_ext.h-205">205</a>
<a href="#freeglut_ext.h-206">206</a>
<a href="#freeglut_ext.h-207">207</a>
<a href="#freeglut_ext.h-208">208</a>
<a href="#freeglut_ext.h-209">209</a>
<a href="#freeglut_ext.h-210">210</a>
<a href="#freeglut_ext.h-211">211</a>
<a href="#freeglut_ext.h-212">212</a>
<a href="#freeglut_ext.h-213">213</a>
<a href="#freeglut_ext.h-214">214</a>
<a href="#freeglut_ext.h-215">215</a>
<a href="#freeglut_ext.h-216">216</a>
<a href="#freeglut_ext.h-217">217</a>
<a href="#freeglut_ext.h-218">218</a>
<a href="#freeglut_ext.h-219">219</a>
<a href="#freeglut_ext.h-220">220</a>
<a href="#freeglut_ext.h-221">221</a>
<a href="#freeglut_ext.h-222">222</a>
<a href="#freeglut_ext.h-223">223</a>
<a href="#freeglut_ext.h-224">224</a>
<a href="#freeglut_ext.h-225">225</a>
<a href="#freeglut_ext.h-226">226</a>
<a href="#freeglut_ext.h-227">227</a>
<a href="#freeglut_ext.h-228">228</a>
<a href="#freeglut_ext.h-229">229</a>
<a href="#freeglut_ext.h-230">230</a>
<a href="#freeglut_ext.h-231">231</a>
<a href="#freeglut_ext.h-232">232</a>
<a href="#freeglut_ext.h-233">233</a>
<a href="#freeglut_ext.h-234">234</a>
<a href="#freeglut_ext.h-235">235</a>
<a href="#freeglut_ext.h-236">236</a>
<a href="#freeglut_ext.h-237">237</a>
<a href="#freeglut_ext.h-238">238</a>
<a href="#freeglut_ext.h-239">239</a>
<a href="#freeglut_ext.h-240">240</a>
<a href="#freeglut_ext.h-241">241</a>
<a href="#freeglut_ext.h-242">242</a>
<a href="#freeglut_ext.h-243">243</a>
<a href="#freeglut_ext.h-244">244</a>
<a href="#freeglut_ext.h-245">245</a>
<a href="#freeglut_ext.h-246">246</a>
<a href="#freeglut_ext.h-247">247</a>
<a href="#freeglut_ext.h-248">248</a>
<a href="#freeglut_ext.h-249">249</a>
<a href="#freeglut_ext.h-250">250</a>
<a href="#freeglut_ext.h-251">251</a>
<a href="#freeglut_ext.h-252">252</a>
<a href="#freeglut_ext.h-253">253</a>
<a href="#freeglut_ext.h-254">254</a>
<a href="#freeglut_ext.h-255">255</a>
<a href="#freeglut_ext.h-256">256</a>
<a href="#freeglut_ext.h-257">257</a>
<a href="#freeglut_ext.h-258">258</a>
<a href="#freeglut_ext.h-259">259</a>
<a href="#freeglut_ext.h-260">260</a>
<a href="#freeglut_ext.h-261">261</a>
<a href="#freeglut_ext.h-262">262</a>
<a href="#freeglut_ext.h-263">263</a>
<a href="#freeglut_ext.h-264">264</a>
<a href="#freeglut_ext.h-265">265</a>
<a href="#freeglut_ext.h-266">266</a>
<a href="#freeglut_ext.h-267">267</a>
<a href="#freeglut_ext.h-268">268</a>
<a href="#freeglut_ext.h-269">269</a>
<a href="#freeglut_ext.h-270">270</a>
<a href="#freeglut_ext.h-271">271</a></pre></div></td><td class="code"><div class="codehilite highlight"><pre><span></span><a name="freeglut_ext.h-1"></a><span class="cp">#ifndef  __FREEGLUT_EXT_H__</span>
<a name="freeglut_ext.h-2"></a><span class="cp">#define  __FREEGLUT_EXT_H__</span>
<a name="freeglut_ext.h-3"></a>
<a name="freeglut_ext.h-4"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-5"></a><span class="cm"> * freeglut_ext.h</span>
<a name="freeglut_ext.h-6"></a><span class="cm"> *</span>
<a name="freeglut_ext.h-7"></a><span class="cm"> * The non-GLUT-compatible extensions to the freeglut library include file</span>
<a name="freeglut_ext.h-8"></a><span class="cm"> *</span>
<a name="freeglut_ext.h-9"></a><span class="cm"> * Copyright (c) 1999-2000 Pawel W. Olszta. All Rights Reserved.</span>
<a name="freeglut_ext.h-10"></a><span class="cm"> * Written by Pawel W. Olszta, &lt;olszta@sourceforge.net&gt;</span>
<a name="freeglut_ext.h-11"></a><span class="cm"> * Creation date: Thu Dec 2 1999</span>
<a name="freeglut_ext.h-12"></a><span class="cm"> *</span>
<a name="freeglut_ext.h-13"></a><span class="cm"> * Permission is hereby granted, free of charge, to any person obtaining a</span>
<a name="freeglut_ext.h-14"></a><span class="cm"> * copy of this software and associated documentation files (the &quot;Software&quot;),</span>
<a name="freeglut_ext.h-15"></a><span class="cm"> * to deal in the Software without restriction, including without limitation</span>
<a name="freeglut_ext.h-16"></a><span class="cm"> * the rights to use, copy, modify, merge, publish, distribute, sublicense,</span>
<a name="freeglut_ext.h-17"></a><span class="cm"> * and/or sell copies of the Software, and to permit persons to whom the</span>
<a name="freeglut_ext.h-18"></a><span class="cm"> * Software is furnished to do so, subject to the following conditions:</span>
<a name="freeglut_ext.h-19"></a><span class="cm"> *</span>
<a name="freeglut_ext.h-20"></a><span class="cm"> * The above copyright notice and this permission notice shall be included</span>
<a name="freeglut_ext.h-21"></a><span class="cm"> * in all copies or substantial portions of the Software.</span>
<a name="freeglut_ext.h-22"></a><span class="cm"> *</span>
<a name="freeglut_ext.h-23"></a><span class="cm"> * THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS</span>
<a name="freeglut_ext.h-24"></a><span class="cm"> * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,</span>
<a name="freeglut_ext.h-25"></a><span class="cm"> * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL</span>
<a name="freeglut_ext.h-26"></a><span class="cm"> * PAWEL W. OLSZTA BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER</span>
<a name="freeglut_ext.h-27"></a><span class="cm"> * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN</span>
<a name="freeglut_ext.h-28"></a><span class="cm"> * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</span>
<a name="freeglut_ext.h-29"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-30"></a>
<a name="freeglut_ext.h-31"></a><span class="cp">#ifdef __cplusplus</span>
<a name="freeglut_ext.h-32"></a>    <span class="k">extern</span> <span class="s">&quot;C&quot;</span> <span class="p">{</span>
<a name="freeglut_ext.h-33"></a><span class="cp">#endif</span>
<a name="freeglut_ext.h-34"></a>
<a name="freeglut_ext.h-35"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-36"></a><span class="cm"> * Additional GLUT Key definitions for the Special key function</span>
<a name="freeglut_ext.h-37"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-38"></a><span class="cp">#define GLUT_KEY_NUM_LOCK           0x006D</span>
<a name="freeglut_ext.h-39"></a><span class="cp">#define GLUT_KEY_BEGIN              0x006E</span>
<a name="freeglut_ext.h-40"></a><span class="cp">#define GLUT_KEY_DELETE             0x006F</span>
<a name="freeglut_ext.h-41"></a><span class="cp">#define GLUT_KEY_SHIFT_L            0x0070</span>
<a name="freeglut_ext.h-42"></a><span class="cp">#define GLUT_KEY_SHIFT_R            0x0071</span>
<a name="freeglut_ext.h-43"></a><span class="cp">#define GLUT_KEY_CTRL_L             0x0072</span>
<a name="freeglut_ext.h-44"></a><span class="cp">#define GLUT_KEY_CTRL_R             0x0073</span>
<a name="freeglut_ext.h-45"></a><span class="cp">#define GLUT_KEY_ALT_L              0x0074</span>
<a name="freeglut_ext.h-46"></a><span class="cp">#define GLUT_KEY_ALT_R              0x0075</span>
<a name="freeglut_ext.h-47"></a>
<a name="freeglut_ext.h-48"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-49"></a><span class="cm"> * GLUT API Extension macro definitions -- behaviour when the user clicks on an &quot;x&quot; to close a window</span>
<a name="freeglut_ext.h-50"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-51"></a><span class="cp">#define GLUT_ACTION_EXIT                         0</span>
<a name="freeglut_ext.h-52"></a><span class="cp">#define GLUT_ACTION_GLUTMAINLOOP_RETURNS         1</span>
<a name="freeglut_ext.h-53"></a><span class="cp">#define GLUT_ACTION_CONTINUE_EXECUTION           2</span>
<a name="freeglut_ext.h-54"></a>
<a name="freeglut_ext.h-55"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-56"></a><span class="cm"> * Create a new rendering context when the user opens a new window?</span>
<a name="freeglut_ext.h-57"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-58"></a><span class="cp">#define GLUT_CREATE_NEW_CONTEXT                  0</span>
<a name="freeglut_ext.h-59"></a><span class="cp">#define GLUT_USE_CURRENT_CONTEXT                 1</span>
<a name="freeglut_ext.h-60"></a>
<a name="freeglut_ext.h-61"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-62"></a><span class="cm"> * Direct/Indirect rendering context options (has meaning only in Unix/X11)</span>
<a name="freeglut_ext.h-63"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-64"></a><span class="cp">#define GLUT_FORCE_INDIRECT_CONTEXT              0</span>
<a name="freeglut_ext.h-65"></a><span class="cp">#define GLUT_ALLOW_DIRECT_CONTEXT                1</span>
<a name="freeglut_ext.h-66"></a><span class="cp">#define GLUT_TRY_DIRECT_CONTEXT                  2</span>
<a name="freeglut_ext.h-67"></a><span class="cp">#define GLUT_FORCE_DIRECT_CONTEXT                3</span>
<a name="freeglut_ext.h-68"></a>
<a name="freeglut_ext.h-69"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-70"></a><span class="cm"> * GLUT API Extension macro definitions -- the glutGet parameters</span>
<a name="freeglut_ext.h-71"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-72"></a><span class="cp">#define  GLUT_INIT_STATE                    0x007C</span>
<a name="freeglut_ext.h-73"></a>
<a name="freeglut_ext.h-74"></a><span class="cp">#define  GLUT_ACTION_ON_WINDOW_CLOSE        0x01F9</span>
<a name="freeglut_ext.h-75"></a>
<a name="freeglut_ext.h-76"></a><span class="cp">#define  GLUT_WINDOW_BORDER_WIDTH           0x01FA</span>
<a name="freeglut_ext.h-77"></a><span class="cp">#define  GLUT_WINDOW_BORDER_HEIGHT          0x01FB</span>
<a name="freeglut_ext.h-78"></a><span class="cp">#define  GLUT_WINDOW_HEADER_HEIGHT          0x01FB  </span><span class="cm">/* Docs say it should always have been GLUT_WINDOW_BORDER_HEIGHT, keep this for backward compatibility */</span><span class="cp"></span>
<a name="freeglut_ext.h-79"></a>
<a name="freeglut_ext.h-80"></a><span class="cp">#define  GLUT_VERSION                       0x01FC</span>
<a name="freeglut_ext.h-81"></a>
<a name="freeglut_ext.h-82"></a><span class="cp">#define  GLUT_RENDERING_CONTEXT             0x01FD</span>
<a name="freeglut_ext.h-83"></a><span class="cp">#define  GLUT_DIRECT_RENDERING              0x01FE</span>
<a name="freeglut_ext.h-84"></a>
<a name="freeglut_ext.h-85"></a><span class="cp">#define  GLUT_FULL_SCREEN                   0x01FF</span>
<a name="freeglut_ext.h-86"></a>
<a name="freeglut_ext.h-87"></a><span class="cp">#define  GLUT_SKIP_STALE_MOTION_EVENTS      0x0204</span>
<a name="freeglut_ext.h-88"></a>
<a name="freeglut_ext.h-89"></a><span class="cp">#define  GLUT_GEOMETRY_VISUALIZE_NORMALS    0x0205</span>
<a name="freeglut_ext.h-90"></a>
<a name="freeglut_ext.h-91"></a><span class="cp">#define  GLUT_STROKE_FONT_DRAW_JOIN_DOTS    0x0206  </span><span class="cm">/* Draw dots between line segments of stroke fonts? */</span><span class="cp"></span>
<a name="freeglut_ext.h-92"></a>
<a name="freeglut_ext.h-93"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-94"></a><span class="cm"> * New tokens for glutInitDisplayMode.</span>
<a name="freeglut_ext.h-95"></a><span class="cm"> * Only one GLUT_AUXn bit may be used at a time.</span>
<a name="freeglut_ext.h-96"></a><span class="cm"> * Value 0x0400 is defined in OpenGLUT.</span>
<a name="freeglut_ext.h-97"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-98"></a><span class="cp">#define  GLUT_AUX                           0x1000</span>
<a name="freeglut_ext.h-99"></a>
<a name="freeglut_ext.h-100"></a><span class="cp">#define  GLUT_AUX1                          0x1000</span>
<a name="freeglut_ext.h-101"></a><span class="cp">#define  GLUT_AUX2                          0x2000</span>
<a name="freeglut_ext.h-102"></a><span class="cp">#define  GLUT_AUX3                          0x4000</span>
<a name="freeglut_ext.h-103"></a><span class="cp">#define  GLUT_AUX4                          0x8000</span>
<a name="freeglut_ext.h-104"></a>
<a name="freeglut_ext.h-105"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-106"></a><span class="cm"> * Context-related flags, see fg_state.c</span>
<a name="freeglut_ext.h-107"></a><span class="cm"> * Set the requested OpenGL version</span>
<a name="freeglut_ext.h-108"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-109"></a><span class="cp">#define  GLUT_INIT_MAJOR_VERSION            0x0200</span>
<a name="freeglut_ext.h-110"></a><span class="cp">#define  GLUT_INIT_MINOR_VERSION            0x0201</span>
<a name="freeglut_ext.h-111"></a><span class="cp">#define  GLUT_INIT_FLAGS                    0x0202</span>
<a name="freeglut_ext.h-112"></a><span class="cp">#define  GLUT_INIT_PROFILE                  0x0203</span>
<a name="freeglut_ext.h-113"></a>
<a name="freeglut_ext.h-114"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-115"></a><span class="cm"> * Flags for glutInitContextFlags, see fg_init.c</span>
<a name="freeglut_ext.h-116"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-117"></a><span class="cp">#define  GLUT_DEBUG                         0x0001</span>
<a name="freeglut_ext.h-118"></a><span class="cp">#define  GLUT_FORWARD_COMPATIBLE            0x0002</span>
<a name="freeglut_ext.h-119"></a>
<a name="freeglut_ext.h-120"></a>
<a name="freeglut_ext.h-121"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-122"></a><span class="cm"> * Flags for glutInitContextProfile, see fg_init.c</span>
<a name="freeglut_ext.h-123"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-124"></a><span class="cp">#define GLUT_CORE_PROFILE                   0x0001</span>
<a name="freeglut_ext.h-125"></a><span class="cp">#define	GLUT_COMPATIBILITY_PROFILE          0x0002</span>
<a name="freeglut_ext.h-126"></a>
<a name="freeglut_ext.h-127"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-128"></a><span class="cm"> * Process loop function, see fg_main.c</span>
<a name="freeglut_ext.h-129"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-130"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="n">glutMainLoopEvent</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-131"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutLeaveMainLoop</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-132"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutExit</span>         <span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-133"></a>
<a name="freeglut_ext.h-134"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-135"></a><span class="cm"> * Window management functions, see fg_window.c</span>
<a name="freeglut_ext.h-136"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-137"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutFullScreenToggle</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-138"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutLeaveFullScreen</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-139"></a>
<a name="freeglut_ext.h-140"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-141"></a><span class="cm"> * Menu functions</span>
<a name="freeglut_ext.h-142"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-143"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetMenuFont</span><span class="p">(</span> <span class="kt">int</span> <span class="n">menuID</span><span class="p">,</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span> <span class="p">);</span>
<a name="freeglut_ext.h-144"></a>
<a name="freeglut_ext.h-145"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-146"></a><span class="cm"> * Window-specific callback functions, see fg_callbacks.c</span>
<a name="freeglut_ext.h-147"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-148"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutMouseWheelFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-149"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPositionFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-150"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutCloseFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">void</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-151"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWMCloseFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">void</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-152"></a><span class="cm">/* And also a destruction callback for menus */</span>
<a name="freeglut_ext.h-153"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutMenuDestroyFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">void</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-154"></a>
<a name="freeglut_ext.h-155"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-156"></a><span class="cm"> * State setting and retrieval functions, see fg_state.c</span>
<a name="freeglut_ext.h-157"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-158"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetOption</span> <span class="p">(</span> <span class="n">GLenum</span> <span class="n">option_flag</span><span class="p">,</span> <span class="kt">int</span> <span class="n">value</span> <span class="p">);</span>
<a name="freeglut_ext.h-159"></a><span class="n">FGAPI</span> <span class="kt">int</span> <span class="o">*</span>   <span class="n">FGAPIENTRY</span> <span class="nf">glutGetModeValues</span><span class="p">(</span><span class="n">GLenum</span> <span class="n">mode</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span> <span class="n">size</span><span class="p">);</span>
<a name="freeglut_ext.h-160"></a><span class="cm">/* A.Donev: User-data manipulation */</span>
<a name="freeglut_ext.h-161"></a><span class="n">FGAPI</span> <span class="kt">void</span><span class="o">*</span>   <span class="n">FGAPIENTRY</span> <span class="nf">glutGetWindowData</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-162"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetWindowData</span><span class="p">(</span><span class="kt">void</span><span class="o">*</span> <span class="n">data</span><span class="p">);</span>
<a name="freeglut_ext.h-163"></a><span class="n">FGAPI</span> <span class="kt">void</span><span class="o">*</span>   <span class="n">FGAPIENTRY</span> <span class="nf">glutGetMenuData</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-164"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetMenuData</span><span class="p">(</span><span class="kt">void</span><span class="o">*</span> <span class="n">data</span><span class="p">);</span>
<a name="freeglut_ext.h-165"></a>
<a name="freeglut_ext.h-166"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-167"></a><span class="cm"> * Font stuff, see fg_font.c</span>
<a name="freeglut_ext.h-168"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-169"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutBitmapHeight</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span> <span class="p">);</span>
<a name="freeglut_ext.h-170"></a><span class="n">FGAPI</span> <span class="n">GLfloat</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutStrokeHeight</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span> <span class="p">);</span>
<a name="freeglut_ext.h-171"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutBitmapString</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="k">const</span> <span class="kt">unsigned</span> <span class="kt">char</span> <span class="o">*</span><span class="n">string</span> <span class="p">);</span>
<a name="freeglut_ext.h-172"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutStrokeString</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="k">const</span> <span class="kt">unsigned</span> <span class="kt">char</span> <span class="o">*</span><span class="n">string</span> <span class="p">);</span>
<a name="freeglut_ext.h-173"></a>
<a name="freeglut_ext.h-174"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-175"></a><span class="cm"> * Geometry functions, see fg_geometry.c</span>
<a name="freeglut_ext.h-176"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-177"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireRhombicDodecahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-178"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidRhombicDodecahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_ext.h-179"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireSierpinskiSponge</span> <span class="p">(</span> <span class="kt">int</span> <span class="n">num_levels</span><span class="p">,</span> <span class="kt">double</span> <span class="n">offset</span><span class="p">[</span><span class="mi">3</span><span class="p">],</span> <span class="kt">double</span> <span class="n">scale</span> <span class="p">);</span>
<a name="freeglut_ext.h-180"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidSierpinskiSponge</span> <span class="p">(</span> <span class="kt">int</span> <span class="n">num_levels</span><span class="p">,</span> <span class="kt">double</span> <span class="n">offset</span><span class="p">[</span><span class="mi">3</span><span class="p">],</span> <span class="kt">double</span> <span class="n">scale</span> <span class="p">);</span>
<a name="freeglut_ext.h-181"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireCylinder</span><span class="p">(</span> <span class="kt">double</span> <span class="n">radius</span><span class="p">,</span> <span class="kt">double</span> <span class="n">height</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">slices</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">stacks</span><span class="p">);</span>
<a name="freeglut_ext.h-182"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidCylinder</span><span class="p">(</span> <span class="kt">double</span> <span class="n">radius</span><span class="p">,</span> <span class="kt">double</span> <span class="n">height</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">slices</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">stacks</span><span class="p">);</span>
<a name="freeglut_ext.h-183"></a>
<a name="freeglut_ext.h-184"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-185"></a><span class="cm"> * Rest of functions for rendering Newell&#39;s teaset, found in fg_teapot.c</span>
<a name="freeglut_ext.h-186"></a><span class="cm"> * NB: front facing polygons have clockwise winding, not counter clockwise</span>
<a name="freeglut_ext.h-187"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-188"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireTeacup</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_ext.h-189"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidTeacup</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_ext.h-190"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireTeaspoon</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_ext.h-191"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidTeaspoon</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_ext.h-192"></a>
<a name="freeglut_ext.h-193"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-194"></a><span class="cm"> * Extension functions, see fg_ext.c</span>
<a name="freeglut_ext.h-195"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-196"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span><span class="o">*</span><span class="n">GLUTproc</span><span class="p">)();</span>
<a name="freeglut_ext.h-197"></a><span class="n">FGAPI</span> <span class="n">GLUTproc</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutGetProcAddress</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">procName</span> <span class="p">);</span>
<a name="freeglut_ext.h-198"></a>
<a name="freeglut_ext.h-199"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-200"></a><span class="cm"> * Multi-touch/multi-pointer extensions</span>
<a name="freeglut_ext.h-201"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-202"></a>
<a name="freeglut_ext.h-203"></a><span class="cp">#define GLUT_HAS_MULTI 1</span>
<a name="freeglut_ext.h-204"></a>
<a name="freeglut_ext.h-205"></a><span class="cm">/* TODO: add device_id parameter,</span>
<a name="freeglut_ext.h-206"></a><span class="cm">   cf. http://sourceforge.net/mailarchive/forum.php?thread_name=20120518071314.GA28061%40perso.beuc.net&amp;forum_name=freeglut-developer */</span>
<a name="freeglut_ext.h-207"></a><span class="n">FGAPI</span> <span class="kt">void</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutMultiEntryFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-208"></a><span class="n">FGAPI</span> <span class="kt">void</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutMultiButtonFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-209"></a><span class="n">FGAPI</span> <span class="kt">void</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutMultiMotionFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-210"></a><span class="n">FGAPI</span> <span class="kt">void</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutMultiPassiveFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-211"></a>
<a name="freeglut_ext.h-212"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-213"></a><span class="cm"> * Joystick functions, see fg_joystick.c</span>
<a name="freeglut_ext.h-214"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-215"></a><span class="cm">/* USE OF THESE FUNCTIONS IS DEPRECATED !!!!! */</span>
<a name="freeglut_ext.h-216"></a><span class="cm">/* If you have a serious need for these functions in your application, please either</span>
<a name="freeglut_ext.h-217"></a><span class="cm"> * contact the &quot;freeglut&quot; developer community at freeglut-developer@lists.sourceforge.net,</span>
<a name="freeglut_ext.h-218"></a><span class="cm"> * switch to the OpenGLUT library, or else port your joystick functionality over to PLIB&#39;s</span>
<a name="freeglut_ext.h-219"></a><span class="cm"> * &quot;js&quot; library.</span>
<a name="freeglut_ext.h-220"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-221"></a><span class="kt">int</span>     <span class="nf">glutJoystickGetNumAxes</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span> <span class="p">);</span>
<a name="freeglut_ext.h-222"></a><span class="kt">int</span>     <span class="nf">glutJoystickGetNumButtons</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span> <span class="p">);</span>
<a name="freeglut_ext.h-223"></a><span class="kt">int</span>     <span class="nf">glutJoystickNotWorking</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span> <span class="p">);</span>
<a name="freeglut_ext.h-224"></a><span class="kt">float</span>   <span class="nf">glutJoystickGetDeadBand</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">int</span> <span class="n">axis</span> <span class="p">);</span>
<a name="freeglut_ext.h-225"></a><span class="kt">void</span>    <span class="nf">glutJoystickSetDeadBand</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">int</span> <span class="n">axis</span><span class="p">,</span> <span class="kt">float</span> <span class="n">db</span> <span class="p">);</span>
<a name="freeglut_ext.h-226"></a><span class="kt">float</span>   <span class="nf">glutJoystickGetSaturation</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">int</span> <span class="n">axis</span> <span class="p">);</span>
<a name="freeglut_ext.h-227"></a><span class="kt">void</span>    <span class="nf">glutJoystickSetSaturation</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">int</span> <span class="n">axis</span><span class="p">,</span> <span class="kt">float</span> <span class="n">st</span> <span class="p">);</span>
<a name="freeglut_ext.h-228"></a><span class="kt">void</span>    <span class="nf">glutJoystickSetMinRange</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">float</span> <span class="o">*</span><span class="n">axes</span> <span class="p">);</span>
<a name="freeglut_ext.h-229"></a><span class="kt">void</span>    <span class="nf">glutJoystickSetMaxRange</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">float</span> <span class="o">*</span><span class="n">axes</span> <span class="p">);</span>
<a name="freeglut_ext.h-230"></a><span class="kt">void</span>    <span class="nf">glutJoystickSetCenter</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">float</span> <span class="o">*</span><span class="n">axes</span> <span class="p">);</span>
<a name="freeglut_ext.h-231"></a><span class="kt">void</span>    <span class="nf">glutJoystickGetMinRange</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">float</span> <span class="o">*</span><span class="n">axes</span> <span class="p">);</span>
<a name="freeglut_ext.h-232"></a><span class="kt">void</span>    <span class="nf">glutJoystickGetMaxRange</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">float</span> <span class="o">*</span><span class="n">axes</span> <span class="p">);</span>
<a name="freeglut_ext.h-233"></a><span class="kt">void</span>    <span class="nf">glutJoystickGetCenter</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ident</span><span class="p">,</span> <span class="kt">float</span> <span class="o">*</span><span class="n">axes</span> <span class="p">);</span>
<a name="freeglut_ext.h-234"></a>
<a name="freeglut_ext.h-235"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-236"></a><span class="cm"> * Initialization functions, see fg_init.c</span>
<a name="freeglut_ext.h-237"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-238"></a><span class="cm">/* to get the typedef for va_list */</span>
<a name="freeglut_ext.h-239"></a><span class="cp">#include</span> <span class="cpf">&lt;stdarg.h&gt;</span><span class="cp"></span>
<a name="freeglut_ext.h-240"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitContextVersion</span><span class="p">(</span> <span class="kt">int</span> <span class="n">majorVersion</span><span class="p">,</span> <span class="kt">int</span> <span class="n">minorVersion</span> <span class="p">);</span>
<a name="freeglut_ext.h-241"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitContextFlags</span><span class="p">(</span> <span class="kt">int</span> <span class="n">flags</span> <span class="p">);</span>
<a name="freeglut_ext.h-242"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitContextProfile</span><span class="p">(</span> <span class="kt">int</span> <span class="n">profile</span> <span class="p">);</span>
<a name="freeglut_ext.h-243"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitErrorFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">fmt</span><span class="p">,</span> <span class="kt">va_list</span> <span class="n">ap</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-244"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitWarningFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">fmt</span><span class="p">,</span> <span class="kt">va_list</span> <span class="n">ap</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_ext.h-245"></a>
<a name="freeglut_ext.h-246"></a><span class="cm">/* OpenGL &gt;= 2.0 support */</span>
<a name="freeglut_ext.h-247"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetVertexAttribCoord3</span><span class="p">(</span><span class="n">GLint</span> <span class="n">attrib</span><span class="p">);</span>
<a name="freeglut_ext.h-248"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetVertexAttribNormal</span><span class="p">(</span><span class="n">GLint</span> <span class="n">attrib</span><span class="p">);</span>
<a name="freeglut_ext.h-249"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetVertexAttribTexCoord2</span><span class="p">(</span><span class="n">GLint</span> <span class="n">attrib</span><span class="p">);</span>
<a name="freeglut_ext.h-250"></a>
<a name="freeglut_ext.h-251"></a><span class="cm">/* Mobile platforms lifecycle */</span>
<a name="freeglut_ext.h-252"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitContextFunc</span><span class="p">(</span><span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)());</span>
<a name="freeglut_ext.h-253"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutAppStatusFunc</span><span class="p">(</span><span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span><span class="kt">int</span><span class="p">));</span>
<a name="freeglut_ext.h-254"></a><span class="cm">/* state flags that can be passed to callback set by glutAppStatusFunc */</span>
<a name="freeglut_ext.h-255"></a><span class="cp">#define GLUT_APPSTATUS_PAUSE                0x0001</span>
<a name="freeglut_ext.h-256"></a><span class="cp">#define GLUT_APPSTATUS_RESUME               0x0002</span>
<a name="freeglut_ext.h-257"></a>
<a name="freeglut_ext.h-258"></a><span class="cm">/*</span>
<a name="freeglut_ext.h-259"></a><span class="cm"> * GLUT API macro definitions -- the display mode definitions</span>
<a name="freeglut_ext.h-260"></a><span class="cm"> */</span>
<a name="freeglut_ext.h-261"></a><span class="cp">#define  GLUT_CAPTIONLESS                   0x0400</span>
<a name="freeglut_ext.h-262"></a><span class="cp">#define  GLUT_BORDERLESS                    0x0800</span>
<a name="freeglut_ext.h-263"></a><span class="cp">#define  GLUT_SRGB                          0x1000</span>
<a name="freeglut_ext.h-264"></a>
<a name="freeglut_ext.h-265"></a><span class="cp">#ifdef __cplusplus</span>
<a name="freeglut_ext.h-266"></a>    <span class="p">}</span>
<a name="freeglut_ext.h-267"></a><span class="cp">#endif</span>
<a name="freeglut_ext.h-268"></a>
<a name="freeglut_ext.h-269"></a><span class="cm">/*** END OF FILE ***/</span>
<a name="freeglut_ext.h-270"></a>
<a name="freeglut_ext.h-271"></a><span class="cp">#endif </span><span class="cm">/* __FREEGLUT_EXT_H__ */</span><span class="cp"></span>
</pre></div>
</td></tr></table>
    </div>
  


        </div>
        
      </div>
    </div>
  </div>
  
  <div data-module="source/set-changeset" data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"></div>



  
    
    
    
  
  

  </div>

        
        
        
          
    
    
  
        
      </div>
    </div>
    <div id="code-search-cta"></div>
  </div>

      </div>
    </div>
  
</div>

<div id="adg3-dialog"></div>


  

<div data-module="components/mentions/index">
  
    
    
  
  
    
    
  
  
    
    
  
</div>
<div data-module="components/typeahead/emoji/index">
  
    
    
  
</div>

<div data-module="components/repo-typeahead/index">
  
    
    
  
</div>

    
    
  

    
    
  

    
    
  

    
    
  


  


    
    
  

    
    
  


  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  


  
  
  <aui-inline-dialog
    id="help-menu-dialog"
    data-aui-alignment="bottom right"

    
    data-aui-alignment-static="true"
    data-module="header/help-menu"
    responds-to="toggle"
    aria-hidden="true">

  <div id="help-menu-section">
    <h1 class="help-menu-heading">Help</h1>

    <form id="help-menu-search-form" class="aui" target="_blank" method="get"
        action="https://support.atlassian.com/customer/search">
      <span id="help-menu-search-icon" class="aui-icon aui-icon-large aui-iconfont-search"></span>
      <input id="help-menu-search-form-input" name="q" class="text" type="text" placeholder="Ask a question">
    </form>

    <ul id="help-menu-links">
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationHome"
            href="https://confluence.atlassian.com/x/bgozDQ" target="_blank">
          Online help
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="GitTutorials"
            href="https://www.atlassian.com/git?utm_source=bitbucket&amp;utm_medium=link&amp;utm_campaign=help_dropdown&amp;utm_content=learn_git"
            target="_blank">
          Learn Git
        </a>
      </li>
      <li>
        <a id="keyboard-shortcuts-link"
           href="#">Keyboard shortcuts</a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationTutorials"
            href="https://confluence.atlassian.com/x/Q4sFLQ" target="_blank">
          Bitbucket tutorials
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="SiteStatus"
            href="https://status.bitbucket.org/" target="_blank">
          Site status
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="Home"
            href="https://support.atlassian.com/bitbucket-cloud/" target="_blank">
          Support
        </a>
      </li>
    </ul>
  </div>
</aui-inline-dialog>
  


  <div class="omnibar" data-module="components/omnibar/index">
    <form class="omnibar-form aui"></form>
  </div>
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  





  

  <div class="_mustache-templates">
    
      <script id="branch-checkout-template" type="text/html">
        

<div id="checkout-branch-contents">
  <div class="command-line">
    <p>
      Check out this branch on your local machine to begin working on it.
    </p>
    <input type="text" class="checkout-command" readonly="readonly"
        
           value="git fetch && git checkout [[branchName]]"
        
        >
  </div>
  
    <div class="sourcetree-callout clone-in-sourcetree"
  data-module="components/clone/clone-in-sourcetree"
  data-https-url="https://Yanosik@bitbucket.org/Yanosik/zapart.git"
  data-ssh-url="git@bitbucket.org:Yanosik/zapart.git">

  <div>
    <button class="aui-button aui-button-primary">
      
        Check out in Sourcetree
      
    </button>
  </div>

  <p class="windows-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_win" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Windows.
    
  </p>
  <p class="mac-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_mac" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Mac.
    
  </p>
</div>
  
</div>

      </script>
    
      <script id="branch-dialog-template" type="text/html">
        

<div class="tabbed-filter-widget branch-dialog">
  <div class="tabbed-filter">
    <input placeholder="Filter branches" class="filter-box" autosave="branch-dropdown-29632644" type="text">
    [[^ignoreTags]]
      <div class="aui-tabs horizontal-tabs aui-tabs-disabled filter-tabs">
        <ul class="tabs-menu">
          <li class="menu-item active-tab"><a href="#branches">Branches</a></li>
          <li class="menu-item"><a href="#tags">Tags</a></li>
        </ul>
      </div>
    [[/ignoreTags]]
  </div>
  
    <div class="tab-pane active-pane" id="branches" data-filter-placeholder="Filter branches">
      <ol class="filter-list">
        <li class="empty-msg">No matching branches</li>
        [[#branches]]
          
            [[#hasMultipleHeads]]
              [[#heads]]
                <li class="comprev filter-item">
                  <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/freeglut_ext.h?at=[[safeName]]"
                     title="[[name]]">
                    [[name]] ([[shortChangeset]])
                  </a>
                </li>
              [[/heads]]
            [[/hasMultipleHeads]]
            [[^hasMultipleHeads]]
              <li class="comprev filter-item">
                <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/freeglut_ext.h?at=[[safeName]]" title="[[name]]">
                  [[name]]
                </a>
              </li>
            [[/hasMultipleHeads]]
          
        [[/branches]]
      </ol>
    </div>
    <div class="tab-pane" id="tags" data-filter-placeholder="Filter tags">
      <ol class="filter-list">
        <li class="empty-msg">No matching tags</li>
        [[#tags]]
          <li class="comprev filter-item">
            <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/freeglut_ext.h?at=[[safeName]]" title="[[name]]">
              [[name]]
            </a>
          </li>
        [[/tags]]
      </ol>
    </div>
  
</div>

      </script>
    
      <script id="image-upload-template" type="text/html">
        

<form id="upload-image" method="POST"
    
      action="/xhr/Yanosik/zapart/image-upload/"
    >
  <input type='hidden' name='csrfmiddlewaretoken' value='dan7YfiE88uDNBowy85XR5nyhylLLShGltjHgZIXW3noEZvlaRO4NUibjBjnVjdp' />

  <div class="drop-target">
    <p class="centered">Drag image here</p>
  </div>

  
  <div>
    <button class="aui-button click-target">Select an image</button>
    <input name="file" type="file" class="hidden file-target"
           accept="image/jpeg, image/gif, image/png" />
    <input type="submit" class="hidden">
  </div>
</form>


      </script>
    
      <script id="mention-result" type="text/html">
        
<span class="mention-result">
  <span class="aui-avatar aui-avatar-small mention-result--avatar">
    <span class="aui-avatar-inner">
      <img src="[[avatar_url]]">
    </span>
  </span>
  [[#display_name]]
    <span class="display-name mention-result--display-name">[[&display_name]]</span> <small class="username mention-result--username">[[&username]]</small>
  [[/display_name]]
  [[^display_name]]
    <span class="username mention-result--username">[[&username]]</span>
  [[/display_name]]
  [[#is_teammate]][[^is_team]]
    <span class="aui-lozenge aui-lozenge-complete aui-lozenge-subtle mention-result--lozenge">teammate</span>
  [[/is_team]][[/is_teammate]]
</span>
      </script>
    
      <script id="mention-call-to-action" type="text/html">
        
[[^query]]
<li class="bb-typeahead-item">Begin typing to search for a user</li>
[[/query]]
[[#query]]
<li class="bb-typeahead-item">Continue typing to search for a user</li>
[[/query]]

      </script>
    
      <script id="mention-no-results" type="text/html">
        
[[^searching]]
<li class="bb-typeahead-item">Found no matching users for <em>[[query]]</em>.</li>
[[/searching]]
[[#searching]]
<li class="bb-typeahead-item bb-typeahead-searching">Searching for <em>[[query]]</em>.</li>
[[/searching]]

      </script>
    
      <script id="emoji-result" type="text/html">
        
<span class="emoji-result">
  <span class="emoji-result--avatar">
    <img class="emoji" src="[[src]]">
  </span>
  <span class="name emoji-result--name">[[&name]]</span>
</span>

      </script>
    
      <script id="repo-typeahead-result" type="text/html">
        <span class="aui-avatar aui-avatar-project aui-avatar-xsmall">
  <span class="aui-avatar-inner">
    <img src="[[avatar]]">
  </span>
</span>
<span class="owner">[[&owner]]</span>/<span class="slug">[[&slug]]</span>

      </script>
    
      <script id="share-form-template" type="text/html">
        

<div class="error aui-message hidden">
  <span class="aui-icon icon-error"></span>
  <div class="message"></div>
</div>
<form class="aui">
  <table class="widget bb-list aui">
    <thead>
    <tr class="assistive">
      <th class="user">User</th>
      <th class="role">Role</th>
      <th class="actions">Actions</th>
    </tr>
    </thead>
    <tbody>
      <tr class="form">
        <td colspan="2">
          <input type="text" class="text bb-user-typeahead user-or-email"
                 placeholder="Username or email address"
                 autocomplete="off"
                 data-bb-typeahead-focus="false"
                 [[#disabled]]disabled[[/disabled]]>
        </td>
        <td class="actions">
          <button type="submit" class="aui-button aui-button-light" disabled>Add</button>
        </td>
      </tr>
    </tbody>
  </table>
</form>

      </script>
    
      <script id="share-detail-template" type="text/html">
        

[[#username]]
<td class="user
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[displayName]]">
    <a href="/[[username]]/" class="user">
      <span class="aui-avatar aui-avatar-xsmall">
        <span class="aui-avatar-inner">
          <img src="[[avatar]]">
        </span>
      </span>
      <span>[[displayName]]</span>
    </a>
  </div>
</td>
[[/username]]
[[^username]]
<td class="email
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[email]]">
    <span class="aui-icon aui-icon-small aui-iconfont-email"></span>
    [[email]]
  </div>
</td>
[[/username]]
<td class="role
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    [[#group]]
      [[#hasCustomGroups]]
        <select class="group [[#noGroupChoices]]hidden[[/noGroupChoices]]">
          [[#groups]]
            <option value="[[slug]]"
                [[#isSelected]]selected[[/isSelected]]>
              [[name]]
            </option>
          [[/groups]]
        </select>
      [[/hasCustomGroups]]
      [[^hasCustomGroups]]
      <label>
        <input type="checkbox" class="admin"
            [[#isAdmin]]checked[[/isAdmin]]>
        Administrator
      </label>
      [[/hasCustomGroups]]
    [[/group]]
    [[^group]]
      <ul>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^read]]aui-lozenge-subtle[[/read]]"
            data-permission="read">
          read
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^write]]aui-lozenge-subtle[[/write]]"
            data-permission="write">
          write
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^admin]]aui-lozenge-subtle[[/admin]]"
            data-permission="admin">
          admin
        </li>
      </ul>
    [[/group]]
  </div>
</td>
<td class="actions
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    <a href="#" class="delete">
      <span class="aui-icon aui-icon-small aui-iconfont-remove">Delete</span>
    </a>
  </div>
</td>

      </script>
    
      <script id="share-team-template" type="text/html">
        

<div class="clearfix">
  <span class="team-avatar-container">
    <span class="aui-avatar aui-avatar-medium">
      <span class="aui-avatar-inner">
        <img src="[[avatar]]">
      </span>
    </span>
  </span>
  <span class="team-name-container">
    [[display_name]]
  </span>
</div>
<p class="helptext">
  
    Existing users are granted access to this team immediately.
    New users will be sent an invitation.
  
</p>
<div class="share-form"></div>

      </script>
    
      <script id="scope-list-template" type="text/html">
        <ul class="scope-list">
  [[#scopes]]
    <li class="scope-list--item">
      <span class="scope-list--icon aui-icon aui-icon-small [[icon]]"></span>
      <span class="scope-list--description">[[description]]</span>
    </li>
  [[/scopes]]
</ul>

      </script>
    
      <script id="source-changeset" type="text/html">
        

<a href="/Yanosik/zapart/src/[[raw_node]]/[[path]]?at=master"
    class="[[#selected]]highlight[[/selected]]"
    data-hash="[[node]]">
  [[#author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="[[author.avatar]]">
      </span>
    </span>
    <span class="author" title="[[raw_author]]">[[author.display_name]]</span>
  [[/author.username]]
  [[^author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg">
      </span>
    </span>
    <span class="author unmapped" title="[[raw_author]]">[[author]]</span>
  [[/author.username]]
  <time datetime="[[utctimestamp]]" data-title="true">[[utctimestamp]]</time>
  <span class="message">[[message]]</span>
</a>

      </script>
    
      <script id="embed-template" type="text/html">
        

<form class="aui inline-dialog-embed-dialog">
  <label for="embed-code-[[dialogId]]">Embed this source in another page:</label>
  <input type="text" readonly="true" value="&lt;script src=&quot;[[url]]&quot;&gt;&lt;/script&gt;" id="embed-code-[[dialogId]]" class="embed-code">
</form>

      </script>
    
      <script id="edit-form-template" type="text/html">
        


<form class="bb-content-container online-edit-form aui"
      data-repository="[[owner]]/[[slug]]"
      data-destination-repository="[[destinationOwner]]/[[destinationSlug]]"
      data-local-id="[[localID]]"
      [[#isWriter]]data-is-writer="true"[[/isWriter]]
      [[#hasPushAccess]]data-has-push-access="true"[[/hasPushAccess]]
      [[#isPullRequest]]data-is-pull-request="true"[[/isPullRequest]]
      [[#hideCreatePullRequest]]data-hide-create-pull-request="true"[[/hideCreatePullRequest]]
      data-hash="[[hash]]"
      data-branch="[[branch]]"
      data-path="[[path]]"
      data-is-create="[[isCreate]]"
      data-preview-url="/xhr/[[owner]]/[[slug]]/preview/[[hash]]/[[encodedPath]]"
      data-preview-error="We had trouble generating your preview."
      data-unsaved-changes-error="Your changes will be lost. Are you sure you want to leave?">
  <div class="bb-content-container-header">
    <div class="bb-content-container-header-primary">
      <span class="bb-content-container-heading">
        [[#isCreate]]
          [[#branch]]
            
              Creating <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
            
          [[/branch]]
          [[^branch]]
            [[#path]]
              
                Creating <span class="edit-path">[[path]]</span>
              
            [[/path]]
            [[^path]]
              
                Creating <span class="edit-path">unnamed file</span>
              
            [[/path]]
          [[/branch]]
        [[/isCreate]]
        [[^isCreate]]
          
            Editing <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
          
        [[/isCreate]]
      </span>
    </div>
    <div class="bb-content-container-header-secondary">
      <div class="hunk-nav aui-buttons">
        <button class="prev-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Previous change">
          <span class="aui-icon aui-icon-small aui-iconfont-up">Previous change</span>
        </button>
        <button class="next-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Next change">
          <span class="aui-icon aui-icon-small aui-iconfont-down">Next change</span>
        </button>
      </div>
    </div>
  </div>
  <div class="bb-content-container-body has-header has-footer file-editor">
    <textarea id="id_source"></textarea>
  </div>
  <div class="preview-pane"></div>
  <div class="bb-content-container-footer">
    <div class="bb-content-container-footer-primary">
      <div id="syntax-mode" class="bb-content-container-item field">
        <label for="id_syntax-mode" class="online-edit-form--label">Syntax mode:</label>
        <select id="id_syntax-mode">
          [[#syntaxes]]
            <option value="[[#mime]][[mime]][[/mime]][[^mime]][[mode]][[/mime]]">[[name]]</option>
          [[/syntaxes]]
        </select>
      </div>
      <div id="indent-mode" class="bb-content-container-item field">
        <label for="id_indent-mode" class="online-edit-form--label">Indent mode:</label>
        <select id="id_indent-mode">
          <option value="tabs">Tabs</option>
          <option value="spaces">Spaces</option>
        </select>
      </div>
      <div id="indent-size" class="bb-content-container-item field">
        <label for="id_indent-size" class="online-edit-form--label">Indent size:</label>
        <select id="id_indent-size">
          <option value="2">2</option>
          <option value="4">4</option>
          <option value="8">8</option>
        </select>
      </div>
      <div id="wrap-mode" class="bb-content-container-item field">
        <label for="id_wrap-mode" class="online-edit-form--label">Line wrap:</label>
        <select id="id_wrap-mode">
          <option value="">Off</option>
          <option value="soft">On</option>
        </select>
      </div>
    </div>
    <div class="bb-content-container-footer-secondary">
      [[^isCreate]]
        <button class="preview-button aui-button aui-button-light"
                disabled="disabled" aria-disabled="true"
                data-preview-label="View diff"
                data-edit-label="Edit file">View diff</button>
      [[/isCreate]]
      <button class="save-button aui-button aui-button-primary"
              disabled="disabled" aria-disabled="true">Commit</button>
      [[^isCreate]]
        <a class="aui-button aui-button-link cancel-link" href="#">Cancel</a>
      [[/isCreate]]
    </div>
  </div>
</form>

      </script>
    
      <script id="commit-form-template" type="text/html">
        

<form class="aui commit-form"
      data-title="Commit changes"
      [[#isDelete]]
        data-default-message="[[filename]] deleted online with Bitbucket"
      [[/isDelete]]
      [[#isCreate]]
        data-default-message="[[filename]] created online with Bitbucket"
      [[/isCreate]]
      [[^isDelete]]
        [[^isCreate]]
          data-default-message="[[filename]] edited online with Bitbucket"
        [[/isCreate]]
      [[/isDelete]]
      data-fork-error="We had trouble creating your fork."
      data-commit-error="We had trouble committing your changes."
      data-pull-request-error="We had trouble creating your pull request."
      data-update-error="We had trouble updating your pull request."
      data-branch-conflict-error="A branch with that name already exists."
      data-forking-message="Forking repository"
      data-committing-message="Committing changes"
      data-merging-message="Branching and merging changes"
      data-creating-pr-message="Creating pull request"
      data-updating-pr-message="Updating pull request"
      data-cta-label="Commit"
      data-cancel-label="Cancel">
  [[#isDelete]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <span class="message">
        
          Committing this change will delete [[filename]] from your repository.
        
      </span>
    </div>
  [[/isDelete]]
  <div class="aui-message error hidden">
    <span class="aui-icon icon-error"></span>
    <span class="message"></span>
  </div>
  [[^isWriter]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <p class="title">
        
          You don't have write access to this repository.
        
      </p>
      <span class="message">
        
          We'll create a fork for your changes and submit a
          pull request back to this repository.
        
      </span>
    </div>
  [[/isWriter]]
  [[#isRename]]
    <div class="field-group">
      <label for="id_path">New path</label>
      <input type="text" id="id_path" class="text" value="[[path]]"/>
    </div>
  [[/isRename]]
  <div class="field-group">
    <label for="id_message">Commit message</label>
    <textarea id="id_message" class="long-field textarea"></textarea>
  </div>
  [[^isPullRequest]]
    [[#isWriter]]
      <fieldset class="group">
        <div class="checkbox">
          [[#hasPushAccess]]
            [[^hideCreatePullRequest]]
              <input id="id_create-pullrequest" class="checkbox" type="checkbox">
              <label for="id_create-pullrequest">Create a pull request for this change</label>
            [[/hideCreatePullRequest]]
          [[/hasPushAccess]]
          [[^hasPushAccess]]
            <input id="id_create-pullrequest" class="checkbox" type="checkbox" checked="checked" aria-disabled="true" disabled="true">
            <label for="id_create-pullrequest" title="Branch restrictions do not allow you to update this branch.">Create a pull request for this change</label>
          [[/hasPushAccess]]
        </div>
      </fieldset>
      <div id="pr-fields">
        <div id="branch-name-group" class="field-group">
          <label for="id_branch-name">Branch name</label>
          <input type="text" id="id_branch-name" class="text long-field">
        </div>
        

<div class="field-group" id="id_reviewers_group">
  <label for="reviewers">Reviewers</label>

  
  <input id="reviewers" name="reviewers" type="hidden"
          value=""
          data-mention-url="/xhr/mentions/repositories/:dest_username/:dest_slug"
          data-reviewers="[]"
          data-suggested="[]"
          data-locked="[]">

  <div class="error"></div>
  <div class="suggested-reviewers"></div>

</div>

      </div>
    [[/isWriter]]
  [[/isPullRequest]]
  <button type="submit" id="id_submit">Commit</button>
</form>

      </script>
    
      <script id="merge-message-template" type="text/html">
        Merged [[hash]] into [[branch]]

[[message]]

      </script>
    
      <script id="commit-merge-error-template" type="text/html">
        



  We had trouble merging your changes. We stored them on the <strong>[[branch]]</strong> branch, so feel free to
  <a href="/[[owner]]/[[slug]]/full-commit/[[hash]]/[[path]]?at=[[encodedBranch]]">view them</a> or
  <a href="#" class="create-pull-request-link">create a pull request</a>.


      </script>
    
      <script id="selected-reviewer-template" type="text/html">
        <div class="aui-avatar aui-avatar-xsmall">
  <div class="aui-avatar-inner">
    <img src="[[avatar_url]]">
  </div>
</div>
[[display_name]]

      </script>
    
      <script id="suggested-reviewer-template" type="text/html">
        <button class="aui-button aui-button-link" type="button" tabindex="-1">[[display_name]]</button>

      </script>
    
      <script id="suggested-reviewers-template" type="text/html">
        

<span class="suggested-reviewer-list-label">Recent:</span>
<ul class="suggested-reviewer-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-form-template" type="text/html">
        <div class="omnibar-input-container">
  <input class="omnibar-input" type="text" [[#placeholder]]placeholder="[[placeholder]]"[[/placeholder]]>
</div>
<ul class="omnibar-result-group-list"></ul>

      </script>
    
      <script id="omnibar-blank-slate-template" type="text/html">
        

<div class="omnibar-blank-slate">No results found</div>

      </script>
    
      <script id="omnibar-result-group-list-item-template" type="text/html">
        <div class="omnibar-result-group-header clearfix">
  <h2 class="omnibar-result-group-label" title="[[label]]">[[label]]</h2>
  <span class="omnibar-result-group-context" title="[[context]]">[[context]]</span>
</div>
<ul class="omnibar-result-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-result-list-item-template" type="text/html">
        [[#url]]
  <a href="[[&url]]" class="omnibar-result-label">[[&label]]</a>
[[/url]]
[[^url]]
  <span class="omnibar-result-label">[[&label]]</span>
[[/url]]
[[#context]]
  <span class="omnibar-result-context">[[context]]</span>
[[/context]]

      </script>
    
  </div>




  
  


<script nonce="SOF0GG3ium4WyWCT">
  window.__initial_state__ = {"global": {"features": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "locale": "en", "geoip_country": "PL", "targetFeatures": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "isFocusedTask": false, "teams": [], "bitbucketActions": [{"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 100, "url": "/repo/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Repository<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repository-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 110, "url": "/account/create-team/", "tab_name": null, "can_display": true, "label": "<strong>Team<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "team-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 120, "url": "/account/projects/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Project<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "project-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 130, "url": "/snippets/new?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Snippet<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "snippet-create-drawer-item", "icon": ""}], "targetUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "isNavigationOpen": true, "path": "/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_ext.h", "focusedTaskBackButtonUrl": "https://bitbucket.org/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/?at=master", "currentUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}}, "connect": {}, "repository": {"section": {"connectActions": [], "cloneProtocol": "https", "currentRepository": {"scm": "git", "website": "", "name": "Zapart", "language": "", "links": {"clone": [{"href": "https://Yanosik@bitbucket.org/Yanosik/zapart.git", "name": "https"}, {"href": "git@bitbucket.org:Yanosik/zapart.git", "name": "ssh"}], "self": {"href": "https://bitbucket.org/!api/2.0/repositories/Yanosik/zapart"}, "html": {"href": "https://bitbucket.org/Yanosik/zapart"}, "avatar": {"href": "https://bitbucket.org/Yanosik/zapart/avatar/32/"}}, "full_name": "Yanosik/zapart", "owner": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "type": "repository", "slug": "zapart", "is_private": true, "uuid": "{8f59423b-9494-4138-af6f-56e97d727ae4}"}, "menuItems": [{"analytics_label": "repository.overview", "icon_class": "icon-overview", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/overview", "tab_name": "overview", "can_display": true, "label": "Overview", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-overview-link", "icon": "icon-overview"}, {"analytics_label": "repository.source", "icon_class": "icon-source", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/src", "tab_name": "source", "can_display": true, "label": "Source", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-source-link", "icon": "icon-source"}, {"analytics_label": "repository.commits", "icon_class": "icon-commits", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/commits/", "tab_name": "commits", "can_display": true, "label": "Commits", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-commits-link", "icon": "icon-commits"}, {"analytics_label": "repository.branches", "icon_class": "icon-branches", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/", "tab_name": "branches", "can_display": true, "label": "Branches", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-branches-link", "icon": "icon-branches"}, {"analytics_label": "repository.pullrequests", "icon_class": "icon-pull-requests", "badge_label": "0 open pull requests", "weight": 500, "url": "/Yanosik/zapart/pull-requests/", "tab_name": "pullrequests", "can_display": true, "label": "Pull requests", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-pullrequests-link", "icon": "icon-pull-requests"}, {"analytics_label": "site.addon", "icon_class": "aui-iconfont-build", "badge_label": null, "weight": 550, "url": "/Yanosik/zapart/addon/pipelines-installer/home", "tab_name": "repopage-kbbqoq-add-on-link", "can_display": true, "label": "Pipelines", "anchor": true, "analytics_payload": {}, "icon_url": null, "type": "connect_menu_item", "id": "repopage-kbbqoq-add-on-link", "target": "_self"}, {"analytics_label": "repository.downloads", "icon_class": "icon-downloads", "badge_label": null, "weight": 800, "url": "/Yanosik/zapart/downloads/", "tab_name": "downloads", "can_display": true, "label": "Downloads", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-downloads-link", "icon": "icon-downloads"}, {"analytics_label": "user.addon", "icon_class": "aui-iconfont-unfocus", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/addon/bitbucket-trello-addon/trello-board", "tab_name": "repopage-5ABRne-add-on-link", "can_display": true, "label": "Boards", "anchor": false, "analytics_payload": {}, "icon_url": "https://bitbucket-assetroot.s3.amazonaws.com/add-on/icons/35ceae0c-17b1-443c-a6e8-d9de1d7cccdb.svg?Signature=798RyrUGDEslRxsDsrm3TMPD0u4%3D&Expires=1511782792&AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&versionId=3oqdrZZjT.HijZ3kHTPsXE6IcSjhCG.P", "type": "connect_menu_item", "id": "repopage-5ABRne-add-on-link", "target": "_self"}, {"analytics_label": "repository.settings", "icon_class": "icon-settings", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/admin", "tab_name": "admin", "can_display": true, "label": "Settings", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-settings-link", "icon": "icon-settings"}], "bitbucketActions": [{"analytics_label": "repository.clone", "icon_class": "icon-clone", "badge_label": null, "weight": 100, "url": "#clone", "tab_name": "clone", "can_display": true, "label": "<strong>Clone<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-clone-button", "icon": "icon-clone"}, {"analytics_label": "repository.create_branch", "icon_class": "icon-create-branch", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/branch", "tab_name": "create-branch", "can_display": true, "label": "Create a <strong>branch<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-branch-link", "icon": "icon-create-branch"}, {"analytics_label": "create_pullrequest", "icon_class": "icon-create-pull-request", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/pull-requests/new", "tab_name": "create-pullreqs", "can_display": true, "label": "Create a <strong>pull request<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-pull-request-link", "icon": "icon-create-pull-request"}, {"analytics_label": "repository.compare", "icon_class": "aui-icon-small aui-iconfont-devtools-compare", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/compare", "tab_name": "compare", "can_display": true, "label": "<strong>Compare<\/strong> branches or tags", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-compare-link", "icon": "aui-icon-small aui-iconfont-devtools-compare"}, {"analytics_label": "repository.fork", "icon_class": "icon-fork", "badge_label": null, "weight": 500, "url": "/Yanosik/zapart/fork", "tab_name": "fork", "can_display": true, "label": "<strong>Fork<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-fork-link", "icon": "icon-fork"}], "activeMenuItem": "source"}}};
  window.__settings__ = {"SOCIAL_AUTH_ATLASSIANID_LOGOUT_URL": "https://id.atlassian.com/logout", "CANON_URL": "https://bitbucket.org", "API_CANON_URL": "https://api.bitbucket.org"};
</script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/jsi18n/en/djangojs.js" nonce="SOF0GG3ium4WyWCT"></script>

  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/locales/en.js"></script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/vendor.js" nonce="SOF0GG3ium4WyWCT"></script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/app.js" nonce="SOF0GG3ium4WyWCT"></script>


<script async src="https://www.google-analytics.com/analytics.js" nonce="SOF0GG3ium4WyWCT"></script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"licenseKey":"a2cef8c3d3","agent":"","transactionName":"Z11RZxdWW0cEVkYLDV4XdUYLVEFdClsdAAtEWkZQDlJBGgRFQhFMQl1DXFcZQ10AQkFYBFlUVlEXWEJHAA==","applicationID":"1841284","errorBeacon":"bam.nr-data.net","applicationTime":399}</script>
</body>
</html>