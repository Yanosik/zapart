<!DOCTYPE html>
<html lang="en">
<head>
  <meta id="bb-bootstrap" data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="5a097ca786e7c03ff34850dd" />
  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta charset="utf-8">
  <title>
  Yanosik / Zapart 
  / source  / Include / GL / wglew.h
 &mdash; Bitbucket
</title>
  <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
  


<meta id="bb-canon-url" name="bb-canon-url" content="https://bitbucket.org">
<meta name="bb-api-canon-url" content="https://api.bitbucket.org">
<meta name="apitoken" content="{&quot;token&quot;: &quot;sOQErUNDp0vbOMSV277VxGjqCWoTdnh09hLabDvFT-HCkel0CitXfS0GNzHLYvHfKn-PVQIEObKh2ymlSpV_6YScfV8Tv0o199pdAhIpc5IRnb4J&quot;, &quot;expiration&quot;: 1511781319.263218}">

<meta name="bb-commit-hash" content="370568ebc84f">
<meta name="bb-app-node" content="app-166">
<meta name="bb-view-name" content="bitbucket.apps.repo2.views.filebrowse">
<meta name="ignore-whitespace" content="False">
<meta name="tab-size" content="None">
<meta name="locale" content="en">

<meta name="application-name" content="Bitbucket">
<meta name="apple-mobile-web-app-title" content="Bitbucket">


<meta name="theme-color" content="#0049B0">
<meta name="msapplication-TileColor" content="#0052CC">
<meta name="msapplication-TileImage" content="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/mstile-150x150.png">
<link rel="apple-touch-icon" sizes="180x180" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/apple-touch-icon.png">
<link rel="icon" sizes="192x192" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/android-chrome-192x192.png">

<link rel="icon" sizes="16x16 24x24 32x32 64x64" type="image/x-icon" href="/favicon.ico?v=2">
<link rel="mask-icon" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/safari-pinned-tab.svg" color="#0052CC">

<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Bitbucket">

  <meta name="description" content="">
  
  
    
  



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/vendor.css" />
<link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/app.css" />



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/adg3.css">
  
  <script nonce="dK7CPKzQkyQ7a0iZ">
  window.__sentry__ = {"dsn": "https://ea49358f525d4019945839a3d7a8292a@sentry.io/159509", "release": "370568ebc84f (production)", "tags": {"project": "bitbucket-core"}, "environment": "production"};
</script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/sentry.js" nonce="dK7CPKzQkyQ7a0iZ"></script>
  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/early.js" nonce="dK7CPKzQkyQ7a0iZ"></script>
  
  
    <link href="/Yanosik/zapart/rss?token=a07b0067199174cef445f1e8fa79b35f" rel="alternate nofollow" type="application/rss+xml" title="RSS feed for Zapart" />

</head>
<body class="production adg3  "
    data-static-url="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/"
data-base-url="https://bitbucket.org"
data-no-avatar-image="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg"
data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="{&quot;loginStatusUrl&quot;: &quot;https://id.atlassian.com/profile/rest/profile&quot;}"
data-settings="{&quot;MENTIONS_MIN_QUERY_LENGTH&quot;: 3}"

data-current-repo="{&quot;scm&quot;: &quot;git&quot;, &quot;readOnly&quot;: false, &quot;mainbranch&quot;: {&quot;name&quot;: &quot;master&quot;}, &quot;uuid&quot;: &quot;8f59423b-9494-4138-af6f-56e97d727ae4&quot;, &quot;language&quot;: &quot;&quot;, &quot;owner&quot;: {&quot;username&quot;: &quot;Yanosik&quot;, &quot;uuid&quot;: &quot;8aa0a974-0d88-41f1-b96a-c1ebd6c01af9&quot;, &quot;isTeam&quot;: false}, &quot;fullslug&quot;: &quot;Yanosik/zapart&quot;, &quot;slug&quot;: &quot;zapart&quot;, &quot;id&quot;: 29632644, &quot;pygmentsLanguage&quot;: null}"
data-current-cset="f71033cf7729d442b43ee6f67d340a04470f80ae"





data-browser-monitoring="true"
data-switch-create-pullrequest-commit-status="true"




>
<div id="page">
  
    <div id="adg3-navigation"
  
  
  >
  <nav class="skeleton-nav">
    <div class="skeleton-nav--left">
      <div class="skeleton-nav--left-top">
        <ul class="skeleton-nav--items">
          <li></li>
          <li></li>
          <li></li>
          <li class="skeleton--icon"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
        </ul>
      </div>
      <div class="skeleton-nav--left-bottom">
        <div class="skeleton-nav--left-bottom-wrapper">
          <div class="skeleton-nav--item-help"></div>
          <div class="skeleton-nav--item-profile"></div>
        </div>
      </div>
    </div>
    <div class="skeleton-nav--right">
      <ul class="skeleton-nav--items-wide">
        <li class="skeleton--icon-logo-container">
          <div class="skeleton--icon-container"></div>
          <div class="skeleton--icon-description"></div>
          <div class="skeleton--icon-logo"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
      </ul>
    </div>
  </nav>
</div>

    <div id="wrapper">
      
  


      
  <div id="nps-survey-container"></div>

 

      
  

<div id="account-warning" data-module="header/account-warning"
  data-unconfirmed-addresses="false"
  data-no-addresses="false"
  
></div>



      
  
<header id="aui-message-bar">
  
</header>


      <div id="content" role="main">

        
          <header class="app-header">
            <div class="app-header--primary">
              
                <div class="app-header--context">
                  <div class="app-header--breadcrumbs">
                    
  <ol class="aui-nav aui-nav-breadcrumbs">
    <li>
  <a href="/Yanosik/">Jan</a>
</li>

<li>
  <a href="/Yanosik/zapart">Zapart</a>
</li>
    
  <li>
    <a href="/Yanosik/zapart/src">
      Source
    </a>
  </li>

  </ol>

                  </div>
                  <h1 class="app-header--heading">
                    <span class="file-path">wglew.h</span>
                  </h1>
                </div>
              
            </div>

            <div class="app-header--secondary">
              
                
              
            </div>
          </header>
        

        
        
  <div class="aui-page-panel ">
    <div class="hidden">
  
  </div>
    <div class="aui-page-panel-inner">

      <div
        id="repo-content"
        class="aui-page-panel-content forks-enabled can-create"
        data-module="repo/index"
        
      >
        
        
  <div id="source-container" class="maskable" data-module="repo/source/index">
    



<header id="source-path">
  
    <div class="labels labels-csv">
      <div class="aui-buttons">
        <button data-branches-tags-url="/api/1.0/repositories/Yanosik/zapart/branches-tags"
                data-module="components/branch-dialog"
                
                class="aui-button branch-dialog-trigger" title="master">
          
            
              <span class="aui-icon aui-icon-small aui-iconfont-devtools-branch">Branch</span>
            
            <span class="name">master</span>
          
          <span class="aui-icon-dropdown"></span>
        </button>
        <button class="aui-button" id="checkout-branch-button"
                title="Check out this branch">
          <span class="aui-icon aui-icon-small aui-iconfont-devtools-clone">Check out branch</span>
          <span class="aui-icon-dropdown"></span>
        </button>
      </div>
      
    
    
  
    </div>
  
  
    <div class="secondary-actions">
      <div class="aui-buttons">
        
          <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/wglew.h?at=master"
            class="aui-button pjax-trigger source-toggle" aria-pressed="true">
            Source
          </a>
          <a href="/Yanosik/zapart/diff/Include/GL/wglew.h?diff2=f71033cf7729&at=master"
            class="aui-button pjax-trigger diff-toggle"
            title="Diff to previous change">
            Diff
          </a>
          <a href="/Yanosik/zapart/history-node/f71033cf7729/Include/GL/wglew.h?at=master"
            class="aui-button pjax-trigger history-toggle">
            History
          </a>
        
      </div>

      
        
        
      

    </div>
  
  <h1>
    
      
        <a href="/Yanosik/zapart/src/f71033cf7729?at=master"
          class="pjax-trigger root" title="Yanosik/zapart at f71033cf7729">Zapart</a> /
      
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/?at=master"
              class="pjax-trigger directory-name">Include</a> /
          
        
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/?at=master"
              class="pjax-trigger directory-name">GL</a> /
          
        
      
        
          
            <span class="file-name">wglew.h</span>
          
        
      
    
  </h1>
  
    
    
  
  <div class="clearfix"></div>
</header>


  
    
  

  <div id="editor-container" class="maskable"
       data-module="repo/source/editor"
       data-owner="Yanosik"
       data-slug="zapart"
       data-is-writer="true"
       data-has-push-access="true"
       data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"
       data-branch="master"
       data-path="Include/GL/wglew.h"
       data-source-url="/api/internal/repositories/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/wglew.h">
    <div id="source-view" class="file-source-container" data-module="repo/source/view-file" data-is-lfs="false">
      <div class="toolbar">
        <div class="primary">
          <div class="aui-buttons">
            
              <button id="file-history-trigger" class="aui-button aui-button-light changeset-info"
                      data-changeset="f71033cf7729d442b43ee6f67d340a04470f80ae"
                      data-path="Include/GL/wglew.h"
                      data-current="f71033cf7729d442b43ee6f67d340a04470f80ae">
                 

  <div class="aui-avatar aui-avatar-xsmall">
    <div class="aui-avatar-inner">
      <img src="https://bitbucket.org/account/Yanosik/avatar/16/?ts=1511779772">
    </div>
  </div>
  <span class="changeset-hash">f71033c</span>
  <time datetime="2017-11-13T12:06:29+00:00" class="timestamp"></time>
  <span class="aui-icon-dropdown"></span>

              </button>
            
          </div>
          
          <a href="/Yanosik/zapart/full-commit/f71033cf7729/Include/GL/wglew.h" id="full-commit-link"
             title="View full commit f71033c">Full commit</a>
        </div>
        <div class="secondary">
          
          <div class="aui-buttons">
            
              <a href="/Yanosik/zapart/annotate/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/wglew.h?at=master"
                 class="aui-button aui-button-light pjax-trigger blame-link">Blame</a>
              
            
            <a href="/Yanosik/zapart/raw/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/wglew.h" class="aui-button aui-button-light raw-link">Raw</a>
          </div>
          
            

            <div class="aui-buttons">
              
              <button id="file-edit-button" class="edit-button aui-button aui-button-light aui-button-split-main"
                  

                  >
                Edit
                
              </button>
              <button id="file-more-actions-button" class="aui-button aui-button-light aui-dropdown2-trigger aui-button-split-more" aria-owns="split-container-dropdown" aria-haspopup="true"
                  >
                More file actions
              </button>
            </div>
            <div id="split-container-dropdown" class="aui-dropdown2 aui-style-default" data-container="#editor-container">
              <ul class="aui-list-truncate">
                <li><a href="#" data-module="repo/source/rename-file" class="rename-link">Rename</a></li>
                <li><a href="#" data-module="repo/source/delete-file" class="delete-link">Delete</a></li>
              </ul>
            </div>
          
        </div>

        <div id="fileview-dropdown"
            class="aui-dropdown2 aui-style-default"
            data-fileview-container="#fileview-container"
            
            
            data-fileview-button="#fileview-trigger"
            data-module="connect/fileview">
          <div class="aui-dropdown2-section">
            <ul>
              <li>
                <a class="aui-dropdown2-radio aui-dropdown2-checked"
                    data-fileview-id="-1"
                    data-fileview-loaded="true"
                    data-fileview-target="#fileview-original"
                    data-fileview-connection-key=""
                    data-fileview-module-key="file-view-default">
                  Default File Viewer
                </a>
              </li>
              
            </ul>
          </div>
        </div>

        <div class="clearfix"></div>
      </div>
      <div id="fileview-container">
        <div id="fileview-original"
            class="fileview"
            data-module="repo/source/highlight-lines"
            data-fileview-loaded="true">
          


  
    <div class="file-source">
      <table class="codehilite highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre><a href="#wglew.h-1">   1</a>
<a href="#wglew.h-2">   2</a>
<a href="#wglew.h-3">   3</a>
<a href="#wglew.h-4">   4</a>
<a href="#wglew.h-5">   5</a>
<a href="#wglew.h-6">   6</a>
<a href="#wglew.h-7">   7</a>
<a href="#wglew.h-8">   8</a>
<a href="#wglew.h-9">   9</a>
<a href="#wglew.h-10">  10</a>
<a href="#wglew.h-11">  11</a>
<a href="#wglew.h-12">  12</a>
<a href="#wglew.h-13">  13</a>
<a href="#wglew.h-14">  14</a>
<a href="#wglew.h-15">  15</a>
<a href="#wglew.h-16">  16</a>
<a href="#wglew.h-17">  17</a>
<a href="#wglew.h-18">  18</a>
<a href="#wglew.h-19">  19</a>
<a href="#wglew.h-20">  20</a>
<a href="#wglew.h-21">  21</a>
<a href="#wglew.h-22">  22</a>
<a href="#wglew.h-23">  23</a>
<a href="#wglew.h-24">  24</a>
<a href="#wglew.h-25">  25</a>
<a href="#wglew.h-26">  26</a>
<a href="#wglew.h-27">  27</a>
<a href="#wglew.h-28">  28</a>
<a href="#wglew.h-29">  29</a>
<a href="#wglew.h-30">  30</a>
<a href="#wglew.h-31">  31</a>
<a href="#wglew.h-32">  32</a>
<a href="#wglew.h-33">  33</a>
<a href="#wglew.h-34">  34</a>
<a href="#wglew.h-35">  35</a>
<a href="#wglew.h-36">  36</a>
<a href="#wglew.h-37">  37</a>
<a href="#wglew.h-38">  38</a>
<a href="#wglew.h-39">  39</a>
<a href="#wglew.h-40">  40</a>
<a href="#wglew.h-41">  41</a>
<a href="#wglew.h-42">  42</a>
<a href="#wglew.h-43">  43</a>
<a href="#wglew.h-44">  44</a>
<a href="#wglew.h-45">  45</a>
<a href="#wglew.h-46">  46</a>
<a href="#wglew.h-47">  47</a>
<a href="#wglew.h-48">  48</a>
<a href="#wglew.h-49">  49</a>
<a href="#wglew.h-50">  50</a>
<a href="#wglew.h-51">  51</a>
<a href="#wglew.h-52">  52</a>
<a href="#wglew.h-53">  53</a>
<a href="#wglew.h-54">  54</a>
<a href="#wglew.h-55">  55</a>
<a href="#wglew.h-56">  56</a>
<a href="#wglew.h-57">  57</a>
<a href="#wglew.h-58">  58</a>
<a href="#wglew.h-59">  59</a>
<a href="#wglew.h-60">  60</a>
<a href="#wglew.h-61">  61</a>
<a href="#wglew.h-62">  62</a>
<a href="#wglew.h-63">  63</a>
<a href="#wglew.h-64">  64</a>
<a href="#wglew.h-65">  65</a>
<a href="#wglew.h-66">  66</a>
<a href="#wglew.h-67">  67</a>
<a href="#wglew.h-68">  68</a>
<a href="#wglew.h-69">  69</a>
<a href="#wglew.h-70">  70</a>
<a href="#wglew.h-71">  71</a>
<a href="#wglew.h-72">  72</a>
<a href="#wglew.h-73">  73</a>
<a href="#wglew.h-74">  74</a>
<a href="#wglew.h-75">  75</a>
<a href="#wglew.h-76">  76</a>
<a href="#wglew.h-77">  77</a>
<a href="#wglew.h-78">  78</a>
<a href="#wglew.h-79">  79</a>
<a href="#wglew.h-80">  80</a>
<a href="#wglew.h-81">  81</a>
<a href="#wglew.h-82">  82</a>
<a href="#wglew.h-83">  83</a>
<a href="#wglew.h-84">  84</a>
<a href="#wglew.h-85">  85</a>
<a href="#wglew.h-86">  86</a>
<a href="#wglew.h-87">  87</a>
<a href="#wglew.h-88">  88</a>
<a href="#wglew.h-89">  89</a>
<a href="#wglew.h-90">  90</a>
<a href="#wglew.h-91">  91</a>
<a href="#wglew.h-92">  92</a>
<a href="#wglew.h-93">  93</a>
<a href="#wglew.h-94">  94</a>
<a href="#wglew.h-95">  95</a>
<a href="#wglew.h-96">  96</a>
<a href="#wglew.h-97">  97</a>
<a href="#wglew.h-98">  98</a>
<a href="#wglew.h-99">  99</a>
<a href="#wglew.h-100"> 100</a>
<a href="#wglew.h-101"> 101</a>
<a href="#wglew.h-102"> 102</a>
<a href="#wglew.h-103"> 103</a>
<a href="#wglew.h-104"> 104</a>
<a href="#wglew.h-105"> 105</a>
<a href="#wglew.h-106"> 106</a>
<a href="#wglew.h-107"> 107</a>
<a href="#wglew.h-108"> 108</a>
<a href="#wglew.h-109"> 109</a>
<a href="#wglew.h-110"> 110</a>
<a href="#wglew.h-111"> 111</a>
<a href="#wglew.h-112"> 112</a>
<a href="#wglew.h-113"> 113</a>
<a href="#wglew.h-114"> 114</a>
<a href="#wglew.h-115"> 115</a>
<a href="#wglew.h-116"> 116</a>
<a href="#wglew.h-117"> 117</a>
<a href="#wglew.h-118"> 118</a>
<a href="#wglew.h-119"> 119</a>
<a href="#wglew.h-120"> 120</a>
<a href="#wglew.h-121"> 121</a>
<a href="#wglew.h-122"> 122</a>
<a href="#wglew.h-123"> 123</a>
<a href="#wglew.h-124"> 124</a>
<a href="#wglew.h-125"> 125</a>
<a href="#wglew.h-126"> 126</a>
<a href="#wglew.h-127"> 127</a>
<a href="#wglew.h-128"> 128</a>
<a href="#wglew.h-129"> 129</a>
<a href="#wglew.h-130"> 130</a>
<a href="#wglew.h-131"> 131</a>
<a href="#wglew.h-132"> 132</a>
<a href="#wglew.h-133"> 133</a>
<a href="#wglew.h-134"> 134</a>
<a href="#wglew.h-135"> 135</a>
<a href="#wglew.h-136"> 136</a>
<a href="#wglew.h-137"> 137</a>
<a href="#wglew.h-138"> 138</a>
<a href="#wglew.h-139"> 139</a>
<a href="#wglew.h-140"> 140</a>
<a href="#wglew.h-141"> 141</a>
<a href="#wglew.h-142"> 142</a>
<a href="#wglew.h-143"> 143</a>
<a href="#wglew.h-144"> 144</a>
<a href="#wglew.h-145"> 145</a>
<a href="#wglew.h-146"> 146</a>
<a href="#wglew.h-147"> 147</a>
<a href="#wglew.h-148"> 148</a>
<a href="#wglew.h-149"> 149</a>
<a href="#wglew.h-150"> 150</a>
<a href="#wglew.h-151"> 151</a>
<a href="#wglew.h-152"> 152</a>
<a href="#wglew.h-153"> 153</a>
<a href="#wglew.h-154"> 154</a>
<a href="#wglew.h-155"> 155</a>
<a href="#wglew.h-156"> 156</a>
<a href="#wglew.h-157"> 157</a>
<a href="#wglew.h-158"> 158</a>
<a href="#wglew.h-159"> 159</a>
<a href="#wglew.h-160"> 160</a>
<a href="#wglew.h-161"> 161</a>
<a href="#wglew.h-162"> 162</a>
<a href="#wglew.h-163"> 163</a>
<a href="#wglew.h-164"> 164</a>
<a href="#wglew.h-165"> 165</a>
<a href="#wglew.h-166"> 166</a>
<a href="#wglew.h-167"> 167</a>
<a href="#wglew.h-168"> 168</a>
<a href="#wglew.h-169"> 169</a>
<a href="#wglew.h-170"> 170</a>
<a href="#wglew.h-171"> 171</a>
<a href="#wglew.h-172"> 172</a>
<a href="#wglew.h-173"> 173</a>
<a href="#wglew.h-174"> 174</a>
<a href="#wglew.h-175"> 175</a>
<a href="#wglew.h-176"> 176</a>
<a href="#wglew.h-177"> 177</a>
<a href="#wglew.h-178"> 178</a>
<a href="#wglew.h-179"> 179</a>
<a href="#wglew.h-180"> 180</a>
<a href="#wglew.h-181"> 181</a>
<a href="#wglew.h-182"> 182</a>
<a href="#wglew.h-183"> 183</a>
<a href="#wglew.h-184"> 184</a>
<a href="#wglew.h-185"> 185</a>
<a href="#wglew.h-186"> 186</a>
<a href="#wglew.h-187"> 187</a>
<a href="#wglew.h-188"> 188</a>
<a href="#wglew.h-189"> 189</a>
<a href="#wglew.h-190"> 190</a>
<a href="#wglew.h-191"> 191</a>
<a href="#wglew.h-192"> 192</a>
<a href="#wglew.h-193"> 193</a>
<a href="#wglew.h-194"> 194</a>
<a href="#wglew.h-195"> 195</a>
<a href="#wglew.h-196"> 196</a>
<a href="#wglew.h-197"> 197</a>
<a href="#wglew.h-198"> 198</a>
<a href="#wglew.h-199"> 199</a>
<a href="#wglew.h-200"> 200</a>
<a href="#wglew.h-201"> 201</a>
<a href="#wglew.h-202"> 202</a>
<a href="#wglew.h-203"> 203</a>
<a href="#wglew.h-204"> 204</a>
<a href="#wglew.h-205"> 205</a>
<a href="#wglew.h-206"> 206</a>
<a href="#wglew.h-207"> 207</a>
<a href="#wglew.h-208"> 208</a>
<a href="#wglew.h-209"> 209</a>
<a href="#wglew.h-210"> 210</a>
<a href="#wglew.h-211"> 211</a>
<a href="#wglew.h-212"> 212</a>
<a href="#wglew.h-213"> 213</a>
<a href="#wglew.h-214"> 214</a>
<a href="#wglew.h-215"> 215</a>
<a href="#wglew.h-216"> 216</a>
<a href="#wglew.h-217"> 217</a>
<a href="#wglew.h-218"> 218</a>
<a href="#wglew.h-219"> 219</a>
<a href="#wglew.h-220"> 220</a>
<a href="#wglew.h-221"> 221</a>
<a href="#wglew.h-222"> 222</a>
<a href="#wglew.h-223"> 223</a>
<a href="#wglew.h-224"> 224</a>
<a href="#wglew.h-225"> 225</a>
<a href="#wglew.h-226"> 226</a>
<a href="#wglew.h-227"> 227</a>
<a href="#wglew.h-228"> 228</a>
<a href="#wglew.h-229"> 229</a>
<a href="#wglew.h-230"> 230</a>
<a href="#wglew.h-231"> 231</a>
<a href="#wglew.h-232"> 232</a>
<a href="#wglew.h-233"> 233</a>
<a href="#wglew.h-234"> 234</a>
<a href="#wglew.h-235"> 235</a>
<a href="#wglew.h-236"> 236</a>
<a href="#wglew.h-237"> 237</a>
<a href="#wglew.h-238"> 238</a>
<a href="#wglew.h-239"> 239</a>
<a href="#wglew.h-240"> 240</a>
<a href="#wglew.h-241"> 241</a>
<a href="#wglew.h-242"> 242</a>
<a href="#wglew.h-243"> 243</a>
<a href="#wglew.h-244"> 244</a>
<a href="#wglew.h-245"> 245</a>
<a href="#wglew.h-246"> 246</a>
<a href="#wglew.h-247"> 247</a>
<a href="#wglew.h-248"> 248</a>
<a href="#wglew.h-249"> 249</a>
<a href="#wglew.h-250"> 250</a>
<a href="#wglew.h-251"> 251</a>
<a href="#wglew.h-252"> 252</a>
<a href="#wglew.h-253"> 253</a>
<a href="#wglew.h-254"> 254</a>
<a href="#wglew.h-255"> 255</a>
<a href="#wglew.h-256"> 256</a>
<a href="#wglew.h-257"> 257</a>
<a href="#wglew.h-258"> 258</a>
<a href="#wglew.h-259"> 259</a>
<a href="#wglew.h-260"> 260</a>
<a href="#wglew.h-261"> 261</a>
<a href="#wglew.h-262"> 262</a>
<a href="#wglew.h-263"> 263</a>
<a href="#wglew.h-264"> 264</a>
<a href="#wglew.h-265"> 265</a>
<a href="#wglew.h-266"> 266</a>
<a href="#wglew.h-267"> 267</a>
<a href="#wglew.h-268"> 268</a>
<a href="#wglew.h-269"> 269</a>
<a href="#wglew.h-270"> 270</a>
<a href="#wglew.h-271"> 271</a>
<a href="#wglew.h-272"> 272</a>
<a href="#wglew.h-273"> 273</a>
<a href="#wglew.h-274"> 274</a>
<a href="#wglew.h-275"> 275</a>
<a href="#wglew.h-276"> 276</a>
<a href="#wglew.h-277"> 277</a>
<a href="#wglew.h-278"> 278</a>
<a href="#wglew.h-279"> 279</a>
<a href="#wglew.h-280"> 280</a>
<a href="#wglew.h-281"> 281</a>
<a href="#wglew.h-282"> 282</a>
<a href="#wglew.h-283"> 283</a>
<a href="#wglew.h-284"> 284</a>
<a href="#wglew.h-285"> 285</a>
<a href="#wglew.h-286"> 286</a>
<a href="#wglew.h-287"> 287</a>
<a href="#wglew.h-288"> 288</a>
<a href="#wglew.h-289"> 289</a>
<a href="#wglew.h-290"> 290</a>
<a href="#wglew.h-291"> 291</a>
<a href="#wglew.h-292"> 292</a>
<a href="#wglew.h-293"> 293</a>
<a href="#wglew.h-294"> 294</a>
<a href="#wglew.h-295"> 295</a>
<a href="#wglew.h-296"> 296</a>
<a href="#wglew.h-297"> 297</a>
<a href="#wglew.h-298"> 298</a>
<a href="#wglew.h-299"> 299</a>
<a href="#wglew.h-300"> 300</a>
<a href="#wglew.h-301"> 301</a>
<a href="#wglew.h-302"> 302</a>
<a href="#wglew.h-303"> 303</a>
<a href="#wglew.h-304"> 304</a>
<a href="#wglew.h-305"> 305</a>
<a href="#wglew.h-306"> 306</a>
<a href="#wglew.h-307"> 307</a>
<a href="#wglew.h-308"> 308</a>
<a href="#wglew.h-309"> 309</a>
<a href="#wglew.h-310"> 310</a>
<a href="#wglew.h-311"> 311</a>
<a href="#wglew.h-312"> 312</a>
<a href="#wglew.h-313"> 313</a>
<a href="#wglew.h-314"> 314</a>
<a href="#wglew.h-315"> 315</a>
<a href="#wglew.h-316"> 316</a>
<a href="#wglew.h-317"> 317</a>
<a href="#wglew.h-318"> 318</a>
<a href="#wglew.h-319"> 319</a>
<a href="#wglew.h-320"> 320</a>
<a href="#wglew.h-321"> 321</a>
<a href="#wglew.h-322"> 322</a>
<a href="#wglew.h-323"> 323</a>
<a href="#wglew.h-324"> 324</a>
<a href="#wglew.h-325"> 325</a>
<a href="#wglew.h-326"> 326</a>
<a href="#wglew.h-327"> 327</a>
<a href="#wglew.h-328"> 328</a>
<a href="#wglew.h-329"> 329</a>
<a href="#wglew.h-330"> 330</a>
<a href="#wglew.h-331"> 331</a>
<a href="#wglew.h-332"> 332</a>
<a href="#wglew.h-333"> 333</a>
<a href="#wglew.h-334"> 334</a>
<a href="#wglew.h-335"> 335</a>
<a href="#wglew.h-336"> 336</a>
<a href="#wglew.h-337"> 337</a>
<a href="#wglew.h-338"> 338</a>
<a href="#wglew.h-339"> 339</a>
<a href="#wglew.h-340"> 340</a>
<a href="#wglew.h-341"> 341</a>
<a href="#wglew.h-342"> 342</a>
<a href="#wglew.h-343"> 343</a>
<a href="#wglew.h-344"> 344</a>
<a href="#wglew.h-345"> 345</a>
<a href="#wglew.h-346"> 346</a>
<a href="#wglew.h-347"> 347</a>
<a href="#wglew.h-348"> 348</a>
<a href="#wglew.h-349"> 349</a>
<a href="#wglew.h-350"> 350</a>
<a href="#wglew.h-351"> 351</a>
<a href="#wglew.h-352"> 352</a>
<a href="#wglew.h-353"> 353</a>
<a href="#wglew.h-354"> 354</a>
<a href="#wglew.h-355"> 355</a>
<a href="#wglew.h-356"> 356</a>
<a href="#wglew.h-357"> 357</a>
<a href="#wglew.h-358"> 358</a>
<a href="#wglew.h-359"> 359</a>
<a href="#wglew.h-360"> 360</a>
<a href="#wglew.h-361"> 361</a>
<a href="#wglew.h-362"> 362</a>
<a href="#wglew.h-363"> 363</a>
<a href="#wglew.h-364"> 364</a>
<a href="#wglew.h-365"> 365</a>
<a href="#wglew.h-366"> 366</a>
<a href="#wglew.h-367"> 367</a>
<a href="#wglew.h-368"> 368</a>
<a href="#wglew.h-369"> 369</a>
<a href="#wglew.h-370"> 370</a>
<a href="#wglew.h-371"> 371</a>
<a href="#wglew.h-372"> 372</a>
<a href="#wglew.h-373"> 373</a>
<a href="#wglew.h-374"> 374</a>
<a href="#wglew.h-375"> 375</a>
<a href="#wglew.h-376"> 376</a>
<a href="#wglew.h-377"> 377</a>
<a href="#wglew.h-378"> 378</a>
<a href="#wglew.h-379"> 379</a>
<a href="#wglew.h-380"> 380</a>
<a href="#wglew.h-381"> 381</a>
<a href="#wglew.h-382"> 382</a>
<a href="#wglew.h-383"> 383</a>
<a href="#wglew.h-384"> 384</a>
<a href="#wglew.h-385"> 385</a>
<a href="#wglew.h-386"> 386</a>
<a href="#wglew.h-387"> 387</a>
<a href="#wglew.h-388"> 388</a>
<a href="#wglew.h-389"> 389</a>
<a href="#wglew.h-390"> 390</a>
<a href="#wglew.h-391"> 391</a>
<a href="#wglew.h-392"> 392</a>
<a href="#wglew.h-393"> 393</a>
<a href="#wglew.h-394"> 394</a>
<a href="#wglew.h-395"> 395</a>
<a href="#wglew.h-396"> 396</a>
<a href="#wglew.h-397"> 397</a>
<a href="#wglew.h-398"> 398</a>
<a href="#wglew.h-399"> 399</a>
<a href="#wglew.h-400"> 400</a>
<a href="#wglew.h-401"> 401</a>
<a href="#wglew.h-402"> 402</a>
<a href="#wglew.h-403"> 403</a>
<a href="#wglew.h-404"> 404</a>
<a href="#wglew.h-405"> 405</a>
<a href="#wglew.h-406"> 406</a>
<a href="#wglew.h-407"> 407</a>
<a href="#wglew.h-408"> 408</a>
<a href="#wglew.h-409"> 409</a>
<a href="#wglew.h-410"> 410</a>
<a href="#wglew.h-411"> 411</a>
<a href="#wglew.h-412"> 412</a>
<a href="#wglew.h-413"> 413</a>
<a href="#wglew.h-414"> 414</a>
<a href="#wglew.h-415"> 415</a>
<a href="#wglew.h-416"> 416</a>
<a href="#wglew.h-417"> 417</a>
<a href="#wglew.h-418"> 418</a>
<a href="#wglew.h-419"> 419</a>
<a href="#wglew.h-420"> 420</a>
<a href="#wglew.h-421"> 421</a>
<a href="#wglew.h-422"> 422</a>
<a href="#wglew.h-423"> 423</a>
<a href="#wglew.h-424"> 424</a>
<a href="#wglew.h-425"> 425</a>
<a href="#wglew.h-426"> 426</a>
<a href="#wglew.h-427"> 427</a>
<a href="#wglew.h-428"> 428</a>
<a href="#wglew.h-429"> 429</a>
<a href="#wglew.h-430"> 430</a>
<a href="#wglew.h-431"> 431</a>
<a href="#wglew.h-432"> 432</a>
<a href="#wglew.h-433"> 433</a>
<a href="#wglew.h-434"> 434</a>
<a href="#wglew.h-435"> 435</a>
<a href="#wglew.h-436"> 436</a>
<a href="#wglew.h-437"> 437</a>
<a href="#wglew.h-438"> 438</a>
<a href="#wglew.h-439"> 439</a>
<a href="#wglew.h-440"> 440</a>
<a href="#wglew.h-441"> 441</a>
<a href="#wglew.h-442"> 442</a>
<a href="#wglew.h-443"> 443</a>
<a href="#wglew.h-444"> 444</a>
<a href="#wglew.h-445"> 445</a>
<a href="#wglew.h-446"> 446</a>
<a href="#wglew.h-447"> 447</a>
<a href="#wglew.h-448"> 448</a>
<a href="#wglew.h-449"> 449</a>
<a href="#wglew.h-450"> 450</a>
<a href="#wglew.h-451"> 451</a>
<a href="#wglew.h-452"> 452</a>
<a href="#wglew.h-453"> 453</a>
<a href="#wglew.h-454"> 454</a>
<a href="#wglew.h-455"> 455</a>
<a href="#wglew.h-456"> 456</a>
<a href="#wglew.h-457"> 457</a>
<a href="#wglew.h-458"> 458</a>
<a href="#wglew.h-459"> 459</a>
<a href="#wglew.h-460"> 460</a>
<a href="#wglew.h-461"> 461</a>
<a href="#wglew.h-462"> 462</a>
<a href="#wglew.h-463"> 463</a>
<a href="#wglew.h-464"> 464</a>
<a href="#wglew.h-465"> 465</a>
<a href="#wglew.h-466"> 466</a>
<a href="#wglew.h-467"> 467</a>
<a href="#wglew.h-468"> 468</a>
<a href="#wglew.h-469"> 469</a>
<a href="#wglew.h-470"> 470</a>
<a href="#wglew.h-471"> 471</a>
<a href="#wglew.h-472"> 472</a>
<a href="#wglew.h-473"> 473</a>
<a href="#wglew.h-474"> 474</a>
<a href="#wglew.h-475"> 475</a>
<a href="#wglew.h-476"> 476</a>
<a href="#wglew.h-477"> 477</a>
<a href="#wglew.h-478"> 478</a>
<a href="#wglew.h-479"> 479</a>
<a href="#wglew.h-480"> 480</a>
<a href="#wglew.h-481"> 481</a>
<a href="#wglew.h-482"> 482</a>
<a href="#wglew.h-483"> 483</a>
<a href="#wglew.h-484"> 484</a>
<a href="#wglew.h-485"> 485</a>
<a href="#wglew.h-486"> 486</a>
<a href="#wglew.h-487"> 487</a>
<a href="#wglew.h-488"> 488</a>
<a href="#wglew.h-489"> 489</a>
<a href="#wglew.h-490"> 490</a>
<a href="#wglew.h-491"> 491</a>
<a href="#wglew.h-492"> 492</a>
<a href="#wglew.h-493"> 493</a>
<a href="#wglew.h-494"> 494</a>
<a href="#wglew.h-495"> 495</a>
<a href="#wglew.h-496"> 496</a>
<a href="#wglew.h-497"> 497</a>
<a href="#wglew.h-498"> 498</a>
<a href="#wglew.h-499"> 499</a>
<a href="#wglew.h-500"> 500</a>
<a href="#wglew.h-501"> 501</a>
<a href="#wglew.h-502"> 502</a>
<a href="#wglew.h-503"> 503</a>
<a href="#wglew.h-504"> 504</a>
<a href="#wglew.h-505"> 505</a>
<a href="#wglew.h-506"> 506</a>
<a href="#wglew.h-507"> 507</a>
<a href="#wglew.h-508"> 508</a>
<a href="#wglew.h-509"> 509</a>
<a href="#wglew.h-510"> 510</a>
<a href="#wglew.h-511"> 511</a>
<a href="#wglew.h-512"> 512</a>
<a href="#wglew.h-513"> 513</a>
<a href="#wglew.h-514"> 514</a>
<a href="#wglew.h-515"> 515</a>
<a href="#wglew.h-516"> 516</a>
<a href="#wglew.h-517"> 517</a>
<a href="#wglew.h-518"> 518</a>
<a href="#wglew.h-519"> 519</a>
<a href="#wglew.h-520"> 520</a>
<a href="#wglew.h-521"> 521</a>
<a href="#wglew.h-522"> 522</a>
<a href="#wglew.h-523"> 523</a>
<a href="#wglew.h-524"> 524</a>
<a href="#wglew.h-525"> 525</a>
<a href="#wglew.h-526"> 526</a>
<a href="#wglew.h-527"> 527</a>
<a href="#wglew.h-528"> 528</a>
<a href="#wglew.h-529"> 529</a>
<a href="#wglew.h-530"> 530</a>
<a href="#wglew.h-531"> 531</a>
<a href="#wglew.h-532"> 532</a>
<a href="#wglew.h-533"> 533</a>
<a href="#wglew.h-534"> 534</a>
<a href="#wglew.h-535"> 535</a>
<a href="#wglew.h-536"> 536</a>
<a href="#wglew.h-537"> 537</a>
<a href="#wglew.h-538"> 538</a>
<a href="#wglew.h-539"> 539</a>
<a href="#wglew.h-540"> 540</a>
<a href="#wglew.h-541"> 541</a>
<a href="#wglew.h-542"> 542</a>
<a href="#wglew.h-543"> 543</a>
<a href="#wglew.h-544"> 544</a>
<a href="#wglew.h-545"> 545</a>
<a href="#wglew.h-546"> 546</a>
<a href="#wglew.h-547"> 547</a>
<a href="#wglew.h-548"> 548</a>
<a href="#wglew.h-549"> 549</a>
<a href="#wglew.h-550"> 550</a>
<a href="#wglew.h-551"> 551</a>
<a href="#wglew.h-552"> 552</a>
<a href="#wglew.h-553"> 553</a>
<a href="#wglew.h-554"> 554</a>
<a href="#wglew.h-555"> 555</a>
<a href="#wglew.h-556"> 556</a>
<a href="#wglew.h-557"> 557</a>
<a href="#wglew.h-558"> 558</a>
<a href="#wglew.h-559"> 559</a>
<a href="#wglew.h-560"> 560</a>
<a href="#wglew.h-561"> 561</a>
<a href="#wglew.h-562"> 562</a>
<a href="#wglew.h-563"> 563</a>
<a href="#wglew.h-564"> 564</a>
<a href="#wglew.h-565"> 565</a>
<a href="#wglew.h-566"> 566</a>
<a href="#wglew.h-567"> 567</a>
<a href="#wglew.h-568"> 568</a>
<a href="#wglew.h-569"> 569</a>
<a href="#wglew.h-570"> 570</a>
<a href="#wglew.h-571"> 571</a>
<a href="#wglew.h-572"> 572</a>
<a href="#wglew.h-573"> 573</a>
<a href="#wglew.h-574"> 574</a>
<a href="#wglew.h-575"> 575</a>
<a href="#wglew.h-576"> 576</a>
<a href="#wglew.h-577"> 577</a>
<a href="#wglew.h-578"> 578</a>
<a href="#wglew.h-579"> 579</a>
<a href="#wglew.h-580"> 580</a>
<a href="#wglew.h-581"> 581</a>
<a href="#wglew.h-582"> 582</a>
<a href="#wglew.h-583"> 583</a>
<a href="#wglew.h-584"> 584</a>
<a href="#wglew.h-585"> 585</a>
<a href="#wglew.h-586"> 586</a>
<a href="#wglew.h-587"> 587</a>
<a href="#wglew.h-588"> 588</a>
<a href="#wglew.h-589"> 589</a>
<a href="#wglew.h-590"> 590</a>
<a href="#wglew.h-591"> 591</a>
<a href="#wglew.h-592"> 592</a>
<a href="#wglew.h-593"> 593</a>
<a href="#wglew.h-594"> 594</a>
<a href="#wglew.h-595"> 595</a>
<a href="#wglew.h-596"> 596</a>
<a href="#wglew.h-597"> 597</a>
<a href="#wglew.h-598"> 598</a>
<a href="#wglew.h-599"> 599</a>
<a href="#wglew.h-600"> 600</a>
<a href="#wglew.h-601"> 601</a>
<a href="#wglew.h-602"> 602</a>
<a href="#wglew.h-603"> 603</a>
<a href="#wglew.h-604"> 604</a>
<a href="#wglew.h-605"> 605</a>
<a href="#wglew.h-606"> 606</a>
<a href="#wglew.h-607"> 607</a>
<a href="#wglew.h-608"> 608</a>
<a href="#wglew.h-609"> 609</a>
<a href="#wglew.h-610"> 610</a>
<a href="#wglew.h-611"> 611</a>
<a href="#wglew.h-612"> 612</a>
<a href="#wglew.h-613"> 613</a>
<a href="#wglew.h-614"> 614</a>
<a href="#wglew.h-615"> 615</a>
<a href="#wglew.h-616"> 616</a>
<a href="#wglew.h-617"> 617</a>
<a href="#wglew.h-618"> 618</a>
<a href="#wglew.h-619"> 619</a>
<a href="#wglew.h-620"> 620</a>
<a href="#wglew.h-621"> 621</a>
<a href="#wglew.h-622"> 622</a>
<a href="#wglew.h-623"> 623</a>
<a href="#wglew.h-624"> 624</a>
<a href="#wglew.h-625"> 625</a>
<a href="#wglew.h-626"> 626</a>
<a href="#wglew.h-627"> 627</a>
<a href="#wglew.h-628"> 628</a>
<a href="#wglew.h-629"> 629</a>
<a href="#wglew.h-630"> 630</a>
<a href="#wglew.h-631"> 631</a>
<a href="#wglew.h-632"> 632</a>
<a href="#wglew.h-633"> 633</a>
<a href="#wglew.h-634"> 634</a>
<a href="#wglew.h-635"> 635</a>
<a href="#wglew.h-636"> 636</a>
<a href="#wglew.h-637"> 637</a>
<a href="#wglew.h-638"> 638</a>
<a href="#wglew.h-639"> 639</a>
<a href="#wglew.h-640"> 640</a>
<a href="#wglew.h-641"> 641</a>
<a href="#wglew.h-642"> 642</a>
<a href="#wglew.h-643"> 643</a>
<a href="#wglew.h-644"> 644</a>
<a href="#wglew.h-645"> 645</a>
<a href="#wglew.h-646"> 646</a>
<a href="#wglew.h-647"> 647</a>
<a href="#wglew.h-648"> 648</a>
<a href="#wglew.h-649"> 649</a>
<a href="#wglew.h-650"> 650</a>
<a href="#wglew.h-651"> 651</a>
<a href="#wglew.h-652"> 652</a>
<a href="#wglew.h-653"> 653</a>
<a href="#wglew.h-654"> 654</a>
<a href="#wglew.h-655"> 655</a>
<a href="#wglew.h-656"> 656</a>
<a href="#wglew.h-657"> 657</a>
<a href="#wglew.h-658"> 658</a>
<a href="#wglew.h-659"> 659</a>
<a href="#wglew.h-660"> 660</a>
<a href="#wglew.h-661"> 661</a>
<a href="#wglew.h-662"> 662</a>
<a href="#wglew.h-663"> 663</a>
<a href="#wglew.h-664"> 664</a>
<a href="#wglew.h-665"> 665</a>
<a href="#wglew.h-666"> 666</a>
<a href="#wglew.h-667"> 667</a>
<a href="#wglew.h-668"> 668</a>
<a href="#wglew.h-669"> 669</a>
<a href="#wglew.h-670"> 670</a>
<a href="#wglew.h-671"> 671</a>
<a href="#wglew.h-672"> 672</a>
<a href="#wglew.h-673"> 673</a>
<a href="#wglew.h-674"> 674</a>
<a href="#wglew.h-675"> 675</a>
<a href="#wglew.h-676"> 676</a>
<a href="#wglew.h-677"> 677</a>
<a href="#wglew.h-678"> 678</a>
<a href="#wglew.h-679"> 679</a>
<a href="#wglew.h-680"> 680</a>
<a href="#wglew.h-681"> 681</a>
<a href="#wglew.h-682"> 682</a>
<a href="#wglew.h-683"> 683</a>
<a href="#wglew.h-684"> 684</a>
<a href="#wglew.h-685"> 685</a>
<a href="#wglew.h-686"> 686</a>
<a href="#wglew.h-687"> 687</a>
<a href="#wglew.h-688"> 688</a>
<a href="#wglew.h-689"> 689</a>
<a href="#wglew.h-690"> 690</a>
<a href="#wglew.h-691"> 691</a>
<a href="#wglew.h-692"> 692</a>
<a href="#wglew.h-693"> 693</a>
<a href="#wglew.h-694"> 694</a>
<a href="#wglew.h-695"> 695</a>
<a href="#wglew.h-696"> 696</a>
<a href="#wglew.h-697"> 697</a>
<a href="#wglew.h-698"> 698</a>
<a href="#wglew.h-699"> 699</a>
<a href="#wglew.h-700"> 700</a>
<a href="#wglew.h-701"> 701</a>
<a href="#wglew.h-702"> 702</a>
<a href="#wglew.h-703"> 703</a>
<a href="#wglew.h-704"> 704</a>
<a href="#wglew.h-705"> 705</a>
<a href="#wglew.h-706"> 706</a>
<a href="#wglew.h-707"> 707</a>
<a href="#wglew.h-708"> 708</a>
<a href="#wglew.h-709"> 709</a>
<a href="#wglew.h-710"> 710</a>
<a href="#wglew.h-711"> 711</a>
<a href="#wglew.h-712"> 712</a>
<a href="#wglew.h-713"> 713</a>
<a href="#wglew.h-714"> 714</a>
<a href="#wglew.h-715"> 715</a>
<a href="#wglew.h-716"> 716</a>
<a href="#wglew.h-717"> 717</a>
<a href="#wglew.h-718"> 718</a>
<a href="#wglew.h-719"> 719</a>
<a href="#wglew.h-720"> 720</a>
<a href="#wglew.h-721"> 721</a>
<a href="#wglew.h-722"> 722</a>
<a href="#wglew.h-723"> 723</a>
<a href="#wglew.h-724"> 724</a>
<a href="#wglew.h-725"> 725</a>
<a href="#wglew.h-726"> 726</a>
<a href="#wglew.h-727"> 727</a>
<a href="#wglew.h-728"> 728</a>
<a href="#wglew.h-729"> 729</a>
<a href="#wglew.h-730"> 730</a>
<a href="#wglew.h-731"> 731</a>
<a href="#wglew.h-732"> 732</a>
<a href="#wglew.h-733"> 733</a>
<a href="#wglew.h-734"> 734</a>
<a href="#wglew.h-735"> 735</a>
<a href="#wglew.h-736"> 736</a>
<a href="#wglew.h-737"> 737</a>
<a href="#wglew.h-738"> 738</a>
<a href="#wglew.h-739"> 739</a>
<a href="#wglew.h-740"> 740</a>
<a href="#wglew.h-741"> 741</a>
<a href="#wglew.h-742"> 742</a>
<a href="#wglew.h-743"> 743</a>
<a href="#wglew.h-744"> 744</a>
<a href="#wglew.h-745"> 745</a>
<a href="#wglew.h-746"> 746</a>
<a href="#wglew.h-747"> 747</a>
<a href="#wglew.h-748"> 748</a>
<a href="#wglew.h-749"> 749</a>
<a href="#wglew.h-750"> 750</a>
<a href="#wglew.h-751"> 751</a>
<a href="#wglew.h-752"> 752</a>
<a href="#wglew.h-753"> 753</a>
<a href="#wglew.h-754"> 754</a>
<a href="#wglew.h-755"> 755</a>
<a href="#wglew.h-756"> 756</a>
<a href="#wglew.h-757"> 757</a>
<a href="#wglew.h-758"> 758</a>
<a href="#wglew.h-759"> 759</a>
<a href="#wglew.h-760"> 760</a>
<a href="#wglew.h-761"> 761</a>
<a href="#wglew.h-762"> 762</a>
<a href="#wglew.h-763"> 763</a>
<a href="#wglew.h-764"> 764</a>
<a href="#wglew.h-765"> 765</a>
<a href="#wglew.h-766"> 766</a>
<a href="#wglew.h-767"> 767</a>
<a href="#wglew.h-768"> 768</a>
<a href="#wglew.h-769"> 769</a>
<a href="#wglew.h-770"> 770</a>
<a href="#wglew.h-771"> 771</a>
<a href="#wglew.h-772"> 772</a>
<a href="#wglew.h-773"> 773</a>
<a href="#wglew.h-774"> 774</a>
<a href="#wglew.h-775"> 775</a>
<a href="#wglew.h-776"> 776</a>
<a href="#wglew.h-777"> 777</a>
<a href="#wglew.h-778"> 778</a>
<a href="#wglew.h-779"> 779</a>
<a href="#wglew.h-780"> 780</a>
<a href="#wglew.h-781"> 781</a>
<a href="#wglew.h-782"> 782</a>
<a href="#wglew.h-783"> 783</a>
<a href="#wglew.h-784"> 784</a>
<a href="#wglew.h-785"> 785</a>
<a href="#wglew.h-786"> 786</a>
<a href="#wglew.h-787"> 787</a>
<a href="#wglew.h-788"> 788</a>
<a href="#wglew.h-789"> 789</a>
<a href="#wglew.h-790"> 790</a>
<a href="#wglew.h-791"> 791</a>
<a href="#wglew.h-792"> 792</a>
<a href="#wglew.h-793"> 793</a>
<a href="#wglew.h-794"> 794</a>
<a href="#wglew.h-795"> 795</a>
<a href="#wglew.h-796"> 796</a>
<a href="#wglew.h-797"> 797</a>
<a href="#wglew.h-798"> 798</a>
<a href="#wglew.h-799"> 799</a>
<a href="#wglew.h-800"> 800</a>
<a href="#wglew.h-801"> 801</a>
<a href="#wglew.h-802"> 802</a>
<a href="#wglew.h-803"> 803</a>
<a href="#wglew.h-804"> 804</a>
<a href="#wglew.h-805"> 805</a>
<a href="#wglew.h-806"> 806</a>
<a href="#wglew.h-807"> 807</a>
<a href="#wglew.h-808"> 808</a>
<a href="#wglew.h-809"> 809</a>
<a href="#wglew.h-810"> 810</a>
<a href="#wglew.h-811"> 811</a>
<a href="#wglew.h-812"> 812</a>
<a href="#wglew.h-813"> 813</a>
<a href="#wglew.h-814"> 814</a>
<a href="#wglew.h-815"> 815</a>
<a href="#wglew.h-816"> 816</a>
<a href="#wglew.h-817"> 817</a>
<a href="#wglew.h-818"> 818</a>
<a href="#wglew.h-819"> 819</a>
<a href="#wglew.h-820"> 820</a>
<a href="#wglew.h-821"> 821</a>
<a href="#wglew.h-822"> 822</a>
<a href="#wglew.h-823"> 823</a>
<a href="#wglew.h-824"> 824</a>
<a href="#wglew.h-825"> 825</a>
<a href="#wglew.h-826"> 826</a>
<a href="#wglew.h-827"> 827</a>
<a href="#wglew.h-828"> 828</a>
<a href="#wglew.h-829"> 829</a>
<a href="#wglew.h-830"> 830</a>
<a href="#wglew.h-831"> 831</a>
<a href="#wglew.h-832"> 832</a>
<a href="#wglew.h-833"> 833</a>
<a href="#wglew.h-834"> 834</a>
<a href="#wglew.h-835"> 835</a>
<a href="#wglew.h-836"> 836</a>
<a href="#wglew.h-837"> 837</a>
<a href="#wglew.h-838"> 838</a>
<a href="#wglew.h-839"> 839</a>
<a href="#wglew.h-840"> 840</a>
<a href="#wglew.h-841"> 841</a>
<a href="#wglew.h-842"> 842</a>
<a href="#wglew.h-843"> 843</a>
<a href="#wglew.h-844"> 844</a>
<a href="#wglew.h-845"> 845</a>
<a href="#wglew.h-846"> 846</a>
<a href="#wglew.h-847"> 847</a>
<a href="#wglew.h-848"> 848</a>
<a href="#wglew.h-849"> 849</a>
<a href="#wglew.h-850"> 850</a>
<a href="#wglew.h-851"> 851</a>
<a href="#wglew.h-852"> 852</a>
<a href="#wglew.h-853"> 853</a>
<a href="#wglew.h-854"> 854</a>
<a href="#wglew.h-855"> 855</a>
<a href="#wglew.h-856"> 856</a>
<a href="#wglew.h-857"> 857</a>
<a href="#wglew.h-858"> 858</a>
<a href="#wglew.h-859"> 859</a>
<a href="#wglew.h-860"> 860</a>
<a href="#wglew.h-861"> 861</a>
<a href="#wglew.h-862"> 862</a>
<a href="#wglew.h-863"> 863</a>
<a href="#wglew.h-864"> 864</a>
<a href="#wglew.h-865"> 865</a>
<a href="#wglew.h-866"> 866</a>
<a href="#wglew.h-867"> 867</a>
<a href="#wglew.h-868"> 868</a>
<a href="#wglew.h-869"> 869</a>
<a href="#wglew.h-870"> 870</a>
<a href="#wglew.h-871"> 871</a>
<a href="#wglew.h-872"> 872</a>
<a href="#wglew.h-873"> 873</a>
<a href="#wglew.h-874"> 874</a>
<a href="#wglew.h-875"> 875</a>
<a href="#wglew.h-876"> 876</a>
<a href="#wglew.h-877"> 877</a>
<a href="#wglew.h-878"> 878</a>
<a href="#wglew.h-879"> 879</a>
<a href="#wglew.h-880"> 880</a>
<a href="#wglew.h-881"> 881</a>
<a href="#wglew.h-882"> 882</a>
<a href="#wglew.h-883"> 883</a>
<a href="#wglew.h-884"> 884</a>
<a href="#wglew.h-885"> 885</a>
<a href="#wglew.h-886"> 886</a>
<a href="#wglew.h-887"> 887</a>
<a href="#wglew.h-888"> 888</a>
<a href="#wglew.h-889"> 889</a>
<a href="#wglew.h-890"> 890</a>
<a href="#wglew.h-891"> 891</a>
<a href="#wglew.h-892"> 892</a>
<a href="#wglew.h-893"> 893</a>
<a href="#wglew.h-894"> 894</a>
<a href="#wglew.h-895"> 895</a>
<a href="#wglew.h-896"> 896</a>
<a href="#wglew.h-897"> 897</a>
<a href="#wglew.h-898"> 898</a>
<a href="#wglew.h-899"> 899</a>
<a href="#wglew.h-900"> 900</a>
<a href="#wglew.h-901"> 901</a>
<a href="#wglew.h-902"> 902</a>
<a href="#wglew.h-903"> 903</a>
<a href="#wglew.h-904"> 904</a>
<a href="#wglew.h-905"> 905</a>
<a href="#wglew.h-906"> 906</a>
<a href="#wglew.h-907"> 907</a>
<a href="#wglew.h-908"> 908</a>
<a href="#wglew.h-909"> 909</a>
<a href="#wglew.h-910"> 910</a>
<a href="#wglew.h-911"> 911</a>
<a href="#wglew.h-912"> 912</a>
<a href="#wglew.h-913"> 913</a>
<a href="#wglew.h-914"> 914</a>
<a href="#wglew.h-915"> 915</a>
<a href="#wglew.h-916"> 916</a>
<a href="#wglew.h-917"> 917</a>
<a href="#wglew.h-918"> 918</a>
<a href="#wglew.h-919"> 919</a>
<a href="#wglew.h-920"> 920</a>
<a href="#wglew.h-921"> 921</a>
<a href="#wglew.h-922"> 922</a>
<a href="#wglew.h-923"> 923</a>
<a href="#wglew.h-924"> 924</a>
<a href="#wglew.h-925"> 925</a>
<a href="#wglew.h-926"> 926</a>
<a href="#wglew.h-927"> 927</a>
<a href="#wglew.h-928"> 928</a>
<a href="#wglew.h-929"> 929</a>
<a href="#wglew.h-930"> 930</a>
<a href="#wglew.h-931"> 931</a>
<a href="#wglew.h-932"> 932</a>
<a href="#wglew.h-933"> 933</a>
<a href="#wglew.h-934"> 934</a>
<a href="#wglew.h-935"> 935</a>
<a href="#wglew.h-936"> 936</a>
<a href="#wglew.h-937"> 937</a>
<a href="#wglew.h-938"> 938</a>
<a href="#wglew.h-939"> 939</a>
<a href="#wglew.h-940"> 940</a>
<a href="#wglew.h-941"> 941</a>
<a href="#wglew.h-942"> 942</a>
<a href="#wglew.h-943"> 943</a>
<a href="#wglew.h-944"> 944</a>
<a href="#wglew.h-945"> 945</a>
<a href="#wglew.h-946"> 946</a>
<a href="#wglew.h-947"> 947</a>
<a href="#wglew.h-948"> 948</a>
<a href="#wglew.h-949"> 949</a>
<a href="#wglew.h-950"> 950</a>
<a href="#wglew.h-951"> 951</a>
<a href="#wglew.h-952"> 952</a>
<a href="#wglew.h-953"> 953</a>
<a href="#wglew.h-954"> 954</a>
<a href="#wglew.h-955"> 955</a>
<a href="#wglew.h-956"> 956</a>
<a href="#wglew.h-957"> 957</a>
<a href="#wglew.h-958"> 958</a>
<a href="#wglew.h-959"> 959</a>
<a href="#wglew.h-960"> 960</a>
<a href="#wglew.h-961"> 961</a>
<a href="#wglew.h-962"> 962</a>
<a href="#wglew.h-963"> 963</a>
<a href="#wglew.h-964"> 964</a>
<a href="#wglew.h-965"> 965</a>
<a href="#wglew.h-966"> 966</a>
<a href="#wglew.h-967"> 967</a>
<a href="#wglew.h-968"> 968</a>
<a href="#wglew.h-969"> 969</a>
<a href="#wglew.h-970"> 970</a>
<a href="#wglew.h-971"> 971</a>
<a href="#wglew.h-972"> 972</a>
<a href="#wglew.h-973"> 973</a>
<a href="#wglew.h-974"> 974</a>
<a href="#wglew.h-975"> 975</a>
<a href="#wglew.h-976"> 976</a>
<a href="#wglew.h-977"> 977</a>
<a href="#wglew.h-978"> 978</a>
<a href="#wglew.h-979"> 979</a>
<a href="#wglew.h-980"> 980</a>
<a href="#wglew.h-981"> 981</a>
<a href="#wglew.h-982"> 982</a>
<a href="#wglew.h-983"> 983</a>
<a href="#wglew.h-984"> 984</a>
<a href="#wglew.h-985"> 985</a>
<a href="#wglew.h-986"> 986</a>
<a href="#wglew.h-987"> 987</a>
<a href="#wglew.h-988"> 988</a>
<a href="#wglew.h-989"> 989</a>
<a href="#wglew.h-990"> 990</a>
<a href="#wglew.h-991"> 991</a>
<a href="#wglew.h-992"> 992</a>
<a href="#wglew.h-993"> 993</a>
<a href="#wglew.h-994"> 994</a>
<a href="#wglew.h-995"> 995</a>
<a href="#wglew.h-996"> 996</a>
<a href="#wglew.h-997"> 997</a>
<a href="#wglew.h-998"> 998</a>
<a href="#wglew.h-999"> 999</a>
<a href="#wglew.h-1000">1000</a>
<a href="#wglew.h-1001">1001</a>
<a href="#wglew.h-1002">1002</a>
<a href="#wglew.h-1003">1003</a>
<a href="#wglew.h-1004">1004</a>
<a href="#wglew.h-1005">1005</a>
<a href="#wglew.h-1006">1006</a>
<a href="#wglew.h-1007">1007</a>
<a href="#wglew.h-1008">1008</a>
<a href="#wglew.h-1009">1009</a>
<a href="#wglew.h-1010">1010</a>
<a href="#wglew.h-1011">1011</a>
<a href="#wglew.h-1012">1012</a>
<a href="#wglew.h-1013">1013</a>
<a href="#wglew.h-1014">1014</a>
<a href="#wglew.h-1015">1015</a>
<a href="#wglew.h-1016">1016</a>
<a href="#wglew.h-1017">1017</a>
<a href="#wglew.h-1018">1018</a>
<a href="#wglew.h-1019">1019</a>
<a href="#wglew.h-1020">1020</a>
<a href="#wglew.h-1021">1021</a>
<a href="#wglew.h-1022">1022</a>
<a href="#wglew.h-1023">1023</a>
<a href="#wglew.h-1024">1024</a>
<a href="#wglew.h-1025">1025</a>
<a href="#wglew.h-1026">1026</a>
<a href="#wglew.h-1027">1027</a>
<a href="#wglew.h-1028">1028</a>
<a href="#wglew.h-1029">1029</a>
<a href="#wglew.h-1030">1030</a>
<a href="#wglew.h-1031">1031</a>
<a href="#wglew.h-1032">1032</a>
<a href="#wglew.h-1033">1033</a>
<a href="#wglew.h-1034">1034</a>
<a href="#wglew.h-1035">1035</a>
<a href="#wglew.h-1036">1036</a>
<a href="#wglew.h-1037">1037</a>
<a href="#wglew.h-1038">1038</a>
<a href="#wglew.h-1039">1039</a>
<a href="#wglew.h-1040">1040</a>
<a href="#wglew.h-1041">1041</a>
<a href="#wglew.h-1042">1042</a>
<a href="#wglew.h-1043">1043</a>
<a href="#wglew.h-1044">1044</a>
<a href="#wglew.h-1045">1045</a>
<a href="#wglew.h-1046">1046</a>
<a href="#wglew.h-1047">1047</a>
<a href="#wglew.h-1048">1048</a>
<a href="#wglew.h-1049">1049</a>
<a href="#wglew.h-1050">1050</a>
<a href="#wglew.h-1051">1051</a>
<a href="#wglew.h-1052">1052</a>
<a href="#wglew.h-1053">1053</a>
<a href="#wglew.h-1054">1054</a>
<a href="#wglew.h-1055">1055</a>
<a href="#wglew.h-1056">1056</a>
<a href="#wglew.h-1057">1057</a>
<a href="#wglew.h-1058">1058</a>
<a href="#wglew.h-1059">1059</a>
<a href="#wglew.h-1060">1060</a>
<a href="#wglew.h-1061">1061</a>
<a href="#wglew.h-1062">1062</a>
<a href="#wglew.h-1063">1063</a>
<a href="#wglew.h-1064">1064</a>
<a href="#wglew.h-1065">1065</a>
<a href="#wglew.h-1066">1066</a>
<a href="#wglew.h-1067">1067</a>
<a href="#wglew.h-1068">1068</a>
<a href="#wglew.h-1069">1069</a>
<a href="#wglew.h-1070">1070</a>
<a href="#wglew.h-1071">1071</a>
<a href="#wglew.h-1072">1072</a>
<a href="#wglew.h-1073">1073</a>
<a href="#wglew.h-1074">1074</a>
<a href="#wglew.h-1075">1075</a>
<a href="#wglew.h-1076">1076</a>
<a href="#wglew.h-1077">1077</a>
<a href="#wglew.h-1078">1078</a>
<a href="#wglew.h-1079">1079</a>
<a href="#wglew.h-1080">1080</a>
<a href="#wglew.h-1081">1081</a>
<a href="#wglew.h-1082">1082</a>
<a href="#wglew.h-1083">1083</a>
<a href="#wglew.h-1084">1084</a>
<a href="#wglew.h-1085">1085</a>
<a href="#wglew.h-1086">1086</a>
<a href="#wglew.h-1087">1087</a>
<a href="#wglew.h-1088">1088</a>
<a href="#wglew.h-1089">1089</a>
<a href="#wglew.h-1090">1090</a>
<a href="#wglew.h-1091">1091</a>
<a href="#wglew.h-1092">1092</a>
<a href="#wglew.h-1093">1093</a>
<a href="#wglew.h-1094">1094</a>
<a href="#wglew.h-1095">1095</a>
<a href="#wglew.h-1096">1096</a>
<a href="#wglew.h-1097">1097</a>
<a href="#wglew.h-1098">1098</a>
<a href="#wglew.h-1099">1099</a>
<a href="#wglew.h-1100">1100</a>
<a href="#wglew.h-1101">1101</a>
<a href="#wglew.h-1102">1102</a>
<a href="#wglew.h-1103">1103</a>
<a href="#wglew.h-1104">1104</a>
<a href="#wglew.h-1105">1105</a>
<a href="#wglew.h-1106">1106</a>
<a href="#wglew.h-1107">1107</a>
<a href="#wglew.h-1108">1108</a>
<a href="#wglew.h-1109">1109</a>
<a href="#wglew.h-1110">1110</a>
<a href="#wglew.h-1111">1111</a>
<a href="#wglew.h-1112">1112</a>
<a href="#wglew.h-1113">1113</a>
<a href="#wglew.h-1114">1114</a>
<a href="#wglew.h-1115">1115</a>
<a href="#wglew.h-1116">1116</a>
<a href="#wglew.h-1117">1117</a>
<a href="#wglew.h-1118">1118</a>
<a href="#wglew.h-1119">1119</a>
<a href="#wglew.h-1120">1120</a>
<a href="#wglew.h-1121">1121</a>
<a href="#wglew.h-1122">1122</a>
<a href="#wglew.h-1123">1123</a>
<a href="#wglew.h-1124">1124</a>
<a href="#wglew.h-1125">1125</a>
<a href="#wglew.h-1126">1126</a>
<a href="#wglew.h-1127">1127</a>
<a href="#wglew.h-1128">1128</a>
<a href="#wglew.h-1129">1129</a>
<a href="#wglew.h-1130">1130</a>
<a href="#wglew.h-1131">1131</a>
<a href="#wglew.h-1132">1132</a>
<a href="#wglew.h-1133">1133</a>
<a href="#wglew.h-1134">1134</a>
<a href="#wglew.h-1135">1135</a>
<a href="#wglew.h-1136">1136</a>
<a href="#wglew.h-1137">1137</a>
<a href="#wglew.h-1138">1138</a>
<a href="#wglew.h-1139">1139</a>
<a href="#wglew.h-1140">1140</a>
<a href="#wglew.h-1141">1141</a>
<a href="#wglew.h-1142">1142</a>
<a href="#wglew.h-1143">1143</a>
<a href="#wglew.h-1144">1144</a>
<a href="#wglew.h-1145">1145</a>
<a href="#wglew.h-1146">1146</a>
<a href="#wglew.h-1147">1147</a>
<a href="#wglew.h-1148">1148</a>
<a href="#wglew.h-1149">1149</a>
<a href="#wglew.h-1150">1150</a>
<a href="#wglew.h-1151">1151</a>
<a href="#wglew.h-1152">1152</a>
<a href="#wglew.h-1153">1153</a>
<a href="#wglew.h-1154">1154</a>
<a href="#wglew.h-1155">1155</a>
<a href="#wglew.h-1156">1156</a>
<a href="#wglew.h-1157">1157</a>
<a href="#wglew.h-1158">1158</a>
<a href="#wglew.h-1159">1159</a>
<a href="#wglew.h-1160">1160</a>
<a href="#wglew.h-1161">1161</a>
<a href="#wglew.h-1162">1162</a>
<a href="#wglew.h-1163">1163</a>
<a href="#wglew.h-1164">1164</a>
<a href="#wglew.h-1165">1165</a>
<a href="#wglew.h-1166">1166</a>
<a href="#wglew.h-1167">1167</a>
<a href="#wglew.h-1168">1168</a>
<a href="#wglew.h-1169">1169</a>
<a href="#wglew.h-1170">1170</a>
<a href="#wglew.h-1171">1171</a>
<a href="#wglew.h-1172">1172</a>
<a href="#wglew.h-1173">1173</a>
<a href="#wglew.h-1174">1174</a>
<a href="#wglew.h-1175">1175</a>
<a href="#wglew.h-1176">1176</a>
<a href="#wglew.h-1177">1177</a>
<a href="#wglew.h-1178">1178</a>
<a href="#wglew.h-1179">1179</a>
<a href="#wglew.h-1180">1180</a>
<a href="#wglew.h-1181">1181</a>
<a href="#wglew.h-1182">1182</a>
<a href="#wglew.h-1183">1183</a>
<a href="#wglew.h-1184">1184</a>
<a href="#wglew.h-1185">1185</a>
<a href="#wglew.h-1186">1186</a>
<a href="#wglew.h-1187">1187</a>
<a href="#wglew.h-1188">1188</a>
<a href="#wglew.h-1189">1189</a>
<a href="#wglew.h-1190">1190</a>
<a href="#wglew.h-1191">1191</a>
<a href="#wglew.h-1192">1192</a>
<a href="#wglew.h-1193">1193</a>
<a href="#wglew.h-1194">1194</a>
<a href="#wglew.h-1195">1195</a>
<a href="#wglew.h-1196">1196</a>
<a href="#wglew.h-1197">1197</a>
<a href="#wglew.h-1198">1198</a>
<a href="#wglew.h-1199">1199</a>
<a href="#wglew.h-1200">1200</a>
<a href="#wglew.h-1201">1201</a>
<a href="#wglew.h-1202">1202</a>
<a href="#wglew.h-1203">1203</a>
<a href="#wglew.h-1204">1204</a>
<a href="#wglew.h-1205">1205</a>
<a href="#wglew.h-1206">1206</a>
<a href="#wglew.h-1207">1207</a>
<a href="#wglew.h-1208">1208</a>
<a href="#wglew.h-1209">1209</a>
<a href="#wglew.h-1210">1210</a>
<a href="#wglew.h-1211">1211</a>
<a href="#wglew.h-1212">1212</a>
<a href="#wglew.h-1213">1213</a>
<a href="#wglew.h-1214">1214</a>
<a href="#wglew.h-1215">1215</a>
<a href="#wglew.h-1216">1216</a>
<a href="#wglew.h-1217">1217</a>
<a href="#wglew.h-1218">1218</a>
<a href="#wglew.h-1219">1219</a>
<a href="#wglew.h-1220">1220</a>
<a href="#wglew.h-1221">1221</a>
<a href="#wglew.h-1222">1222</a>
<a href="#wglew.h-1223">1223</a>
<a href="#wglew.h-1224">1224</a>
<a href="#wglew.h-1225">1225</a>
<a href="#wglew.h-1226">1226</a>
<a href="#wglew.h-1227">1227</a>
<a href="#wglew.h-1228">1228</a>
<a href="#wglew.h-1229">1229</a>
<a href="#wglew.h-1230">1230</a>
<a href="#wglew.h-1231">1231</a>
<a href="#wglew.h-1232">1232</a>
<a href="#wglew.h-1233">1233</a>
<a href="#wglew.h-1234">1234</a>
<a href="#wglew.h-1235">1235</a>
<a href="#wglew.h-1236">1236</a>
<a href="#wglew.h-1237">1237</a>
<a href="#wglew.h-1238">1238</a>
<a href="#wglew.h-1239">1239</a>
<a href="#wglew.h-1240">1240</a>
<a href="#wglew.h-1241">1241</a>
<a href="#wglew.h-1242">1242</a>
<a href="#wglew.h-1243">1243</a>
<a href="#wglew.h-1244">1244</a>
<a href="#wglew.h-1245">1245</a>
<a href="#wglew.h-1246">1246</a>
<a href="#wglew.h-1247">1247</a>
<a href="#wglew.h-1248">1248</a>
<a href="#wglew.h-1249">1249</a>
<a href="#wglew.h-1250">1250</a>
<a href="#wglew.h-1251">1251</a>
<a href="#wglew.h-1252">1252</a>
<a href="#wglew.h-1253">1253</a>
<a href="#wglew.h-1254">1254</a>
<a href="#wglew.h-1255">1255</a>
<a href="#wglew.h-1256">1256</a>
<a href="#wglew.h-1257">1257</a>
<a href="#wglew.h-1258">1258</a>
<a href="#wglew.h-1259">1259</a>
<a href="#wglew.h-1260">1260</a>
<a href="#wglew.h-1261">1261</a>
<a href="#wglew.h-1262">1262</a>
<a href="#wglew.h-1263">1263</a>
<a href="#wglew.h-1264">1264</a>
<a href="#wglew.h-1265">1265</a>
<a href="#wglew.h-1266">1266</a>
<a href="#wglew.h-1267">1267</a>
<a href="#wglew.h-1268">1268</a>
<a href="#wglew.h-1269">1269</a>
<a href="#wglew.h-1270">1270</a>
<a href="#wglew.h-1271">1271</a>
<a href="#wglew.h-1272">1272</a>
<a href="#wglew.h-1273">1273</a>
<a href="#wglew.h-1274">1274</a>
<a href="#wglew.h-1275">1275</a>
<a href="#wglew.h-1276">1276</a>
<a href="#wglew.h-1277">1277</a>
<a href="#wglew.h-1278">1278</a>
<a href="#wglew.h-1279">1279</a>
<a href="#wglew.h-1280">1280</a>
<a href="#wglew.h-1281">1281</a>
<a href="#wglew.h-1282">1282</a>
<a href="#wglew.h-1283">1283</a>
<a href="#wglew.h-1284">1284</a>
<a href="#wglew.h-1285">1285</a>
<a href="#wglew.h-1286">1286</a>
<a href="#wglew.h-1287">1287</a>
<a href="#wglew.h-1288">1288</a>
<a href="#wglew.h-1289">1289</a>
<a href="#wglew.h-1290">1290</a>
<a href="#wglew.h-1291">1291</a>
<a href="#wglew.h-1292">1292</a>
<a href="#wglew.h-1293">1293</a>
<a href="#wglew.h-1294">1294</a>
<a href="#wglew.h-1295">1295</a>
<a href="#wglew.h-1296">1296</a>
<a href="#wglew.h-1297">1297</a>
<a href="#wglew.h-1298">1298</a>
<a href="#wglew.h-1299">1299</a>
<a href="#wglew.h-1300">1300</a>
<a href="#wglew.h-1301">1301</a>
<a href="#wglew.h-1302">1302</a>
<a href="#wglew.h-1303">1303</a>
<a href="#wglew.h-1304">1304</a>
<a href="#wglew.h-1305">1305</a>
<a href="#wglew.h-1306">1306</a>
<a href="#wglew.h-1307">1307</a>
<a href="#wglew.h-1308">1308</a>
<a href="#wglew.h-1309">1309</a>
<a href="#wglew.h-1310">1310</a>
<a href="#wglew.h-1311">1311</a>
<a href="#wglew.h-1312">1312</a>
<a href="#wglew.h-1313">1313</a>
<a href="#wglew.h-1314">1314</a>
<a href="#wglew.h-1315">1315</a>
<a href="#wglew.h-1316">1316</a>
<a href="#wglew.h-1317">1317</a>
<a href="#wglew.h-1318">1318</a>
<a href="#wglew.h-1319">1319</a>
<a href="#wglew.h-1320">1320</a>
<a href="#wglew.h-1321">1321</a>
<a href="#wglew.h-1322">1322</a>
<a href="#wglew.h-1323">1323</a>
<a href="#wglew.h-1324">1324</a>
<a href="#wglew.h-1325">1325</a>
<a href="#wglew.h-1326">1326</a>
<a href="#wglew.h-1327">1327</a>
<a href="#wglew.h-1328">1328</a>
<a href="#wglew.h-1329">1329</a>
<a href="#wglew.h-1330">1330</a>
<a href="#wglew.h-1331">1331</a>
<a href="#wglew.h-1332">1332</a>
<a href="#wglew.h-1333">1333</a>
<a href="#wglew.h-1334">1334</a>
<a href="#wglew.h-1335">1335</a>
<a href="#wglew.h-1336">1336</a>
<a href="#wglew.h-1337">1337</a>
<a href="#wglew.h-1338">1338</a>
<a href="#wglew.h-1339">1339</a>
<a href="#wglew.h-1340">1340</a>
<a href="#wglew.h-1341">1341</a>
<a href="#wglew.h-1342">1342</a>
<a href="#wglew.h-1343">1343</a>
<a href="#wglew.h-1344">1344</a>
<a href="#wglew.h-1345">1345</a>
<a href="#wglew.h-1346">1346</a>
<a href="#wglew.h-1347">1347</a>
<a href="#wglew.h-1348">1348</a>
<a href="#wglew.h-1349">1349</a>
<a href="#wglew.h-1350">1350</a>
<a href="#wglew.h-1351">1351</a>
<a href="#wglew.h-1352">1352</a>
<a href="#wglew.h-1353">1353</a>
<a href="#wglew.h-1354">1354</a>
<a href="#wglew.h-1355">1355</a>
<a href="#wglew.h-1356">1356</a>
<a href="#wglew.h-1357">1357</a>
<a href="#wglew.h-1358">1358</a>
<a href="#wglew.h-1359">1359</a>
<a href="#wglew.h-1360">1360</a>
<a href="#wglew.h-1361">1361</a>
<a href="#wglew.h-1362">1362</a>
<a href="#wglew.h-1363">1363</a>
<a href="#wglew.h-1364">1364</a>
<a href="#wglew.h-1365">1365</a>
<a href="#wglew.h-1366">1366</a>
<a href="#wglew.h-1367">1367</a>
<a href="#wglew.h-1368">1368</a>
<a href="#wglew.h-1369">1369</a>
<a href="#wglew.h-1370">1370</a>
<a href="#wglew.h-1371">1371</a>
<a href="#wglew.h-1372">1372</a>
<a href="#wglew.h-1373">1373</a>
<a href="#wglew.h-1374">1374</a>
<a href="#wglew.h-1375">1375</a>
<a href="#wglew.h-1376">1376</a>
<a href="#wglew.h-1377">1377</a>
<a href="#wglew.h-1378">1378</a>
<a href="#wglew.h-1379">1379</a>
<a href="#wglew.h-1380">1380</a>
<a href="#wglew.h-1381">1381</a>
<a href="#wglew.h-1382">1382</a>
<a href="#wglew.h-1383">1383</a>
<a href="#wglew.h-1384">1384</a>
<a href="#wglew.h-1385">1385</a>
<a href="#wglew.h-1386">1386</a>
<a href="#wglew.h-1387">1387</a>
<a href="#wglew.h-1388">1388</a>
<a href="#wglew.h-1389">1389</a>
<a href="#wglew.h-1390">1390</a>
<a href="#wglew.h-1391">1391</a>
<a href="#wglew.h-1392">1392</a>
<a href="#wglew.h-1393">1393</a>
<a href="#wglew.h-1394">1394</a>
<a href="#wglew.h-1395">1395</a>
<a href="#wglew.h-1396">1396</a>
<a href="#wglew.h-1397">1397</a>
<a href="#wglew.h-1398">1398</a>
<a href="#wglew.h-1399">1399</a>
<a href="#wglew.h-1400">1400</a>
<a href="#wglew.h-1401">1401</a>
<a href="#wglew.h-1402">1402</a>
<a href="#wglew.h-1403">1403</a>
<a href="#wglew.h-1404">1404</a>
<a href="#wglew.h-1405">1405</a>
<a href="#wglew.h-1406">1406</a>
<a href="#wglew.h-1407">1407</a>
<a href="#wglew.h-1408">1408</a>
<a href="#wglew.h-1409">1409</a>
<a href="#wglew.h-1410">1410</a>
<a href="#wglew.h-1411">1411</a>
<a href="#wglew.h-1412">1412</a>
<a href="#wglew.h-1413">1413</a>
<a href="#wglew.h-1414">1414</a>
<a href="#wglew.h-1415">1415</a>
<a href="#wglew.h-1416">1416</a>
<a href="#wglew.h-1417">1417</a>
<a href="#wglew.h-1418">1418</a>
<a href="#wglew.h-1419">1419</a>
<a href="#wglew.h-1420">1420</a>
<a href="#wglew.h-1421">1421</a>
<a href="#wglew.h-1422">1422</a>
<a href="#wglew.h-1423">1423</a>
<a href="#wglew.h-1424">1424</a>
<a href="#wglew.h-1425">1425</a>
<a href="#wglew.h-1426">1426</a>
<a href="#wglew.h-1427">1427</a>
<a href="#wglew.h-1428">1428</a>
<a href="#wglew.h-1429">1429</a>
<a href="#wglew.h-1430">1430</a>
<a href="#wglew.h-1431">1431</a>
<a href="#wglew.h-1432">1432</a>
<a href="#wglew.h-1433">1433</a>
<a href="#wglew.h-1434">1434</a>
<a href="#wglew.h-1435">1435</a>
<a href="#wglew.h-1436">1436</a>
<a href="#wglew.h-1437">1437</a>
<a href="#wglew.h-1438">1438</a>
<a href="#wglew.h-1439">1439</a>
<a href="#wglew.h-1440">1440</a>
<a href="#wglew.h-1441">1441</a>
<a href="#wglew.h-1442">1442</a>
<a href="#wglew.h-1443">1443</a>
<a href="#wglew.h-1444">1444</a>
<a href="#wglew.h-1445">1445</a>
<a href="#wglew.h-1446">1446</a>
<a href="#wglew.h-1447">1447</a></pre></div></td><td class="code"><div class="codehilite highlight"><pre><span></span><a name="wglew.h-1"></a><span class="cm">/*</span>
<a name="wglew.h-2"></a><span class="cm">** The OpenGL Extension Wrangler Library</span>
<a name="wglew.h-3"></a><span class="cm">** Copyright (C) 2008-2017, Nigel Stewart &lt;nigels[]users sourceforge net&gt;</span>
<a name="wglew.h-4"></a><span class="cm">** Copyright (C) 2002-2008, Milan Ikits &lt;milan ikits[]ieee org&gt;</span>
<a name="wglew.h-5"></a><span class="cm">** Copyright (C) 2002-2008, Marcelo E. Magallon &lt;mmagallo[]debian org&gt;</span>
<a name="wglew.h-6"></a><span class="cm">** Copyright (C) 2002, Lev Povalahev</span>
<a name="wglew.h-7"></a><span class="cm">** All rights reserved.</span>
<a name="wglew.h-8"></a><span class="cm">** </span>
<a name="wglew.h-9"></a><span class="cm">** Redistribution and use in source and binary forms, with or without </span>
<a name="wglew.h-10"></a><span class="cm">** modification, are permitted provided that the following conditions are met:</span>
<a name="wglew.h-11"></a><span class="cm">** </span>
<a name="wglew.h-12"></a><span class="cm">** * Redistributions of source code must retain the above copyright notice, </span>
<a name="wglew.h-13"></a><span class="cm">**   this list of conditions and the following disclaimer.</span>
<a name="wglew.h-14"></a><span class="cm">** * Redistributions in binary form must reproduce the above copyright notice, </span>
<a name="wglew.h-15"></a><span class="cm">**   this list of conditions and the following disclaimer in the documentation </span>
<a name="wglew.h-16"></a><span class="cm">**   and/or other materials provided with the distribution.</span>
<a name="wglew.h-17"></a><span class="cm">** * The name of the author may be used to endorse or promote products </span>
<a name="wglew.h-18"></a><span class="cm">**   derived from this software without specific prior written permission.</span>
<a name="wglew.h-19"></a><span class="cm">**</span>
<a name="wglew.h-20"></a><span class="cm">** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; </span>
<a name="wglew.h-21"></a><span class="cm">** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE </span>
<a name="wglew.h-22"></a><span class="cm">** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE</span>
<a name="wglew.h-23"></a><span class="cm">** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE </span>
<a name="wglew.h-24"></a><span class="cm">** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR </span>
<a name="wglew.h-25"></a><span class="cm">** CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF </span>
<a name="wglew.h-26"></a><span class="cm">** SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS</span>
<a name="wglew.h-27"></a><span class="cm">** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN</span>
<a name="wglew.h-28"></a><span class="cm">** CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)</span>
<a name="wglew.h-29"></a><span class="cm">** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF</span>
<a name="wglew.h-30"></a><span class="cm">** THE POSSIBILITY OF SUCH DAMAGE.</span>
<a name="wglew.h-31"></a><span class="cm">*/</span>
<a name="wglew.h-32"></a>
<a name="wglew.h-33"></a><span class="cm">/*</span>
<a name="wglew.h-34"></a><span class="cm">** Copyright (c) 2007 The Khronos Group Inc.</span>
<a name="wglew.h-35"></a><span class="cm">** </span>
<a name="wglew.h-36"></a><span class="cm">** Permission is hereby granted, free of charge, to any person obtaining a</span>
<a name="wglew.h-37"></a><span class="cm">** copy of this software and/or associated documentation files (the</span>
<a name="wglew.h-38"></a><span class="cm">** &quot;Materials&quot;), to deal in the Materials without restriction, including</span>
<a name="wglew.h-39"></a><span class="cm">** without limitation the rights to use, copy, modify, merge, publish,</span>
<a name="wglew.h-40"></a><span class="cm">** distribute, sublicense, and/or sell copies of the Materials, and to</span>
<a name="wglew.h-41"></a><span class="cm">** permit persons to whom the Materials are furnished to do so, subject to</span>
<a name="wglew.h-42"></a><span class="cm">** the following conditions:</span>
<a name="wglew.h-43"></a><span class="cm">** </span>
<a name="wglew.h-44"></a><span class="cm">** The above copyright notice and this permission notice shall be included</span>
<a name="wglew.h-45"></a><span class="cm">** in all copies or substantial portions of the Materials.</span>
<a name="wglew.h-46"></a><span class="cm">** </span>
<a name="wglew.h-47"></a><span class="cm">** THE MATERIALS ARE PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND,</span>
<a name="wglew.h-48"></a><span class="cm">** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF</span>
<a name="wglew.h-49"></a><span class="cm">** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.</span>
<a name="wglew.h-50"></a><span class="cm">** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY</span>
<a name="wglew.h-51"></a><span class="cm">** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,</span>
<a name="wglew.h-52"></a><span class="cm">** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE</span>
<a name="wglew.h-53"></a><span class="cm">** MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.</span>
<a name="wglew.h-54"></a><span class="cm">*/</span>
<a name="wglew.h-55"></a>
<a name="wglew.h-56"></a><span class="cp">#ifndef __wglew_h__</span>
<a name="wglew.h-57"></a><span class="cp">#define __wglew_h__</span>
<a name="wglew.h-58"></a><span class="cp">#define __WGLEW_H__</span>
<a name="wglew.h-59"></a>
<a name="wglew.h-60"></a><span class="cp">#ifdef __wglext_h_</span>
<a name="wglew.h-61"></a><span class="cp">#error wglext.h included before wglew.h</span>
<a name="wglew.h-62"></a><span class="cp">#endif</span>
<a name="wglew.h-63"></a>
<a name="wglew.h-64"></a><span class="cp">#define __wglext_h_</span>
<a name="wglew.h-65"></a>
<a name="wglew.h-66"></a><span class="cp">#if !defined(WINAPI)</span>
<a name="wglew.h-67"></a><span class="cp">#  ifndef WIN32_LEAN_AND_MEAN</span>
<a name="wglew.h-68"></a><span class="cp">#    define WIN32_LEAN_AND_MEAN 1</span>
<a name="wglew.h-69"></a><span class="cp">#  endif</span>
<a name="wglew.h-70"></a><span class="cp">#include</span> <span class="cpf">&lt;windows.h&gt;</span><span class="cp"></span>
<a name="wglew.h-71"></a><span class="cp">#  undef WIN32_LEAN_AND_MEAN</span>
<a name="wglew.h-72"></a><span class="cp">#endif</span>
<a name="wglew.h-73"></a>
<a name="wglew.h-74"></a><span class="cm">/*</span>
<a name="wglew.h-75"></a><span class="cm"> * GLEW_STATIC needs to be set when using the static version.</span>
<a name="wglew.h-76"></a><span class="cm"> * GLEW_BUILD is set when building the DLL version.</span>
<a name="wglew.h-77"></a><span class="cm"> */</span>
<a name="wglew.h-78"></a><span class="cp">#ifdef GLEW_STATIC</span>
<a name="wglew.h-79"></a><span class="cp">#  define GLEWAPI extern</span>
<a name="wglew.h-80"></a><span class="cp">#else</span>
<a name="wglew.h-81"></a><span class="cp">#  ifdef GLEW_BUILD</span>
<a name="wglew.h-82"></a><span class="cp">#    define GLEWAPI extern __declspec(dllexport)</span>
<a name="wglew.h-83"></a><span class="cp">#  else</span>
<a name="wglew.h-84"></a><span class="cp">#    define GLEWAPI extern __declspec(dllimport)</span>
<a name="wglew.h-85"></a><span class="cp">#  endif</span>
<a name="wglew.h-86"></a><span class="cp">#endif</span>
<a name="wglew.h-87"></a>
<a name="wglew.h-88"></a><span class="cp">#ifdef __cplusplus</span>
<a name="wglew.h-89"></a><span class="k">extern</span> <span class="s">&quot;C&quot;</span> <span class="p">{</span>
<a name="wglew.h-90"></a><span class="cp">#endif</span>
<a name="wglew.h-91"></a>
<a name="wglew.h-92"></a><span class="cm">/* -------------------------- WGL_3DFX_multisample ------------------------- */</span>
<a name="wglew.h-93"></a>
<a name="wglew.h-94"></a><span class="cp">#ifndef WGL_3DFX_multisample</span>
<a name="wglew.h-95"></a><span class="cp">#define WGL_3DFX_multisample 1</span>
<a name="wglew.h-96"></a>
<a name="wglew.h-97"></a><span class="cp">#define WGL_SAMPLE_BUFFERS_3DFX 0x2060</span>
<a name="wglew.h-98"></a><span class="cp">#define WGL_SAMPLES_3DFX 0x2061</span>
<a name="wglew.h-99"></a>
<a name="wglew.h-100"></a><span class="cp">#define WGLEW_3DFX_multisample WGLEW_GET_VAR(__WGLEW_3DFX_multisample)</span>
<a name="wglew.h-101"></a>
<a name="wglew.h-102"></a><span class="cp">#endif </span><span class="cm">/* WGL_3DFX_multisample */</span><span class="cp"></span>
<a name="wglew.h-103"></a>
<a name="wglew.h-104"></a><span class="cm">/* ------------------------- WGL_3DL_stereo_control ------------------------ */</span>
<a name="wglew.h-105"></a>
<a name="wglew.h-106"></a><span class="cp">#ifndef WGL_3DL_stereo_control</span>
<a name="wglew.h-107"></a><span class="cp">#define WGL_3DL_stereo_control 1</span>
<a name="wglew.h-108"></a>
<a name="wglew.h-109"></a><span class="cp">#define WGL_STEREO_EMITTER_ENABLE_3DL 0x2055</span>
<a name="wglew.h-110"></a><span class="cp">#define WGL_STEREO_EMITTER_DISABLE_3DL 0x2056</span>
<a name="wglew.h-111"></a><span class="cp">#define WGL_STEREO_POLARITY_NORMAL_3DL 0x2057</span>
<a name="wglew.h-112"></a><span class="cp">#define WGL_STEREO_POLARITY_INVERT_3DL 0x2058</span>
<a name="wglew.h-113"></a>
<a name="wglew.h-114"></a><span class="k">typedef</span> <span class="n">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSETSTEREOEMITTERSTATE3DLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">uState</span><span class="p">);</span>
<a name="wglew.h-115"></a>
<a name="wglew.h-116"></a><span class="cp">#define wglSetStereoEmitterState3DL WGLEW_GET_FUN(__wglewSetStereoEmitterState3DL)</span>
<a name="wglew.h-117"></a>
<a name="wglew.h-118"></a><span class="cp">#define WGLEW_3DL_stereo_control WGLEW_GET_VAR(__WGLEW_3DL_stereo_control)</span>
<a name="wglew.h-119"></a>
<a name="wglew.h-120"></a><span class="cp">#endif </span><span class="cm">/* WGL_3DL_stereo_control */</span><span class="cp"></span>
<a name="wglew.h-121"></a>
<a name="wglew.h-122"></a><span class="cm">/* ------------------------ WGL_AMD_gpu_association ------------------------ */</span>
<a name="wglew.h-123"></a>
<a name="wglew.h-124"></a><span class="cp">#ifndef WGL_AMD_gpu_association</span>
<a name="wglew.h-125"></a><span class="cp">#define WGL_AMD_gpu_association 1</span>
<a name="wglew.h-126"></a>
<a name="wglew.h-127"></a><span class="cp">#define WGL_GPU_VENDOR_AMD 0x1F00</span>
<a name="wglew.h-128"></a><span class="cp">#define WGL_GPU_RENDERER_STRING_AMD 0x1F01</span>
<a name="wglew.h-129"></a><span class="cp">#define WGL_GPU_OPENGL_VERSION_STRING_AMD 0x1F02</span>
<a name="wglew.h-130"></a><span class="cp">#define WGL_GPU_FASTEST_TARGET_GPUS_AMD 0x21A2</span>
<a name="wglew.h-131"></a><span class="cp">#define WGL_GPU_RAM_AMD 0x21A3</span>
<a name="wglew.h-132"></a><span class="cp">#define WGL_GPU_CLOCK_AMD 0x21A4</span>
<a name="wglew.h-133"></a><span class="cp">#define WGL_GPU_NUM_PIPES_AMD 0x21A5</span>
<a name="wglew.h-134"></a><span class="cp">#define WGL_GPU_NUM_SIMD_AMD 0x21A6</span>
<a name="wglew.h-135"></a><span class="cp">#define WGL_GPU_NUM_RB_AMD 0x21A7</span>
<a name="wglew.h-136"></a><span class="cp">#define WGL_GPU_NUM_SPI_AMD 0x21A8</span>
<a name="wglew.h-137"></a>
<a name="wglew.h-138"></a><span class="k">typedef</span> <span class="nf">VOID</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBLITCONTEXTFRAMEBUFFERAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HGLRC</span> <span class="n">dstCtx</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcX0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcY0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcX1</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcY1</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstX0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstY0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstX1</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstY1</span><span class="p">,</span> <span class="n">GLbitfield</span> <span class="n">mask</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">filter</span><span class="p">);</span>
<a name="wglew.h-139"></a><span class="k">typedef</span> <span class="nf">HGLRC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEASSOCIATEDCONTEXTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">UINT</span> <span class="n">id</span><span class="p">);</span>
<a name="wglew.h-140"></a><span class="k">typedef</span> <span class="nf">HGLRC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEASSOCIATEDCONTEXTATTRIBSAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">UINT</span> <span class="n">id</span><span class="p">,</span> <span class="n">HGLRC</span> <span class="n">hShareContext</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">attribList</span><span class="p">);</span>
<a name="wglew.h-141"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDELETEASSOCIATEDCONTEXTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HGLRC</span> <span class="n">hglrc</span><span class="p">);</span>
<a name="wglew.h-142"></a><span class="k">typedef</span> <span class="nf">UINT</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETCONTEXTGPUIDAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HGLRC</span> <span class="n">hglrc</span><span class="p">);</span>
<a name="wglew.h-143"></a><span class="k">typedef</span> <span class="nf">HGLRC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETCURRENTASSOCIATEDCONTEXTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="wglew.h-144"></a><span class="k">typedef</span> <span class="nf">UINT</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGPUIDSAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">UINT</span> <span class="n">maxCount</span><span class="p">,</span> <span class="n">UINT</span><span class="o">*</span> <span class="n">ids</span><span class="p">);</span>
<a name="wglew.h-145"></a><span class="k">typedef</span> <span class="nf">INT</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGPUINFOAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">UINT</span> <span class="n">id</span><span class="p">,</span> <span class="n">INT</span> <span class="n">property</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">dataType</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">size</span><span class="p">,</span> <span class="kt">void</span><span class="o">*</span> <span class="n">data</span><span class="p">);</span>
<a name="wglew.h-146"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLMAKEASSOCIATEDCONTEXTCURRENTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HGLRC</span> <span class="n">hglrc</span><span class="p">);</span>
<a name="wglew.h-147"></a>
<a name="wglew.h-148"></a><span class="cp">#define wglBlitContextFramebufferAMD WGLEW_GET_FUN(__wglewBlitContextFramebufferAMD)</span>
<a name="wglew.h-149"></a><span class="cp">#define wglCreateAssociatedContextAMD WGLEW_GET_FUN(__wglewCreateAssociatedContextAMD)</span>
<a name="wglew.h-150"></a><span class="cp">#define wglCreateAssociatedContextAttribsAMD WGLEW_GET_FUN(__wglewCreateAssociatedContextAttribsAMD)</span>
<a name="wglew.h-151"></a><span class="cp">#define wglDeleteAssociatedContextAMD WGLEW_GET_FUN(__wglewDeleteAssociatedContextAMD)</span>
<a name="wglew.h-152"></a><span class="cp">#define wglGetContextGPUIDAMD WGLEW_GET_FUN(__wglewGetContextGPUIDAMD)</span>
<a name="wglew.h-153"></a><span class="cp">#define wglGetCurrentAssociatedContextAMD WGLEW_GET_FUN(__wglewGetCurrentAssociatedContextAMD)</span>
<a name="wglew.h-154"></a><span class="cp">#define wglGetGPUIDsAMD WGLEW_GET_FUN(__wglewGetGPUIDsAMD)</span>
<a name="wglew.h-155"></a><span class="cp">#define wglGetGPUInfoAMD WGLEW_GET_FUN(__wglewGetGPUInfoAMD)</span>
<a name="wglew.h-156"></a><span class="cp">#define wglMakeAssociatedContextCurrentAMD WGLEW_GET_FUN(__wglewMakeAssociatedContextCurrentAMD)</span>
<a name="wglew.h-157"></a>
<a name="wglew.h-158"></a><span class="cp">#define WGLEW_AMD_gpu_association WGLEW_GET_VAR(__WGLEW_AMD_gpu_association)</span>
<a name="wglew.h-159"></a>
<a name="wglew.h-160"></a><span class="cp">#endif </span><span class="cm">/* WGL_AMD_gpu_association */</span><span class="cp"></span>
<a name="wglew.h-161"></a>
<a name="wglew.h-162"></a><span class="cm">/* ------------------------- WGL_ARB_buffer_region ------------------------- */</span>
<a name="wglew.h-163"></a>
<a name="wglew.h-164"></a><span class="cp">#ifndef WGL_ARB_buffer_region</span>
<a name="wglew.h-165"></a><span class="cp">#define WGL_ARB_buffer_region 1</span>
<a name="wglew.h-166"></a>
<a name="wglew.h-167"></a><span class="cp">#define WGL_FRONT_COLOR_BUFFER_BIT_ARB 0x00000001</span>
<a name="wglew.h-168"></a><span class="cp">#define WGL_BACK_COLOR_BUFFER_BIT_ARB 0x00000002</span>
<a name="wglew.h-169"></a><span class="cp">#define WGL_DEPTH_BUFFER_BIT_ARB 0x00000004</span>
<a name="wglew.h-170"></a><span class="cp">#define WGL_STENCIL_BUFFER_BIT_ARB 0x00000008</span>
<a name="wglew.h-171"></a>
<a name="wglew.h-172"></a><span class="k">typedef</span> <span class="nf">HANDLE</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEBUFFERREGIONARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iLayerPlane</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">uType</span><span class="p">);</span>
<a name="wglew.h-173"></a><span class="k">typedef</span> <span class="nf">VOID</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDELETEBUFFERREGIONARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hRegion</span><span class="p">);</span>
<a name="wglew.h-174"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRESTOREBUFFERREGIONARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hRegion</span><span class="p">,</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span><span class="p">,</span> <span class="kt">int</span> <span class="n">xSrc</span><span class="p">,</span> <span class="kt">int</span> <span class="n">ySrc</span><span class="p">);</span>
<a name="wglew.h-175"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSAVEBUFFERREGIONARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hRegion</span><span class="p">,</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span><span class="p">);</span>
<a name="wglew.h-176"></a>
<a name="wglew.h-177"></a><span class="cp">#define wglCreateBufferRegionARB WGLEW_GET_FUN(__wglewCreateBufferRegionARB)</span>
<a name="wglew.h-178"></a><span class="cp">#define wglDeleteBufferRegionARB WGLEW_GET_FUN(__wglewDeleteBufferRegionARB)</span>
<a name="wglew.h-179"></a><span class="cp">#define wglRestoreBufferRegionARB WGLEW_GET_FUN(__wglewRestoreBufferRegionARB)</span>
<a name="wglew.h-180"></a><span class="cp">#define wglSaveBufferRegionARB WGLEW_GET_FUN(__wglewSaveBufferRegionARB)</span>
<a name="wglew.h-181"></a>
<a name="wglew.h-182"></a><span class="cp">#define WGLEW_ARB_buffer_region WGLEW_GET_VAR(__WGLEW_ARB_buffer_region)</span>
<a name="wglew.h-183"></a>
<a name="wglew.h-184"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_buffer_region */</span><span class="cp"></span>
<a name="wglew.h-185"></a>
<a name="wglew.h-186"></a><span class="cm">/* --------------------- WGL_ARB_context_flush_control --------------------- */</span>
<a name="wglew.h-187"></a>
<a name="wglew.h-188"></a><span class="cp">#ifndef WGL_ARB_context_flush_control</span>
<a name="wglew.h-189"></a><span class="cp">#define WGL_ARB_context_flush_control 1</span>
<a name="wglew.h-190"></a>
<a name="wglew.h-191"></a><span class="cp">#define WGLEW_ARB_context_flush_control WGLEW_GET_VAR(__WGLEW_ARB_context_flush_control)</span>
<a name="wglew.h-192"></a>
<a name="wglew.h-193"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_context_flush_control */</span><span class="cp"></span>
<a name="wglew.h-194"></a>
<a name="wglew.h-195"></a><span class="cm">/* ------------------------- WGL_ARB_create_context ------------------------ */</span>
<a name="wglew.h-196"></a>
<a name="wglew.h-197"></a><span class="cp">#ifndef WGL_ARB_create_context</span>
<a name="wglew.h-198"></a><span class="cp">#define WGL_ARB_create_context 1</span>
<a name="wglew.h-199"></a>
<a name="wglew.h-200"></a><span class="cp">#define WGL_CONTEXT_DEBUG_BIT_ARB 0x0001</span>
<a name="wglew.h-201"></a><span class="cp">#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x0002</span>
<a name="wglew.h-202"></a><span class="cp">#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091</span>
<a name="wglew.h-203"></a><span class="cp">#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092</span>
<a name="wglew.h-204"></a><span class="cp">#define WGL_CONTEXT_LAYER_PLANE_ARB 0x2093</span>
<a name="wglew.h-205"></a><span class="cp">#define WGL_CONTEXT_FLAGS_ARB 0x2094</span>
<a name="wglew.h-206"></a><span class="cp">#define ERROR_INVALID_VERSION_ARB 0x2095</span>
<a name="wglew.h-207"></a><span class="cp">#define ERROR_INVALID_PROFILE_ARB 0x2096</span>
<a name="wglew.h-208"></a>
<a name="wglew.h-209"></a><span class="k">typedef</span> <span class="nf">HGLRC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATECONTEXTATTRIBSARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">HGLRC</span> <span class="n">hShareContext</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">attribList</span><span class="p">);</span>
<a name="wglew.h-210"></a>
<a name="wglew.h-211"></a><span class="cp">#define wglCreateContextAttribsARB WGLEW_GET_FUN(__wglewCreateContextAttribsARB)</span>
<a name="wglew.h-212"></a>
<a name="wglew.h-213"></a><span class="cp">#define WGLEW_ARB_create_context WGLEW_GET_VAR(__WGLEW_ARB_create_context)</span>
<a name="wglew.h-214"></a>
<a name="wglew.h-215"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_create_context */</span><span class="cp"></span>
<a name="wglew.h-216"></a>
<a name="wglew.h-217"></a><span class="cm">/* -------------------- WGL_ARB_create_context_no_error -------------------- */</span>
<a name="wglew.h-218"></a>
<a name="wglew.h-219"></a><span class="cp">#ifndef WGL_ARB_create_context_no_error</span>
<a name="wglew.h-220"></a><span class="cp">#define WGL_ARB_create_context_no_error 1</span>
<a name="wglew.h-221"></a>
<a name="wglew.h-222"></a><span class="cp">#define WGLEW_ARB_create_context_no_error WGLEW_GET_VAR(__WGLEW_ARB_create_context_no_error)</span>
<a name="wglew.h-223"></a>
<a name="wglew.h-224"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_create_context_no_error */</span><span class="cp"></span>
<a name="wglew.h-225"></a>
<a name="wglew.h-226"></a><span class="cm">/* --------------------- WGL_ARB_create_context_profile -------------------- */</span>
<a name="wglew.h-227"></a>
<a name="wglew.h-228"></a><span class="cp">#ifndef WGL_ARB_create_context_profile</span>
<a name="wglew.h-229"></a><span class="cp">#define WGL_ARB_create_context_profile 1</span>
<a name="wglew.h-230"></a>
<a name="wglew.h-231"></a><span class="cp">#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001</span>
<a name="wglew.h-232"></a><span class="cp">#define WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002</span>
<a name="wglew.h-233"></a><span class="cp">#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126</span>
<a name="wglew.h-234"></a>
<a name="wglew.h-235"></a><span class="cp">#define WGLEW_ARB_create_context_profile WGLEW_GET_VAR(__WGLEW_ARB_create_context_profile)</span>
<a name="wglew.h-236"></a>
<a name="wglew.h-237"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_create_context_profile */</span><span class="cp"></span>
<a name="wglew.h-238"></a>
<a name="wglew.h-239"></a><span class="cm">/* ------------------- WGL_ARB_create_context_robustness ------------------- */</span>
<a name="wglew.h-240"></a>
<a name="wglew.h-241"></a><span class="cp">#ifndef WGL_ARB_create_context_robustness</span>
<a name="wglew.h-242"></a><span class="cp">#define WGL_ARB_create_context_robustness 1</span>
<a name="wglew.h-243"></a>
<a name="wglew.h-244"></a><span class="cp">#define WGL_CONTEXT_ROBUST_ACCESS_BIT_ARB 0x00000004</span>
<a name="wglew.h-245"></a><span class="cp">#define WGL_LOSE_CONTEXT_ON_RESET_ARB 0x8252</span>
<a name="wglew.h-246"></a><span class="cp">#define WGL_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB 0x8256</span>
<a name="wglew.h-247"></a><span class="cp">#define WGL_NO_RESET_NOTIFICATION_ARB 0x8261</span>
<a name="wglew.h-248"></a>
<a name="wglew.h-249"></a><span class="cp">#define WGLEW_ARB_create_context_robustness WGLEW_GET_VAR(__WGLEW_ARB_create_context_robustness)</span>
<a name="wglew.h-250"></a>
<a name="wglew.h-251"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_create_context_robustness */</span><span class="cp"></span>
<a name="wglew.h-252"></a>
<a name="wglew.h-253"></a><span class="cm">/* ----------------------- WGL_ARB_extensions_string ----------------------- */</span>
<a name="wglew.h-254"></a>
<a name="wglew.h-255"></a><span class="cp">#ifndef WGL_ARB_extensions_string</span>
<a name="wglew.h-256"></a><span class="cp">#define WGL_ARB_extensions_string 1</span>
<a name="wglew.h-257"></a>
<a name="wglew.h-258"></a><span class="k">typedef</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETEXTENSIONSSTRINGARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">);</span>
<a name="wglew.h-259"></a>
<a name="wglew.h-260"></a><span class="cp">#define wglGetExtensionsStringARB WGLEW_GET_FUN(__wglewGetExtensionsStringARB)</span>
<a name="wglew.h-261"></a>
<a name="wglew.h-262"></a><span class="cp">#define WGLEW_ARB_extensions_string WGLEW_GET_VAR(__WGLEW_ARB_extensions_string)</span>
<a name="wglew.h-263"></a>
<a name="wglew.h-264"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_extensions_string */</span><span class="cp"></span>
<a name="wglew.h-265"></a>
<a name="wglew.h-266"></a><span class="cm">/* ------------------------ WGL_ARB_framebuffer_sRGB ----------------------- */</span>
<a name="wglew.h-267"></a>
<a name="wglew.h-268"></a><span class="cp">#ifndef WGL_ARB_framebuffer_sRGB</span>
<a name="wglew.h-269"></a><span class="cp">#define WGL_ARB_framebuffer_sRGB 1</span>
<a name="wglew.h-270"></a>
<a name="wglew.h-271"></a><span class="cp">#define WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB 0x20A9</span>
<a name="wglew.h-272"></a>
<a name="wglew.h-273"></a><span class="cp">#define WGLEW_ARB_framebuffer_sRGB WGLEW_GET_VAR(__WGLEW_ARB_framebuffer_sRGB)</span>
<a name="wglew.h-274"></a>
<a name="wglew.h-275"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_framebuffer_sRGB */</span><span class="cp"></span>
<a name="wglew.h-276"></a>
<a name="wglew.h-277"></a><span class="cm">/* ----------------------- WGL_ARB_make_current_read ----------------------- */</span>
<a name="wglew.h-278"></a>
<a name="wglew.h-279"></a><span class="cp">#ifndef WGL_ARB_make_current_read</span>
<a name="wglew.h-280"></a><span class="cp">#define WGL_ARB_make_current_read 1</span>
<a name="wglew.h-281"></a>
<a name="wglew.h-282"></a><span class="cp">#define ERROR_INVALID_PIXEL_TYPE_ARB 0x2043</span>
<a name="wglew.h-283"></a><span class="cp">#define ERROR_INCOMPATIBLE_DEVICE_CONTEXTS_ARB 0x2054</span>
<a name="wglew.h-284"></a>
<a name="wglew.h-285"></a><span class="k">typedef</span> <span class="nf">HDC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETCURRENTREADDCARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">VOID</span><span class="p">);</span>
<a name="wglew.h-286"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLMAKECONTEXTCURRENTARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDrawDC</span><span class="p">,</span> <span class="n">HDC</span> <span class="n">hReadDC</span><span class="p">,</span> <span class="n">HGLRC</span> <span class="n">hglrc</span><span class="p">);</span>
<a name="wglew.h-287"></a>
<a name="wglew.h-288"></a><span class="cp">#define wglGetCurrentReadDCARB WGLEW_GET_FUN(__wglewGetCurrentReadDCARB)</span>
<a name="wglew.h-289"></a><span class="cp">#define wglMakeContextCurrentARB WGLEW_GET_FUN(__wglewMakeContextCurrentARB)</span>
<a name="wglew.h-290"></a>
<a name="wglew.h-291"></a><span class="cp">#define WGLEW_ARB_make_current_read WGLEW_GET_VAR(__WGLEW_ARB_make_current_read)</span>
<a name="wglew.h-292"></a>
<a name="wglew.h-293"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_make_current_read */</span><span class="cp"></span>
<a name="wglew.h-294"></a>
<a name="wglew.h-295"></a><span class="cm">/* -------------------------- WGL_ARB_multisample -------------------------- */</span>
<a name="wglew.h-296"></a>
<a name="wglew.h-297"></a><span class="cp">#ifndef WGL_ARB_multisample</span>
<a name="wglew.h-298"></a><span class="cp">#define WGL_ARB_multisample 1</span>
<a name="wglew.h-299"></a>
<a name="wglew.h-300"></a><span class="cp">#define WGL_SAMPLE_BUFFERS_ARB 0x2041</span>
<a name="wglew.h-301"></a><span class="cp">#define WGL_SAMPLES_ARB 0x2042</span>
<a name="wglew.h-302"></a>
<a name="wglew.h-303"></a><span class="cp">#define WGLEW_ARB_multisample WGLEW_GET_VAR(__WGLEW_ARB_multisample)</span>
<a name="wglew.h-304"></a>
<a name="wglew.h-305"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_multisample */</span><span class="cp"></span>
<a name="wglew.h-306"></a>
<a name="wglew.h-307"></a><span class="cm">/* ---------------------------- WGL_ARB_pbuffer ---------------------------- */</span>
<a name="wglew.h-308"></a>
<a name="wglew.h-309"></a><span class="cp">#ifndef WGL_ARB_pbuffer</span>
<a name="wglew.h-310"></a><span class="cp">#define WGL_ARB_pbuffer 1</span>
<a name="wglew.h-311"></a>
<a name="wglew.h-312"></a><span class="cp">#define WGL_DRAW_TO_PBUFFER_ARB 0x202D</span>
<a name="wglew.h-313"></a><span class="cp">#define WGL_MAX_PBUFFER_PIXELS_ARB 0x202E</span>
<a name="wglew.h-314"></a><span class="cp">#define WGL_MAX_PBUFFER_WIDTH_ARB 0x202F</span>
<a name="wglew.h-315"></a><span class="cp">#define WGL_MAX_PBUFFER_HEIGHT_ARB 0x2030</span>
<a name="wglew.h-316"></a><span class="cp">#define WGL_PBUFFER_LARGEST_ARB 0x2033</span>
<a name="wglew.h-317"></a><span class="cp">#define WGL_PBUFFER_WIDTH_ARB 0x2034</span>
<a name="wglew.h-318"></a><span class="cp">#define WGL_PBUFFER_HEIGHT_ARB 0x2035</span>
<a name="wglew.h-319"></a><span class="cp">#define WGL_PBUFFER_LOST_ARB 0x2036</span>
<a name="wglew.h-320"></a>
<a name="wglew.h-321"></a><span class="n">DECLARE_HANDLE</span><span class="p">(</span><span class="n">HPBUFFERARB</span><span class="p">);</span>
<a name="wglew.h-322"></a>
<a name="wglew.h-323"></a><span class="k">typedef</span> <span class="nf">HPBUFFERARB</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEPBUFFERARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iPixelFormat</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iWidth</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iHeight</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttribList</span><span class="p">);</span>
<a name="wglew.h-324"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDESTROYPBUFFERARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">);</span>
<a name="wglew.h-325"></a><span class="k">typedef</span> <span class="nf">HDC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETPBUFFERDCARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">);</span>
<a name="wglew.h-326"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYPBUFFERARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-327"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRELEASEPBUFFERDCARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="n">HDC</span> <span class="n">hDC</span><span class="p">);</span>
<a name="wglew.h-328"></a>
<a name="wglew.h-329"></a><span class="cp">#define wglCreatePbufferARB WGLEW_GET_FUN(__wglewCreatePbufferARB)</span>
<a name="wglew.h-330"></a><span class="cp">#define wglDestroyPbufferARB WGLEW_GET_FUN(__wglewDestroyPbufferARB)</span>
<a name="wglew.h-331"></a><span class="cp">#define wglGetPbufferDCARB WGLEW_GET_FUN(__wglewGetPbufferDCARB)</span>
<a name="wglew.h-332"></a><span class="cp">#define wglQueryPbufferARB WGLEW_GET_FUN(__wglewQueryPbufferARB)</span>
<a name="wglew.h-333"></a><span class="cp">#define wglReleasePbufferDCARB WGLEW_GET_FUN(__wglewReleasePbufferDCARB)</span>
<a name="wglew.h-334"></a>
<a name="wglew.h-335"></a><span class="cp">#define WGLEW_ARB_pbuffer WGLEW_GET_VAR(__WGLEW_ARB_pbuffer)</span>
<a name="wglew.h-336"></a>
<a name="wglew.h-337"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_pbuffer */</span><span class="cp"></span>
<a name="wglew.h-338"></a>
<a name="wglew.h-339"></a><span class="cm">/* -------------------------- WGL_ARB_pixel_format ------------------------- */</span>
<a name="wglew.h-340"></a>
<a name="wglew.h-341"></a><span class="cp">#ifndef WGL_ARB_pixel_format</span>
<a name="wglew.h-342"></a><span class="cp">#define WGL_ARB_pixel_format 1</span>
<a name="wglew.h-343"></a>
<a name="wglew.h-344"></a><span class="cp">#define WGL_NUMBER_PIXEL_FORMATS_ARB 0x2000</span>
<a name="wglew.h-345"></a><span class="cp">#define WGL_DRAW_TO_WINDOW_ARB 0x2001</span>
<a name="wglew.h-346"></a><span class="cp">#define WGL_DRAW_TO_BITMAP_ARB 0x2002</span>
<a name="wglew.h-347"></a><span class="cp">#define WGL_ACCELERATION_ARB 0x2003</span>
<a name="wglew.h-348"></a><span class="cp">#define WGL_NEED_PALETTE_ARB 0x2004</span>
<a name="wglew.h-349"></a><span class="cp">#define WGL_NEED_SYSTEM_PALETTE_ARB 0x2005</span>
<a name="wglew.h-350"></a><span class="cp">#define WGL_SWAP_LAYER_BUFFERS_ARB 0x2006</span>
<a name="wglew.h-351"></a><span class="cp">#define WGL_SWAP_METHOD_ARB 0x2007</span>
<a name="wglew.h-352"></a><span class="cp">#define WGL_NUMBER_OVERLAYS_ARB 0x2008</span>
<a name="wglew.h-353"></a><span class="cp">#define WGL_NUMBER_UNDERLAYS_ARB 0x2009</span>
<a name="wglew.h-354"></a><span class="cp">#define WGL_TRANSPARENT_ARB 0x200A</span>
<a name="wglew.h-355"></a><span class="cp">#define WGL_SHARE_DEPTH_ARB 0x200C</span>
<a name="wglew.h-356"></a><span class="cp">#define WGL_SHARE_STENCIL_ARB 0x200D</span>
<a name="wglew.h-357"></a><span class="cp">#define WGL_SHARE_ACCUM_ARB 0x200E</span>
<a name="wglew.h-358"></a><span class="cp">#define WGL_SUPPORT_GDI_ARB 0x200F</span>
<a name="wglew.h-359"></a><span class="cp">#define WGL_SUPPORT_OPENGL_ARB 0x2010</span>
<a name="wglew.h-360"></a><span class="cp">#define WGL_DOUBLE_BUFFER_ARB 0x2011</span>
<a name="wglew.h-361"></a><span class="cp">#define WGL_STEREO_ARB 0x2012</span>
<a name="wglew.h-362"></a><span class="cp">#define WGL_PIXEL_TYPE_ARB 0x2013</span>
<a name="wglew.h-363"></a><span class="cp">#define WGL_COLOR_BITS_ARB 0x2014</span>
<a name="wglew.h-364"></a><span class="cp">#define WGL_RED_BITS_ARB 0x2015</span>
<a name="wglew.h-365"></a><span class="cp">#define WGL_RED_SHIFT_ARB 0x2016</span>
<a name="wglew.h-366"></a><span class="cp">#define WGL_GREEN_BITS_ARB 0x2017</span>
<a name="wglew.h-367"></a><span class="cp">#define WGL_GREEN_SHIFT_ARB 0x2018</span>
<a name="wglew.h-368"></a><span class="cp">#define WGL_BLUE_BITS_ARB 0x2019</span>
<a name="wglew.h-369"></a><span class="cp">#define WGL_BLUE_SHIFT_ARB 0x201A</span>
<a name="wglew.h-370"></a><span class="cp">#define WGL_ALPHA_BITS_ARB 0x201B</span>
<a name="wglew.h-371"></a><span class="cp">#define WGL_ALPHA_SHIFT_ARB 0x201C</span>
<a name="wglew.h-372"></a><span class="cp">#define WGL_ACCUM_BITS_ARB 0x201D</span>
<a name="wglew.h-373"></a><span class="cp">#define WGL_ACCUM_RED_BITS_ARB 0x201E</span>
<a name="wglew.h-374"></a><span class="cp">#define WGL_ACCUM_GREEN_BITS_ARB 0x201F</span>
<a name="wglew.h-375"></a><span class="cp">#define WGL_ACCUM_BLUE_BITS_ARB 0x2020</span>
<a name="wglew.h-376"></a><span class="cp">#define WGL_ACCUM_ALPHA_BITS_ARB 0x2021</span>
<a name="wglew.h-377"></a><span class="cp">#define WGL_DEPTH_BITS_ARB 0x2022</span>
<a name="wglew.h-378"></a><span class="cp">#define WGL_STENCIL_BITS_ARB 0x2023</span>
<a name="wglew.h-379"></a><span class="cp">#define WGL_AUX_BUFFERS_ARB 0x2024</span>
<a name="wglew.h-380"></a><span class="cp">#define WGL_NO_ACCELERATION_ARB 0x2025</span>
<a name="wglew.h-381"></a><span class="cp">#define WGL_GENERIC_ACCELERATION_ARB 0x2026</span>
<a name="wglew.h-382"></a><span class="cp">#define WGL_FULL_ACCELERATION_ARB 0x2027</span>
<a name="wglew.h-383"></a><span class="cp">#define WGL_SWAP_EXCHANGE_ARB 0x2028</span>
<a name="wglew.h-384"></a><span class="cp">#define WGL_SWAP_COPY_ARB 0x2029</span>
<a name="wglew.h-385"></a><span class="cp">#define WGL_SWAP_UNDEFINED_ARB 0x202A</span>
<a name="wglew.h-386"></a><span class="cp">#define WGL_TYPE_RGBA_ARB 0x202B</span>
<a name="wglew.h-387"></a><span class="cp">#define WGL_TYPE_COLORINDEX_ARB 0x202C</span>
<a name="wglew.h-388"></a><span class="cp">#define WGL_TRANSPARENT_RED_VALUE_ARB 0x2037</span>
<a name="wglew.h-389"></a><span class="cp">#define WGL_TRANSPARENT_GREEN_VALUE_ARB 0x2038</span>
<a name="wglew.h-390"></a><span class="cp">#define WGL_TRANSPARENT_BLUE_VALUE_ARB 0x2039</span>
<a name="wglew.h-391"></a><span class="cp">#define WGL_TRANSPARENT_ALPHA_VALUE_ARB 0x203A</span>
<a name="wglew.h-392"></a><span class="cp">#define WGL_TRANSPARENT_INDEX_VALUE_ARB 0x203B</span>
<a name="wglew.h-393"></a>
<a name="wglew.h-394"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCHOOSEPIXELFORMATARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttribIList</span><span class="p">,</span> <span class="k">const</span> <span class="n">FLOAT</span> <span class="o">*</span><span class="n">pfAttribFList</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">nMaxFormats</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">piFormats</span><span class="p">,</span> <span class="n">UINT</span> <span class="o">*</span><span class="n">nNumFormats</span><span class="p">);</span>
<a name="wglew.h-395"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBFVARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iPixelFormat</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iLayerPlane</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">nAttributes</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttributes</span><span class="p">,</span> <span class="n">FLOAT</span> <span class="o">*</span><span class="n">pfValues</span><span class="p">);</span>
<a name="wglew.h-396"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBIVARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iPixelFormat</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iLayerPlane</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">nAttributes</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttributes</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">piValues</span><span class="p">);</span>
<a name="wglew.h-397"></a>
<a name="wglew.h-398"></a><span class="cp">#define wglChoosePixelFormatARB WGLEW_GET_FUN(__wglewChoosePixelFormatARB)</span>
<a name="wglew.h-399"></a><span class="cp">#define wglGetPixelFormatAttribfvARB WGLEW_GET_FUN(__wglewGetPixelFormatAttribfvARB)</span>
<a name="wglew.h-400"></a><span class="cp">#define wglGetPixelFormatAttribivARB WGLEW_GET_FUN(__wglewGetPixelFormatAttribivARB)</span>
<a name="wglew.h-401"></a>
<a name="wglew.h-402"></a><span class="cp">#define WGLEW_ARB_pixel_format WGLEW_GET_VAR(__WGLEW_ARB_pixel_format)</span>
<a name="wglew.h-403"></a>
<a name="wglew.h-404"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_pixel_format */</span><span class="cp"></span>
<a name="wglew.h-405"></a>
<a name="wglew.h-406"></a><span class="cm">/* ----------------------- WGL_ARB_pixel_format_float ---------------------- */</span>
<a name="wglew.h-407"></a>
<a name="wglew.h-408"></a><span class="cp">#ifndef WGL_ARB_pixel_format_float</span>
<a name="wglew.h-409"></a><span class="cp">#define WGL_ARB_pixel_format_float 1</span>
<a name="wglew.h-410"></a>
<a name="wglew.h-411"></a><span class="cp">#define WGL_TYPE_RGBA_FLOAT_ARB 0x21A0</span>
<a name="wglew.h-412"></a>
<a name="wglew.h-413"></a><span class="cp">#define WGLEW_ARB_pixel_format_float WGLEW_GET_VAR(__WGLEW_ARB_pixel_format_float)</span>
<a name="wglew.h-414"></a>
<a name="wglew.h-415"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_pixel_format_float */</span><span class="cp"></span>
<a name="wglew.h-416"></a>
<a name="wglew.h-417"></a><span class="cm">/* ------------------------- WGL_ARB_render_texture ------------------------ */</span>
<a name="wglew.h-418"></a>
<a name="wglew.h-419"></a><span class="cp">#ifndef WGL_ARB_render_texture</span>
<a name="wglew.h-420"></a><span class="cp">#define WGL_ARB_render_texture 1</span>
<a name="wglew.h-421"></a>
<a name="wglew.h-422"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RGB_ARB 0x2070</span>
<a name="wglew.h-423"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RGBA_ARB 0x2071</span>
<a name="wglew.h-424"></a><span class="cp">#define WGL_TEXTURE_FORMAT_ARB 0x2072</span>
<a name="wglew.h-425"></a><span class="cp">#define WGL_TEXTURE_TARGET_ARB 0x2073</span>
<a name="wglew.h-426"></a><span class="cp">#define WGL_MIPMAP_TEXTURE_ARB 0x2074</span>
<a name="wglew.h-427"></a><span class="cp">#define WGL_TEXTURE_RGB_ARB 0x2075</span>
<a name="wglew.h-428"></a><span class="cp">#define WGL_TEXTURE_RGBA_ARB 0x2076</span>
<a name="wglew.h-429"></a><span class="cp">#define WGL_NO_TEXTURE_ARB 0x2077</span>
<a name="wglew.h-430"></a><span class="cp">#define WGL_TEXTURE_CUBE_MAP_ARB 0x2078</span>
<a name="wglew.h-431"></a><span class="cp">#define WGL_TEXTURE_1D_ARB 0x2079</span>
<a name="wglew.h-432"></a><span class="cp">#define WGL_TEXTURE_2D_ARB 0x207A</span>
<a name="wglew.h-433"></a><span class="cp">#define WGL_MIPMAP_LEVEL_ARB 0x207B</span>
<a name="wglew.h-434"></a><span class="cp">#define WGL_CUBE_MAP_FACE_ARB 0x207C</span>
<a name="wglew.h-435"></a><span class="cp">#define WGL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB 0x207D</span>
<a name="wglew.h-436"></a><span class="cp">#define WGL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB 0x207E</span>
<a name="wglew.h-437"></a><span class="cp">#define WGL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB 0x207F</span>
<a name="wglew.h-438"></a><span class="cp">#define WGL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB 0x2080</span>
<a name="wglew.h-439"></a><span class="cp">#define WGL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB 0x2081</span>
<a name="wglew.h-440"></a><span class="cp">#define WGL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB 0x2082</span>
<a name="wglew.h-441"></a><span class="cp">#define WGL_FRONT_LEFT_ARB 0x2083</span>
<a name="wglew.h-442"></a><span class="cp">#define WGL_FRONT_RIGHT_ARB 0x2084</span>
<a name="wglew.h-443"></a><span class="cp">#define WGL_BACK_LEFT_ARB 0x2085</span>
<a name="wglew.h-444"></a><span class="cp">#define WGL_BACK_RIGHT_ARB 0x2086</span>
<a name="wglew.h-445"></a><span class="cp">#define WGL_AUX0_ARB 0x2087</span>
<a name="wglew.h-446"></a><span class="cp">#define WGL_AUX1_ARB 0x2088</span>
<a name="wglew.h-447"></a><span class="cp">#define WGL_AUX2_ARB 0x2089</span>
<a name="wglew.h-448"></a><span class="cp">#define WGL_AUX3_ARB 0x208A</span>
<a name="wglew.h-449"></a><span class="cp">#define WGL_AUX4_ARB 0x208B</span>
<a name="wglew.h-450"></a><span class="cp">#define WGL_AUX5_ARB 0x208C</span>
<a name="wglew.h-451"></a><span class="cp">#define WGL_AUX6_ARB 0x208D</span>
<a name="wglew.h-452"></a><span class="cp">#define WGL_AUX7_ARB 0x208E</span>
<a name="wglew.h-453"></a><span class="cp">#define WGL_AUX8_ARB 0x208F</span>
<a name="wglew.h-454"></a><span class="cp">#define WGL_AUX9_ARB 0x2090</span>
<a name="wglew.h-455"></a>
<a name="wglew.h-456"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBINDTEXIMAGEARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iBuffer</span><span class="p">);</span>
<a name="wglew.h-457"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRELEASETEXIMAGEARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iBuffer</span><span class="p">);</span>
<a name="wglew.h-458"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSETPBUFFERATTRIBARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttribList</span><span class="p">);</span>
<a name="wglew.h-459"></a>
<a name="wglew.h-460"></a><span class="cp">#define wglBindTexImageARB WGLEW_GET_FUN(__wglewBindTexImageARB)</span>
<a name="wglew.h-461"></a><span class="cp">#define wglReleaseTexImageARB WGLEW_GET_FUN(__wglewReleaseTexImageARB)</span>
<a name="wglew.h-462"></a><span class="cp">#define wglSetPbufferAttribARB WGLEW_GET_FUN(__wglewSetPbufferAttribARB)</span>
<a name="wglew.h-463"></a>
<a name="wglew.h-464"></a><span class="cp">#define WGLEW_ARB_render_texture WGLEW_GET_VAR(__WGLEW_ARB_render_texture)</span>
<a name="wglew.h-465"></a>
<a name="wglew.h-466"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_render_texture */</span><span class="cp"></span>
<a name="wglew.h-467"></a>
<a name="wglew.h-468"></a><span class="cm">/* ---------------- WGL_ARB_robustness_application_isolation --------------- */</span>
<a name="wglew.h-469"></a>
<a name="wglew.h-470"></a><span class="cp">#ifndef WGL_ARB_robustness_application_isolation</span>
<a name="wglew.h-471"></a><span class="cp">#define WGL_ARB_robustness_application_isolation 1</span>
<a name="wglew.h-472"></a>
<a name="wglew.h-473"></a><span class="cp">#define WGL_CONTEXT_RESET_ISOLATION_BIT_ARB 0x00000008</span>
<a name="wglew.h-474"></a>
<a name="wglew.h-475"></a><span class="cp">#define WGLEW_ARB_robustness_application_isolation WGLEW_GET_VAR(__WGLEW_ARB_robustness_application_isolation)</span>
<a name="wglew.h-476"></a>
<a name="wglew.h-477"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_robustness_application_isolation */</span><span class="cp"></span>
<a name="wglew.h-478"></a>
<a name="wglew.h-479"></a><span class="cm">/* ---------------- WGL_ARB_robustness_share_group_isolation --------------- */</span>
<a name="wglew.h-480"></a>
<a name="wglew.h-481"></a><span class="cp">#ifndef WGL_ARB_robustness_share_group_isolation</span>
<a name="wglew.h-482"></a><span class="cp">#define WGL_ARB_robustness_share_group_isolation 1</span>
<a name="wglew.h-483"></a>
<a name="wglew.h-484"></a><span class="cp">#define WGL_CONTEXT_RESET_ISOLATION_BIT_ARB 0x00000008</span>
<a name="wglew.h-485"></a>
<a name="wglew.h-486"></a><span class="cp">#define WGLEW_ARB_robustness_share_group_isolation WGLEW_GET_VAR(__WGLEW_ARB_robustness_share_group_isolation)</span>
<a name="wglew.h-487"></a>
<a name="wglew.h-488"></a><span class="cp">#endif </span><span class="cm">/* WGL_ARB_robustness_share_group_isolation */</span><span class="cp"></span>
<a name="wglew.h-489"></a>
<a name="wglew.h-490"></a><span class="cm">/* ----------------------- WGL_ATI_pixel_format_float ---------------------- */</span>
<a name="wglew.h-491"></a>
<a name="wglew.h-492"></a><span class="cp">#ifndef WGL_ATI_pixel_format_float</span>
<a name="wglew.h-493"></a><span class="cp">#define WGL_ATI_pixel_format_float 1</span>
<a name="wglew.h-494"></a>
<a name="wglew.h-495"></a><span class="cp">#define WGL_TYPE_RGBA_FLOAT_ATI 0x21A0</span>
<a name="wglew.h-496"></a><span class="cp">#define GL_RGBA_FLOAT_MODE_ATI 0x8820</span>
<a name="wglew.h-497"></a><span class="cp">#define GL_COLOR_CLEAR_UNCLAMPED_VALUE_ATI 0x8835</span>
<a name="wglew.h-498"></a>
<a name="wglew.h-499"></a><span class="cp">#define WGLEW_ATI_pixel_format_float WGLEW_GET_VAR(__WGLEW_ATI_pixel_format_float)</span>
<a name="wglew.h-500"></a>
<a name="wglew.h-501"></a><span class="cp">#endif </span><span class="cm">/* WGL_ATI_pixel_format_float */</span><span class="cp"></span>
<a name="wglew.h-502"></a>
<a name="wglew.h-503"></a><span class="cm">/* -------------------- WGL_ATI_render_texture_rectangle ------------------- */</span>
<a name="wglew.h-504"></a>
<a name="wglew.h-505"></a><span class="cp">#ifndef WGL_ATI_render_texture_rectangle</span>
<a name="wglew.h-506"></a><span class="cp">#define WGL_ATI_render_texture_rectangle 1</span>
<a name="wglew.h-507"></a>
<a name="wglew.h-508"></a><span class="cp">#define WGL_TEXTURE_RECTANGLE_ATI 0x21A5</span>
<a name="wglew.h-509"></a>
<a name="wglew.h-510"></a><span class="cp">#define WGLEW_ATI_render_texture_rectangle WGLEW_GET_VAR(__WGLEW_ATI_render_texture_rectangle)</span>
<a name="wglew.h-511"></a>
<a name="wglew.h-512"></a><span class="cp">#endif </span><span class="cm">/* WGL_ATI_render_texture_rectangle */</span><span class="cp"></span>
<a name="wglew.h-513"></a>
<a name="wglew.h-514"></a><span class="cm">/* --------------------------- WGL_EXT_colorspace -------------------------- */</span>
<a name="wglew.h-515"></a>
<a name="wglew.h-516"></a><span class="cp">#ifndef WGL_EXT_colorspace</span>
<a name="wglew.h-517"></a><span class="cp">#define WGL_EXT_colorspace 1</span>
<a name="wglew.h-518"></a>
<a name="wglew.h-519"></a><span class="cp">#define WGL_COLORSPACE_SRGB_EXT 0x3089</span>
<a name="wglew.h-520"></a><span class="cp">#define WGL_COLORSPACE_LINEAR_EXT 0x308A</span>
<a name="wglew.h-521"></a><span class="cp">#define WGL_COLORSPACE_EXT 0x309D</span>
<a name="wglew.h-522"></a>
<a name="wglew.h-523"></a><span class="cp">#define WGLEW_EXT_colorspace WGLEW_GET_VAR(__WGLEW_EXT_colorspace)</span>
<a name="wglew.h-524"></a>
<a name="wglew.h-525"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_colorspace */</span><span class="cp"></span>
<a name="wglew.h-526"></a>
<a name="wglew.h-527"></a><span class="cm">/* ------------------- WGL_EXT_create_context_es2_profile ------------------ */</span>
<a name="wglew.h-528"></a>
<a name="wglew.h-529"></a><span class="cp">#ifndef WGL_EXT_create_context_es2_profile</span>
<a name="wglew.h-530"></a><span class="cp">#define WGL_EXT_create_context_es2_profile 1</span>
<a name="wglew.h-531"></a>
<a name="wglew.h-532"></a><span class="cp">#define WGL_CONTEXT_ES2_PROFILE_BIT_EXT 0x00000004</span>
<a name="wglew.h-533"></a>
<a name="wglew.h-534"></a><span class="cp">#define WGLEW_EXT_create_context_es2_profile WGLEW_GET_VAR(__WGLEW_EXT_create_context_es2_profile)</span>
<a name="wglew.h-535"></a>
<a name="wglew.h-536"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_create_context_es2_profile */</span><span class="cp"></span>
<a name="wglew.h-537"></a>
<a name="wglew.h-538"></a><span class="cm">/* ------------------- WGL_EXT_create_context_es_profile ------------------- */</span>
<a name="wglew.h-539"></a>
<a name="wglew.h-540"></a><span class="cp">#ifndef WGL_EXT_create_context_es_profile</span>
<a name="wglew.h-541"></a><span class="cp">#define WGL_EXT_create_context_es_profile 1</span>
<a name="wglew.h-542"></a>
<a name="wglew.h-543"></a><span class="cp">#define WGL_CONTEXT_ES_PROFILE_BIT_EXT 0x00000004</span>
<a name="wglew.h-544"></a>
<a name="wglew.h-545"></a><span class="cp">#define WGLEW_EXT_create_context_es_profile WGLEW_GET_VAR(__WGLEW_EXT_create_context_es_profile)</span>
<a name="wglew.h-546"></a>
<a name="wglew.h-547"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_create_context_es_profile */</span><span class="cp"></span>
<a name="wglew.h-548"></a>
<a name="wglew.h-549"></a><span class="cm">/* -------------------------- WGL_EXT_depth_float -------------------------- */</span>
<a name="wglew.h-550"></a>
<a name="wglew.h-551"></a><span class="cp">#ifndef WGL_EXT_depth_float</span>
<a name="wglew.h-552"></a><span class="cp">#define WGL_EXT_depth_float 1</span>
<a name="wglew.h-553"></a>
<a name="wglew.h-554"></a><span class="cp">#define WGL_DEPTH_FLOAT_EXT 0x2040</span>
<a name="wglew.h-555"></a>
<a name="wglew.h-556"></a><span class="cp">#define WGLEW_EXT_depth_float WGLEW_GET_VAR(__WGLEW_EXT_depth_float)</span>
<a name="wglew.h-557"></a>
<a name="wglew.h-558"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_depth_float */</span><span class="cp"></span>
<a name="wglew.h-559"></a>
<a name="wglew.h-560"></a><span class="cm">/* ---------------------- WGL_EXT_display_color_table ---------------------- */</span>
<a name="wglew.h-561"></a>
<a name="wglew.h-562"></a><span class="cp">#ifndef WGL_EXT_display_color_table</span>
<a name="wglew.h-563"></a><span class="cp">#define WGL_EXT_display_color_table 1</span>
<a name="wglew.h-564"></a>
<a name="wglew.h-565"></a><span class="k">typedef</span> <span class="nf">GLboolean</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBINDDISPLAYCOLORTABLEEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLushort</span> <span class="n">id</span><span class="p">);</span>
<a name="wglew.h-566"></a><span class="k">typedef</span> <span class="nf">GLboolean</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEDISPLAYCOLORTABLEEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLushort</span> <span class="n">id</span><span class="p">);</span>
<a name="wglew.h-567"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDESTROYDISPLAYCOLORTABLEEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLushort</span> <span class="n">id</span><span class="p">);</span>
<a name="wglew.h-568"></a><span class="k">typedef</span> <span class="nf">GLboolean</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLLOADDISPLAYCOLORTABLEEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLushort</span><span class="o">*</span> <span class="n">table</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">length</span><span class="p">);</span>
<a name="wglew.h-569"></a>
<a name="wglew.h-570"></a><span class="cp">#define wglBindDisplayColorTableEXT WGLEW_GET_FUN(__wglewBindDisplayColorTableEXT)</span>
<a name="wglew.h-571"></a><span class="cp">#define wglCreateDisplayColorTableEXT WGLEW_GET_FUN(__wglewCreateDisplayColorTableEXT)</span>
<a name="wglew.h-572"></a><span class="cp">#define wglDestroyDisplayColorTableEXT WGLEW_GET_FUN(__wglewDestroyDisplayColorTableEXT)</span>
<a name="wglew.h-573"></a><span class="cp">#define wglLoadDisplayColorTableEXT WGLEW_GET_FUN(__wglewLoadDisplayColorTableEXT)</span>
<a name="wglew.h-574"></a>
<a name="wglew.h-575"></a><span class="cp">#define WGLEW_EXT_display_color_table WGLEW_GET_VAR(__WGLEW_EXT_display_color_table)</span>
<a name="wglew.h-576"></a>
<a name="wglew.h-577"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_display_color_table */</span><span class="cp"></span>
<a name="wglew.h-578"></a>
<a name="wglew.h-579"></a><span class="cm">/* ----------------------- WGL_EXT_extensions_string ----------------------- */</span>
<a name="wglew.h-580"></a>
<a name="wglew.h-581"></a><span class="cp">#ifndef WGL_EXT_extensions_string</span>
<a name="wglew.h-582"></a><span class="cp">#define WGL_EXT_extensions_string 1</span>
<a name="wglew.h-583"></a>
<a name="wglew.h-584"></a><span class="k">typedef</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETEXTENSIONSSTRINGEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="wglew.h-585"></a>
<a name="wglew.h-586"></a><span class="cp">#define wglGetExtensionsStringEXT WGLEW_GET_FUN(__wglewGetExtensionsStringEXT)</span>
<a name="wglew.h-587"></a>
<a name="wglew.h-588"></a><span class="cp">#define WGLEW_EXT_extensions_string WGLEW_GET_VAR(__WGLEW_EXT_extensions_string)</span>
<a name="wglew.h-589"></a>
<a name="wglew.h-590"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_extensions_string */</span><span class="cp"></span>
<a name="wglew.h-591"></a>
<a name="wglew.h-592"></a><span class="cm">/* ------------------------ WGL_EXT_framebuffer_sRGB ----------------------- */</span>
<a name="wglew.h-593"></a>
<a name="wglew.h-594"></a><span class="cp">#ifndef WGL_EXT_framebuffer_sRGB</span>
<a name="wglew.h-595"></a><span class="cp">#define WGL_EXT_framebuffer_sRGB 1</span>
<a name="wglew.h-596"></a>
<a name="wglew.h-597"></a><span class="cp">#define WGL_FRAMEBUFFER_SRGB_CAPABLE_EXT 0x20A9</span>
<a name="wglew.h-598"></a>
<a name="wglew.h-599"></a><span class="cp">#define WGLEW_EXT_framebuffer_sRGB WGLEW_GET_VAR(__WGLEW_EXT_framebuffer_sRGB)</span>
<a name="wglew.h-600"></a>
<a name="wglew.h-601"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_framebuffer_sRGB */</span><span class="cp"></span>
<a name="wglew.h-602"></a>
<a name="wglew.h-603"></a><span class="cm">/* ----------------------- WGL_EXT_make_current_read ----------------------- */</span>
<a name="wglew.h-604"></a>
<a name="wglew.h-605"></a><span class="cp">#ifndef WGL_EXT_make_current_read</span>
<a name="wglew.h-606"></a><span class="cp">#define WGL_EXT_make_current_read 1</span>
<a name="wglew.h-607"></a>
<a name="wglew.h-608"></a><span class="cp">#define ERROR_INVALID_PIXEL_TYPE_EXT 0x2043</span>
<a name="wglew.h-609"></a>
<a name="wglew.h-610"></a><span class="k">typedef</span> <span class="nf">HDC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETCURRENTREADDCEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">VOID</span><span class="p">);</span>
<a name="wglew.h-611"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLMAKECONTEXTCURRENTEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDrawDC</span><span class="p">,</span> <span class="n">HDC</span> <span class="n">hReadDC</span><span class="p">,</span> <span class="n">HGLRC</span> <span class="n">hglrc</span><span class="p">);</span>
<a name="wglew.h-612"></a>
<a name="wglew.h-613"></a><span class="cp">#define wglGetCurrentReadDCEXT WGLEW_GET_FUN(__wglewGetCurrentReadDCEXT)</span>
<a name="wglew.h-614"></a><span class="cp">#define wglMakeContextCurrentEXT WGLEW_GET_FUN(__wglewMakeContextCurrentEXT)</span>
<a name="wglew.h-615"></a>
<a name="wglew.h-616"></a><span class="cp">#define WGLEW_EXT_make_current_read WGLEW_GET_VAR(__WGLEW_EXT_make_current_read)</span>
<a name="wglew.h-617"></a>
<a name="wglew.h-618"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_make_current_read */</span><span class="cp"></span>
<a name="wglew.h-619"></a>
<a name="wglew.h-620"></a><span class="cm">/* -------------------------- WGL_EXT_multisample -------------------------- */</span>
<a name="wglew.h-621"></a>
<a name="wglew.h-622"></a><span class="cp">#ifndef WGL_EXT_multisample</span>
<a name="wglew.h-623"></a><span class="cp">#define WGL_EXT_multisample 1</span>
<a name="wglew.h-624"></a>
<a name="wglew.h-625"></a><span class="cp">#define WGL_SAMPLE_BUFFERS_EXT 0x2041</span>
<a name="wglew.h-626"></a><span class="cp">#define WGL_SAMPLES_EXT 0x2042</span>
<a name="wglew.h-627"></a>
<a name="wglew.h-628"></a><span class="cp">#define WGLEW_EXT_multisample WGLEW_GET_VAR(__WGLEW_EXT_multisample)</span>
<a name="wglew.h-629"></a>
<a name="wglew.h-630"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_multisample */</span><span class="cp"></span>
<a name="wglew.h-631"></a>
<a name="wglew.h-632"></a><span class="cm">/* ---------------------------- WGL_EXT_pbuffer ---------------------------- */</span>
<a name="wglew.h-633"></a>
<a name="wglew.h-634"></a><span class="cp">#ifndef WGL_EXT_pbuffer</span>
<a name="wglew.h-635"></a><span class="cp">#define WGL_EXT_pbuffer 1</span>
<a name="wglew.h-636"></a>
<a name="wglew.h-637"></a><span class="cp">#define WGL_DRAW_TO_PBUFFER_EXT 0x202D</span>
<a name="wglew.h-638"></a><span class="cp">#define WGL_MAX_PBUFFER_PIXELS_EXT 0x202E</span>
<a name="wglew.h-639"></a><span class="cp">#define WGL_MAX_PBUFFER_WIDTH_EXT 0x202F</span>
<a name="wglew.h-640"></a><span class="cp">#define WGL_MAX_PBUFFER_HEIGHT_EXT 0x2030</span>
<a name="wglew.h-641"></a><span class="cp">#define WGL_OPTIMAL_PBUFFER_WIDTH_EXT 0x2031</span>
<a name="wglew.h-642"></a><span class="cp">#define WGL_OPTIMAL_PBUFFER_HEIGHT_EXT 0x2032</span>
<a name="wglew.h-643"></a><span class="cp">#define WGL_PBUFFER_LARGEST_EXT 0x2033</span>
<a name="wglew.h-644"></a><span class="cp">#define WGL_PBUFFER_WIDTH_EXT 0x2034</span>
<a name="wglew.h-645"></a><span class="cp">#define WGL_PBUFFER_HEIGHT_EXT 0x2035</span>
<a name="wglew.h-646"></a>
<a name="wglew.h-647"></a><span class="n">DECLARE_HANDLE</span><span class="p">(</span><span class="n">HPBUFFEREXT</span><span class="p">);</span>
<a name="wglew.h-648"></a>
<a name="wglew.h-649"></a><span class="k">typedef</span> <span class="nf">HPBUFFEREXT</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEPBUFFEREXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iPixelFormat</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iWidth</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iHeight</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttribList</span><span class="p">);</span>
<a name="wglew.h-650"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDESTROYPBUFFEREXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFEREXT</span> <span class="n">hPbuffer</span><span class="p">);</span>
<a name="wglew.h-651"></a><span class="k">typedef</span> <span class="nf">HDC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETPBUFFERDCEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFEREXT</span> <span class="n">hPbuffer</span><span class="p">);</span>
<a name="wglew.h-652"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYPBUFFEREXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFEREXT</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-653"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRELEASEPBUFFERDCEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFEREXT</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="n">HDC</span> <span class="n">hDC</span><span class="p">);</span>
<a name="wglew.h-654"></a>
<a name="wglew.h-655"></a><span class="cp">#define wglCreatePbufferEXT WGLEW_GET_FUN(__wglewCreatePbufferEXT)</span>
<a name="wglew.h-656"></a><span class="cp">#define wglDestroyPbufferEXT WGLEW_GET_FUN(__wglewDestroyPbufferEXT)</span>
<a name="wglew.h-657"></a><span class="cp">#define wglGetPbufferDCEXT WGLEW_GET_FUN(__wglewGetPbufferDCEXT)</span>
<a name="wglew.h-658"></a><span class="cp">#define wglQueryPbufferEXT WGLEW_GET_FUN(__wglewQueryPbufferEXT)</span>
<a name="wglew.h-659"></a><span class="cp">#define wglReleasePbufferDCEXT WGLEW_GET_FUN(__wglewReleasePbufferDCEXT)</span>
<a name="wglew.h-660"></a>
<a name="wglew.h-661"></a><span class="cp">#define WGLEW_EXT_pbuffer WGLEW_GET_VAR(__WGLEW_EXT_pbuffer)</span>
<a name="wglew.h-662"></a>
<a name="wglew.h-663"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_pbuffer */</span><span class="cp"></span>
<a name="wglew.h-664"></a>
<a name="wglew.h-665"></a><span class="cm">/* -------------------------- WGL_EXT_pixel_format ------------------------- */</span>
<a name="wglew.h-666"></a>
<a name="wglew.h-667"></a><span class="cp">#ifndef WGL_EXT_pixel_format</span>
<a name="wglew.h-668"></a><span class="cp">#define WGL_EXT_pixel_format 1</span>
<a name="wglew.h-669"></a>
<a name="wglew.h-670"></a><span class="cp">#define WGL_NUMBER_PIXEL_FORMATS_EXT 0x2000</span>
<a name="wglew.h-671"></a><span class="cp">#define WGL_DRAW_TO_WINDOW_EXT 0x2001</span>
<a name="wglew.h-672"></a><span class="cp">#define WGL_DRAW_TO_BITMAP_EXT 0x2002</span>
<a name="wglew.h-673"></a><span class="cp">#define WGL_ACCELERATION_EXT 0x2003</span>
<a name="wglew.h-674"></a><span class="cp">#define WGL_NEED_PALETTE_EXT 0x2004</span>
<a name="wglew.h-675"></a><span class="cp">#define WGL_NEED_SYSTEM_PALETTE_EXT 0x2005</span>
<a name="wglew.h-676"></a><span class="cp">#define WGL_SWAP_LAYER_BUFFERS_EXT 0x2006</span>
<a name="wglew.h-677"></a><span class="cp">#define WGL_SWAP_METHOD_EXT 0x2007</span>
<a name="wglew.h-678"></a><span class="cp">#define WGL_NUMBER_OVERLAYS_EXT 0x2008</span>
<a name="wglew.h-679"></a><span class="cp">#define WGL_NUMBER_UNDERLAYS_EXT 0x2009</span>
<a name="wglew.h-680"></a><span class="cp">#define WGL_TRANSPARENT_EXT 0x200A</span>
<a name="wglew.h-681"></a><span class="cp">#define WGL_TRANSPARENT_VALUE_EXT 0x200B</span>
<a name="wglew.h-682"></a><span class="cp">#define WGL_SHARE_DEPTH_EXT 0x200C</span>
<a name="wglew.h-683"></a><span class="cp">#define WGL_SHARE_STENCIL_EXT 0x200D</span>
<a name="wglew.h-684"></a><span class="cp">#define WGL_SHARE_ACCUM_EXT 0x200E</span>
<a name="wglew.h-685"></a><span class="cp">#define WGL_SUPPORT_GDI_EXT 0x200F</span>
<a name="wglew.h-686"></a><span class="cp">#define WGL_SUPPORT_OPENGL_EXT 0x2010</span>
<a name="wglew.h-687"></a><span class="cp">#define WGL_DOUBLE_BUFFER_EXT 0x2011</span>
<a name="wglew.h-688"></a><span class="cp">#define WGL_STEREO_EXT 0x2012</span>
<a name="wglew.h-689"></a><span class="cp">#define WGL_PIXEL_TYPE_EXT 0x2013</span>
<a name="wglew.h-690"></a><span class="cp">#define WGL_COLOR_BITS_EXT 0x2014</span>
<a name="wglew.h-691"></a><span class="cp">#define WGL_RED_BITS_EXT 0x2015</span>
<a name="wglew.h-692"></a><span class="cp">#define WGL_RED_SHIFT_EXT 0x2016</span>
<a name="wglew.h-693"></a><span class="cp">#define WGL_GREEN_BITS_EXT 0x2017</span>
<a name="wglew.h-694"></a><span class="cp">#define WGL_GREEN_SHIFT_EXT 0x2018</span>
<a name="wglew.h-695"></a><span class="cp">#define WGL_BLUE_BITS_EXT 0x2019</span>
<a name="wglew.h-696"></a><span class="cp">#define WGL_BLUE_SHIFT_EXT 0x201A</span>
<a name="wglew.h-697"></a><span class="cp">#define WGL_ALPHA_BITS_EXT 0x201B</span>
<a name="wglew.h-698"></a><span class="cp">#define WGL_ALPHA_SHIFT_EXT 0x201C</span>
<a name="wglew.h-699"></a><span class="cp">#define WGL_ACCUM_BITS_EXT 0x201D</span>
<a name="wglew.h-700"></a><span class="cp">#define WGL_ACCUM_RED_BITS_EXT 0x201E</span>
<a name="wglew.h-701"></a><span class="cp">#define WGL_ACCUM_GREEN_BITS_EXT 0x201F</span>
<a name="wglew.h-702"></a><span class="cp">#define WGL_ACCUM_BLUE_BITS_EXT 0x2020</span>
<a name="wglew.h-703"></a><span class="cp">#define WGL_ACCUM_ALPHA_BITS_EXT 0x2021</span>
<a name="wglew.h-704"></a><span class="cp">#define WGL_DEPTH_BITS_EXT 0x2022</span>
<a name="wglew.h-705"></a><span class="cp">#define WGL_STENCIL_BITS_EXT 0x2023</span>
<a name="wglew.h-706"></a><span class="cp">#define WGL_AUX_BUFFERS_EXT 0x2024</span>
<a name="wglew.h-707"></a><span class="cp">#define WGL_NO_ACCELERATION_EXT 0x2025</span>
<a name="wglew.h-708"></a><span class="cp">#define WGL_GENERIC_ACCELERATION_EXT 0x2026</span>
<a name="wglew.h-709"></a><span class="cp">#define WGL_FULL_ACCELERATION_EXT 0x2027</span>
<a name="wglew.h-710"></a><span class="cp">#define WGL_SWAP_EXCHANGE_EXT 0x2028</span>
<a name="wglew.h-711"></a><span class="cp">#define WGL_SWAP_COPY_EXT 0x2029</span>
<a name="wglew.h-712"></a><span class="cp">#define WGL_SWAP_UNDEFINED_EXT 0x202A</span>
<a name="wglew.h-713"></a><span class="cp">#define WGL_TYPE_RGBA_EXT 0x202B</span>
<a name="wglew.h-714"></a><span class="cp">#define WGL_TYPE_COLORINDEX_EXT 0x202C</span>
<a name="wglew.h-715"></a>
<a name="wglew.h-716"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCHOOSEPIXELFORMATEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttribIList</span><span class="p">,</span> <span class="k">const</span> <span class="n">FLOAT</span> <span class="o">*</span><span class="n">pfAttribFList</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">nMaxFormats</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">piFormats</span><span class="p">,</span> <span class="n">UINT</span> <span class="o">*</span><span class="n">nNumFormats</span><span class="p">);</span>
<a name="wglew.h-717"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBFVEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iPixelFormat</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iLayerPlane</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">nAttributes</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttributes</span><span class="p">,</span> <span class="n">FLOAT</span> <span class="o">*</span><span class="n">pfValues</span><span class="p">);</span>
<a name="wglew.h-718"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBIVEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iPixelFormat</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iLayerPlane</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">nAttributes</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttributes</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">piValues</span><span class="p">);</span>
<a name="wglew.h-719"></a>
<a name="wglew.h-720"></a><span class="cp">#define wglChoosePixelFormatEXT WGLEW_GET_FUN(__wglewChoosePixelFormatEXT)</span>
<a name="wglew.h-721"></a><span class="cp">#define wglGetPixelFormatAttribfvEXT WGLEW_GET_FUN(__wglewGetPixelFormatAttribfvEXT)</span>
<a name="wglew.h-722"></a><span class="cp">#define wglGetPixelFormatAttribivEXT WGLEW_GET_FUN(__wglewGetPixelFormatAttribivEXT)</span>
<a name="wglew.h-723"></a>
<a name="wglew.h-724"></a><span class="cp">#define WGLEW_EXT_pixel_format WGLEW_GET_VAR(__WGLEW_EXT_pixel_format)</span>
<a name="wglew.h-725"></a>
<a name="wglew.h-726"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_pixel_format */</span><span class="cp"></span>
<a name="wglew.h-727"></a>
<a name="wglew.h-728"></a><span class="cm">/* ------------------- WGL_EXT_pixel_format_packed_float ------------------- */</span>
<a name="wglew.h-729"></a>
<a name="wglew.h-730"></a><span class="cp">#ifndef WGL_EXT_pixel_format_packed_float</span>
<a name="wglew.h-731"></a><span class="cp">#define WGL_EXT_pixel_format_packed_float 1</span>
<a name="wglew.h-732"></a>
<a name="wglew.h-733"></a><span class="cp">#define WGL_TYPE_RGBA_UNSIGNED_FLOAT_EXT 0x20A8</span>
<a name="wglew.h-734"></a>
<a name="wglew.h-735"></a><span class="cp">#define WGLEW_EXT_pixel_format_packed_float WGLEW_GET_VAR(__WGLEW_EXT_pixel_format_packed_float)</span>
<a name="wglew.h-736"></a>
<a name="wglew.h-737"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_pixel_format_packed_float */</span><span class="cp"></span>
<a name="wglew.h-738"></a>
<a name="wglew.h-739"></a><span class="cm">/* -------------------------- WGL_EXT_swap_control ------------------------- */</span>
<a name="wglew.h-740"></a>
<a name="wglew.h-741"></a><span class="cp">#ifndef WGL_EXT_swap_control</span>
<a name="wglew.h-742"></a><span class="cp">#define WGL_EXT_swap_control 1</span>
<a name="wglew.h-743"></a>
<a name="wglew.h-744"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETSWAPINTERVALEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="wglew.h-745"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSWAPINTERVALEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">int</span> <span class="n">interval</span><span class="p">);</span>
<a name="wglew.h-746"></a>
<a name="wglew.h-747"></a><span class="cp">#define wglGetSwapIntervalEXT WGLEW_GET_FUN(__wglewGetSwapIntervalEXT)</span>
<a name="wglew.h-748"></a><span class="cp">#define wglSwapIntervalEXT WGLEW_GET_FUN(__wglewSwapIntervalEXT)</span>
<a name="wglew.h-749"></a>
<a name="wglew.h-750"></a><span class="cp">#define WGLEW_EXT_swap_control WGLEW_GET_VAR(__WGLEW_EXT_swap_control)</span>
<a name="wglew.h-751"></a>
<a name="wglew.h-752"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_swap_control */</span><span class="cp"></span>
<a name="wglew.h-753"></a>
<a name="wglew.h-754"></a><span class="cm">/* ----------------------- WGL_EXT_swap_control_tear ----------------------- */</span>
<a name="wglew.h-755"></a>
<a name="wglew.h-756"></a><span class="cp">#ifndef WGL_EXT_swap_control_tear</span>
<a name="wglew.h-757"></a><span class="cp">#define WGL_EXT_swap_control_tear 1</span>
<a name="wglew.h-758"></a>
<a name="wglew.h-759"></a><span class="cp">#define WGLEW_EXT_swap_control_tear WGLEW_GET_VAR(__WGLEW_EXT_swap_control_tear)</span>
<a name="wglew.h-760"></a>
<a name="wglew.h-761"></a><span class="cp">#endif </span><span class="cm">/* WGL_EXT_swap_control_tear */</span><span class="cp"></span>
<a name="wglew.h-762"></a>
<a name="wglew.h-763"></a><span class="cm">/* --------------------- WGL_I3D_digital_video_control --------------------- */</span>
<a name="wglew.h-764"></a>
<a name="wglew.h-765"></a><span class="cp">#ifndef WGL_I3D_digital_video_control</span>
<a name="wglew.h-766"></a><span class="cp">#define WGL_I3D_digital_video_control 1</span>
<a name="wglew.h-767"></a>
<a name="wglew.h-768"></a><span class="cp">#define WGL_DIGITAL_VIDEO_CURSOR_ALPHA_FRAMEBUFFER_I3D 0x2050</span>
<a name="wglew.h-769"></a><span class="cp">#define WGL_DIGITAL_VIDEO_CURSOR_ALPHA_VALUE_I3D 0x2051</span>
<a name="wglew.h-770"></a><span class="cp">#define WGL_DIGITAL_VIDEO_CURSOR_INCLUDED_I3D 0x2052</span>
<a name="wglew.h-771"></a><span class="cp">#define WGL_DIGITAL_VIDEO_GAMMA_CORRECTED_I3D 0x2053</span>
<a name="wglew.h-772"></a>
<a name="wglew.h-773"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETDIGITALVIDEOPARAMETERSI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-774"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSETDIGITALVIDEOPARAMETERSI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-775"></a>
<a name="wglew.h-776"></a><span class="cp">#define wglGetDigitalVideoParametersI3D WGLEW_GET_FUN(__wglewGetDigitalVideoParametersI3D)</span>
<a name="wglew.h-777"></a><span class="cp">#define wglSetDigitalVideoParametersI3D WGLEW_GET_FUN(__wglewSetDigitalVideoParametersI3D)</span>
<a name="wglew.h-778"></a>
<a name="wglew.h-779"></a><span class="cp">#define WGLEW_I3D_digital_video_control WGLEW_GET_VAR(__WGLEW_I3D_digital_video_control)</span>
<a name="wglew.h-780"></a>
<a name="wglew.h-781"></a><span class="cp">#endif </span><span class="cm">/* WGL_I3D_digital_video_control */</span><span class="cp"></span>
<a name="wglew.h-782"></a>
<a name="wglew.h-783"></a><span class="cm">/* ----------------------------- WGL_I3D_gamma ----------------------------- */</span>
<a name="wglew.h-784"></a>
<a name="wglew.h-785"></a><span class="cp">#ifndef WGL_I3D_gamma</span>
<a name="wglew.h-786"></a><span class="cp">#define WGL_I3D_gamma 1</span>
<a name="wglew.h-787"></a>
<a name="wglew.h-788"></a><span class="cp">#define WGL_GAMMA_TABLE_SIZE_I3D 0x204E</span>
<a name="wglew.h-789"></a><span class="cp">#define WGL_GAMMA_EXCLUDE_DESKTOP_I3D 0x204F</span>
<a name="wglew.h-790"></a>
<a name="wglew.h-791"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGAMMATABLEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iEntries</span><span class="p">,</span> <span class="n">USHORT</span><span class="o">*</span> <span class="n">puRed</span><span class="p">,</span> <span class="n">USHORT</span> <span class="o">*</span><span class="n">puGreen</span><span class="p">,</span> <span class="n">USHORT</span> <span class="o">*</span><span class="n">puBlue</span><span class="p">);</span>
<a name="wglew.h-792"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGAMMATABLEPARAMETERSI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-793"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSETGAMMATABLEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iEntries</span><span class="p">,</span> <span class="k">const</span> <span class="n">USHORT</span><span class="o">*</span> <span class="n">puRed</span><span class="p">,</span> <span class="k">const</span> <span class="n">USHORT</span> <span class="o">*</span><span class="n">puGreen</span><span class="p">,</span> <span class="k">const</span> <span class="n">USHORT</span> <span class="o">*</span><span class="n">puBlue</span><span class="p">);</span>
<a name="wglew.h-794"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSETGAMMATABLEPARAMETERSI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-795"></a>
<a name="wglew.h-796"></a><span class="cp">#define wglGetGammaTableI3D WGLEW_GET_FUN(__wglewGetGammaTableI3D)</span>
<a name="wglew.h-797"></a><span class="cp">#define wglGetGammaTableParametersI3D WGLEW_GET_FUN(__wglewGetGammaTableParametersI3D)</span>
<a name="wglew.h-798"></a><span class="cp">#define wglSetGammaTableI3D WGLEW_GET_FUN(__wglewSetGammaTableI3D)</span>
<a name="wglew.h-799"></a><span class="cp">#define wglSetGammaTableParametersI3D WGLEW_GET_FUN(__wglewSetGammaTableParametersI3D)</span>
<a name="wglew.h-800"></a>
<a name="wglew.h-801"></a><span class="cp">#define WGLEW_I3D_gamma WGLEW_GET_VAR(__WGLEW_I3D_gamma)</span>
<a name="wglew.h-802"></a>
<a name="wglew.h-803"></a><span class="cp">#endif </span><span class="cm">/* WGL_I3D_gamma */</span><span class="cp"></span>
<a name="wglew.h-804"></a>
<a name="wglew.h-805"></a><span class="cm">/* ---------------------------- WGL_I3D_genlock ---------------------------- */</span>
<a name="wglew.h-806"></a>
<a name="wglew.h-807"></a><span class="cp">#ifndef WGL_I3D_genlock</span>
<a name="wglew.h-808"></a><span class="cp">#define WGL_I3D_genlock 1</span>
<a name="wglew.h-809"></a>
<a name="wglew.h-810"></a><span class="cp">#define WGL_GENLOCK_SOURCE_MULTIVIEW_I3D 0x2044</span>
<a name="wglew.h-811"></a><span class="cp">#define WGL_GENLOCK_SOURCE_EXTERNAL_SYNC_I3D 0x2045</span>
<a name="wglew.h-812"></a><span class="cp">#define WGL_GENLOCK_SOURCE_EXTERNAL_FIELD_I3D 0x2046</span>
<a name="wglew.h-813"></a><span class="cp">#define WGL_GENLOCK_SOURCE_EXTERNAL_TTL_I3D 0x2047</span>
<a name="wglew.h-814"></a><span class="cp">#define WGL_GENLOCK_SOURCE_DIGITAL_SYNC_I3D 0x2048</span>
<a name="wglew.h-815"></a><span class="cp">#define WGL_GENLOCK_SOURCE_DIGITAL_FIELD_I3D 0x2049</span>
<a name="wglew.h-816"></a><span class="cp">#define WGL_GENLOCK_SOURCE_EDGE_FALLING_I3D 0x204A</span>
<a name="wglew.h-817"></a><span class="cp">#define WGL_GENLOCK_SOURCE_EDGE_RISING_I3D 0x204B</span>
<a name="wglew.h-818"></a><span class="cp">#define WGL_GENLOCK_SOURCE_EDGE_BOTH_I3D 0x204C</span>
<a name="wglew.h-819"></a>
<a name="wglew.h-820"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDISABLEGENLOCKI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">);</span>
<a name="wglew.h-821"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENABLEGENLOCKI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">);</span>
<a name="wglew.h-822"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGENLOCKSAMPLERATEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">uRate</span><span class="p">);</span>
<a name="wglew.h-823"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGENLOCKSOURCEDELAYI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">uDelay</span><span class="p">);</span>
<a name="wglew.h-824"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGENLOCKSOURCEEDGEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">uEdge</span><span class="p">);</span>
<a name="wglew.h-825"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGENLOCKSOURCEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">uSource</span><span class="p">);</span>
<a name="wglew.h-826"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGENLOCKSAMPLERATEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span><span class="o">*</span> <span class="n">uRate</span><span class="p">);</span>
<a name="wglew.h-827"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGENLOCKSOURCEDELAYI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span><span class="o">*</span> <span class="n">uDelay</span><span class="p">);</span>
<a name="wglew.h-828"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGENLOCKSOURCEEDGEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span><span class="o">*</span> <span class="n">uEdge</span><span class="p">);</span>
<a name="wglew.h-829"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETGENLOCKSOURCEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span><span class="o">*</span> <span class="n">uSource</span><span class="p">);</span>
<a name="wglew.h-830"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLISENABLEDGENLOCKI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">BOOL</span><span class="o">*</span> <span class="n">pFlag</span><span class="p">);</span>
<a name="wglew.h-831"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYGENLOCKMAXSOURCEDELAYI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">UINT</span><span class="o">*</span> <span class="n">uMaxLineDelay</span><span class="p">,</span> <span class="n">UINT</span> <span class="o">*</span><span class="n">uMaxPixelDelay</span><span class="p">);</span>
<a name="wglew.h-832"></a>
<a name="wglew.h-833"></a><span class="cp">#define wglDisableGenlockI3D WGLEW_GET_FUN(__wglewDisableGenlockI3D)</span>
<a name="wglew.h-834"></a><span class="cp">#define wglEnableGenlockI3D WGLEW_GET_FUN(__wglewEnableGenlockI3D)</span>
<a name="wglew.h-835"></a><span class="cp">#define wglGenlockSampleRateI3D WGLEW_GET_FUN(__wglewGenlockSampleRateI3D)</span>
<a name="wglew.h-836"></a><span class="cp">#define wglGenlockSourceDelayI3D WGLEW_GET_FUN(__wglewGenlockSourceDelayI3D)</span>
<a name="wglew.h-837"></a><span class="cp">#define wglGenlockSourceEdgeI3D WGLEW_GET_FUN(__wglewGenlockSourceEdgeI3D)</span>
<a name="wglew.h-838"></a><span class="cp">#define wglGenlockSourceI3D WGLEW_GET_FUN(__wglewGenlockSourceI3D)</span>
<a name="wglew.h-839"></a><span class="cp">#define wglGetGenlockSampleRateI3D WGLEW_GET_FUN(__wglewGetGenlockSampleRateI3D)</span>
<a name="wglew.h-840"></a><span class="cp">#define wglGetGenlockSourceDelayI3D WGLEW_GET_FUN(__wglewGetGenlockSourceDelayI3D)</span>
<a name="wglew.h-841"></a><span class="cp">#define wglGetGenlockSourceEdgeI3D WGLEW_GET_FUN(__wglewGetGenlockSourceEdgeI3D)</span>
<a name="wglew.h-842"></a><span class="cp">#define wglGetGenlockSourceI3D WGLEW_GET_FUN(__wglewGetGenlockSourceI3D)</span>
<a name="wglew.h-843"></a><span class="cp">#define wglIsEnabledGenlockI3D WGLEW_GET_FUN(__wglewIsEnabledGenlockI3D)</span>
<a name="wglew.h-844"></a><span class="cp">#define wglQueryGenlockMaxSourceDelayI3D WGLEW_GET_FUN(__wglewQueryGenlockMaxSourceDelayI3D)</span>
<a name="wglew.h-845"></a>
<a name="wglew.h-846"></a><span class="cp">#define WGLEW_I3D_genlock WGLEW_GET_VAR(__WGLEW_I3D_genlock)</span>
<a name="wglew.h-847"></a>
<a name="wglew.h-848"></a><span class="cp">#endif </span><span class="cm">/* WGL_I3D_genlock */</span><span class="cp"></span>
<a name="wglew.h-849"></a>
<a name="wglew.h-850"></a><span class="cm">/* -------------------------- WGL_I3D_image_buffer ------------------------- */</span>
<a name="wglew.h-851"></a>
<a name="wglew.h-852"></a><span class="cp">#ifndef WGL_I3D_image_buffer</span>
<a name="wglew.h-853"></a><span class="cp">#define WGL_I3D_image_buffer 1</span>
<a name="wglew.h-854"></a>
<a name="wglew.h-855"></a><span class="cp">#define WGL_IMAGE_BUFFER_MIN_ACCESS_I3D 0x00000001</span>
<a name="wglew.h-856"></a><span class="cp">#define WGL_IMAGE_BUFFER_LOCK_I3D 0x00000002</span>
<a name="wglew.h-857"></a>
<a name="wglew.h-858"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLASSOCIATEIMAGEBUFFEREVENTSI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">HANDLE</span><span class="o">*</span> <span class="n">pEvent</span><span class="p">,</span> <span class="n">LPVOID</span> <span class="o">*</span><span class="n">pAddress</span><span class="p">,</span> <span class="n">DWORD</span> <span class="o">*</span><span class="n">pSize</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">count</span><span class="p">);</span>
<a name="wglew.h-859"></a><span class="k">typedef</span> <span class="nf">LPVOID</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEIMAGEBUFFERI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">DWORD</span> <span class="n">dwSize</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">uFlags</span><span class="p">);</span>
<a name="wglew.h-860"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDESTROYIMAGEBUFFERI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">LPVOID</span> <span class="n">pAddress</span><span class="p">);</span>
<a name="wglew.h-861"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRELEASEIMAGEBUFFEREVENTSI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">LPVOID</span><span class="o">*</span> <span class="n">pAddress</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">count</span><span class="p">);</span>
<a name="wglew.h-862"></a>
<a name="wglew.h-863"></a><span class="cp">#define wglAssociateImageBufferEventsI3D WGLEW_GET_FUN(__wglewAssociateImageBufferEventsI3D)</span>
<a name="wglew.h-864"></a><span class="cp">#define wglCreateImageBufferI3D WGLEW_GET_FUN(__wglewCreateImageBufferI3D)</span>
<a name="wglew.h-865"></a><span class="cp">#define wglDestroyImageBufferI3D WGLEW_GET_FUN(__wglewDestroyImageBufferI3D)</span>
<a name="wglew.h-866"></a><span class="cp">#define wglReleaseImageBufferEventsI3D WGLEW_GET_FUN(__wglewReleaseImageBufferEventsI3D)</span>
<a name="wglew.h-867"></a>
<a name="wglew.h-868"></a><span class="cp">#define WGLEW_I3D_image_buffer WGLEW_GET_VAR(__WGLEW_I3D_image_buffer)</span>
<a name="wglew.h-869"></a>
<a name="wglew.h-870"></a><span class="cp">#endif </span><span class="cm">/* WGL_I3D_image_buffer */</span><span class="cp"></span>
<a name="wglew.h-871"></a>
<a name="wglew.h-872"></a><span class="cm">/* ------------------------ WGL_I3D_swap_frame_lock ------------------------ */</span>
<a name="wglew.h-873"></a>
<a name="wglew.h-874"></a><span class="cp">#ifndef WGL_I3D_swap_frame_lock</span>
<a name="wglew.h-875"></a><span class="cp">#define WGL_I3D_swap_frame_lock 1</span>
<a name="wglew.h-876"></a>
<a name="wglew.h-877"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDISABLEFRAMELOCKI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">VOID</span><span class="p">);</span>
<a name="wglew.h-878"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENABLEFRAMELOCKI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">VOID</span><span class="p">);</span>
<a name="wglew.h-879"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLISENABLEDFRAMELOCKI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">BOOL</span><span class="o">*</span> <span class="n">pFlag</span><span class="p">);</span>
<a name="wglew.h-880"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYFRAMELOCKMASTERI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">BOOL</span><span class="o">*</span> <span class="n">pFlag</span><span class="p">);</span>
<a name="wglew.h-881"></a>
<a name="wglew.h-882"></a><span class="cp">#define wglDisableFrameLockI3D WGLEW_GET_FUN(__wglewDisableFrameLockI3D)</span>
<a name="wglew.h-883"></a><span class="cp">#define wglEnableFrameLockI3D WGLEW_GET_FUN(__wglewEnableFrameLockI3D)</span>
<a name="wglew.h-884"></a><span class="cp">#define wglIsEnabledFrameLockI3D WGLEW_GET_FUN(__wglewIsEnabledFrameLockI3D)</span>
<a name="wglew.h-885"></a><span class="cp">#define wglQueryFrameLockMasterI3D WGLEW_GET_FUN(__wglewQueryFrameLockMasterI3D)</span>
<a name="wglew.h-886"></a>
<a name="wglew.h-887"></a><span class="cp">#define WGLEW_I3D_swap_frame_lock WGLEW_GET_VAR(__WGLEW_I3D_swap_frame_lock)</span>
<a name="wglew.h-888"></a>
<a name="wglew.h-889"></a><span class="cp">#endif </span><span class="cm">/* WGL_I3D_swap_frame_lock */</span><span class="cp"></span>
<a name="wglew.h-890"></a>
<a name="wglew.h-891"></a><span class="cm">/* ------------------------ WGL_I3D_swap_frame_usage ----------------------- */</span>
<a name="wglew.h-892"></a>
<a name="wglew.h-893"></a><span class="cp">#ifndef WGL_I3D_swap_frame_usage</span>
<a name="wglew.h-894"></a><span class="cp">#define WGL_I3D_swap_frame_usage 1</span>
<a name="wglew.h-895"></a>
<a name="wglew.h-896"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBEGINFRAMETRACKINGI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="wglew.h-897"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENDFRAMETRACKINGI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="wglew.h-898"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETFRAMEUSAGEI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">float</span><span class="o">*</span> <span class="n">pUsage</span><span class="p">);</span>
<a name="wglew.h-899"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYFRAMETRACKINGI3DPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">DWORD</span><span class="o">*</span> <span class="n">pFrameCount</span><span class="p">,</span> <span class="n">DWORD</span> <span class="o">*</span><span class="n">pMissedFrames</span><span class="p">,</span> <span class="kt">float</span> <span class="o">*</span><span class="n">pLastMissedUsage</span><span class="p">);</span>
<a name="wglew.h-900"></a>
<a name="wglew.h-901"></a><span class="cp">#define wglBeginFrameTrackingI3D WGLEW_GET_FUN(__wglewBeginFrameTrackingI3D)</span>
<a name="wglew.h-902"></a><span class="cp">#define wglEndFrameTrackingI3D WGLEW_GET_FUN(__wglewEndFrameTrackingI3D)</span>
<a name="wglew.h-903"></a><span class="cp">#define wglGetFrameUsageI3D WGLEW_GET_FUN(__wglewGetFrameUsageI3D)</span>
<a name="wglew.h-904"></a><span class="cp">#define wglQueryFrameTrackingI3D WGLEW_GET_FUN(__wglewQueryFrameTrackingI3D)</span>
<a name="wglew.h-905"></a>
<a name="wglew.h-906"></a><span class="cp">#define WGLEW_I3D_swap_frame_usage WGLEW_GET_VAR(__WGLEW_I3D_swap_frame_usage)</span>
<a name="wglew.h-907"></a>
<a name="wglew.h-908"></a><span class="cp">#endif </span><span class="cm">/* WGL_I3D_swap_frame_usage */</span><span class="cp"></span>
<a name="wglew.h-909"></a>
<a name="wglew.h-910"></a><span class="cm">/* --------------------------- WGL_NV_DX_interop --------------------------- */</span>
<a name="wglew.h-911"></a>
<a name="wglew.h-912"></a><span class="cp">#ifndef WGL_NV_DX_interop</span>
<a name="wglew.h-913"></a><span class="cp">#define WGL_NV_DX_interop 1</span>
<a name="wglew.h-914"></a>
<a name="wglew.h-915"></a><span class="cp">#define WGL_ACCESS_READ_ONLY_NV 0x0000</span>
<a name="wglew.h-916"></a><span class="cp">#define WGL_ACCESS_READ_WRITE_NV 0x0001</span>
<a name="wglew.h-917"></a><span class="cp">#define WGL_ACCESS_WRITE_DISCARD_NV 0x0002</span>
<a name="wglew.h-918"></a>
<a name="wglew.h-919"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXCLOSEDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hDevice</span><span class="p">);</span>
<a name="wglew.h-920"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXLOCKOBJECTSNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hDevice</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">count</span><span class="p">,</span> <span class="n">HANDLE</span><span class="o">*</span> <span class="n">hObjects</span><span class="p">);</span>
<a name="wglew.h-921"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXOBJECTACCESSNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hObject</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">access</span><span class="p">);</span>
<a name="wglew.h-922"></a><span class="k">typedef</span> <span class="nf">HANDLE</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXOPENDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="o">*</span> <span class="n">dxDevice</span><span class="p">);</span>
<a name="wglew.h-923"></a><span class="k">typedef</span> <span class="nf">HANDLE</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXREGISTEROBJECTNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hDevice</span><span class="p">,</span> <span class="kt">void</span><span class="o">*</span> <span class="n">dxObject</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">name</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">type</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">access</span><span class="p">);</span>
<a name="wglew.h-924"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXSETRESOURCESHAREHANDLENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="o">*</span> <span class="n">dxObject</span><span class="p">,</span> <span class="n">HANDLE</span> <span class="n">shareHandle</span><span class="p">);</span>
<a name="wglew.h-925"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXUNLOCKOBJECTSNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hDevice</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">count</span><span class="p">,</span> <span class="n">HANDLE</span><span class="o">*</span> <span class="n">hObjects</span><span class="p">);</span>
<a name="wglew.h-926"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDXUNREGISTEROBJECTNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HANDLE</span> <span class="n">hDevice</span><span class="p">,</span> <span class="n">HANDLE</span> <span class="n">hObject</span><span class="p">);</span>
<a name="wglew.h-927"></a>
<a name="wglew.h-928"></a><span class="cp">#define wglDXCloseDeviceNV WGLEW_GET_FUN(__wglewDXCloseDeviceNV)</span>
<a name="wglew.h-929"></a><span class="cp">#define wglDXLockObjectsNV WGLEW_GET_FUN(__wglewDXLockObjectsNV)</span>
<a name="wglew.h-930"></a><span class="cp">#define wglDXObjectAccessNV WGLEW_GET_FUN(__wglewDXObjectAccessNV)</span>
<a name="wglew.h-931"></a><span class="cp">#define wglDXOpenDeviceNV WGLEW_GET_FUN(__wglewDXOpenDeviceNV)</span>
<a name="wglew.h-932"></a><span class="cp">#define wglDXRegisterObjectNV WGLEW_GET_FUN(__wglewDXRegisterObjectNV)</span>
<a name="wglew.h-933"></a><span class="cp">#define wglDXSetResourceShareHandleNV WGLEW_GET_FUN(__wglewDXSetResourceShareHandleNV)</span>
<a name="wglew.h-934"></a><span class="cp">#define wglDXUnlockObjectsNV WGLEW_GET_FUN(__wglewDXUnlockObjectsNV)</span>
<a name="wglew.h-935"></a><span class="cp">#define wglDXUnregisterObjectNV WGLEW_GET_FUN(__wglewDXUnregisterObjectNV)</span>
<a name="wglew.h-936"></a>
<a name="wglew.h-937"></a><span class="cp">#define WGLEW_NV_DX_interop WGLEW_GET_VAR(__WGLEW_NV_DX_interop)</span>
<a name="wglew.h-938"></a>
<a name="wglew.h-939"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_DX_interop */</span><span class="cp"></span>
<a name="wglew.h-940"></a>
<a name="wglew.h-941"></a><span class="cm">/* --------------------------- WGL_NV_DX_interop2 -------------------------- */</span>
<a name="wglew.h-942"></a>
<a name="wglew.h-943"></a><span class="cp">#ifndef WGL_NV_DX_interop2</span>
<a name="wglew.h-944"></a><span class="cp">#define WGL_NV_DX_interop2 1</span>
<a name="wglew.h-945"></a>
<a name="wglew.h-946"></a><span class="cp">#define WGLEW_NV_DX_interop2 WGLEW_GET_VAR(__WGLEW_NV_DX_interop2)</span>
<a name="wglew.h-947"></a>
<a name="wglew.h-948"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_DX_interop2 */</span><span class="cp"></span>
<a name="wglew.h-949"></a>
<a name="wglew.h-950"></a><span class="cm">/* --------------------------- WGL_NV_copy_image --------------------------- */</span>
<a name="wglew.h-951"></a>
<a name="wglew.h-952"></a><span class="cp">#ifndef WGL_NV_copy_image</span>
<a name="wglew.h-953"></a><span class="cp">#define WGL_NV_copy_image 1</span>
<a name="wglew.h-954"></a>
<a name="wglew.h-955"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCOPYIMAGESUBDATANVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HGLRC</span> <span class="n">hSrcRC</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">srcName</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">srcTarget</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcLevel</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcX</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcY</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcZ</span><span class="p">,</span> <span class="n">HGLRC</span> <span class="n">hDstRC</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">dstName</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">dstTarget</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstLevel</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstX</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstY</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstZ</span><span class="p">,</span> <span class="n">GLsizei</span> <span class="n">width</span><span class="p">,</span> <span class="n">GLsizei</span> <span class="n">height</span><span class="p">,</span> <span class="n">GLsizei</span> <span class="n">depth</span><span class="p">);</span>
<a name="wglew.h-956"></a>
<a name="wglew.h-957"></a><span class="cp">#define wglCopyImageSubDataNV WGLEW_GET_FUN(__wglewCopyImageSubDataNV)</span>
<a name="wglew.h-958"></a>
<a name="wglew.h-959"></a><span class="cp">#define WGLEW_NV_copy_image WGLEW_GET_VAR(__WGLEW_NV_copy_image)</span>
<a name="wglew.h-960"></a>
<a name="wglew.h-961"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_copy_image */</span><span class="cp"></span>
<a name="wglew.h-962"></a>
<a name="wglew.h-963"></a><span class="cm">/* ------------------------ WGL_NV_delay_before_swap ----------------------- */</span>
<a name="wglew.h-964"></a>
<a name="wglew.h-965"></a><span class="cp">#ifndef WGL_NV_delay_before_swap</span>
<a name="wglew.h-966"></a><span class="cp">#define WGL_NV_delay_before_swap 1</span>
<a name="wglew.h-967"></a>
<a name="wglew.h-968"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDELAYBEFORESWAPNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">seconds</span><span class="p">);</span>
<a name="wglew.h-969"></a>
<a name="wglew.h-970"></a><span class="cp">#define wglDelayBeforeSwapNV WGLEW_GET_FUN(__wglewDelayBeforeSwapNV)</span>
<a name="wglew.h-971"></a>
<a name="wglew.h-972"></a><span class="cp">#define WGLEW_NV_delay_before_swap WGLEW_GET_VAR(__WGLEW_NV_delay_before_swap)</span>
<a name="wglew.h-973"></a>
<a name="wglew.h-974"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_delay_before_swap */</span><span class="cp"></span>
<a name="wglew.h-975"></a>
<a name="wglew.h-976"></a><span class="cm">/* -------------------------- WGL_NV_float_buffer -------------------------- */</span>
<a name="wglew.h-977"></a>
<a name="wglew.h-978"></a><span class="cp">#ifndef WGL_NV_float_buffer</span>
<a name="wglew.h-979"></a><span class="cp">#define WGL_NV_float_buffer 1</span>
<a name="wglew.h-980"></a>
<a name="wglew.h-981"></a><span class="cp">#define WGL_FLOAT_COMPONENTS_NV 0x20B0</span>
<a name="wglew.h-982"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RECTANGLE_FLOAT_R_NV 0x20B1</span>
<a name="wglew.h-983"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RECTANGLE_FLOAT_RG_NV 0x20B2</span>
<a name="wglew.h-984"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RECTANGLE_FLOAT_RGB_NV 0x20B3</span>
<a name="wglew.h-985"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RECTANGLE_FLOAT_RGBA_NV 0x20B4</span>
<a name="wglew.h-986"></a><span class="cp">#define WGL_TEXTURE_FLOAT_R_NV 0x20B5</span>
<a name="wglew.h-987"></a><span class="cp">#define WGL_TEXTURE_FLOAT_RG_NV 0x20B6</span>
<a name="wglew.h-988"></a><span class="cp">#define WGL_TEXTURE_FLOAT_RGB_NV 0x20B7</span>
<a name="wglew.h-989"></a><span class="cp">#define WGL_TEXTURE_FLOAT_RGBA_NV 0x20B8</span>
<a name="wglew.h-990"></a>
<a name="wglew.h-991"></a><span class="cp">#define WGLEW_NV_float_buffer WGLEW_GET_VAR(__WGLEW_NV_float_buffer)</span>
<a name="wglew.h-992"></a>
<a name="wglew.h-993"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_float_buffer */</span><span class="cp"></span>
<a name="wglew.h-994"></a>
<a name="wglew.h-995"></a><span class="cm">/* -------------------------- WGL_NV_gpu_affinity -------------------------- */</span>
<a name="wglew.h-996"></a>
<a name="wglew.h-997"></a><span class="cp">#ifndef WGL_NV_gpu_affinity</span>
<a name="wglew.h-998"></a><span class="cp">#define WGL_NV_gpu_affinity 1</span>
<a name="wglew.h-999"></a>
<a name="wglew.h-1000"></a><span class="cp">#define WGL_ERROR_INCOMPATIBLE_AFFINITY_MASKS_NV 0x20D0</span>
<a name="wglew.h-1001"></a><span class="cp">#define WGL_ERROR_MISSING_AFFINITY_MASK_NV 0x20D1</span>
<a name="wglew.h-1002"></a>
<a name="wglew.h-1003"></a><span class="n">DECLARE_HANDLE</span><span class="p">(</span><span class="n">HGPUNV</span><span class="p">);</span>
<a name="wglew.h-1004"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="n">_GPU_DEVICE</span> <span class="p">{</span>
<a name="wglew.h-1005"></a>  <span class="n">DWORD</span> <span class="n">cb</span><span class="p">;</span> 
<a name="wglew.h-1006"></a>  <span class="n">CHAR</span> <span class="n">DeviceName</span><span class="p">[</span><span class="mi">32</span><span class="p">];</span> 
<a name="wglew.h-1007"></a>  <span class="n">CHAR</span> <span class="n">DeviceString</span><span class="p">[</span><span class="mi">128</span><span class="p">];</span> 
<a name="wglew.h-1008"></a>  <span class="n">DWORD</span> <span class="n">Flags</span><span class="p">;</span> 
<a name="wglew.h-1009"></a>  <span class="n">RECT</span> <span class="n">rcVirtualScreen</span><span class="p">;</span> 
<a name="wglew.h-1010"></a><span class="p">}</span> <span class="n">GPU_DEVICE</span><span class="p">,</span> <span class="o">*</span><span class="n">PGPU_DEVICE</span><span class="p">;</span>
<a name="wglew.h-1011"></a>
<a name="wglew.h-1012"></a><span class="k">typedef</span> <span class="nf">HDC</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLCREATEAFFINITYDCNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="k">const</span> <span class="n">HGPUNV</span> <span class="o">*</span><span class="n">phGpuList</span><span class="p">);</span>
<a name="wglew.h-1013"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLDELETEDCNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">);</span>
<a name="wglew.h-1014"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENUMGPUDEVICESNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HGPUNV</span> <span class="n">hGpu</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">iDeviceIndex</span><span class="p">,</span> <span class="n">PGPU_DEVICE</span> <span class="n">lpGpuDevice</span><span class="p">);</span>
<a name="wglew.h-1015"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENUMGPUSFROMAFFINITYDCNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hAffinityDC</span><span class="p">,</span> <span class="n">UINT</span> <span class="n">iGpuIndex</span><span class="p">,</span> <span class="n">HGPUNV</span> <span class="o">*</span><span class="n">hGpu</span><span class="p">);</span>
<a name="wglew.h-1016"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENUMGPUSNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">UINT</span> <span class="n">iGpuIndex</span><span class="p">,</span> <span class="n">HGPUNV</span> <span class="o">*</span><span class="n">phGpu</span><span class="p">);</span>
<a name="wglew.h-1017"></a>
<a name="wglew.h-1018"></a><span class="cp">#define wglCreateAffinityDCNV WGLEW_GET_FUN(__wglewCreateAffinityDCNV)</span>
<a name="wglew.h-1019"></a><span class="cp">#define wglDeleteDCNV WGLEW_GET_FUN(__wglewDeleteDCNV)</span>
<a name="wglew.h-1020"></a><span class="cp">#define wglEnumGpuDevicesNV WGLEW_GET_FUN(__wglewEnumGpuDevicesNV)</span>
<a name="wglew.h-1021"></a><span class="cp">#define wglEnumGpusFromAffinityDCNV WGLEW_GET_FUN(__wglewEnumGpusFromAffinityDCNV)</span>
<a name="wglew.h-1022"></a><span class="cp">#define wglEnumGpusNV WGLEW_GET_FUN(__wglewEnumGpusNV)</span>
<a name="wglew.h-1023"></a>
<a name="wglew.h-1024"></a><span class="cp">#define WGLEW_NV_gpu_affinity WGLEW_GET_VAR(__WGLEW_NV_gpu_affinity)</span>
<a name="wglew.h-1025"></a>
<a name="wglew.h-1026"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_gpu_affinity */</span><span class="cp"></span>
<a name="wglew.h-1027"></a>
<a name="wglew.h-1028"></a><span class="cm">/* ---------------------- WGL_NV_multisample_coverage ---------------------- */</span>
<a name="wglew.h-1029"></a>
<a name="wglew.h-1030"></a><span class="cp">#ifndef WGL_NV_multisample_coverage</span>
<a name="wglew.h-1031"></a><span class="cp">#define WGL_NV_multisample_coverage 1</span>
<a name="wglew.h-1032"></a>
<a name="wglew.h-1033"></a><span class="cp">#define WGL_COVERAGE_SAMPLES_NV 0x2042</span>
<a name="wglew.h-1034"></a><span class="cp">#define WGL_COLOR_SAMPLES_NV 0x20B9</span>
<a name="wglew.h-1035"></a>
<a name="wglew.h-1036"></a><span class="cp">#define WGLEW_NV_multisample_coverage WGLEW_GET_VAR(__WGLEW_NV_multisample_coverage)</span>
<a name="wglew.h-1037"></a>
<a name="wglew.h-1038"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_multisample_coverage */</span><span class="cp"></span>
<a name="wglew.h-1039"></a>
<a name="wglew.h-1040"></a><span class="cm">/* -------------------------- WGL_NV_present_video ------------------------- */</span>
<a name="wglew.h-1041"></a>
<a name="wglew.h-1042"></a><span class="cp">#ifndef WGL_NV_present_video</span>
<a name="wglew.h-1043"></a><span class="cp">#define WGL_NV_present_video 1</span>
<a name="wglew.h-1044"></a>
<a name="wglew.h-1045"></a><span class="cp">#define WGL_NUM_VIDEO_SLOTS_NV 0x20F0</span>
<a name="wglew.h-1046"></a>
<a name="wglew.h-1047"></a><span class="n">DECLARE_HANDLE</span><span class="p">(</span><span class="n">HVIDEOOUTPUTDEVICENV</span><span class="p">);</span>
<a name="wglew.h-1048"></a>
<a name="wglew.h-1049"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBINDVIDEODEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDc</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">uVideoSlot</span><span class="p">,</span> <span class="n">HVIDEOOUTPUTDEVICENV</span> <span class="n">hVideoDevice</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piAttribList</span><span class="p">);</span>
<a name="wglew.h-1050"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENUMERATEVIDEODEVICESNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDc</span><span class="p">,</span> <span class="n">HVIDEOOUTPUTDEVICENV</span><span class="o">*</span> <span class="n">phDeviceList</span><span class="p">);</span>
<a name="wglew.h-1051"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYCURRENTCONTEXTNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-1052"></a>
<a name="wglew.h-1053"></a><span class="cp">#define wglBindVideoDeviceNV WGLEW_GET_FUN(__wglewBindVideoDeviceNV)</span>
<a name="wglew.h-1054"></a><span class="cp">#define wglEnumerateVideoDevicesNV WGLEW_GET_FUN(__wglewEnumerateVideoDevicesNV)</span>
<a name="wglew.h-1055"></a><span class="cp">#define wglQueryCurrentContextNV WGLEW_GET_FUN(__wglewQueryCurrentContextNV)</span>
<a name="wglew.h-1056"></a>
<a name="wglew.h-1057"></a><span class="cp">#define WGLEW_NV_present_video WGLEW_GET_VAR(__WGLEW_NV_present_video)</span>
<a name="wglew.h-1058"></a>
<a name="wglew.h-1059"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_present_video */</span><span class="cp"></span>
<a name="wglew.h-1060"></a>
<a name="wglew.h-1061"></a><span class="cm">/* ---------------------- WGL_NV_render_depth_texture ---------------------- */</span>
<a name="wglew.h-1062"></a>
<a name="wglew.h-1063"></a><span class="cp">#ifndef WGL_NV_render_depth_texture</span>
<a name="wglew.h-1064"></a><span class="cp">#define WGL_NV_render_depth_texture 1</span>
<a name="wglew.h-1065"></a>
<a name="wglew.h-1066"></a><span class="cp">#define WGL_NO_TEXTURE_ARB 0x2077</span>
<a name="wglew.h-1067"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_DEPTH_NV 0x20A3</span>
<a name="wglew.h-1068"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RECTANGLE_DEPTH_NV 0x20A4</span>
<a name="wglew.h-1069"></a><span class="cp">#define WGL_DEPTH_TEXTURE_FORMAT_NV 0x20A5</span>
<a name="wglew.h-1070"></a><span class="cp">#define WGL_TEXTURE_DEPTH_COMPONENT_NV 0x20A6</span>
<a name="wglew.h-1071"></a><span class="cp">#define WGL_DEPTH_COMPONENT_NV 0x20A7</span>
<a name="wglew.h-1072"></a>
<a name="wglew.h-1073"></a><span class="cp">#define WGLEW_NV_render_depth_texture WGLEW_GET_VAR(__WGLEW_NV_render_depth_texture)</span>
<a name="wglew.h-1074"></a>
<a name="wglew.h-1075"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_render_depth_texture */</span><span class="cp"></span>
<a name="wglew.h-1076"></a>
<a name="wglew.h-1077"></a><span class="cm">/* -------------------- WGL_NV_render_texture_rectangle -------------------- */</span>
<a name="wglew.h-1078"></a>
<a name="wglew.h-1079"></a><span class="cp">#ifndef WGL_NV_render_texture_rectangle</span>
<a name="wglew.h-1080"></a><span class="cp">#define WGL_NV_render_texture_rectangle 1</span>
<a name="wglew.h-1081"></a>
<a name="wglew.h-1082"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RECTANGLE_RGB_NV 0x20A0</span>
<a name="wglew.h-1083"></a><span class="cp">#define WGL_BIND_TO_TEXTURE_RECTANGLE_RGBA_NV 0x20A1</span>
<a name="wglew.h-1084"></a><span class="cp">#define WGL_TEXTURE_RECTANGLE_NV 0x20A2</span>
<a name="wglew.h-1085"></a>
<a name="wglew.h-1086"></a><span class="cp">#define WGLEW_NV_render_texture_rectangle WGLEW_GET_VAR(__WGLEW_NV_render_texture_rectangle)</span>
<a name="wglew.h-1087"></a>
<a name="wglew.h-1088"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_render_texture_rectangle */</span><span class="cp"></span>
<a name="wglew.h-1089"></a>
<a name="wglew.h-1090"></a><span class="cm">/* --------------------------- WGL_NV_swap_group --------------------------- */</span>
<a name="wglew.h-1091"></a>
<a name="wglew.h-1092"></a><span class="cp">#ifndef WGL_NV_swap_group</span>
<a name="wglew.h-1093"></a><span class="cp">#define WGL_NV_swap_group 1</span>
<a name="wglew.h-1094"></a>
<a name="wglew.h-1095"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBINDSWAPBARRIERNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLuint</span> <span class="n">group</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">barrier</span><span class="p">);</span>
<a name="wglew.h-1096"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLJOINSWAPGROUPNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">group</span><span class="p">);</span>
<a name="wglew.h-1097"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYFRAMECOUNTNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">GLuint</span><span class="o">*</span> <span class="n">count</span><span class="p">);</span>
<a name="wglew.h-1098"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYMAXSWAPGROUPSNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">GLuint</span><span class="o">*</span> <span class="n">maxGroups</span><span class="p">,</span> <span class="n">GLuint</span> <span class="o">*</span><span class="n">maxBarriers</span><span class="p">);</span>
<a name="wglew.h-1099"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYSWAPGROUPNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="n">GLuint</span><span class="o">*</span> <span class="n">group</span><span class="p">,</span> <span class="n">GLuint</span> <span class="o">*</span><span class="n">barrier</span><span class="p">);</span>
<a name="wglew.h-1100"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRESETFRAMECOUNTNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">);</span>
<a name="wglew.h-1101"></a>
<a name="wglew.h-1102"></a><span class="cp">#define wglBindSwapBarrierNV WGLEW_GET_FUN(__wglewBindSwapBarrierNV)</span>
<a name="wglew.h-1103"></a><span class="cp">#define wglJoinSwapGroupNV WGLEW_GET_FUN(__wglewJoinSwapGroupNV)</span>
<a name="wglew.h-1104"></a><span class="cp">#define wglQueryFrameCountNV WGLEW_GET_FUN(__wglewQueryFrameCountNV)</span>
<a name="wglew.h-1105"></a><span class="cp">#define wglQueryMaxSwapGroupsNV WGLEW_GET_FUN(__wglewQueryMaxSwapGroupsNV)</span>
<a name="wglew.h-1106"></a><span class="cp">#define wglQuerySwapGroupNV WGLEW_GET_FUN(__wglewQuerySwapGroupNV)</span>
<a name="wglew.h-1107"></a><span class="cp">#define wglResetFrameCountNV WGLEW_GET_FUN(__wglewResetFrameCountNV)</span>
<a name="wglew.h-1108"></a>
<a name="wglew.h-1109"></a><span class="cp">#define WGLEW_NV_swap_group WGLEW_GET_VAR(__WGLEW_NV_swap_group)</span>
<a name="wglew.h-1110"></a>
<a name="wglew.h-1111"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_swap_group */</span><span class="cp"></span>
<a name="wglew.h-1112"></a>
<a name="wglew.h-1113"></a><span class="cm">/* ----------------------- WGL_NV_vertex_array_range ----------------------- */</span>
<a name="wglew.h-1114"></a>
<a name="wglew.h-1115"></a><span class="cp">#ifndef WGL_NV_vertex_array_range</span>
<a name="wglew.h-1116"></a><span class="cp">#define WGL_NV_vertex_array_range 1</span>
<a name="wglew.h-1117"></a>
<a name="wglew.h-1118"></a><span class="k">typedef</span> <span class="kt">void</span> <span class="o">*</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLALLOCATEMEMORYNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLsizei</span> <span class="n">size</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">readFrequency</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">writeFrequency</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">priority</span><span class="p">);</span>
<a name="wglew.h-1119"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLFREEMEMORYNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span> <span class="o">*</span><span class="n">pointer</span><span class="p">);</span>
<a name="wglew.h-1120"></a>
<a name="wglew.h-1121"></a><span class="cp">#define wglAllocateMemoryNV WGLEW_GET_FUN(__wglewAllocateMemoryNV)</span>
<a name="wglew.h-1122"></a><span class="cp">#define wglFreeMemoryNV WGLEW_GET_FUN(__wglewFreeMemoryNV)</span>
<a name="wglew.h-1123"></a>
<a name="wglew.h-1124"></a><span class="cp">#define WGLEW_NV_vertex_array_range WGLEW_GET_VAR(__WGLEW_NV_vertex_array_range)</span>
<a name="wglew.h-1125"></a>
<a name="wglew.h-1126"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_vertex_array_range */</span><span class="cp"></span>
<a name="wglew.h-1127"></a>
<a name="wglew.h-1128"></a><span class="cm">/* -------------------------- WGL_NV_video_capture ------------------------- */</span>
<a name="wglew.h-1129"></a>
<a name="wglew.h-1130"></a><span class="cp">#ifndef WGL_NV_video_capture</span>
<a name="wglew.h-1131"></a><span class="cp">#define WGL_NV_video_capture 1</span>
<a name="wglew.h-1132"></a>
<a name="wglew.h-1133"></a><span class="cp">#define WGL_UNIQUE_ID_NV 0x20CE</span>
<a name="wglew.h-1134"></a><span class="cp">#define WGL_NUM_VIDEO_CAPTURE_SLOTS_NV 0x20CF</span>
<a name="wglew.h-1135"></a>
<a name="wglew.h-1136"></a><span class="n">DECLARE_HANDLE</span><span class="p">(</span><span class="n">HVIDEOINPUTDEVICENV</span><span class="p">);</span>
<a name="wglew.h-1137"></a>
<a name="wglew.h-1138"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBINDVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">UINT</span> <span class="n">uVideoSlot</span><span class="p">,</span> <span class="n">HVIDEOINPUTDEVICENV</span> <span class="n">hDevice</span><span class="p">);</span>
<a name="wglew.h-1139"></a><span class="k">typedef</span> <span class="nf">UINT</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLENUMERATEVIDEOCAPTUREDEVICESNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDc</span><span class="p">,</span> <span class="n">HVIDEOINPUTDEVICENV</span><span class="o">*</span> <span class="n">phDeviceList</span><span class="p">);</span>
<a name="wglew.h-1140"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLLOCKVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDc</span><span class="p">,</span> <span class="n">HVIDEOINPUTDEVICENV</span> <span class="n">hDevice</span><span class="p">);</span>
<a name="wglew.h-1141"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLQUERYVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDc</span><span class="p">,</span> <span class="n">HVIDEOINPUTDEVICENV</span> <span class="n">hDevice</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iAttribute</span><span class="p">,</span> <span class="kt">int</span><span class="o">*</span> <span class="n">piValue</span><span class="p">);</span>
<a name="wglew.h-1142"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRELEASEVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDc</span><span class="p">,</span> <span class="n">HVIDEOINPUTDEVICENV</span> <span class="n">hDevice</span><span class="p">);</span>
<a name="wglew.h-1143"></a>
<a name="wglew.h-1144"></a><span class="cp">#define wglBindVideoCaptureDeviceNV WGLEW_GET_FUN(__wglewBindVideoCaptureDeviceNV)</span>
<a name="wglew.h-1145"></a><span class="cp">#define wglEnumerateVideoCaptureDevicesNV WGLEW_GET_FUN(__wglewEnumerateVideoCaptureDevicesNV)</span>
<a name="wglew.h-1146"></a><span class="cp">#define wglLockVideoCaptureDeviceNV WGLEW_GET_FUN(__wglewLockVideoCaptureDeviceNV)</span>
<a name="wglew.h-1147"></a><span class="cp">#define wglQueryVideoCaptureDeviceNV WGLEW_GET_FUN(__wglewQueryVideoCaptureDeviceNV)</span>
<a name="wglew.h-1148"></a><span class="cp">#define wglReleaseVideoCaptureDeviceNV WGLEW_GET_FUN(__wglewReleaseVideoCaptureDeviceNV)</span>
<a name="wglew.h-1149"></a>
<a name="wglew.h-1150"></a><span class="cp">#define WGLEW_NV_video_capture WGLEW_GET_VAR(__WGLEW_NV_video_capture)</span>
<a name="wglew.h-1151"></a>
<a name="wglew.h-1152"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_video_capture */</span><span class="cp"></span>
<a name="wglew.h-1153"></a>
<a name="wglew.h-1154"></a><span class="cm">/* -------------------------- WGL_NV_video_output -------------------------- */</span>
<a name="wglew.h-1155"></a>
<a name="wglew.h-1156"></a><span class="cp">#ifndef WGL_NV_video_output</span>
<a name="wglew.h-1157"></a><span class="cp">#define WGL_NV_video_output 1</span>
<a name="wglew.h-1158"></a>
<a name="wglew.h-1159"></a><span class="cp">#define WGL_BIND_TO_VIDEO_RGB_NV 0x20C0</span>
<a name="wglew.h-1160"></a><span class="cp">#define WGL_BIND_TO_VIDEO_RGBA_NV 0x20C1</span>
<a name="wglew.h-1161"></a><span class="cp">#define WGL_BIND_TO_VIDEO_RGB_AND_DEPTH_NV 0x20C2</span>
<a name="wglew.h-1162"></a><span class="cp">#define WGL_VIDEO_OUT_COLOR_NV 0x20C3</span>
<a name="wglew.h-1163"></a><span class="cp">#define WGL_VIDEO_OUT_ALPHA_NV 0x20C4</span>
<a name="wglew.h-1164"></a><span class="cp">#define WGL_VIDEO_OUT_DEPTH_NV 0x20C5</span>
<a name="wglew.h-1165"></a><span class="cp">#define WGL_VIDEO_OUT_COLOR_AND_ALPHA_NV 0x20C6</span>
<a name="wglew.h-1166"></a><span class="cp">#define WGL_VIDEO_OUT_COLOR_AND_DEPTH_NV 0x20C7</span>
<a name="wglew.h-1167"></a><span class="cp">#define WGL_VIDEO_OUT_FRAME 0x20C8</span>
<a name="wglew.h-1168"></a><span class="cp">#define WGL_VIDEO_OUT_FIELD_1 0x20C9</span>
<a name="wglew.h-1169"></a><span class="cp">#define WGL_VIDEO_OUT_FIELD_2 0x20CA</span>
<a name="wglew.h-1170"></a><span class="cp">#define WGL_VIDEO_OUT_STACKED_FIELDS_1_2 0x20CB</span>
<a name="wglew.h-1171"></a><span class="cp">#define WGL_VIDEO_OUT_STACKED_FIELDS_2_1 0x20CC</span>
<a name="wglew.h-1172"></a>
<a name="wglew.h-1173"></a><span class="n">DECLARE_HANDLE</span><span class="p">(</span><span class="n">HPVIDEODEV</span><span class="p">);</span>
<a name="wglew.h-1174"></a>
<a name="wglew.h-1175"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLBINDVIDEOIMAGENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPVIDEODEV</span> <span class="n">hVideoDevice</span><span class="p">,</span> <span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iVideoBuffer</span><span class="p">);</span>
<a name="wglew.h-1176"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETVIDEODEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hDC</span><span class="p">,</span> <span class="kt">int</span> <span class="n">numDevices</span><span class="p">,</span> <span class="n">HPVIDEODEV</span><span class="o">*</span> <span class="n">hVideoDevice</span><span class="p">);</span>
<a name="wglew.h-1177"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETVIDEOINFONVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPVIDEODEV</span> <span class="n">hpVideoDevice</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span><span class="o">*</span> <span class="n">pulCounterOutputPbuffer</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="o">*</span><span class="n">pulCounterOutputVideo</span><span class="p">);</span>
<a name="wglew.h-1178"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRELEASEVIDEODEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPVIDEODEV</span> <span class="n">hVideoDevice</span><span class="p">);</span>
<a name="wglew.h-1179"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLRELEASEVIDEOIMAGENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iVideoBuffer</span><span class="p">);</span>
<a name="wglew.h-1180"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSENDPBUFFERTOVIDEONVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HPBUFFERARB</span> <span class="n">hPbuffer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iBufferType</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span><span class="o">*</span> <span class="n">pulCounterPbuffer</span><span class="p">,</span> <span class="n">BOOL</span> <span class="n">bBlock</span><span class="p">);</span>
<a name="wglew.h-1181"></a>
<a name="wglew.h-1182"></a><span class="cp">#define wglBindVideoImageNV WGLEW_GET_FUN(__wglewBindVideoImageNV)</span>
<a name="wglew.h-1183"></a><span class="cp">#define wglGetVideoDeviceNV WGLEW_GET_FUN(__wglewGetVideoDeviceNV)</span>
<a name="wglew.h-1184"></a><span class="cp">#define wglGetVideoInfoNV WGLEW_GET_FUN(__wglewGetVideoInfoNV)</span>
<a name="wglew.h-1185"></a><span class="cp">#define wglReleaseVideoDeviceNV WGLEW_GET_FUN(__wglewReleaseVideoDeviceNV)</span>
<a name="wglew.h-1186"></a><span class="cp">#define wglReleaseVideoImageNV WGLEW_GET_FUN(__wglewReleaseVideoImageNV)</span>
<a name="wglew.h-1187"></a><span class="cp">#define wglSendPbufferToVideoNV WGLEW_GET_FUN(__wglewSendPbufferToVideoNV)</span>
<a name="wglew.h-1188"></a>
<a name="wglew.h-1189"></a><span class="cp">#define WGLEW_NV_video_output WGLEW_GET_VAR(__WGLEW_NV_video_output)</span>
<a name="wglew.h-1190"></a>
<a name="wglew.h-1191"></a><span class="cp">#endif </span><span class="cm">/* WGL_NV_video_output */</span><span class="cp"></span>
<a name="wglew.h-1192"></a>
<a name="wglew.h-1193"></a><span class="cm">/* -------------------------- WGL_OML_sync_control ------------------------- */</span>
<a name="wglew.h-1194"></a>
<a name="wglew.h-1195"></a><span class="cp">#ifndef WGL_OML_sync_control</span>
<a name="wglew.h-1196"></a><span class="cp">#define WGL_OML_sync_control 1</span>
<a name="wglew.h-1197"></a>
<a name="wglew.h-1198"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETMSCRATEOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">INT32</span><span class="o">*</span> <span class="n">numerator</span><span class="p">,</span> <span class="n">INT32</span> <span class="o">*</span><span class="n">denominator</span><span class="p">);</span>
<a name="wglew.h-1199"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLGETSYNCVALUESOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">INT64</span><span class="o">*</span> <span class="n">ust</span><span class="p">,</span> <span class="n">INT64</span> <span class="o">*</span><span class="n">msc</span><span class="p">,</span> <span class="n">INT64</span> <span class="o">*</span><span class="n">sbc</span><span class="p">);</span>
<a name="wglew.h-1200"></a><span class="k">typedef</span> <span class="nf">INT64</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSWAPBUFFERSMSCOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">target_msc</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">divisor</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">remainder</span><span class="p">);</span>
<a name="wglew.h-1201"></a><span class="k">typedef</span> <span class="nf">INT64</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLSWAPLAYERBUFFERSMSCOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">INT</span> <span class="n">fuPlanes</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">target_msc</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">divisor</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">remainder</span><span class="p">);</span>
<a name="wglew.h-1202"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLWAITFORMSCOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">target_msc</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">divisor</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">remainder</span><span class="p">,</span> <span class="n">INT64</span><span class="o">*</span> <span class="n">ust</span><span class="p">,</span> <span class="n">INT64</span> <span class="o">*</span><span class="n">msc</span><span class="p">,</span> <span class="n">INT64</span> <span class="o">*</span><span class="n">sbc</span><span class="p">);</span>
<a name="wglew.h-1203"></a><span class="k">typedef</span> <span class="nf">BOOL</span> <span class="p">(</span><span class="n">WINAPI</span> <span class="o">*</span> <span class="n">PFNWGLWAITFORSBCOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">HDC</span> <span class="n">hdc</span><span class="p">,</span> <span class="n">INT64</span> <span class="n">target_sbc</span><span class="p">,</span> <span class="n">INT64</span><span class="o">*</span> <span class="n">ust</span><span class="p">,</span> <span class="n">INT64</span> <span class="o">*</span><span class="n">msc</span><span class="p">,</span> <span class="n">INT64</span> <span class="o">*</span><span class="n">sbc</span><span class="p">);</span>
<a name="wglew.h-1204"></a>
<a name="wglew.h-1205"></a><span class="cp">#define wglGetMscRateOML WGLEW_GET_FUN(__wglewGetMscRateOML)</span>
<a name="wglew.h-1206"></a><span class="cp">#define wglGetSyncValuesOML WGLEW_GET_FUN(__wglewGetSyncValuesOML)</span>
<a name="wglew.h-1207"></a><span class="cp">#define wglSwapBuffersMscOML WGLEW_GET_FUN(__wglewSwapBuffersMscOML)</span>
<a name="wglew.h-1208"></a><span class="cp">#define wglSwapLayerBuffersMscOML WGLEW_GET_FUN(__wglewSwapLayerBuffersMscOML)</span>
<a name="wglew.h-1209"></a><span class="cp">#define wglWaitForMscOML WGLEW_GET_FUN(__wglewWaitForMscOML)</span>
<a name="wglew.h-1210"></a><span class="cp">#define wglWaitForSbcOML WGLEW_GET_FUN(__wglewWaitForSbcOML)</span>
<a name="wglew.h-1211"></a>
<a name="wglew.h-1212"></a><span class="cp">#define WGLEW_OML_sync_control WGLEW_GET_VAR(__WGLEW_OML_sync_control)</span>
<a name="wglew.h-1213"></a>
<a name="wglew.h-1214"></a><span class="cp">#endif </span><span class="cm">/* WGL_OML_sync_control */</span><span class="cp"></span>
<a name="wglew.h-1215"></a>
<a name="wglew.h-1216"></a><span class="cm">/* ------------------------------------------------------------------------- */</span>
<a name="wglew.h-1217"></a>
<a name="wglew.h-1218"></a><span class="cp">#define WGLEW_FUN_EXPORT GLEW_FUN_EXPORT</span>
<a name="wglew.h-1219"></a><span class="cp">#define WGLEW_VAR_EXPORT GLEW_VAR_EXPORT</span>
<a name="wglew.h-1220"></a>
<a name="wglew.h-1221"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSETSTEREOEMITTERSTATE3DLPROC</span> <span class="n">__wglewSetStereoEmitterState3DL</span><span class="p">;</span>
<a name="wglew.h-1222"></a>
<a name="wglew.h-1223"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBLITCONTEXTFRAMEBUFFERAMDPROC</span> <span class="n">__wglewBlitContextFramebufferAMD</span><span class="p">;</span>
<a name="wglew.h-1224"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEASSOCIATEDCONTEXTAMDPROC</span> <span class="n">__wglewCreateAssociatedContextAMD</span><span class="p">;</span>
<a name="wglew.h-1225"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEASSOCIATEDCONTEXTATTRIBSAMDPROC</span> <span class="n">__wglewCreateAssociatedContextAttribsAMD</span><span class="p">;</span>
<a name="wglew.h-1226"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDELETEASSOCIATEDCONTEXTAMDPROC</span> <span class="n">__wglewDeleteAssociatedContextAMD</span><span class="p">;</span>
<a name="wglew.h-1227"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETCONTEXTGPUIDAMDPROC</span> <span class="n">__wglewGetContextGPUIDAMD</span><span class="p">;</span>
<a name="wglew.h-1228"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETCURRENTASSOCIATEDCONTEXTAMDPROC</span> <span class="n">__wglewGetCurrentAssociatedContextAMD</span><span class="p">;</span>
<a name="wglew.h-1229"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGPUIDSAMDPROC</span> <span class="n">__wglewGetGPUIDsAMD</span><span class="p">;</span>
<a name="wglew.h-1230"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGPUINFOAMDPROC</span> <span class="n">__wglewGetGPUInfoAMD</span><span class="p">;</span>
<a name="wglew.h-1231"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLMAKEASSOCIATEDCONTEXTCURRENTAMDPROC</span> <span class="n">__wglewMakeAssociatedContextCurrentAMD</span><span class="p">;</span>
<a name="wglew.h-1232"></a>
<a name="wglew.h-1233"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEBUFFERREGIONARBPROC</span> <span class="n">__wglewCreateBufferRegionARB</span><span class="p">;</span>
<a name="wglew.h-1234"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDELETEBUFFERREGIONARBPROC</span> <span class="n">__wglewDeleteBufferRegionARB</span><span class="p">;</span>
<a name="wglew.h-1235"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRESTOREBUFFERREGIONARBPROC</span> <span class="n">__wglewRestoreBufferRegionARB</span><span class="p">;</span>
<a name="wglew.h-1236"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSAVEBUFFERREGIONARBPROC</span> <span class="n">__wglewSaveBufferRegionARB</span><span class="p">;</span>
<a name="wglew.h-1237"></a>
<a name="wglew.h-1238"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATECONTEXTATTRIBSARBPROC</span> <span class="n">__wglewCreateContextAttribsARB</span><span class="p">;</span>
<a name="wglew.h-1239"></a>
<a name="wglew.h-1240"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETEXTENSIONSSTRINGARBPROC</span> <span class="n">__wglewGetExtensionsStringARB</span><span class="p">;</span>
<a name="wglew.h-1241"></a>
<a name="wglew.h-1242"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETCURRENTREADDCARBPROC</span> <span class="n">__wglewGetCurrentReadDCARB</span><span class="p">;</span>
<a name="wglew.h-1243"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLMAKECONTEXTCURRENTARBPROC</span> <span class="n">__wglewMakeContextCurrentARB</span><span class="p">;</span>
<a name="wglew.h-1244"></a>
<a name="wglew.h-1245"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEPBUFFERARBPROC</span> <span class="n">__wglewCreatePbufferARB</span><span class="p">;</span>
<a name="wglew.h-1246"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDESTROYPBUFFERARBPROC</span> <span class="n">__wglewDestroyPbufferARB</span><span class="p">;</span>
<a name="wglew.h-1247"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETPBUFFERDCARBPROC</span> <span class="n">__wglewGetPbufferDCARB</span><span class="p">;</span>
<a name="wglew.h-1248"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYPBUFFERARBPROC</span> <span class="n">__wglewQueryPbufferARB</span><span class="p">;</span>
<a name="wglew.h-1249"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRELEASEPBUFFERDCARBPROC</span> <span class="n">__wglewReleasePbufferDCARB</span><span class="p">;</span>
<a name="wglew.h-1250"></a>
<a name="wglew.h-1251"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCHOOSEPIXELFORMATARBPROC</span> <span class="n">__wglewChoosePixelFormatARB</span><span class="p">;</span>
<a name="wglew.h-1252"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBFVARBPROC</span> <span class="n">__wglewGetPixelFormatAttribfvARB</span><span class="p">;</span>
<a name="wglew.h-1253"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBIVARBPROC</span> <span class="n">__wglewGetPixelFormatAttribivARB</span><span class="p">;</span>
<a name="wglew.h-1254"></a>
<a name="wglew.h-1255"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBINDTEXIMAGEARBPROC</span> <span class="n">__wglewBindTexImageARB</span><span class="p">;</span>
<a name="wglew.h-1256"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRELEASETEXIMAGEARBPROC</span> <span class="n">__wglewReleaseTexImageARB</span><span class="p">;</span>
<a name="wglew.h-1257"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSETPBUFFERATTRIBARBPROC</span> <span class="n">__wglewSetPbufferAttribARB</span><span class="p">;</span>
<a name="wglew.h-1258"></a>
<a name="wglew.h-1259"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBINDDISPLAYCOLORTABLEEXTPROC</span> <span class="n">__wglewBindDisplayColorTableEXT</span><span class="p">;</span>
<a name="wglew.h-1260"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEDISPLAYCOLORTABLEEXTPROC</span> <span class="n">__wglewCreateDisplayColorTableEXT</span><span class="p">;</span>
<a name="wglew.h-1261"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDESTROYDISPLAYCOLORTABLEEXTPROC</span> <span class="n">__wglewDestroyDisplayColorTableEXT</span><span class="p">;</span>
<a name="wglew.h-1262"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLLOADDISPLAYCOLORTABLEEXTPROC</span> <span class="n">__wglewLoadDisplayColorTableEXT</span><span class="p">;</span>
<a name="wglew.h-1263"></a>
<a name="wglew.h-1264"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETEXTENSIONSSTRINGEXTPROC</span> <span class="n">__wglewGetExtensionsStringEXT</span><span class="p">;</span>
<a name="wglew.h-1265"></a>
<a name="wglew.h-1266"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETCURRENTREADDCEXTPROC</span> <span class="n">__wglewGetCurrentReadDCEXT</span><span class="p">;</span>
<a name="wglew.h-1267"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLMAKECONTEXTCURRENTEXTPROC</span> <span class="n">__wglewMakeContextCurrentEXT</span><span class="p">;</span>
<a name="wglew.h-1268"></a>
<a name="wglew.h-1269"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEPBUFFEREXTPROC</span> <span class="n">__wglewCreatePbufferEXT</span><span class="p">;</span>
<a name="wglew.h-1270"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDESTROYPBUFFEREXTPROC</span> <span class="n">__wglewDestroyPbufferEXT</span><span class="p">;</span>
<a name="wglew.h-1271"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETPBUFFERDCEXTPROC</span> <span class="n">__wglewGetPbufferDCEXT</span><span class="p">;</span>
<a name="wglew.h-1272"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYPBUFFEREXTPROC</span> <span class="n">__wglewQueryPbufferEXT</span><span class="p">;</span>
<a name="wglew.h-1273"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRELEASEPBUFFERDCEXTPROC</span> <span class="n">__wglewReleasePbufferDCEXT</span><span class="p">;</span>
<a name="wglew.h-1274"></a>
<a name="wglew.h-1275"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCHOOSEPIXELFORMATEXTPROC</span> <span class="n">__wglewChoosePixelFormatEXT</span><span class="p">;</span>
<a name="wglew.h-1276"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBFVEXTPROC</span> <span class="n">__wglewGetPixelFormatAttribfvEXT</span><span class="p">;</span>
<a name="wglew.h-1277"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETPIXELFORMATATTRIBIVEXTPROC</span> <span class="n">__wglewGetPixelFormatAttribivEXT</span><span class="p">;</span>
<a name="wglew.h-1278"></a>
<a name="wglew.h-1279"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETSWAPINTERVALEXTPROC</span> <span class="n">__wglewGetSwapIntervalEXT</span><span class="p">;</span>
<a name="wglew.h-1280"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSWAPINTERVALEXTPROC</span> <span class="n">__wglewSwapIntervalEXT</span><span class="p">;</span>
<a name="wglew.h-1281"></a>
<a name="wglew.h-1282"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETDIGITALVIDEOPARAMETERSI3DPROC</span> <span class="n">__wglewGetDigitalVideoParametersI3D</span><span class="p">;</span>
<a name="wglew.h-1283"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSETDIGITALVIDEOPARAMETERSI3DPROC</span> <span class="n">__wglewSetDigitalVideoParametersI3D</span><span class="p">;</span>
<a name="wglew.h-1284"></a>
<a name="wglew.h-1285"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGAMMATABLEI3DPROC</span> <span class="n">__wglewGetGammaTableI3D</span><span class="p">;</span>
<a name="wglew.h-1286"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGAMMATABLEPARAMETERSI3DPROC</span> <span class="n">__wglewGetGammaTableParametersI3D</span><span class="p">;</span>
<a name="wglew.h-1287"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSETGAMMATABLEI3DPROC</span> <span class="n">__wglewSetGammaTableI3D</span><span class="p">;</span>
<a name="wglew.h-1288"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSETGAMMATABLEPARAMETERSI3DPROC</span> <span class="n">__wglewSetGammaTableParametersI3D</span><span class="p">;</span>
<a name="wglew.h-1289"></a>
<a name="wglew.h-1290"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDISABLEGENLOCKI3DPROC</span> <span class="n">__wglewDisableGenlockI3D</span><span class="p">;</span>
<a name="wglew.h-1291"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENABLEGENLOCKI3DPROC</span> <span class="n">__wglewEnableGenlockI3D</span><span class="p">;</span>
<a name="wglew.h-1292"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGENLOCKSAMPLERATEI3DPROC</span> <span class="n">__wglewGenlockSampleRateI3D</span><span class="p">;</span>
<a name="wglew.h-1293"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGENLOCKSOURCEDELAYI3DPROC</span> <span class="n">__wglewGenlockSourceDelayI3D</span><span class="p">;</span>
<a name="wglew.h-1294"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGENLOCKSOURCEEDGEI3DPROC</span> <span class="n">__wglewGenlockSourceEdgeI3D</span><span class="p">;</span>
<a name="wglew.h-1295"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGENLOCKSOURCEI3DPROC</span> <span class="n">__wglewGenlockSourceI3D</span><span class="p">;</span>
<a name="wglew.h-1296"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGENLOCKSAMPLERATEI3DPROC</span> <span class="n">__wglewGetGenlockSampleRateI3D</span><span class="p">;</span>
<a name="wglew.h-1297"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGENLOCKSOURCEDELAYI3DPROC</span> <span class="n">__wglewGetGenlockSourceDelayI3D</span><span class="p">;</span>
<a name="wglew.h-1298"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGENLOCKSOURCEEDGEI3DPROC</span> <span class="n">__wglewGetGenlockSourceEdgeI3D</span><span class="p">;</span>
<a name="wglew.h-1299"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETGENLOCKSOURCEI3DPROC</span> <span class="n">__wglewGetGenlockSourceI3D</span><span class="p">;</span>
<a name="wglew.h-1300"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLISENABLEDGENLOCKI3DPROC</span> <span class="n">__wglewIsEnabledGenlockI3D</span><span class="p">;</span>
<a name="wglew.h-1301"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYGENLOCKMAXSOURCEDELAYI3DPROC</span> <span class="n">__wglewQueryGenlockMaxSourceDelayI3D</span><span class="p">;</span>
<a name="wglew.h-1302"></a>
<a name="wglew.h-1303"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLASSOCIATEIMAGEBUFFEREVENTSI3DPROC</span> <span class="n">__wglewAssociateImageBufferEventsI3D</span><span class="p">;</span>
<a name="wglew.h-1304"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEIMAGEBUFFERI3DPROC</span> <span class="n">__wglewCreateImageBufferI3D</span><span class="p">;</span>
<a name="wglew.h-1305"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDESTROYIMAGEBUFFERI3DPROC</span> <span class="n">__wglewDestroyImageBufferI3D</span><span class="p">;</span>
<a name="wglew.h-1306"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRELEASEIMAGEBUFFEREVENTSI3DPROC</span> <span class="n">__wglewReleaseImageBufferEventsI3D</span><span class="p">;</span>
<a name="wglew.h-1307"></a>
<a name="wglew.h-1308"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDISABLEFRAMELOCKI3DPROC</span> <span class="n">__wglewDisableFrameLockI3D</span><span class="p">;</span>
<a name="wglew.h-1309"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENABLEFRAMELOCKI3DPROC</span> <span class="n">__wglewEnableFrameLockI3D</span><span class="p">;</span>
<a name="wglew.h-1310"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLISENABLEDFRAMELOCKI3DPROC</span> <span class="n">__wglewIsEnabledFrameLockI3D</span><span class="p">;</span>
<a name="wglew.h-1311"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYFRAMELOCKMASTERI3DPROC</span> <span class="n">__wglewQueryFrameLockMasterI3D</span><span class="p">;</span>
<a name="wglew.h-1312"></a>
<a name="wglew.h-1313"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBEGINFRAMETRACKINGI3DPROC</span> <span class="n">__wglewBeginFrameTrackingI3D</span><span class="p">;</span>
<a name="wglew.h-1314"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENDFRAMETRACKINGI3DPROC</span> <span class="n">__wglewEndFrameTrackingI3D</span><span class="p">;</span>
<a name="wglew.h-1315"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETFRAMEUSAGEI3DPROC</span> <span class="n">__wglewGetFrameUsageI3D</span><span class="p">;</span>
<a name="wglew.h-1316"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYFRAMETRACKINGI3DPROC</span> <span class="n">__wglewQueryFrameTrackingI3D</span><span class="p">;</span>
<a name="wglew.h-1317"></a>
<a name="wglew.h-1318"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXCLOSEDEVICENVPROC</span> <span class="n">__wglewDXCloseDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1319"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXLOCKOBJECTSNVPROC</span> <span class="n">__wglewDXLockObjectsNV</span><span class="p">;</span>
<a name="wglew.h-1320"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXOBJECTACCESSNVPROC</span> <span class="n">__wglewDXObjectAccessNV</span><span class="p">;</span>
<a name="wglew.h-1321"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXOPENDEVICENVPROC</span> <span class="n">__wglewDXOpenDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1322"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXREGISTEROBJECTNVPROC</span> <span class="n">__wglewDXRegisterObjectNV</span><span class="p">;</span>
<a name="wglew.h-1323"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXSETRESOURCESHAREHANDLENVPROC</span> <span class="n">__wglewDXSetResourceShareHandleNV</span><span class="p">;</span>
<a name="wglew.h-1324"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXUNLOCKOBJECTSNVPROC</span> <span class="n">__wglewDXUnlockObjectsNV</span><span class="p">;</span>
<a name="wglew.h-1325"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDXUNREGISTEROBJECTNVPROC</span> <span class="n">__wglewDXUnregisterObjectNV</span><span class="p">;</span>
<a name="wglew.h-1326"></a>
<a name="wglew.h-1327"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCOPYIMAGESUBDATANVPROC</span> <span class="n">__wglewCopyImageSubDataNV</span><span class="p">;</span>
<a name="wglew.h-1328"></a>
<a name="wglew.h-1329"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDELAYBEFORESWAPNVPROC</span> <span class="n">__wglewDelayBeforeSwapNV</span><span class="p">;</span>
<a name="wglew.h-1330"></a>
<a name="wglew.h-1331"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLCREATEAFFINITYDCNVPROC</span> <span class="n">__wglewCreateAffinityDCNV</span><span class="p">;</span>
<a name="wglew.h-1332"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLDELETEDCNVPROC</span> <span class="n">__wglewDeleteDCNV</span><span class="p">;</span>
<a name="wglew.h-1333"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENUMGPUDEVICESNVPROC</span> <span class="n">__wglewEnumGpuDevicesNV</span><span class="p">;</span>
<a name="wglew.h-1334"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENUMGPUSFROMAFFINITYDCNVPROC</span> <span class="n">__wglewEnumGpusFromAffinityDCNV</span><span class="p">;</span>
<a name="wglew.h-1335"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENUMGPUSNVPROC</span> <span class="n">__wglewEnumGpusNV</span><span class="p">;</span>
<a name="wglew.h-1336"></a>
<a name="wglew.h-1337"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBINDVIDEODEVICENVPROC</span> <span class="n">__wglewBindVideoDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1338"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENUMERATEVIDEODEVICESNVPROC</span> <span class="n">__wglewEnumerateVideoDevicesNV</span><span class="p">;</span>
<a name="wglew.h-1339"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYCURRENTCONTEXTNVPROC</span> <span class="n">__wglewQueryCurrentContextNV</span><span class="p">;</span>
<a name="wglew.h-1340"></a>
<a name="wglew.h-1341"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBINDSWAPBARRIERNVPROC</span> <span class="n">__wglewBindSwapBarrierNV</span><span class="p">;</span>
<a name="wglew.h-1342"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLJOINSWAPGROUPNVPROC</span> <span class="n">__wglewJoinSwapGroupNV</span><span class="p">;</span>
<a name="wglew.h-1343"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYFRAMECOUNTNVPROC</span> <span class="n">__wglewQueryFrameCountNV</span><span class="p">;</span>
<a name="wglew.h-1344"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYMAXSWAPGROUPSNVPROC</span> <span class="n">__wglewQueryMaxSwapGroupsNV</span><span class="p">;</span>
<a name="wglew.h-1345"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYSWAPGROUPNVPROC</span> <span class="n">__wglewQuerySwapGroupNV</span><span class="p">;</span>
<a name="wglew.h-1346"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRESETFRAMECOUNTNVPROC</span> <span class="n">__wglewResetFrameCountNV</span><span class="p">;</span>
<a name="wglew.h-1347"></a>
<a name="wglew.h-1348"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLALLOCATEMEMORYNVPROC</span> <span class="n">__wglewAllocateMemoryNV</span><span class="p">;</span>
<a name="wglew.h-1349"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLFREEMEMORYNVPROC</span> <span class="n">__wglewFreeMemoryNV</span><span class="p">;</span>
<a name="wglew.h-1350"></a>
<a name="wglew.h-1351"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBINDVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__wglewBindVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1352"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLENUMERATEVIDEOCAPTUREDEVICESNVPROC</span> <span class="n">__wglewEnumerateVideoCaptureDevicesNV</span><span class="p">;</span>
<a name="wglew.h-1353"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLLOCKVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__wglewLockVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1354"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLQUERYVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__wglewQueryVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1355"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRELEASEVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__wglewReleaseVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1356"></a>
<a name="wglew.h-1357"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLBINDVIDEOIMAGENVPROC</span> <span class="n">__wglewBindVideoImageNV</span><span class="p">;</span>
<a name="wglew.h-1358"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETVIDEODEVICENVPROC</span> <span class="n">__wglewGetVideoDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1359"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETVIDEOINFONVPROC</span> <span class="n">__wglewGetVideoInfoNV</span><span class="p">;</span>
<a name="wglew.h-1360"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRELEASEVIDEODEVICENVPROC</span> <span class="n">__wglewReleaseVideoDeviceNV</span><span class="p">;</span>
<a name="wglew.h-1361"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLRELEASEVIDEOIMAGENVPROC</span> <span class="n">__wglewReleaseVideoImageNV</span><span class="p">;</span>
<a name="wglew.h-1362"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSENDPBUFFERTOVIDEONVPROC</span> <span class="n">__wglewSendPbufferToVideoNV</span><span class="p">;</span>
<a name="wglew.h-1363"></a>
<a name="wglew.h-1364"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETMSCRATEOMLPROC</span> <span class="n">__wglewGetMscRateOML</span><span class="p">;</span>
<a name="wglew.h-1365"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLGETSYNCVALUESOMLPROC</span> <span class="n">__wglewGetSyncValuesOML</span><span class="p">;</span>
<a name="wglew.h-1366"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSWAPBUFFERSMSCOMLPROC</span> <span class="n">__wglewSwapBuffersMscOML</span><span class="p">;</span>
<a name="wglew.h-1367"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLSWAPLAYERBUFFERSMSCOMLPROC</span> <span class="n">__wglewSwapLayerBuffersMscOML</span><span class="p">;</span>
<a name="wglew.h-1368"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLWAITFORMSCOMLPROC</span> <span class="n">__wglewWaitForMscOML</span><span class="p">;</span>
<a name="wglew.h-1369"></a><span class="n">WGLEW_FUN_EXPORT</span> <span class="n">PFNWGLWAITFORSBCOMLPROC</span> <span class="n">__wglewWaitForSbcOML</span><span class="p">;</span>
<a name="wglew.h-1370"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_3DFX_multisample</span><span class="p">;</span>
<a name="wglew.h-1371"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_3DL_stereo_control</span><span class="p">;</span>
<a name="wglew.h-1372"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_AMD_gpu_association</span><span class="p">;</span>
<a name="wglew.h-1373"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_buffer_region</span><span class="p">;</span>
<a name="wglew.h-1374"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_context_flush_control</span><span class="p">;</span>
<a name="wglew.h-1375"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_create_context</span><span class="p">;</span>
<a name="wglew.h-1376"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_create_context_no_error</span><span class="p">;</span>
<a name="wglew.h-1377"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_create_context_profile</span><span class="p">;</span>
<a name="wglew.h-1378"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_create_context_robustness</span><span class="p">;</span>
<a name="wglew.h-1379"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_extensions_string</span><span class="p">;</span>
<a name="wglew.h-1380"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_framebuffer_sRGB</span><span class="p">;</span>
<a name="wglew.h-1381"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_make_current_read</span><span class="p">;</span>
<a name="wglew.h-1382"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_multisample</span><span class="p">;</span>
<a name="wglew.h-1383"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_pbuffer</span><span class="p">;</span>
<a name="wglew.h-1384"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_pixel_format</span><span class="p">;</span>
<a name="wglew.h-1385"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_pixel_format_float</span><span class="p">;</span>
<a name="wglew.h-1386"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_render_texture</span><span class="p">;</span>
<a name="wglew.h-1387"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_robustness_application_isolation</span><span class="p">;</span>
<a name="wglew.h-1388"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ARB_robustness_share_group_isolation</span><span class="p">;</span>
<a name="wglew.h-1389"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ATI_pixel_format_float</span><span class="p">;</span>
<a name="wglew.h-1390"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_ATI_render_texture_rectangle</span><span class="p">;</span>
<a name="wglew.h-1391"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_colorspace</span><span class="p">;</span>
<a name="wglew.h-1392"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_create_context_es2_profile</span><span class="p">;</span>
<a name="wglew.h-1393"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_create_context_es_profile</span><span class="p">;</span>
<a name="wglew.h-1394"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_depth_float</span><span class="p">;</span>
<a name="wglew.h-1395"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_display_color_table</span><span class="p">;</span>
<a name="wglew.h-1396"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_extensions_string</span><span class="p">;</span>
<a name="wglew.h-1397"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_framebuffer_sRGB</span><span class="p">;</span>
<a name="wglew.h-1398"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_make_current_read</span><span class="p">;</span>
<a name="wglew.h-1399"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_multisample</span><span class="p">;</span>
<a name="wglew.h-1400"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_pbuffer</span><span class="p">;</span>
<a name="wglew.h-1401"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_pixel_format</span><span class="p">;</span>
<a name="wglew.h-1402"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_pixel_format_packed_float</span><span class="p">;</span>
<a name="wglew.h-1403"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_swap_control</span><span class="p">;</span>
<a name="wglew.h-1404"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_EXT_swap_control_tear</span><span class="p">;</span>
<a name="wglew.h-1405"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_I3D_digital_video_control</span><span class="p">;</span>
<a name="wglew.h-1406"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_I3D_gamma</span><span class="p">;</span>
<a name="wglew.h-1407"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_I3D_genlock</span><span class="p">;</span>
<a name="wglew.h-1408"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_I3D_image_buffer</span><span class="p">;</span>
<a name="wglew.h-1409"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_I3D_swap_frame_lock</span><span class="p">;</span>
<a name="wglew.h-1410"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_I3D_swap_frame_usage</span><span class="p">;</span>
<a name="wglew.h-1411"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_DX_interop</span><span class="p">;</span>
<a name="wglew.h-1412"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_DX_interop2</span><span class="p">;</span>
<a name="wglew.h-1413"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_copy_image</span><span class="p">;</span>
<a name="wglew.h-1414"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_delay_before_swap</span><span class="p">;</span>
<a name="wglew.h-1415"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_float_buffer</span><span class="p">;</span>
<a name="wglew.h-1416"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_gpu_affinity</span><span class="p">;</span>
<a name="wglew.h-1417"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_multisample_coverage</span><span class="p">;</span>
<a name="wglew.h-1418"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_present_video</span><span class="p">;</span>
<a name="wglew.h-1419"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_render_depth_texture</span><span class="p">;</span>
<a name="wglew.h-1420"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_render_texture_rectangle</span><span class="p">;</span>
<a name="wglew.h-1421"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_swap_group</span><span class="p">;</span>
<a name="wglew.h-1422"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_vertex_array_range</span><span class="p">;</span>
<a name="wglew.h-1423"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_video_capture</span><span class="p">;</span>
<a name="wglew.h-1424"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_NV_video_output</span><span class="p">;</span>
<a name="wglew.h-1425"></a><span class="n">WGLEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__WGLEW_OML_sync_control</span><span class="p">;</span>
<a name="wglew.h-1426"></a><span class="cm">/* ------------------------------------------------------------------------- */</span>
<a name="wglew.h-1427"></a>
<a name="wglew.h-1428"></a><span class="n">GLEWAPI</span> <span class="n">GLenum</span> <span class="n">GLEWAPIENTRY</span> <span class="nf">wglewInit</span> <span class="p">();</span>
<a name="wglew.h-1429"></a><span class="n">GLEWAPI</span> <span class="n">GLboolean</span> <span class="n">GLEWAPIENTRY</span> <span class="nf">wglewIsSupported</span> <span class="p">(</span><span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">name</span><span class="p">);</span>
<a name="wglew.h-1430"></a>
<a name="wglew.h-1431"></a><span class="cp">#ifndef WGLEW_GET_VAR</span>
<a name="wglew.h-1432"></a><span class="cp">#define WGLEW_GET_VAR(x) (*(const GLboolean*)&amp;x)</span>
<a name="wglew.h-1433"></a><span class="cp">#endif</span>
<a name="wglew.h-1434"></a>
<a name="wglew.h-1435"></a><span class="cp">#ifndef WGLEW_GET_FUN</span>
<a name="wglew.h-1436"></a><span class="cp">#define WGLEW_GET_FUN(x) x</span>
<a name="wglew.h-1437"></a><span class="cp">#endif</span>
<a name="wglew.h-1438"></a>
<a name="wglew.h-1439"></a><span class="n">GLEWAPI</span> <span class="n">GLboolean</span> <span class="n">GLEWAPIENTRY</span> <span class="nf">wglewGetExtension</span> <span class="p">(</span><span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">name</span><span class="p">);</span>
<a name="wglew.h-1440"></a>
<a name="wglew.h-1441"></a><span class="cp">#ifdef __cplusplus</span>
<a name="wglew.h-1442"></a><span class="p">}</span>
<a name="wglew.h-1443"></a><span class="cp">#endif</span>
<a name="wglew.h-1444"></a>
<a name="wglew.h-1445"></a><span class="cp">#undef GLEWAPI</span>
<a name="wglew.h-1446"></a>
<a name="wglew.h-1447"></a><span class="cp">#endif </span><span class="cm">/* __wglew_h__ */</span><span class="cp"></span>
</pre></div>
</td></tr></table>
    </div>
  


        </div>
        
      </div>
    </div>
  </div>
  
  <div data-module="source/set-changeset" data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"></div>



  
    
    
    
  
  

  </div>

        
        
        
          
    
    
  
        
      </div>
    </div>
    <div id="code-search-cta"></div>
  </div>

      </div>
    </div>
  
</div>

<div id="adg3-dialog"></div>


  

<div data-module="components/mentions/index">
  
    
    
  
  
    
    
  
  
    
    
  
</div>
<div data-module="components/typeahead/emoji/index">
  
    
    
  
</div>

<div data-module="components/repo-typeahead/index">
  
    
    
  
</div>

    
    
  

    
    
  

    
    
  

    
    
  


  


    
    
  

    
    
  


  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  


  
  
  <aui-inline-dialog
    id="help-menu-dialog"
    data-aui-alignment="bottom right"

    
    data-aui-alignment-static="true"
    data-module="header/help-menu"
    responds-to="toggle"
    aria-hidden="true">

  <div id="help-menu-section">
    <h1 class="help-menu-heading">Help</h1>

    <form id="help-menu-search-form" class="aui" target="_blank" method="get"
        action="https://support.atlassian.com/customer/search">
      <span id="help-menu-search-icon" class="aui-icon aui-icon-large aui-iconfont-search"></span>
      <input id="help-menu-search-form-input" name="q" class="text" type="text" placeholder="Ask a question">
    </form>

    <ul id="help-menu-links">
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationHome"
            href="https://confluence.atlassian.com/x/bgozDQ" target="_blank">
          Online help
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="GitTutorials"
            href="https://www.atlassian.com/git?utm_source=bitbucket&amp;utm_medium=link&amp;utm_campaign=help_dropdown&amp;utm_content=learn_git"
            target="_blank">
          Learn Git
        </a>
      </li>
      <li>
        <a id="keyboard-shortcuts-link"
           href="#">Keyboard shortcuts</a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationTutorials"
            href="https://confluence.atlassian.com/x/Q4sFLQ" target="_blank">
          Bitbucket tutorials
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="SiteStatus"
            href="https://status.bitbucket.org/" target="_blank">
          Site status
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="Home"
            href="https://support.atlassian.com/bitbucket-cloud/" target="_blank">
          Support
        </a>
      </li>
    </ul>
  </div>
</aui-inline-dialog>
  


  <div class="omnibar" data-module="components/omnibar/index">
    <form class="omnibar-form aui"></form>
  </div>
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  





  

  <div class="_mustache-templates">
    
      <script id="branch-checkout-template" type="text/html">
        

<div id="checkout-branch-contents">
  <div class="command-line">
    <p>
      Check out this branch on your local machine to begin working on it.
    </p>
    <input type="text" class="checkout-command" readonly="readonly"
        
           value="git fetch && git checkout [[branchName]]"
        
        >
  </div>
  
    <div class="sourcetree-callout clone-in-sourcetree"
  data-module="components/clone/clone-in-sourcetree"
  data-https-url="https://Yanosik@bitbucket.org/Yanosik/zapart.git"
  data-ssh-url="git@bitbucket.org:Yanosik/zapart.git">

  <div>
    <button class="aui-button aui-button-primary">
      
        Check out in Sourcetree
      
    </button>
  </div>

  <p class="windows-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_win" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Windows.
    
  </p>
  <p class="mac-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_mac" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Mac.
    
  </p>
</div>
  
</div>

      </script>
    
      <script id="branch-dialog-template" type="text/html">
        

<div class="tabbed-filter-widget branch-dialog">
  <div class="tabbed-filter">
    <input placeholder="Filter branches" class="filter-box" autosave="branch-dropdown-29632644" type="text">
    [[^ignoreTags]]
      <div class="aui-tabs horizontal-tabs aui-tabs-disabled filter-tabs">
        <ul class="tabs-menu">
          <li class="menu-item active-tab"><a href="#branches">Branches</a></li>
          <li class="menu-item"><a href="#tags">Tags</a></li>
        </ul>
      </div>
    [[/ignoreTags]]
  </div>
  
    <div class="tab-pane active-pane" id="branches" data-filter-placeholder="Filter branches">
      <ol class="filter-list">
        <li class="empty-msg">No matching branches</li>
        [[#branches]]
          
            [[#hasMultipleHeads]]
              [[#heads]]
                <li class="comprev filter-item">
                  <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/wglew.h?at=[[safeName]]"
                     title="[[name]]">
                    [[name]] ([[shortChangeset]])
                  </a>
                </li>
              [[/heads]]
            [[/hasMultipleHeads]]
            [[^hasMultipleHeads]]
              <li class="comprev filter-item">
                <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/wglew.h?at=[[safeName]]" title="[[name]]">
                  [[name]]
                </a>
              </li>
            [[/hasMultipleHeads]]
          
        [[/branches]]
      </ol>
    </div>
    <div class="tab-pane" id="tags" data-filter-placeholder="Filter tags">
      <ol class="filter-list">
        <li class="empty-msg">No matching tags</li>
        [[#tags]]
          <li class="comprev filter-item">
            <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/wglew.h?at=[[safeName]]" title="[[name]]">
              [[name]]
            </a>
          </li>
        [[/tags]]
      </ol>
    </div>
  
</div>

      </script>
    
      <script id="image-upload-template" type="text/html">
        

<form id="upload-image" method="POST"
    
      action="/xhr/Yanosik/zapart/image-upload/"
    >
  <input type='hidden' name='csrfmiddlewaretoken' value='RzQhUNgZtP5HbXohOC12D1kxITq4EQbZZSMRcxGihKYs2lv6qlK9zQfaKWoGOh7I' />

  <div class="drop-target">
    <p class="centered">Drag image here</p>
  </div>

  
  <div>
    <button class="aui-button click-target">Select an image</button>
    <input name="file" type="file" class="hidden file-target"
           accept="image/jpeg, image/gif, image/png" />
    <input type="submit" class="hidden">
  </div>
</form>


      </script>
    
      <script id="mention-result" type="text/html">
        
<span class="mention-result">
  <span class="aui-avatar aui-avatar-small mention-result--avatar">
    <span class="aui-avatar-inner">
      <img src="[[avatar_url]]">
    </span>
  </span>
  [[#display_name]]
    <span class="display-name mention-result--display-name">[[&display_name]]</span> <small class="username mention-result--username">[[&username]]</small>
  [[/display_name]]
  [[^display_name]]
    <span class="username mention-result--username">[[&username]]</span>
  [[/display_name]]
  [[#is_teammate]][[^is_team]]
    <span class="aui-lozenge aui-lozenge-complete aui-lozenge-subtle mention-result--lozenge">teammate</span>
  [[/is_team]][[/is_teammate]]
</span>
      </script>
    
      <script id="mention-call-to-action" type="text/html">
        
[[^query]]
<li class="bb-typeahead-item">Begin typing to search for a user</li>
[[/query]]
[[#query]]
<li class="bb-typeahead-item">Continue typing to search for a user</li>
[[/query]]

      </script>
    
      <script id="mention-no-results" type="text/html">
        
[[^searching]]
<li class="bb-typeahead-item">Found no matching users for <em>[[query]]</em>.</li>
[[/searching]]
[[#searching]]
<li class="bb-typeahead-item bb-typeahead-searching">Searching for <em>[[query]]</em>.</li>
[[/searching]]

      </script>
    
      <script id="emoji-result" type="text/html">
        
<span class="emoji-result">
  <span class="emoji-result--avatar">
    <img class="emoji" src="[[src]]">
  </span>
  <span class="name emoji-result--name">[[&name]]</span>
</span>

      </script>
    
      <script id="repo-typeahead-result" type="text/html">
        <span class="aui-avatar aui-avatar-project aui-avatar-xsmall">
  <span class="aui-avatar-inner">
    <img src="[[avatar]]">
  </span>
</span>
<span class="owner">[[&owner]]</span>/<span class="slug">[[&slug]]</span>

      </script>
    
      <script id="share-form-template" type="text/html">
        

<div class="error aui-message hidden">
  <span class="aui-icon icon-error"></span>
  <div class="message"></div>
</div>
<form class="aui">
  <table class="widget bb-list aui">
    <thead>
    <tr class="assistive">
      <th class="user">User</th>
      <th class="role">Role</th>
      <th class="actions">Actions</th>
    </tr>
    </thead>
    <tbody>
      <tr class="form">
        <td colspan="2">
          <input type="text" class="text bb-user-typeahead user-or-email"
                 placeholder="Username or email address"
                 autocomplete="off"
                 data-bb-typeahead-focus="false"
                 [[#disabled]]disabled[[/disabled]]>
        </td>
        <td class="actions">
          <button type="submit" class="aui-button aui-button-light" disabled>Add</button>
        </td>
      </tr>
    </tbody>
  </table>
</form>

      </script>
    
      <script id="share-detail-template" type="text/html">
        

[[#username]]
<td class="user
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[displayName]]">
    <a href="/[[username]]/" class="user">
      <span class="aui-avatar aui-avatar-xsmall">
        <span class="aui-avatar-inner">
          <img src="[[avatar]]">
        </span>
      </span>
      <span>[[displayName]]</span>
    </a>
  </div>
</td>
[[/username]]
[[^username]]
<td class="email
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[email]]">
    <span class="aui-icon aui-icon-small aui-iconfont-email"></span>
    [[email]]
  </div>
</td>
[[/username]]
<td class="role
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    [[#group]]
      [[#hasCustomGroups]]
        <select class="group [[#noGroupChoices]]hidden[[/noGroupChoices]]">
          [[#groups]]
            <option value="[[slug]]"
                [[#isSelected]]selected[[/isSelected]]>
              [[name]]
            </option>
          [[/groups]]
        </select>
      [[/hasCustomGroups]]
      [[^hasCustomGroups]]
      <label>
        <input type="checkbox" class="admin"
            [[#isAdmin]]checked[[/isAdmin]]>
        Administrator
      </label>
      [[/hasCustomGroups]]
    [[/group]]
    [[^group]]
      <ul>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^read]]aui-lozenge-subtle[[/read]]"
            data-permission="read">
          read
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^write]]aui-lozenge-subtle[[/write]]"
            data-permission="write">
          write
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^admin]]aui-lozenge-subtle[[/admin]]"
            data-permission="admin">
          admin
        </li>
      </ul>
    [[/group]]
  </div>
</td>
<td class="actions
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    <a href="#" class="delete">
      <span class="aui-icon aui-icon-small aui-iconfont-remove">Delete</span>
    </a>
  </div>
</td>

      </script>
    
      <script id="share-team-template" type="text/html">
        

<div class="clearfix">
  <span class="team-avatar-container">
    <span class="aui-avatar aui-avatar-medium">
      <span class="aui-avatar-inner">
        <img src="[[avatar]]">
      </span>
    </span>
  </span>
  <span class="team-name-container">
    [[display_name]]
  </span>
</div>
<p class="helptext">
  
    Existing users are granted access to this team immediately.
    New users will be sent an invitation.
  
</p>
<div class="share-form"></div>

      </script>
    
      <script id="scope-list-template" type="text/html">
        <ul class="scope-list">
  [[#scopes]]
    <li class="scope-list--item">
      <span class="scope-list--icon aui-icon aui-icon-small [[icon]]"></span>
      <span class="scope-list--description">[[description]]</span>
    </li>
  [[/scopes]]
</ul>

      </script>
    
      <script id="source-changeset" type="text/html">
        

<a href="/Yanosik/zapart/src/[[raw_node]]/[[path]]?at=master"
    class="[[#selected]]highlight[[/selected]]"
    data-hash="[[node]]">
  [[#author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="[[author.avatar]]">
      </span>
    </span>
    <span class="author" title="[[raw_author]]">[[author.display_name]]</span>
  [[/author.username]]
  [[^author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg">
      </span>
    </span>
    <span class="author unmapped" title="[[raw_author]]">[[author]]</span>
  [[/author.username]]
  <time datetime="[[utctimestamp]]" data-title="true">[[utctimestamp]]</time>
  <span class="message">[[message]]</span>
</a>

      </script>
    
      <script id="embed-template" type="text/html">
        

<form class="aui inline-dialog-embed-dialog">
  <label for="embed-code-[[dialogId]]">Embed this source in another page:</label>
  <input type="text" readonly="true" value="&lt;script src=&quot;[[url]]&quot;&gt;&lt;/script&gt;" id="embed-code-[[dialogId]]" class="embed-code">
</form>

      </script>
    
      <script id="edit-form-template" type="text/html">
        


<form class="bb-content-container online-edit-form aui"
      data-repository="[[owner]]/[[slug]]"
      data-destination-repository="[[destinationOwner]]/[[destinationSlug]]"
      data-local-id="[[localID]]"
      [[#isWriter]]data-is-writer="true"[[/isWriter]]
      [[#hasPushAccess]]data-has-push-access="true"[[/hasPushAccess]]
      [[#isPullRequest]]data-is-pull-request="true"[[/isPullRequest]]
      [[#hideCreatePullRequest]]data-hide-create-pull-request="true"[[/hideCreatePullRequest]]
      data-hash="[[hash]]"
      data-branch="[[branch]]"
      data-path="[[path]]"
      data-is-create="[[isCreate]]"
      data-preview-url="/xhr/[[owner]]/[[slug]]/preview/[[hash]]/[[encodedPath]]"
      data-preview-error="We had trouble generating your preview."
      data-unsaved-changes-error="Your changes will be lost. Are you sure you want to leave?">
  <div class="bb-content-container-header">
    <div class="bb-content-container-header-primary">
      <span class="bb-content-container-heading">
        [[#isCreate]]
          [[#branch]]
            
              Creating <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
            
          [[/branch]]
          [[^branch]]
            [[#path]]
              
                Creating <span class="edit-path">[[path]]</span>
              
            [[/path]]
            [[^path]]
              
                Creating <span class="edit-path">unnamed file</span>
              
            [[/path]]
          [[/branch]]
        [[/isCreate]]
        [[^isCreate]]
          
            Editing <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
          
        [[/isCreate]]
      </span>
    </div>
    <div class="bb-content-container-header-secondary">
      <div class="hunk-nav aui-buttons">
        <button class="prev-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Previous change">
          <span class="aui-icon aui-icon-small aui-iconfont-up">Previous change</span>
        </button>
        <button class="next-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Next change">
          <span class="aui-icon aui-icon-small aui-iconfont-down">Next change</span>
        </button>
      </div>
    </div>
  </div>
  <div class="bb-content-container-body has-header has-footer file-editor">
    <textarea id="id_source"></textarea>
  </div>
  <div class="preview-pane"></div>
  <div class="bb-content-container-footer">
    <div class="bb-content-container-footer-primary">
      <div id="syntax-mode" class="bb-content-container-item field">
        <label for="id_syntax-mode" class="online-edit-form--label">Syntax mode:</label>
        <select id="id_syntax-mode">
          [[#syntaxes]]
            <option value="[[#mime]][[mime]][[/mime]][[^mime]][[mode]][[/mime]]">[[name]]</option>
          [[/syntaxes]]
        </select>
      </div>
      <div id="indent-mode" class="bb-content-container-item field">
        <label for="id_indent-mode" class="online-edit-form--label">Indent mode:</label>
        <select id="id_indent-mode">
          <option value="tabs">Tabs</option>
          <option value="spaces">Spaces</option>
        </select>
      </div>
      <div id="indent-size" class="bb-content-container-item field">
        <label for="id_indent-size" class="online-edit-form--label">Indent size:</label>
        <select id="id_indent-size">
          <option value="2">2</option>
          <option value="4">4</option>
          <option value="8">8</option>
        </select>
      </div>
      <div id="wrap-mode" class="bb-content-container-item field">
        <label for="id_wrap-mode" class="online-edit-form--label">Line wrap:</label>
        <select id="id_wrap-mode">
          <option value="">Off</option>
          <option value="soft">On</option>
        </select>
      </div>
    </div>
    <div class="bb-content-container-footer-secondary">
      [[^isCreate]]
        <button class="preview-button aui-button aui-button-light"
                disabled="disabled" aria-disabled="true"
                data-preview-label="View diff"
                data-edit-label="Edit file">View diff</button>
      [[/isCreate]]
      <button class="save-button aui-button aui-button-primary"
              disabled="disabled" aria-disabled="true">Commit</button>
      [[^isCreate]]
        <a class="aui-button aui-button-link cancel-link" href="#">Cancel</a>
      [[/isCreate]]
    </div>
  </div>
</form>

      </script>
    
      <script id="commit-form-template" type="text/html">
        

<form class="aui commit-form"
      data-title="Commit changes"
      [[#isDelete]]
        data-default-message="[[filename]] deleted online with Bitbucket"
      [[/isDelete]]
      [[#isCreate]]
        data-default-message="[[filename]] created online with Bitbucket"
      [[/isCreate]]
      [[^isDelete]]
        [[^isCreate]]
          data-default-message="[[filename]] edited online with Bitbucket"
        [[/isCreate]]
      [[/isDelete]]
      data-fork-error="We had trouble creating your fork."
      data-commit-error="We had trouble committing your changes."
      data-pull-request-error="We had trouble creating your pull request."
      data-update-error="We had trouble updating your pull request."
      data-branch-conflict-error="A branch with that name already exists."
      data-forking-message="Forking repository"
      data-committing-message="Committing changes"
      data-merging-message="Branching and merging changes"
      data-creating-pr-message="Creating pull request"
      data-updating-pr-message="Updating pull request"
      data-cta-label="Commit"
      data-cancel-label="Cancel">
  [[#isDelete]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <span class="message">
        
          Committing this change will delete [[filename]] from your repository.
        
      </span>
    </div>
  [[/isDelete]]
  <div class="aui-message error hidden">
    <span class="aui-icon icon-error"></span>
    <span class="message"></span>
  </div>
  [[^isWriter]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <p class="title">
        
          You don't have write access to this repository.
        
      </p>
      <span class="message">
        
          We'll create a fork for your changes and submit a
          pull request back to this repository.
        
      </span>
    </div>
  [[/isWriter]]
  [[#isRename]]
    <div class="field-group">
      <label for="id_path">New path</label>
      <input type="text" id="id_path" class="text" value="[[path]]"/>
    </div>
  [[/isRename]]
  <div class="field-group">
    <label for="id_message">Commit message</label>
    <textarea id="id_message" class="long-field textarea"></textarea>
  </div>
  [[^isPullRequest]]
    [[#isWriter]]
      <fieldset class="group">
        <div class="checkbox">
          [[#hasPushAccess]]
            [[^hideCreatePullRequest]]
              <input id="id_create-pullrequest" class="checkbox" type="checkbox">
              <label for="id_create-pullrequest">Create a pull request for this change</label>
            [[/hideCreatePullRequest]]
          [[/hasPushAccess]]
          [[^hasPushAccess]]
            <input id="id_create-pullrequest" class="checkbox" type="checkbox" checked="checked" aria-disabled="true" disabled="true">
            <label for="id_create-pullrequest" title="Branch restrictions do not allow you to update this branch.">Create a pull request for this change</label>
          [[/hasPushAccess]]
        </div>
      </fieldset>
      <div id="pr-fields">
        <div id="branch-name-group" class="field-group">
          <label for="id_branch-name">Branch name</label>
          <input type="text" id="id_branch-name" class="text long-field">
        </div>
        

<div class="field-group" id="id_reviewers_group">
  <label for="reviewers">Reviewers</label>

  
  <input id="reviewers" name="reviewers" type="hidden"
          value=""
          data-mention-url="/xhr/mentions/repositories/:dest_username/:dest_slug"
          data-reviewers="[]"
          data-suggested="[]"
          data-locked="[]">

  <div class="error"></div>
  <div class="suggested-reviewers"></div>

</div>

      </div>
    [[/isWriter]]
  [[/isPullRequest]]
  <button type="submit" id="id_submit">Commit</button>
</form>

      </script>
    
      <script id="merge-message-template" type="text/html">
        Merged [[hash]] into [[branch]]

[[message]]

      </script>
    
      <script id="commit-merge-error-template" type="text/html">
        



  We had trouble merging your changes. We stored them on the <strong>[[branch]]</strong> branch, so feel free to
  <a href="/[[owner]]/[[slug]]/full-commit/[[hash]]/[[path]]?at=[[encodedBranch]]">view them</a> or
  <a href="#" class="create-pull-request-link">create a pull request</a>.


      </script>
    
      <script id="selected-reviewer-template" type="text/html">
        <div class="aui-avatar aui-avatar-xsmall">
  <div class="aui-avatar-inner">
    <img src="[[avatar_url]]">
  </div>
</div>
[[display_name]]

      </script>
    
      <script id="suggested-reviewer-template" type="text/html">
        <button class="aui-button aui-button-link" type="button" tabindex="-1">[[display_name]]</button>

      </script>
    
      <script id="suggested-reviewers-template" type="text/html">
        

<span class="suggested-reviewer-list-label">Recent:</span>
<ul class="suggested-reviewer-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-form-template" type="text/html">
        <div class="omnibar-input-container">
  <input class="omnibar-input" type="text" [[#placeholder]]placeholder="[[placeholder]]"[[/placeholder]]>
</div>
<ul class="omnibar-result-group-list"></ul>

      </script>
    
      <script id="omnibar-blank-slate-template" type="text/html">
        

<div class="omnibar-blank-slate">No results found</div>

      </script>
    
      <script id="omnibar-result-group-list-item-template" type="text/html">
        <div class="omnibar-result-group-header clearfix">
  <h2 class="omnibar-result-group-label" title="[[label]]">[[label]]</h2>
  <span class="omnibar-result-group-context" title="[[context]]">[[context]]</span>
</div>
<ul class="omnibar-result-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-result-list-item-template" type="text/html">
        [[#url]]
  <a href="[[&url]]" class="omnibar-result-label">[[&label]]</a>
[[/url]]
[[^url]]
  <span class="omnibar-result-label">[[&label]]</span>
[[/url]]
[[#context]]
  <span class="omnibar-result-context">[[context]]</span>
[[/context]]

      </script>
    
  </div>




  
  


<script nonce="dK7CPKzQkyQ7a0iZ">
  window.__initial_state__ = {"global": {"features": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "locale": "en", "geoip_country": "PL", "targetFeatures": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "isFocusedTask": false, "teams": [], "bitbucketActions": [{"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 100, "url": "/repo/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Repository<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repository-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 110, "url": "/account/create-team/", "tab_name": null, "can_display": true, "label": "<strong>Team<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "team-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 120, "url": "/account/projects/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Project<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "project-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 130, "url": "/snippets/new?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Snippet<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "snippet-create-drawer-item", "icon": ""}], "targetUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "isNavigationOpen": true, "path": "/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/wglew.h", "focusedTaskBackButtonUrl": "https://bitbucket.org/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/?at=master", "currentUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}}, "connect": {}, "repository": {"section": {"connectActions": [], "cloneProtocol": "https", "currentRepository": {"scm": "git", "website": "", "name": "Zapart", "language": "", "links": {"clone": [{"href": "https://Yanosik@bitbucket.org/Yanosik/zapart.git", "name": "https"}, {"href": "git@bitbucket.org:Yanosik/zapart.git", "name": "ssh"}], "self": {"href": "https://bitbucket.org/!api/2.0/repositories/Yanosik/zapart"}, "html": {"href": "https://bitbucket.org/Yanosik/zapart"}, "avatar": {"href": "https://bitbucket.org/Yanosik/zapart/avatar/32/"}}, "full_name": "Yanosik/zapart", "owner": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "type": "repository", "slug": "zapart", "is_private": true, "uuid": "{8f59423b-9494-4138-af6f-56e97d727ae4}"}, "menuItems": [{"analytics_label": "repository.overview", "icon_class": "icon-overview", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/overview", "tab_name": "overview", "can_display": true, "label": "Overview", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-overview-link", "icon": "icon-overview"}, {"analytics_label": "repository.source", "icon_class": "icon-source", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/src", "tab_name": "source", "can_display": true, "label": "Source", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-source-link", "icon": "icon-source"}, {"analytics_label": "repository.commits", "icon_class": "icon-commits", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/commits/", "tab_name": "commits", "can_display": true, "label": "Commits", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-commits-link", "icon": "icon-commits"}, {"analytics_label": "repository.branches", "icon_class": "icon-branches", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/", "tab_name": "branches", "can_display": true, "label": "Branches", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-branches-link", "icon": "icon-branches"}, {"analytics_label": "repository.pullrequests", "icon_class": "icon-pull-requests", "badge_label": "0 open pull requests", "weight": 500, "url": "/Yanosik/zapart/pull-requests/", "tab_name": "pullrequests", "can_display": true, "label": "Pull requests", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-pullrequests-link", "icon": "icon-pull-requests"}, {"analytics_label": "site.addon", "icon_class": "aui-iconfont-build", "badge_label": null, "weight": 550, "url": "/Yanosik/zapart/addon/pipelines-installer/home", "tab_name": "repopage-kbbqoq-add-on-link", "can_display": true, "label": "Pipelines", "anchor": true, "analytics_payload": {}, "icon_url": null, "type": "connect_menu_item", "id": "repopage-kbbqoq-add-on-link", "target": "_self"}, {"analytics_label": "repository.downloads", "icon_class": "icon-downloads", "badge_label": null, "weight": 800, "url": "/Yanosik/zapart/downloads/", "tab_name": "downloads", "can_display": true, "label": "Downloads", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-downloads-link", "icon": "icon-downloads"}, {"analytics_label": "user.addon", "icon_class": "aui-iconfont-unfocus", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/addon/bitbucket-trello-addon/trello-board", "tab_name": "repopage-5ABRne-add-on-link", "can_display": true, "label": "Boards", "anchor": false, "analytics_payload": {}, "icon_url": "https://bitbucket-assetroot.s3.amazonaws.com/add-on/icons/35ceae0c-17b1-443c-a6e8-d9de1d7cccdb.svg?Signature=kGYOmnNgcnVsyMtYFi%2BET7jaVkM%3D&Expires=1511782819&AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&versionId=3oqdrZZjT.HijZ3kHTPsXE6IcSjhCG.P", "type": "connect_menu_item", "id": "repopage-5ABRne-add-on-link", "target": "_self"}, {"analytics_label": "repository.settings", "icon_class": "icon-settings", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/admin", "tab_name": "admin", "can_display": true, "label": "Settings", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-settings-link", "icon": "icon-settings"}], "bitbucketActions": [{"analytics_label": "repository.clone", "icon_class": "icon-clone", "badge_label": null, "weight": 100, "url": "#clone", "tab_name": "clone", "can_display": true, "label": "<strong>Clone<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-clone-button", "icon": "icon-clone"}, {"analytics_label": "repository.create_branch", "icon_class": "icon-create-branch", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/branch", "tab_name": "create-branch", "can_display": true, "label": "Create a <strong>branch<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-branch-link", "icon": "icon-create-branch"}, {"analytics_label": "create_pullrequest", "icon_class": "icon-create-pull-request", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/pull-requests/new", "tab_name": "create-pullreqs", "can_display": true, "label": "Create a <strong>pull request<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-pull-request-link", "icon": "icon-create-pull-request"}, {"analytics_label": "repository.compare", "icon_class": "aui-icon-small aui-iconfont-devtools-compare", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/compare", "tab_name": "compare", "can_display": true, "label": "<strong>Compare<\/strong> branches or tags", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-compare-link", "icon": "aui-icon-small aui-iconfont-devtools-compare"}, {"analytics_label": "repository.fork", "icon_class": "icon-fork", "badge_label": null, "weight": 500, "url": "/Yanosik/zapart/fork", "tab_name": "fork", "can_display": true, "label": "<strong>Fork<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-fork-link", "icon": "icon-fork"}], "activeMenuItem": "source"}}};
  window.__settings__ = {"SOCIAL_AUTH_ATLASSIANID_LOGOUT_URL": "https://id.atlassian.com/logout", "CANON_URL": "https://bitbucket.org", "API_CANON_URL": "https://api.bitbucket.org"};
</script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/jsi18n/en/djangojs.js" nonce="dK7CPKzQkyQ7a0iZ"></script>

  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/locales/en.js"></script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/vendor.js" nonce="dK7CPKzQkyQ7a0iZ"></script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/app.js" nonce="dK7CPKzQkyQ7a0iZ"></script>


<script async src="https://www.google-analytics.com/analytics.js" nonce="dK7CPKzQkyQ7a0iZ"></script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"licenseKey":"a2cef8c3d3","agent":"","transactionName":"Z11RZxdWW0cEVkYLDV4XdUYLVEFdClsdAAtEWkZQDlJBGgRFQhFMQl1DXFcZQ10AQkFYBFlUVlEXWEJHAA==","applicationID":"1841284","errorBeacon":"bam.nr-data.net","applicationTime":899}</script>
</body>
</html>