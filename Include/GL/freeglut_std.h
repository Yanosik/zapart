<!DOCTYPE html>
<html lang="en">
<head>
  <meta id="bb-bootstrap" data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="5a097ca786e7c03ff34850dd" />
  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta charset="utf-8">
  <title>
  Yanosik / Zapart 
  / source  / Include / GL / freeglut_std.h
 &mdash; Bitbucket
</title>
  <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
  


<meta id="bb-canon-url" name="bb-canon-url" content="https://bitbucket.org">
<meta name="bb-api-canon-url" content="https://api.bitbucket.org">
<meta name="apitoken" content="{&quot;token&quot;: &quot;0lsCO4Os-nJV-who-YnBMcYmV4JWvjEV6Gq-hJEw7bVKJI3MpOw_TF5KNIrTW28Bmj1WTP7uR_qSuI3cIUgOyAzXFaqppekUHGsqbeoc08KAmgSf&quot;, &quot;expiration&quot;: 1511781299.121352}">

<meta name="bb-commit-hash" content="370568ebc84f">
<meta name="bb-app-node" content="app-169">
<meta name="bb-view-name" content="bitbucket.apps.repo2.views.filebrowse">
<meta name="ignore-whitespace" content="False">
<meta name="tab-size" content="None">
<meta name="locale" content="en">

<meta name="application-name" content="Bitbucket">
<meta name="apple-mobile-web-app-title" content="Bitbucket">


<meta name="theme-color" content="#0049B0">
<meta name="msapplication-TileColor" content="#0052CC">
<meta name="msapplication-TileImage" content="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/mstile-150x150.png">
<link rel="apple-touch-icon" sizes="180x180" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/apple-touch-icon.png">
<link rel="icon" sizes="192x192" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/android-chrome-192x192.png">

<link rel="icon" sizes="16x16 24x24 32x32 64x64" type="image/x-icon" href="/favicon.ico?v=2">
<link rel="mask-icon" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/safari-pinned-tab.svg" color="#0052CC">

<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Bitbucket">

  <meta name="description" content="">
  
  
    
  



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/vendor.css" />
<link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/app.css" />



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/adg3.css">
  
  <script nonce="dIoy0WyKpxyqa7fi">
  window.__sentry__ = {"dsn": "https://ea49358f525d4019945839a3d7a8292a@sentry.io/159509", "release": "370568ebc84f (canary)", "tags": {"project": "bitbucket-core"}, "environment": "canary"};
</script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/sentry.js" nonce="dIoy0WyKpxyqa7fi"></script>
  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/early.js" nonce="dIoy0WyKpxyqa7fi"></script>
  
  
    <link href="/Yanosik/zapart/rss?token=a07b0067199174cef445f1e8fa79b35f" rel="alternate nofollow" type="application/rss+xml" title="RSS feed for Zapart" />

</head>
<body class="production adg3  "
    data-static-url="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/"
data-base-url="https://bitbucket.org"
data-no-avatar-image="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg"
data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="{&quot;loginStatusUrl&quot;: &quot;https://id.atlassian.com/profile/rest/profile&quot;}"
data-settings="{&quot;MENTIONS_MIN_QUERY_LENGTH&quot;: 3}"

data-current-repo="{&quot;scm&quot;: &quot;git&quot;, &quot;readOnly&quot;: false, &quot;mainbranch&quot;: {&quot;name&quot;: &quot;master&quot;}, &quot;uuid&quot;: &quot;8f59423b-9494-4138-af6f-56e97d727ae4&quot;, &quot;language&quot;: &quot;&quot;, &quot;owner&quot;: {&quot;username&quot;: &quot;Yanosik&quot;, &quot;uuid&quot;: &quot;8aa0a974-0d88-41f1-b96a-c1ebd6c01af9&quot;, &quot;isTeam&quot;: false}, &quot;fullslug&quot;: &quot;Yanosik/zapart&quot;, &quot;slug&quot;: &quot;zapart&quot;, &quot;id&quot;: 29632644, &quot;pygmentsLanguage&quot;: null}"
data-current-cset="f71033cf7729d442b43ee6f67d340a04470f80ae"





data-browser-monitoring="true"
data-switch-create-pullrequest-commit-status="true"




>
<div id="page">
  
    <div id="adg3-navigation"
  
  
  >
  <nav class="skeleton-nav">
    <div class="skeleton-nav--left">
      <div class="skeleton-nav--left-top">
        <ul class="skeleton-nav--items">
          <li></li>
          <li></li>
          <li></li>
          <li class="skeleton--icon"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
        </ul>
      </div>
      <div class="skeleton-nav--left-bottom">
        <div class="skeleton-nav--left-bottom-wrapper">
          <div class="skeleton-nav--item-help"></div>
          <div class="skeleton-nav--item-profile"></div>
        </div>
      </div>
    </div>
    <div class="skeleton-nav--right">
      <ul class="skeleton-nav--items-wide">
        <li class="skeleton--icon-logo-container">
          <div class="skeleton--icon-container"></div>
          <div class="skeleton--icon-description"></div>
          <div class="skeleton--icon-logo"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
      </ul>
    </div>
  </nav>
</div>

    <div id="wrapper">
      
  


      
  <div id="nps-survey-container"></div>

 

      
  

<div id="account-warning" data-module="header/account-warning"
  data-unconfirmed-addresses="false"
  data-no-addresses="false"
  
></div>



      
  
<header id="aui-message-bar">
  
</header>


      <div id="content" role="main">

        
          <header class="app-header">
            <div class="app-header--primary">
              
                <div class="app-header--context">
                  <div class="app-header--breadcrumbs">
                    
  <ol class="aui-nav aui-nav-breadcrumbs">
    <li>
  <a href="/Yanosik/">Jan</a>
</li>

<li>
  <a href="/Yanosik/zapart">Zapart</a>
</li>
    
  <li>
    <a href="/Yanosik/zapart/src">
      Source
    </a>
  </li>

  </ol>

                  </div>
                  <h1 class="app-header--heading">
                    <span class="file-path">freeglut_std.h</span>
                  </h1>
                </div>
              
            </div>

            <div class="app-header--secondary">
              
                
              
            </div>
          </header>
        

        
        
  <div class="aui-page-panel ">
    <div class="hidden">
  
  </div>
    <div class="aui-page-panel-inner">

      <div
        id="repo-content"
        class="aui-page-panel-content forks-enabled can-create"
        data-module="repo/index"
        
      >
        
        
  <div id="source-container" class="maskable" data-module="repo/source/index">
    



<header id="source-path">
  
    <div class="labels labels-csv">
      <div class="aui-buttons">
        <button data-branches-tags-url="/api/1.0/repositories/Yanosik/zapart/branches-tags"
                data-module="components/branch-dialog"
                
                class="aui-button branch-dialog-trigger" title="master">
          
            
              <span class="aui-icon aui-icon-small aui-iconfont-devtools-branch">Branch</span>
            
            <span class="name">master</span>
          
          <span class="aui-icon-dropdown"></span>
        </button>
        <button class="aui-button" id="checkout-branch-button"
                title="Check out this branch">
          <span class="aui-icon aui-icon-small aui-iconfont-devtools-clone">Check out branch</span>
          <span class="aui-icon-dropdown"></span>
        </button>
      </div>
      
    
    
  
    </div>
  
  
    <div class="secondary-actions">
      <div class="aui-buttons">
        
          <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/freeglut_std.h?at=master"
            class="aui-button pjax-trigger source-toggle" aria-pressed="true">
            Source
          </a>
          <a href="/Yanosik/zapart/diff/Include/GL/freeglut_std.h?diff2=f71033cf7729&at=master"
            class="aui-button pjax-trigger diff-toggle"
            title="Diff to previous change">
            Diff
          </a>
          <a href="/Yanosik/zapart/history-node/f71033cf7729/Include/GL/freeglut_std.h?at=master"
            class="aui-button pjax-trigger history-toggle">
            History
          </a>
        
      </div>

      
        
        
      

    </div>
  
  <h1>
    
      
        <a href="/Yanosik/zapart/src/f71033cf7729?at=master"
          class="pjax-trigger root" title="Yanosik/zapart at f71033cf7729">Zapart</a> /
      
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/?at=master"
              class="pjax-trigger directory-name">Include</a> /
          
        
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/?at=master"
              class="pjax-trigger directory-name">GL</a> /
          
        
      
        
          
            <span class="file-name">freeglut_std.h</span>
          
        
      
    
  </h1>
  
    
    
  
  <div class="clearfix"></div>
</header>


  
    
  

  <div id="editor-container" class="maskable"
       data-module="repo/source/editor"
       data-owner="Yanosik"
       data-slug="zapart"
       data-is-writer="true"
       data-has-push-access="true"
       data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"
       data-branch="master"
       data-path="Include/GL/freeglut_std.h"
       data-source-url="/api/internal/repositories/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_std.h">
    <div id="source-view" class="file-source-container" data-module="repo/source/view-file" data-is-lfs="false">
      <div class="toolbar">
        <div class="primary">
          <div class="aui-buttons">
            
              <button id="file-history-trigger" class="aui-button aui-button-light changeset-info"
                      data-changeset="f71033cf7729d442b43ee6f67d340a04470f80ae"
                      data-path="Include/GL/freeglut_std.h"
                      data-current="f71033cf7729d442b43ee6f67d340a04470f80ae">
                 

  <div class="aui-avatar aui-avatar-xsmall">
    <div class="aui-avatar-inner">
      <img src="https://bitbucket.org/account/Yanosik/avatar/16/?ts=1511779772">
    </div>
  </div>
  <span class="changeset-hash">f71033c</span>
  <time datetime="2017-11-13T12:06:29+00:00" class="timestamp"></time>
  <span class="aui-icon-dropdown"></span>

              </button>
            
          </div>
          
          <a href="/Yanosik/zapart/full-commit/f71033cf7729/Include/GL/freeglut_std.h" id="full-commit-link"
             title="View full commit f71033c">Full commit</a>
        </div>
        <div class="secondary">
          
          <div class="aui-buttons">
            
              <a href="/Yanosik/zapart/annotate/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_std.h?at=master"
                 class="aui-button aui-button-light pjax-trigger blame-link">Blame</a>
              
            
            <a href="/Yanosik/zapart/raw/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_std.h" class="aui-button aui-button-light raw-link">Raw</a>
          </div>
          
            

            <div class="aui-buttons">
              
              <button id="file-edit-button" class="edit-button aui-button aui-button-light aui-button-split-main"
                  

                  >
                Edit
                
              </button>
              <button id="file-more-actions-button" class="aui-button aui-button-light aui-dropdown2-trigger aui-button-split-more" aria-owns="split-container-dropdown" aria-haspopup="true"
                  >
                More file actions
              </button>
            </div>
            <div id="split-container-dropdown" class="aui-dropdown2 aui-style-default" data-container="#editor-container">
              <ul class="aui-list-truncate">
                <li><a href="#" data-module="repo/source/rename-file" class="rename-link">Rename</a></li>
                <li><a href="#" data-module="repo/source/delete-file" class="delete-link">Delete</a></li>
              </ul>
            </div>
          
        </div>

        <div id="fileview-dropdown"
            class="aui-dropdown2 aui-style-default"
            data-fileview-container="#fileview-container"
            
            
            data-fileview-button="#fileview-trigger"
            data-module="connect/fileview">
          <div class="aui-dropdown2-section">
            <ul>
              <li>
                <a class="aui-dropdown2-radio aui-dropdown2-checked"
                    data-fileview-id="-1"
                    data-fileview-loaded="true"
                    data-fileview-target="#fileview-original"
                    data-fileview-connection-key=""
                    data-fileview-module-key="file-view-default">
                  Default File Viewer
                </a>
              </li>
              
            </ul>
          </div>
        </div>

        <div class="clearfix"></div>
      </div>
      <div id="fileview-container">
        <div id="fileview-original"
            class="fileview"
            data-module="repo/source/highlight-lines"
            data-fileview-loaded="true">
          


  
    <div class="file-source">
      <table class="codehilite highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre><a href="#freeglut_std.h-1">  1</a>
<a href="#freeglut_std.h-2">  2</a>
<a href="#freeglut_std.h-3">  3</a>
<a href="#freeglut_std.h-4">  4</a>
<a href="#freeglut_std.h-5">  5</a>
<a href="#freeglut_std.h-6">  6</a>
<a href="#freeglut_std.h-7">  7</a>
<a href="#freeglut_std.h-8">  8</a>
<a href="#freeglut_std.h-9">  9</a>
<a href="#freeglut_std.h-10"> 10</a>
<a href="#freeglut_std.h-11"> 11</a>
<a href="#freeglut_std.h-12"> 12</a>
<a href="#freeglut_std.h-13"> 13</a>
<a href="#freeglut_std.h-14"> 14</a>
<a href="#freeglut_std.h-15"> 15</a>
<a href="#freeglut_std.h-16"> 16</a>
<a href="#freeglut_std.h-17"> 17</a>
<a href="#freeglut_std.h-18"> 18</a>
<a href="#freeglut_std.h-19"> 19</a>
<a href="#freeglut_std.h-20"> 20</a>
<a href="#freeglut_std.h-21"> 21</a>
<a href="#freeglut_std.h-22"> 22</a>
<a href="#freeglut_std.h-23"> 23</a>
<a href="#freeglut_std.h-24"> 24</a>
<a href="#freeglut_std.h-25"> 25</a>
<a href="#freeglut_std.h-26"> 26</a>
<a href="#freeglut_std.h-27"> 27</a>
<a href="#freeglut_std.h-28"> 28</a>
<a href="#freeglut_std.h-29"> 29</a>
<a href="#freeglut_std.h-30"> 30</a>
<a href="#freeglut_std.h-31"> 31</a>
<a href="#freeglut_std.h-32"> 32</a>
<a href="#freeglut_std.h-33"> 33</a>
<a href="#freeglut_std.h-34"> 34</a>
<a href="#freeglut_std.h-35"> 35</a>
<a href="#freeglut_std.h-36"> 36</a>
<a href="#freeglut_std.h-37"> 37</a>
<a href="#freeglut_std.h-38"> 38</a>
<a href="#freeglut_std.h-39"> 39</a>
<a href="#freeglut_std.h-40"> 40</a>
<a href="#freeglut_std.h-41"> 41</a>
<a href="#freeglut_std.h-42"> 42</a>
<a href="#freeglut_std.h-43"> 43</a>
<a href="#freeglut_std.h-44"> 44</a>
<a href="#freeglut_std.h-45"> 45</a>
<a href="#freeglut_std.h-46"> 46</a>
<a href="#freeglut_std.h-47"> 47</a>
<a href="#freeglut_std.h-48"> 48</a>
<a href="#freeglut_std.h-49"> 49</a>
<a href="#freeglut_std.h-50"> 50</a>
<a href="#freeglut_std.h-51"> 51</a>
<a href="#freeglut_std.h-52"> 52</a>
<a href="#freeglut_std.h-53"> 53</a>
<a href="#freeglut_std.h-54"> 54</a>
<a href="#freeglut_std.h-55"> 55</a>
<a href="#freeglut_std.h-56"> 56</a>
<a href="#freeglut_std.h-57"> 57</a>
<a href="#freeglut_std.h-58"> 58</a>
<a href="#freeglut_std.h-59"> 59</a>
<a href="#freeglut_std.h-60"> 60</a>
<a href="#freeglut_std.h-61"> 61</a>
<a href="#freeglut_std.h-62"> 62</a>
<a href="#freeglut_std.h-63"> 63</a>
<a href="#freeglut_std.h-64"> 64</a>
<a href="#freeglut_std.h-65"> 65</a>
<a href="#freeglut_std.h-66"> 66</a>
<a href="#freeglut_std.h-67"> 67</a>
<a href="#freeglut_std.h-68"> 68</a>
<a href="#freeglut_std.h-69"> 69</a>
<a href="#freeglut_std.h-70"> 70</a>
<a href="#freeglut_std.h-71"> 71</a>
<a href="#freeglut_std.h-72"> 72</a>
<a href="#freeglut_std.h-73"> 73</a>
<a href="#freeglut_std.h-74"> 74</a>
<a href="#freeglut_std.h-75"> 75</a>
<a href="#freeglut_std.h-76"> 76</a>
<a href="#freeglut_std.h-77"> 77</a>
<a href="#freeglut_std.h-78"> 78</a>
<a href="#freeglut_std.h-79"> 79</a>
<a href="#freeglut_std.h-80"> 80</a>
<a href="#freeglut_std.h-81"> 81</a>
<a href="#freeglut_std.h-82"> 82</a>
<a href="#freeglut_std.h-83"> 83</a>
<a href="#freeglut_std.h-84"> 84</a>
<a href="#freeglut_std.h-85"> 85</a>
<a href="#freeglut_std.h-86"> 86</a>
<a href="#freeglut_std.h-87"> 87</a>
<a href="#freeglut_std.h-88"> 88</a>
<a href="#freeglut_std.h-89"> 89</a>
<a href="#freeglut_std.h-90"> 90</a>
<a href="#freeglut_std.h-91"> 91</a>
<a href="#freeglut_std.h-92"> 92</a>
<a href="#freeglut_std.h-93"> 93</a>
<a href="#freeglut_std.h-94"> 94</a>
<a href="#freeglut_std.h-95"> 95</a>
<a href="#freeglut_std.h-96"> 96</a>
<a href="#freeglut_std.h-97"> 97</a>
<a href="#freeglut_std.h-98"> 98</a>
<a href="#freeglut_std.h-99"> 99</a>
<a href="#freeglut_std.h-100">100</a>
<a href="#freeglut_std.h-101">101</a>
<a href="#freeglut_std.h-102">102</a>
<a href="#freeglut_std.h-103">103</a>
<a href="#freeglut_std.h-104">104</a>
<a href="#freeglut_std.h-105">105</a>
<a href="#freeglut_std.h-106">106</a>
<a href="#freeglut_std.h-107">107</a>
<a href="#freeglut_std.h-108">108</a>
<a href="#freeglut_std.h-109">109</a>
<a href="#freeglut_std.h-110">110</a>
<a href="#freeglut_std.h-111">111</a>
<a href="#freeglut_std.h-112">112</a>
<a href="#freeglut_std.h-113">113</a>
<a href="#freeglut_std.h-114">114</a>
<a href="#freeglut_std.h-115">115</a>
<a href="#freeglut_std.h-116">116</a>
<a href="#freeglut_std.h-117">117</a>
<a href="#freeglut_std.h-118">118</a>
<a href="#freeglut_std.h-119">119</a>
<a href="#freeglut_std.h-120">120</a>
<a href="#freeglut_std.h-121">121</a>
<a href="#freeglut_std.h-122">122</a>
<a href="#freeglut_std.h-123">123</a>
<a href="#freeglut_std.h-124">124</a>
<a href="#freeglut_std.h-125">125</a>
<a href="#freeglut_std.h-126">126</a>
<a href="#freeglut_std.h-127">127</a>
<a href="#freeglut_std.h-128">128</a>
<a href="#freeglut_std.h-129">129</a>
<a href="#freeglut_std.h-130">130</a>
<a href="#freeglut_std.h-131">131</a>
<a href="#freeglut_std.h-132">132</a>
<a href="#freeglut_std.h-133">133</a>
<a href="#freeglut_std.h-134">134</a>
<a href="#freeglut_std.h-135">135</a>
<a href="#freeglut_std.h-136">136</a>
<a href="#freeglut_std.h-137">137</a>
<a href="#freeglut_std.h-138">138</a>
<a href="#freeglut_std.h-139">139</a>
<a href="#freeglut_std.h-140">140</a>
<a href="#freeglut_std.h-141">141</a>
<a href="#freeglut_std.h-142">142</a>
<a href="#freeglut_std.h-143">143</a>
<a href="#freeglut_std.h-144">144</a>
<a href="#freeglut_std.h-145">145</a>
<a href="#freeglut_std.h-146">146</a>
<a href="#freeglut_std.h-147">147</a>
<a href="#freeglut_std.h-148">148</a>
<a href="#freeglut_std.h-149">149</a>
<a href="#freeglut_std.h-150">150</a>
<a href="#freeglut_std.h-151">151</a>
<a href="#freeglut_std.h-152">152</a>
<a href="#freeglut_std.h-153">153</a>
<a href="#freeglut_std.h-154">154</a>
<a href="#freeglut_std.h-155">155</a>
<a href="#freeglut_std.h-156">156</a>
<a href="#freeglut_std.h-157">157</a>
<a href="#freeglut_std.h-158">158</a>
<a href="#freeglut_std.h-159">159</a>
<a href="#freeglut_std.h-160">160</a>
<a href="#freeglut_std.h-161">161</a>
<a href="#freeglut_std.h-162">162</a>
<a href="#freeglut_std.h-163">163</a>
<a href="#freeglut_std.h-164">164</a>
<a href="#freeglut_std.h-165">165</a>
<a href="#freeglut_std.h-166">166</a>
<a href="#freeglut_std.h-167">167</a>
<a href="#freeglut_std.h-168">168</a>
<a href="#freeglut_std.h-169">169</a>
<a href="#freeglut_std.h-170">170</a>
<a href="#freeglut_std.h-171">171</a>
<a href="#freeglut_std.h-172">172</a>
<a href="#freeglut_std.h-173">173</a>
<a href="#freeglut_std.h-174">174</a>
<a href="#freeglut_std.h-175">175</a>
<a href="#freeglut_std.h-176">176</a>
<a href="#freeglut_std.h-177">177</a>
<a href="#freeglut_std.h-178">178</a>
<a href="#freeglut_std.h-179">179</a>
<a href="#freeglut_std.h-180">180</a>
<a href="#freeglut_std.h-181">181</a>
<a href="#freeglut_std.h-182">182</a>
<a href="#freeglut_std.h-183">183</a>
<a href="#freeglut_std.h-184">184</a>
<a href="#freeglut_std.h-185">185</a>
<a href="#freeglut_std.h-186">186</a>
<a href="#freeglut_std.h-187">187</a>
<a href="#freeglut_std.h-188">188</a>
<a href="#freeglut_std.h-189">189</a>
<a href="#freeglut_std.h-190">190</a>
<a href="#freeglut_std.h-191">191</a>
<a href="#freeglut_std.h-192">192</a>
<a href="#freeglut_std.h-193">193</a>
<a href="#freeglut_std.h-194">194</a>
<a href="#freeglut_std.h-195">195</a>
<a href="#freeglut_std.h-196">196</a>
<a href="#freeglut_std.h-197">197</a>
<a href="#freeglut_std.h-198">198</a>
<a href="#freeglut_std.h-199">199</a>
<a href="#freeglut_std.h-200">200</a>
<a href="#freeglut_std.h-201">201</a>
<a href="#freeglut_std.h-202">202</a>
<a href="#freeglut_std.h-203">203</a>
<a href="#freeglut_std.h-204">204</a>
<a href="#freeglut_std.h-205">205</a>
<a href="#freeglut_std.h-206">206</a>
<a href="#freeglut_std.h-207">207</a>
<a href="#freeglut_std.h-208">208</a>
<a href="#freeglut_std.h-209">209</a>
<a href="#freeglut_std.h-210">210</a>
<a href="#freeglut_std.h-211">211</a>
<a href="#freeglut_std.h-212">212</a>
<a href="#freeglut_std.h-213">213</a>
<a href="#freeglut_std.h-214">214</a>
<a href="#freeglut_std.h-215">215</a>
<a href="#freeglut_std.h-216">216</a>
<a href="#freeglut_std.h-217">217</a>
<a href="#freeglut_std.h-218">218</a>
<a href="#freeglut_std.h-219">219</a>
<a href="#freeglut_std.h-220">220</a>
<a href="#freeglut_std.h-221">221</a>
<a href="#freeglut_std.h-222">222</a>
<a href="#freeglut_std.h-223">223</a>
<a href="#freeglut_std.h-224">224</a>
<a href="#freeglut_std.h-225">225</a>
<a href="#freeglut_std.h-226">226</a>
<a href="#freeglut_std.h-227">227</a>
<a href="#freeglut_std.h-228">228</a>
<a href="#freeglut_std.h-229">229</a>
<a href="#freeglut_std.h-230">230</a>
<a href="#freeglut_std.h-231">231</a>
<a href="#freeglut_std.h-232">232</a>
<a href="#freeglut_std.h-233">233</a>
<a href="#freeglut_std.h-234">234</a>
<a href="#freeglut_std.h-235">235</a>
<a href="#freeglut_std.h-236">236</a>
<a href="#freeglut_std.h-237">237</a>
<a href="#freeglut_std.h-238">238</a>
<a href="#freeglut_std.h-239">239</a>
<a href="#freeglut_std.h-240">240</a>
<a href="#freeglut_std.h-241">241</a>
<a href="#freeglut_std.h-242">242</a>
<a href="#freeglut_std.h-243">243</a>
<a href="#freeglut_std.h-244">244</a>
<a href="#freeglut_std.h-245">245</a>
<a href="#freeglut_std.h-246">246</a>
<a href="#freeglut_std.h-247">247</a>
<a href="#freeglut_std.h-248">248</a>
<a href="#freeglut_std.h-249">249</a>
<a href="#freeglut_std.h-250">250</a>
<a href="#freeglut_std.h-251">251</a>
<a href="#freeglut_std.h-252">252</a>
<a href="#freeglut_std.h-253">253</a>
<a href="#freeglut_std.h-254">254</a>
<a href="#freeglut_std.h-255">255</a>
<a href="#freeglut_std.h-256">256</a>
<a href="#freeglut_std.h-257">257</a>
<a href="#freeglut_std.h-258">258</a>
<a href="#freeglut_std.h-259">259</a>
<a href="#freeglut_std.h-260">260</a>
<a href="#freeglut_std.h-261">261</a>
<a href="#freeglut_std.h-262">262</a>
<a href="#freeglut_std.h-263">263</a>
<a href="#freeglut_std.h-264">264</a>
<a href="#freeglut_std.h-265">265</a>
<a href="#freeglut_std.h-266">266</a>
<a href="#freeglut_std.h-267">267</a>
<a href="#freeglut_std.h-268">268</a>
<a href="#freeglut_std.h-269">269</a>
<a href="#freeglut_std.h-270">270</a>
<a href="#freeglut_std.h-271">271</a>
<a href="#freeglut_std.h-272">272</a>
<a href="#freeglut_std.h-273">273</a>
<a href="#freeglut_std.h-274">274</a>
<a href="#freeglut_std.h-275">275</a>
<a href="#freeglut_std.h-276">276</a>
<a href="#freeglut_std.h-277">277</a>
<a href="#freeglut_std.h-278">278</a>
<a href="#freeglut_std.h-279">279</a>
<a href="#freeglut_std.h-280">280</a>
<a href="#freeglut_std.h-281">281</a>
<a href="#freeglut_std.h-282">282</a>
<a href="#freeglut_std.h-283">283</a>
<a href="#freeglut_std.h-284">284</a>
<a href="#freeglut_std.h-285">285</a>
<a href="#freeglut_std.h-286">286</a>
<a href="#freeglut_std.h-287">287</a>
<a href="#freeglut_std.h-288">288</a>
<a href="#freeglut_std.h-289">289</a>
<a href="#freeglut_std.h-290">290</a>
<a href="#freeglut_std.h-291">291</a>
<a href="#freeglut_std.h-292">292</a>
<a href="#freeglut_std.h-293">293</a>
<a href="#freeglut_std.h-294">294</a>
<a href="#freeglut_std.h-295">295</a>
<a href="#freeglut_std.h-296">296</a>
<a href="#freeglut_std.h-297">297</a>
<a href="#freeglut_std.h-298">298</a>
<a href="#freeglut_std.h-299">299</a>
<a href="#freeglut_std.h-300">300</a>
<a href="#freeglut_std.h-301">301</a>
<a href="#freeglut_std.h-302">302</a>
<a href="#freeglut_std.h-303">303</a>
<a href="#freeglut_std.h-304">304</a>
<a href="#freeglut_std.h-305">305</a>
<a href="#freeglut_std.h-306">306</a>
<a href="#freeglut_std.h-307">307</a>
<a href="#freeglut_std.h-308">308</a>
<a href="#freeglut_std.h-309">309</a>
<a href="#freeglut_std.h-310">310</a>
<a href="#freeglut_std.h-311">311</a>
<a href="#freeglut_std.h-312">312</a>
<a href="#freeglut_std.h-313">313</a>
<a href="#freeglut_std.h-314">314</a>
<a href="#freeglut_std.h-315">315</a>
<a href="#freeglut_std.h-316">316</a>
<a href="#freeglut_std.h-317">317</a>
<a href="#freeglut_std.h-318">318</a>
<a href="#freeglut_std.h-319">319</a>
<a href="#freeglut_std.h-320">320</a>
<a href="#freeglut_std.h-321">321</a>
<a href="#freeglut_std.h-322">322</a>
<a href="#freeglut_std.h-323">323</a>
<a href="#freeglut_std.h-324">324</a>
<a href="#freeglut_std.h-325">325</a>
<a href="#freeglut_std.h-326">326</a>
<a href="#freeglut_std.h-327">327</a>
<a href="#freeglut_std.h-328">328</a>
<a href="#freeglut_std.h-329">329</a>
<a href="#freeglut_std.h-330">330</a>
<a href="#freeglut_std.h-331">331</a>
<a href="#freeglut_std.h-332">332</a>
<a href="#freeglut_std.h-333">333</a>
<a href="#freeglut_std.h-334">334</a>
<a href="#freeglut_std.h-335">335</a>
<a href="#freeglut_std.h-336">336</a>
<a href="#freeglut_std.h-337">337</a>
<a href="#freeglut_std.h-338">338</a>
<a href="#freeglut_std.h-339">339</a>
<a href="#freeglut_std.h-340">340</a>
<a href="#freeglut_std.h-341">341</a>
<a href="#freeglut_std.h-342">342</a>
<a href="#freeglut_std.h-343">343</a>
<a href="#freeglut_std.h-344">344</a>
<a href="#freeglut_std.h-345">345</a>
<a href="#freeglut_std.h-346">346</a>
<a href="#freeglut_std.h-347">347</a>
<a href="#freeglut_std.h-348">348</a>
<a href="#freeglut_std.h-349">349</a>
<a href="#freeglut_std.h-350">350</a>
<a href="#freeglut_std.h-351">351</a>
<a href="#freeglut_std.h-352">352</a>
<a href="#freeglut_std.h-353">353</a>
<a href="#freeglut_std.h-354">354</a>
<a href="#freeglut_std.h-355">355</a>
<a href="#freeglut_std.h-356">356</a>
<a href="#freeglut_std.h-357">357</a>
<a href="#freeglut_std.h-358">358</a>
<a href="#freeglut_std.h-359">359</a>
<a href="#freeglut_std.h-360">360</a>
<a href="#freeglut_std.h-361">361</a>
<a href="#freeglut_std.h-362">362</a>
<a href="#freeglut_std.h-363">363</a>
<a href="#freeglut_std.h-364">364</a>
<a href="#freeglut_std.h-365">365</a>
<a href="#freeglut_std.h-366">366</a>
<a href="#freeglut_std.h-367">367</a>
<a href="#freeglut_std.h-368">368</a>
<a href="#freeglut_std.h-369">369</a>
<a href="#freeglut_std.h-370">370</a>
<a href="#freeglut_std.h-371">371</a>
<a href="#freeglut_std.h-372">372</a>
<a href="#freeglut_std.h-373">373</a>
<a href="#freeglut_std.h-374">374</a>
<a href="#freeglut_std.h-375">375</a>
<a href="#freeglut_std.h-376">376</a>
<a href="#freeglut_std.h-377">377</a>
<a href="#freeglut_std.h-378">378</a>
<a href="#freeglut_std.h-379">379</a>
<a href="#freeglut_std.h-380">380</a>
<a href="#freeglut_std.h-381">381</a>
<a href="#freeglut_std.h-382">382</a>
<a href="#freeglut_std.h-383">383</a>
<a href="#freeglut_std.h-384">384</a>
<a href="#freeglut_std.h-385">385</a>
<a href="#freeglut_std.h-386">386</a>
<a href="#freeglut_std.h-387">387</a>
<a href="#freeglut_std.h-388">388</a>
<a href="#freeglut_std.h-389">389</a>
<a href="#freeglut_std.h-390">390</a>
<a href="#freeglut_std.h-391">391</a>
<a href="#freeglut_std.h-392">392</a>
<a href="#freeglut_std.h-393">393</a>
<a href="#freeglut_std.h-394">394</a>
<a href="#freeglut_std.h-395">395</a>
<a href="#freeglut_std.h-396">396</a>
<a href="#freeglut_std.h-397">397</a>
<a href="#freeglut_std.h-398">398</a>
<a href="#freeglut_std.h-399">399</a>
<a href="#freeglut_std.h-400">400</a>
<a href="#freeglut_std.h-401">401</a>
<a href="#freeglut_std.h-402">402</a>
<a href="#freeglut_std.h-403">403</a>
<a href="#freeglut_std.h-404">404</a>
<a href="#freeglut_std.h-405">405</a>
<a href="#freeglut_std.h-406">406</a>
<a href="#freeglut_std.h-407">407</a>
<a href="#freeglut_std.h-408">408</a>
<a href="#freeglut_std.h-409">409</a>
<a href="#freeglut_std.h-410">410</a>
<a href="#freeglut_std.h-411">411</a>
<a href="#freeglut_std.h-412">412</a>
<a href="#freeglut_std.h-413">413</a>
<a href="#freeglut_std.h-414">414</a>
<a href="#freeglut_std.h-415">415</a>
<a href="#freeglut_std.h-416">416</a>
<a href="#freeglut_std.h-417">417</a>
<a href="#freeglut_std.h-418">418</a>
<a href="#freeglut_std.h-419">419</a>
<a href="#freeglut_std.h-420">420</a>
<a href="#freeglut_std.h-421">421</a>
<a href="#freeglut_std.h-422">422</a>
<a href="#freeglut_std.h-423">423</a>
<a href="#freeglut_std.h-424">424</a>
<a href="#freeglut_std.h-425">425</a>
<a href="#freeglut_std.h-426">426</a>
<a href="#freeglut_std.h-427">427</a>
<a href="#freeglut_std.h-428">428</a>
<a href="#freeglut_std.h-429">429</a>
<a href="#freeglut_std.h-430">430</a>
<a href="#freeglut_std.h-431">431</a>
<a href="#freeglut_std.h-432">432</a>
<a href="#freeglut_std.h-433">433</a>
<a href="#freeglut_std.h-434">434</a>
<a href="#freeglut_std.h-435">435</a>
<a href="#freeglut_std.h-436">436</a>
<a href="#freeglut_std.h-437">437</a>
<a href="#freeglut_std.h-438">438</a>
<a href="#freeglut_std.h-439">439</a>
<a href="#freeglut_std.h-440">440</a>
<a href="#freeglut_std.h-441">441</a>
<a href="#freeglut_std.h-442">442</a>
<a href="#freeglut_std.h-443">443</a>
<a href="#freeglut_std.h-444">444</a>
<a href="#freeglut_std.h-445">445</a>
<a href="#freeglut_std.h-446">446</a>
<a href="#freeglut_std.h-447">447</a>
<a href="#freeglut_std.h-448">448</a>
<a href="#freeglut_std.h-449">449</a>
<a href="#freeglut_std.h-450">450</a>
<a href="#freeglut_std.h-451">451</a>
<a href="#freeglut_std.h-452">452</a>
<a href="#freeglut_std.h-453">453</a>
<a href="#freeglut_std.h-454">454</a>
<a href="#freeglut_std.h-455">455</a>
<a href="#freeglut_std.h-456">456</a>
<a href="#freeglut_std.h-457">457</a>
<a href="#freeglut_std.h-458">458</a>
<a href="#freeglut_std.h-459">459</a>
<a href="#freeglut_std.h-460">460</a>
<a href="#freeglut_std.h-461">461</a>
<a href="#freeglut_std.h-462">462</a>
<a href="#freeglut_std.h-463">463</a>
<a href="#freeglut_std.h-464">464</a>
<a href="#freeglut_std.h-465">465</a>
<a href="#freeglut_std.h-466">466</a>
<a href="#freeglut_std.h-467">467</a>
<a href="#freeglut_std.h-468">468</a>
<a href="#freeglut_std.h-469">469</a>
<a href="#freeglut_std.h-470">470</a>
<a href="#freeglut_std.h-471">471</a>
<a href="#freeglut_std.h-472">472</a>
<a href="#freeglut_std.h-473">473</a>
<a href="#freeglut_std.h-474">474</a>
<a href="#freeglut_std.h-475">475</a>
<a href="#freeglut_std.h-476">476</a>
<a href="#freeglut_std.h-477">477</a>
<a href="#freeglut_std.h-478">478</a>
<a href="#freeglut_std.h-479">479</a>
<a href="#freeglut_std.h-480">480</a>
<a href="#freeglut_std.h-481">481</a>
<a href="#freeglut_std.h-482">482</a>
<a href="#freeglut_std.h-483">483</a>
<a href="#freeglut_std.h-484">484</a>
<a href="#freeglut_std.h-485">485</a>
<a href="#freeglut_std.h-486">486</a>
<a href="#freeglut_std.h-487">487</a>
<a href="#freeglut_std.h-488">488</a>
<a href="#freeglut_std.h-489">489</a>
<a href="#freeglut_std.h-490">490</a>
<a href="#freeglut_std.h-491">491</a>
<a href="#freeglut_std.h-492">492</a>
<a href="#freeglut_std.h-493">493</a>
<a href="#freeglut_std.h-494">494</a>
<a href="#freeglut_std.h-495">495</a>
<a href="#freeglut_std.h-496">496</a>
<a href="#freeglut_std.h-497">497</a>
<a href="#freeglut_std.h-498">498</a>
<a href="#freeglut_std.h-499">499</a>
<a href="#freeglut_std.h-500">500</a>
<a href="#freeglut_std.h-501">501</a>
<a href="#freeglut_std.h-502">502</a>
<a href="#freeglut_std.h-503">503</a>
<a href="#freeglut_std.h-504">504</a>
<a href="#freeglut_std.h-505">505</a>
<a href="#freeglut_std.h-506">506</a>
<a href="#freeglut_std.h-507">507</a>
<a href="#freeglut_std.h-508">508</a>
<a href="#freeglut_std.h-509">509</a>
<a href="#freeglut_std.h-510">510</a>
<a href="#freeglut_std.h-511">511</a>
<a href="#freeglut_std.h-512">512</a>
<a href="#freeglut_std.h-513">513</a>
<a href="#freeglut_std.h-514">514</a>
<a href="#freeglut_std.h-515">515</a>
<a href="#freeglut_std.h-516">516</a>
<a href="#freeglut_std.h-517">517</a>
<a href="#freeglut_std.h-518">518</a>
<a href="#freeglut_std.h-519">519</a>
<a href="#freeglut_std.h-520">520</a>
<a href="#freeglut_std.h-521">521</a>
<a href="#freeglut_std.h-522">522</a>
<a href="#freeglut_std.h-523">523</a>
<a href="#freeglut_std.h-524">524</a>
<a href="#freeglut_std.h-525">525</a>
<a href="#freeglut_std.h-526">526</a>
<a href="#freeglut_std.h-527">527</a>
<a href="#freeglut_std.h-528">528</a>
<a href="#freeglut_std.h-529">529</a>
<a href="#freeglut_std.h-530">530</a>
<a href="#freeglut_std.h-531">531</a>
<a href="#freeglut_std.h-532">532</a>
<a href="#freeglut_std.h-533">533</a>
<a href="#freeglut_std.h-534">534</a>
<a href="#freeglut_std.h-535">535</a>
<a href="#freeglut_std.h-536">536</a>
<a href="#freeglut_std.h-537">537</a>
<a href="#freeglut_std.h-538">538</a>
<a href="#freeglut_std.h-539">539</a>
<a href="#freeglut_std.h-540">540</a>
<a href="#freeglut_std.h-541">541</a>
<a href="#freeglut_std.h-542">542</a>
<a href="#freeglut_std.h-543">543</a>
<a href="#freeglut_std.h-544">544</a>
<a href="#freeglut_std.h-545">545</a>
<a href="#freeglut_std.h-546">546</a>
<a href="#freeglut_std.h-547">547</a>
<a href="#freeglut_std.h-548">548</a>
<a href="#freeglut_std.h-549">549</a>
<a href="#freeglut_std.h-550">550</a>
<a href="#freeglut_std.h-551">551</a>
<a href="#freeglut_std.h-552">552</a>
<a href="#freeglut_std.h-553">553</a>
<a href="#freeglut_std.h-554">554</a>
<a href="#freeglut_std.h-555">555</a>
<a href="#freeglut_std.h-556">556</a>
<a href="#freeglut_std.h-557">557</a>
<a href="#freeglut_std.h-558">558</a>
<a href="#freeglut_std.h-559">559</a>
<a href="#freeglut_std.h-560">560</a>
<a href="#freeglut_std.h-561">561</a>
<a href="#freeglut_std.h-562">562</a>
<a href="#freeglut_std.h-563">563</a>
<a href="#freeglut_std.h-564">564</a>
<a href="#freeglut_std.h-565">565</a>
<a href="#freeglut_std.h-566">566</a>
<a href="#freeglut_std.h-567">567</a>
<a href="#freeglut_std.h-568">568</a>
<a href="#freeglut_std.h-569">569</a>
<a href="#freeglut_std.h-570">570</a>
<a href="#freeglut_std.h-571">571</a>
<a href="#freeglut_std.h-572">572</a>
<a href="#freeglut_std.h-573">573</a>
<a href="#freeglut_std.h-574">574</a>
<a href="#freeglut_std.h-575">575</a>
<a href="#freeglut_std.h-576">576</a>
<a href="#freeglut_std.h-577">577</a>
<a href="#freeglut_std.h-578">578</a>
<a href="#freeglut_std.h-579">579</a>
<a href="#freeglut_std.h-580">580</a>
<a href="#freeglut_std.h-581">581</a>
<a href="#freeglut_std.h-582">582</a>
<a href="#freeglut_std.h-583">583</a>
<a href="#freeglut_std.h-584">584</a>
<a href="#freeglut_std.h-585">585</a>
<a href="#freeglut_std.h-586">586</a>
<a href="#freeglut_std.h-587">587</a>
<a href="#freeglut_std.h-588">588</a>
<a href="#freeglut_std.h-589">589</a>
<a href="#freeglut_std.h-590">590</a>
<a href="#freeglut_std.h-591">591</a>
<a href="#freeglut_std.h-592">592</a>
<a href="#freeglut_std.h-593">593</a>
<a href="#freeglut_std.h-594">594</a>
<a href="#freeglut_std.h-595">595</a>
<a href="#freeglut_std.h-596">596</a>
<a href="#freeglut_std.h-597">597</a>
<a href="#freeglut_std.h-598">598</a>
<a href="#freeglut_std.h-599">599</a>
<a href="#freeglut_std.h-600">600</a>
<a href="#freeglut_std.h-601">601</a>
<a href="#freeglut_std.h-602">602</a>
<a href="#freeglut_std.h-603">603</a>
<a href="#freeglut_std.h-604">604</a>
<a href="#freeglut_std.h-605">605</a>
<a href="#freeglut_std.h-606">606</a>
<a href="#freeglut_std.h-607">607</a>
<a href="#freeglut_std.h-608">608</a>
<a href="#freeglut_std.h-609">609</a>
<a href="#freeglut_std.h-610">610</a>
<a href="#freeglut_std.h-611">611</a>
<a href="#freeglut_std.h-612">612</a>
<a href="#freeglut_std.h-613">613</a>
<a href="#freeglut_std.h-614">614</a>
<a href="#freeglut_std.h-615">615</a>
<a href="#freeglut_std.h-616">616</a>
<a href="#freeglut_std.h-617">617</a>
<a href="#freeglut_std.h-618">618</a>
<a href="#freeglut_std.h-619">619</a>
<a href="#freeglut_std.h-620">620</a>
<a href="#freeglut_std.h-621">621</a>
<a href="#freeglut_std.h-622">622</a>
<a href="#freeglut_std.h-623">623</a>
<a href="#freeglut_std.h-624">624</a>
<a href="#freeglut_std.h-625">625</a>
<a href="#freeglut_std.h-626">626</a>
<a href="#freeglut_std.h-627">627</a>
<a href="#freeglut_std.h-628">628</a>
<a href="#freeglut_std.h-629">629</a>
<a href="#freeglut_std.h-630">630</a>
<a href="#freeglut_std.h-631">631</a>
<a href="#freeglut_std.h-632">632</a>
<a href="#freeglut_std.h-633">633</a>
<a href="#freeglut_std.h-634">634</a>
<a href="#freeglut_std.h-635">635</a>
<a href="#freeglut_std.h-636">636</a>
<a href="#freeglut_std.h-637">637</a>
<a href="#freeglut_std.h-638">638</a></pre></div></td><td class="code"><div class="codehilite highlight"><pre><span></span><a name="freeglut_std.h-1"></a><span class="cp">#ifndef  __FREEGLUT_STD_H__</span>
<a name="freeglut_std.h-2"></a><span class="cp">#define  __FREEGLUT_STD_H__</span>
<a name="freeglut_std.h-3"></a>
<a name="freeglut_std.h-4"></a><span class="cm">/*</span>
<a name="freeglut_std.h-5"></a><span class="cm"> * freeglut_std.h</span>
<a name="freeglut_std.h-6"></a><span class="cm"> *</span>
<a name="freeglut_std.h-7"></a><span class="cm"> * The GLUT-compatible part of the freeglut library include file</span>
<a name="freeglut_std.h-8"></a><span class="cm"> *</span>
<a name="freeglut_std.h-9"></a><span class="cm"> * Copyright (c) 1999-2000 Pawel W. Olszta. All Rights Reserved.</span>
<a name="freeglut_std.h-10"></a><span class="cm"> * Written by Pawel W. Olszta, &lt;olszta@sourceforge.net&gt;</span>
<a name="freeglut_std.h-11"></a><span class="cm"> * Creation date: Thu Dec 2 1999</span>
<a name="freeglut_std.h-12"></a><span class="cm"> *</span>
<a name="freeglut_std.h-13"></a><span class="cm"> * Permission is hereby granted, free of charge, to any person obtaining a</span>
<a name="freeglut_std.h-14"></a><span class="cm"> * copy of this software and associated documentation files (the &quot;Software&quot;),</span>
<a name="freeglut_std.h-15"></a><span class="cm"> * to deal in the Software without restriction, including without limitation</span>
<a name="freeglut_std.h-16"></a><span class="cm"> * the rights to use, copy, modify, merge, publish, distribute, sublicense,</span>
<a name="freeglut_std.h-17"></a><span class="cm"> * and/or sell copies of the Software, and to permit persons to whom the</span>
<a name="freeglut_std.h-18"></a><span class="cm"> * Software is furnished to do so, subject to the following conditions:</span>
<a name="freeglut_std.h-19"></a><span class="cm"> *</span>
<a name="freeglut_std.h-20"></a><span class="cm"> * The above copyright notice and this permission notice shall be included</span>
<a name="freeglut_std.h-21"></a><span class="cm"> * in all copies or substantial portions of the Software.</span>
<a name="freeglut_std.h-22"></a><span class="cm"> *</span>
<a name="freeglut_std.h-23"></a><span class="cm"> * THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS</span>
<a name="freeglut_std.h-24"></a><span class="cm"> * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,</span>
<a name="freeglut_std.h-25"></a><span class="cm"> * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL</span>
<a name="freeglut_std.h-26"></a><span class="cm"> * PAWEL W. OLSZTA BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER</span>
<a name="freeglut_std.h-27"></a><span class="cm"> * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN</span>
<a name="freeglut_std.h-28"></a><span class="cm"> * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</span>
<a name="freeglut_std.h-29"></a><span class="cm"> */</span>
<a name="freeglut_std.h-30"></a>
<a name="freeglut_std.h-31"></a><span class="cp">#ifdef __cplusplus</span>
<a name="freeglut_std.h-32"></a>    <span class="k">extern</span> <span class="s">&quot;C&quot;</span> <span class="p">{</span>
<a name="freeglut_std.h-33"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-34"></a>
<a name="freeglut_std.h-35"></a><span class="cm">/*</span>
<a name="freeglut_std.h-36"></a><span class="cm"> * Under windows, we have to differentiate between static and dynamic libraries</span>
<a name="freeglut_std.h-37"></a><span class="cm"> */</span>
<a name="freeglut_std.h-38"></a><span class="cp">#ifdef _WIN32</span>
<a name="freeglut_std.h-39"></a><span class="cm">/* #pragma may not be supported by some compilers.</span>
<a name="freeglut_std.h-40"></a><span class="cm"> * Discussion by FreeGLUT developers suggests that</span>
<a name="freeglut_std.h-41"></a><span class="cm"> * Visual C++ specific code involving pragmas may</span>
<a name="freeglut_std.h-42"></a><span class="cm"> * need to move to a separate header.  24th Dec 2003</span>
<a name="freeglut_std.h-43"></a><span class="cm"> */</span>
<a name="freeglut_std.h-44"></a>
<a name="freeglut_std.h-45"></a><span class="cm">/* Define FREEGLUT_LIB_PRAGMAS to 1 to include library</span>
<a name="freeglut_std.h-46"></a><span class="cm"> * pragmas or to 0 to exclude library pragmas.</span>
<a name="freeglut_std.h-47"></a><span class="cm"> * The default behavior depends on the compiler/platform.</span>
<a name="freeglut_std.h-48"></a><span class="cm"> */</span>
<a name="freeglut_std.h-49"></a><span class="cp">#   ifndef FREEGLUT_LIB_PRAGMAS</span>
<a name="freeglut_std.h-50"></a><span class="cp">#       if ( defined(_MSC_VER) || defined(__WATCOMC__) ) &amp;&amp; !defined(_WIN32_WCE)</span>
<a name="freeglut_std.h-51"></a><span class="cp">#           define FREEGLUT_LIB_PRAGMAS 1</span>
<a name="freeglut_std.h-52"></a><span class="cp">#       else</span>
<a name="freeglut_std.h-53"></a><span class="cp">#           define FREEGLUT_LIB_PRAGMAS 0</span>
<a name="freeglut_std.h-54"></a><span class="cp">#       endif</span>
<a name="freeglut_std.h-55"></a><span class="cp">#   endif</span>
<a name="freeglut_std.h-56"></a>
<a name="freeglut_std.h-57"></a><span class="cp">#  ifndef WIN32_LEAN_AND_MEAN</span>
<a name="freeglut_std.h-58"></a><span class="cp">#    define WIN32_LEAN_AND_MEAN 1</span>
<a name="freeglut_std.h-59"></a><span class="cp">#  endif</span>
<a name="freeglut_std.h-60"></a><span class="cp">#  ifndef NOMINMAX</span>
<a name="freeglut_std.h-61"></a><span class="cp">#    define NOMINMAX</span>
<a name="freeglut_std.h-62"></a><span class="cp">#  endif</span>
<a name="freeglut_std.h-63"></a><span class="cp">#   include &lt;windows.h&gt;</span>
<a name="freeglut_std.h-64"></a>
<a name="freeglut_std.h-65"></a><span class="cm">/* Windows static library */</span>
<a name="freeglut_std.h-66"></a><span class="cp">#   ifdef FREEGLUT_STATIC</span>
<a name="freeglut_std.h-67"></a>
<a name="freeglut_std.h-68"></a><span class="cp">#error Static linking is not supported with this build. Please remove the FREEGLUT_STATIC preprocessor directive, or download the source code from http:</span><span class="c1">//freeglut.sf.net/ and build against that.</span>
<a name="freeglut_std.h-69"></a>
<a name="freeglut_std.h-70"></a><span class="cm">/* Windows shared library (DLL) */</span>
<a name="freeglut_std.h-71"></a><span class="cp">#   else</span>
<a name="freeglut_std.h-72"></a>
<a name="freeglut_std.h-73"></a><span class="cp">#       define FGAPIENTRY __stdcall</span>
<a name="freeglut_std.h-74"></a><span class="cp">#       if defined(FREEGLUT_EXPORTS)</span>
<a name="freeglut_std.h-75"></a><span class="cp">#           define FGAPI __declspec(dllexport)</span>
<a name="freeglut_std.h-76"></a><span class="cp">#       else</span>
<a name="freeglut_std.h-77"></a><span class="cp">#           define FGAPI __declspec(dllimport)</span>
<a name="freeglut_std.h-78"></a>
<a name="freeglut_std.h-79"></a>            <span class="cm">/* Link with Win32 shared freeglut lib */</span>
<a name="freeglut_std.h-80"></a><span class="cp">#           if FREEGLUT_LIB_PRAGMAS</span>
<a name="freeglut_std.h-81"></a><span class="cp">#             pragma comment (lib, &quot;freeglut.lib&quot;)</span>
<a name="freeglut_std.h-82"></a><span class="cp">#           endif</span>
<a name="freeglut_std.h-83"></a>
<a name="freeglut_std.h-84"></a><span class="cp">#       endif</span>
<a name="freeglut_std.h-85"></a>
<a name="freeglut_std.h-86"></a><span class="cp">#   endif</span>
<a name="freeglut_std.h-87"></a>
<a name="freeglut_std.h-88"></a><span class="cm">/* Drag in other Windows libraries as required by FreeGLUT */</span>
<a name="freeglut_std.h-89"></a><span class="cp">#   if FREEGLUT_LIB_PRAGMAS</span>
<a name="freeglut_std.h-90"></a><span class="cp">#       pragma comment (lib, &quot;glu32.lib&quot;)    </span><span class="cm">/* link OpenGL Utility lib     */</span><span class="cp"></span>
<a name="freeglut_std.h-91"></a><span class="cp">#       pragma comment (lib, &quot;opengl32.lib&quot;) </span><span class="cm">/* link Microsoft OpenGL lib   */</span><span class="cp"></span>
<a name="freeglut_std.h-92"></a><span class="cp">#       pragma comment (lib, &quot;gdi32.lib&quot;)    </span><span class="cm">/* link Windows GDI lib        */</span><span class="cp"></span>
<a name="freeglut_std.h-93"></a><span class="cp">#       pragma comment (lib, &quot;winmm.lib&quot;)    </span><span class="cm">/* link Windows MultiMedia lib */</span><span class="cp"></span>
<a name="freeglut_std.h-94"></a><span class="cp">#       pragma comment (lib, &quot;user32.lib&quot;)   </span><span class="cm">/* link Windows user lib       */</span><span class="cp"></span>
<a name="freeglut_std.h-95"></a><span class="cp">#   endif</span>
<a name="freeglut_std.h-96"></a>
<a name="freeglut_std.h-97"></a><span class="cp">#else</span>
<a name="freeglut_std.h-98"></a>
<a name="freeglut_std.h-99"></a><span class="cm">/* Non-Windows definition of FGAPI and FGAPIENTRY  */</span>
<a name="freeglut_std.h-100"></a><span class="cp">#        define FGAPI</span>
<a name="freeglut_std.h-101"></a><span class="cp">#        define FGAPIENTRY</span>
<a name="freeglut_std.h-102"></a>
<a name="freeglut_std.h-103"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-104"></a>
<a name="freeglut_std.h-105"></a><span class="cm">/*</span>
<a name="freeglut_std.h-106"></a><span class="cm"> * The freeglut and GLUT API versions</span>
<a name="freeglut_std.h-107"></a><span class="cm"> */</span>
<a name="freeglut_std.h-108"></a><span class="cp">#define  FREEGLUT             1</span>
<a name="freeglut_std.h-109"></a><span class="cp">#define  GLUT_API_VERSION     4</span>
<a name="freeglut_std.h-110"></a><span class="cp">#define  GLUT_XLIB_IMPLEMENTATION 13</span>
<a name="freeglut_std.h-111"></a><span class="cm">/* Deprecated:</span>
<a name="freeglut_std.h-112"></a><span class="cm">   cf. http://sourceforge.net/mailarchive/forum.php?thread_name=CABcAi1hw7cr4xtigckaGXB5X8wddLfMcbA_rZ3NAuwMrX_zmsw%40mail.gmail.com&amp;forum_name=freeglut-developer */</span>
<a name="freeglut_std.h-113"></a><span class="cp">#define  FREEGLUT_VERSION_2_0 1</span>
<a name="freeglut_std.h-114"></a>
<a name="freeglut_std.h-115"></a><span class="cm">/*</span>
<a name="freeglut_std.h-116"></a><span class="cm"> * Always include OpenGL and GLU headers</span>
<a name="freeglut_std.h-117"></a><span class="cm"> */</span>
<a name="freeglut_std.h-118"></a><span class="cm">/* Note: FREEGLUT_GLES is only used to cleanly bootstrap headers</span>
<a name="freeglut_std.h-119"></a><span class="cm">   inclusion here; use GLES constants directly</span>
<a name="freeglut_std.h-120"></a><span class="cm">   (e.g. GL_ES_VERSION_2_0) for all other needs */</span>
<a name="freeglut_std.h-121"></a><span class="cp">#ifdef FREEGLUT_GLES</span>
<a name="freeglut_std.h-122"></a><span class="cp">#   include &lt;EGL/egl.h&gt;</span>
<a name="freeglut_std.h-123"></a><span class="cp">#   include &lt;GLES/gl.h&gt;</span>
<a name="freeglut_std.h-124"></a><span class="cp">#   include &lt;GLES2/gl2.h&gt;</span>
<a name="freeglut_std.h-125"></a><span class="cp">#elif __APPLE__</span>
<a name="freeglut_std.h-126"></a><span class="cp">#   include &lt;OpenGL/gl.h&gt;</span>
<a name="freeglut_std.h-127"></a><span class="cp">#   include &lt;OpenGL/glu.h&gt;</span>
<a name="freeglut_std.h-128"></a><span class="cp">#else</span>
<a name="freeglut_std.h-129"></a><span class="cp">#   include &lt;GL/gl.h&gt;</span>
<a name="freeglut_std.h-130"></a><span class="cp">#   include &lt;GL/glu.h&gt;</span>
<a name="freeglut_std.h-131"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-132"></a>
<a name="freeglut_std.h-133"></a><span class="cm">/*</span>
<a name="freeglut_std.h-134"></a><span class="cm"> * GLUT API macro definitions -- the special key codes:</span>
<a name="freeglut_std.h-135"></a><span class="cm"> */</span>
<a name="freeglut_std.h-136"></a><span class="cp">#define  GLUT_KEY_F1                        0x0001</span>
<a name="freeglut_std.h-137"></a><span class="cp">#define  GLUT_KEY_F2                        0x0002</span>
<a name="freeglut_std.h-138"></a><span class="cp">#define  GLUT_KEY_F3                        0x0003</span>
<a name="freeglut_std.h-139"></a><span class="cp">#define  GLUT_KEY_F4                        0x0004</span>
<a name="freeglut_std.h-140"></a><span class="cp">#define  GLUT_KEY_F5                        0x0005</span>
<a name="freeglut_std.h-141"></a><span class="cp">#define  GLUT_KEY_F6                        0x0006</span>
<a name="freeglut_std.h-142"></a><span class="cp">#define  GLUT_KEY_F7                        0x0007</span>
<a name="freeglut_std.h-143"></a><span class="cp">#define  GLUT_KEY_F8                        0x0008</span>
<a name="freeglut_std.h-144"></a><span class="cp">#define  GLUT_KEY_F9                        0x0009</span>
<a name="freeglut_std.h-145"></a><span class="cp">#define  GLUT_KEY_F10                       0x000A</span>
<a name="freeglut_std.h-146"></a><span class="cp">#define  GLUT_KEY_F11                       0x000B</span>
<a name="freeglut_std.h-147"></a><span class="cp">#define  GLUT_KEY_F12                       0x000C</span>
<a name="freeglut_std.h-148"></a><span class="cp">#define  GLUT_KEY_LEFT                      0x0064</span>
<a name="freeglut_std.h-149"></a><span class="cp">#define  GLUT_KEY_UP                        0x0065</span>
<a name="freeglut_std.h-150"></a><span class="cp">#define  GLUT_KEY_RIGHT                     0x0066</span>
<a name="freeglut_std.h-151"></a><span class="cp">#define  GLUT_KEY_DOWN                      0x0067</span>
<a name="freeglut_std.h-152"></a><span class="cp">#define  GLUT_KEY_PAGE_UP                   0x0068</span>
<a name="freeglut_std.h-153"></a><span class="cp">#define  GLUT_KEY_PAGE_DOWN                 0x0069</span>
<a name="freeglut_std.h-154"></a><span class="cp">#define  GLUT_KEY_HOME                      0x006A</span>
<a name="freeglut_std.h-155"></a><span class="cp">#define  GLUT_KEY_END                       0x006B</span>
<a name="freeglut_std.h-156"></a><span class="cp">#define  GLUT_KEY_INSERT                    0x006C</span>
<a name="freeglut_std.h-157"></a>
<a name="freeglut_std.h-158"></a><span class="cm">/*</span>
<a name="freeglut_std.h-159"></a><span class="cm"> * GLUT API macro definitions -- mouse state definitions</span>
<a name="freeglut_std.h-160"></a><span class="cm"> */</span>
<a name="freeglut_std.h-161"></a><span class="cp">#define  GLUT_LEFT_BUTTON                   0x0000</span>
<a name="freeglut_std.h-162"></a><span class="cp">#define  GLUT_MIDDLE_BUTTON                 0x0001</span>
<a name="freeglut_std.h-163"></a><span class="cp">#define  GLUT_RIGHT_BUTTON                  0x0002</span>
<a name="freeglut_std.h-164"></a><span class="cp">#define  GLUT_DOWN                          0x0000</span>
<a name="freeglut_std.h-165"></a><span class="cp">#define  GLUT_UP                            0x0001</span>
<a name="freeglut_std.h-166"></a><span class="cp">#define  GLUT_LEFT                          0x0000</span>
<a name="freeglut_std.h-167"></a><span class="cp">#define  GLUT_ENTERED                       0x0001</span>
<a name="freeglut_std.h-168"></a>
<a name="freeglut_std.h-169"></a><span class="cm">/*</span>
<a name="freeglut_std.h-170"></a><span class="cm"> * GLUT API macro definitions -- the display mode definitions</span>
<a name="freeglut_std.h-171"></a><span class="cm"> */</span>
<a name="freeglut_std.h-172"></a><span class="cp">#define  GLUT_RGB                           0x0000</span>
<a name="freeglut_std.h-173"></a><span class="cp">#define  GLUT_RGBA                          0x0000</span>
<a name="freeglut_std.h-174"></a><span class="cp">#define  GLUT_INDEX                         0x0001</span>
<a name="freeglut_std.h-175"></a><span class="cp">#define  GLUT_SINGLE                        0x0000</span>
<a name="freeglut_std.h-176"></a><span class="cp">#define  GLUT_DOUBLE                        0x0002</span>
<a name="freeglut_std.h-177"></a><span class="cp">#define  GLUT_ACCUM                         0x0004</span>
<a name="freeglut_std.h-178"></a><span class="cp">#define  GLUT_ALPHA                         0x0008</span>
<a name="freeglut_std.h-179"></a><span class="cp">#define  GLUT_DEPTH                         0x0010</span>
<a name="freeglut_std.h-180"></a><span class="cp">#define  GLUT_STENCIL                       0x0020</span>
<a name="freeglut_std.h-181"></a><span class="cp">#define  GLUT_MULTISAMPLE                   0x0080</span>
<a name="freeglut_std.h-182"></a><span class="cp">#define  GLUT_STEREO                        0x0100</span>
<a name="freeglut_std.h-183"></a><span class="cp">#define  GLUT_LUMINANCE                     0x0200</span>
<a name="freeglut_std.h-184"></a>
<a name="freeglut_std.h-185"></a><span class="cm">/*</span>
<a name="freeglut_std.h-186"></a><span class="cm"> * GLUT API macro definitions -- windows and menu related definitions</span>
<a name="freeglut_std.h-187"></a><span class="cm"> */</span>
<a name="freeglut_std.h-188"></a><span class="cp">#define  GLUT_MENU_NOT_IN_USE               0x0000</span>
<a name="freeglut_std.h-189"></a><span class="cp">#define  GLUT_MENU_IN_USE                   0x0001</span>
<a name="freeglut_std.h-190"></a><span class="cp">#define  GLUT_NOT_VISIBLE                   0x0000</span>
<a name="freeglut_std.h-191"></a><span class="cp">#define  GLUT_VISIBLE                       0x0001</span>
<a name="freeglut_std.h-192"></a><span class="cp">#define  GLUT_HIDDEN                        0x0000</span>
<a name="freeglut_std.h-193"></a><span class="cp">#define  GLUT_FULLY_RETAINED                0x0001</span>
<a name="freeglut_std.h-194"></a><span class="cp">#define  GLUT_PARTIALLY_RETAINED            0x0002</span>
<a name="freeglut_std.h-195"></a><span class="cp">#define  GLUT_FULLY_COVERED                 0x0003</span>
<a name="freeglut_std.h-196"></a>
<a name="freeglut_std.h-197"></a><span class="cm">/*</span>
<a name="freeglut_std.h-198"></a><span class="cm"> * GLUT API macro definitions -- fonts definitions</span>
<a name="freeglut_std.h-199"></a><span class="cm"> *</span>
<a name="freeglut_std.h-200"></a><span class="cm"> * Steve Baker suggested to make it binary compatible with GLUT:</span>
<a name="freeglut_std.h-201"></a><span class="cm"> */</span>
<a name="freeglut_std.h-202"></a><span class="cp">#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__WATCOMC__)</span>
<a name="freeglut_std.h-203"></a><span class="cp">#   define  GLUT_STROKE_ROMAN               ((void *)0x0000)</span>
<a name="freeglut_std.h-204"></a><span class="cp">#   define  GLUT_STROKE_MONO_ROMAN          ((void *)0x0001)</span>
<a name="freeglut_std.h-205"></a><span class="cp">#   define  GLUT_BITMAP_9_BY_15             ((void *)0x0002)</span>
<a name="freeglut_std.h-206"></a><span class="cp">#   define  GLUT_BITMAP_8_BY_13             ((void *)0x0003)</span>
<a name="freeglut_std.h-207"></a><span class="cp">#   define  GLUT_BITMAP_TIMES_ROMAN_10      ((void *)0x0004)</span>
<a name="freeglut_std.h-208"></a><span class="cp">#   define  GLUT_BITMAP_TIMES_ROMAN_24      ((void *)0x0005)</span>
<a name="freeglut_std.h-209"></a><span class="cp">#   define  GLUT_BITMAP_HELVETICA_10        ((void *)0x0006)</span>
<a name="freeglut_std.h-210"></a><span class="cp">#   define  GLUT_BITMAP_HELVETICA_12        ((void *)0x0007)</span>
<a name="freeglut_std.h-211"></a><span class="cp">#   define  GLUT_BITMAP_HELVETICA_18        ((void *)0x0008)</span>
<a name="freeglut_std.h-212"></a><span class="cp">#else</span>
<a name="freeglut_std.h-213"></a>    <span class="cm">/*</span>
<a name="freeglut_std.h-214"></a><span class="cm">     * I don&#39;t really know if it&#39;s a good idea... But here it goes:</span>
<a name="freeglut_std.h-215"></a><span class="cm">     */</span>
<a name="freeglut_std.h-216"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutStrokeRoman</span><span class="p">;</span>
<a name="freeglut_std.h-217"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutStrokeMonoRoman</span><span class="p">;</span>
<a name="freeglut_std.h-218"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutBitmap9By15</span><span class="p">;</span>
<a name="freeglut_std.h-219"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutBitmap8By13</span><span class="p">;</span>
<a name="freeglut_std.h-220"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutBitmapTimesRoman10</span><span class="p">;</span>
<a name="freeglut_std.h-221"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutBitmapTimesRoman24</span><span class="p">;</span>
<a name="freeglut_std.h-222"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutBitmapHelvetica10</span><span class="p">;</span>
<a name="freeglut_std.h-223"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutBitmapHelvetica12</span><span class="p">;</span>
<a name="freeglut_std.h-224"></a>    <span class="k">extern</span> <span class="kt">void</span><span class="o">*</span> <span class="n">glutBitmapHelvetica18</span><span class="p">;</span>
<a name="freeglut_std.h-225"></a>
<a name="freeglut_std.h-226"></a>    <span class="cm">/*</span>
<a name="freeglut_std.h-227"></a><span class="cm">     * Those pointers will be used by following definitions:</span>
<a name="freeglut_std.h-228"></a><span class="cm">     */</span>
<a name="freeglut_std.h-229"></a><span class="cp">#   define  GLUT_STROKE_ROMAN               ((void *) &amp;glutStrokeRoman)</span>
<a name="freeglut_std.h-230"></a><span class="cp">#   define  GLUT_STROKE_MONO_ROMAN          ((void *) &amp;glutStrokeMonoRoman)</span>
<a name="freeglut_std.h-231"></a><span class="cp">#   define  GLUT_BITMAP_9_BY_15             ((void *) &amp;glutBitmap9By15)</span>
<a name="freeglut_std.h-232"></a><span class="cp">#   define  GLUT_BITMAP_8_BY_13             ((void *) &amp;glutBitmap8By13)</span>
<a name="freeglut_std.h-233"></a><span class="cp">#   define  GLUT_BITMAP_TIMES_ROMAN_10      ((void *) &amp;glutBitmapTimesRoman10)</span>
<a name="freeglut_std.h-234"></a><span class="cp">#   define  GLUT_BITMAP_TIMES_ROMAN_24      ((void *) &amp;glutBitmapTimesRoman24)</span>
<a name="freeglut_std.h-235"></a><span class="cp">#   define  GLUT_BITMAP_HELVETICA_10        ((void *) &amp;glutBitmapHelvetica10)</span>
<a name="freeglut_std.h-236"></a><span class="cp">#   define  GLUT_BITMAP_HELVETICA_12        ((void *) &amp;glutBitmapHelvetica12)</span>
<a name="freeglut_std.h-237"></a><span class="cp">#   define  GLUT_BITMAP_HELVETICA_18        ((void *) &amp;glutBitmapHelvetica18)</span>
<a name="freeglut_std.h-238"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-239"></a>
<a name="freeglut_std.h-240"></a><span class="cm">/*</span>
<a name="freeglut_std.h-241"></a><span class="cm"> * GLUT API macro definitions -- the glutGet parameters</span>
<a name="freeglut_std.h-242"></a><span class="cm"> */</span>
<a name="freeglut_std.h-243"></a><span class="cp">#define  GLUT_WINDOW_X                      0x0064</span>
<a name="freeglut_std.h-244"></a><span class="cp">#define  GLUT_WINDOW_Y                      0x0065</span>
<a name="freeglut_std.h-245"></a><span class="cp">#define  GLUT_WINDOW_WIDTH                  0x0066</span>
<a name="freeglut_std.h-246"></a><span class="cp">#define  GLUT_WINDOW_HEIGHT                 0x0067</span>
<a name="freeglut_std.h-247"></a><span class="cp">#define  GLUT_WINDOW_BUFFER_SIZE            0x0068</span>
<a name="freeglut_std.h-248"></a><span class="cp">#define  GLUT_WINDOW_STENCIL_SIZE           0x0069</span>
<a name="freeglut_std.h-249"></a><span class="cp">#define  GLUT_WINDOW_DEPTH_SIZE             0x006A</span>
<a name="freeglut_std.h-250"></a><span class="cp">#define  GLUT_WINDOW_RED_SIZE               0x006B</span>
<a name="freeglut_std.h-251"></a><span class="cp">#define  GLUT_WINDOW_GREEN_SIZE             0x006C</span>
<a name="freeglut_std.h-252"></a><span class="cp">#define  GLUT_WINDOW_BLUE_SIZE              0x006D</span>
<a name="freeglut_std.h-253"></a><span class="cp">#define  GLUT_WINDOW_ALPHA_SIZE             0x006E</span>
<a name="freeglut_std.h-254"></a><span class="cp">#define  GLUT_WINDOW_ACCUM_RED_SIZE         0x006F</span>
<a name="freeglut_std.h-255"></a><span class="cp">#define  GLUT_WINDOW_ACCUM_GREEN_SIZE       0x0070</span>
<a name="freeglut_std.h-256"></a><span class="cp">#define  GLUT_WINDOW_ACCUM_BLUE_SIZE        0x0071</span>
<a name="freeglut_std.h-257"></a><span class="cp">#define  GLUT_WINDOW_ACCUM_ALPHA_SIZE       0x0072</span>
<a name="freeglut_std.h-258"></a><span class="cp">#define  GLUT_WINDOW_DOUBLEBUFFER           0x0073</span>
<a name="freeglut_std.h-259"></a><span class="cp">#define  GLUT_WINDOW_RGBA                   0x0074</span>
<a name="freeglut_std.h-260"></a><span class="cp">#define  GLUT_WINDOW_PARENT                 0x0075</span>
<a name="freeglut_std.h-261"></a><span class="cp">#define  GLUT_WINDOW_NUM_CHILDREN           0x0076</span>
<a name="freeglut_std.h-262"></a><span class="cp">#define  GLUT_WINDOW_COLORMAP_SIZE          0x0077</span>
<a name="freeglut_std.h-263"></a><span class="cp">#define  GLUT_WINDOW_NUM_SAMPLES            0x0078</span>
<a name="freeglut_std.h-264"></a><span class="cp">#define  GLUT_WINDOW_STEREO                 0x0079</span>
<a name="freeglut_std.h-265"></a><span class="cp">#define  GLUT_WINDOW_CURSOR                 0x007A</span>
<a name="freeglut_std.h-266"></a>
<a name="freeglut_std.h-267"></a><span class="cp">#define  GLUT_SCREEN_WIDTH                  0x00C8</span>
<a name="freeglut_std.h-268"></a><span class="cp">#define  GLUT_SCREEN_HEIGHT                 0x00C9</span>
<a name="freeglut_std.h-269"></a><span class="cp">#define  GLUT_SCREEN_WIDTH_MM               0x00CA</span>
<a name="freeglut_std.h-270"></a><span class="cp">#define  GLUT_SCREEN_HEIGHT_MM              0x00CB</span>
<a name="freeglut_std.h-271"></a><span class="cp">#define  GLUT_MENU_NUM_ITEMS                0x012C</span>
<a name="freeglut_std.h-272"></a><span class="cp">#define  GLUT_DISPLAY_MODE_POSSIBLE         0x0190</span>
<a name="freeglut_std.h-273"></a><span class="cp">#define  GLUT_INIT_WINDOW_X                 0x01F4</span>
<a name="freeglut_std.h-274"></a><span class="cp">#define  GLUT_INIT_WINDOW_Y                 0x01F5</span>
<a name="freeglut_std.h-275"></a><span class="cp">#define  GLUT_INIT_WINDOW_WIDTH             0x01F6</span>
<a name="freeglut_std.h-276"></a><span class="cp">#define  GLUT_INIT_WINDOW_HEIGHT            0x01F7</span>
<a name="freeglut_std.h-277"></a><span class="cp">#define  GLUT_INIT_DISPLAY_MODE             0x01F8</span>
<a name="freeglut_std.h-278"></a><span class="cp">#define  GLUT_ELAPSED_TIME                  0x02BC</span>
<a name="freeglut_std.h-279"></a><span class="cp">#define  GLUT_WINDOW_FORMAT_ID              0x007B</span>
<a name="freeglut_std.h-280"></a>
<a name="freeglut_std.h-281"></a><span class="cm">/*</span>
<a name="freeglut_std.h-282"></a><span class="cm"> * GLUT API macro definitions -- the glutDeviceGet parameters</span>
<a name="freeglut_std.h-283"></a><span class="cm"> */</span>
<a name="freeglut_std.h-284"></a><span class="cp">#define  GLUT_HAS_KEYBOARD                  0x0258</span>
<a name="freeglut_std.h-285"></a><span class="cp">#define  GLUT_HAS_MOUSE                     0x0259</span>
<a name="freeglut_std.h-286"></a><span class="cp">#define  GLUT_HAS_SPACEBALL                 0x025A</span>
<a name="freeglut_std.h-287"></a><span class="cp">#define  GLUT_HAS_DIAL_AND_BUTTON_BOX       0x025B</span>
<a name="freeglut_std.h-288"></a><span class="cp">#define  GLUT_HAS_TABLET                    0x025C</span>
<a name="freeglut_std.h-289"></a><span class="cp">#define  GLUT_NUM_MOUSE_BUTTONS             0x025D</span>
<a name="freeglut_std.h-290"></a><span class="cp">#define  GLUT_NUM_SPACEBALL_BUTTONS         0x025E</span>
<a name="freeglut_std.h-291"></a><span class="cp">#define  GLUT_NUM_BUTTON_BOX_BUTTONS        0x025F</span>
<a name="freeglut_std.h-292"></a><span class="cp">#define  GLUT_NUM_DIALS                     0x0260</span>
<a name="freeglut_std.h-293"></a><span class="cp">#define  GLUT_NUM_TABLET_BUTTONS            0x0261</span>
<a name="freeglut_std.h-294"></a><span class="cp">#define  GLUT_DEVICE_IGNORE_KEY_REPEAT      0x0262</span>
<a name="freeglut_std.h-295"></a><span class="cp">#define  GLUT_DEVICE_KEY_REPEAT             0x0263</span>
<a name="freeglut_std.h-296"></a><span class="cp">#define  GLUT_HAS_JOYSTICK                  0x0264</span>
<a name="freeglut_std.h-297"></a><span class="cp">#define  GLUT_OWNS_JOYSTICK                 0x0265</span>
<a name="freeglut_std.h-298"></a><span class="cp">#define  GLUT_JOYSTICK_BUTTONS              0x0266</span>
<a name="freeglut_std.h-299"></a><span class="cp">#define  GLUT_JOYSTICK_AXES                 0x0267</span>
<a name="freeglut_std.h-300"></a><span class="cp">#define  GLUT_JOYSTICK_POLL_RATE            0x0268</span>
<a name="freeglut_std.h-301"></a>
<a name="freeglut_std.h-302"></a><span class="cm">/*</span>
<a name="freeglut_std.h-303"></a><span class="cm"> * GLUT API macro definitions -- the glutLayerGet parameters</span>
<a name="freeglut_std.h-304"></a><span class="cm"> */</span>
<a name="freeglut_std.h-305"></a><span class="cp">#define  GLUT_OVERLAY_POSSIBLE              0x0320</span>
<a name="freeglut_std.h-306"></a><span class="cp">#define  GLUT_LAYER_IN_USE                  0x0321</span>
<a name="freeglut_std.h-307"></a><span class="cp">#define  GLUT_HAS_OVERLAY                   0x0322</span>
<a name="freeglut_std.h-308"></a><span class="cp">#define  GLUT_TRANSPARENT_INDEX             0x0323</span>
<a name="freeglut_std.h-309"></a><span class="cp">#define  GLUT_NORMAL_DAMAGED                0x0324</span>
<a name="freeglut_std.h-310"></a><span class="cp">#define  GLUT_OVERLAY_DAMAGED               0x0325</span>
<a name="freeglut_std.h-311"></a>
<a name="freeglut_std.h-312"></a><span class="cm">/*</span>
<a name="freeglut_std.h-313"></a><span class="cm"> * GLUT API macro definitions -- the glutVideoResizeGet parameters</span>
<a name="freeglut_std.h-314"></a><span class="cm"> */</span>
<a name="freeglut_std.h-315"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_POSSIBLE         0x0384</span>
<a name="freeglut_std.h-316"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_IN_USE           0x0385</span>
<a name="freeglut_std.h-317"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_X_DELTA          0x0386</span>
<a name="freeglut_std.h-318"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_Y_DELTA          0x0387</span>
<a name="freeglut_std.h-319"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_WIDTH_DELTA      0x0388</span>
<a name="freeglut_std.h-320"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_HEIGHT_DELTA     0x0389</span>
<a name="freeglut_std.h-321"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_X                0x038A</span>
<a name="freeglut_std.h-322"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_Y                0x038B</span>
<a name="freeglut_std.h-323"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_WIDTH            0x038C</span>
<a name="freeglut_std.h-324"></a><span class="cp">#define  GLUT_VIDEO_RESIZE_HEIGHT           0x038D</span>
<a name="freeglut_std.h-325"></a>
<a name="freeglut_std.h-326"></a><span class="cm">/*</span>
<a name="freeglut_std.h-327"></a><span class="cm"> * GLUT API macro definitions -- the glutUseLayer parameters</span>
<a name="freeglut_std.h-328"></a><span class="cm"> */</span>
<a name="freeglut_std.h-329"></a><span class="cp">#define  GLUT_NORMAL                        0x0000</span>
<a name="freeglut_std.h-330"></a><span class="cp">#define  GLUT_OVERLAY                       0x0001</span>
<a name="freeglut_std.h-331"></a>
<a name="freeglut_std.h-332"></a><span class="cm">/*</span>
<a name="freeglut_std.h-333"></a><span class="cm"> * GLUT API macro definitions -- the glutGetModifiers parameters</span>
<a name="freeglut_std.h-334"></a><span class="cm"> */</span>
<a name="freeglut_std.h-335"></a><span class="cp">#define  GLUT_ACTIVE_SHIFT                  0x0001</span>
<a name="freeglut_std.h-336"></a><span class="cp">#define  GLUT_ACTIVE_CTRL                   0x0002</span>
<a name="freeglut_std.h-337"></a><span class="cp">#define  GLUT_ACTIVE_ALT                    0x0004</span>
<a name="freeglut_std.h-338"></a>
<a name="freeglut_std.h-339"></a><span class="cm">/*</span>
<a name="freeglut_std.h-340"></a><span class="cm"> * GLUT API macro definitions -- the glutSetCursor parameters</span>
<a name="freeglut_std.h-341"></a><span class="cm"> */</span>
<a name="freeglut_std.h-342"></a><span class="cp">#define  GLUT_CURSOR_RIGHT_ARROW            0x0000</span>
<a name="freeglut_std.h-343"></a><span class="cp">#define  GLUT_CURSOR_LEFT_ARROW             0x0001</span>
<a name="freeglut_std.h-344"></a><span class="cp">#define  GLUT_CURSOR_INFO                   0x0002</span>
<a name="freeglut_std.h-345"></a><span class="cp">#define  GLUT_CURSOR_DESTROY                0x0003</span>
<a name="freeglut_std.h-346"></a><span class="cp">#define  GLUT_CURSOR_HELP                   0x0004</span>
<a name="freeglut_std.h-347"></a><span class="cp">#define  GLUT_CURSOR_CYCLE                  0x0005</span>
<a name="freeglut_std.h-348"></a><span class="cp">#define  GLUT_CURSOR_SPRAY                  0x0006</span>
<a name="freeglut_std.h-349"></a><span class="cp">#define  GLUT_CURSOR_WAIT                   0x0007</span>
<a name="freeglut_std.h-350"></a><span class="cp">#define  GLUT_CURSOR_TEXT                   0x0008</span>
<a name="freeglut_std.h-351"></a><span class="cp">#define  GLUT_CURSOR_CROSSHAIR              0x0009</span>
<a name="freeglut_std.h-352"></a><span class="cp">#define  GLUT_CURSOR_UP_DOWN                0x000A</span>
<a name="freeglut_std.h-353"></a><span class="cp">#define  GLUT_CURSOR_LEFT_RIGHT             0x000B</span>
<a name="freeglut_std.h-354"></a><span class="cp">#define  GLUT_CURSOR_TOP_SIDE               0x000C</span>
<a name="freeglut_std.h-355"></a><span class="cp">#define  GLUT_CURSOR_BOTTOM_SIDE            0x000D</span>
<a name="freeglut_std.h-356"></a><span class="cp">#define  GLUT_CURSOR_LEFT_SIDE              0x000E</span>
<a name="freeglut_std.h-357"></a><span class="cp">#define  GLUT_CURSOR_RIGHT_SIDE             0x000F</span>
<a name="freeglut_std.h-358"></a><span class="cp">#define  GLUT_CURSOR_TOP_LEFT_CORNER        0x0010</span>
<a name="freeglut_std.h-359"></a><span class="cp">#define  GLUT_CURSOR_TOP_RIGHT_CORNER       0x0011</span>
<a name="freeglut_std.h-360"></a><span class="cp">#define  GLUT_CURSOR_BOTTOM_RIGHT_CORNER    0x0012</span>
<a name="freeglut_std.h-361"></a><span class="cp">#define  GLUT_CURSOR_BOTTOM_LEFT_CORNER     0x0013</span>
<a name="freeglut_std.h-362"></a><span class="cp">#define  GLUT_CURSOR_INHERIT                0x0064</span>
<a name="freeglut_std.h-363"></a><span class="cp">#define  GLUT_CURSOR_NONE                   0x0065</span>
<a name="freeglut_std.h-364"></a><span class="cp">#define  GLUT_CURSOR_FULL_CROSSHAIR         0x0066</span>
<a name="freeglut_std.h-365"></a>
<a name="freeglut_std.h-366"></a><span class="cm">/*</span>
<a name="freeglut_std.h-367"></a><span class="cm"> * GLUT API macro definitions -- RGB color component specification definitions</span>
<a name="freeglut_std.h-368"></a><span class="cm"> */</span>
<a name="freeglut_std.h-369"></a><span class="cp">#define  GLUT_RED                           0x0000</span>
<a name="freeglut_std.h-370"></a><span class="cp">#define  GLUT_GREEN                         0x0001</span>
<a name="freeglut_std.h-371"></a><span class="cp">#define  GLUT_BLUE                          0x0002</span>
<a name="freeglut_std.h-372"></a>
<a name="freeglut_std.h-373"></a><span class="cm">/*</span>
<a name="freeglut_std.h-374"></a><span class="cm"> * GLUT API macro definitions -- additional keyboard and joystick definitions</span>
<a name="freeglut_std.h-375"></a><span class="cm"> */</span>
<a name="freeglut_std.h-376"></a><span class="cp">#define  GLUT_KEY_REPEAT_OFF                0x0000</span>
<a name="freeglut_std.h-377"></a><span class="cp">#define  GLUT_KEY_REPEAT_ON                 0x0001</span>
<a name="freeglut_std.h-378"></a><span class="cp">#define  GLUT_KEY_REPEAT_DEFAULT            0x0002</span>
<a name="freeglut_std.h-379"></a>
<a name="freeglut_std.h-380"></a><span class="cp">#define  GLUT_JOYSTICK_BUTTON_A             0x0001</span>
<a name="freeglut_std.h-381"></a><span class="cp">#define  GLUT_JOYSTICK_BUTTON_B             0x0002</span>
<a name="freeglut_std.h-382"></a><span class="cp">#define  GLUT_JOYSTICK_BUTTON_C             0x0004</span>
<a name="freeglut_std.h-383"></a><span class="cp">#define  GLUT_JOYSTICK_BUTTON_D             0x0008</span>
<a name="freeglut_std.h-384"></a>
<a name="freeglut_std.h-385"></a><span class="cm">/*</span>
<a name="freeglut_std.h-386"></a><span class="cm"> * GLUT API macro definitions -- game mode definitions</span>
<a name="freeglut_std.h-387"></a><span class="cm"> */</span>
<a name="freeglut_std.h-388"></a><span class="cp">#define  GLUT_GAME_MODE_ACTIVE              0x0000</span>
<a name="freeglut_std.h-389"></a><span class="cp">#define  GLUT_GAME_MODE_POSSIBLE            0x0001</span>
<a name="freeglut_std.h-390"></a><span class="cp">#define  GLUT_GAME_MODE_WIDTH               0x0002</span>
<a name="freeglut_std.h-391"></a><span class="cp">#define  GLUT_GAME_MODE_HEIGHT              0x0003</span>
<a name="freeglut_std.h-392"></a><span class="cp">#define  GLUT_GAME_MODE_PIXEL_DEPTH         0x0004</span>
<a name="freeglut_std.h-393"></a><span class="cp">#define  GLUT_GAME_MODE_REFRESH_RATE        0x0005</span>
<a name="freeglut_std.h-394"></a><span class="cp">#define  GLUT_GAME_MODE_DISPLAY_CHANGED     0x0006</span>
<a name="freeglut_std.h-395"></a>
<a name="freeglut_std.h-396"></a><span class="cm">/*</span>
<a name="freeglut_std.h-397"></a><span class="cm"> * Initialization functions, see fglut_init.c</span>
<a name="freeglut_std.h-398"></a><span class="cm"> */</span>
<a name="freeglut_std.h-399"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInit</span><span class="p">(</span> <span class="kt">int</span><span class="o">*</span> <span class="n">pargc</span><span class="p">,</span> <span class="kt">char</span><span class="o">**</span> <span class="n">argv</span> <span class="p">);</span>
<a name="freeglut_std.h-400"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitWindowPosition</span><span class="p">(</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span> <span class="p">);</span>
<a name="freeglut_std.h-401"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitWindowSize</span><span class="p">(</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span> <span class="p">);</span>
<a name="freeglut_std.h-402"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitDisplayMode</span><span class="p">(</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">displayMode</span> <span class="p">);</span>
<a name="freeglut_std.h-403"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutInitDisplayString</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">displayMode</span> <span class="p">);</span>
<a name="freeglut_std.h-404"></a>
<a name="freeglut_std.h-405"></a><span class="cm">/*</span>
<a name="freeglut_std.h-406"></a><span class="cm"> * Process loop function, see fg_main.c</span>
<a name="freeglut_std.h-407"></a><span class="cm"> */</span>
<a name="freeglut_std.h-408"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutMainLoop</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-409"></a>
<a name="freeglut_std.h-410"></a><span class="cm">/*</span>
<a name="freeglut_std.h-411"></a><span class="cm"> * Window management functions, see fg_window.c</span>
<a name="freeglut_std.h-412"></a><span class="cm"> */</span>
<a name="freeglut_std.h-413"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutCreateWindow</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">title</span> <span class="p">);</span>
<a name="freeglut_std.h-414"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutCreateSubWindow</span><span class="p">(</span> <span class="kt">int</span> <span class="n">window</span><span class="p">,</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span> <span class="p">);</span>
<a name="freeglut_std.h-415"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutDestroyWindow</span><span class="p">(</span> <span class="kt">int</span> <span class="n">window</span> <span class="p">);</span>
<a name="freeglut_std.h-416"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetWindow</span><span class="p">(</span> <span class="kt">int</span> <span class="n">window</span> <span class="p">);</span>
<a name="freeglut_std.h-417"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutGetWindow</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-418"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetWindowTitle</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">title</span> <span class="p">);</span>
<a name="freeglut_std.h-419"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetIconTitle</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">title</span> <span class="p">);</span>
<a name="freeglut_std.h-420"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutReshapeWindow</span><span class="p">(</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span> <span class="p">);</span>
<a name="freeglut_std.h-421"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPositionWindow</span><span class="p">(</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span> <span class="p">);</span>
<a name="freeglut_std.h-422"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutShowWindow</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-423"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutHideWindow</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-424"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutIconifyWindow</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-425"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPushWindow</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-426"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPopWindow</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-427"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutFullScreen</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-428"></a>
<a name="freeglut_std.h-429"></a><span class="cm">/*</span>
<a name="freeglut_std.h-430"></a><span class="cm"> * Display-related functions, see fg_display.c</span>
<a name="freeglut_std.h-431"></a><span class="cm"> */</span>
<a name="freeglut_std.h-432"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPostWindowRedisplay</span><span class="p">(</span> <span class="kt">int</span> <span class="n">window</span> <span class="p">);</span>
<a name="freeglut_std.h-433"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPostRedisplay</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-434"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSwapBuffers</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-435"></a>
<a name="freeglut_std.h-436"></a><span class="cm">/*</span>
<a name="freeglut_std.h-437"></a><span class="cm"> * Mouse cursor functions, see fg_cursor.c</span>
<a name="freeglut_std.h-438"></a><span class="cm"> */</span>
<a name="freeglut_std.h-439"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWarpPointer</span><span class="p">(</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span> <span class="p">);</span>
<a name="freeglut_std.h-440"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetCursor</span><span class="p">(</span> <span class="kt">int</span> <span class="n">cursor</span> <span class="p">);</span>
<a name="freeglut_std.h-441"></a>
<a name="freeglut_std.h-442"></a><span class="cm">/*</span>
<a name="freeglut_std.h-443"></a><span class="cm"> * Overlay stuff, see fg_overlay.c</span>
<a name="freeglut_std.h-444"></a><span class="cm"> */</span>
<a name="freeglut_std.h-445"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutEstablishOverlay</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-446"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutRemoveOverlay</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-447"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutUseLayer</span><span class="p">(</span> <span class="n">GLenum</span> <span class="n">layer</span> <span class="p">);</span>
<a name="freeglut_std.h-448"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPostOverlayRedisplay</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-449"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPostWindowOverlayRedisplay</span><span class="p">(</span> <span class="kt">int</span> <span class="n">window</span> <span class="p">);</span>
<a name="freeglut_std.h-450"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutShowOverlay</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-451"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutHideOverlay</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-452"></a>
<a name="freeglut_std.h-453"></a><span class="cm">/*</span>
<a name="freeglut_std.h-454"></a><span class="cm"> * Menu stuff, see fg_menu.c</span>
<a name="freeglut_std.h-455"></a><span class="cm"> */</span>
<a name="freeglut_std.h-456"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutCreateMenu</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span> <span class="n">menu</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-457"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutDestroyMenu</span><span class="p">(</span> <span class="kt">int</span> <span class="n">menu</span> <span class="p">);</span>
<a name="freeglut_std.h-458"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutGetMenu</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-459"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetMenu</span><span class="p">(</span> <span class="kt">int</span> <span class="n">menu</span> <span class="p">);</span>
<a name="freeglut_std.h-460"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutAddMenuEntry</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">label</span><span class="p">,</span> <span class="kt">int</span> <span class="n">value</span> <span class="p">);</span>
<a name="freeglut_std.h-461"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutAddSubMenu</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">label</span><span class="p">,</span> <span class="kt">int</span> <span class="n">subMenu</span> <span class="p">);</span>
<a name="freeglut_std.h-462"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutChangeToMenuEntry</span><span class="p">(</span> <span class="kt">int</span> <span class="n">item</span><span class="p">,</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">label</span><span class="p">,</span> <span class="kt">int</span> <span class="n">value</span> <span class="p">);</span>
<a name="freeglut_std.h-463"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutChangeToSubMenu</span><span class="p">(</span> <span class="kt">int</span> <span class="n">item</span><span class="p">,</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">label</span><span class="p">,</span> <span class="kt">int</span> <span class="n">value</span> <span class="p">);</span>
<a name="freeglut_std.h-464"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutRemoveMenuItem</span><span class="p">(</span> <span class="kt">int</span> <span class="n">item</span> <span class="p">);</span>
<a name="freeglut_std.h-465"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutAttachMenu</span><span class="p">(</span> <span class="kt">int</span> <span class="n">button</span> <span class="p">);</span>
<a name="freeglut_std.h-466"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutDetachMenu</span><span class="p">(</span> <span class="kt">int</span> <span class="n">button</span> <span class="p">);</span>
<a name="freeglut_std.h-467"></a>
<a name="freeglut_std.h-468"></a><span class="cm">/*</span>
<a name="freeglut_std.h-469"></a><span class="cm"> * Global callback functions, see fg_callbacks.c</span>
<a name="freeglut_std.h-470"></a><span class="cm"> */</span>
<a name="freeglut_std.h-471"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutTimerFunc</span><span class="p">(</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">time</span><span class="p">,</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span> <span class="p">),</span> <span class="kt">int</span> <span class="n">value</span> <span class="p">);</span>
<a name="freeglut_std.h-472"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutIdleFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">void</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-473"></a>
<a name="freeglut_std.h-474"></a><span class="cm">/*</span>
<a name="freeglut_std.h-475"></a><span class="cm"> * Window-specific callback functions, see fg_callbacks.c</span>
<a name="freeglut_std.h-476"></a><span class="cm"> */</span>
<a name="freeglut_std.h-477"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutKeyboardFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">unsigned</span> <span class="kt">char</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-478"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSpecialFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-479"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutReshapeFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-480"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutVisibilityFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-481"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutDisplayFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">void</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-482"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutMouseFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-483"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutMotionFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-484"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutPassiveMotionFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-485"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutEntryFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-486"></a>
<a name="freeglut_std.h-487"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutKeyboardUpFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">unsigned</span> <span class="kt">char</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-488"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSpecialUpFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-489"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutJoystickFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">unsigned</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">),</span> <span class="kt">int</span> <span class="n">pollInterval</span> <span class="p">);</span>
<a name="freeglut_std.h-490"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutMenuStateFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-491"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutMenuStatusFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-492"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutOverlayDisplayFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">void</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-493"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWindowStatusFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-494"></a>
<a name="freeglut_std.h-495"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSpaceballMotionFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-496"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSpaceballRotateFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-497"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSpaceballButtonFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-498"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutButtonBoxFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-499"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutDialsFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-500"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutTabletMotionFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-501"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutTabletButtonFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">callback</span><span class="p">)(</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span><span class="p">,</span> <span class="kt">int</span> <span class="p">)</span> <span class="p">);</span>
<a name="freeglut_std.h-502"></a>
<a name="freeglut_std.h-503"></a><span class="cm">/*</span>
<a name="freeglut_std.h-504"></a><span class="cm"> * State setting and retrieval functions, see fg_state.c</span>
<a name="freeglut_std.h-505"></a><span class="cm"> */</span>
<a name="freeglut_std.h-506"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutGet</span><span class="p">(</span> <span class="n">GLenum</span> <span class="n">query</span> <span class="p">);</span>
<a name="freeglut_std.h-507"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutDeviceGet</span><span class="p">(</span> <span class="n">GLenum</span> <span class="n">query</span> <span class="p">);</span>
<a name="freeglut_std.h-508"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutGetModifiers</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-509"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutLayerGet</span><span class="p">(</span> <span class="n">GLenum</span> <span class="n">query</span> <span class="p">);</span>
<a name="freeglut_std.h-510"></a>
<a name="freeglut_std.h-511"></a><span class="cm">/*</span>
<a name="freeglut_std.h-512"></a><span class="cm"> * Font stuff, see fg_font.c</span>
<a name="freeglut_std.h-513"></a><span class="cm"> */</span>
<a name="freeglut_std.h-514"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutBitmapCharacter</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="kt">int</span> <span class="n">character</span> <span class="p">);</span>
<a name="freeglut_std.h-515"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutBitmapWidth</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="kt">int</span> <span class="n">character</span> <span class="p">);</span>
<a name="freeglut_std.h-516"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutStrokeCharacter</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="kt">int</span> <span class="n">character</span> <span class="p">);</span>
<a name="freeglut_std.h-517"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutStrokeWidth</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="kt">int</span> <span class="n">character</span> <span class="p">);</span>
<a name="freeglut_std.h-518"></a><span class="n">FGAPI</span> <span class="n">GLfloat</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutStrokeWidthf</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="kt">int</span> <span class="n">character</span> <span class="p">);</span> <span class="cm">/* GLUT 3.8 */</span>
<a name="freeglut_std.h-519"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutBitmapLength</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="k">const</span> <span class="kt">unsigned</span> <span class="kt">char</span><span class="o">*</span> <span class="n">string</span> <span class="p">);</span>
<a name="freeglut_std.h-520"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutStrokeLength</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="k">const</span> <span class="kt">unsigned</span> <span class="kt">char</span><span class="o">*</span> <span class="n">string</span> <span class="p">);</span>
<a name="freeglut_std.h-521"></a><span class="n">FGAPI</span> <span class="n">GLfloat</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutStrokeLengthf</span><span class="p">(</span> <span class="kt">void</span><span class="o">*</span> <span class="n">font</span><span class="p">,</span> <span class="k">const</span> <span class="kt">unsigned</span> <span class="kt">char</span> <span class="o">*</span><span class="n">string</span> <span class="p">);</span> <span class="cm">/* GLUT 3.8 */</span>
<a name="freeglut_std.h-522"></a>
<a name="freeglut_std.h-523"></a><span class="cm">/*</span>
<a name="freeglut_std.h-524"></a><span class="cm"> * Geometry functions, see fg_geometry.c</span>
<a name="freeglut_std.h-525"></a><span class="cm"> */</span>
<a name="freeglut_std.h-526"></a>
<a name="freeglut_std.h-527"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireCube</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_std.h-528"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidCube</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_std.h-529"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireSphere</span><span class="p">(</span> <span class="kt">double</span> <span class="n">radius</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">slices</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">stacks</span> <span class="p">);</span>
<a name="freeglut_std.h-530"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidSphere</span><span class="p">(</span> <span class="kt">double</span> <span class="n">radius</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">slices</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">stacks</span> <span class="p">);</span>
<a name="freeglut_std.h-531"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireCone</span><span class="p">(</span> <span class="kt">double</span> <span class="n">base</span><span class="p">,</span> <span class="kt">double</span> <span class="n">height</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">slices</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">stacks</span> <span class="p">);</span>
<a name="freeglut_std.h-532"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidCone</span><span class="p">(</span> <span class="kt">double</span> <span class="n">base</span><span class="p">,</span> <span class="kt">double</span> <span class="n">height</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">slices</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">stacks</span> <span class="p">);</span>
<a name="freeglut_std.h-533"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireTorus</span><span class="p">(</span> <span class="kt">double</span> <span class="n">innerRadius</span><span class="p">,</span> <span class="kt">double</span> <span class="n">outerRadius</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">sides</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">rings</span> <span class="p">);</span>
<a name="freeglut_std.h-534"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidTorus</span><span class="p">(</span> <span class="kt">double</span> <span class="n">innerRadius</span><span class="p">,</span> <span class="kt">double</span> <span class="n">outerRadius</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">sides</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">rings</span> <span class="p">);</span>
<a name="freeglut_std.h-535"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireDodecahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-536"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidDodecahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-537"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireOctahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-538"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidOctahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-539"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireTetrahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-540"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidTetrahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-541"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireIcosahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-542"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidIcosahedron</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-543"></a>
<a name="freeglut_std.h-544"></a><span class="cm">/*</span>
<a name="freeglut_std.h-545"></a><span class="cm"> * Teapot rendering functions, found in fg_teapot.c</span>
<a name="freeglut_std.h-546"></a><span class="cm"> * NB: front facing polygons have clockwise winding, not counter clockwise</span>
<a name="freeglut_std.h-547"></a><span class="cm"> */</span>
<a name="freeglut_std.h-548"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutWireTeapot</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_std.h-549"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSolidTeapot</span><span class="p">(</span> <span class="kt">double</span> <span class="n">size</span> <span class="p">);</span>
<a name="freeglut_std.h-550"></a>
<a name="freeglut_std.h-551"></a><span class="cm">/*</span>
<a name="freeglut_std.h-552"></a><span class="cm"> * Game mode functions, see fg_gamemode.c</span>
<a name="freeglut_std.h-553"></a><span class="cm"> */</span>
<a name="freeglut_std.h-554"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutGameModeString</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">string</span> <span class="p">);</span>
<a name="freeglut_std.h-555"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutEnterGameMode</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-556"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutLeaveGameMode</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-557"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutGameModeGet</span><span class="p">(</span> <span class="n">GLenum</span> <span class="n">query</span> <span class="p">);</span>
<a name="freeglut_std.h-558"></a>
<a name="freeglut_std.h-559"></a><span class="cm">/*</span>
<a name="freeglut_std.h-560"></a><span class="cm"> * Video resize functions, see fg_videoresize.c</span>
<a name="freeglut_std.h-561"></a><span class="cm"> */</span>
<a name="freeglut_std.h-562"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutVideoResizeGet</span><span class="p">(</span> <span class="n">GLenum</span> <span class="n">query</span> <span class="p">);</span>
<a name="freeglut_std.h-563"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetupVideoResizing</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-564"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutStopVideoResizing</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-565"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutVideoResize</span><span class="p">(</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span> <span class="p">);</span>
<a name="freeglut_std.h-566"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutVideoPan</span><span class="p">(</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span> <span class="p">);</span>
<a name="freeglut_std.h-567"></a>
<a name="freeglut_std.h-568"></a><span class="cm">/*</span>
<a name="freeglut_std.h-569"></a><span class="cm"> * Colormap functions, see fg_misc.c</span>
<a name="freeglut_std.h-570"></a><span class="cm"> */</span>
<a name="freeglut_std.h-571"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetColor</span><span class="p">(</span> <span class="kt">int</span> <span class="n">color</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">red</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">green</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">blue</span> <span class="p">);</span>
<a name="freeglut_std.h-572"></a><span class="n">FGAPI</span> <span class="n">GLfloat</span> <span class="n">FGAPIENTRY</span> <span class="nf">glutGetColor</span><span class="p">(</span> <span class="kt">int</span> <span class="n">color</span><span class="p">,</span> <span class="kt">int</span> <span class="n">component</span> <span class="p">);</span>
<a name="freeglut_std.h-573"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutCopyColormap</span><span class="p">(</span> <span class="kt">int</span> <span class="n">window</span> <span class="p">);</span>
<a name="freeglut_std.h-574"></a>
<a name="freeglut_std.h-575"></a><span class="cm">/*</span>
<a name="freeglut_std.h-576"></a><span class="cm"> * Misc keyboard and joystick functions, see fg_misc.c</span>
<a name="freeglut_std.h-577"></a><span class="cm"> */</span>
<a name="freeglut_std.h-578"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutIgnoreKeyRepeat</span><span class="p">(</span> <span class="kt">int</span> <span class="n">ignore</span> <span class="p">);</span>
<a name="freeglut_std.h-579"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutSetKeyRepeat</span><span class="p">(</span> <span class="kt">int</span> <span class="n">repeatMode</span> <span class="p">);</span>
<a name="freeglut_std.h-580"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutForceJoystickFunc</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-581"></a>
<a name="freeglut_std.h-582"></a><span class="cm">/*</span>
<a name="freeglut_std.h-583"></a><span class="cm"> * Misc functions, see fg_misc.c</span>
<a name="freeglut_std.h-584"></a><span class="cm"> */</span>
<a name="freeglut_std.h-585"></a><span class="n">FGAPI</span> <span class="kt">int</span>     <span class="n">FGAPIENTRY</span> <span class="nf">glutExtensionSupported</span><span class="p">(</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="n">extension</span> <span class="p">);</span>
<a name="freeglut_std.h-586"></a><span class="n">FGAPI</span> <span class="kt">void</span>    <span class="n">FGAPIENTRY</span> <span class="nf">glutReportErrors</span><span class="p">(</span> <span class="kt">void</span> <span class="p">);</span>
<a name="freeglut_std.h-587"></a>
<a name="freeglut_std.h-588"></a><span class="cm">/* Comment from glut.h of classic GLUT:</span>
<a name="freeglut_std.h-589"></a>
<a name="freeglut_std.h-590"></a><span class="cm">   Win32 has an annoying issue where there are multiple C run-time</span>
<a name="freeglut_std.h-591"></a><span class="cm">   libraries (CRTs).  If the executable is linked with a different CRT</span>
<a name="freeglut_std.h-592"></a><span class="cm">   from the GLUT DLL, the GLUT DLL will not share the same CRT static</span>
<a name="freeglut_std.h-593"></a><span class="cm">   data seen by the executable.  In particular, atexit callbacks registered</span>
<a name="freeglut_std.h-594"></a><span class="cm">   in the executable will not be called if GLUT calls its (different)</span>
<a name="freeglut_std.h-595"></a><span class="cm">   exit routine).  GLUT is typically built with the</span>
<a name="freeglut_std.h-596"></a><span class="cm">   &quot;/MD&quot; option (the CRT with multithreading DLL support), but the Visual</span>
<a name="freeglut_std.h-597"></a><span class="cm">   C++ linker default is &quot;/ML&quot; (the single threaded CRT).</span>
<a name="freeglut_std.h-598"></a>
<a name="freeglut_std.h-599"></a><span class="cm">   One workaround to this issue is requiring users to always link with</span>
<a name="freeglut_std.h-600"></a><span class="cm">   the same CRT as GLUT is compiled with.  That requires users supply a</span>
<a name="freeglut_std.h-601"></a><span class="cm">   non-standard option.  GLUT 3.7 has its own built-in workaround where</span>
<a name="freeglut_std.h-602"></a><span class="cm">   the executable&#39;s &quot;exit&quot; function pointer is covertly passed to GLUT.</span>
<a name="freeglut_std.h-603"></a><span class="cm">   GLUT then calls the executable&#39;s exit function pointer to ensure that</span>
<a name="freeglut_std.h-604"></a><span class="cm">   any &quot;atexit&quot; calls registered by the application are called if GLUT</span>
<a name="freeglut_std.h-605"></a><span class="cm">   needs to exit.</span>
<a name="freeglut_std.h-606"></a>
<a name="freeglut_std.h-607"></a><span class="cm">   Note that the __glut*WithExit routines should NEVER be called directly.</span>
<a name="freeglut_std.h-608"></a><span class="cm">   To avoid the atexit workaround, #define GLUT_DISABLE_ATEXIT_HACK. */</span>
<a name="freeglut_std.h-609"></a>
<a name="freeglut_std.h-610"></a><span class="cm">/* to get the prototype for exit() */</span>
<a name="freeglut_std.h-611"></a><span class="cp">#include</span> <span class="cpf">&lt;stdlib.h&gt;</span><span class="cp"></span>
<a name="freeglut_std.h-612"></a>
<a name="freeglut_std.h-613"></a><span class="cp">#if defined(_WIN32) &amp;&amp; !defined(GLUT_DISABLE_ATEXIT_HACK) &amp;&amp; !defined(__WATCOMC__)</span>
<a name="freeglut_std.h-614"></a><span class="n">FGAPI</span> <span class="kt">void</span> <span class="n">FGAPIENTRY</span> <span class="nf">__glutInitWithExit</span><span class="p">(</span><span class="kt">int</span> <span class="o">*</span><span class="n">argcp</span><span class="p">,</span> <span class="kt">char</span> <span class="o">**</span><span class="n">argv</span><span class="p">,</span> <span class="kt">void</span> <span class="p">(</span><span class="kr">__cdecl</span> <span class="o">*</span><span class="n">exitfunc</span><span class="p">)(</span><span class="kt">int</span><span class="p">));</span>
<a name="freeglut_std.h-615"></a><span class="n">FGAPI</span> <span class="kt">int</span> <span class="n">FGAPIENTRY</span> <span class="nf">__glutCreateWindowWithExit</span><span class="p">(</span><span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">title</span><span class="p">,</span> <span class="kt">void</span> <span class="p">(</span><span class="kr">__cdecl</span> <span class="o">*</span><span class="n">exitfunc</span><span class="p">)(</span><span class="kt">int</span><span class="p">));</span>
<a name="freeglut_std.h-616"></a><span class="n">FGAPI</span> <span class="kt">int</span> <span class="n">FGAPIENTRY</span> <span class="nf">__glutCreateMenuWithExit</span><span class="p">(</span><span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">func</span><span class="p">)(</span><span class="kt">int</span><span class="p">),</span> <span class="kt">void</span> <span class="p">(</span><span class="kr">__cdecl</span> <span class="o">*</span><span class="n">exitfunc</span><span class="p">)(</span><span class="kt">int</span><span class="p">));</span>
<a name="freeglut_std.h-617"></a><span class="cp">#ifndef FREEGLUT_BUILDING_LIB</span>
<a name="freeglut_std.h-618"></a><span class="cp">#if defined(__GNUC__)</span>
<a name="freeglut_std.h-619"></a><span class="cp">#define FGUNUSED __attribute__((unused))</span>
<a name="freeglut_std.h-620"></a><span class="cp">#else</span>
<a name="freeglut_std.h-621"></a><span class="cp">#define FGUNUSED</span>
<a name="freeglut_std.h-622"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-623"></a><span class="k">static</span> <span class="kt">void</span> <span class="n">FGAPIENTRY</span> <span class="n">FGUNUSED</span> <span class="nf">glutInit_ATEXIT_HACK</span><span class="p">(</span><span class="kt">int</span> <span class="o">*</span><span class="n">argcp</span><span class="p">,</span> <span class="kt">char</span> <span class="o">**</span><span class="n">argv</span><span class="p">)</span> <span class="p">{</span> <span class="n">__glutInitWithExit</span><span class="p">(</span><span class="n">argcp</span><span class="p">,</span> <span class="n">argv</span><span class="p">,</span> <span class="n">exit</span><span class="p">);</span> <span class="p">}</span>
<a name="freeglut_std.h-624"></a><span class="cp">#define glutInit glutInit_ATEXIT_HACK</span>
<a name="freeglut_std.h-625"></a><span class="k">static</span> <span class="kt">int</span> <span class="n">FGAPIENTRY</span> <span class="n">FGUNUSED</span> <span class="nf">glutCreateWindow_ATEXIT_HACK</span><span class="p">(</span><span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">title</span><span class="p">)</span> <span class="p">{</span> <span class="k">return</span> <span class="n">__glutCreateWindowWithExit</span><span class="p">(</span><span class="n">title</span><span class="p">,</span> <span class="n">exit</span><span class="p">);</span> <span class="p">}</span>
<a name="freeglut_std.h-626"></a><span class="cp">#define glutCreateWindow glutCreateWindow_ATEXIT_HACK</span>
<a name="freeglut_std.h-627"></a><span class="k">static</span> <span class="kt">int</span> <span class="n">FGAPIENTRY</span> <span class="n">FGUNUSED</span> <span class="nf">glutCreateMenu_ATEXIT_HACK</span><span class="p">(</span><span class="kt">void</span> <span class="p">(</span><span class="o">*</span> <span class="n">func</span><span class="p">)(</span><span class="kt">int</span><span class="p">))</span> <span class="p">{</span> <span class="k">return</span> <span class="n">__glutCreateMenuWithExit</span><span class="p">(</span><span class="n">func</span><span class="p">,</span> <span class="n">exit</span><span class="p">);</span> <span class="p">}</span>
<a name="freeglut_std.h-628"></a><span class="cp">#define glutCreateMenu glutCreateMenu_ATEXIT_HACK</span>
<a name="freeglut_std.h-629"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-630"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-631"></a>
<a name="freeglut_std.h-632"></a><span class="cp">#ifdef __cplusplus</span>
<a name="freeglut_std.h-633"></a>    <span class="p">}</span>
<a name="freeglut_std.h-634"></a><span class="cp">#endif</span>
<a name="freeglut_std.h-635"></a>
<a name="freeglut_std.h-636"></a><span class="cm">/*** END OF FILE ***/</span>
<a name="freeglut_std.h-637"></a>
<a name="freeglut_std.h-638"></a><span class="cp">#endif </span><span class="cm">/* __FREEGLUT_STD_H__ */</span><span class="cp"></span>
</pre></div>
</td></tr></table>
    </div>
  


        </div>
        
      </div>
    </div>
  </div>
  
  <div data-module="source/set-changeset" data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"></div>



  
    
    
    
  
  

  </div>

        
        
        
          
    
    
  
        
      </div>
    </div>
    <div id="code-search-cta"></div>
  </div>

      </div>
    </div>
  
</div>

<div id="adg3-dialog"></div>


  

<div data-module="components/mentions/index">
  
    
    
  
  
    
    
  
  
    
    
  
</div>
<div data-module="components/typeahead/emoji/index">
  
    
    
  
</div>

<div data-module="components/repo-typeahead/index">
  
    
    
  
</div>

    
    
  

    
    
  

    
    
  

    
    
  


  


    
    
  

    
    
  


  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  


  
  
  <aui-inline-dialog
    id="help-menu-dialog"
    data-aui-alignment="bottom right"

    
    data-aui-alignment-static="true"
    data-module="header/help-menu"
    responds-to="toggle"
    aria-hidden="true">

  <div id="help-menu-section">
    <h1 class="help-menu-heading">Help</h1>

    <form id="help-menu-search-form" class="aui" target="_blank" method="get"
        action="https://support.atlassian.com/customer/search">
      <span id="help-menu-search-icon" class="aui-icon aui-icon-large aui-iconfont-search"></span>
      <input id="help-menu-search-form-input" name="q" class="text" type="text" placeholder="Ask a question">
    </form>

    <ul id="help-menu-links">
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationHome"
            href="https://confluence.atlassian.com/x/bgozDQ" target="_blank">
          Online help
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="GitTutorials"
            href="https://www.atlassian.com/git?utm_source=bitbucket&amp;utm_medium=link&amp;utm_campaign=help_dropdown&amp;utm_content=learn_git"
            target="_blank">
          Learn Git
        </a>
      </li>
      <li>
        <a id="keyboard-shortcuts-link"
           href="#">Keyboard shortcuts</a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationTutorials"
            href="https://confluence.atlassian.com/x/Q4sFLQ" target="_blank">
          Bitbucket tutorials
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="SiteStatus"
            href="https://status.bitbucket.org/" target="_blank">
          Site status
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="Home"
            href="https://support.atlassian.com/bitbucket-cloud/" target="_blank">
          Support
        </a>
      </li>
    </ul>
  </div>
</aui-inline-dialog>
  


  <div class="omnibar" data-module="components/omnibar/index">
    <form class="omnibar-form aui"></form>
  </div>
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  





  

  <div class="_mustache-templates">
    
      <script id="branch-checkout-template" type="text/html">
        

<div id="checkout-branch-contents">
  <div class="command-line">
    <p>
      Check out this branch on your local machine to begin working on it.
    </p>
    <input type="text" class="checkout-command" readonly="readonly"
        
           value="git fetch && git checkout [[branchName]]"
        
        >
  </div>
  
    <div class="sourcetree-callout clone-in-sourcetree"
  data-module="components/clone/clone-in-sourcetree"
  data-https-url="https://Yanosik@bitbucket.org/Yanosik/zapart.git"
  data-ssh-url="git@bitbucket.org:Yanosik/zapart.git">

  <div>
    <button class="aui-button aui-button-primary">
      
        Check out in Sourcetree
      
    </button>
  </div>

  <p class="windows-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_win" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Windows.
    
  </p>
  <p class="mac-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_mac" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Mac.
    
  </p>
</div>
  
</div>

      </script>
    
      <script id="branch-dialog-template" type="text/html">
        

<div class="tabbed-filter-widget branch-dialog">
  <div class="tabbed-filter">
    <input placeholder="Filter branches" class="filter-box" autosave="branch-dropdown-29632644" type="text">
    [[^ignoreTags]]
      <div class="aui-tabs horizontal-tabs aui-tabs-disabled filter-tabs">
        <ul class="tabs-menu">
          <li class="menu-item active-tab"><a href="#branches">Branches</a></li>
          <li class="menu-item"><a href="#tags">Tags</a></li>
        </ul>
      </div>
    [[/ignoreTags]]
  </div>
  
    <div class="tab-pane active-pane" id="branches" data-filter-placeholder="Filter branches">
      <ol class="filter-list">
        <li class="empty-msg">No matching branches</li>
        [[#branches]]
          
            [[#hasMultipleHeads]]
              [[#heads]]
                <li class="comprev filter-item">
                  <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/freeglut_std.h?at=[[safeName]]"
                     title="[[name]]">
                    [[name]] ([[shortChangeset]])
                  </a>
                </li>
              [[/heads]]
            [[/hasMultipleHeads]]
            [[^hasMultipleHeads]]
              <li class="comprev filter-item">
                <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/freeglut_std.h?at=[[safeName]]" title="[[name]]">
                  [[name]]
                </a>
              </li>
            [[/hasMultipleHeads]]
          
        [[/branches]]
      </ol>
    </div>
    <div class="tab-pane" id="tags" data-filter-placeholder="Filter tags">
      <ol class="filter-list">
        <li class="empty-msg">No matching tags</li>
        [[#tags]]
          <li class="comprev filter-item">
            <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/freeglut_std.h?at=[[safeName]]" title="[[name]]">
              [[name]]
            </a>
          </li>
        [[/tags]]
      </ol>
    </div>
  
</div>

      </script>
    
      <script id="image-upload-template" type="text/html">
        

<form id="upload-image" method="POST"
    
      action="/xhr/Yanosik/zapart/image-upload/"
    >
  <input type='hidden' name='csrfmiddlewaretoken' value='HdsICwDUqvG8eOLvXcUQ9CT1DwyBXJPFPwoiUg3deqzT5cSkzVDX5rOEFzwd7aLo' />

  <div class="drop-target">
    <p class="centered">Drag image here</p>
  </div>

  
  <div>
    <button class="aui-button click-target">Select an image</button>
    <input name="file" type="file" class="hidden file-target"
           accept="image/jpeg, image/gif, image/png" />
    <input type="submit" class="hidden">
  </div>
</form>


      </script>
    
      <script id="mention-result" type="text/html">
        
<span class="mention-result">
  <span class="aui-avatar aui-avatar-small mention-result--avatar">
    <span class="aui-avatar-inner">
      <img src="[[avatar_url]]">
    </span>
  </span>
  [[#display_name]]
    <span class="display-name mention-result--display-name">[[&display_name]]</span> <small class="username mention-result--username">[[&username]]</small>
  [[/display_name]]
  [[^display_name]]
    <span class="username mention-result--username">[[&username]]</span>
  [[/display_name]]
  [[#is_teammate]][[^is_team]]
    <span class="aui-lozenge aui-lozenge-complete aui-lozenge-subtle mention-result--lozenge">teammate</span>
  [[/is_team]][[/is_teammate]]
</span>
      </script>
    
      <script id="mention-call-to-action" type="text/html">
        
[[^query]]
<li class="bb-typeahead-item">Begin typing to search for a user</li>
[[/query]]
[[#query]]
<li class="bb-typeahead-item">Continue typing to search for a user</li>
[[/query]]

      </script>
    
      <script id="mention-no-results" type="text/html">
        
[[^searching]]
<li class="bb-typeahead-item">Found no matching users for <em>[[query]]</em>.</li>
[[/searching]]
[[#searching]]
<li class="bb-typeahead-item bb-typeahead-searching">Searching for <em>[[query]]</em>.</li>
[[/searching]]

      </script>
    
      <script id="emoji-result" type="text/html">
        
<span class="emoji-result">
  <span class="emoji-result--avatar">
    <img class="emoji" src="[[src]]">
  </span>
  <span class="name emoji-result--name">[[&name]]</span>
</span>

      </script>
    
      <script id="repo-typeahead-result" type="text/html">
        <span class="aui-avatar aui-avatar-project aui-avatar-xsmall">
  <span class="aui-avatar-inner">
    <img src="[[avatar]]">
  </span>
</span>
<span class="owner">[[&owner]]</span>/<span class="slug">[[&slug]]</span>

      </script>
    
      <script id="share-form-template" type="text/html">
        

<div class="error aui-message hidden">
  <span class="aui-icon icon-error"></span>
  <div class="message"></div>
</div>
<form class="aui">
  <table class="widget bb-list aui">
    <thead>
    <tr class="assistive">
      <th class="user">User</th>
      <th class="role">Role</th>
      <th class="actions">Actions</th>
    </tr>
    </thead>
    <tbody>
      <tr class="form">
        <td colspan="2">
          <input type="text" class="text bb-user-typeahead user-or-email"
                 placeholder="Username or email address"
                 autocomplete="off"
                 data-bb-typeahead-focus="false"
                 [[#disabled]]disabled[[/disabled]]>
        </td>
        <td class="actions">
          <button type="submit" class="aui-button aui-button-light" disabled>Add</button>
        </td>
      </tr>
    </tbody>
  </table>
</form>

      </script>
    
      <script id="share-detail-template" type="text/html">
        

[[#username]]
<td class="user
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[displayName]]">
    <a href="/[[username]]/" class="user">
      <span class="aui-avatar aui-avatar-xsmall">
        <span class="aui-avatar-inner">
          <img src="[[avatar]]">
        </span>
      </span>
      <span>[[displayName]]</span>
    </a>
  </div>
</td>
[[/username]]
[[^username]]
<td class="email
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[email]]">
    <span class="aui-icon aui-icon-small aui-iconfont-email"></span>
    [[email]]
  </div>
</td>
[[/username]]
<td class="role
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    [[#group]]
      [[#hasCustomGroups]]
        <select class="group [[#noGroupChoices]]hidden[[/noGroupChoices]]">
          [[#groups]]
            <option value="[[slug]]"
                [[#isSelected]]selected[[/isSelected]]>
              [[name]]
            </option>
          [[/groups]]
        </select>
      [[/hasCustomGroups]]
      [[^hasCustomGroups]]
      <label>
        <input type="checkbox" class="admin"
            [[#isAdmin]]checked[[/isAdmin]]>
        Administrator
      </label>
      [[/hasCustomGroups]]
    [[/group]]
    [[^group]]
      <ul>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^read]]aui-lozenge-subtle[[/read]]"
            data-permission="read">
          read
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^write]]aui-lozenge-subtle[[/write]]"
            data-permission="write">
          write
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^admin]]aui-lozenge-subtle[[/admin]]"
            data-permission="admin">
          admin
        </li>
      </ul>
    [[/group]]
  </div>
</td>
<td class="actions
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    <a href="#" class="delete">
      <span class="aui-icon aui-icon-small aui-iconfont-remove">Delete</span>
    </a>
  </div>
</td>

      </script>
    
      <script id="share-team-template" type="text/html">
        

<div class="clearfix">
  <span class="team-avatar-container">
    <span class="aui-avatar aui-avatar-medium">
      <span class="aui-avatar-inner">
        <img src="[[avatar]]">
      </span>
    </span>
  </span>
  <span class="team-name-container">
    [[display_name]]
  </span>
</div>
<p class="helptext">
  
    Existing users are granted access to this team immediately.
    New users will be sent an invitation.
  
</p>
<div class="share-form"></div>

      </script>
    
      <script id="scope-list-template" type="text/html">
        <ul class="scope-list">
  [[#scopes]]
    <li class="scope-list--item">
      <span class="scope-list--icon aui-icon aui-icon-small [[icon]]"></span>
      <span class="scope-list--description">[[description]]</span>
    </li>
  [[/scopes]]
</ul>

      </script>
    
      <script id="source-changeset" type="text/html">
        

<a href="/Yanosik/zapart/src/[[raw_node]]/[[path]]?at=master"
    class="[[#selected]]highlight[[/selected]]"
    data-hash="[[node]]">
  [[#author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="[[author.avatar]]">
      </span>
    </span>
    <span class="author" title="[[raw_author]]">[[author.display_name]]</span>
  [[/author.username]]
  [[^author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg">
      </span>
    </span>
    <span class="author unmapped" title="[[raw_author]]">[[author]]</span>
  [[/author.username]]
  <time datetime="[[utctimestamp]]" data-title="true">[[utctimestamp]]</time>
  <span class="message">[[message]]</span>
</a>

      </script>
    
      <script id="embed-template" type="text/html">
        

<form class="aui inline-dialog-embed-dialog">
  <label for="embed-code-[[dialogId]]">Embed this source in another page:</label>
  <input type="text" readonly="true" value="&lt;script src=&quot;[[url]]&quot;&gt;&lt;/script&gt;" id="embed-code-[[dialogId]]" class="embed-code">
</form>

      </script>
    
      <script id="edit-form-template" type="text/html">
        


<form class="bb-content-container online-edit-form aui"
      data-repository="[[owner]]/[[slug]]"
      data-destination-repository="[[destinationOwner]]/[[destinationSlug]]"
      data-local-id="[[localID]]"
      [[#isWriter]]data-is-writer="true"[[/isWriter]]
      [[#hasPushAccess]]data-has-push-access="true"[[/hasPushAccess]]
      [[#isPullRequest]]data-is-pull-request="true"[[/isPullRequest]]
      [[#hideCreatePullRequest]]data-hide-create-pull-request="true"[[/hideCreatePullRequest]]
      data-hash="[[hash]]"
      data-branch="[[branch]]"
      data-path="[[path]]"
      data-is-create="[[isCreate]]"
      data-preview-url="/xhr/[[owner]]/[[slug]]/preview/[[hash]]/[[encodedPath]]"
      data-preview-error="We had trouble generating your preview."
      data-unsaved-changes-error="Your changes will be lost. Are you sure you want to leave?">
  <div class="bb-content-container-header">
    <div class="bb-content-container-header-primary">
      <span class="bb-content-container-heading">
        [[#isCreate]]
          [[#branch]]
            
              Creating <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
            
          [[/branch]]
          [[^branch]]
            [[#path]]
              
                Creating <span class="edit-path">[[path]]</span>
              
            [[/path]]
            [[^path]]
              
                Creating <span class="edit-path">unnamed file</span>
              
            [[/path]]
          [[/branch]]
        [[/isCreate]]
        [[^isCreate]]
          
            Editing <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
          
        [[/isCreate]]
      </span>
    </div>
    <div class="bb-content-container-header-secondary">
      <div class="hunk-nav aui-buttons">
        <button class="prev-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Previous change">
          <span class="aui-icon aui-icon-small aui-iconfont-up">Previous change</span>
        </button>
        <button class="next-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Next change">
          <span class="aui-icon aui-icon-small aui-iconfont-down">Next change</span>
        </button>
      </div>
    </div>
  </div>
  <div class="bb-content-container-body has-header has-footer file-editor">
    <textarea id="id_source"></textarea>
  </div>
  <div class="preview-pane"></div>
  <div class="bb-content-container-footer">
    <div class="bb-content-container-footer-primary">
      <div id="syntax-mode" class="bb-content-container-item field">
        <label for="id_syntax-mode" class="online-edit-form--label">Syntax mode:</label>
        <select id="id_syntax-mode">
          [[#syntaxes]]
            <option value="[[#mime]][[mime]][[/mime]][[^mime]][[mode]][[/mime]]">[[name]]</option>
          [[/syntaxes]]
        </select>
      </div>
      <div id="indent-mode" class="bb-content-container-item field">
        <label for="id_indent-mode" class="online-edit-form--label">Indent mode:</label>
        <select id="id_indent-mode">
          <option value="tabs">Tabs</option>
          <option value="spaces">Spaces</option>
        </select>
      </div>
      <div id="indent-size" class="bb-content-container-item field">
        <label for="id_indent-size" class="online-edit-form--label">Indent size:</label>
        <select id="id_indent-size">
          <option value="2">2</option>
          <option value="4">4</option>
          <option value="8">8</option>
        </select>
      </div>
      <div id="wrap-mode" class="bb-content-container-item field">
        <label for="id_wrap-mode" class="online-edit-form--label">Line wrap:</label>
        <select id="id_wrap-mode">
          <option value="">Off</option>
          <option value="soft">On</option>
        </select>
      </div>
    </div>
    <div class="bb-content-container-footer-secondary">
      [[^isCreate]]
        <button class="preview-button aui-button aui-button-light"
                disabled="disabled" aria-disabled="true"
                data-preview-label="View diff"
                data-edit-label="Edit file">View diff</button>
      [[/isCreate]]
      <button class="save-button aui-button aui-button-primary"
              disabled="disabled" aria-disabled="true">Commit</button>
      [[^isCreate]]
        <a class="aui-button aui-button-link cancel-link" href="#">Cancel</a>
      [[/isCreate]]
    </div>
  </div>
</form>

      </script>
    
      <script id="commit-form-template" type="text/html">
        

<form class="aui commit-form"
      data-title="Commit changes"
      [[#isDelete]]
        data-default-message="[[filename]] deleted online with Bitbucket"
      [[/isDelete]]
      [[#isCreate]]
        data-default-message="[[filename]] created online with Bitbucket"
      [[/isCreate]]
      [[^isDelete]]
        [[^isCreate]]
          data-default-message="[[filename]] edited online with Bitbucket"
        [[/isCreate]]
      [[/isDelete]]
      data-fork-error="We had trouble creating your fork."
      data-commit-error="We had trouble committing your changes."
      data-pull-request-error="We had trouble creating your pull request."
      data-update-error="We had trouble updating your pull request."
      data-branch-conflict-error="A branch with that name already exists."
      data-forking-message="Forking repository"
      data-committing-message="Committing changes"
      data-merging-message="Branching and merging changes"
      data-creating-pr-message="Creating pull request"
      data-updating-pr-message="Updating pull request"
      data-cta-label="Commit"
      data-cancel-label="Cancel">
  [[#isDelete]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <span class="message">
        
          Committing this change will delete [[filename]] from your repository.
        
      </span>
    </div>
  [[/isDelete]]
  <div class="aui-message error hidden">
    <span class="aui-icon icon-error"></span>
    <span class="message"></span>
  </div>
  [[^isWriter]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <p class="title">
        
          You don't have write access to this repository.
        
      </p>
      <span class="message">
        
          We'll create a fork for your changes and submit a
          pull request back to this repository.
        
      </span>
    </div>
  [[/isWriter]]
  [[#isRename]]
    <div class="field-group">
      <label for="id_path">New path</label>
      <input type="text" id="id_path" class="text" value="[[path]]"/>
    </div>
  [[/isRename]]
  <div class="field-group">
    <label for="id_message">Commit message</label>
    <textarea id="id_message" class="long-field textarea"></textarea>
  </div>
  [[^isPullRequest]]
    [[#isWriter]]
      <fieldset class="group">
        <div class="checkbox">
          [[#hasPushAccess]]
            [[^hideCreatePullRequest]]
              <input id="id_create-pullrequest" class="checkbox" type="checkbox">
              <label for="id_create-pullrequest">Create a pull request for this change</label>
            [[/hideCreatePullRequest]]
          [[/hasPushAccess]]
          [[^hasPushAccess]]
            <input id="id_create-pullrequest" class="checkbox" type="checkbox" checked="checked" aria-disabled="true" disabled="true">
            <label for="id_create-pullrequest" title="Branch restrictions do not allow you to update this branch.">Create a pull request for this change</label>
          [[/hasPushAccess]]
        </div>
      </fieldset>
      <div id="pr-fields">
        <div id="branch-name-group" class="field-group">
          <label for="id_branch-name">Branch name</label>
          <input type="text" id="id_branch-name" class="text long-field">
        </div>
        

<div class="field-group" id="id_reviewers_group">
  <label for="reviewers">Reviewers</label>

  
  <input id="reviewers" name="reviewers" type="hidden"
          value=""
          data-mention-url="/xhr/mentions/repositories/:dest_username/:dest_slug"
          data-reviewers="[]"
          data-suggested="[]"
          data-locked="[]">

  <div class="error"></div>
  <div class="suggested-reviewers"></div>

</div>

      </div>
    [[/isWriter]]
  [[/isPullRequest]]
  <button type="submit" id="id_submit">Commit</button>
</form>

      </script>
    
      <script id="merge-message-template" type="text/html">
        Merged [[hash]] into [[branch]]

[[message]]

      </script>
    
      <script id="commit-merge-error-template" type="text/html">
        



  We had trouble merging your changes. We stored them on the <strong>[[branch]]</strong> branch, so feel free to
  <a href="/[[owner]]/[[slug]]/full-commit/[[hash]]/[[path]]?at=[[encodedBranch]]">view them</a> or
  <a href="#" class="create-pull-request-link">create a pull request</a>.


      </script>
    
      <script id="selected-reviewer-template" type="text/html">
        <div class="aui-avatar aui-avatar-xsmall">
  <div class="aui-avatar-inner">
    <img src="[[avatar_url]]">
  </div>
</div>
[[display_name]]

      </script>
    
      <script id="suggested-reviewer-template" type="text/html">
        <button class="aui-button aui-button-link" type="button" tabindex="-1">[[display_name]]</button>

      </script>
    
      <script id="suggested-reviewers-template" type="text/html">
        

<span class="suggested-reviewer-list-label">Recent:</span>
<ul class="suggested-reviewer-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-form-template" type="text/html">
        <div class="omnibar-input-container">
  <input class="omnibar-input" type="text" [[#placeholder]]placeholder="[[placeholder]]"[[/placeholder]]>
</div>
<ul class="omnibar-result-group-list"></ul>

      </script>
    
      <script id="omnibar-blank-slate-template" type="text/html">
        

<div class="omnibar-blank-slate">No results found</div>

      </script>
    
      <script id="omnibar-result-group-list-item-template" type="text/html">
        <div class="omnibar-result-group-header clearfix">
  <h2 class="omnibar-result-group-label" title="[[label]]">[[label]]</h2>
  <span class="omnibar-result-group-context" title="[[context]]">[[context]]</span>
</div>
<ul class="omnibar-result-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-result-list-item-template" type="text/html">
        [[#url]]
  <a href="[[&url]]" class="omnibar-result-label">[[&label]]</a>
[[/url]]
[[^url]]
  <span class="omnibar-result-label">[[&label]]</span>
[[/url]]
[[#context]]
  <span class="omnibar-result-context">[[context]]</span>
[[/context]]

      </script>
    
  </div>




  
  


<script nonce="dIoy0WyKpxyqa7fi">
  window.__initial_state__ = {"global": {"features": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "locale": "en", "geoip_country": "PL", "targetFeatures": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "isFocusedTask": false, "teams": [], "bitbucketActions": [{"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 100, "url": "/repo/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Repository<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repository-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 110, "url": "/account/create-team/", "tab_name": null, "can_display": true, "label": "<strong>Team<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "team-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 120, "url": "/account/projects/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Project<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "project-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 130, "url": "/snippets/new?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Snippet<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "snippet-create-drawer-item", "icon": ""}], "targetUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "isNavigationOpen": true, "path": "/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/freeglut_std.h", "focusedTaskBackButtonUrl": "https://bitbucket.org/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/?at=master", "currentUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}}, "connect": {}, "repository": {"section": {"connectActions": [], "cloneProtocol": "https", "currentRepository": {"scm": "git", "website": "", "name": "Zapart", "language": "", "links": {"clone": [{"href": "https://Yanosik@bitbucket.org/Yanosik/zapart.git", "name": "https"}, {"href": "git@bitbucket.org:Yanosik/zapart.git", "name": "ssh"}], "self": {"href": "https://bitbucket.org/!api/2.0/repositories/Yanosik/zapart"}, "html": {"href": "https://bitbucket.org/Yanosik/zapart"}, "avatar": {"href": "https://bitbucket.org/Yanosik/zapart/avatar/32/"}}, "full_name": "Yanosik/zapart", "owner": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "type": "repository", "slug": "zapart", "is_private": true, "uuid": "{8f59423b-9494-4138-af6f-56e97d727ae4}"}, "menuItems": [{"analytics_label": "repository.overview", "icon_class": "icon-overview", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/overview", "tab_name": "overview", "can_display": true, "label": "Overview", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-overview-link", "icon": "icon-overview"}, {"analytics_label": "repository.source", "icon_class": "icon-source", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/src", "tab_name": "source", "can_display": true, "label": "Source", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-source-link", "icon": "icon-source"}, {"analytics_label": "repository.commits", "icon_class": "icon-commits", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/commits/", "tab_name": "commits", "can_display": true, "label": "Commits", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-commits-link", "icon": "icon-commits"}, {"analytics_label": "repository.branches", "icon_class": "icon-branches", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/", "tab_name": "branches", "can_display": true, "label": "Branches", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-branches-link", "icon": "icon-branches"}, {"analytics_label": "repository.pullrequests", "icon_class": "icon-pull-requests", "badge_label": "0 open pull requests", "weight": 500, "url": "/Yanosik/zapart/pull-requests/", "tab_name": "pullrequests", "can_display": true, "label": "Pull requests", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-pullrequests-link", "icon": "icon-pull-requests"}, {"analytics_label": "site.addon", "icon_class": "aui-iconfont-build", "badge_label": null, "weight": 550, "url": "/Yanosik/zapart/addon/pipelines-installer/home", "tab_name": "repopage-kbbqoq-add-on-link", "can_display": true, "label": "Pipelines", "anchor": true, "analytics_payload": {}, "icon_url": null, "type": "connect_menu_item", "id": "repopage-kbbqoq-add-on-link", "target": "_self"}, {"analytics_label": "repository.downloads", "icon_class": "icon-downloads", "badge_label": null, "weight": 800, "url": "/Yanosik/zapart/downloads/", "tab_name": "downloads", "can_display": true, "label": "Downloads", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-downloads-link", "icon": "icon-downloads"}, {"analytics_label": "user.addon", "icon_class": "aui-iconfont-unfocus", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/addon/bitbucket-trello-addon/trello-board", "tab_name": "repopage-5ABRne-add-on-link", "can_display": true, "label": "Boards", "anchor": false, "analytics_payload": {}, "icon_url": "https://bitbucket-assetroot.s3.amazonaws.com/add-on/icons/35ceae0c-17b1-443c-a6e8-d9de1d7cccdb.svg?Signature=weyND%2BQOC0WTawG4O810gCLBeok%3D&Expires=1511782799&AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&versionId=3oqdrZZjT.HijZ3kHTPsXE6IcSjhCG.P", "type": "connect_menu_item", "id": "repopage-5ABRne-add-on-link", "target": "_self"}, {"analytics_label": "repository.settings", "icon_class": "icon-settings", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/admin", "tab_name": "admin", "can_display": true, "label": "Settings", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-settings-link", "icon": "icon-settings"}], "bitbucketActions": [{"analytics_label": "repository.clone", "icon_class": "icon-clone", "badge_label": null, "weight": 100, "url": "#clone", "tab_name": "clone", "can_display": true, "label": "<strong>Clone<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-clone-button", "icon": "icon-clone"}, {"analytics_label": "repository.create_branch", "icon_class": "icon-create-branch", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/branch", "tab_name": "create-branch", "can_display": true, "label": "Create a <strong>branch<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-branch-link", "icon": "icon-create-branch"}, {"analytics_label": "create_pullrequest", "icon_class": "icon-create-pull-request", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/pull-requests/new", "tab_name": "create-pullreqs", "can_display": true, "label": "Create a <strong>pull request<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-pull-request-link", "icon": "icon-create-pull-request"}, {"analytics_label": "repository.compare", "icon_class": "aui-icon-small aui-iconfont-devtools-compare", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/compare", "tab_name": "compare", "can_display": true, "label": "<strong>Compare<\/strong> branches or tags", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-compare-link", "icon": "aui-icon-small aui-iconfont-devtools-compare"}, {"analytics_label": "repository.fork", "icon_class": "icon-fork", "badge_label": null, "weight": 500, "url": "/Yanosik/zapart/fork", "tab_name": "fork", "can_display": true, "label": "<strong>Fork<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-fork-link", "icon": "icon-fork"}], "activeMenuItem": "source"}}};
  window.__settings__ = {"SOCIAL_AUTH_ATLASSIANID_LOGOUT_URL": "https://id.atlassian.com/logout", "CANON_URL": "https://bitbucket.org", "API_CANON_URL": "https://api.bitbucket.org"};
</script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/jsi18n/en/djangojs.js" nonce="dIoy0WyKpxyqa7fi"></script>

  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/locales/en.js"></script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/vendor.js" nonce="dIoy0WyKpxyqa7fi"></script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/app.js" nonce="dIoy0WyKpxyqa7fi"></script>


<script async src="https://www.google-analytics.com/analytics.js" nonce="dIoy0WyKpxyqa7fi"></script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"licenseKey":"a2cef8c3d3","agent":"","transactionName":"Z11RZxdWW0cEVkYLDV4XdUYLVEFdClsdAAtEWkZQDlJBGgRFQhFMQl1DXFcZQ10AQkFYBFlUVlEXWEJHAA==","applicationID":"1841284","errorBeacon":"bam.nr-data.net","applicationTime":467}</script>
</body>
</html>