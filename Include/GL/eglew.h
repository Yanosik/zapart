<!DOCTYPE html>
<html lang="en">
<head>
  <meta id="bb-bootstrap" data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="5a097ca786e7c03ff34850dd" />
  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta charset="utf-8">
  <title>
  Yanosik / Zapart 
  / source  / Include / GL / eglew.h
 &mdash; Bitbucket
</title>
  <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
  


<meta id="bb-canon-url" name="bb-canon-url" content="https://bitbucket.org">
<meta name="bb-api-canon-url" content="https://api.bitbucket.org">
<meta name="apitoken" content="{&quot;token&quot;: &quot;FNu4147CXy01fGVR7DKVIhthLc0t2fsdp2EPUDmZbNRQ967aKHhgbj8FJaEglBjZqnH5zO_N069PuQ3Bg3yfeXjSkkX9Pgv-9BoPaDeo-2fvFaVX&quot;, &quot;expiration&quot;: 1511781276.133977}">

<meta name="bb-commit-hash" content="370568ebc84f">
<meta name="bb-app-node" content="app-164">
<meta name="bb-view-name" content="bitbucket.apps.repo2.views.filebrowse">
<meta name="ignore-whitespace" content="False">
<meta name="tab-size" content="None">
<meta name="locale" content="en">

<meta name="application-name" content="Bitbucket">
<meta name="apple-mobile-web-app-title" content="Bitbucket">


<meta name="theme-color" content="#0049B0">
<meta name="msapplication-TileColor" content="#0052CC">
<meta name="msapplication-TileImage" content="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/mstile-150x150.png">
<link rel="apple-touch-icon" sizes="180x180" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/apple-touch-icon.png">
<link rel="icon" sizes="192x192" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/android-chrome-192x192.png">

<link rel="icon" sizes="16x16 24x24 32x32 64x64" type="image/x-icon" href="/favicon.ico?v=2">
<link rel="mask-icon" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/safari-pinned-tab.svg" color="#0052CC">

<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Bitbucket">

  <meta name="description" content="">
  
  
    
  



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/vendor.css" />
<link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/app.css" />



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/adg3.css">
  
  <script nonce="QT0q4c6MTmDGzd3X">
  window.__sentry__ = {"dsn": "https://ea49358f525d4019945839a3d7a8292a@sentry.io/159509", "release": "370568ebc84f (production)", "tags": {"project": "bitbucket-core"}, "environment": "production"};
</script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/sentry.js" nonce="QT0q4c6MTmDGzd3X"></script>
  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/early.js" nonce="QT0q4c6MTmDGzd3X"></script>
  
  
    <link href="/Yanosik/zapart/rss?token=a07b0067199174cef445f1e8fa79b35f" rel="alternate nofollow" type="application/rss+xml" title="RSS feed for Zapart" />

</head>
<body class="production adg3  "
    data-static-url="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/"
data-base-url="https://bitbucket.org"
data-no-avatar-image="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg"
data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="{&quot;loginStatusUrl&quot;: &quot;https://id.atlassian.com/profile/rest/profile&quot;}"
data-settings="{&quot;MENTIONS_MIN_QUERY_LENGTH&quot;: 3}"

data-current-repo="{&quot;scm&quot;: &quot;git&quot;, &quot;readOnly&quot;: false, &quot;mainbranch&quot;: {&quot;name&quot;: &quot;master&quot;}, &quot;uuid&quot;: &quot;8f59423b-9494-4138-af6f-56e97d727ae4&quot;, &quot;language&quot;: &quot;&quot;, &quot;owner&quot;: {&quot;username&quot;: &quot;Yanosik&quot;, &quot;uuid&quot;: &quot;8aa0a974-0d88-41f1-b96a-c1ebd6c01af9&quot;, &quot;isTeam&quot;: false}, &quot;fullslug&quot;: &quot;Yanosik/zapart&quot;, &quot;slug&quot;: &quot;zapart&quot;, &quot;id&quot;: 29632644, &quot;pygmentsLanguage&quot;: null}"
data-current-cset="f71033cf7729d442b43ee6f67d340a04470f80ae"





data-browser-monitoring="true"
data-switch-create-pullrequest-commit-status="true"




>
<div id="page">
  
    <div id="adg3-navigation"
  
  
  >
  <nav class="skeleton-nav">
    <div class="skeleton-nav--left">
      <div class="skeleton-nav--left-top">
        <ul class="skeleton-nav--items">
          <li></li>
          <li></li>
          <li></li>
          <li class="skeleton--icon"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
        </ul>
      </div>
      <div class="skeleton-nav--left-bottom">
        <div class="skeleton-nav--left-bottom-wrapper">
          <div class="skeleton-nav--item-help"></div>
          <div class="skeleton-nav--item-profile"></div>
        </div>
      </div>
    </div>
    <div class="skeleton-nav--right">
      <ul class="skeleton-nav--items-wide">
        <li class="skeleton--icon-logo-container">
          <div class="skeleton--icon-container"></div>
          <div class="skeleton--icon-description"></div>
          <div class="skeleton--icon-logo"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
      </ul>
    </div>
  </nav>
</div>

    <div id="wrapper">
      
  


      
  <div id="nps-survey-container"></div>

 

      
  

<div id="account-warning" data-module="header/account-warning"
  data-unconfirmed-addresses="false"
  data-no-addresses="false"
  
></div>



      
  
<header id="aui-message-bar">
  
</header>


      <div id="content" role="main">

        
          <header class="app-header">
            <div class="app-header--primary">
              
                <div class="app-header--context">
                  <div class="app-header--breadcrumbs">
                    
  <ol class="aui-nav aui-nav-breadcrumbs">
    <li>
  <a href="/Yanosik/">Jan</a>
</li>

<li>
  <a href="/Yanosik/zapart">Zapart</a>
</li>
    
  <li>
    <a href="/Yanosik/zapart/src">
      Source
    </a>
  </li>

  </ol>

                  </div>
                  <h1 class="app-header--heading">
                    <span class="file-path">eglew.h</span>
                  </h1>
                </div>
              
            </div>

            <div class="app-header--secondary">
              
                
              
            </div>
          </header>
        

        
        
  <div class="aui-page-panel ">
    <div class="hidden">
  
  </div>
    <div class="aui-page-panel-inner">

      <div
        id="repo-content"
        class="aui-page-panel-content forks-enabled can-create"
        data-module="repo/index"
        
      >
        
        
  <div id="source-container" class="maskable" data-module="repo/source/index">
    



<header id="source-path">
  
    <div class="labels labels-csv">
      <div class="aui-buttons">
        <button data-branches-tags-url="/api/1.0/repositories/Yanosik/zapart/branches-tags"
                data-module="components/branch-dialog"
                
                class="aui-button branch-dialog-trigger" title="master">
          
            
              <span class="aui-icon aui-icon-small aui-iconfont-devtools-branch">Branch</span>
            
            <span class="name">master</span>
          
          <span class="aui-icon-dropdown"></span>
        </button>
        <button class="aui-button" id="checkout-branch-button"
                title="Check out this branch">
          <span class="aui-icon aui-icon-small aui-iconfont-devtools-clone">Check out branch</span>
          <span class="aui-icon-dropdown"></span>
        </button>
      </div>
      
    
    
  
    </div>
  
  
    <div class="secondary-actions">
      <div class="aui-buttons">
        
          <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/eglew.h?at=master"
            class="aui-button pjax-trigger source-toggle" aria-pressed="true">
            Source
          </a>
          <a href="/Yanosik/zapart/diff/Include/GL/eglew.h?diff2=f71033cf7729&at=master"
            class="aui-button pjax-trigger diff-toggle"
            title="Diff to previous change">
            Diff
          </a>
          <a href="/Yanosik/zapart/history-node/f71033cf7729/Include/GL/eglew.h?at=master"
            class="aui-button pjax-trigger history-toggle">
            History
          </a>
        
      </div>

      
        
        
      

    </div>
  
  <h1>
    
      
        <a href="/Yanosik/zapart/src/f71033cf7729?at=master"
          class="pjax-trigger root" title="Yanosik/zapart at f71033cf7729">Zapart</a> /
      
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/?at=master"
              class="pjax-trigger directory-name">Include</a> /
          
        
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/?at=master"
              class="pjax-trigger directory-name">GL</a> /
          
        
      
        
          
            <span class="file-name">eglew.h</span>
          
        
      
    
  </h1>
  
    
    
  
  <div class="clearfix"></div>
</header>


  
    
  

  <div id="editor-container" class="maskable"
       data-module="repo/source/editor"
       data-owner="Yanosik"
       data-slug="zapart"
       data-is-writer="true"
       data-has-push-access="true"
       data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"
       data-branch="master"
       data-path="Include/GL/eglew.h"
       data-source-url="/api/internal/repositories/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/eglew.h">
    <div id="source-view" class="file-source-container" data-module="repo/source/view-file" data-is-lfs="false">
      <div class="toolbar">
        <div class="primary">
          <div class="aui-buttons">
            
              <button id="file-history-trigger" class="aui-button aui-button-light changeset-info"
                      data-changeset="f71033cf7729d442b43ee6f67d340a04470f80ae"
                      data-path="Include/GL/eglew.h"
                      data-current="f71033cf7729d442b43ee6f67d340a04470f80ae">
                 

  <div class="aui-avatar aui-avatar-xsmall">
    <div class="aui-avatar-inner">
      <img src="https://bitbucket.org/account/Yanosik/avatar/16/?ts=1511779772">
    </div>
  </div>
  <span class="changeset-hash">f71033c</span>
  <time datetime="2017-11-13T12:06:29+00:00" class="timestamp"></time>
  <span class="aui-icon-dropdown"></span>

              </button>
            
          </div>
          
          <a href="/Yanosik/zapart/full-commit/f71033cf7729/Include/GL/eglew.h" id="full-commit-link"
             title="View full commit f71033c">Full commit</a>
        </div>
        <div class="secondary">
          
          <div class="aui-buttons">
            
              <a href="/Yanosik/zapart/annotate/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/eglew.h?at=master"
                 class="aui-button aui-button-light pjax-trigger blame-link">Blame</a>
              
            
            <a href="/Yanosik/zapart/raw/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/eglew.h" class="aui-button aui-button-light raw-link">Raw</a>
          </div>
          
            

            <div class="aui-buttons">
              
              <button id="file-edit-button" class="edit-button aui-button aui-button-light aui-button-split-main"
                  

                  >
                Edit
                
              </button>
              <button id="file-more-actions-button" class="aui-button aui-button-light aui-dropdown2-trigger aui-button-split-more" aria-owns="split-container-dropdown" aria-haspopup="true"
                  >
                More file actions
              </button>
            </div>
            <div id="split-container-dropdown" class="aui-dropdown2 aui-style-default" data-container="#editor-container">
              <ul class="aui-list-truncate">
                <li><a href="#" data-module="repo/source/rename-file" class="rename-link">Rename</a></li>
                <li><a href="#" data-module="repo/source/delete-file" class="delete-link">Delete</a></li>
              </ul>
            </div>
          
        </div>

        <div id="fileview-dropdown"
            class="aui-dropdown2 aui-style-default"
            data-fileview-container="#fileview-container"
            
            
            data-fileview-button="#fileview-trigger"
            data-module="connect/fileview">
          <div class="aui-dropdown2-section">
            <ul>
              <li>
                <a class="aui-dropdown2-radio aui-dropdown2-checked"
                    data-fileview-id="-1"
                    data-fileview-loaded="true"
                    data-fileview-target="#fileview-original"
                    data-fileview-connection-key=""
                    data-fileview-module-key="file-view-default">
                  Default File Viewer
                </a>
              </li>
              
            </ul>
          </div>
        </div>

        <div class="clearfix"></div>
      </div>
      <div id="fileview-container">
        <div id="fileview-original"
            class="fileview"
            data-module="repo/source/highlight-lines"
            data-fileview-loaded="true">
          


  
    <div class="file-source">
      <table class="highlighttable">
<tr><td class="linenos"><div class="linenodiv"><pre>
<a href="#eglew.h-1">1</a>
<a href="#eglew.h-2">2</a>
<a href="#eglew.h-3">3</a>
<a href="#eglew.h-4">4</a>
<a href="#eglew.h-5">5</a>
<a href="#eglew.h-6">6</a>
<a href="#eglew.h-7">7</a>
<a href="#eglew.h-8">8</a>
<a href="#eglew.h-9">9</a>
<a href="#eglew.h-10">10</a>
<a href="#eglew.h-11">11</a>
<a href="#eglew.h-12">12</a>
<a href="#eglew.h-13">13</a>
<a href="#eglew.h-14">14</a>
<a href="#eglew.h-15">15</a>
<a href="#eglew.h-16">16</a>
<a href="#eglew.h-17">17</a>
<a href="#eglew.h-18">18</a>
<a href="#eglew.h-19">19</a>
<a href="#eglew.h-20">20</a>
<a href="#eglew.h-21">21</a>
<a href="#eglew.h-22">22</a>
<a href="#eglew.h-23">23</a>
<a href="#eglew.h-24">24</a>
<a href="#eglew.h-25">25</a>
<a href="#eglew.h-26">26</a>
<a href="#eglew.h-27">27</a>
<a href="#eglew.h-28">28</a>
<a href="#eglew.h-29">29</a>
<a href="#eglew.h-30">30</a>
<a href="#eglew.h-31">31</a>
<a href="#eglew.h-32">32</a>
<a href="#eglew.h-33">33</a>
<a href="#eglew.h-34">34</a>
<a href="#eglew.h-35">35</a>
<a href="#eglew.h-36">36</a>
<a href="#eglew.h-37">37</a>
<a href="#eglew.h-38">38</a>
<a href="#eglew.h-39">39</a>
<a href="#eglew.h-40">40</a>
<a href="#eglew.h-41">41</a>
<a href="#eglew.h-42">42</a>
<a href="#eglew.h-43">43</a>
<a href="#eglew.h-44">44</a>
<a href="#eglew.h-45">45</a>
<a href="#eglew.h-46">46</a>
<a href="#eglew.h-47">47</a>
<a href="#eglew.h-48">48</a>
<a href="#eglew.h-49">49</a>
<a href="#eglew.h-50">50</a>
<a href="#eglew.h-51">51</a>
<a href="#eglew.h-52">52</a>
<a href="#eglew.h-53">53</a>
<a href="#eglew.h-54">54</a>
<a href="#eglew.h-55">55</a>
<a href="#eglew.h-56">56</a>
<a href="#eglew.h-57">57</a>
<a href="#eglew.h-58">58</a>
<a href="#eglew.h-59">59</a>
<a href="#eglew.h-60">60</a>
<a href="#eglew.h-61">61</a>
<a href="#eglew.h-62">62</a>
<a href="#eglew.h-63">63</a>
<a href="#eglew.h-64">64</a>
<a href="#eglew.h-65">65</a>
<a href="#eglew.h-66">66</a>
<a href="#eglew.h-67">67</a>
<a href="#eglew.h-68">68</a>
<a href="#eglew.h-69">69</a>
<a href="#eglew.h-70">70</a>
<a href="#eglew.h-71">71</a>
<a href="#eglew.h-72">72</a>
<a href="#eglew.h-73">73</a>
<a href="#eglew.h-74">74</a>
<a href="#eglew.h-75">75</a>
<a href="#eglew.h-76">76</a>
<a href="#eglew.h-77">77</a>
<a href="#eglew.h-78">78</a>
<a href="#eglew.h-79">79</a>
<a href="#eglew.h-80">80</a>
<a href="#eglew.h-81">81</a>
<a href="#eglew.h-82">82</a>
<a href="#eglew.h-83">83</a>
<a href="#eglew.h-84">84</a>
<a href="#eglew.h-85">85</a>
<a href="#eglew.h-86">86</a>
<a href="#eglew.h-87">87</a>
<a href="#eglew.h-88">88</a>
<a href="#eglew.h-89">89</a>
<a href="#eglew.h-90">90</a>
<a href="#eglew.h-91">91</a>
<a href="#eglew.h-92">92</a>
<a href="#eglew.h-93">93</a>
<a href="#eglew.h-94">94</a>
<a href="#eglew.h-95">95</a>
<a href="#eglew.h-96">96</a>
<a href="#eglew.h-97">97</a>
<a href="#eglew.h-98">98</a>
<a href="#eglew.h-99">99</a>
<a href="#eglew.h-100">100</a>
<a href="#eglew.h-101">101</a>
<a href="#eglew.h-102">102</a>
<a href="#eglew.h-103">103</a>
<a href="#eglew.h-104">104</a>
<a href="#eglew.h-105">105</a>
<a href="#eglew.h-106">106</a>
<a href="#eglew.h-107">107</a>
<a href="#eglew.h-108">108</a>
<a href="#eglew.h-109">109</a>
<a href="#eglew.h-110">110</a>
<a href="#eglew.h-111">111</a>
<a href="#eglew.h-112">112</a>
<a href="#eglew.h-113">113</a>
<a href="#eglew.h-114">114</a>
<a href="#eglew.h-115">115</a>
<a href="#eglew.h-116">116</a>
<a href="#eglew.h-117">117</a>
<a href="#eglew.h-118">118</a>
<a href="#eglew.h-119">119</a>
<a href="#eglew.h-120">120</a>
<a href="#eglew.h-121">121</a>
<a href="#eglew.h-122">122</a>
<a href="#eglew.h-123">123</a>
<a href="#eglew.h-124">124</a>
<a href="#eglew.h-125">125</a>
<a href="#eglew.h-126">126</a>
<a href="#eglew.h-127">127</a>
<a href="#eglew.h-128">128</a>
<a href="#eglew.h-129">129</a>
<a href="#eglew.h-130">130</a>
<a href="#eglew.h-131">131</a>
<a href="#eglew.h-132">132</a>
<a href="#eglew.h-133">133</a>
<a href="#eglew.h-134">134</a>
<a href="#eglew.h-135">135</a>
<a href="#eglew.h-136">136</a>
<a href="#eglew.h-137">137</a>
<a href="#eglew.h-138">138</a>
<a href="#eglew.h-139">139</a>
<a href="#eglew.h-140">140</a>
<a href="#eglew.h-141">141</a>
<a href="#eglew.h-142">142</a>
<a href="#eglew.h-143">143</a>
<a href="#eglew.h-144">144</a>
<a href="#eglew.h-145">145</a>
<a href="#eglew.h-146">146</a>
<a href="#eglew.h-147">147</a>
<a href="#eglew.h-148">148</a>
<a href="#eglew.h-149">149</a>
<a href="#eglew.h-150">150</a>
<a href="#eglew.h-151">151</a>
<a href="#eglew.h-152">152</a>
<a href="#eglew.h-153">153</a>
<a href="#eglew.h-154">154</a>
<a href="#eglew.h-155">155</a>
<a href="#eglew.h-156">156</a>
<a href="#eglew.h-157">157</a>
<a href="#eglew.h-158">158</a>
<a href="#eglew.h-159">159</a>
<a href="#eglew.h-160">160</a>
<a href="#eglew.h-161">161</a>
<a href="#eglew.h-162">162</a>
<a href="#eglew.h-163">163</a>
<a href="#eglew.h-164">164</a>
<a href="#eglew.h-165">165</a>
<a href="#eglew.h-166">166</a>
<a href="#eglew.h-167">167</a>
<a href="#eglew.h-168">168</a>
<a href="#eglew.h-169">169</a>
<a href="#eglew.h-170">170</a>
<a href="#eglew.h-171">171</a>
<a href="#eglew.h-172">172</a>
<a href="#eglew.h-173">173</a>
<a href="#eglew.h-174">174</a>
<a href="#eglew.h-175">175</a>
<a href="#eglew.h-176">176</a>
<a href="#eglew.h-177">177</a>
<a href="#eglew.h-178">178</a>
<a href="#eglew.h-179">179</a>
<a href="#eglew.h-180">180</a>
<a href="#eglew.h-181">181</a>
<a href="#eglew.h-182">182</a>
<a href="#eglew.h-183">183</a>
<a href="#eglew.h-184">184</a>
<a href="#eglew.h-185">185</a>
<a href="#eglew.h-186">186</a>
<a href="#eglew.h-187">187</a>
<a href="#eglew.h-188">188</a>
<a href="#eglew.h-189">189</a>
<a href="#eglew.h-190">190</a>
<a href="#eglew.h-191">191</a>
<a href="#eglew.h-192">192</a>
<a href="#eglew.h-193">193</a>
<a href="#eglew.h-194">194</a>
<a href="#eglew.h-195">195</a>
<a href="#eglew.h-196">196</a>
<a href="#eglew.h-197">197</a>
<a href="#eglew.h-198">198</a>
<a href="#eglew.h-199">199</a>
<a href="#eglew.h-200">200</a>
<a href="#eglew.h-201">201</a>
<a href="#eglew.h-202">202</a>
<a href="#eglew.h-203">203</a>
<a href="#eglew.h-204">204</a>
<a href="#eglew.h-205">205</a>
<a href="#eglew.h-206">206</a>
<a href="#eglew.h-207">207</a>
<a href="#eglew.h-208">208</a>
<a href="#eglew.h-209">209</a>
<a href="#eglew.h-210">210</a>
<a href="#eglew.h-211">211</a>
<a href="#eglew.h-212">212</a>
<a href="#eglew.h-213">213</a>
<a href="#eglew.h-214">214</a>
<a href="#eglew.h-215">215</a>
<a href="#eglew.h-216">216</a>
<a href="#eglew.h-217">217</a>
<a href="#eglew.h-218">218</a>
<a href="#eglew.h-219">219</a>
<a href="#eglew.h-220">220</a>
<a href="#eglew.h-221">221</a>
<a href="#eglew.h-222">222</a>
<a href="#eglew.h-223">223</a>
<a href="#eglew.h-224">224</a>
<a href="#eglew.h-225">225</a>
<a href="#eglew.h-226">226</a>
<a href="#eglew.h-227">227</a>
<a href="#eglew.h-228">228</a>
<a href="#eglew.h-229">229</a>
<a href="#eglew.h-230">230</a>
<a href="#eglew.h-231">231</a>
<a href="#eglew.h-232">232</a>
<a href="#eglew.h-233">233</a>
<a href="#eglew.h-234">234</a>
<a href="#eglew.h-235">235</a>
<a href="#eglew.h-236">236</a>
<a href="#eglew.h-237">237</a>
<a href="#eglew.h-238">238</a>
<a href="#eglew.h-239">239</a>
<a href="#eglew.h-240">240</a>
<a href="#eglew.h-241">241</a>
<a href="#eglew.h-242">242</a>
<a href="#eglew.h-243">243</a>
<a href="#eglew.h-244">244</a>
<a href="#eglew.h-245">245</a>
<a href="#eglew.h-246">246</a>
<a href="#eglew.h-247">247</a>
<a href="#eglew.h-248">248</a>
<a href="#eglew.h-249">249</a>
<a href="#eglew.h-250">250</a>
<a href="#eglew.h-251">251</a>
<a href="#eglew.h-252">252</a>
<a href="#eglew.h-253">253</a>
<a href="#eglew.h-254">254</a>
<a href="#eglew.h-255">255</a>
<a href="#eglew.h-256">256</a>
<a href="#eglew.h-257">257</a>
<a href="#eglew.h-258">258</a>
<a href="#eglew.h-259">259</a>
<a href="#eglew.h-260">260</a>
<a href="#eglew.h-261">261</a>
<a href="#eglew.h-262">262</a>
<a href="#eglew.h-263">263</a>
<a href="#eglew.h-264">264</a>
<a href="#eglew.h-265">265</a>
<a href="#eglew.h-266">266</a>
<a href="#eglew.h-267">267</a>
<a href="#eglew.h-268">268</a>
<a href="#eglew.h-269">269</a>
<a href="#eglew.h-270">270</a>
<a href="#eglew.h-271">271</a>
<a href="#eglew.h-272">272</a>
<a href="#eglew.h-273">273</a>
<a href="#eglew.h-274">274</a>
<a href="#eglew.h-275">275</a>
<a href="#eglew.h-276">276</a>
<a href="#eglew.h-277">277</a>
<a href="#eglew.h-278">278</a>
<a href="#eglew.h-279">279</a>
<a href="#eglew.h-280">280</a>
<a href="#eglew.h-281">281</a>
<a href="#eglew.h-282">282</a>
<a href="#eglew.h-283">283</a>
<a href="#eglew.h-284">284</a>
<a href="#eglew.h-285">285</a>
<a href="#eglew.h-286">286</a>
<a href="#eglew.h-287">287</a>
<a href="#eglew.h-288">288</a>
<a href="#eglew.h-289">289</a>
<a href="#eglew.h-290">290</a>
<a href="#eglew.h-291">291</a>
<a href="#eglew.h-292">292</a>
<a href="#eglew.h-293">293</a>
<a href="#eglew.h-294">294</a>
<a href="#eglew.h-295">295</a>
<a href="#eglew.h-296">296</a>
<a href="#eglew.h-297">297</a>
<a href="#eglew.h-298">298</a>
<a href="#eglew.h-299">299</a>
<a href="#eglew.h-300">300</a>
<a href="#eglew.h-301">301</a>
<a href="#eglew.h-302">302</a>
<a href="#eglew.h-303">303</a>
<a href="#eglew.h-304">304</a>
<a href="#eglew.h-305">305</a>
<a href="#eglew.h-306">306</a>
<a href="#eglew.h-307">307</a>
<a href="#eglew.h-308">308</a>
<a href="#eglew.h-309">309</a>
<a href="#eglew.h-310">310</a>
<a href="#eglew.h-311">311</a>
<a href="#eglew.h-312">312</a>
<a href="#eglew.h-313">313</a>
<a href="#eglew.h-314">314</a>
<a href="#eglew.h-315">315</a>
<a href="#eglew.h-316">316</a>
<a href="#eglew.h-317">317</a>
<a href="#eglew.h-318">318</a>
<a href="#eglew.h-319">319</a>
<a href="#eglew.h-320">320</a>
<a href="#eglew.h-321">321</a>
<a href="#eglew.h-322">322</a>
<a href="#eglew.h-323">323</a>
<a href="#eglew.h-324">324</a>
<a href="#eglew.h-325">325</a>
<a href="#eglew.h-326">326</a>
<a href="#eglew.h-327">327</a>
<a href="#eglew.h-328">328</a>
<a href="#eglew.h-329">329</a>
<a href="#eglew.h-330">330</a>
<a href="#eglew.h-331">331</a>
<a href="#eglew.h-332">332</a>
<a href="#eglew.h-333">333</a>
<a href="#eglew.h-334">334</a>
<a href="#eglew.h-335">335</a>
<a href="#eglew.h-336">336</a>
<a href="#eglew.h-337">337</a>
<a href="#eglew.h-338">338</a>
<a href="#eglew.h-339">339</a>
<a href="#eglew.h-340">340</a>
<a href="#eglew.h-341">341</a>
<a href="#eglew.h-342">342</a>
<a href="#eglew.h-343">343</a>
<a href="#eglew.h-344">344</a>
<a href="#eglew.h-345">345</a>
<a href="#eglew.h-346">346</a>
<a href="#eglew.h-347">347</a>
<a href="#eglew.h-348">348</a>
<a href="#eglew.h-349">349</a>
<a href="#eglew.h-350">350</a>
<a href="#eglew.h-351">351</a>
<a href="#eglew.h-352">352</a>
<a href="#eglew.h-353">353</a>
<a href="#eglew.h-354">354</a>
<a href="#eglew.h-355">355</a>
<a href="#eglew.h-356">356</a>
<a href="#eglew.h-357">357</a>
<a href="#eglew.h-358">358</a>
<a href="#eglew.h-359">359</a>
<a href="#eglew.h-360">360</a>
<a href="#eglew.h-361">361</a>
<a href="#eglew.h-362">362</a>
<a href="#eglew.h-363">363</a>
<a href="#eglew.h-364">364</a>
<a href="#eglew.h-365">365</a>
<a href="#eglew.h-366">366</a>
<a href="#eglew.h-367">367</a>
<a href="#eglew.h-368">368</a>
<a href="#eglew.h-369">369</a>
<a href="#eglew.h-370">370</a>
<a href="#eglew.h-371">371</a>
<a href="#eglew.h-372">372</a>
<a href="#eglew.h-373">373</a>
<a href="#eglew.h-374">374</a>
<a href="#eglew.h-375">375</a>
<a href="#eglew.h-376">376</a>
<a href="#eglew.h-377">377</a>
<a href="#eglew.h-378">378</a>
<a href="#eglew.h-379">379</a>
<a href="#eglew.h-380">380</a>
<a href="#eglew.h-381">381</a>
<a href="#eglew.h-382">382</a>
<a href="#eglew.h-383">383</a>
<a href="#eglew.h-384">384</a>
<a href="#eglew.h-385">385</a>
<a href="#eglew.h-386">386</a>
<a href="#eglew.h-387">387</a>
<a href="#eglew.h-388">388</a>
<a href="#eglew.h-389">389</a>
<a href="#eglew.h-390">390</a>
<a href="#eglew.h-391">391</a>
<a href="#eglew.h-392">392</a>
<a href="#eglew.h-393">393</a>
<a href="#eglew.h-394">394</a>
<a href="#eglew.h-395">395</a>
<a href="#eglew.h-396">396</a>
<a href="#eglew.h-397">397</a>
<a href="#eglew.h-398">398</a>
<a href="#eglew.h-399">399</a>
<a href="#eglew.h-400">400</a>
<a href="#eglew.h-401">401</a>
<a href="#eglew.h-402">402</a>
<a href="#eglew.h-403">403</a>
<a href="#eglew.h-404">404</a>
<a href="#eglew.h-405">405</a>
<a href="#eglew.h-406">406</a>
<a href="#eglew.h-407">407</a>
<a href="#eglew.h-408">408</a>
<a href="#eglew.h-409">409</a>
<a href="#eglew.h-410">410</a>
<a href="#eglew.h-411">411</a>
<a href="#eglew.h-412">412</a>
<a href="#eglew.h-413">413</a>
<a href="#eglew.h-414">414</a>
<a href="#eglew.h-415">415</a>
<a href="#eglew.h-416">416</a>
<a href="#eglew.h-417">417</a>
<a href="#eglew.h-418">418</a>
<a href="#eglew.h-419">419</a>
<a href="#eglew.h-420">420</a>
<a href="#eglew.h-421">421</a>
<a href="#eglew.h-422">422</a>
<a href="#eglew.h-423">423</a>
<a href="#eglew.h-424">424</a>
<a href="#eglew.h-425">425</a>
<a href="#eglew.h-426">426</a>
<a href="#eglew.h-427">427</a>
<a href="#eglew.h-428">428</a>
<a href="#eglew.h-429">429</a>
<a href="#eglew.h-430">430</a>
<a href="#eglew.h-431">431</a>
<a href="#eglew.h-432">432</a>
<a href="#eglew.h-433">433</a>
<a href="#eglew.h-434">434</a>
<a href="#eglew.h-435">435</a>
<a href="#eglew.h-436">436</a>
<a href="#eglew.h-437">437</a>
<a href="#eglew.h-438">438</a>
<a href="#eglew.h-439">439</a>
<a href="#eglew.h-440">440</a>
<a href="#eglew.h-441">441</a>
<a href="#eglew.h-442">442</a>
<a href="#eglew.h-443">443</a>
<a href="#eglew.h-444">444</a>
<a href="#eglew.h-445">445</a>
<a href="#eglew.h-446">446</a>
<a href="#eglew.h-447">447</a>
<a href="#eglew.h-448">448</a>
<a href="#eglew.h-449">449</a>
<a href="#eglew.h-450">450</a>
<a href="#eglew.h-451">451</a>
<a href="#eglew.h-452">452</a>
<a href="#eglew.h-453">453</a>
<a href="#eglew.h-454">454</a>
<a href="#eglew.h-455">455</a>
<a href="#eglew.h-456">456</a>
<a href="#eglew.h-457">457</a>
<a href="#eglew.h-458">458</a>
<a href="#eglew.h-459">459</a>
<a href="#eglew.h-460">460</a>
<a href="#eglew.h-461">461</a>
<a href="#eglew.h-462">462</a>
<a href="#eglew.h-463">463</a>
<a href="#eglew.h-464">464</a>
<a href="#eglew.h-465">465</a>
<a href="#eglew.h-466">466</a>
<a href="#eglew.h-467">467</a>
<a href="#eglew.h-468">468</a>
<a href="#eglew.h-469">469</a>
<a href="#eglew.h-470">470</a>
<a href="#eglew.h-471">471</a>
<a href="#eglew.h-472">472</a>
<a href="#eglew.h-473">473</a>
<a href="#eglew.h-474">474</a>
<a href="#eglew.h-475">475</a>
<a href="#eglew.h-476">476</a>
<a href="#eglew.h-477">477</a>
<a href="#eglew.h-478">478</a>
<a href="#eglew.h-479">479</a>
<a href="#eglew.h-480">480</a>
<a href="#eglew.h-481">481</a>
<a href="#eglew.h-482">482</a>
<a href="#eglew.h-483">483</a>
<a href="#eglew.h-484">484</a>
<a href="#eglew.h-485">485</a>
<a href="#eglew.h-486">486</a>
<a href="#eglew.h-487">487</a>
<a href="#eglew.h-488">488</a>
<a href="#eglew.h-489">489</a>
<a href="#eglew.h-490">490</a>
<a href="#eglew.h-491">491</a>
<a href="#eglew.h-492">492</a>
<a href="#eglew.h-493">493</a>
<a href="#eglew.h-494">494</a>
<a href="#eglew.h-495">495</a>
<a href="#eglew.h-496">496</a>
<a href="#eglew.h-497">497</a>
<a href="#eglew.h-498">498</a>
<a href="#eglew.h-499">499</a>
<a href="#eglew.h-500">500</a>
<a href="#eglew.h-501">501</a>
<a href="#eglew.h-502">502</a>
<a href="#eglew.h-503">503</a>
<a href="#eglew.h-504">504</a>
<a href="#eglew.h-505">505</a>
<a href="#eglew.h-506">506</a>
<a href="#eglew.h-507">507</a>
<a href="#eglew.h-508">508</a>
<a href="#eglew.h-509">509</a>
<a href="#eglew.h-510">510</a>
<a href="#eglew.h-511">511</a>
<a href="#eglew.h-512">512</a>
<a href="#eglew.h-513">513</a>
<a href="#eglew.h-514">514</a>
<a href="#eglew.h-515">515</a>
<a href="#eglew.h-516">516</a>
<a href="#eglew.h-517">517</a>
<a href="#eglew.h-518">518</a>
<a href="#eglew.h-519">519</a>
<a href="#eglew.h-520">520</a>
<a href="#eglew.h-521">521</a>
<a href="#eglew.h-522">522</a>
<a href="#eglew.h-523">523</a>
<a href="#eglew.h-524">524</a>
<a href="#eglew.h-525">525</a>
<a href="#eglew.h-526">526</a>
<a href="#eglew.h-527">527</a>
<a href="#eglew.h-528">528</a>
<a href="#eglew.h-529">529</a>
<a href="#eglew.h-530">530</a>
<a href="#eglew.h-531">531</a>
<a href="#eglew.h-532">532</a>
<a href="#eglew.h-533">533</a>
<a href="#eglew.h-534">534</a>
<a href="#eglew.h-535">535</a>
<a href="#eglew.h-536">536</a>
<a href="#eglew.h-537">537</a>
<a href="#eglew.h-538">538</a>
<a href="#eglew.h-539">539</a>
<a href="#eglew.h-540">540</a>
<a href="#eglew.h-541">541</a>
<a href="#eglew.h-542">542</a>
<a href="#eglew.h-543">543</a>
<a href="#eglew.h-544">544</a>
<a href="#eglew.h-545">545</a>
<a href="#eglew.h-546">546</a>
<a href="#eglew.h-547">547</a>
<a href="#eglew.h-548">548</a>
<a href="#eglew.h-549">549</a>
<a href="#eglew.h-550">550</a>
<a href="#eglew.h-551">551</a>
<a href="#eglew.h-552">552</a>
<a href="#eglew.h-553">553</a>
<a href="#eglew.h-554">554</a>
<a href="#eglew.h-555">555</a>
<a href="#eglew.h-556">556</a>
<a href="#eglew.h-557">557</a>
<a href="#eglew.h-558">558</a>
<a href="#eglew.h-559">559</a>
<a href="#eglew.h-560">560</a>
<a href="#eglew.h-561">561</a>
<a href="#eglew.h-562">562</a>
<a href="#eglew.h-563">563</a>
<a href="#eglew.h-564">564</a>
<a href="#eglew.h-565">565</a>
<a href="#eglew.h-566">566</a>
<a href="#eglew.h-567">567</a>
<a href="#eglew.h-568">568</a>
<a href="#eglew.h-569">569</a>
<a href="#eglew.h-570">570</a>
<a href="#eglew.h-571">571</a>
<a href="#eglew.h-572">572</a>
<a href="#eglew.h-573">573</a>
<a href="#eglew.h-574">574</a>
<a href="#eglew.h-575">575</a>
<a href="#eglew.h-576">576</a>
<a href="#eglew.h-577">577</a>
<a href="#eglew.h-578">578</a>
<a href="#eglew.h-579">579</a>
<a href="#eglew.h-580">580</a>
<a href="#eglew.h-581">581</a>
<a href="#eglew.h-582">582</a>
<a href="#eglew.h-583">583</a>
<a href="#eglew.h-584">584</a>
<a href="#eglew.h-585">585</a>
<a href="#eglew.h-586">586</a>
<a href="#eglew.h-587">587</a>
<a href="#eglew.h-588">588</a>
<a href="#eglew.h-589">589</a>
<a href="#eglew.h-590">590</a>
<a href="#eglew.h-591">591</a>
<a href="#eglew.h-592">592</a>
<a href="#eglew.h-593">593</a>
<a href="#eglew.h-594">594</a>
<a href="#eglew.h-595">595</a>
<a href="#eglew.h-596">596</a>
<a href="#eglew.h-597">597</a>
<a href="#eglew.h-598">598</a>
<a href="#eglew.h-599">599</a>
<a href="#eglew.h-600">600</a>
<a href="#eglew.h-601">601</a>
<a href="#eglew.h-602">602</a>
<a href="#eglew.h-603">603</a>
<a href="#eglew.h-604">604</a>
<a href="#eglew.h-605">605</a>
<a href="#eglew.h-606">606</a>
<a href="#eglew.h-607">607</a>
<a href="#eglew.h-608">608</a>
<a href="#eglew.h-609">609</a>
<a href="#eglew.h-610">610</a>
<a href="#eglew.h-611">611</a>
<a href="#eglew.h-612">612</a>
<a href="#eglew.h-613">613</a>
<a href="#eglew.h-614">614</a>
<a href="#eglew.h-615">615</a>
<a href="#eglew.h-616">616</a>
<a href="#eglew.h-617">617</a>
<a href="#eglew.h-618">618</a>
<a href="#eglew.h-619">619</a>
<a href="#eglew.h-620">620</a>
<a href="#eglew.h-621">621</a>
<a href="#eglew.h-622">622</a>
<a href="#eglew.h-623">623</a>
<a href="#eglew.h-624">624</a>
<a href="#eglew.h-625">625</a>
<a href="#eglew.h-626">626</a>
<a href="#eglew.h-627">627</a>
<a href="#eglew.h-628">628</a>
<a href="#eglew.h-629">629</a>
<a href="#eglew.h-630">630</a>
<a href="#eglew.h-631">631</a>
<a href="#eglew.h-632">632</a>
<a href="#eglew.h-633">633</a>
<a href="#eglew.h-634">634</a>
<a href="#eglew.h-635">635</a>
<a href="#eglew.h-636">636</a>
<a href="#eglew.h-637">637</a>
<a href="#eglew.h-638">638</a>
<a href="#eglew.h-639">639</a>
<a href="#eglew.h-640">640</a>
<a href="#eglew.h-641">641</a>
<a href="#eglew.h-642">642</a>
<a href="#eglew.h-643">643</a>
<a href="#eglew.h-644">644</a>
<a href="#eglew.h-645">645</a>
<a href="#eglew.h-646">646</a>
<a href="#eglew.h-647">647</a>
<a href="#eglew.h-648">648</a>
<a href="#eglew.h-649">649</a>
<a href="#eglew.h-650">650</a>
<a href="#eglew.h-651">651</a>
<a href="#eglew.h-652">652</a>
<a href="#eglew.h-653">653</a>
<a href="#eglew.h-654">654</a>
<a href="#eglew.h-655">655</a>
<a href="#eglew.h-656">656</a>
<a href="#eglew.h-657">657</a>
<a href="#eglew.h-658">658</a>
<a href="#eglew.h-659">659</a>
<a href="#eglew.h-660">660</a>
<a href="#eglew.h-661">661</a>
<a href="#eglew.h-662">662</a>
<a href="#eglew.h-663">663</a>
<a href="#eglew.h-664">664</a>
<a href="#eglew.h-665">665</a>
<a href="#eglew.h-666">666</a>
<a href="#eglew.h-667">667</a>
<a href="#eglew.h-668">668</a>
<a href="#eglew.h-669">669</a>
<a href="#eglew.h-670">670</a>
<a href="#eglew.h-671">671</a>
<a href="#eglew.h-672">672</a>
<a href="#eglew.h-673">673</a>
<a href="#eglew.h-674">674</a>
<a href="#eglew.h-675">675</a>
<a href="#eglew.h-676">676</a>
<a href="#eglew.h-677">677</a>
<a href="#eglew.h-678">678</a>
<a href="#eglew.h-679">679</a>
<a href="#eglew.h-680">680</a>
<a href="#eglew.h-681">681</a>
<a href="#eglew.h-682">682</a>
<a href="#eglew.h-683">683</a>
<a href="#eglew.h-684">684</a>
<a href="#eglew.h-685">685</a>
<a href="#eglew.h-686">686</a>
<a href="#eglew.h-687">687</a>
<a href="#eglew.h-688">688</a>
<a href="#eglew.h-689">689</a>
<a href="#eglew.h-690">690</a>
<a href="#eglew.h-691">691</a>
<a href="#eglew.h-692">692</a>
<a href="#eglew.h-693">693</a>
<a href="#eglew.h-694">694</a>
<a href="#eglew.h-695">695</a>
<a href="#eglew.h-696">696</a>
<a href="#eglew.h-697">697</a>
<a href="#eglew.h-698">698</a>
<a href="#eglew.h-699">699</a>
<a href="#eglew.h-700">700</a>
<a href="#eglew.h-701">701</a>
<a href="#eglew.h-702">702</a>
<a href="#eglew.h-703">703</a>
<a href="#eglew.h-704">704</a>
<a href="#eglew.h-705">705</a>
<a href="#eglew.h-706">706</a>
<a href="#eglew.h-707">707</a>
<a href="#eglew.h-708">708</a>
<a href="#eglew.h-709">709</a>
<a href="#eglew.h-710">710</a>
<a href="#eglew.h-711">711</a>
<a href="#eglew.h-712">712</a>
<a href="#eglew.h-713">713</a>
<a href="#eglew.h-714">714</a>
<a href="#eglew.h-715">715</a>
<a href="#eglew.h-716">716</a>
<a href="#eglew.h-717">717</a>
<a href="#eglew.h-718">718</a>
<a href="#eglew.h-719">719</a>
<a href="#eglew.h-720">720</a>
<a href="#eglew.h-721">721</a>
<a href="#eglew.h-722">722</a>
<a href="#eglew.h-723">723</a>
<a href="#eglew.h-724">724</a>
<a href="#eglew.h-725">725</a>
<a href="#eglew.h-726">726</a>
<a href="#eglew.h-727">727</a>
<a href="#eglew.h-728">728</a>
<a href="#eglew.h-729">729</a>
<a href="#eglew.h-730">730</a>
<a href="#eglew.h-731">731</a>
<a href="#eglew.h-732">732</a>
<a href="#eglew.h-733">733</a>
<a href="#eglew.h-734">734</a>
<a href="#eglew.h-735">735</a>
<a href="#eglew.h-736">736</a>
<a href="#eglew.h-737">737</a>
<a href="#eglew.h-738">738</a>
<a href="#eglew.h-739">739</a>
<a href="#eglew.h-740">740</a>
<a href="#eglew.h-741">741</a>
<a href="#eglew.h-742">742</a>
<a href="#eglew.h-743">743</a>
<a href="#eglew.h-744">744</a>
<a href="#eglew.h-745">745</a>
<a href="#eglew.h-746">746</a>
<a href="#eglew.h-747">747</a>
<a href="#eglew.h-748">748</a>
<a href="#eglew.h-749">749</a>
<a href="#eglew.h-750">750</a>
<a href="#eglew.h-751">751</a>
<a href="#eglew.h-752">752</a>
<a href="#eglew.h-753">753</a>
<a href="#eglew.h-754">754</a>
<a href="#eglew.h-755">755</a>
<a href="#eglew.h-756">756</a>
<a href="#eglew.h-757">757</a>
<a href="#eglew.h-758">758</a>
<a href="#eglew.h-759">759</a>
<a href="#eglew.h-760">760</a>
<a href="#eglew.h-761">761</a>
<a href="#eglew.h-762">762</a>
<a href="#eglew.h-763">763</a>
<a href="#eglew.h-764">764</a>
<a href="#eglew.h-765">765</a>
<a href="#eglew.h-766">766</a>
<a href="#eglew.h-767">767</a>
<a href="#eglew.h-768">768</a>
<a href="#eglew.h-769">769</a>
<a href="#eglew.h-770">770</a>
<a href="#eglew.h-771">771</a>
<a href="#eglew.h-772">772</a>
<a href="#eglew.h-773">773</a>
<a href="#eglew.h-774">774</a>
<a href="#eglew.h-775">775</a>
<a href="#eglew.h-776">776</a>
<a href="#eglew.h-777">777</a>
<a href="#eglew.h-778">778</a>
<a href="#eglew.h-779">779</a>
<a href="#eglew.h-780">780</a>
<a href="#eglew.h-781">781</a>
<a href="#eglew.h-782">782</a>
<a href="#eglew.h-783">783</a>
<a href="#eglew.h-784">784</a>
<a href="#eglew.h-785">785</a>
<a href="#eglew.h-786">786</a>
<a href="#eglew.h-787">787</a>
<a href="#eglew.h-788">788</a>
<a href="#eglew.h-789">789</a>
<a href="#eglew.h-790">790</a>
<a href="#eglew.h-791">791</a>
<a href="#eglew.h-792">792</a>
<a href="#eglew.h-793">793</a>
<a href="#eglew.h-794">794</a>
<a href="#eglew.h-795">795</a>
<a href="#eglew.h-796">796</a>
<a href="#eglew.h-797">797</a>
<a href="#eglew.h-798">798</a>
<a href="#eglew.h-799">799</a>
<a href="#eglew.h-800">800</a>
<a href="#eglew.h-801">801</a>
<a href="#eglew.h-802">802</a>
<a href="#eglew.h-803">803</a>
<a href="#eglew.h-804">804</a>
<a href="#eglew.h-805">805</a>
<a href="#eglew.h-806">806</a>
<a href="#eglew.h-807">807</a>
<a href="#eglew.h-808">808</a>
<a href="#eglew.h-809">809</a>
<a href="#eglew.h-810">810</a>
<a href="#eglew.h-811">811</a>
<a href="#eglew.h-812">812</a>
<a href="#eglew.h-813">813</a>
<a href="#eglew.h-814">814</a>
<a href="#eglew.h-815">815</a>
<a href="#eglew.h-816">816</a>
<a href="#eglew.h-817">817</a>
<a href="#eglew.h-818">818</a>
<a href="#eglew.h-819">819</a>
<a href="#eglew.h-820">820</a>
<a href="#eglew.h-821">821</a>
<a href="#eglew.h-822">822</a>
<a href="#eglew.h-823">823</a>
<a href="#eglew.h-824">824</a>
<a href="#eglew.h-825">825</a>
<a href="#eglew.h-826">826</a>
<a href="#eglew.h-827">827</a>
<a href="#eglew.h-828">828</a>
<a href="#eglew.h-829">829</a>
<a href="#eglew.h-830">830</a>
<a href="#eglew.h-831">831</a>
<a href="#eglew.h-832">832</a>
<a href="#eglew.h-833">833</a>
<a href="#eglew.h-834">834</a>
<a href="#eglew.h-835">835</a>
<a href="#eglew.h-836">836</a>
<a href="#eglew.h-837">837</a>
<a href="#eglew.h-838">838</a>
<a href="#eglew.h-839">839</a>
<a href="#eglew.h-840">840</a>
<a href="#eglew.h-841">841</a>
<a href="#eglew.h-842">842</a>
<a href="#eglew.h-843">843</a>
<a href="#eglew.h-844">844</a>
<a href="#eglew.h-845">845</a>
<a href="#eglew.h-846">846</a>
<a href="#eglew.h-847">847</a>
<a href="#eglew.h-848">848</a>
<a href="#eglew.h-849">849</a>
<a href="#eglew.h-850">850</a>
<a href="#eglew.h-851">851</a>
<a href="#eglew.h-852">852</a>
<a href="#eglew.h-853">853</a>
<a href="#eglew.h-854">854</a>
<a href="#eglew.h-855">855</a>
<a href="#eglew.h-856">856</a>
<a href="#eglew.h-857">857</a>
<a href="#eglew.h-858">858</a>
<a href="#eglew.h-859">859</a>
<a href="#eglew.h-860">860</a>
<a href="#eglew.h-861">861</a>
<a href="#eglew.h-862">862</a>
<a href="#eglew.h-863">863</a>
<a href="#eglew.h-864">864</a>
<a href="#eglew.h-865">865</a>
<a href="#eglew.h-866">866</a>
<a href="#eglew.h-867">867</a>
<a href="#eglew.h-868">868</a>
<a href="#eglew.h-869">869</a>
<a href="#eglew.h-870">870</a>
<a href="#eglew.h-871">871</a>
<a href="#eglew.h-872">872</a>
<a href="#eglew.h-873">873</a>
<a href="#eglew.h-874">874</a>
<a href="#eglew.h-875">875</a>
<a href="#eglew.h-876">876</a>
<a href="#eglew.h-877">877</a>
<a href="#eglew.h-878">878</a>
<a href="#eglew.h-879">879</a>
<a href="#eglew.h-880">880</a>
<a href="#eglew.h-881">881</a>
<a href="#eglew.h-882">882</a>
<a href="#eglew.h-883">883</a>
<a href="#eglew.h-884">884</a>
<a href="#eglew.h-885">885</a>
<a href="#eglew.h-886">886</a>
<a href="#eglew.h-887">887</a>
<a href="#eglew.h-888">888</a>
<a href="#eglew.h-889">889</a>
<a href="#eglew.h-890">890</a>
<a href="#eglew.h-891">891</a>
<a href="#eglew.h-892">892</a>
<a href="#eglew.h-893">893</a>
<a href="#eglew.h-894">894</a>
<a href="#eglew.h-895">895</a>
<a href="#eglew.h-896">896</a>
<a href="#eglew.h-897">897</a>
<a href="#eglew.h-898">898</a>
<a href="#eglew.h-899">899</a>
<a href="#eglew.h-900">900</a>
<a href="#eglew.h-901">901</a>
<a href="#eglew.h-902">902</a>
<a href="#eglew.h-903">903</a>
<a href="#eglew.h-904">904</a>
<a href="#eglew.h-905">905</a>
<a href="#eglew.h-906">906</a>
<a href="#eglew.h-907">907</a>
<a href="#eglew.h-908">908</a>
<a href="#eglew.h-909">909</a>
<a href="#eglew.h-910">910</a>
<a href="#eglew.h-911">911</a>
<a href="#eglew.h-912">912</a>
<a href="#eglew.h-913">913</a>
<a href="#eglew.h-914">914</a>
<a href="#eglew.h-915">915</a>
<a href="#eglew.h-916">916</a>
<a href="#eglew.h-917">917</a>
<a href="#eglew.h-918">918</a>
<a href="#eglew.h-919">919</a>
<a href="#eglew.h-920">920</a>
<a href="#eglew.h-921">921</a>
<a href="#eglew.h-922">922</a>
<a href="#eglew.h-923">923</a>
<a href="#eglew.h-924">924</a>
<a href="#eglew.h-925">925</a>
<a href="#eglew.h-926">926</a>
<a href="#eglew.h-927">927</a>
<a href="#eglew.h-928">928</a>
<a href="#eglew.h-929">929</a>
<a href="#eglew.h-930">930</a>
<a href="#eglew.h-931">931</a>
<a href="#eglew.h-932">932</a>
<a href="#eglew.h-933">933</a>
<a href="#eglew.h-934">934</a>
<a href="#eglew.h-935">935</a>
<a href="#eglew.h-936">936</a>
<a href="#eglew.h-937">937</a>
<a href="#eglew.h-938">938</a>
<a href="#eglew.h-939">939</a>
<a href="#eglew.h-940">940</a>
<a href="#eglew.h-941">941</a>
<a href="#eglew.h-942">942</a>
<a href="#eglew.h-943">943</a>
<a href="#eglew.h-944">944</a>
<a href="#eglew.h-945">945</a>
<a href="#eglew.h-946">946</a>
<a href="#eglew.h-947">947</a>
<a href="#eglew.h-948">948</a>
<a href="#eglew.h-949">949</a>
<a href="#eglew.h-950">950</a>
<a href="#eglew.h-951">951</a>
<a href="#eglew.h-952">952</a>
<a href="#eglew.h-953">953</a>
<a href="#eglew.h-954">954</a>
<a href="#eglew.h-955">955</a>
<a href="#eglew.h-956">956</a>
<a href="#eglew.h-957">957</a>
<a href="#eglew.h-958">958</a>
<a href="#eglew.h-959">959</a>
<a href="#eglew.h-960">960</a>
<a href="#eglew.h-961">961</a>
<a href="#eglew.h-962">962</a>
<a href="#eglew.h-963">963</a>
<a href="#eglew.h-964">964</a>
<a href="#eglew.h-965">965</a>
<a href="#eglew.h-966">966</a>
<a href="#eglew.h-967">967</a>
<a href="#eglew.h-968">968</a>
<a href="#eglew.h-969">969</a>
<a href="#eglew.h-970">970</a>
<a href="#eglew.h-971">971</a>
<a href="#eglew.h-972">972</a>
<a href="#eglew.h-973">973</a>
<a href="#eglew.h-974">974</a>
<a href="#eglew.h-975">975</a>
<a href="#eglew.h-976">976</a>
<a href="#eglew.h-977">977</a>
<a href="#eglew.h-978">978</a>
<a href="#eglew.h-979">979</a>
<a href="#eglew.h-980">980</a>
<a href="#eglew.h-981">981</a>
<a href="#eglew.h-982">982</a>
<a href="#eglew.h-983">983</a>
<a href="#eglew.h-984">984</a>
<a href="#eglew.h-985">985</a>
<a href="#eglew.h-986">986</a>
<a href="#eglew.h-987">987</a>
<a href="#eglew.h-988">988</a>
<a href="#eglew.h-989">989</a>
<a href="#eglew.h-990">990</a>
<a href="#eglew.h-991">991</a>
<a href="#eglew.h-992">992</a>
<a href="#eglew.h-993">993</a>
<a href="#eglew.h-994">994</a>
<a href="#eglew.h-995">995</a>
<a href="#eglew.h-996">996</a>
<a href="#eglew.h-997">997</a>
<a href="#eglew.h-998">998</a>
<a href="#eglew.h-999">999</a>
<a href="#eglew.h-1000">1000</a>
<a href="#eglew.h-1001">1001</a>
<a href="#eglew.h-1002">1002</a>
<a href="#eglew.h-1003">1003</a>
<a href="#eglew.h-1004">1004</a>
<a href="#eglew.h-1005">1005</a>
<a href="#eglew.h-1006">1006</a>
<a href="#eglew.h-1007">1007</a>
<a href="#eglew.h-1008">1008</a>
<a href="#eglew.h-1009">1009</a>
<a href="#eglew.h-1010">1010</a>
<a href="#eglew.h-1011">1011</a>
<a href="#eglew.h-1012">1012</a>
<a href="#eglew.h-1013">1013</a>
<a href="#eglew.h-1014">1014</a>
<a href="#eglew.h-1015">1015</a>
<a href="#eglew.h-1016">1016</a>
<a href="#eglew.h-1017">1017</a>
<a href="#eglew.h-1018">1018</a>
<a href="#eglew.h-1019">1019</a>
<a href="#eglew.h-1020">1020</a>
<a href="#eglew.h-1021">1021</a>
<a href="#eglew.h-1022">1022</a>
<a href="#eglew.h-1023">1023</a>
<a href="#eglew.h-1024">1024</a>
<a href="#eglew.h-1025">1025</a>
<a href="#eglew.h-1026">1026</a>
<a href="#eglew.h-1027">1027</a>
<a href="#eglew.h-1028">1028</a>
<a href="#eglew.h-1029">1029</a>
<a href="#eglew.h-1030">1030</a>
<a href="#eglew.h-1031">1031</a>
<a href="#eglew.h-1032">1032</a>
<a href="#eglew.h-1033">1033</a>
<a href="#eglew.h-1034">1034</a>
<a href="#eglew.h-1035">1035</a>
<a href="#eglew.h-1036">1036</a>
<a href="#eglew.h-1037">1037</a>
<a href="#eglew.h-1038">1038</a>
<a href="#eglew.h-1039">1039</a>
<a href="#eglew.h-1040">1040</a>
<a href="#eglew.h-1041">1041</a>
<a href="#eglew.h-1042">1042</a>
<a href="#eglew.h-1043">1043</a>
<a href="#eglew.h-1044">1044</a>
<a href="#eglew.h-1045">1045</a>
<a href="#eglew.h-1046">1046</a>
<a href="#eglew.h-1047">1047</a>
<a href="#eglew.h-1048">1048</a>
<a href="#eglew.h-1049">1049</a>
<a href="#eglew.h-1050">1050</a>
<a href="#eglew.h-1051">1051</a>
<a href="#eglew.h-1052">1052</a>
<a href="#eglew.h-1053">1053</a>
<a href="#eglew.h-1054">1054</a>
<a href="#eglew.h-1055">1055</a>
<a href="#eglew.h-1056">1056</a>
<a href="#eglew.h-1057">1057</a>
<a href="#eglew.h-1058">1058</a>
<a href="#eglew.h-1059">1059</a>
<a href="#eglew.h-1060">1060</a>
<a href="#eglew.h-1061">1061</a>
<a href="#eglew.h-1062">1062</a>
<a href="#eglew.h-1063">1063</a>
<a href="#eglew.h-1064">1064</a>
<a href="#eglew.h-1065">1065</a>
<a href="#eglew.h-1066">1066</a>
<a href="#eglew.h-1067">1067</a>
<a href="#eglew.h-1068">1068</a>
<a href="#eglew.h-1069">1069</a>
<a href="#eglew.h-1070">1070</a>
<a href="#eglew.h-1071">1071</a>
<a href="#eglew.h-1072">1072</a>
<a href="#eglew.h-1073">1073</a>
<a href="#eglew.h-1074">1074</a>
<a href="#eglew.h-1075">1075</a>
<a href="#eglew.h-1076">1076</a>
<a href="#eglew.h-1077">1077</a>
<a href="#eglew.h-1078">1078</a>
<a href="#eglew.h-1079">1079</a>
<a href="#eglew.h-1080">1080</a>
<a href="#eglew.h-1081">1081</a>
<a href="#eglew.h-1082">1082</a>
<a href="#eglew.h-1083">1083</a>
<a href="#eglew.h-1084">1084</a>
<a href="#eglew.h-1085">1085</a>
<a href="#eglew.h-1086">1086</a>
<a href="#eglew.h-1087">1087</a>
<a href="#eglew.h-1088">1088</a>
<a href="#eglew.h-1089">1089</a>
<a href="#eglew.h-1090">1090</a>
<a href="#eglew.h-1091">1091</a>
<a href="#eglew.h-1092">1092</a>
<a href="#eglew.h-1093">1093</a>
<a href="#eglew.h-1094">1094</a>
<a href="#eglew.h-1095">1095</a>
<a href="#eglew.h-1096">1096</a>
<a href="#eglew.h-1097">1097</a>
<a href="#eglew.h-1098">1098</a>
<a href="#eglew.h-1099">1099</a>
<a href="#eglew.h-1100">1100</a>
<a href="#eglew.h-1101">1101</a>
<a href="#eglew.h-1102">1102</a>
<a href="#eglew.h-1103">1103</a>
<a href="#eglew.h-1104">1104</a>
<a href="#eglew.h-1105">1105</a>
<a href="#eglew.h-1106">1106</a>
<a href="#eglew.h-1107">1107</a>
<a href="#eglew.h-1108">1108</a>
<a href="#eglew.h-1109">1109</a>
<a href="#eglew.h-1110">1110</a>
<a href="#eglew.h-1111">1111</a>
<a href="#eglew.h-1112">1112</a>
<a href="#eglew.h-1113">1113</a>
<a href="#eglew.h-1114">1114</a>
<a href="#eglew.h-1115">1115</a>
<a href="#eglew.h-1116">1116</a>
<a href="#eglew.h-1117">1117</a>
<a href="#eglew.h-1118">1118</a>
<a href="#eglew.h-1119">1119</a>
<a href="#eglew.h-1120">1120</a>
<a href="#eglew.h-1121">1121</a>
<a href="#eglew.h-1122">1122</a>
<a href="#eglew.h-1123">1123</a>
<a href="#eglew.h-1124">1124</a>
<a href="#eglew.h-1125">1125</a>
<a href="#eglew.h-1126">1126</a>
<a href="#eglew.h-1127">1127</a>
<a href="#eglew.h-1128">1128</a>
<a href="#eglew.h-1129">1129</a>
<a href="#eglew.h-1130">1130</a>
<a href="#eglew.h-1131">1131</a>
<a href="#eglew.h-1132">1132</a>
<a href="#eglew.h-1133">1133</a>
<a href="#eglew.h-1134">1134</a>
<a href="#eglew.h-1135">1135</a>
<a href="#eglew.h-1136">1136</a>
<a href="#eglew.h-1137">1137</a>
<a href="#eglew.h-1138">1138</a>
<a href="#eglew.h-1139">1139</a>
<a href="#eglew.h-1140">1140</a>
<a href="#eglew.h-1141">1141</a>
<a href="#eglew.h-1142">1142</a>
<a href="#eglew.h-1143">1143</a>
<a href="#eglew.h-1144">1144</a>
<a href="#eglew.h-1145">1145</a>
<a href="#eglew.h-1146">1146</a>
<a href="#eglew.h-1147">1147</a>
<a href="#eglew.h-1148">1148</a>
<a href="#eglew.h-1149">1149</a>
<a href="#eglew.h-1150">1150</a>
<a href="#eglew.h-1151">1151</a>
<a href="#eglew.h-1152">1152</a>
<a href="#eglew.h-1153">1153</a>
<a href="#eglew.h-1154">1154</a>
<a href="#eglew.h-1155">1155</a>
<a href="#eglew.h-1156">1156</a>
<a href="#eglew.h-1157">1157</a>
<a href="#eglew.h-1158">1158</a>
<a href="#eglew.h-1159">1159</a>
<a href="#eglew.h-1160">1160</a>
<a href="#eglew.h-1161">1161</a>
<a href="#eglew.h-1162">1162</a>
<a href="#eglew.h-1163">1163</a>
<a href="#eglew.h-1164">1164</a>
<a href="#eglew.h-1165">1165</a>
<a href="#eglew.h-1166">1166</a>
<a href="#eglew.h-1167">1167</a>
<a href="#eglew.h-1168">1168</a>
<a href="#eglew.h-1169">1169</a>
<a href="#eglew.h-1170">1170</a>
<a href="#eglew.h-1171">1171</a>
<a href="#eglew.h-1172">1172</a>
<a href="#eglew.h-1173">1173</a>
<a href="#eglew.h-1174">1174</a>
<a href="#eglew.h-1175">1175</a>
<a href="#eglew.h-1176">1176</a>
<a href="#eglew.h-1177">1177</a>
<a href="#eglew.h-1178">1178</a>
<a href="#eglew.h-1179">1179</a>
<a href="#eglew.h-1180">1180</a>
<a href="#eglew.h-1181">1181</a>
<a href="#eglew.h-1182">1182</a>
<a href="#eglew.h-1183">1183</a>
<a href="#eglew.h-1184">1184</a>
<a href="#eglew.h-1185">1185</a>
<a href="#eglew.h-1186">1186</a>
<a href="#eglew.h-1187">1187</a>
<a href="#eglew.h-1188">1188</a>
<a href="#eglew.h-1189">1189</a>
<a href="#eglew.h-1190">1190</a>
<a href="#eglew.h-1191">1191</a>
<a href="#eglew.h-1192">1192</a>
<a href="#eglew.h-1193">1193</a>
<a href="#eglew.h-1194">1194</a>
<a href="#eglew.h-1195">1195</a>
<a href="#eglew.h-1196">1196</a>
<a href="#eglew.h-1197">1197</a>
<a href="#eglew.h-1198">1198</a>
<a href="#eglew.h-1199">1199</a>
<a href="#eglew.h-1200">1200</a>
<a href="#eglew.h-1201">1201</a>
<a href="#eglew.h-1202">1202</a>
<a href="#eglew.h-1203">1203</a>
<a href="#eglew.h-1204">1204</a>
<a href="#eglew.h-1205">1205</a>
<a href="#eglew.h-1206">1206</a>
<a href="#eglew.h-1207">1207</a>
<a href="#eglew.h-1208">1208</a>
<a href="#eglew.h-1209">1209</a>
<a href="#eglew.h-1210">1210</a>
<a href="#eglew.h-1211">1211</a>
<a href="#eglew.h-1212">1212</a>
<a href="#eglew.h-1213">1213</a>
<a href="#eglew.h-1214">1214</a>
<a href="#eglew.h-1215">1215</a>
<a href="#eglew.h-1216">1216</a>
<a href="#eglew.h-1217">1217</a>
<a href="#eglew.h-1218">1218</a>
<a href="#eglew.h-1219">1219</a>
<a href="#eglew.h-1220">1220</a>
<a href="#eglew.h-1221">1221</a>
<a href="#eglew.h-1222">1222</a>
<a href="#eglew.h-1223">1223</a>
<a href="#eglew.h-1224">1224</a>
<a href="#eglew.h-1225">1225</a>
<a href="#eglew.h-1226">1226</a>
<a href="#eglew.h-1227">1227</a>
<a href="#eglew.h-1228">1228</a>
<a href="#eglew.h-1229">1229</a>
<a href="#eglew.h-1230">1230</a>
<a href="#eglew.h-1231">1231</a>
<a href="#eglew.h-1232">1232</a>
<a href="#eglew.h-1233">1233</a>
<a href="#eglew.h-1234">1234</a>
<a href="#eglew.h-1235">1235</a>
<a href="#eglew.h-1236">1236</a>
<a href="#eglew.h-1237">1237</a>
<a href="#eglew.h-1238">1238</a>
<a href="#eglew.h-1239">1239</a>
<a href="#eglew.h-1240">1240</a>
<a href="#eglew.h-1241">1241</a>
<a href="#eglew.h-1242">1242</a>
<a href="#eglew.h-1243">1243</a>
<a href="#eglew.h-1244">1244</a>
<a href="#eglew.h-1245">1245</a>
<a href="#eglew.h-1246">1246</a>
<a href="#eglew.h-1247">1247</a>
<a href="#eglew.h-1248">1248</a>
<a href="#eglew.h-1249">1249</a>
<a href="#eglew.h-1250">1250</a>
<a href="#eglew.h-1251">1251</a>
<a href="#eglew.h-1252">1252</a>
<a href="#eglew.h-1253">1253</a>
<a href="#eglew.h-1254">1254</a>
<a href="#eglew.h-1255">1255</a>
<a href="#eglew.h-1256">1256</a>
<a href="#eglew.h-1257">1257</a>
<a href="#eglew.h-1258">1258</a>
<a href="#eglew.h-1259">1259</a>
<a href="#eglew.h-1260">1260</a>
<a href="#eglew.h-1261">1261</a>
<a href="#eglew.h-1262">1262</a>
<a href="#eglew.h-1263">1263</a>
<a href="#eglew.h-1264">1264</a>
<a href="#eglew.h-1265">1265</a>
<a href="#eglew.h-1266">1266</a>
<a href="#eglew.h-1267">1267</a>
<a href="#eglew.h-1268">1268</a>
<a href="#eglew.h-1269">1269</a>
<a href="#eglew.h-1270">1270</a>
<a href="#eglew.h-1271">1271</a>
<a href="#eglew.h-1272">1272</a>
<a href="#eglew.h-1273">1273</a>
<a href="#eglew.h-1274">1274</a>
<a href="#eglew.h-1275">1275</a>
<a href="#eglew.h-1276">1276</a>
<a href="#eglew.h-1277">1277</a>
<a href="#eglew.h-1278">1278</a>
<a href="#eglew.h-1279">1279</a>
<a href="#eglew.h-1280">1280</a>
<a href="#eglew.h-1281">1281</a>
<a href="#eglew.h-1282">1282</a>
<a href="#eglew.h-1283">1283</a>
<a href="#eglew.h-1284">1284</a>
<a href="#eglew.h-1285">1285</a>
<a href="#eglew.h-1286">1286</a>
<a href="#eglew.h-1287">1287</a>
<a href="#eglew.h-1288">1288</a>
<a href="#eglew.h-1289">1289</a>
<a href="#eglew.h-1290">1290</a>
<a href="#eglew.h-1291">1291</a>
<a href="#eglew.h-1292">1292</a>
<a href="#eglew.h-1293">1293</a>
<a href="#eglew.h-1294">1294</a>
<a href="#eglew.h-1295">1295</a>
<a href="#eglew.h-1296">1296</a>
<a href="#eglew.h-1297">1297</a>
<a href="#eglew.h-1298">1298</a>
<a href="#eglew.h-1299">1299</a>
<a href="#eglew.h-1300">1300</a>
<a href="#eglew.h-1301">1301</a>
<a href="#eglew.h-1302">1302</a>
<a href="#eglew.h-1303">1303</a>
<a href="#eglew.h-1304">1304</a>
<a href="#eglew.h-1305">1305</a>
<a href="#eglew.h-1306">1306</a>
<a href="#eglew.h-1307">1307</a>
<a href="#eglew.h-1308">1308</a>
<a href="#eglew.h-1309">1309</a>
<a href="#eglew.h-1310">1310</a>
<a href="#eglew.h-1311">1311</a>
<a href="#eglew.h-1312">1312</a>
<a href="#eglew.h-1313">1313</a>
<a href="#eglew.h-1314">1314</a>
<a href="#eglew.h-1315">1315</a>
<a href="#eglew.h-1316">1316</a>
<a href="#eglew.h-1317">1317</a>
<a href="#eglew.h-1318">1318</a>
<a href="#eglew.h-1319">1319</a>
<a href="#eglew.h-1320">1320</a>
<a href="#eglew.h-1321">1321</a>
<a href="#eglew.h-1322">1322</a>
<a href="#eglew.h-1323">1323</a>
<a href="#eglew.h-1324">1324</a>
<a href="#eglew.h-1325">1325</a>
<a href="#eglew.h-1326">1326</a>
<a href="#eglew.h-1327">1327</a>
<a href="#eglew.h-1328">1328</a>
<a href="#eglew.h-1329">1329</a>
<a href="#eglew.h-1330">1330</a>
<a href="#eglew.h-1331">1331</a>
<a href="#eglew.h-1332">1332</a>
<a href="#eglew.h-1333">1333</a>
<a href="#eglew.h-1334">1334</a>
<a href="#eglew.h-1335">1335</a>
<a href="#eglew.h-1336">1336</a>
<a href="#eglew.h-1337">1337</a>
<a href="#eglew.h-1338">1338</a>
<a href="#eglew.h-1339">1339</a>
<a href="#eglew.h-1340">1340</a>
<a href="#eglew.h-1341">1341</a>
<a href="#eglew.h-1342">1342</a>
<a href="#eglew.h-1343">1343</a>
<a href="#eglew.h-1344">1344</a>
<a href="#eglew.h-1345">1345</a>
<a href="#eglew.h-1346">1346</a>
<a href="#eglew.h-1347">1347</a>
<a href="#eglew.h-1348">1348</a>
<a href="#eglew.h-1349">1349</a>
<a href="#eglew.h-1350">1350</a>
<a href="#eglew.h-1351">1351</a>
<a href="#eglew.h-1352">1352</a>
<a href="#eglew.h-1353">1353</a>
<a href="#eglew.h-1354">1354</a>
<a href="#eglew.h-1355">1355</a>
<a href="#eglew.h-1356">1356</a>
<a href="#eglew.h-1357">1357</a>
<a href="#eglew.h-1358">1358</a>
<a href="#eglew.h-1359">1359</a>
<a href="#eglew.h-1360">1360</a>
<a href="#eglew.h-1361">1361</a>
<a href="#eglew.h-1362">1362</a>
<a href="#eglew.h-1363">1363</a>
<a href="#eglew.h-1364">1364</a>
<a href="#eglew.h-1365">1365</a>
<a href="#eglew.h-1366">1366</a>
<a href="#eglew.h-1367">1367</a>
<a href="#eglew.h-1368">1368</a>
<a href="#eglew.h-1369">1369</a>
<a href="#eglew.h-1370">1370</a>
<a href="#eglew.h-1371">1371</a>
<a href="#eglew.h-1372">1372</a>
<a href="#eglew.h-1373">1373</a>
<a href="#eglew.h-1374">1374</a>
<a href="#eglew.h-1375">1375</a>
<a href="#eglew.h-1376">1376</a>
<a href="#eglew.h-1377">1377</a>
<a href="#eglew.h-1378">1378</a>
<a href="#eglew.h-1379">1379</a>
<a href="#eglew.h-1380">1380</a>
<a href="#eglew.h-1381">1381</a>
<a href="#eglew.h-1382">1382</a>
<a href="#eglew.h-1383">1383</a>
<a href="#eglew.h-1384">1384</a>
<a href="#eglew.h-1385">1385</a>
<a href="#eglew.h-1386">1386</a>
<a href="#eglew.h-1387">1387</a>
<a href="#eglew.h-1388">1388</a>
<a href="#eglew.h-1389">1389</a>
<a href="#eglew.h-1390">1390</a>
<a href="#eglew.h-1391">1391</a>
<a href="#eglew.h-1392">1392</a>
<a href="#eglew.h-1393">1393</a>
<a href="#eglew.h-1394">1394</a>
<a href="#eglew.h-1395">1395</a>
<a href="#eglew.h-1396">1396</a>
<a href="#eglew.h-1397">1397</a>
<a href="#eglew.h-1398">1398</a>
<a href="#eglew.h-1399">1399</a>
<a href="#eglew.h-1400">1400</a>
<a href="#eglew.h-1401">1401</a>
<a href="#eglew.h-1402">1402</a>
<a href="#eglew.h-1403">1403</a>
<a href="#eglew.h-1404">1404</a>
<a href="#eglew.h-1405">1405</a>
<a href="#eglew.h-1406">1406</a>
<a href="#eglew.h-1407">1407</a>
<a href="#eglew.h-1408">1408</a>
<a href="#eglew.h-1409">1409</a>
<a href="#eglew.h-1410">1410</a>
<a href="#eglew.h-1411">1411</a>
<a href="#eglew.h-1412">1412</a>
<a href="#eglew.h-1413">1413</a>
<a href="#eglew.h-1414">1414</a>
<a href="#eglew.h-1415">1415</a>
<a href="#eglew.h-1416">1416</a>
<a href="#eglew.h-1417">1417</a>
<a href="#eglew.h-1418">1418</a>
<a href="#eglew.h-1419">1419</a>
<a href="#eglew.h-1420">1420</a>
<a href="#eglew.h-1421">1421</a>
<a href="#eglew.h-1422">1422</a>
<a href="#eglew.h-1423">1423</a>
<a href="#eglew.h-1424">1424</a>
<a href="#eglew.h-1425">1425</a>
<a href="#eglew.h-1426">1426</a>
<a href="#eglew.h-1427">1427</a>
<a href="#eglew.h-1428">1428</a>
<a href="#eglew.h-1429">1429</a>
<a href="#eglew.h-1430">1430</a>
<a href="#eglew.h-1431">1431</a>
<a href="#eglew.h-1432">1432</a>
<a href="#eglew.h-1433">1433</a>
<a href="#eglew.h-1434">1434</a>
<a href="#eglew.h-1435">1435</a>
<a href="#eglew.h-1436">1436</a>
<a href="#eglew.h-1437">1437</a>
<a href="#eglew.h-1438">1438</a>
<a href="#eglew.h-1439">1439</a>
<a href="#eglew.h-1440">1440</a>
<a href="#eglew.h-1441">1441</a>
<a href="#eglew.h-1442">1442</a>
<a href="#eglew.h-1443">1443</a>
<a href="#eglew.h-1444">1444</a>
<a href="#eglew.h-1445">1445</a>
<a href="#eglew.h-1446">1446</a>
<a href="#eglew.h-1447">1447</a>
<a href="#eglew.h-1448">1448</a>
<a href="#eglew.h-1449">1449</a>
<a href="#eglew.h-1450">1450</a>
<a href="#eglew.h-1451">1451</a>
<a href="#eglew.h-1452">1452</a>
<a href="#eglew.h-1453">1453</a>
<a href="#eglew.h-1454">1454</a>
<a href="#eglew.h-1455">1455</a>
<a href="#eglew.h-1456">1456</a>
<a href="#eglew.h-1457">1457</a>
<a href="#eglew.h-1458">1458</a>
<a href="#eglew.h-1459">1459</a>
<a href="#eglew.h-1460">1460</a>
<a href="#eglew.h-1461">1461</a>
<a href="#eglew.h-1462">1462</a>
<a href="#eglew.h-1463">1463</a>
<a href="#eglew.h-1464">1464</a>
<a href="#eglew.h-1465">1465</a>
<a href="#eglew.h-1466">1466</a>
<a href="#eglew.h-1467">1467</a>
<a href="#eglew.h-1468">1468</a>
<a href="#eglew.h-1469">1469</a>
<a href="#eglew.h-1470">1470</a>
<a href="#eglew.h-1471">1471</a>
<a href="#eglew.h-1472">1472</a>
<a href="#eglew.h-1473">1473</a>
<a href="#eglew.h-1474">1474</a>
<a href="#eglew.h-1475">1475</a>
<a href="#eglew.h-1476">1476</a>
<a href="#eglew.h-1477">1477</a>
<a href="#eglew.h-1478">1478</a>
<a href="#eglew.h-1479">1479</a>
<a href="#eglew.h-1480">1480</a>
<a href="#eglew.h-1481">1481</a>
<a href="#eglew.h-1482">1482</a>
<a href="#eglew.h-1483">1483</a>
<a href="#eglew.h-1484">1484</a>
<a href="#eglew.h-1485">1485</a>
<a href="#eglew.h-1486">1486</a>
<a href="#eglew.h-1487">1487</a>
<a href="#eglew.h-1488">1488</a>
<a href="#eglew.h-1489">1489</a>
<a href="#eglew.h-1490">1490</a>
<a href="#eglew.h-1491">1491</a>
<a href="#eglew.h-1492">1492</a>
<a href="#eglew.h-1493">1493</a>
<a href="#eglew.h-1494">1494</a>
<a href="#eglew.h-1495">1495</a>
<a href="#eglew.h-1496">1496</a>
<a href="#eglew.h-1497">1497</a>
<a href="#eglew.h-1498">1498</a>
<a href="#eglew.h-1499">1499</a>
<a href="#eglew.h-1500">1500</a>
<a href="#eglew.h-1501">1501</a>
<a href="#eglew.h-1502">1502</a>
<a href="#eglew.h-1503">1503</a>
<a href="#eglew.h-1504">1504</a>
<a href="#eglew.h-1505">1505</a>
<a href="#eglew.h-1506">1506</a>
<a href="#eglew.h-1507">1507</a>
<a href="#eglew.h-1508">1508</a>
<a href="#eglew.h-1509">1509</a>
<a href="#eglew.h-1510">1510</a>
<a href="#eglew.h-1511">1511</a>
<a href="#eglew.h-1512">1512</a>
<a href="#eglew.h-1513">1513</a>
<a href="#eglew.h-1514">1514</a>
<a href="#eglew.h-1515">1515</a>
<a href="#eglew.h-1516">1516</a>
<a href="#eglew.h-1517">1517</a>
<a href="#eglew.h-1518">1518</a>
<a href="#eglew.h-1519">1519</a>
<a href="#eglew.h-1520">1520</a>
<a href="#eglew.h-1521">1521</a>
<a href="#eglew.h-1522">1522</a>
<a href="#eglew.h-1523">1523</a>
<a href="#eglew.h-1524">1524</a>
<a href="#eglew.h-1525">1525</a>
<a href="#eglew.h-1526">1526</a>
<a href="#eglew.h-1527">1527</a>
<a href="#eglew.h-1528">1528</a>
<a href="#eglew.h-1529">1529</a>
<a href="#eglew.h-1530">1530</a>
<a href="#eglew.h-1531">1531</a>
<a href="#eglew.h-1532">1532</a>
<a href="#eglew.h-1533">1533</a>
<a href="#eglew.h-1534">1534</a>
<a href="#eglew.h-1535">1535</a>
<a href="#eglew.h-1536">1536</a>
<a href="#eglew.h-1537">1537</a>
<a href="#eglew.h-1538">1538</a>
<a href="#eglew.h-1539">1539</a>
<a href="#eglew.h-1540">1540</a>
<a href="#eglew.h-1541">1541</a>
<a href="#eglew.h-1542">1542</a>
<a href="#eglew.h-1543">1543</a>
<a href="#eglew.h-1544">1544</a>
<a href="#eglew.h-1545">1545</a>
<a href="#eglew.h-1546">1546</a>
<a href="#eglew.h-1547">1547</a>
<a href="#eglew.h-1548">1548</a>
<a href="#eglew.h-1549">1549</a>
<a href="#eglew.h-1550">1550</a>
<a href="#eglew.h-1551">1551</a>
<a href="#eglew.h-1552">1552</a>
<a href="#eglew.h-1553">1553</a>
<a href="#eglew.h-1554">1554</a>
<a href="#eglew.h-1555">1555</a>
<a href="#eglew.h-1556">1556</a>
<a href="#eglew.h-1557">1557</a>
<a href="#eglew.h-1558">1558</a>
<a href="#eglew.h-1559">1559</a>
<a href="#eglew.h-1560">1560</a>
<a href="#eglew.h-1561">1561</a>
<a href="#eglew.h-1562">1562</a>
<a href="#eglew.h-1563">1563</a>
<a href="#eglew.h-1564">1564</a>
<a href="#eglew.h-1565">1565</a>
<a href="#eglew.h-1566">1566</a>
<a href="#eglew.h-1567">1567</a>
<a href="#eglew.h-1568">1568</a>
<a href="#eglew.h-1569">1569</a>
<a href="#eglew.h-1570">1570</a>
<a href="#eglew.h-1571">1571</a>
<a href="#eglew.h-1572">1572</a>
<a href="#eglew.h-1573">1573</a>
<a href="#eglew.h-1574">1574</a>
<a href="#eglew.h-1575">1575</a>
<a href="#eglew.h-1576">1576</a>
<a href="#eglew.h-1577">1577</a>
<a href="#eglew.h-1578">1578</a>
<a href="#eglew.h-1579">1579</a>
<a href="#eglew.h-1580">1580</a>
<a href="#eglew.h-1581">1581</a>
<a href="#eglew.h-1582">1582</a>
<a href="#eglew.h-1583">1583</a>
<a href="#eglew.h-1584">1584</a>
<a href="#eglew.h-1585">1585</a>
<a href="#eglew.h-1586">1586</a>
<a href="#eglew.h-1587">1587</a>
<a href="#eglew.h-1588">1588</a>
<a href="#eglew.h-1589">1589</a>
<a href="#eglew.h-1590">1590</a>
<a href="#eglew.h-1591">1591</a>
<a href="#eglew.h-1592">1592</a>
<a href="#eglew.h-1593">1593</a>
<a href="#eglew.h-1594">1594</a>
<a href="#eglew.h-1595">1595</a>
<a href="#eglew.h-1596">1596</a>
<a href="#eglew.h-1597">1597</a>
<a href="#eglew.h-1598">1598</a>
<a href="#eglew.h-1599">1599</a>
<a href="#eglew.h-1600">1600</a>
<a href="#eglew.h-1601">1601</a>
<a href="#eglew.h-1602">1602</a>
<a href="#eglew.h-1603">1603</a>
<a href="#eglew.h-1604">1604</a>
<a href="#eglew.h-1605">1605</a>
<a href="#eglew.h-1606">1606</a>
<a href="#eglew.h-1607">1607</a>
<a href="#eglew.h-1608">1608</a>
<a href="#eglew.h-1609">1609</a>
<a href="#eglew.h-1610">1610</a>
<a href="#eglew.h-1611">1611</a>
<a href="#eglew.h-1612">1612</a>
<a href="#eglew.h-1613">1613</a>
<a href="#eglew.h-1614">1614</a>
<a href="#eglew.h-1615">1615</a>
<a href="#eglew.h-1616">1616</a>
<a href="#eglew.h-1617">1617</a>
<a href="#eglew.h-1618">1618</a>
<a href="#eglew.h-1619">1619</a>
<a href="#eglew.h-1620">1620</a>
<a href="#eglew.h-1621">1621</a>
<a href="#eglew.h-1622">1622</a>
<a href="#eglew.h-1623">1623</a>
<a href="#eglew.h-1624">1624</a>
<a href="#eglew.h-1625">1625</a>
<a href="#eglew.h-1626">1626</a>
<a href="#eglew.h-1627">1627</a>
<a href="#eglew.h-1628">1628</a>
<a href="#eglew.h-1629">1629</a>
<a href="#eglew.h-1630">1630</a>
<a href="#eglew.h-1631">1631</a>
<a href="#eglew.h-1632">1632</a>
<a href="#eglew.h-1633">1633</a>
<a href="#eglew.h-1634">1634</a>
<a href="#eglew.h-1635">1635</a>
<a href="#eglew.h-1636">1636</a>
<a href="#eglew.h-1637">1637</a>
<a href="#eglew.h-1638">1638</a>
<a href="#eglew.h-1639">1639</a>
<a href="#eglew.h-1640">1640</a>
<a href="#eglew.h-1641">1641</a>
<a href="#eglew.h-1642">1642</a>
<a href="#eglew.h-1643">1643</a>
<a href="#eglew.h-1644">1644</a>
<a href="#eglew.h-1645">1645</a>
<a href="#eglew.h-1646">1646</a>
<a href="#eglew.h-1647">1647</a>
<a href="#eglew.h-1648">1648</a>
<a href="#eglew.h-1649">1649</a>
<a href="#eglew.h-1650">1650</a>
<a href="#eglew.h-1651">1651</a>
<a href="#eglew.h-1652">1652</a>
<a href="#eglew.h-1653">1653</a>
<a href="#eglew.h-1654">1654</a>
<a href="#eglew.h-1655">1655</a>
<a href="#eglew.h-1656">1656</a>
<a href="#eglew.h-1657">1657</a>
<a href="#eglew.h-1658">1658</a>
<a href="#eglew.h-1659">1659</a>
<a href="#eglew.h-1660">1660</a>
<a href="#eglew.h-1661">1661</a>
<a href="#eglew.h-1662">1662</a>
<a href="#eglew.h-1663">1663</a>
<a href="#eglew.h-1664">1664</a>
<a href="#eglew.h-1665">1665</a>
<a href="#eglew.h-1666">1666</a>
<a href="#eglew.h-1667">1667</a>
<a href="#eglew.h-1668">1668</a>
<a href="#eglew.h-1669">1669</a>
<a href="#eglew.h-1670">1670</a>
<a href="#eglew.h-1671">1671</a>
<a href="#eglew.h-1672">1672</a>
<a href="#eglew.h-1673">1673</a>
<a href="#eglew.h-1674">1674</a>
<a href="#eglew.h-1675">1675</a>
<a href="#eglew.h-1676">1676</a>
<a href="#eglew.h-1677">1677</a>
<a href="#eglew.h-1678">1678</a>
<a href="#eglew.h-1679">1679</a>
<a href="#eglew.h-1680">1680</a>
<a href="#eglew.h-1681">1681</a>
<a href="#eglew.h-1682">1682</a>
<a href="#eglew.h-1683">1683</a>
<a href="#eglew.h-1684">1684</a>
<a href="#eglew.h-1685">1685</a>
<a href="#eglew.h-1686">1686</a>
<a href="#eglew.h-1687">1687</a>
<a href="#eglew.h-1688">1688</a>
<a href="#eglew.h-1689">1689</a>
<a href="#eglew.h-1690">1690</a>
<a href="#eglew.h-1691">1691</a>
<a href="#eglew.h-1692">1692</a>
<a href="#eglew.h-1693">1693</a>
<a href="#eglew.h-1694">1694</a>
<a href="#eglew.h-1695">1695</a>
<a href="#eglew.h-1696">1696</a>
<a href="#eglew.h-1697">1697</a>
<a href="#eglew.h-1698">1698</a>
<a href="#eglew.h-1699">1699</a>
<a href="#eglew.h-1700">1700</a>
<a href="#eglew.h-1701">1701</a>
<a href="#eglew.h-1702">1702</a>
<a href="#eglew.h-1703">1703</a>
<a href="#eglew.h-1704">1704</a>
<a href="#eglew.h-1705">1705</a>
<a href="#eglew.h-1706">1706</a>
<a href="#eglew.h-1707">1707</a>
<a href="#eglew.h-1708">1708</a>
<a href="#eglew.h-1709">1709</a>
<a href="#eglew.h-1710">1710</a>
<a href="#eglew.h-1711">1711</a>
<a href="#eglew.h-1712">1712</a>
<a href="#eglew.h-1713">1713</a>
<a href="#eglew.h-1714">1714</a>
<a href="#eglew.h-1715">1715</a>
<a href="#eglew.h-1716">1716</a>
<a href="#eglew.h-1717">1717</a>
<a href="#eglew.h-1718">1718</a>
<a href="#eglew.h-1719">1719</a>
<a href="#eglew.h-1720">1720</a>
<a href="#eglew.h-1721">1721</a>
<a href="#eglew.h-1722">1722</a>
<a href="#eglew.h-1723">1723</a>
<a href="#eglew.h-1724">1724</a>
<a href="#eglew.h-1725">1725</a>
<a href="#eglew.h-1726">1726</a>
<a href="#eglew.h-1727">1727</a>
<a href="#eglew.h-1728">1728</a>
<a href="#eglew.h-1729">1729</a>
<a href="#eglew.h-1730">1730</a>
<a href="#eglew.h-1731">1731</a>
<a href="#eglew.h-1732">1732</a>
<a href="#eglew.h-1733">1733</a>
<a href="#eglew.h-1734">1734</a>
<a href="#eglew.h-1735">1735</a>
<a href="#eglew.h-1736">1736</a>
<a href="#eglew.h-1737">1737</a>
<a href="#eglew.h-1738">1738</a>
<a href="#eglew.h-1739">1739</a>
<a href="#eglew.h-1740">1740</a>
<a href="#eglew.h-1741">1741</a>
<a href="#eglew.h-1742">1742</a>
<a href="#eglew.h-1743">1743</a>
<a href="#eglew.h-1744">1744</a>
<a href="#eglew.h-1745">1745</a>
<a href="#eglew.h-1746">1746</a>
<a href="#eglew.h-1747">1747</a>
<a href="#eglew.h-1748">1748</a>
<a href="#eglew.h-1749">1749</a>
<a href="#eglew.h-1750">1750</a>
<a href="#eglew.h-1751">1751</a>
<a href="#eglew.h-1752">1752</a>
<a href="#eglew.h-1753">1753</a>
<a href="#eglew.h-1754">1754</a>
<a href="#eglew.h-1755">1755</a>
<a href="#eglew.h-1756">1756</a>
<a href="#eglew.h-1757">1757</a>
<a href="#eglew.h-1758">1758</a>
<a href="#eglew.h-1759">1759</a>
<a href="#eglew.h-1760">1760</a>
<a href="#eglew.h-1761">1761</a>
<a href="#eglew.h-1762">1762</a>
<a href="#eglew.h-1763">1763</a>
<a href="#eglew.h-1764">1764</a>
<a href="#eglew.h-1765">1765</a>
<a href="#eglew.h-1766">1766</a>
<a href="#eglew.h-1767">1767</a>
<a href="#eglew.h-1768">1768</a>
<a href="#eglew.h-1769">1769</a>
<a href="#eglew.h-1770">1770</a>
<a href="#eglew.h-1771">1771</a>
<a href="#eglew.h-1772">1772</a>
<a href="#eglew.h-1773">1773</a>
<a href="#eglew.h-1774">1774</a>
<a href="#eglew.h-1775">1775</a>
<a href="#eglew.h-1776">1776</a>
<a href="#eglew.h-1777">1777</a>
<a href="#eglew.h-1778">1778</a>
<a href="#eglew.h-1779">1779</a>
<a href="#eglew.h-1780">1780</a>
<a href="#eglew.h-1781">1781</a>
<a href="#eglew.h-1782">1782</a>
<a href="#eglew.h-1783">1783</a>
<a href="#eglew.h-1784">1784</a>
<a href="#eglew.h-1785">1785</a>
<a href="#eglew.h-1786">1786</a>
<a href="#eglew.h-1787">1787</a>
<a href="#eglew.h-1788">1788</a>
<a href="#eglew.h-1789">1789</a>
<a href="#eglew.h-1790">1790</a>
<a href="#eglew.h-1791">1791</a>
<a href="#eglew.h-1792">1792</a>
<a href="#eglew.h-1793">1793</a>
<a href="#eglew.h-1794">1794</a>
<a href="#eglew.h-1795">1795</a>
<a href="#eglew.h-1796">1796</a>
<a href="#eglew.h-1797">1797</a>
<a href="#eglew.h-1798">1798</a>
<a href="#eglew.h-1799">1799</a>
<a href="#eglew.h-1800">1800</a>
<a href="#eglew.h-1801">1801</a>
<a href="#eglew.h-1802">1802</a>
<a href="#eglew.h-1803">1803</a>
<a href="#eglew.h-1804">1804</a>
<a href="#eglew.h-1805">1805</a>
<a href="#eglew.h-1806">1806</a>
<a href="#eglew.h-1807">1807</a>
<a href="#eglew.h-1808">1808</a>
<a href="#eglew.h-1809">1809</a>
<a href="#eglew.h-1810">1810</a>
<a href="#eglew.h-1811">1811</a>
<a href="#eglew.h-1812">1812</a>
<a href="#eglew.h-1813">1813</a>
<a href="#eglew.h-1814">1814</a>
<a href="#eglew.h-1815">1815</a>
<a href="#eglew.h-1816">1816</a>
<a href="#eglew.h-1817">1817</a>
<a href="#eglew.h-1818">1818</a>
<a href="#eglew.h-1819">1819</a>
<a href="#eglew.h-1820">1820</a>
<a href="#eglew.h-1821">1821</a>
<a href="#eglew.h-1822">1822</a>
<a href="#eglew.h-1823">1823</a>
<a href="#eglew.h-1824">1824</a>
<a href="#eglew.h-1825">1825</a>
<a href="#eglew.h-1826">1826</a>
<a href="#eglew.h-1827">1827</a>
<a href="#eglew.h-1828">1828</a>
<a href="#eglew.h-1829">1829</a>
<a href="#eglew.h-1830">1830</a>
<a href="#eglew.h-1831">1831</a>
<a href="#eglew.h-1832">1832</a>
<a href="#eglew.h-1833">1833</a>
<a href="#eglew.h-1834">1834</a>
<a href="#eglew.h-1835">1835</a>
<a href="#eglew.h-1836">1836</a>
<a href="#eglew.h-1837">1837</a>
<a href="#eglew.h-1838">1838</a>
<a href="#eglew.h-1839">1839</a>
<a href="#eglew.h-1840">1840</a>
<a href="#eglew.h-1841">1841</a>
<a href="#eglew.h-1842">1842</a>
<a href="#eglew.h-1843">1843</a>
<a href="#eglew.h-1844">1844</a>
<a href="#eglew.h-1845">1845</a>
<a href="#eglew.h-1846">1846</a>
<a href="#eglew.h-1847">1847</a>
<a href="#eglew.h-1848">1848</a>
<a href="#eglew.h-1849">1849</a>
<a href="#eglew.h-1850">1850</a>
<a href="#eglew.h-1851">1851</a>
<a href="#eglew.h-1852">1852</a>
<a href="#eglew.h-1853">1853</a>
<a href="#eglew.h-1854">1854</a>
<a href="#eglew.h-1855">1855</a>
<a href="#eglew.h-1856">1856</a>
<a href="#eglew.h-1857">1857</a>
<a href="#eglew.h-1858">1858</a>
<a href="#eglew.h-1859">1859</a>
<a href="#eglew.h-1860">1860</a>
<a href="#eglew.h-1861">1861</a>
<a href="#eglew.h-1862">1862</a>
<a href="#eglew.h-1863">1863</a>
<a href="#eglew.h-1864">1864</a>
<a href="#eglew.h-1865">1865</a>
<a href="#eglew.h-1866">1866</a>
<a href="#eglew.h-1867">1867</a>
<a href="#eglew.h-1868">1868</a>
<a href="#eglew.h-1869">1869</a>
<a href="#eglew.h-1870">1870</a>
<a href="#eglew.h-1871">1871</a>
<a href="#eglew.h-1872">1872</a>
<a href="#eglew.h-1873">1873</a>
<a href="#eglew.h-1874">1874</a>
<a href="#eglew.h-1875">1875</a>
<a href="#eglew.h-1876">1876</a>
<a href="#eglew.h-1877">1877</a>
<a href="#eglew.h-1878">1878</a>
<a href="#eglew.h-1879">1879</a>
<a href="#eglew.h-1880">1880</a>
<a href="#eglew.h-1881">1881</a>
<a href="#eglew.h-1882">1882</a>
<a href="#eglew.h-1883">1883</a>
<a href="#eglew.h-1884">1884</a>
<a href="#eglew.h-1885">1885</a>
<a href="#eglew.h-1886">1886</a>
<a href="#eglew.h-1887">1887</a>
<a href="#eglew.h-1888">1888</a>
<a href="#eglew.h-1889">1889</a>
<a href="#eglew.h-1890">1890</a>
<a href="#eglew.h-1891">1891</a>
<a href="#eglew.h-1892">1892</a>
<a href="#eglew.h-1893">1893</a>
<a href="#eglew.h-1894">1894</a>
<a href="#eglew.h-1895">1895</a>
<a href="#eglew.h-1896">1896</a>
<a href="#eglew.h-1897">1897</a>
<a href="#eglew.h-1898">1898</a>
<a href="#eglew.h-1899">1899</a>
<a href="#eglew.h-1900">1900</a>
<a href="#eglew.h-1901">1901</a>
<a href="#eglew.h-1902">1902</a>
<a href="#eglew.h-1903">1903</a>
<a href="#eglew.h-1904">1904</a>
<a href="#eglew.h-1905">1905</a>
<a href="#eglew.h-1906">1906</a>
<a href="#eglew.h-1907">1907</a>
<a href="#eglew.h-1908">1908</a>
<a href="#eglew.h-1909">1909</a>
<a href="#eglew.h-1910">1910</a>
<a href="#eglew.h-1911">1911</a>
<a href="#eglew.h-1912">1912</a>
<a href="#eglew.h-1913">1913</a>
<a href="#eglew.h-1914">1914</a>
<a href="#eglew.h-1915">1915</a>
<a href="#eglew.h-1916">1916</a>
<a href="#eglew.h-1917">1917</a>
<a href="#eglew.h-1918">1918</a>
<a href="#eglew.h-1919">1919</a>
<a href="#eglew.h-1920">1920</a>
<a href="#eglew.h-1921">1921</a>
<a href="#eglew.h-1922">1922</a>
<a href="#eglew.h-1923">1923</a>
<a href="#eglew.h-1924">1924</a>
<a href="#eglew.h-1925">1925</a>
<a href="#eglew.h-1926">1926</a>
<a href="#eglew.h-1927">1927</a>
<a href="#eglew.h-1928">1928</a>
<a href="#eglew.h-1929">1929</a>
<a href="#eglew.h-1930">1930</a>
<a href="#eglew.h-1931">1931</a>
<a href="#eglew.h-1932">1932</a>
<a href="#eglew.h-1933">1933</a>
<a href="#eglew.h-1934">1934</a>
<a href="#eglew.h-1935">1935</a>
<a href="#eglew.h-1936">1936</a>
<a href="#eglew.h-1937">1937</a>
<a href="#eglew.h-1938">1938</a>
<a href="#eglew.h-1939">1939</a>
<a href="#eglew.h-1940">1940</a>
<a href="#eglew.h-1941">1941</a>
<a href="#eglew.h-1942">1942</a>
<a href="#eglew.h-1943">1943</a>
<a href="#eglew.h-1944">1944</a>
<a href="#eglew.h-1945">1945</a>
<a href="#eglew.h-1946">1946</a>
<a href="#eglew.h-1947">1947</a>
<a href="#eglew.h-1948">1948</a>
<a href="#eglew.h-1949">1949</a>
<a href="#eglew.h-1950">1950</a>
<a href="#eglew.h-1951">1951</a>
<a href="#eglew.h-1952">1952</a>
<a href="#eglew.h-1953">1953</a>
<a href="#eglew.h-1954">1954</a>
<a href="#eglew.h-1955">1955</a>
<a href="#eglew.h-1956">1956</a>
<a href="#eglew.h-1957">1957</a>
<a href="#eglew.h-1958">1958</a>
<a href="#eglew.h-1959">1959</a>
<a href="#eglew.h-1960">1960</a>
<a href="#eglew.h-1961">1961</a>
<a href="#eglew.h-1962">1962</a>
<a href="#eglew.h-1963">1963</a>
<a href="#eglew.h-1964">1964</a>
<a href="#eglew.h-1965">1965</a>
<a href="#eglew.h-1966">1966</a>
<a href="#eglew.h-1967">1967</a>
<a href="#eglew.h-1968">1968</a>
<a href="#eglew.h-1969">1969</a>
<a href="#eglew.h-1970">1970</a>
<a href="#eglew.h-1971">1971</a>
<a href="#eglew.h-1972">1972</a>
<a href="#eglew.h-1973">1973</a>
<a href="#eglew.h-1974">1974</a>
<a href="#eglew.h-1975">1975</a>
<a href="#eglew.h-1976">1976</a>
<a href="#eglew.h-1977">1977</a>
<a href="#eglew.h-1978">1978</a>
<a href="#eglew.h-1979">1979</a>
<a href="#eglew.h-1980">1980</a>
<a href="#eglew.h-1981">1981</a>
<a href="#eglew.h-1982">1982</a>
<a href="#eglew.h-1983">1983</a>
<a href="#eglew.h-1984">1984</a>
<a href="#eglew.h-1985">1985</a>
<a href="#eglew.h-1986">1986</a>
<a href="#eglew.h-1987">1987</a>
<a href="#eglew.h-1988">1988</a>
<a href="#eglew.h-1989">1989</a>
<a href="#eglew.h-1990">1990</a>
<a href="#eglew.h-1991">1991</a>
<a href="#eglew.h-1992">1992</a>
<a href="#eglew.h-1993">1993</a>
<a href="#eglew.h-1994">1994</a>
<a href="#eglew.h-1995">1995</a>
<a href="#eglew.h-1996">1996</a>
<a href="#eglew.h-1997">1997</a>
<a href="#eglew.h-1998">1998</a>
<a href="#eglew.h-1999">1999</a>
<a href="#eglew.h-2000">2000</a>
<a href="#eglew.h-2001">2001</a>
<a href="#eglew.h-2002">2002</a>
<a href="#eglew.h-2003">2003</a>
<a href="#eglew.h-2004">2004</a>
<a href="#eglew.h-2005">2005</a>
<a href="#eglew.h-2006">2006</a>
<a href="#eglew.h-2007">2007</a>
<a href="#eglew.h-2008">2008</a>
<a href="#eglew.h-2009">2009</a>
<a href="#eglew.h-2010">2010</a>
<a href="#eglew.h-2011">2011</a>
<a href="#eglew.h-2012">2012</a>
<a href="#eglew.h-2013">2013</a>
<a href="#eglew.h-2014">2014</a>
<a href="#eglew.h-2015">2015</a>
<a href="#eglew.h-2016">2016</a>
<a href="#eglew.h-2017">2017</a>
<a href="#eglew.h-2018">2018</a>
<a href="#eglew.h-2019">2019</a>
<a href="#eglew.h-2020">2020</a>
<a href="#eglew.h-2021">2021</a>
<a href="#eglew.h-2022">2022</a>
<a href="#eglew.h-2023">2023</a>
<a href="#eglew.h-2024">2024</a>
<a href="#eglew.h-2025">2025</a>
<a href="#eglew.h-2026">2026</a>
<a href="#eglew.h-2027">2027</a>
<a href="#eglew.h-2028">2028</a>
<a href="#eglew.h-2029">2029</a>
<a href="#eglew.h-2030">2030</a>
<a href="#eglew.h-2031">2031</a>
<a href="#eglew.h-2032">2032</a>
<a href="#eglew.h-2033">2033</a>
<a href="#eglew.h-2034">2034</a>
<a href="#eglew.h-2035">2035</a>
<a href="#eglew.h-2036">2036</a>
<a href="#eglew.h-2037">2037</a>
<a href="#eglew.h-2038">2038</a>
<a href="#eglew.h-2039">2039</a>
<a href="#eglew.h-2040">2040</a>
<a href="#eglew.h-2041">2041</a>
<a href="#eglew.h-2042">2042</a>
<a href="#eglew.h-2043">2043</a>
<a href="#eglew.h-2044">2044</a>
<a href="#eglew.h-2045">2045</a>
<a href="#eglew.h-2046">2046</a>
<a href="#eglew.h-2047">2047</a>
<a href="#eglew.h-2048">2048</a>
<a href="#eglew.h-2049">2049</a>
<a href="#eglew.h-2050">2050</a>
<a href="#eglew.h-2051">2051</a>
<a href="#eglew.h-2052">2052</a>
<a href="#eglew.h-2053">2053</a>
<a href="#eglew.h-2054">2054</a>
<a href="#eglew.h-2055">2055</a>
<a href="#eglew.h-2056">2056</a>
<a href="#eglew.h-2057">2057</a>
<a href="#eglew.h-2058">2058</a>
<a href="#eglew.h-2059">2059</a>
<a href="#eglew.h-2060">2060</a>
<a href="#eglew.h-2061">2061</a>
<a href="#eglew.h-2062">2062</a>
<a href="#eglew.h-2063">2063</a>
<a href="#eglew.h-2064">2064</a>
<a href="#eglew.h-2065">2065</a>
<a href="#eglew.h-2066">2066</a>
<a href="#eglew.h-2067">2067</a>
<a href="#eglew.h-2068">2068</a>
<a href="#eglew.h-2069">2069</a>
<a href="#eglew.h-2070">2070</a>
<a href="#eglew.h-2071">2071</a>
<a href="#eglew.h-2072">2072</a>
<a href="#eglew.h-2073">2073</a>
<a href="#eglew.h-2074">2074</a>
<a href="#eglew.h-2075">2075</a>
<a href="#eglew.h-2076">2076</a>
<a href="#eglew.h-2077">2077</a>
<a href="#eglew.h-2078">2078</a>
<a href="#eglew.h-2079">2079</a>
<a href="#eglew.h-2080">2080</a>
<a href="#eglew.h-2081">2081</a>
<a href="#eglew.h-2082">2082</a>
<a href="#eglew.h-2083">2083</a>
<a href="#eglew.h-2084">2084</a>
<a href="#eglew.h-2085">2085</a>
<a href="#eglew.h-2086">2086</a>
<a href="#eglew.h-2087">2087</a>
<a href="#eglew.h-2088">2088</a>
<a href="#eglew.h-2089">2089</a>
<a href="#eglew.h-2090">2090</a>
<a href="#eglew.h-2091">2091</a>
<a href="#eglew.h-2092">2092</a>
<a href="#eglew.h-2093">2093</a>
<a href="#eglew.h-2094">2094</a>
<a href="#eglew.h-2095">2095</a>
<a href="#eglew.h-2096">2096</a>
<a href="#eglew.h-2097">2097</a>
<a href="#eglew.h-2098">2098</a>
<a href="#eglew.h-2099">2099</a>
<a href="#eglew.h-2100">2100</a>
<a href="#eglew.h-2101">2101</a>
<a href="#eglew.h-2102">2102</a>
<a href="#eglew.h-2103">2103</a>
<a href="#eglew.h-2104">2104</a>
<a href="#eglew.h-2105">2105</a>
<a href="#eglew.h-2106">2106</a>
<a href="#eglew.h-2107">2107</a>
<a href="#eglew.h-2108">2108</a>
<a href="#eglew.h-2109">2109</a>
<a href="#eglew.h-2110">2110</a>
<a href="#eglew.h-2111">2111</a>
<a href="#eglew.h-2112">2112</a>
<a href="#eglew.h-2113">2113</a>
<a href="#eglew.h-2114">2114</a>
<a href="#eglew.h-2115">2115</a>
<a href="#eglew.h-2116">2116</a>
<a href="#eglew.h-2117">2117</a>
<a href="#eglew.h-2118">2118</a>
<a href="#eglew.h-2119">2119</a>
<a href="#eglew.h-2120">2120</a>
<a href="#eglew.h-2121">2121</a>
<a href="#eglew.h-2122">2122</a>
<a href="#eglew.h-2123">2123</a>
<a href="#eglew.h-2124">2124</a>
<a href="#eglew.h-2125">2125</a>
<a href="#eglew.h-2126">2126</a>
<a href="#eglew.h-2127">2127</a>
<a href="#eglew.h-2128">2128</a>
<a href="#eglew.h-2129">2129</a>
<a href="#eglew.h-2130">2130</a>
<a href="#eglew.h-2131">2131</a>
<a href="#eglew.h-2132">2132</a>
<a href="#eglew.h-2133">2133</a>
<a href="#eglew.h-2134">2134</a>
<a href="#eglew.h-2135">2135</a>
<a href="#eglew.h-2136">2136</a>
<a href="#eglew.h-2137">2137</a>
<a href="#eglew.h-2138">2138</a>
<a href="#eglew.h-2139">2139</a>
<a href="#eglew.h-2140">2140</a>
<a href="#eglew.h-2141">2141</a>
<a href="#eglew.h-2142">2142</a>
<a href="#eglew.h-2143">2143</a>
<a href="#eglew.h-2144">2144</a>
<a href="#eglew.h-2145">2145</a>
<a href="#eglew.h-2146">2146</a>
<a href="#eglew.h-2147">2147</a>
<a href="#eglew.h-2148">2148</a>
<a href="#eglew.h-2149">2149</a>
<a href="#eglew.h-2150">2150</a>
<a href="#eglew.h-2151">2151</a>
<a href="#eglew.h-2152">2152</a>
<a href="#eglew.h-2153">2153</a>
<a href="#eglew.h-2154">2154</a>
<a href="#eglew.h-2155">2155</a>
<a href="#eglew.h-2156">2156</a>
<a href="#eglew.h-2157">2157</a>
<a href="#eglew.h-2158">2158</a>
<a href="#eglew.h-2159">2159</a>
<a href="#eglew.h-2160">2160</a>
<a href="#eglew.h-2161">2161</a>
<a href="#eglew.h-2162">2162</a>
<a href="#eglew.h-2163">2163</a>
<a href="#eglew.h-2164">2164</a>
<a href="#eglew.h-2165">2165</a>
<a href="#eglew.h-2166">2166</a>
<a href="#eglew.h-2167">2167</a>
<a href="#eglew.h-2168">2168</a>
<a href="#eglew.h-2169">2169</a>
<a href="#eglew.h-2170">2170</a>
<a href="#eglew.h-2171">2171</a>
<a href="#eglew.h-2172">2172</a>
<a href="#eglew.h-2173">2173</a>
<a href="#eglew.h-2174">2174</a>
<a href="#eglew.h-2175">2175</a>
<a href="#eglew.h-2176">2176</a>
<a href="#eglew.h-2177">2177</a>
<a href="#eglew.h-2178">2178</a>
<a href="#eglew.h-2179">2179</a>
<a href="#eglew.h-2180">2180</a>
<a href="#eglew.h-2181">2181</a>
<a href="#eglew.h-2182">2182</a>
<a href="#eglew.h-2183">2183</a>
<a href="#eglew.h-2184">2184</a>
<a href="#eglew.h-2185">2185</a>
<a href="#eglew.h-2186">2186</a>
<a href="#eglew.h-2187">2187</a>
<a href="#eglew.h-2188">2188</a>
<a href="#eglew.h-2189">2189</a>
<a href="#eglew.h-2190">2190</a>
<a href="#eglew.h-2191">2191</a>
<a href="#eglew.h-2192">2192</a>
<a href="#eglew.h-2193">2193</a>
<a href="#eglew.h-2194">2194</a>
<a href="#eglew.h-2195">2195</a>
<a href="#eglew.h-2196">2196</a>
<a href="#eglew.h-2197">2197</a>
<a href="#eglew.h-2198">2198</a>
<a href="#eglew.h-2199">2199</a>
<a href="#eglew.h-2200">2200</a>
<a href="#eglew.h-2201">2201</a>
<a href="#eglew.h-2202">2202</a>
<a href="#eglew.h-2203">2203</a>
<a href="#eglew.h-2204">2204</a>
<a href="#eglew.h-2205">2205</a>
<a href="#eglew.h-2206">2206</a>
<a href="#eglew.h-2207">2207</a>
<a href="#eglew.h-2208">2208</a>
<a href="#eglew.h-2209">2209</a>
<a href="#eglew.h-2210">2210</a>
<a href="#eglew.h-2211">2211</a>
<a href="#eglew.h-2212">2212</a>
<a href="#eglew.h-2213">2213</a>
<a href="#eglew.h-2214">2214</a>
<a href="#eglew.h-2215">2215</a>
<a href="#eglew.h-2216">2216</a>
<a href="#eglew.h-2217">2217</a>
<a href="#eglew.h-2218">2218</a>
<a href="#eglew.h-2219">2219</a>
<a href="#eglew.h-2220">2220</a>
<a href="#eglew.h-2221">2221</a>
<a href="#eglew.h-2222">2222</a>
<a href="#eglew.h-2223">2223</a>
<a href="#eglew.h-2224">2224</a>
<a href="#eglew.h-2225">2225</a>
<a href="#eglew.h-2226">2226</a>
<a href="#eglew.h-2227">2227</a>
<a href="#eglew.h-2228">2228</a>
<a href="#eglew.h-2229">2229</a>
<a href="#eglew.h-2230">2230</a>
<a href="#eglew.h-2231">2231</a>
<a href="#eglew.h-2232">2232</a>
<a href="#eglew.h-2233">2233</a>
<a href="#eglew.h-2234">2234</a>
<a href="#eglew.h-2235">2235</a>
<a href="#eglew.h-2236">2236</a>
<a href="#eglew.h-2237">2237</a>
<a href="#eglew.h-2238">2238</a>
<a href="#eglew.h-2239">2239</a>
<a href="#eglew.h-2240">2240</a>
<a href="#eglew.h-2241">2241</a>
<a href="#eglew.h-2242">2242</a>
<a href="#eglew.h-2243">2243</a>
<a href="#eglew.h-2244">2244</a>
<a href="#eglew.h-2245">2245</a>
<a href="#eglew.h-2246">2246</a>
<a href="#eglew.h-2247">2247</a>
<a href="#eglew.h-2248">2248</a>
<a href="#eglew.h-2249">2249</a>
<a href="#eglew.h-2250">2250</a>
<a href="#eglew.h-2251">2251</a>
<a href="#eglew.h-2252">2252</a>
<a href="#eglew.h-2253">2253</a>
<a href="#eglew.h-2254">2254</a>
<a href="#eglew.h-2255">2255</a>
<a href="#eglew.h-2256">2256</a>
<a href="#eglew.h-2257">2257</a>
<a href="#eglew.h-2258">2258</a>
<a href="#eglew.h-2259">2259</a>
<a href="#eglew.h-2260">2260</a>
<a href="#eglew.h-2261">2261</a>
<a href="#eglew.h-2262">2262</a>
<a href="#eglew.h-2263">2263</a>
<a href="#eglew.h-2264">2264</a>
<a href="#eglew.h-2265">2265</a>
<a href="#eglew.h-2266">2266</a>
<a href="#eglew.h-2267">2267</a>
<a href="#eglew.h-2268">2268</a>
<a href="#eglew.h-2269">2269</a>
<a href="#eglew.h-2270">2270</a>
<a href="#eglew.h-2271">2271</a>
<a href="#eglew.h-2272">2272</a>
<a href="#eglew.h-2273">2273</a>
<a href="#eglew.h-2274">2274</a>
<a href="#eglew.h-2275">2275</a>
<a href="#eglew.h-2276">2276</a>
<a href="#eglew.h-2277">2277</a>
<a href="#eglew.h-2278">2278</a>
<a href="#eglew.h-2279">2279</a>
<a href="#eglew.h-2280">2280</a>
<a href="#eglew.h-2281">2281</a>
<a href="#eglew.h-2282">2282</a>
<a href="#eglew.h-2283">2283</a>
<a href="#eglew.h-2284">2284</a>
<a href="#eglew.h-2285">2285</a>
<a href="#eglew.h-2286">2286</a>
<a href="#eglew.h-2287">2287</a>
<a href="#eglew.h-2288">2288</a>
<a href="#eglew.h-2289">2289</a>
<a href="#eglew.h-2290">2290</a>
<a href="#eglew.h-2291">2291</a>
<a href="#eglew.h-2292">2292</a>
<a href="#eglew.h-2293">2293</a>
<a href="#eglew.h-2294">2294</a>
<a href="#eglew.h-2295">2295</a>
<a href="#eglew.h-2296">2296</a>
<a href="#eglew.h-2297">2297</a>
<a href="#eglew.h-2298">2298</a>
<a href="#eglew.h-2299">2299</a>
<a href="#eglew.h-2300">2300</a>
<a href="#eglew.h-2301">2301</a>
<a href="#eglew.h-2302">2302</a>
<a href="#eglew.h-2303">2303</a>
<a href="#eglew.h-2304">2304</a>
<a href="#eglew.h-2305">2305</a>
<a href="#eglew.h-2306">2306</a>
<a href="#eglew.h-2307">2307</a>
<a href="#eglew.h-2308">2308</a>
<a href="#eglew.h-2309">2309</a>
<a href="#eglew.h-2310">2310</a>
<a href="#eglew.h-2311">2311</a>
<a href="#eglew.h-2312">2312</a>
<a href="#eglew.h-2313">2313</a>
<a href="#eglew.h-2314">2314</a>
<a href="#eglew.h-2315">2315</a>
<a href="#eglew.h-2316">2316</a>
<a href="#eglew.h-2317">2317</a>
<a href="#eglew.h-2318">2318</a>
<a href="#eglew.h-2319">2319</a>
<a href="#eglew.h-2320">2320</a>
<a href="#eglew.h-2321">2321</a>
<a href="#eglew.h-2322">2322</a>
<a href="#eglew.h-2323">2323</a>
<a href="#eglew.h-2324">2324</a>
<a href="#eglew.h-2325">2325</a>
<a href="#eglew.h-2326">2326</a>
<a href="#eglew.h-2327">2327</a>
<a href="#eglew.h-2328">2328</a>
<a href="#eglew.h-2329">2329</a>
<a href="#eglew.h-2330">2330</a>
<a href="#eglew.h-2331">2331</a>
<a href="#eglew.h-2332">2332</a>
<a href="#eglew.h-2333">2333</a>
<a href="#eglew.h-2334">2334</a>
<a href="#eglew.h-2335">2335</a>
<a href="#eglew.h-2336">2336</a>
<a href="#eglew.h-2337">2337</a>
<a href="#eglew.h-2338">2338</a>
<a href="#eglew.h-2339">2339</a>
<a href="#eglew.h-2340">2340</a>
<a href="#eglew.h-2341">2341</a>
<a href="#eglew.h-2342">2342</a>
<a href="#eglew.h-2343">2343</a>
<a href="#eglew.h-2344">2344</a>
<a href="#eglew.h-2345">2345</a>
<a href="#eglew.h-2346">2346</a>
<a href="#eglew.h-2347">2347</a>
<a href="#eglew.h-2348">2348</a>
<a href="#eglew.h-2349">2349</a>
<a href="#eglew.h-2350">2350</a>
<a href="#eglew.h-2351">2351</a>
<a href="#eglew.h-2352">2352</a>
<a href="#eglew.h-2353">2353</a>
<a href="#eglew.h-2354">2354</a>
<a href="#eglew.h-2355">2355</a>
<a href="#eglew.h-2356">2356</a>
<a href="#eglew.h-2357">2357</a>
<a href="#eglew.h-2358">2358</a>
<a href="#eglew.h-2359">2359</a>
<a href="#eglew.h-2360">2360</a>
<a href="#eglew.h-2361">2361</a>
<a href="#eglew.h-2362">2362</a>
<a href="#eglew.h-2363">2363</a>
<a href="#eglew.h-2364">2364</a>
<a href="#eglew.h-2365">2365</a>
<a href="#eglew.h-2366">2366</a>
<a href="#eglew.h-2367">2367</a>
<a href="#eglew.h-2368">2368</a>
<a href="#eglew.h-2369">2369</a>
<a href="#eglew.h-2370">2370</a>
<a href="#eglew.h-2371">2371</a>
<a href="#eglew.h-2372">2372</a>
<a href="#eglew.h-2373">2373</a>
<a href="#eglew.h-2374">2374</a>
<a href="#eglew.h-2375">2375</a>
<a href="#eglew.h-2376">2376</a>
<a href="#eglew.h-2377">2377</a>
<a href="#eglew.h-2378">2378</a>
<a href="#eglew.h-2379">2379</a>
<a href="#eglew.h-2380">2380</a>
<a href="#eglew.h-2381">2381</a>
<a href="#eglew.h-2382">2382</a>
<a href="#eglew.h-2383">2383</a>
<a href="#eglew.h-2384">2384</a>
<a href="#eglew.h-2385">2385</a>
<a href="#eglew.h-2386">2386</a>
<a href="#eglew.h-2387">2387</a>
<a href="#eglew.h-2388">2388</a>
<a href="#eglew.h-2389">2389</a>
<a href="#eglew.h-2390">2390</a>
<a href="#eglew.h-2391">2391</a>
<a href="#eglew.h-2392">2392</a>
<a href="#eglew.h-2393">2393</a>
<a href="#eglew.h-2394">2394</a>
<a href="#eglew.h-2395">2395</a>
<a href="#eglew.h-2396">2396</a>
<a href="#eglew.h-2397">2397</a>
<a href="#eglew.h-2398">2398</a>
<a href="#eglew.h-2399">2399</a>
<a href="#eglew.h-2400">2400</a>
<a href="#eglew.h-2401">2401</a>
<a href="#eglew.h-2402">2402</a>
<a href="#eglew.h-2403">2403</a>
<a href="#eglew.h-2404">2404</a>
<a href="#eglew.h-2405">2405</a>
<a href="#eglew.h-2406">2406</a>
<a href="#eglew.h-2407">2407</a>
<a href="#eglew.h-2408">2408</a>
<a href="#eglew.h-2409">2409</a>
<a href="#eglew.h-2410">2410</a>
<a href="#eglew.h-2411">2411</a>
<a href="#eglew.h-2412">2412</a>
<a href="#eglew.h-2413">2413</a>
<a href="#eglew.h-2414">2414</a>
<a href="#eglew.h-2415">2415</a>
<a href="#eglew.h-2416">2416</a>
<a href="#eglew.h-2417">2417</a>
<a href="#eglew.h-2418">2418</a>
<a href="#eglew.h-2419">2419</a>
<a href="#eglew.h-2420">2420</a>
<a href="#eglew.h-2421">2421</a>
<a href="#eglew.h-2422">2422</a>
<a href="#eglew.h-2423">2423</a>
<a href="#eglew.h-2424">2424</a>
<a href="#eglew.h-2425">2425</a>
<a href="#eglew.h-2426">2426</a>
<a href="#eglew.h-2427">2427</a>
<a href="#eglew.h-2428">2428</a>
<a href="#eglew.h-2429">2429</a>
<a href="#eglew.h-2430">2430</a>
<a href="#eglew.h-2431">2431</a>
<a href="#eglew.h-2432">2432</a>
<a href="#eglew.h-2433">2433</a>
<a href="#eglew.h-2434">2434</a>
<a href="#eglew.h-2435">2435</a>
<a href="#eglew.h-2436">2436</a>
<a href="#eglew.h-2437">2437</a>
<a href="#eglew.h-2438">2438</a>
<a href="#eglew.h-2439">2439</a>
<a href="#eglew.h-2440">2440</a>
<a href="#eglew.h-2441">2441</a>
<a href="#eglew.h-2442">2442</a>
<a href="#eglew.h-2443">2443</a>
<a href="#eglew.h-2444">2444</a>
<a href="#eglew.h-2445">2445</a>
<a href="#eglew.h-2446">2446</a>
<a href="#eglew.h-2447">2447</a>
<a href="#eglew.h-2448">2448</a>
<a href="#eglew.h-2449">2449</a>
<a href="#eglew.h-2450">2450</a>
<a href="#eglew.h-2451">2451</a>
<a href="#eglew.h-2452">2452</a>
<a href="#eglew.h-2453">2453</a>
<a href="#eglew.h-2454">2454</a>
<a href="#eglew.h-2455">2455</a>
<a href="#eglew.h-2456">2456</a>
<a href="#eglew.h-2457">2457</a>
<a href="#eglew.h-2458">2458</a>
<a href="#eglew.h-2459">2459</a>
<a href="#eglew.h-2460">2460</a>
<a href="#eglew.h-2461">2461</a>
<a href="#eglew.h-2462">2462</a>
<a href="#eglew.h-2463">2463</a>
<a href="#eglew.h-2464">2464</a>
<a href="#eglew.h-2465">2465</a>
<a href="#eglew.h-2466">2466</a>
<a href="#eglew.h-2467">2467</a>
<a href="#eglew.h-2468">2468</a>
<a href="#eglew.h-2469">2469</a>
<a href="#eglew.h-2470">2470</a>
<a href="#eglew.h-2471">2471</a>
<a href="#eglew.h-2472">2472</a>
<a href="#eglew.h-2473">2473</a>
<a href="#eglew.h-2474">2474</a>
<a href="#eglew.h-2475">2475</a>
<a href="#eglew.h-2476">2476</a>
<a href="#eglew.h-2477">2477</a>
<a href="#eglew.h-2478">2478</a>
<a href="#eglew.h-2479">2479</a>
<a href="#eglew.h-2480">2480</a>
<a href="#eglew.h-2481">2481</a>
<a href="#eglew.h-2482">2482</a>
<a href="#eglew.h-2483">2483</a>
<a href="#eglew.h-2484">2484</a>
<a href="#eglew.h-2485">2485</a>
<a href="#eglew.h-2486">2486</a>
<a href="#eglew.h-2487">2487</a>
<a href="#eglew.h-2488">2488</a>
<a href="#eglew.h-2489">2489</a>
<a href="#eglew.h-2490">2490</a>
<a href="#eglew.h-2491">2491</a>
<a href="#eglew.h-2492">2492</a>
<a href="#eglew.h-2493">2493</a>
<a href="#eglew.h-2494">2494</a>
<a href="#eglew.h-2495">2495</a>
<a href="#eglew.h-2496">2496</a>
<a href="#eglew.h-2497">2497</a>
<a href="#eglew.h-2498">2498</a>
<a href="#eglew.h-2499">2499</a>
<a href="#eglew.h-2500">2500</a>
<a href="#eglew.h-2501">2501</a>
<a href="#eglew.h-2502">2502</a>
<a href="#eglew.h-2503">2503</a>
<a href="#eglew.h-2504">2504</a>
<a href="#eglew.h-2505">2505</a>
<a href="#eglew.h-2506">2506</a>
<a href="#eglew.h-2507">2507</a>
<a href="#eglew.h-2508">2508</a>
<a href="#eglew.h-2509">2509</a>
<a href="#eglew.h-2510">2510</a>
<a href="#eglew.h-2511">2511</a>
<a href="#eglew.h-2512">2512</a>
<a href="#eglew.h-2513">2513</a>
<a href="#eglew.h-2514">2514</a>
<a href="#eglew.h-2515">2515</a>
<a href="#eglew.h-2516">2516</a>
<a href="#eglew.h-2517">2517</a>
<a href="#eglew.h-2518">2518</a>
<a href="#eglew.h-2519">2519</a>
<a href="#eglew.h-2520">2520</a>
<a href="#eglew.h-2521">2521</a>
<a href="#eglew.h-2522">2522</a>
<a href="#eglew.h-2523">2523</a>
<a href="#eglew.h-2524">2524</a>
<a href="#eglew.h-2525">2525</a>
<a href="#eglew.h-2526">2526</a>
<a href="#eglew.h-2527">2527</a>
<a href="#eglew.h-2528">2528</a>
<a href="#eglew.h-2529">2529</a>
<a href="#eglew.h-2530">2530</a>
<a href="#eglew.h-2531">2531</a>
<a href="#eglew.h-2532">2532</a>
<a href="#eglew.h-2533">2533</a>
<a href="#eglew.h-2534">2534</a>
<a href="#eglew.h-2535">2535</a>
<a href="#eglew.h-2536">2536</a>
<a href="#eglew.h-2537">2537</a>
<a href="#eglew.h-2538">2538</a>
<a href="#eglew.h-2539">2539</a>
<a href="#eglew.h-2540">2540</a>
<a href="#eglew.h-2541">2541</a>
<a href="#eglew.h-2542">2542</a>
<a href="#eglew.h-2543">2543</a>
<a href="#eglew.h-2544">2544</a>
<a href="#eglew.h-2545">2545</a>
<a href="#eglew.h-2546">2546</a>
<a href="#eglew.h-2547">2547</a>
<a href="#eglew.h-2548">2548</a>
<a href="#eglew.h-2549">2549</a>
<a href="#eglew.h-2550">2550</a>
<a href="#eglew.h-2551">2551</a>
<a href="#eglew.h-2552">2552</a>
<a href="#eglew.h-2553">2553</a>
<a href="#eglew.h-2554">2554</a>
<a href="#eglew.h-2555">2555</a>
<a href="#eglew.h-2556">2556</a>
<a href="#eglew.h-2557">2557</a>
<a href="#eglew.h-2558">2558</a>
<a href="#eglew.h-2559">2559</a>
<a href="#eglew.h-2560">2560</a>
<a href="#eglew.h-2561">2561</a>
<a href="#eglew.h-2562">2562</a>
<a href="#eglew.h-2563">2563</a>
<a href="#eglew.h-2564">2564</a>
<a href="#eglew.h-2565">2565</a>
<a href="#eglew.h-2566">2566</a>
<a href="#eglew.h-2567">2567</a>
<a href="#eglew.h-2568">2568</a>
<a href="#eglew.h-2569">2569</a>
<a href="#eglew.h-2570">2570</a>
<a href="#eglew.h-2571">2571</a>
<a href="#eglew.h-2572">2572</a>
<a href="#eglew.h-2573">2573</a>
<a href="#eglew.h-2574">2574</a>
<a href="#eglew.h-2575">2575</a>
<a href="#eglew.h-2576">2576</a>
<a href="#eglew.h-2577">2577</a>
<a href="#eglew.h-2578">2578</a>
<a href="#eglew.h-2579">2579</a>
<a href="#eglew.h-2580">2580</a>
<a href="#eglew.h-2581">2581</a>
<a href="#eglew.h-2582">2582</a>
<a href="#eglew.h-2583">2583</a>
<a href="#eglew.h-2584">2584</a>
<a href="#eglew.h-2585">2585</a>
<a href="#eglew.h-2586">2586</a>
<a href="#eglew.h-2587">2587</a>
<a href="#eglew.h-2588">2588</a>
<a href="#eglew.h-2589">2589</a>
<a href="#eglew.h-2590">2590</a>
<a href="#eglew.h-2591">2591</a>
<a href="#eglew.h-2592">2592</a>
<a href="#eglew.h-2593">2593</a>
<a href="#eglew.h-2594">2594</a>
<a href="#eglew.h-2595">2595</a>
<a href="#eglew.h-2596">2596</a>
<a href="#eglew.h-2597">2597</a>
<a href="#eglew.h-2598">2598</a>
<a href="#eglew.h-2599">2599</a>
<a href="#eglew.h-2600">2600</a>
<a href="#eglew.h-2601">2601</a>
<a href="#eglew.h-2602">2602</a>
<a href="#eglew.h-2603">2603</a>
<a href="#eglew.h-2604">2604</a>
<a href="#eglew.h-2605">2605</a>
<a href="#eglew.h-2606">2606</a>
<a href="#eglew.h-2607">2607</a>
<a href="#eglew.h-2608">2608</a>
<a href="#eglew.h-2609">2609</a>
<a href="#eglew.h-2610">2610</a>
<a href="#eglew.h-2611">2611</a>
<a href="#eglew.h-2612">2612</a>
<a href="#eglew.h-2613">2613</a>
<a href="#eglew.h-2614">2614</a>
<a href="#eglew.h-2615">2615</a>
<a href="#eglew.h-2616">2616</a>
<a href="#eglew.h-2617">2617</a>
<a href="#eglew.h-2618">2618</a>
</pre></div></td>
<td class="code"><div class="highlight"><pre>
<a name="eglew.h-1"></a>/*
<a name="eglew.h-2"></a>** The OpenGL Extension Wrangler Library
<a name="eglew.h-3"></a>** Copyright (C) 2008-2017, Nigel Stewart &lt;nigels[]users sourceforge net&gt;
<a name="eglew.h-4"></a>** Copyright (C) 2002-2008, Milan Ikits &lt;milan ikits[]ieee org&gt;
<a name="eglew.h-5"></a>** Copyright (C) 2002-2008, Marcelo E. Magallon &lt;mmagallo[]debian org&gt;
<a name="eglew.h-6"></a>** Copyright (C) 2002, Lev Povalahev
<a name="eglew.h-7"></a>** All rights reserved.
<a name="eglew.h-8"></a>** 
<a name="eglew.h-9"></a>** Redistribution and use in source and binary forms, with or without 
<a name="eglew.h-10"></a>** modification, are permitted provided that the following conditions are met:
<a name="eglew.h-11"></a>** 
<a name="eglew.h-12"></a>** * Redistributions of source code must retain the above copyright notice, 
<a name="eglew.h-13"></a>**   this list of conditions and the following disclaimer.
<a name="eglew.h-14"></a>** * Redistributions in binary form must reproduce the above copyright notice, 
<a name="eglew.h-15"></a>**   this list of conditions and the following disclaimer in the documentation 
<a name="eglew.h-16"></a>**   and/or other materials provided with the distribution.
<a name="eglew.h-17"></a>** * The name of the author may be used to endorse or promote products 
<a name="eglew.h-18"></a>**   derived from this software without specific prior written permission.
<a name="eglew.h-19"></a>**
<a name="eglew.h-20"></a>** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
<a name="eglew.h-21"></a>** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
<a name="eglew.h-22"></a>** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
<a name="eglew.h-23"></a>** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
<a name="eglew.h-24"></a>** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
<a name="eglew.h-25"></a>** CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
<a name="eglew.h-26"></a>** SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
<a name="eglew.h-27"></a>** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
<a name="eglew.h-28"></a>** CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
<a name="eglew.h-29"></a>** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
<a name="eglew.h-30"></a>** THE POSSIBILITY OF SUCH DAMAGE.
<a name="eglew.h-31"></a>*/
<a name="eglew.h-32"></a>
<a name="eglew.h-33"></a>/*
<a name="eglew.h-34"></a> * Mesa 3-D graphics library
<a name="eglew.h-35"></a> * Version:  7.0
<a name="eglew.h-36"></a> *
<a name="eglew.h-37"></a> * Copyright (C) 1999-2007  Brian Paul   All Rights Reserved.
<a name="eglew.h-38"></a> *
<a name="eglew.h-39"></a> * Permission is hereby granted, free of charge, to any person obtaining a
<a name="eglew.h-40"></a> * copy of this software and associated documentation files (the "Software"),
<a name="eglew.h-41"></a> * to deal in the Software without restriction, including without limitation
<a name="eglew.h-42"></a> * the rights to use, copy, modify, merge, publish, distribute, sublicense,
<a name="eglew.h-43"></a> * and/or sell copies of the Software, and to permit persons to whom the
<a name="eglew.h-44"></a> * Software is furnished to do so, subject to the following conditions:
<a name="eglew.h-45"></a> *
<a name="eglew.h-46"></a> * The above copyright notice and this permission notice shall be included
<a name="eglew.h-47"></a> * in all copies or substantial portions of the Software.
<a name="eglew.h-48"></a> *
<a name="eglew.h-49"></a> * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
<a name="eglew.h-50"></a> * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
<a name="eglew.h-51"></a> * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
<a name="eglew.h-52"></a> * BRIAN PAUL BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
<a name="eglew.h-53"></a> * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
<a name="eglew.h-54"></a> * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
<a name="eglew.h-55"></a> */
<a name="eglew.h-56"></a>
<a name="eglew.h-57"></a>/*
<a name="eglew.h-58"></a>** Copyright (c) 2007 The Khronos Group Inc.
<a name="eglew.h-59"></a>** 
<a name="eglew.h-60"></a>** Permission is hereby granted, free of charge, to any person obtaining a
<a name="eglew.h-61"></a>** copy of this software and/or associated documentation files (the
<a name="eglew.h-62"></a>** "Materials"), to deal in the Materials without restriction, including
<a name="eglew.h-63"></a>** without limitation the rights to use, copy, modify, merge, publish,
<a name="eglew.h-64"></a>** distribute, sublicense, and/or sell copies of the Materials, and to
<a name="eglew.h-65"></a>** permit persons to whom the Materials are furnished to do so, subject to
<a name="eglew.h-66"></a>** the following conditions:
<a name="eglew.h-67"></a>** 
<a name="eglew.h-68"></a>** The above copyright notice and this permission notice shall be included
<a name="eglew.h-69"></a>** in all copies or substantial portions of the Materials.
<a name="eglew.h-70"></a>** 
<a name="eglew.h-71"></a>** THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
<a name="eglew.h-72"></a>** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
<a name="eglew.h-73"></a>** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
<a name="eglew.h-74"></a>** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
<a name="eglew.h-75"></a>** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
<a name="eglew.h-76"></a>** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
<a name="eglew.h-77"></a>** MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
<a name="eglew.h-78"></a>*/
<a name="eglew.h-79"></a>
<a name="eglew.h-80"></a>#ifndef __eglew_h__
<a name="eglew.h-81"></a>#define __eglew_h__
<a name="eglew.h-82"></a>#define __EGLEW_H__
<a name="eglew.h-83"></a>
<a name="eglew.h-84"></a>#ifdef __eglext_h_
<a name="eglew.h-85"></a>#error eglext.h included before eglew.h
<a name="eglew.h-86"></a>#endif
<a name="eglew.h-87"></a>
<a name="eglew.h-88"></a>#if defined(__egl_h_)
<a name="eglew.h-89"></a>#error egl.h included before eglew.h
<a name="eglew.h-90"></a>#endif
<a name="eglew.h-91"></a>
<a name="eglew.h-92"></a>#define __eglext_h_
<a name="eglew.h-93"></a>
<a name="eglew.h-94"></a>#define __egl_h_
<a name="eglew.h-95"></a>
<a name="eglew.h-96"></a>#ifndef EGLAPIENTRY
<a name="eglew.h-97"></a>#define EGLAPIENTRY
<a name="eglew.h-98"></a>#endif
<a name="eglew.h-99"></a>#ifndef EGLAPI
<a name="eglew.h-100"></a>#define EGLAPI extern
<a name="eglew.h-101"></a>#endif
<a name="eglew.h-102"></a>
<a name="eglew.h-103"></a>/* EGL Types */
<a name="eglew.h-104"></a>#include &lt;sys/types.h&gt;
<a name="eglew.h-105"></a>
<a name="eglew.h-106"></a>#include &lt;KHR/khrplatform.h&gt;
<a name="eglew.h-107"></a>#include &lt;EGL/eglplatform.h&gt;
<a name="eglew.h-108"></a>
<a name="eglew.h-109"></a>#include &lt;GL/glew.h&gt;
<a name="eglew.h-110"></a>
<a name="eglew.h-111"></a>#ifdef __cplusplus
<a name="eglew.h-112"></a>extern "C" {
<a name="eglew.h-113"></a>#endif
<a name="eglew.h-114"></a>
<a name="eglew.h-115"></a>typedef int32_t EGLint;
<a name="eglew.h-116"></a>
<a name="eglew.h-117"></a>typedef unsigned int EGLBoolean;
<a name="eglew.h-118"></a>typedef void *EGLDisplay;
<a name="eglew.h-119"></a>typedef void *EGLConfig;
<a name="eglew.h-120"></a>typedef void *EGLSurface;
<a name="eglew.h-121"></a>typedef void *EGLContext;
<a name="eglew.h-122"></a>typedef void (*__eglMustCastToProperFunctionPointerType)(void);
<a name="eglew.h-123"></a>
<a name="eglew.h-124"></a>typedef unsigned int EGLenum;
<a name="eglew.h-125"></a>typedef void *EGLClientBuffer;
<a name="eglew.h-126"></a>
<a name="eglew.h-127"></a>typedef void *EGLSync;
<a name="eglew.h-128"></a>typedef intptr_t EGLAttrib;
<a name="eglew.h-129"></a>typedef khronos_utime_nanoseconds_t EGLTime;
<a name="eglew.h-130"></a>typedef void *EGLImage;
<a name="eglew.h-131"></a>
<a name="eglew.h-132"></a>typedef void *EGLSyncKHR;
<a name="eglew.h-133"></a>typedef intptr_t EGLAttribKHR;
<a name="eglew.h-134"></a>typedef void *EGLLabelKHR;
<a name="eglew.h-135"></a>typedef void *EGLObjectKHR;
<a name="eglew.h-136"></a>typedef void (EGLAPIENTRY  *EGLDEBUGPROCKHR)(EGLenum error,const char *command,EGLint messageType,EGLLabelKHR threadLabel,EGLLabelKHR objectLabel,const char* message);
<a name="eglew.h-137"></a>typedef khronos_utime_nanoseconds_t EGLTimeKHR;
<a name="eglew.h-138"></a>typedef void *EGLImageKHR;
<a name="eglew.h-139"></a>typedef void *EGLStreamKHR;
<a name="eglew.h-140"></a>typedef khronos_uint64_t EGLuint64KHR;
<a name="eglew.h-141"></a>typedef int EGLNativeFileDescriptorKHR;
<a name="eglew.h-142"></a>typedef khronos_ssize_t EGLsizeiANDROID;
<a name="eglew.h-143"></a>typedef void (*EGLSetBlobFuncANDROID) (const void *key, EGLsizeiANDROID keySize, const void *value, EGLsizeiANDROID valueSize);
<a name="eglew.h-144"></a>typedef EGLsizeiANDROID (*EGLGetBlobFuncANDROID) (const void *key, EGLsizeiANDROID keySize, void *value, EGLsizeiANDROID valueSize);
<a name="eglew.h-145"></a>typedef void *EGLDeviceEXT;
<a name="eglew.h-146"></a>typedef void *EGLOutputLayerEXT;
<a name="eglew.h-147"></a>typedef void *EGLOutputPortEXT;
<a name="eglew.h-148"></a>typedef void *EGLSyncNV;
<a name="eglew.h-149"></a>typedef khronos_utime_nanoseconds_t EGLTimeNV;
<a name="eglew.h-150"></a>typedef khronos_utime_nanoseconds_t EGLuint64NV;
<a name="eglew.h-151"></a>typedef khronos_stime_nanoseconds_t EGLnsecsANDROID;
<a name="eglew.h-152"></a>
<a name="eglew.h-153"></a>struct EGLClientPixmapHI;
<a name="eglew.h-154"></a>
<a name="eglew.h-155"></a>#define EGL_DONT_CARE                     ((EGLint)-1)
<a name="eglew.h-156"></a>
<a name="eglew.h-157"></a>#define EGL_NO_CONTEXT                    ((EGLContext)0)
<a name="eglew.h-158"></a>#define EGL_NO_DISPLAY                    ((EGLDisplay)0)
<a name="eglew.h-159"></a>#define EGL_NO_IMAGE                      ((EGLImage)0)
<a name="eglew.h-160"></a>#define EGL_NO_SURFACE                    ((EGLSurface)0)
<a name="eglew.h-161"></a>#define EGL_NO_SYNC                       ((EGLSync)0)
<a name="eglew.h-162"></a>
<a name="eglew.h-163"></a>#define EGL_UNKNOWN                       ((EGLint)-1)
<a name="eglew.h-164"></a>
<a name="eglew.h-165"></a>#define EGL_DEFAULT_DISPLAY               ((EGLNativeDisplayType)0)
<a name="eglew.h-166"></a>
<a name="eglew.h-167"></a>EGLAPI __eglMustCastToProperFunctionPointerType EGLAPIENTRY eglGetProcAddress (const char *procname);
<a name="eglew.h-168"></a>/* ---------------------------- EGL_VERSION_1_0 ---------------------------- */
<a name="eglew.h-169"></a>
<a name="eglew.h-170"></a>#ifndef EGL_VERSION_1_0
<a name="eglew.h-171"></a>#define EGL_VERSION_1_0 1
<a name="eglew.h-172"></a>
<a name="eglew.h-173"></a>#define EGL_FALSE 0
<a name="eglew.h-174"></a>#define EGL_PBUFFER_BIT 0x0001
<a name="eglew.h-175"></a>#define EGL_TRUE 1
<a name="eglew.h-176"></a>#define EGL_PIXMAP_BIT 0x0002
<a name="eglew.h-177"></a>#define EGL_WINDOW_BIT 0x0004
<a name="eglew.h-178"></a>#define EGL_SUCCESS 0x3000
<a name="eglew.h-179"></a>#define EGL_NOT_INITIALIZED 0x3001
<a name="eglew.h-180"></a>#define EGL_BAD_ACCESS 0x3002
<a name="eglew.h-181"></a>#define EGL_BAD_ALLOC 0x3003
<a name="eglew.h-182"></a>#define EGL_BAD_ATTRIBUTE 0x3004
<a name="eglew.h-183"></a>#define EGL_BAD_CONFIG 0x3005
<a name="eglew.h-184"></a>#define EGL_BAD_CONTEXT 0x3006
<a name="eglew.h-185"></a>#define EGL_BAD_CURRENT_SURFACE 0x3007
<a name="eglew.h-186"></a>#define EGL_BAD_DISPLAY 0x3008
<a name="eglew.h-187"></a>#define EGL_BAD_MATCH 0x3009
<a name="eglew.h-188"></a>#define EGL_BAD_NATIVE_PIXMAP 0x300A
<a name="eglew.h-189"></a>#define EGL_BAD_NATIVE_WINDOW 0x300B
<a name="eglew.h-190"></a>#define EGL_BAD_PARAMETER 0x300C
<a name="eglew.h-191"></a>#define EGL_BAD_SURFACE 0x300D
<a name="eglew.h-192"></a>#define EGL_BUFFER_SIZE 0x3020
<a name="eglew.h-193"></a>#define EGL_ALPHA_SIZE 0x3021
<a name="eglew.h-194"></a>#define EGL_BLUE_SIZE 0x3022
<a name="eglew.h-195"></a>#define EGL_GREEN_SIZE 0x3023
<a name="eglew.h-196"></a>#define EGL_RED_SIZE 0x3024
<a name="eglew.h-197"></a>#define EGL_DEPTH_SIZE 0x3025
<a name="eglew.h-198"></a>#define EGL_STENCIL_SIZE 0x3026
<a name="eglew.h-199"></a>#define EGL_CONFIG_CAVEAT 0x3027
<a name="eglew.h-200"></a>#define EGL_CONFIG_ID 0x3028
<a name="eglew.h-201"></a>#define EGL_LEVEL 0x3029
<a name="eglew.h-202"></a>#define EGL_MAX_PBUFFER_HEIGHT 0x302A
<a name="eglew.h-203"></a>#define EGL_MAX_PBUFFER_PIXELS 0x302B
<a name="eglew.h-204"></a>#define EGL_MAX_PBUFFER_WIDTH 0x302C
<a name="eglew.h-205"></a>#define EGL_NATIVE_RENDERABLE 0x302D
<a name="eglew.h-206"></a>#define EGL_NATIVE_VISUAL_ID 0x302E
<a name="eglew.h-207"></a>#define EGL_NATIVE_VISUAL_TYPE 0x302F
<a name="eglew.h-208"></a>#define EGL_SAMPLES 0x3031
<a name="eglew.h-209"></a>#define EGL_SAMPLE_BUFFERS 0x3032
<a name="eglew.h-210"></a>#define EGL_SURFACE_TYPE 0x3033
<a name="eglew.h-211"></a>#define EGL_TRANSPARENT_TYPE 0x3034
<a name="eglew.h-212"></a>#define EGL_TRANSPARENT_BLUE_VALUE 0x3035
<a name="eglew.h-213"></a>#define EGL_TRANSPARENT_GREEN_VALUE 0x3036
<a name="eglew.h-214"></a>#define EGL_TRANSPARENT_RED_VALUE 0x3037
<a name="eglew.h-215"></a>#define EGL_NONE 0x3038
<a name="eglew.h-216"></a>#define EGL_SLOW_CONFIG 0x3050
<a name="eglew.h-217"></a>#define EGL_NON_CONFORMANT_CONFIG 0x3051
<a name="eglew.h-218"></a>#define EGL_TRANSPARENT_RGB 0x3052
<a name="eglew.h-219"></a>#define EGL_VENDOR 0x3053
<a name="eglew.h-220"></a>#define EGL_VERSION 0x3054
<a name="eglew.h-221"></a>#define EGL_EXTENSIONS 0x3055
<a name="eglew.h-222"></a>#define EGL_HEIGHT 0x3056
<a name="eglew.h-223"></a>#define EGL_WIDTH 0x3057
<a name="eglew.h-224"></a>#define EGL_LARGEST_PBUFFER 0x3058
<a name="eglew.h-225"></a>#define EGL_DRAW 0x3059
<a name="eglew.h-226"></a>#define EGL_READ 0x305A
<a name="eglew.h-227"></a>#define EGL_CORE_NATIVE_ENGINE 0x305B
<a name="eglew.h-228"></a>
<a name="eglew.h-229"></a>typedef EGLBoolean  ( * PFNEGLCHOOSECONFIGPROC) (EGLDisplay  dpy, const EGLint * attrib_list, EGLConfig * configs, EGLint  config_size, EGLint * num_config);
<a name="eglew.h-230"></a>typedef EGLBoolean  ( * PFNEGLCOPYBUFFERSPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLNativePixmapType  target);
<a name="eglew.h-231"></a>typedef EGLContext  ( * PFNEGLCREATECONTEXTPROC) (EGLDisplay  dpy, EGLConfig  config, EGLContext  share_context, const EGLint * attrib_list);
<a name="eglew.h-232"></a>typedef EGLSurface  ( * PFNEGLCREATEPBUFFERSURFACEPROC) (EGLDisplay  dpy, EGLConfig  config, const EGLint * attrib_list);
<a name="eglew.h-233"></a>typedef EGLSurface  ( * PFNEGLCREATEPIXMAPSURFACEPROC) (EGLDisplay  dpy, EGLConfig  config, EGLNativePixmapType  pixmap, const EGLint * attrib_list);
<a name="eglew.h-234"></a>typedef EGLSurface  ( * PFNEGLCREATEWINDOWSURFACEPROC) (EGLDisplay  dpy, EGLConfig  config, EGLNativeWindowType  win, const EGLint * attrib_list);
<a name="eglew.h-235"></a>typedef EGLBoolean  ( * PFNEGLDESTROYCONTEXTPROC) (EGLDisplay  dpy, EGLContext  ctx);
<a name="eglew.h-236"></a>typedef EGLBoolean  ( * PFNEGLDESTROYSURFACEPROC) (EGLDisplay  dpy, EGLSurface  surface);
<a name="eglew.h-237"></a>typedef EGLBoolean  ( * PFNEGLGETCONFIGATTRIBPROC) (EGLDisplay  dpy, EGLConfig  config, EGLint  attribute, EGLint * value);
<a name="eglew.h-238"></a>typedef EGLBoolean  ( * PFNEGLGETCONFIGSPROC) (EGLDisplay  dpy, EGLConfig * configs, EGLint  config_size, EGLint * num_config);
<a name="eglew.h-239"></a>typedef EGLDisplay  ( * PFNEGLGETCURRENTDISPLAYPROC) ( void );
<a name="eglew.h-240"></a>typedef EGLSurface  ( * PFNEGLGETCURRENTSURFACEPROC) (EGLint  readdraw);
<a name="eglew.h-241"></a>typedef EGLDisplay  ( * PFNEGLGETDISPLAYPROC) (EGLNativeDisplayType  display_id);
<a name="eglew.h-242"></a>typedef EGLint  ( * PFNEGLGETERRORPROC) ( void );
<a name="eglew.h-243"></a>typedef EGLBoolean  ( * PFNEGLINITIALIZEPROC) (EGLDisplay  dpy, EGLint * major, EGLint * minor);
<a name="eglew.h-244"></a>typedef EGLBoolean  ( * PFNEGLMAKECURRENTPROC) (EGLDisplay  dpy, EGLSurface  draw, EGLSurface  read, EGLContext  ctx);
<a name="eglew.h-245"></a>typedef EGLBoolean  ( * PFNEGLQUERYCONTEXTPROC) (EGLDisplay  dpy, EGLContext  ctx, EGLint  attribute, EGLint * value);
<a name="eglew.h-246"></a>typedef const char * ( * PFNEGLQUERYSTRINGPROC) (EGLDisplay  dpy, EGLint  name);
<a name="eglew.h-247"></a>typedef EGLBoolean  ( * PFNEGLQUERYSURFACEPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  attribute, EGLint * value);
<a name="eglew.h-248"></a>typedef EGLBoolean  ( * PFNEGLSWAPBUFFERSPROC) (EGLDisplay  dpy, EGLSurface  surface);
<a name="eglew.h-249"></a>typedef EGLBoolean  ( * PFNEGLTERMINATEPROC) (EGLDisplay  dpy);
<a name="eglew.h-250"></a>typedef EGLBoolean  ( * PFNEGLWAITGLPROC) ( void );
<a name="eglew.h-251"></a>typedef EGLBoolean  ( * PFNEGLWAITNATIVEPROC) (EGLint  engine);
<a name="eglew.h-252"></a>
<a name="eglew.h-253"></a>#define eglChooseConfig EGLEW_GET_FUN(__eglewChooseConfig)
<a name="eglew.h-254"></a>#define eglCopyBuffers EGLEW_GET_FUN(__eglewCopyBuffers)
<a name="eglew.h-255"></a>#define eglCreateContext EGLEW_GET_FUN(__eglewCreateContext)
<a name="eglew.h-256"></a>#define eglCreatePbufferSurface EGLEW_GET_FUN(__eglewCreatePbufferSurface)
<a name="eglew.h-257"></a>#define eglCreatePixmapSurface EGLEW_GET_FUN(__eglewCreatePixmapSurface)
<a name="eglew.h-258"></a>#define eglCreateWindowSurface EGLEW_GET_FUN(__eglewCreateWindowSurface)
<a name="eglew.h-259"></a>#define eglDestroyContext EGLEW_GET_FUN(__eglewDestroyContext)
<a name="eglew.h-260"></a>#define eglDestroySurface EGLEW_GET_FUN(__eglewDestroySurface)
<a name="eglew.h-261"></a>#define eglGetConfigAttrib EGLEW_GET_FUN(__eglewGetConfigAttrib)
<a name="eglew.h-262"></a>#define eglGetConfigs EGLEW_GET_FUN(__eglewGetConfigs)
<a name="eglew.h-263"></a>#define eglGetCurrentDisplay EGLEW_GET_FUN(__eglewGetCurrentDisplay)
<a name="eglew.h-264"></a>#define eglGetCurrentSurface EGLEW_GET_FUN(__eglewGetCurrentSurface)
<a name="eglew.h-265"></a>#define eglGetDisplay EGLEW_GET_FUN(__eglewGetDisplay)
<a name="eglew.h-266"></a>#define eglGetError EGLEW_GET_FUN(__eglewGetError)
<a name="eglew.h-267"></a>#define eglInitialize EGLEW_GET_FUN(__eglewInitialize)
<a name="eglew.h-268"></a>#define eglMakeCurrent EGLEW_GET_FUN(__eglewMakeCurrent)
<a name="eglew.h-269"></a>#define eglQueryContext EGLEW_GET_FUN(__eglewQueryContext)
<a name="eglew.h-270"></a>#define eglQueryString EGLEW_GET_FUN(__eglewQueryString)
<a name="eglew.h-271"></a>#define eglQuerySurface EGLEW_GET_FUN(__eglewQuerySurface)
<a name="eglew.h-272"></a>#define eglSwapBuffers EGLEW_GET_FUN(__eglewSwapBuffers)
<a name="eglew.h-273"></a>#define eglTerminate EGLEW_GET_FUN(__eglewTerminate)
<a name="eglew.h-274"></a>#define eglWaitGL EGLEW_GET_FUN(__eglewWaitGL)
<a name="eglew.h-275"></a>#define eglWaitNative EGLEW_GET_FUN(__eglewWaitNative)
<a name="eglew.h-276"></a>
<a name="eglew.h-277"></a>#define EGLEW_VERSION_1_0 EGLEW_GET_VAR(__EGLEW_VERSION_1_0)
<a name="eglew.h-278"></a>
<a name="eglew.h-279"></a>#endif /* EGL_VERSION_1_0 */
<a name="eglew.h-280"></a>
<a name="eglew.h-281"></a>/* ---------------------------- EGL_VERSION_1_1 ---------------------------- */
<a name="eglew.h-282"></a>
<a name="eglew.h-283"></a>#ifndef EGL_VERSION_1_1
<a name="eglew.h-284"></a>#define EGL_VERSION_1_1 1
<a name="eglew.h-285"></a>
<a name="eglew.h-286"></a>#define EGL_CONTEXT_LOST 0x300E
<a name="eglew.h-287"></a>#define EGL_BIND_TO_TEXTURE_RGB 0x3039
<a name="eglew.h-288"></a>#define EGL_BIND_TO_TEXTURE_RGBA 0x303A
<a name="eglew.h-289"></a>#define EGL_MIN_SWAP_INTERVAL 0x303B
<a name="eglew.h-290"></a>#define EGL_MAX_SWAP_INTERVAL 0x303C
<a name="eglew.h-291"></a>#define EGL_NO_TEXTURE 0x305C
<a name="eglew.h-292"></a>#define EGL_TEXTURE_RGB 0x305D
<a name="eglew.h-293"></a>#define EGL_TEXTURE_RGBA 0x305E
<a name="eglew.h-294"></a>#define EGL_TEXTURE_2D 0x305F
<a name="eglew.h-295"></a>#define EGL_TEXTURE_FORMAT 0x3080
<a name="eglew.h-296"></a>#define EGL_TEXTURE_TARGET 0x3081
<a name="eglew.h-297"></a>#define EGL_MIPMAP_TEXTURE 0x3082
<a name="eglew.h-298"></a>#define EGL_MIPMAP_LEVEL 0x3083
<a name="eglew.h-299"></a>#define EGL_BACK_BUFFER 0x3084
<a name="eglew.h-300"></a>
<a name="eglew.h-301"></a>typedef EGLBoolean  ( * PFNEGLBINDTEXIMAGEPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  buffer);
<a name="eglew.h-302"></a>typedef EGLBoolean  ( * PFNEGLRELEASETEXIMAGEPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  buffer);
<a name="eglew.h-303"></a>typedef EGLBoolean  ( * PFNEGLSURFACEATTRIBPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  attribute, EGLint  value);
<a name="eglew.h-304"></a>typedef EGLBoolean  ( * PFNEGLSWAPINTERVALPROC) (EGLDisplay  dpy, EGLint  interval);
<a name="eglew.h-305"></a>
<a name="eglew.h-306"></a>#define eglBindTexImage EGLEW_GET_FUN(__eglewBindTexImage)
<a name="eglew.h-307"></a>#define eglReleaseTexImage EGLEW_GET_FUN(__eglewReleaseTexImage)
<a name="eglew.h-308"></a>#define eglSurfaceAttrib EGLEW_GET_FUN(__eglewSurfaceAttrib)
<a name="eglew.h-309"></a>#define eglSwapInterval EGLEW_GET_FUN(__eglewSwapInterval)
<a name="eglew.h-310"></a>
<a name="eglew.h-311"></a>#define EGLEW_VERSION_1_1 EGLEW_GET_VAR(__EGLEW_VERSION_1_1)
<a name="eglew.h-312"></a>
<a name="eglew.h-313"></a>#endif /* EGL_VERSION_1_1 */
<a name="eglew.h-314"></a>
<a name="eglew.h-315"></a>/* ---------------------------- EGL_VERSION_1_2 ---------------------------- */
<a name="eglew.h-316"></a>
<a name="eglew.h-317"></a>#ifndef EGL_VERSION_1_2
<a name="eglew.h-318"></a>#define EGL_VERSION_1_2 1
<a name="eglew.h-319"></a>
<a name="eglew.h-320"></a>#define EGL_OPENGL_ES_BIT 0x0001
<a name="eglew.h-321"></a>#define EGL_OPENVG_BIT 0x0002
<a name="eglew.h-322"></a>#define EGL_LUMINANCE_SIZE 0x303D
<a name="eglew.h-323"></a>#define EGL_ALPHA_MASK_SIZE 0x303E
<a name="eglew.h-324"></a>#define EGL_COLOR_BUFFER_TYPE 0x303F
<a name="eglew.h-325"></a>#define EGL_RENDERABLE_TYPE 0x3040
<a name="eglew.h-326"></a>#define EGL_SINGLE_BUFFER 0x3085
<a name="eglew.h-327"></a>#define EGL_RENDER_BUFFER 0x3086
<a name="eglew.h-328"></a>#define EGL_COLORSPACE 0x3087
<a name="eglew.h-329"></a>#define EGL_ALPHA_FORMAT 0x3088
<a name="eglew.h-330"></a>#define EGL_COLORSPACE_LINEAR 0x308A
<a name="eglew.h-331"></a>#define EGL_ALPHA_FORMAT_NONPRE 0x308B
<a name="eglew.h-332"></a>#define EGL_ALPHA_FORMAT_PRE 0x308C
<a name="eglew.h-333"></a>#define EGL_CLIENT_APIS 0x308D
<a name="eglew.h-334"></a>#define EGL_RGB_BUFFER 0x308E
<a name="eglew.h-335"></a>#define EGL_LUMINANCE_BUFFER 0x308F
<a name="eglew.h-336"></a>#define EGL_HORIZONTAL_RESOLUTION 0x3090
<a name="eglew.h-337"></a>#define EGL_VERTICAL_RESOLUTION 0x3091
<a name="eglew.h-338"></a>#define EGL_PIXEL_ASPECT_RATIO 0x3092
<a name="eglew.h-339"></a>#define EGL_SWAP_BEHAVIOR 0x3093
<a name="eglew.h-340"></a>#define EGL_BUFFER_PRESERVED 0x3094
<a name="eglew.h-341"></a>#define EGL_BUFFER_DESTROYED 0x3095
<a name="eglew.h-342"></a>#define EGL_OPENVG_IMAGE 0x3096
<a name="eglew.h-343"></a>#define EGL_CONTEXT_CLIENT_TYPE 0x3097
<a name="eglew.h-344"></a>#define EGL_OPENGL_ES_API 0x30A0
<a name="eglew.h-345"></a>#define EGL_OPENVG_API 0x30A1
<a name="eglew.h-346"></a>#define EGL_DISPLAY_SCALING 10000
<a name="eglew.h-347"></a>
<a name="eglew.h-348"></a>typedef EGLBoolean  ( * PFNEGLBINDAPIPROC) (EGLenum  api);
<a name="eglew.h-349"></a>typedef EGLSurface  ( * PFNEGLCREATEPBUFFERFROMCLIENTBUFFERPROC) (EGLDisplay  dpy, EGLenum  buftype, EGLClientBuffer  buffer, EGLConfig  config, const EGLint * attrib_list);
<a name="eglew.h-350"></a>typedef EGLenum  ( * PFNEGLQUERYAPIPROC) ( void );
<a name="eglew.h-351"></a>typedef EGLBoolean  ( * PFNEGLRELEASETHREADPROC) ( void );
<a name="eglew.h-352"></a>typedef EGLBoolean  ( * PFNEGLWAITCLIENTPROC) ( void );
<a name="eglew.h-353"></a>
<a name="eglew.h-354"></a>#define eglBindAPI EGLEW_GET_FUN(__eglewBindAPI)
<a name="eglew.h-355"></a>#define eglCreatePbufferFromClientBuffer EGLEW_GET_FUN(__eglewCreatePbufferFromClientBuffer)
<a name="eglew.h-356"></a>#define eglQueryAPI EGLEW_GET_FUN(__eglewQueryAPI)
<a name="eglew.h-357"></a>#define eglReleaseThread EGLEW_GET_FUN(__eglewReleaseThread)
<a name="eglew.h-358"></a>#define eglWaitClient EGLEW_GET_FUN(__eglewWaitClient)
<a name="eglew.h-359"></a>
<a name="eglew.h-360"></a>#define EGLEW_VERSION_1_2 EGLEW_GET_VAR(__EGLEW_VERSION_1_2)
<a name="eglew.h-361"></a>
<a name="eglew.h-362"></a>#endif /* EGL_VERSION_1_2 */
<a name="eglew.h-363"></a>
<a name="eglew.h-364"></a>/* ---------------------------- EGL_VERSION_1_3 ---------------------------- */
<a name="eglew.h-365"></a>
<a name="eglew.h-366"></a>#ifndef EGL_VERSION_1_3
<a name="eglew.h-367"></a>#define EGL_VERSION_1_3 1
<a name="eglew.h-368"></a>
<a name="eglew.h-369"></a>#define EGL_OPENGL_ES2_BIT 0x0004
<a name="eglew.h-370"></a>#define EGL_VG_COLORSPACE_LINEAR_BIT 0x0020
<a name="eglew.h-371"></a>#define EGL_VG_ALPHA_FORMAT_PRE_BIT 0x0040
<a name="eglew.h-372"></a>#define EGL_MATCH_NATIVE_PIXMAP 0x3041
<a name="eglew.h-373"></a>#define EGL_CONFORMANT 0x3042
<a name="eglew.h-374"></a>#define EGL_VG_COLORSPACE 0x3087
<a name="eglew.h-375"></a>#define EGL_VG_ALPHA_FORMAT 0x3088
<a name="eglew.h-376"></a>#define EGL_VG_COLORSPACE_LINEAR 0x308A
<a name="eglew.h-377"></a>#define EGL_VG_ALPHA_FORMAT_NONPRE 0x308B
<a name="eglew.h-378"></a>#define EGL_VG_ALPHA_FORMAT_PRE 0x308C
<a name="eglew.h-379"></a>#define EGL_CONTEXT_CLIENT_VERSION 0x3098
<a name="eglew.h-380"></a>
<a name="eglew.h-381"></a>#define EGLEW_VERSION_1_3 EGLEW_GET_VAR(__EGLEW_VERSION_1_3)
<a name="eglew.h-382"></a>
<a name="eglew.h-383"></a>#endif /* EGL_VERSION_1_3 */
<a name="eglew.h-384"></a>
<a name="eglew.h-385"></a>/* ---------------------------- EGL_VERSION_1_4 ---------------------------- */
<a name="eglew.h-386"></a>
<a name="eglew.h-387"></a>#ifndef EGL_VERSION_1_4
<a name="eglew.h-388"></a>#define EGL_VERSION_1_4 1
<a name="eglew.h-389"></a>
<a name="eglew.h-390"></a>#define EGL_OPENGL_BIT 0x0008
<a name="eglew.h-391"></a>#define EGL_MULTISAMPLE_RESOLVE_BOX_BIT 0x0200
<a name="eglew.h-392"></a>#define EGL_SWAP_BEHAVIOR_PRESERVED_BIT 0x0400
<a name="eglew.h-393"></a>#define EGL_MULTISAMPLE_RESOLVE 0x3099
<a name="eglew.h-394"></a>#define EGL_MULTISAMPLE_RESOLVE_DEFAULT 0x309A
<a name="eglew.h-395"></a>#define EGL_MULTISAMPLE_RESOLVE_BOX 0x309B
<a name="eglew.h-396"></a>#define EGL_OPENGL_API 0x30A2
<a name="eglew.h-397"></a>
<a name="eglew.h-398"></a>typedef EGLContext  ( * PFNEGLGETCURRENTCONTEXTPROC) ( void );
<a name="eglew.h-399"></a>
<a name="eglew.h-400"></a>#define eglGetCurrentContext EGLEW_GET_FUN(__eglewGetCurrentContext)
<a name="eglew.h-401"></a>
<a name="eglew.h-402"></a>#define EGLEW_VERSION_1_4 EGLEW_GET_VAR(__EGLEW_VERSION_1_4)
<a name="eglew.h-403"></a>
<a name="eglew.h-404"></a>#endif /* EGL_VERSION_1_4 */
<a name="eglew.h-405"></a>
<a name="eglew.h-406"></a>/* ---------------------------- EGL_VERSION_1_5 ---------------------------- */
<a name="eglew.h-407"></a>
<a name="eglew.h-408"></a>#ifndef EGL_VERSION_1_5
<a name="eglew.h-409"></a>#define EGL_VERSION_1_5 1
<a name="eglew.h-410"></a>
<a name="eglew.h-411"></a>#define EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT 0x00000001
<a name="eglew.h-412"></a>#define EGL_SYNC_FLUSH_COMMANDS_BIT 0x0001
<a name="eglew.h-413"></a>#define EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT 0x00000002
<a name="eglew.h-414"></a>#define EGL_OPENGL_ES3_BIT 0x00000040
<a name="eglew.h-415"></a>#define EGL_GL_COLORSPACE_SRGB 0x3089
<a name="eglew.h-416"></a>#define EGL_GL_COLORSPACE_LINEAR 0x308A
<a name="eglew.h-417"></a>#define EGL_CONTEXT_MAJOR_VERSION 0x3098
<a name="eglew.h-418"></a>#define EGL_CL_EVENT_HANDLE 0x309C
<a name="eglew.h-419"></a>#define EGL_GL_COLORSPACE 0x309D
<a name="eglew.h-420"></a>#define EGL_GL_TEXTURE_2D 0x30B1
<a name="eglew.h-421"></a>#define EGL_GL_TEXTURE_3D 0x30B2
<a name="eglew.h-422"></a>#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X 0x30B3
<a name="eglew.h-423"></a>#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X 0x30B4
<a name="eglew.h-424"></a>#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y 0x30B5
<a name="eglew.h-425"></a>#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y 0x30B6
<a name="eglew.h-426"></a>#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z 0x30B7
<a name="eglew.h-427"></a>#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 0x30B8
<a name="eglew.h-428"></a>#define EGL_GL_RENDERBUFFER 0x30B9
<a name="eglew.h-429"></a>#define EGL_GL_TEXTURE_LEVEL 0x30BC
<a name="eglew.h-430"></a>#define EGL_GL_TEXTURE_ZOFFSET 0x30BD
<a name="eglew.h-431"></a>#define EGL_IMAGE_PRESERVED 0x30D2
<a name="eglew.h-432"></a>#define EGL_SYNC_PRIOR_COMMANDS_COMPLETE 0x30F0
<a name="eglew.h-433"></a>#define EGL_SYNC_STATUS 0x30F1
<a name="eglew.h-434"></a>#define EGL_SIGNALED 0x30F2
<a name="eglew.h-435"></a>#define EGL_UNSIGNALED 0x30F3
<a name="eglew.h-436"></a>#define EGL_TIMEOUT_EXPIRED 0x30F5
<a name="eglew.h-437"></a>#define EGL_CONDITION_SATISFIED 0x30F6
<a name="eglew.h-438"></a>#define EGL_SYNC_TYPE 0x30F7
<a name="eglew.h-439"></a>#define EGL_SYNC_CONDITION 0x30F8
<a name="eglew.h-440"></a>#define EGL_SYNC_FENCE 0x30F9
<a name="eglew.h-441"></a>#define EGL_CONTEXT_MINOR_VERSION 0x30FB
<a name="eglew.h-442"></a>#define EGL_CONTEXT_OPENGL_PROFILE_MASK 0x30FD
<a name="eglew.h-443"></a>#define EGL_SYNC_CL_EVENT 0x30FE
<a name="eglew.h-444"></a>#define EGL_SYNC_CL_EVENT_COMPLETE 0x30FF
<a name="eglew.h-445"></a>#define EGL_CONTEXT_OPENGL_DEBUG 0x31B0
<a name="eglew.h-446"></a>#define EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE 0x31B1
<a name="eglew.h-447"></a>#define EGL_CONTEXT_OPENGL_ROBUST_ACCESS 0x31B2
<a name="eglew.h-448"></a>#define EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY 0x31BD
<a name="eglew.h-449"></a>#define EGL_NO_RESET_NOTIFICATION 0x31BE
<a name="eglew.h-450"></a>#define EGL_LOSE_CONTEXT_ON_RESET 0x31BF
<a name="eglew.h-451"></a>#define EGL_FOREVER 0xFFFFFFFFFFFFFFFF
<a name="eglew.h-452"></a>
<a name="eglew.h-453"></a>typedef EGLint  ( * PFNEGLCLIENTWAITSYNCPROC) (EGLDisplay  dpy, EGLSync  sync, EGLint  flags, EGLTime  timeout);
<a name="eglew.h-454"></a>typedef EGLImage  ( * PFNEGLCREATEIMAGEPROC) (EGLDisplay  dpy, EGLContext  ctx, EGLenum  target, EGLClientBuffer  buffer, const EGLAttrib * attrib_list);
<a name="eglew.h-455"></a>typedef EGLSurface  ( * PFNEGLCREATEPLATFORMPIXMAPSURFACEPROC) (EGLDisplay  dpy, EGLConfig  config, void * native_pixmap, const EGLAttrib * attrib_list);
<a name="eglew.h-456"></a>typedef EGLSurface  ( * PFNEGLCREATEPLATFORMWINDOWSURFACEPROC) (EGLDisplay  dpy, EGLConfig  config, void * native_window, const EGLAttrib * attrib_list);
<a name="eglew.h-457"></a>typedef EGLSync  ( * PFNEGLCREATESYNCPROC) (EGLDisplay  dpy, EGLenum  type, const EGLAttrib * attrib_list);
<a name="eglew.h-458"></a>typedef EGLBoolean  ( * PFNEGLDESTROYIMAGEPROC) (EGLDisplay  dpy, EGLImage  image);
<a name="eglew.h-459"></a>typedef EGLBoolean  ( * PFNEGLDESTROYSYNCPROC) (EGLDisplay  dpy, EGLSync  sync);
<a name="eglew.h-460"></a>typedef EGLDisplay  ( * PFNEGLGETPLATFORMDISPLAYPROC) (EGLenum  platform, void * native_display, const EGLAttrib * attrib_list);
<a name="eglew.h-461"></a>typedef EGLBoolean  ( * PFNEGLGETSYNCATTRIBPROC) (EGLDisplay  dpy, EGLSync  sync, EGLint  attribute, EGLAttrib * value);
<a name="eglew.h-462"></a>typedef EGLBoolean  ( * PFNEGLWAITSYNCPROC) (EGLDisplay  dpy, EGLSync  sync, EGLint  flags);
<a name="eglew.h-463"></a>
<a name="eglew.h-464"></a>#define eglClientWaitSync EGLEW_GET_FUN(__eglewClientWaitSync)
<a name="eglew.h-465"></a>#define eglCreateImage EGLEW_GET_FUN(__eglewCreateImage)
<a name="eglew.h-466"></a>#define eglCreatePlatformPixmapSurface EGLEW_GET_FUN(__eglewCreatePlatformPixmapSurface)
<a name="eglew.h-467"></a>#define eglCreatePlatformWindowSurface EGLEW_GET_FUN(__eglewCreatePlatformWindowSurface)
<a name="eglew.h-468"></a>#define eglCreateSync EGLEW_GET_FUN(__eglewCreateSync)
<a name="eglew.h-469"></a>#define eglDestroyImage EGLEW_GET_FUN(__eglewDestroyImage)
<a name="eglew.h-470"></a>#define eglDestroySync EGLEW_GET_FUN(__eglewDestroySync)
<a name="eglew.h-471"></a>#define eglGetPlatformDisplay EGLEW_GET_FUN(__eglewGetPlatformDisplay)
<a name="eglew.h-472"></a>#define eglGetSyncAttrib EGLEW_GET_FUN(__eglewGetSyncAttrib)
<a name="eglew.h-473"></a>#define eglWaitSync EGLEW_GET_FUN(__eglewWaitSync)
<a name="eglew.h-474"></a>
<a name="eglew.h-475"></a>#define EGLEW_VERSION_1_5 EGLEW_GET_VAR(__EGLEW_VERSION_1_5)
<a name="eglew.h-476"></a>
<a name="eglew.h-477"></a>#endif /* EGL_VERSION_1_5 */
<a name="eglew.h-478"></a>
<a name="eglew.h-479"></a>/* ------------------------- EGL_ANDROID_blob_cache ------------------------ */
<a name="eglew.h-480"></a>
<a name="eglew.h-481"></a>#ifndef EGL_ANDROID_blob_cache
<a name="eglew.h-482"></a>#define EGL_ANDROID_blob_cache 1
<a name="eglew.h-483"></a>
<a name="eglew.h-484"></a>typedef void  ( * PFNEGLSETBLOBCACHEFUNCSANDROIDPROC) (EGLDisplay  dpy, EGLSetBlobFuncANDROID  set, EGLGetBlobFuncANDROID  get);
<a name="eglew.h-485"></a>
<a name="eglew.h-486"></a>#define eglSetBlobCacheFuncsANDROID EGLEW_GET_FUN(__eglewSetBlobCacheFuncsANDROID)
<a name="eglew.h-487"></a>
<a name="eglew.h-488"></a>#define EGLEW_ANDROID_blob_cache EGLEW_GET_VAR(__EGLEW_ANDROID_blob_cache)
<a name="eglew.h-489"></a>
<a name="eglew.h-490"></a>#endif /* EGL_ANDROID_blob_cache */
<a name="eglew.h-491"></a>
<a name="eglew.h-492"></a>/* ---------------- EGL_ANDROID_create_native_client_buffer ---------------- */
<a name="eglew.h-493"></a>
<a name="eglew.h-494"></a>#ifndef EGL_ANDROID_create_native_client_buffer
<a name="eglew.h-495"></a>#define EGL_ANDROID_create_native_client_buffer 1
<a name="eglew.h-496"></a>
<a name="eglew.h-497"></a>#define EGL_NATIVE_BUFFER_USAGE_PROTECTED_BIT_ANDROID 0x00000001
<a name="eglew.h-498"></a>#define EGL_NATIVE_BUFFER_USAGE_RENDERBUFFER_BIT_ANDROID 0x00000002
<a name="eglew.h-499"></a>#define EGL_NATIVE_BUFFER_USAGE_TEXTURE_BIT_ANDROID 0x00000004
<a name="eglew.h-500"></a>#define EGL_NATIVE_BUFFER_USAGE_ANDROID 0x3143
<a name="eglew.h-501"></a>
<a name="eglew.h-502"></a>typedef EGLClientBuffer  ( * PFNEGLCREATENATIVECLIENTBUFFERANDROIDPROC) (const EGLint * attrib_list);
<a name="eglew.h-503"></a>
<a name="eglew.h-504"></a>#define eglCreateNativeClientBufferANDROID EGLEW_GET_FUN(__eglewCreateNativeClientBufferANDROID)
<a name="eglew.h-505"></a>
<a name="eglew.h-506"></a>#define EGLEW_ANDROID_create_native_client_buffer EGLEW_GET_VAR(__EGLEW_ANDROID_create_native_client_buffer)
<a name="eglew.h-507"></a>
<a name="eglew.h-508"></a>#endif /* EGL_ANDROID_create_native_client_buffer */
<a name="eglew.h-509"></a>
<a name="eglew.h-510"></a>/* --------------------- EGL_ANDROID_framebuffer_target -------------------- */
<a name="eglew.h-511"></a>
<a name="eglew.h-512"></a>#ifndef EGL_ANDROID_framebuffer_target
<a name="eglew.h-513"></a>#define EGL_ANDROID_framebuffer_target 1
<a name="eglew.h-514"></a>
<a name="eglew.h-515"></a>#define EGL_FRAMEBUFFER_TARGET_ANDROID 0x3147
<a name="eglew.h-516"></a>
<a name="eglew.h-517"></a>#define EGLEW_ANDROID_framebuffer_target EGLEW_GET_VAR(__EGLEW_ANDROID_framebuffer_target)
<a name="eglew.h-518"></a>
<a name="eglew.h-519"></a>#endif /* EGL_ANDROID_framebuffer_target */
<a name="eglew.h-520"></a>
<a name="eglew.h-521"></a>/* ----------------- EGL_ANDROID_front_buffer_auto_refresh ----------------- */
<a name="eglew.h-522"></a>
<a name="eglew.h-523"></a>#ifndef EGL_ANDROID_front_buffer_auto_refresh
<a name="eglew.h-524"></a>#define EGL_ANDROID_front_buffer_auto_refresh 1
<a name="eglew.h-525"></a>
<a name="eglew.h-526"></a>#define EGL_FRONT_BUFFER_AUTO_REFRESH_ANDROID 0x314C
<a name="eglew.h-527"></a>
<a name="eglew.h-528"></a>#define EGLEW_ANDROID_front_buffer_auto_refresh EGLEW_GET_VAR(__EGLEW_ANDROID_front_buffer_auto_refresh)
<a name="eglew.h-529"></a>
<a name="eglew.h-530"></a>#endif /* EGL_ANDROID_front_buffer_auto_refresh */
<a name="eglew.h-531"></a>
<a name="eglew.h-532"></a>/* -------------------- EGL_ANDROID_image_native_buffer -------------------- */
<a name="eglew.h-533"></a>
<a name="eglew.h-534"></a>#ifndef EGL_ANDROID_image_native_buffer
<a name="eglew.h-535"></a>#define EGL_ANDROID_image_native_buffer 1
<a name="eglew.h-536"></a>
<a name="eglew.h-537"></a>#define EGL_NATIVE_BUFFER_ANDROID 0x3140
<a name="eglew.h-538"></a>
<a name="eglew.h-539"></a>#define EGLEW_ANDROID_image_native_buffer EGLEW_GET_VAR(__EGLEW_ANDROID_image_native_buffer)
<a name="eglew.h-540"></a>
<a name="eglew.h-541"></a>#endif /* EGL_ANDROID_image_native_buffer */
<a name="eglew.h-542"></a>
<a name="eglew.h-543"></a>/* --------------------- EGL_ANDROID_native_fence_sync --------------------- */
<a name="eglew.h-544"></a>
<a name="eglew.h-545"></a>#ifndef EGL_ANDROID_native_fence_sync
<a name="eglew.h-546"></a>#define EGL_ANDROID_native_fence_sync 1
<a name="eglew.h-547"></a>
<a name="eglew.h-548"></a>#define EGL_SYNC_NATIVE_FENCE_ANDROID 0x3144
<a name="eglew.h-549"></a>#define EGL_SYNC_NATIVE_FENCE_FD_ANDROID 0x3145
<a name="eglew.h-550"></a>#define EGL_SYNC_NATIVE_FENCE_SIGNALED_ANDROID 0x3146
<a name="eglew.h-551"></a>
<a name="eglew.h-552"></a>typedef EGLint  ( * PFNEGLDUPNATIVEFENCEFDANDROIDPROC) (EGLDisplay  dpy, EGLSyncKHR  sync);
<a name="eglew.h-553"></a>
<a name="eglew.h-554"></a>#define eglDupNativeFenceFDANDROID EGLEW_GET_FUN(__eglewDupNativeFenceFDANDROID)
<a name="eglew.h-555"></a>
<a name="eglew.h-556"></a>#define EGLEW_ANDROID_native_fence_sync EGLEW_GET_VAR(__EGLEW_ANDROID_native_fence_sync)
<a name="eglew.h-557"></a>
<a name="eglew.h-558"></a>#endif /* EGL_ANDROID_native_fence_sync */
<a name="eglew.h-559"></a>
<a name="eglew.h-560"></a>/* --------------------- EGL_ANDROID_presentation_time --------------------- */
<a name="eglew.h-561"></a>
<a name="eglew.h-562"></a>#ifndef EGL_ANDROID_presentation_time
<a name="eglew.h-563"></a>#define EGL_ANDROID_presentation_time 1
<a name="eglew.h-564"></a>
<a name="eglew.h-565"></a>typedef EGLBoolean  ( * PFNEGLPRESENTATIONTIMEANDROIDPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLnsecsANDROID  time);
<a name="eglew.h-566"></a>
<a name="eglew.h-567"></a>#define eglPresentationTimeANDROID EGLEW_GET_FUN(__eglewPresentationTimeANDROID)
<a name="eglew.h-568"></a>
<a name="eglew.h-569"></a>#define EGLEW_ANDROID_presentation_time EGLEW_GET_VAR(__EGLEW_ANDROID_presentation_time)
<a name="eglew.h-570"></a>
<a name="eglew.h-571"></a>#endif /* EGL_ANDROID_presentation_time */
<a name="eglew.h-572"></a>
<a name="eglew.h-573"></a>/* ------------------------- EGL_ANDROID_recordable ------------------------ */
<a name="eglew.h-574"></a>
<a name="eglew.h-575"></a>#ifndef EGL_ANDROID_recordable
<a name="eglew.h-576"></a>#define EGL_ANDROID_recordable 1
<a name="eglew.h-577"></a>
<a name="eglew.h-578"></a>#define EGL_RECORDABLE_ANDROID 0x3142
<a name="eglew.h-579"></a>
<a name="eglew.h-580"></a>#define EGLEW_ANDROID_recordable EGLEW_GET_VAR(__EGLEW_ANDROID_recordable)
<a name="eglew.h-581"></a>
<a name="eglew.h-582"></a>#endif /* EGL_ANDROID_recordable */
<a name="eglew.h-583"></a>
<a name="eglew.h-584"></a>/* ---------------- EGL_ANGLE_d3d_share_handle_client_buffer --------------- */
<a name="eglew.h-585"></a>
<a name="eglew.h-586"></a>#ifndef EGL_ANGLE_d3d_share_handle_client_buffer
<a name="eglew.h-587"></a>#define EGL_ANGLE_d3d_share_handle_client_buffer 1
<a name="eglew.h-588"></a>
<a name="eglew.h-589"></a>#define EGL_D3D_TEXTURE_2D_SHARE_HANDLE_ANGLE 0x3200
<a name="eglew.h-590"></a>
<a name="eglew.h-591"></a>#define EGLEW_ANGLE_d3d_share_handle_client_buffer EGLEW_GET_VAR(__EGLEW_ANGLE_d3d_share_handle_client_buffer)
<a name="eglew.h-592"></a>
<a name="eglew.h-593"></a>#endif /* EGL_ANGLE_d3d_share_handle_client_buffer */
<a name="eglew.h-594"></a>
<a name="eglew.h-595"></a>/* -------------------------- EGL_ANGLE_device_d3d ------------------------- */
<a name="eglew.h-596"></a>
<a name="eglew.h-597"></a>#ifndef EGL_ANGLE_device_d3d
<a name="eglew.h-598"></a>#define EGL_ANGLE_device_d3d 1
<a name="eglew.h-599"></a>
<a name="eglew.h-600"></a>#define EGL_D3D9_DEVICE_ANGLE 0x33A0
<a name="eglew.h-601"></a>#define EGL_D3D11_DEVICE_ANGLE 0x33A1
<a name="eglew.h-602"></a>
<a name="eglew.h-603"></a>#define EGLEW_ANGLE_device_d3d EGLEW_GET_VAR(__EGLEW_ANGLE_device_d3d)
<a name="eglew.h-604"></a>
<a name="eglew.h-605"></a>#endif /* EGL_ANGLE_device_d3d */
<a name="eglew.h-606"></a>
<a name="eglew.h-607"></a>/* -------------------- EGL_ANGLE_query_surface_pointer -------------------- */
<a name="eglew.h-608"></a>
<a name="eglew.h-609"></a>#ifndef EGL_ANGLE_query_surface_pointer
<a name="eglew.h-610"></a>#define EGL_ANGLE_query_surface_pointer 1
<a name="eglew.h-611"></a>
<a name="eglew.h-612"></a>typedef EGLBoolean  ( * PFNEGLQUERYSURFACEPOINTERANGLEPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  attribute, void ** value);
<a name="eglew.h-613"></a>
<a name="eglew.h-614"></a>#define eglQuerySurfacePointerANGLE EGLEW_GET_FUN(__eglewQuerySurfacePointerANGLE)
<a name="eglew.h-615"></a>
<a name="eglew.h-616"></a>#define EGLEW_ANGLE_query_surface_pointer EGLEW_GET_VAR(__EGLEW_ANGLE_query_surface_pointer)
<a name="eglew.h-617"></a>
<a name="eglew.h-618"></a>#endif /* EGL_ANGLE_query_surface_pointer */
<a name="eglew.h-619"></a>
<a name="eglew.h-620"></a>/* ------------- EGL_ANGLE_surface_d3d_texture_2d_share_handle ------------- */
<a name="eglew.h-621"></a>
<a name="eglew.h-622"></a>#ifndef EGL_ANGLE_surface_d3d_texture_2d_share_handle
<a name="eglew.h-623"></a>#define EGL_ANGLE_surface_d3d_texture_2d_share_handle 1
<a name="eglew.h-624"></a>
<a name="eglew.h-625"></a>#define EGL_D3D_TEXTURE_2D_SHARE_HANDLE_ANGLE 0x3200
<a name="eglew.h-626"></a>
<a name="eglew.h-627"></a>#define EGLEW_ANGLE_surface_d3d_texture_2d_share_handle EGLEW_GET_VAR(__EGLEW_ANGLE_surface_d3d_texture_2d_share_handle)
<a name="eglew.h-628"></a>
<a name="eglew.h-629"></a>#endif /* EGL_ANGLE_surface_d3d_texture_2d_share_handle */
<a name="eglew.h-630"></a>
<a name="eglew.h-631"></a>/* ---------------------- EGL_ANGLE_window_fixed_size ---------------------- */
<a name="eglew.h-632"></a>
<a name="eglew.h-633"></a>#ifndef EGL_ANGLE_window_fixed_size
<a name="eglew.h-634"></a>#define EGL_ANGLE_window_fixed_size 1
<a name="eglew.h-635"></a>
<a name="eglew.h-636"></a>#define EGL_FIXED_SIZE_ANGLE 0x3201
<a name="eglew.h-637"></a>
<a name="eglew.h-638"></a>#define EGLEW_ANGLE_window_fixed_size EGLEW_GET_VAR(__EGLEW_ANGLE_window_fixed_size)
<a name="eglew.h-639"></a>
<a name="eglew.h-640"></a>#endif /* EGL_ANGLE_window_fixed_size */
<a name="eglew.h-641"></a>
<a name="eglew.h-642"></a>/* --------------------- EGL_ARM_implicit_external_sync -------------------- */
<a name="eglew.h-643"></a>
<a name="eglew.h-644"></a>#ifndef EGL_ARM_implicit_external_sync
<a name="eglew.h-645"></a>#define EGL_ARM_implicit_external_sync 1
<a name="eglew.h-646"></a>
<a name="eglew.h-647"></a>#define EGL_SYNC_PRIOR_COMMANDS_IMPLICIT_EXTERNAL_ARM 0x328A
<a name="eglew.h-648"></a>
<a name="eglew.h-649"></a>#define EGLEW_ARM_implicit_external_sync EGLEW_GET_VAR(__EGLEW_ARM_implicit_external_sync)
<a name="eglew.h-650"></a>
<a name="eglew.h-651"></a>#endif /* EGL_ARM_implicit_external_sync */
<a name="eglew.h-652"></a>
<a name="eglew.h-653"></a>/* ------------------- EGL_ARM_pixmap_multisample_discard ------------------ */
<a name="eglew.h-654"></a>
<a name="eglew.h-655"></a>#ifndef EGL_ARM_pixmap_multisample_discard
<a name="eglew.h-656"></a>#define EGL_ARM_pixmap_multisample_discard 1
<a name="eglew.h-657"></a>
<a name="eglew.h-658"></a>#define EGL_DISCARD_SAMPLES_ARM 0x3286
<a name="eglew.h-659"></a>
<a name="eglew.h-660"></a>#define EGLEW_ARM_pixmap_multisample_discard EGLEW_GET_VAR(__EGLEW_ARM_pixmap_multisample_discard)
<a name="eglew.h-661"></a>
<a name="eglew.h-662"></a>#endif /* EGL_ARM_pixmap_multisample_discard */
<a name="eglew.h-663"></a>
<a name="eglew.h-664"></a>/* --------------------------- EGL_EXT_buffer_age -------------------------- */
<a name="eglew.h-665"></a>
<a name="eglew.h-666"></a>#ifndef EGL_EXT_buffer_age
<a name="eglew.h-667"></a>#define EGL_EXT_buffer_age 1
<a name="eglew.h-668"></a>
<a name="eglew.h-669"></a>#define EGL_BUFFER_AGE_EXT 0x313D
<a name="eglew.h-670"></a>
<a name="eglew.h-671"></a>#define EGLEW_EXT_buffer_age EGLEW_GET_VAR(__EGLEW_EXT_buffer_age)
<a name="eglew.h-672"></a>
<a name="eglew.h-673"></a>#endif /* EGL_EXT_buffer_age */
<a name="eglew.h-674"></a>
<a name="eglew.h-675"></a>/* ----------------------- EGL_EXT_client_extensions ----------------------- */
<a name="eglew.h-676"></a>
<a name="eglew.h-677"></a>#ifndef EGL_EXT_client_extensions
<a name="eglew.h-678"></a>#define EGL_EXT_client_extensions 1
<a name="eglew.h-679"></a>
<a name="eglew.h-680"></a>#define EGLEW_EXT_client_extensions EGLEW_GET_VAR(__EGLEW_EXT_client_extensions)
<a name="eglew.h-681"></a>
<a name="eglew.h-682"></a>#endif /* EGL_EXT_client_extensions */
<a name="eglew.h-683"></a>
<a name="eglew.h-684"></a>/* ------------------- EGL_EXT_create_context_robustness ------------------- */
<a name="eglew.h-685"></a>
<a name="eglew.h-686"></a>#ifndef EGL_EXT_create_context_robustness
<a name="eglew.h-687"></a>#define EGL_EXT_create_context_robustness 1
<a name="eglew.h-688"></a>
<a name="eglew.h-689"></a>#define EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT 0x30BF
<a name="eglew.h-690"></a>#define EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT 0x3138
<a name="eglew.h-691"></a>#define EGL_NO_RESET_NOTIFICATION_EXT 0x31BE
<a name="eglew.h-692"></a>#define EGL_LOSE_CONTEXT_ON_RESET_EXT 0x31BF
<a name="eglew.h-693"></a>
<a name="eglew.h-694"></a>#define EGLEW_EXT_create_context_robustness EGLEW_GET_VAR(__EGLEW_EXT_create_context_robustness)
<a name="eglew.h-695"></a>
<a name="eglew.h-696"></a>#endif /* EGL_EXT_create_context_robustness */
<a name="eglew.h-697"></a>
<a name="eglew.h-698"></a>/* -------------------------- EGL_EXT_device_base -------------------------- */
<a name="eglew.h-699"></a>
<a name="eglew.h-700"></a>#ifndef EGL_EXT_device_base
<a name="eglew.h-701"></a>#define EGL_EXT_device_base 1
<a name="eglew.h-702"></a>
<a name="eglew.h-703"></a>#define EGL_BAD_DEVICE_EXT 0x322B
<a name="eglew.h-704"></a>#define EGL_DEVICE_EXT 0x322C
<a name="eglew.h-705"></a>
<a name="eglew.h-706"></a>#define EGLEW_EXT_device_base EGLEW_GET_VAR(__EGLEW_EXT_device_base)
<a name="eglew.h-707"></a>
<a name="eglew.h-708"></a>#endif /* EGL_EXT_device_base */
<a name="eglew.h-709"></a>
<a name="eglew.h-710"></a>/* --------------------------- EGL_EXT_device_drm -------------------------- */
<a name="eglew.h-711"></a>
<a name="eglew.h-712"></a>#ifndef EGL_EXT_device_drm
<a name="eglew.h-713"></a>#define EGL_EXT_device_drm 1
<a name="eglew.h-714"></a>
<a name="eglew.h-715"></a>#define EGL_DRM_DEVICE_FILE_EXT 0x3233
<a name="eglew.h-716"></a>
<a name="eglew.h-717"></a>#define EGLEW_EXT_device_drm EGLEW_GET_VAR(__EGLEW_EXT_device_drm)
<a name="eglew.h-718"></a>
<a name="eglew.h-719"></a>#endif /* EGL_EXT_device_drm */
<a name="eglew.h-720"></a>
<a name="eglew.h-721"></a>/* ----------------------- EGL_EXT_device_enumeration ---------------------- */
<a name="eglew.h-722"></a>
<a name="eglew.h-723"></a>#ifndef EGL_EXT_device_enumeration
<a name="eglew.h-724"></a>#define EGL_EXT_device_enumeration 1
<a name="eglew.h-725"></a>
<a name="eglew.h-726"></a>typedef EGLBoolean  ( * PFNEGLQUERYDEVICESEXTPROC) (EGLint  max_devices, EGLDeviceEXT * devices, EGLint * num_devices);
<a name="eglew.h-727"></a>
<a name="eglew.h-728"></a>#define eglQueryDevicesEXT EGLEW_GET_FUN(__eglewQueryDevicesEXT)
<a name="eglew.h-729"></a>
<a name="eglew.h-730"></a>#define EGLEW_EXT_device_enumeration EGLEW_GET_VAR(__EGLEW_EXT_device_enumeration)
<a name="eglew.h-731"></a>
<a name="eglew.h-732"></a>#endif /* EGL_EXT_device_enumeration */
<a name="eglew.h-733"></a>
<a name="eglew.h-734"></a>/* ------------------------- EGL_EXT_device_openwf ------------------------- */
<a name="eglew.h-735"></a>
<a name="eglew.h-736"></a>#ifndef EGL_EXT_device_openwf
<a name="eglew.h-737"></a>#define EGL_EXT_device_openwf 1
<a name="eglew.h-738"></a>
<a name="eglew.h-739"></a>#define EGL_OPENWF_DEVICE_ID_EXT 0x3237
<a name="eglew.h-740"></a>
<a name="eglew.h-741"></a>#define EGLEW_EXT_device_openwf EGLEW_GET_VAR(__EGLEW_EXT_device_openwf)
<a name="eglew.h-742"></a>
<a name="eglew.h-743"></a>#endif /* EGL_EXT_device_openwf */
<a name="eglew.h-744"></a>
<a name="eglew.h-745"></a>/* -------------------------- EGL_EXT_device_query ------------------------- */
<a name="eglew.h-746"></a>
<a name="eglew.h-747"></a>#ifndef EGL_EXT_device_query
<a name="eglew.h-748"></a>#define EGL_EXT_device_query 1
<a name="eglew.h-749"></a>
<a name="eglew.h-750"></a>#define EGL_BAD_DEVICE_EXT 0x322B
<a name="eglew.h-751"></a>#define EGL_DEVICE_EXT 0x322C
<a name="eglew.h-752"></a>
<a name="eglew.h-753"></a>typedef EGLBoolean  ( * PFNEGLQUERYDEVICEATTRIBEXTPROC) (EGLDeviceEXT  device, EGLint  attribute, EGLAttrib * value);
<a name="eglew.h-754"></a>typedef const char * ( * PFNEGLQUERYDEVICESTRINGEXTPROC) (EGLDeviceEXT  device, EGLint  name);
<a name="eglew.h-755"></a>typedef EGLBoolean  ( * PFNEGLQUERYDISPLAYATTRIBEXTPROC) (EGLDisplay  dpy, EGLint  attribute, EGLAttrib * value);
<a name="eglew.h-756"></a>
<a name="eglew.h-757"></a>#define eglQueryDeviceAttribEXT EGLEW_GET_FUN(__eglewQueryDeviceAttribEXT)
<a name="eglew.h-758"></a>#define eglQueryDeviceStringEXT EGLEW_GET_FUN(__eglewQueryDeviceStringEXT)
<a name="eglew.h-759"></a>#define eglQueryDisplayAttribEXT EGLEW_GET_FUN(__eglewQueryDisplayAttribEXT)
<a name="eglew.h-760"></a>
<a name="eglew.h-761"></a>#define EGLEW_EXT_device_query EGLEW_GET_VAR(__EGLEW_EXT_device_query)
<a name="eglew.h-762"></a>
<a name="eglew.h-763"></a>#endif /* EGL_EXT_device_query */
<a name="eglew.h-764"></a>
<a name="eglew.h-765"></a>/* ------------------ EGL_EXT_gl_colorspace_bt2020_linear ------------------ */
<a name="eglew.h-766"></a>
<a name="eglew.h-767"></a>#ifndef EGL_EXT_gl_colorspace_bt2020_linear
<a name="eglew.h-768"></a>#define EGL_EXT_gl_colorspace_bt2020_linear 1
<a name="eglew.h-769"></a>
<a name="eglew.h-770"></a>#define EGL_GL_COLORSPACE_BT2020_LINEAR_EXT 0x333F
<a name="eglew.h-771"></a>
<a name="eglew.h-772"></a>#define EGLEW_EXT_gl_colorspace_bt2020_linear EGLEW_GET_VAR(__EGLEW_EXT_gl_colorspace_bt2020_linear)
<a name="eglew.h-773"></a>
<a name="eglew.h-774"></a>#endif /* EGL_EXT_gl_colorspace_bt2020_linear */
<a name="eglew.h-775"></a>
<a name="eglew.h-776"></a>/* -------------------- EGL_EXT_gl_colorspace_bt2020_pq -------------------- */
<a name="eglew.h-777"></a>
<a name="eglew.h-778"></a>#ifndef EGL_EXT_gl_colorspace_bt2020_pq
<a name="eglew.h-779"></a>#define EGL_EXT_gl_colorspace_bt2020_pq 1
<a name="eglew.h-780"></a>
<a name="eglew.h-781"></a>#define EGL_GL_COLORSPACE_BT2020_PQ_EXT 0x3340
<a name="eglew.h-782"></a>
<a name="eglew.h-783"></a>#define EGLEW_EXT_gl_colorspace_bt2020_pq EGLEW_GET_VAR(__EGLEW_EXT_gl_colorspace_bt2020_pq)
<a name="eglew.h-784"></a>
<a name="eglew.h-785"></a>#endif /* EGL_EXT_gl_colorspace_bt2020_pq */
<a name="eglew.h-786"></a>
<a name="eglew.h-787"></a>/* ------------------- EGL_EXT_gl_colorspace_scrgb_linear ------------------ */
<a name="eglew.h-788"></a>
<a name="eglew.h-789"></a>#ifndef EGL_EXT_gl_colorspace_scrgb_linear
<a name="eglew.h-790"></a>#define EGL_EXT_gl_colorspace_scrgb_linear 1
<a name="eglew.h-791"></a>
<a name="eglew.h-792"></a>#define EGL_GL_COLORSPACE_SCRGB_LINEAR_EXT 0x3350
<a name="eglew.h-793"></a>
<a name="eglew.h-794"></a>#define EGLEW_EXT_gl_colorspace_scrgb_linear EGLEW_GET_VAR(__EGLEW_EXT_gl_colorspace_scrgb_linear)
<a name="eglew.h-795"></a>
<a name="eglew.h-796"></a>#endif /* EGL_EXT_gl_colorspace_scrgb_linear */
<a name="eglew.h-797"></a>
<a name="eglew.h-798"></a>/* ---------------------- EGL_EXT_image_dma_buf_import --------------------- */
<a name="eglew.h-799"></a>
<a name="eglew.h-800"></a>#ifndef EGL_EXT_image_dma_buf_import
<a name="eglew.h-801"></a>#define EGL_EXT_image_dma_buf_import 1
<a name="eglew.h-802"></a>
<a name="eglew.h-803"></a>#define EGL_LINUX_DMA_BUF_EXT 0x3270
<a name="eglew.h-804"></a>#define EGL_LINUX_DRM_FOURCC_EXT 0x3271
<a name="eglew.h-805"></a>#define EGL_DMA_BUF_PLANE0_FD_EXT 0x3272
<a name="eglew.h-806"></a>#define EGL_DMA_BUF_PLANE0_OFFSET_EXT 0x3273
<a name="eglew.h-807"></a>#define EGL_DMA_BUF_PLANE0_PITCH_EXT 0x3274
<a name="eglew.h-808"></a>#define EGL_DMA_BUF_PLANE1_FD_EXT 0x3275
<a name="eglew.h-809"></a>#define EGL_DMA_BUF_PLANE1_OFFSET_EXT 0x3276
<a name="eglew.h-810"></a>#define EGL_DMA_BUF_PLANE1_PITCH_EXT 0x3277
<a name="eglew.h-811"></a>#define EGL_DMA_BUF_PLANE2_FD_EXT 0x3278
<a name="eglew.h-812"></a>#define EGL_DMA_BUF_PLANE2_OFFSET_EXT 0x3279
<a name="eglew.h-813"></a>#define EGL_DMA_BUF_PLANE2_PITCH_EXT 0x327A
<a name="eglew.h-814"></a>#define EGL_YUV_COLOR_SPACE_HINT_EXT 0x327B
<a name="eglew.h-815"></a>#define EGL_SAMPLE_RANGE_HINT_EXT 0x327C
<a name="eglew.h-816"></a>#define EGL_YUV_CHROMA_HORIZONTAL_SITING_HINT_EXT 0x327D
<a name="eglew.h-817"></a>#define EGL_YUV_CHROMA_VERTICAL_SITING_HINT_EXT 0x327E
<a name="eglew.h-818"></a>#define EGL_ITU_REC601_EXT 0x327F
<a name="eglew.h-819"></a>#define EGL_ITU_REC709_EXT 0x3280
<a name="eglew.h-820"></a>#define EGL_ITU_REC2020_EXT 0x3281
<a name="eglew.h-821"></a>#define EGL_YUV_FULL_RANGE_EXT 0x3282
<a name="eglew.h-822"></a>#define EGL_YUV_NARROW_RANGE_EXT 0x3283
<a name="eglew.h-823"></a>#define EGL_YUV_CHROMA_SITING_0_EXT 0x3284
<a name="eglew.h-824"></a>#define EGL_YUV_CHROMA_SITING_0_5_EXT 0x3285
<a name="eglew.h-825"></a>
<a name="eglew.h-826"></a>#define EGLEW_EXT_image_dma_buf_import EGLEW_GET_VAR(__EGLEW_EXT_image_dma_buf_import)
<a name="eglew.h-827"></a>
<a name="eglew.h-828"></a>#endif /* EGL_EXT_image_dma_buf_import */
<a name="eglew.h-829"></a>
<a name="eglew.h-830"></a>/* ----------------- EGL_EXT_image_dma_buf_import_modifiers ---------------- */
<a name="eglew.h-831"></a>
<a name="eglew.h-832"></a>#ifndef EGL_EXT_image_dma_buf_import_modifiers
<a name="eglew.h-833"></a>#define EGL_EXT_image_dma_buf_import_modifiers 1
<a name="eglew.h-834"></a>
<a name="eglew.h-835"></a>#define EGL_DMA_BUF_PLANE3_FD_EXT 0x3440
<a name="eglew.h-836"></a>#define EGL_DMA_BUF_PLANE3_OFFSET_EXT 0x3441
<a name="eglew.h-837"></a>#define EGL_DMA_BUF_PLANE3_PITCH_EXT 0x3442
<a name="eglew.h-838"></a>#define EGL_DMA_BUF_PLANE0_MODIFIER_LO_EXT 0x3443
<a name="eglew.h-839"></a>#define EGL_DMA_BUF_PLANE0_MODIFIER_HI_EXT 0x3444
<a name="eglew.h-840"></a>#define EGL_DMA_BUF_PLANE1_MODIFIER_LO_EXT 0x3445
<a name="eglew.h-841"></a>#define EGL_DMA_BUF_PLANE1_MODIFIER_HI_EXT 0x3446
<a name="eglew.h-842"></a>#define EGL_DMA_BUF_PLANE2_MODIFIER_LO_EXT 0x3447
<a name="eglew.h-843"></a>#define EGL_DMA_BUF_PLANE2_MODIFIER_HI_EXT 0x3448
<a name="eglew.h-844"></a>#define EGL_DMA_BUF_PLANE3_MODIFIER_LO_EXT 0x3449
<a name="eglew.h-845"></a>#define EGL_DMA_BUF_PLANE3_MODIFIER_HI_EXT 0x344A
<a name="eglew.h-846"></a>
<a name="eglew.h-847"></a>typedef EGLBoolean  ( * PFNEGLQUERYDMABUFFORMATSEXTPROC) (EGLDisplay  dpy, EGLint  max_formats, EGLint  *formats, EGLint  *num_formats);
<a name="eglew.h-848"></a>typedef EGLBoolean  ( * PFNEGLQUERYDMABUFMODIFIERSEXTPROC) (EGLDisplay  dpy, EGLint  format, EGLint  max_modifiers, EGLuint64KHR  *modifiers, EGLBoolean  *external_only, EGLint  *num_modifiers);
<a name="eglew.h-849"></a>
<a name="eglew.h-850"></a>#define eglQueryDmaBufFormatsEXT EGLEW_GET_FUN(__eglewQueryDmaBufFormatsEXT)
<a name="eglew.h-851"></a>#define eglQueryDmaBufModifiersEXT EGLEW_GET_FUN(__eglewQueryDmaBufModifiersEXT)
<a name="eglew.h-852"></a>
<a name="eglew.h-853"></a>#define EGLEW_EXT_image_dma_buf_import_modifiers EGLEW_GET_VAR(__EGLEW_EXT_image_dma_buf_import_modifiers)
<a name="eglew.h-854"></a>
<a name="eglew.h-855"></a>#endif /* EGL_EXT_image_dma_buf_import_modifiers */
<a name="eglew.h-856"></a>
<a name="eglew.h-857"></a>/* ------------------------ EGL_EXT_multiview_window ----------------------- */
<a name="eglew.h-858"></a>
<a name="eglew.h-859"></a>#ifndef EGL_EXT_multiview_window
<a name="eglew.h-860"></a>#define EGL_EXT_multiview_window 1
<a name="eglew.h-861"></a>
<a name="eglew.h-862"></a>#define EGL_MULTIVIEW_VIEW_COUNT_EXT 0x3134
<a name="eglew.h-863"></a>
<a name="eglew.h-864"></a>#define EGLEW_EXT_multiview_window EGLEW_GET_VAR(__EGLEW_EXT_multiview_window)
<a name="eglew.h-865"></a>
<a name="eglew.h-866"></a>#endif /* EGL_EXT_multiview_window */
<a name="eglew.h-867"></a>
<a name="eglew.h-868"></a>/* -------------------------- EGL_EXT_output_base -------------------------- */
<a name="eglew.h-869"></a>
<a name="eglew.h-870"></a>#ifndef EGL_EXT_output_base
<a name="eglew.h-871"></a>#define EGL_EXT_output_base 1
<a name="eglew.h-872"></a>
<a name="eglew.h-873"></a>#define EGL_BAD_OUTPUT_LAYER_EXT 0x322D
<a name="eglew.h-874"></a>#define EGL_BAD_OUTPUT_PORT_EXT 0x322E
<a name="eglew.h-875"></a>#define EGL_SWAP_INTERVAL_EXT 0x322F
<a name="eglew.h-876"></a>
<a name="eglew.h-877"></a>typedef EGLBoolean  ( * PFNEGLGETOUTPUTLAYERSEXTPROC) (EGLDisplay  dpy, const EGLAttrib * attrib_list, EGLOutputLayerEXT * layers, EGLint  max_layers, EGLint * num_layers);
<a name="eglew.h-878"></a>typedef EGLBoolean  ( * PFNEGLGETOUTPUTPORTSEXTPROC) (EGLDisplay  dpy, const EGLAttrib * attrib_list, EGLOutputPortEXT * ports, EGLint  max_ports, EGLint * num_ports);
<a name="eglew.h-879"></a>typedef EGLBoolean  ( * PFNEGLOUTPUTLAYERATTRIBEXTPROC) (EGLDisplay  dpy, EGLOutputLayerEXT  layer, EGLint  attribute, EGLAttrib  value);
<a name="eglew.h-880"></a>typedef EGLBoolean  ( * PFNEGLOUTPUTPORTATTRIBEXTPROC) (EGLDisplay  dpy, EGLOutputPortEXT  port, EGLint  attribute, EGLAttrib  value);
<a name="eglew.h-881"></a>typedef EGLBoolean  ( * PFNEGLQUERYOUTPUTLAYERATTRIBEXTPROC) (EGLDisplay  dpy, EGLOutputLayerEXT  layer, EGLint  attribute, EGLAttrib * value);
<a name="eglew.h-882"></a>typedef const char * ( * PFNEGLQUERYOUTPUTLAYERSTRINGEXTPROC) (EGLDisplay  dpy, EGLOutputLayerEXT  layer, EGLint  name);
<a name="eglew.h-883"></a>typedef EGLBoolean  ( * PFNEGLQUERYOUTPUTPORTATTRIBEXTPROC) (EGLDisplay  dpy, EGLOutputPortEXT  port, EGLint  attribute, EGLAttrib * value);
<a name="eglew.h-884"></a>typedef const char * ( * PFNEGLQUERYOUTPUTPORTSTRINGEXTPROC) (EGLDisplay  dpy, EGLOutputPortEXT  port, EGLint  name);
<a name="eglew.h-885"></a>
<a name="eglew.h-886"></a>#define eglGetOutputLayersEXT EGLEW_GET_FUN(__eglewGetOutputLayersEXT)
<a name="eglew.h-887"></a>#define eglGetOutputPortsEXT EGLEW_GET_FUN(__eglewGetOutputPortsEXT)
<a name="eglew.h-888"></a>#define eglOutputLayerAttribEXT EGLEW_GET_FUN(__eglewOutputLayerAttribEXT)
<a name="eglew.h-889"></a>#define eglOutputPortAttribEXT EGLEW_GET_FUN(__eglewOutputPortAttribEXT)
<a name="eglew.h-890"></a>#define eglQueryOutputLayerAttribEXT EGLEW_GET_FUN(__eglewQueryOutputLayerAttribEXT)
<a name="eglew.h-891"></a>#define eglQueryOutputLayerStringEXT EGLEW_GET_FUN(__eglewQueryOutputLayerStringEXT)
<a name="eglew.h-892"></a>#define eglQueryOutputPortAttribEXT EGLEW_GET_FUN(__eglewQueryOutputPortAttribEXT)
<a name="eglew.h-893"></a>#define eglQueryOutputPortStringEXT EGLEW_GET_FUN(__eglewQueryOutputPortStringEXT)
<a name="eglew.h-894"></a>
<a name="eglew.h-895"></a>#define EGLEW_EXT_output_base EGLEW_GET_VAR(__EGLEW_EXT_output_base)
<a name="eglew.h-896"></a>
<a name="eglew.h-897"></a>#endif /* EGL_EXT_output_base */
<a name="eglew.h-898"></a>
<a name="eglew.h-899"></a>/* --------------------------- EGL_EXT_output_drm -------------------------- */
<a name="eglew.h-900"></a>
<a name="eglew.h-901"></a>#ifndef EGL_EXT_output_drm
<a name="eglew.h-902"></a>#define EGL_EXT_output_drm 1
<a name="eglew.h-903"></a>
<a name="eglew.h-904"></a>#define EGL_DRM_CRTC_EXT 0x3234
<a name="eglew.h-905"></a>#define EGL_DRM_PLANE_EXT 0x3235
<a name="eglew.h-906"></a>#define EGL_DRM_CONNECTOR_EXT 0x3236
<a name="eglew.h-907"></a>
<a name="eglew.h-908"></a>#define EGLEW_EXT_output_drm EGLEW_GET_VAR(__EGLEW_EXT_output_drm)
<a name="eglew.h-909"></a>
<a name="eglew.h-910"></a>#endif /* EGL_EXT_output_drm */
<a name="eglew.h-911"></a>
<a name="eglew.h-912"></a>/* ------------------------- EGL_EXT_output_openwf ------------------------- */
<a name="eglew.h-913"></a>
<a name="eglew.h-914"></a>#ifndef EGL_EXT_output_openwf
<a name="eglew.h-915"></a>#define EGL_EXT_output_openwf 1
<a name="eglew.h-916"></a>
<a name="eglew.h-917"></a>#define EGL_OPENWF_PIPELINE_ID_EXT 0x3238
<a name="eglew.h-918"></a>#define EGL_OPENWF_PORT_ID_EXT 0x3239
<a name="eglew.h-919"></a>
<a name="eglew.h-920"></a>#define EGLEW_EXT_output_openwf EGLEW_GET_VAR(__EGLEW_EXT_output_openwf)
<a name="eglew.h-921"></a>
<a name="eglew.h-922"></a>#endif /* EGL_EXT_output_openwf */
<a name="eglew.h-923"></a>
<a name="eglew.h-924"></a>/* ----------------------- EGL_EXT_pixel_format_float ---------------------- */
<a name="eglew.h-925"></a>
<a name="eglew.h-926"></a>#ifndef EGL_EXT_pixel_format_float
<a name="eglew.h-927"></a>#define EGL_EXT_pixel_format_float 1
<a name="eglew.h-928"></a>
<a name="eglew.h-929"></a>#define EGL_COLOR_COMPONENT_TYPE_EXT 0x3339
<a name="eglew.h-930"></a>#define EGL_COLOR_COMPONENT_TYPE_FIXED_EXT 0x333A
<a name="eglew.h-931"></a>#define EGL_COLOR_COMPONENT_TYPE_FLOAT_EXT 0x333B
<a name="eglew.h-932"></a>
<a name="eglew.h-933"></a>#define EGLEW_EXT_pixel_format_float EGLEW_GET_VAR(__EGLEW_EXT_pixel_format_float)
<a name="eglew.h-934"></a>
<a name="eglew.h-935"></a>#endif /* EGL_EXT_pixel_format_float */
<a name="eglew.h-936"></a>
<a name="eglew.h-937"></a>/* ------------------------- EGL_EXT_platform_base ------------------------- */
<a name="eglew.h-938"></a>
<a name="eglew.h-939"></a>#ifndef EGL_EXT_platform_base
<a name="eglew.h-940"></a>#define EGL_EXT_platform_base 1
<a name="eglew.h-941"></a>
<a name="eglew.h-942"></a>typedef EGLSurface  ( * PFNEGLCREATEPLATFORMPIXMAPSURFACEEXTPROC) (EGLDisplay  dpy, EGLConfig  config, void * native_pixmap, const EGLint * attrib_list);
<a name="eglew.h-943"></a>typedef EGLSurface  ( * PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC) (EGLDisplay  dpy, EGLConfig  config, void * native_window, const EGLint * attrib_list);
<a name="eglew.h-944"></a>typedef EGLDisplay  ( * PFNEGLGETPLATFORMDISPLAYEXTPROC) (EGLenum  platform, void * native_display, const EGLint * attrib_list);
<a name="eglew.h-945"></a>
<a name="eglew.h-946"></a>#define eglCreatePlatformPixmapSurfaceEXT EGLEW_GET_FUN(__eglewCreatePlatformPixmapSurfaceEXT)
<a name="eglew.h-947"></a>#define eglCreatePlatformWindowSurfaceEXT EGLEW_GET_FUN(__eglewCreatePlatformWindowSurfaceEXT)
<a name="eglew.h-948"></a>#define eglGetPlatformDisplayEXT EGLEW_GET_FUN(__eglewGetPlatformDisplayEXT)
<a name="eglew.h-949"></a>
<a name="eglew.h-950"></a>#define EGLEW_EXT_platform_base EGLEW_GET_VAR(__EGLEW_EXT_platform_base)
<a name="eglew.h-951"></a>
<a name="eglew.h-952"></a>#endif /* EGL_EXT_platform_base */
<a name="eglew.h-953"></a>
<a name="eglew.h-954"></a>/* ------------------------ EGL_EXT_platform_device ------------------------ */
<a name="eglew.h-955"></a>
<a name="eglew.h-956"></a>#ifndef EGL_EXT_platform_device
<a name="eglew.h-957"></a>#define EGL_EXT_platform_device 1
<a name="eglew.h-958"></a>
<a name="eglew.h-959"></a>#define EGL_PLATFORM_DEVICE_EXT 0x313F
<a name="eglew.h-960"></a>
<a name="eglew.h-961"></a>#define EGLEW_EXT_platform_device EGLEW_GET_VAR(__EGLEW_EXT_platform_device)
<a name="eglew.h-962"></a>
<a name="eglew.h-963"></a>#endif /* EGL_EXT_platform_device */
<a name="eglew.h-964"></a>
<a name="eglew.h-965"></a>/* ------------------------ EGL_EXT_platform_wayland ----------------------- */
<a name="eglew.h-966"></a>
<a name="eglew.h-967"></a>#ifndef EGL_EXT_platform_wayland
<a name="eglew.h-968"></a>#define EGL_EXT_platform_wayland 1
<a name="eglew.h-969"></a>
<a name="eglew.h-970"></a>#define EGL_PLATFORM_WAYLAND_EXT 0x31D8
<a name="eglew.h-971"></a>
<a name="eglew.h-972"></a>#define EGLEW_EXT_platform_wayland EGLEW_GET_VAR(__EGLEW_EXT_platform_wayland)
<a name="eglew.h-973"></a>
<a name="eglew.h-974"></a>#endif /* EGL_EXT_platform_wayland */
<a name="eglew.h-975"></a>
<a name="eglew.h-976"></a>/* -------------------------- EGL_EXT_platform_x11 ------------------------- */
<a name="eglew.h-977"></a>
<a name="eglew.h-978"></a>#ifndef EGL_EXT_platform_x11
<a name="eglew.h-979"></a>#define EGL_EXT_platform_x11 1
<a name="eglew.h-980"></a>
<a name="eglew.h-981"></a>#define EGL_PLATFORM_X11_EXT 0x31D5
<a name="eglew.h-982"></a>#define EGL_PLATFORM_X11_SCREEN_EXT 0x31D6
<a name="eglew.h-983"></a>
<a name="eglew.h-984"></a>#define EGLEW_EXT_platform_x11 EGLEW_GET_VAR(__EGLEW_EXT_platform_x11)
<a name="eglew.h-985"></a>
<a name="eglew.h-986"></a>#endif /* EGL_EXT_platform_x11 */
<a name="eglew.h-987"></a>
<a name="eglew.h-988"></a>/* ----------------------- EGL_EXT_protected_content ----------------------- */
<a name="eglew.h-989"></a>
<a name="eglew.h-990"></a>#ifndef EGL_EXT_protected_content
<a name="eglew.h-991"></a>#define EGL_EXT_protected_content 1
<a name="eglew.h-992"></a>
<a name="eglew.h-993"></a>#define EGL_PROTECTED_CONTENT_EXT 0x32C0
<a name="eglew.h-994"></a>
<a name="eglew.h-995"></a>#define EGLEW_EXT_protected_content EGLEW_GET_VAR(__EGLEW_EXT_protected_content)
<a name="eglew.h-996"></a>
<a name="eglew.h-997"></a>#endif /* EGL_EXT_protected_content */
<a name="eglew.h-998"></a>
<a name="eglew.h-999"></a>/* ----------------------- EGL_EXT_protected_surface ----------------------- */
<a name="eglew.h-1000"></a>
<a name="eglew.h-1001"></a>#ifndef EGL_EXT_protected_surface
<a name="eglew.h-1002"></a>#define EGL_EXT_protected_surface 1
<a name="eglew.h-1003"></a>
<a name="eglew.h-1004"></a>#define EGL_PROTECTED_CONTENT_EXT 0x32C0
<a name="eglew.h-1005"></a>
<a name="eglew.h-1006"></a>#define EGLEW_EXT_protected_surface EGLEW_GET_VAR(__EGLEW_EXT_protected_surface)
<a name="eglew.h-1007"></a>
<a name="eglew.h-1008"></a>#endif /* EGL_EXT_protected_surface */
<a name="eglew.h-1009"></a>
<a name="eglew.h-1010"></a>/* ------------------- EGL_EXT_stream_consumer_egloutput ------------------- */
<a name="eglew.h-1011"></a>
<a name="eglew.h-1012"></a>#ifndef EGL_EXT_stream_consumer_egloutput
<a name="eglew.h-1013"></a>#define EGL_EXT_stream_consumer_egloutput 1
<a name="eglew.h-1014"></a>
<a name="eglew.h-1015"></a>typedef EGLBoolean  ( * PFNEGLSTREAMCONSUMEROUTPUTEXTPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLOutputLayerEXT  layer);
<a name="eglew.h-1016"></a>
<a name="eglew.h-1017"></a>#define eglStreamConsumerOutputEXT EGLEW_GET_FUN(__eglewStreamConsumerOutputEXT)
<a name="eglew.h-1018"></a>
<a name="eglew.h-1019"></a>#define EGLEW_EXT_stream_consumer_egloutput EGLEW_GET_VAR(__EGLEW_EXT_stream_consumer_egloutput)
<a name="eglew.h-1020"></a>
<a name="eglew.h-1021"></a>#endif /* EGL_EXT_stream_consumer_egloutput */
<a name="eglew.h-1022"></a>
<a name="eglew.h-1023"></a>/* ------------------- EGL_EXT_surface_SMPTE2086_metadata ------------------ */
<a name="eglew.h-1024"></a>
<a name="eglew.h-1025"></a>#ifndef EGL_EXT_surface_SMPTE2086_metadata
<a name="eglew.h-1026"></a>#define EGL_EXT_surface_SMPTE2086_metadata 1
<a name="eglew.h-1027"></a>
<a name="eglew.h-1028"></a>#define EGL_SMPTE2086_DISPLAY_PRIMARY_RX_EXT 0x3341
<a name="eglew.h-1029"></a>#define EGL_SMPTE2086_DISPLAY_PRIMARY_RY_EXT 0x3342
<a name="eglew.h-1030"></a>#define EGL_SMPTE2086_DISPLAY_PRIMARY_GX_EXT 0x3343
<a name="eglew.h-1031"></a>#define EGL_SMPTE2086_DISPLAY_PRIMARY_GY_EXT 0x3344
<a name="eglew.h-1032"></a>#define EGL_SMPTE2086_DISPLAY_PRIMARY_BX_EXT 0x3345
<a name="eglew.h-1033"></a>#define EGL_SMPTE2086_DISPLAY_PRIMARY_BY_EXT 0x3346
<a name="eglew.h-1034"></a>#define EGL_SMPTE2086_WHITE_POINT_X_EXT 0x3347
<a name="eglew.h-1035"></a>#define EGL_SMPTE2086_WHITE_POINT_Y_EXT 0x3348
<a name="eglew.h-1036"></a>#define EGL_SMPTE2086_MAX_LUMINANCE_EXT 0x3349
<a name="eglew.h-1037"></a>#define EGL_SMPTE2086_MIN_LUMINANCE_EXT 0x334A
<a name="eglew.h-1038"></a>
<a name="eglew.h-1039"></a>#define EGLEW_EXT_surface_SMPTE2086_metadata EGLEW_GET_VAR(__EGLEW_EXT_surface_SMPTE2086_metadata)
<a name="eglew.h-1040"></a>
<a name="eglew.h-1041"></a>#endif /* EGL_EXT_surface_SMPTE2086_metadata */
<a name="eglew.h-1042"></a>
<a name="eglew.h-1043"></a>/* -------------------- EGL_EXT_swap_buffers_with_damage ------------------- */
<a name="eglew.h-1044"></a>
<a name="eglew.h-1045"></a>#ifndef EGL_EXT_swap_buffers_with_damage
<a name="eglew.h-1046"></a>#define EGL_EXT_swap_buffers_with_damage 1
<a name="eglew.h-1047"></a>
<a name="eglew.h-1048"></a>typedef EGLBoolean  ( * PFNEGLSWAPBUFFERSWITHDAMAGEEXTPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint * rects, EGLint  n_rects);
<a name="eglew.h-1049"></a>
<a name="eglew.h-1050"></a>#define eglSwapBuffersWithDamageEXT EGLEW_GET_FUN(__eglewSwapBuffersWithDamageEXT)
<a name="eglew.h-1051"></a>
<a name="eglew.h-1052"></a>#define EGLEW_EXT_swap_buffers_with_damage EGLEW_GET_VAR(__EGLEW_EXT_swap_buffers_with_damage)
<a name="eglew.h-1053"></a>
<a name="eglew.h-1054"></a>#endif /* EGL_EXT_swap_buffers_with_damage */
<a name="eglew.h-1055"></a>
<a name="eglew.h-1056"></a>/* -------------------------- EGL_EXT_yuv_surface -------------------------- */
<a name="eglew.h-1057"></a>
<a name="eglew.h-1058"></a>#ifndef EGL_EXT_yuv_surface
<a name="eglew.h-1059"></a>#define EGL_EXT_yuv_surface 1
<a name="eglew.h-1060"></a>
<a name="eglew.h-1061"></a>#define EGL_YUV_BUFFER_EXT 0x3300
<a name="eglew.h-1062"></a>#define EGL_YUV_ORDER_EXT 0x3301
<a name="eglew.h-1063"></a>#define EGL_YUV_ORDER_YUV_EXT 0x3302
<a name="eglew.h-1064"></a>#define EGL_YUV_ORDER_YVU_EXT 0x3303
<a name="eglew.h-1065"></a>#define EGL_YUV_ORDER_YUYV_EXT 0x3304
<a name="eglew.h-1066"></a>#define EGL_YUV_ORDER_UYVY_EXT 0x3305
<a name="eglew.h-1067"></a>#define EGL_YUV_ORDER_YVYU_EXT 0x3306
<a name="eglew.h-1068"></a>#define EGL_YUV_ORDER_VYUY_EXT 0x3307
<a name="eglew.h-1069"></a>#define EGL_YUV_ORDER_AYUV_EXT 0x3308
<a name="eglew.h-1070"></a>#define EGL_YUV_CSC_STANDARD_EXT 0x330A
<a name="eglew.h-1071"></a>#define EGL_YUV_CSC_STANDARD_601_EXT 0x330B
<a name="eglew.h-1072"></a>#define EGL_YUV_CSC_STANDARD_709_EXT 0x330C
<a name="eglew.h-1073"></a>#define EGL_YUV_CSC_STANDARD_2020_EXT 0x330D
<a name="eglew.h-1074"></a>#define EGL_YUV_NUMBER_OF_PLANES_EXT 0x3311
<a name="eglew.h-1075"></a>#define EGL_YUV_SUBSAMPLE_EXT 0x3312
<a name="eglew.h-1076"></a>#define EGL_YUV_SUBSAMPLE_4_2_0_EXT 0x3313
<a name="eglew.h-1077"></a>#define EGL_YUV_SUBSAMPLE_4_2_2_EXT 0x3314
<a name="eglew.h-1078"></a>#define EGL_YUV_SUBSAMPLE_4_4_4_EXT 0x3315
<a name="eglew.h-1079"></a>#define EGL_YUV_DEPTH_RANGE_EXT 0x3317
<a name="eglew.h-1080"></a>#define EGL_YUV_DEPTH_RANGE_LIMITED_EXT 0x3318
<a name="eglew.h-1081"></a>#define EGL_YUV_DEPTH_RANGE_FULL_EXT 0x3319
<a name="eglew.h-1082"></a>#define EGL_YUV_PLANE_BPP_EXT 0x331A
<a name="eglew.h-1083"></a>#define EGL_YUV_PLANE_BPP_0_EXT 0x331B
<a name="eglew.h-1084"></a>#define EGL_YUV_PLANE_BPP_8_EXT 0x331C
<a name="eglew.h-1085"></a>#define EGL_YUV_PLANE_BPP_10_EXT 0x331D
<a name="eglew.h-1086"></a>
<a name="eglew.h-1087"></a>#define EGLEW_EXT_yuv_surface EGLEW_GET_VAR(__EGLEW_EXT_yuv_surface)
<a name="eglew.h-1088"></a>
<a name="eglew.h-1089"></a>#endif /* EGL_EXT_yuv_surface */
<a name="eglew.h-1090"></a>
<a name="eglew.h-1091"></a>/* -------------------------- EGL_HI_clientpixmap -------------------------- */
<a name="eglew.h-1092"></a>
<a name="eglew.h-1093"></a>#ifndef EGL_HI_clientpixmap
<a name="eglew.h-1094"></a>#define EGL_HI_clientpixmap 1
<a name="eglew.h-1095"></a>
<a name="eglew.h-1096"></a>#define EGL_CLIENT_PIXMAP_POINTER_HI 0x8F74
<a name="eglew.h-1097"></a>
<a name="eglew.h-1098"></a>typedef EGLSurface  ( * PFNEGLCREATEPIXMAPSURFACEHIPROC) (EGLDisplay  dpy, EGLConfig  config, struct EGLClientPixmapHI * pixmap);
<a name="eglew.h-1099"></a>
<a name="eglew.h-1100"></a>#define eglCreatePixmapSurfaceHI EGLEW_GET_FUN(__eglewCreatePixmapSurfaceHI)
<a name="eglew.h-1101"></a>
<a name="eglew.h-1102"></a>#define EGLEW_HI_clientpixmap EGLEW_GET_VAR(__EGLEW_HI_clientpixmap)
<a name="eglew.h-1103"></a>
<a name="eglew.h-1104"></a>#endif /* EGL_HI_clientpixmap */
<a name="eglew.h-1105"></a>
<a name="eglew.h-1106"></a>/* -------------------------- EGL_HI_colorformats -------------------------- */
<a name="eglew.h-1107"></a>
<a name="eglew.h-1108"></a>#ifndef EGL_HI_colorformats
<a name="eglew.h-1109"></a>#define EGL_HI_colorformats 1
<a name="eglew.h-1110"></a>
<a name="eglew.h-1111"></a>#define EGL_COLOR_FORMAT_HI 0x8F70
<a name="eglew.h-1112"></a>#define EGL_COLOR_RGB_HI 0x8F71
<a name="eglew.h-1113"></a>#define EGL_COLOR_RGBA_HI 0x8F72
<a name="eglew.h-1114"></a>#define EGL_COLOR_ARGB_HI 0x8F73
<a name="eglew.h-1115"></a>
<a name="eglew.h-1116"></a>#define EGLEW_HI_colorformats EGLEW_GET_VAR(__EGLEW_HI_colorformats)
<a name="eglew.h-1117"></a>
<a name="eglew.h-1118"></a>#endif /* EGL_HI_colorformats */
<a name="eglew.h-1119"></a>
<a name="eglew.h-1120"></a>/* ------------------------ EGL_IMG_context_priority ----------------------- */
<a name="eglew.h-1121"></a>
<a name="eglew.h-1122"></a>#ifndef EGL_IMG_context_priority
<a name="eglew.h-1123"></a>#define EGL_IMG_context_priority 1
<a name="eglew.h-1124"></a>
<a name="eglew.h-1125"></a>#define EGL_CONTEXT_PRIORITY_LEVEL_IMG 0x3100
<a name="eglew.h-1126"></a>#define EGL_CONTEXT_PRIORITY_HIGH_IMG 0x3101
<a name="eglew.h-1127"></a>#define EGL_CONTEXT_PRIORITY_MEDIUM_IMG 0x3102
<a name="eglew.h-1128"></a>#define EGL_CONTEXT_PRIORITY_LOW_IMG 0x3103
<a name="eglew.h-1129"></a>
<a name="eglew.h-1130"></a>#define EGLEW_IMG_context_priority EGLEW_GET_VAR(__EGLEW_IMG_context_priority)
<a name="eglew.h-1131"></a>
<a name="eglew.h-1132"></a>#endif /* EGL_IMG_context_priority */
<a name="eglew.h-1133"></a>
<a name="eglew.h-1134"></a>/* ---------------------- EGL_IMG_image_plane_attribs ---------------------- */
<a name="eglew.h-1135"></a>
<a name="eglew.h-1136"></a>#ifndef EGL_IMG_image_plane_attribs
<a name="eglew.h-1137"></a>#define EGL_IMG_image_plane_attribs 1
<a name="eglew.h-1138"></a>
<a name="eglew.h-1139"></a>#define EGL_NATIVE_BUFFER_MULTIPLANE_SEPARATE_IMG 0x3105
<a name="eglew.h-1140"></a>#define EGL_NATIVE_BUFFER_PLANE_OFFSET_IMG 0x3106
<a name="eglew.h-1141"></a>
<a name="eglew.h-1142"></a>#define EGLEW_IMG_image_plane_attribs EGLEW_GET_VAR(__EGLEW_IMG_image_plane_attribs)
<a name="eglew.h-1143"></a>
<a name="eglew.h-1144"></a>#endif /* EGL_IMG_image_plane_attribs */
<a name="eglew.h-1145"></a>
<a name="eglew.h-1146"></a>/* ---------------------------- EGL_KHR_cl_event --------------------------- */
<a name="eglew.h-1147"></a>
<a name="eglew.h-1148"></a>#ifndef EGL_KHR_cl_event
<a name="eglew.h-1149"></a>#define EGL_KHR_cl_event 1
<a name="eglew.h-1150"></a>
<a name="eglew.h-1151"></a>#define EGL_CL_EVENT_HANDLE_KHR 0x309C
<a name="eglew.h-1152"></a>#define EGL_SYNC_CL_EVENT_KHR 0x30FE
<a name="eglew.h-1153"></a>#define EGL_SYNC_CL_EVENT_COMPLETE_KHR 0x30FF
<a name="eglew.h-1154"></a>
<a name="eglew.h-1155"></a>#define EGLEW_KHR_cl_event EGLEW_GET_VAR(__EGLEW_KHR_cl_event)
<a name="eglew.h-1156"></a>
<a name="eglew.h-1157"></a>#endif /* EGL_KHR_cl_event */
<a name="eglew.h-1158"></a>
<a name="eglew.h-1159"></a>/* --------------------------- EGL_KHR_cl_event2 --------------------------- */
<a name="eglew.h-1160"></a>
<a name="eglew.h-1161"></a>#ifndef EGL_KHR_cl_event2
<a name="eglew.h-1162"></a>#define EGL_KHR_cl_event2 1
<a name="eglew.h-1163"></a>
<a name="eglew.h-1164"></a>#define EGL_CL_EVENT_HANDLE_KHR 0x309C
<a name="eglew.h-1165"></a>#define EGL_SYNC_CL_EVENT_KHR 0x30FE
<a name="eglew.h-1166"></a>#define EGL_SYNC_CL_EVENT_COMPLETE_KHR 0x30FF
<a name="eglew.h-1167"></a>
<a name="eglew.h-1168"></a>typedef EGLSyncKHR  ( * PFNEGLCREATESYNC64KHRPROC) (EGLDisplay  dpy, EGLenum  type, const EGLAttribKHR * attrib_list);
<a name="eglew.h-1169"></a>
<a name="eglew.h-1170"></a>#define eglCreateSync64KHR EGLEW_GET_FUN(__eglewCreateSync64KHR)
<a name="eglew.h-1171"></a>
<a name="eglew.h-1172"></a>#define EGLEW_KHR_cl_event2 EGLEW_GET_VAR(__EGLEW_KHR_cl_event2)
<a name="eglew.h-1173"></a>
<a name="eglew.h-1174"></a>#endif /* EGL_KHR_cl_event2 */
<a name="eglew.h-1175"></a>
<a name="eglew.h-1176"></a>/* ----------------- EGL_KHR_client_get_all_proc_addresses ----------------- */
<a name="eglew.h-1177"></a>
<a name="eglew.h-1178"></a>#ifndef EGL_KHR_client_get_all_proc_addresses
<a name="eglew.h-1179"></a>#define EGL_KHR_client_get_all_proc_addresses 1
<a name="eglew.h-1180"></a>
<a name="eglew.h-1181"></a>#define EGLEW_KHR_client_get_all_proc_addresses EGLEW_GET_VAR(__EGLEW_KHR_client_get_all_proc_addresses)
<a name="eglew.h-1182"></a>
<a name="eglew.h-1183"></a>#endif /* EGL_KHR_client_get_all_proc_addresses */
<a name="eglew.h-1184"></a>
<a name="eglew.h-1185"></a>/* ------------------------- EGL_KHR_config_attribs ------------------------ */
<a name="eglew.h-1186"></a>
<a name="eglew.h-1187"></a>#ifndef EGL_KHR_config_attribs
<a name="eglew.h-1188"></a>#define EGL_KHR_config_attribs 1
<a name="eglew.h-1189"></a>
<a name="eglew.h-1190"></a>#define EGL_VG_COLORSPACE_LINEAR_BIT_KHR 0x0020
<a name="eglew.h-1191"></a>#define EGL_VG_ALPHA_FORMAT_PRE_BIT_KHR 0x0040
<a name="eglew.h-1192"></a>#define EGL_CONFORMANT_KHR 0x3042
<a name="eglew.h-1193"></a>
<a name="eglew.h-1194"></a>#define EGLEW_KHR_config_attribs EGLEW_GET_VAR(__EGLEW_KHR_config_attribs)
<a name="eglew.h-1195"></a>
<a name="eglew.h-1196"></a>#endif /* EGL_KHR_config_attribs */
<a name="eglew.h-1197"></a>
<a name="eglew.h-1198"></a>/* --------------------- EGL_KHR_context_flush_control --------------------- */
<a name="eglew.h-1199"></a>
<a name="eglew.h-1200"></a>#ifndef EGL_KHR_context_flush_control
<a name="eglew.h-1201"></a>#define EGL_KHR_context_flush_control 1
<a name="eglew.h-1202"></a>
<a name="eglew.h-1203"></a>#define EGL_CONTEXT_RELEASE_BEHAVIOR_NONE_KHR 0
<a name="eglew.h-1204"></a>#define EGL_CONTEXT_RELEASE_BEHAVIOR_KHR 0x2097
<a name="eglew.h-1205"></a>#define EGL_CONTEXT_RELEASE_BEHAVIOR_FLUSH_KHR 0x2098
<a name="eglew.h-1206"></a>
<a name="eglew.h-1207"></a>#define EGLEW_KHR_context_flush_control EGLEW_GET_VAR(__EGLEW_KHR_context_flush_control)
<a name="eglew.h-1208"></a>
<a name="eglew.h-1209"></a>#endif /* EGL_KHR_context_flush_control */
<a name="eglew.h-1210"></a>
<a name="eglew.h-1211"></a>/* ------------------------- EGL_KHR_create_context ------------------------ */
<a name="eglew.h-1212"></a>
<a name="eglew.h-1213"></a>#ifndef EGL_KHR_create_context
<a name="eglew.h-1214"></a>#define EGL_KHR_create_context 1
<a name="eglew.h-1215"></a>
<a name="eglew.h-1216"></a>#define EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR 0x00000001
<a name="eglew.h-1217"></a>#define EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR 0x00000001
<a name="eglew.h-1218"></a>#define EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT_KHR 0x00000002
<a name="eglew.h-1219"></a>#define EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE_BIT_KHR 0x00000002
<a name="eglew.h-1220"></a>#define EGL_CONTEXT_OPENGL_ROBUST_ACCESS_BIT_KHR 0x00000004
<a name="eglew.h-1221"></a>#define EGL_OPENGL_ES3_BIT 0x00000040
<a name="eglew.h-1222"></a>#define EGL_OPENGL_ES3_BIT_KHR 0x00000040
<a name="eglew.h-1223"></a>#define EGL_CONTEXT_MAJOR_VERSION_KHR 0x3098
<a name="eglew.h-1224"></a>#define EGL_CONTEXT_MINOR_VERSION_KHR 0x30FB
<a name="eglew.h-1225"></a>#define EGL_CONTEXT_FLAGS_KHR 0x30FC
<a name="eglew.h-1226"></a>#define EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR 0x30FD
<a name="eglew.h-1227"></a>#define EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_KHR 0x31BD
<a name="eglew.h-1228"></a>#define EGL_NO_RESET_NOTIFICATION_KHR 0x31BE
<a name="eglew.h-1229"></a>#define EGL_LOSE_CONTEXT_ON_RESET_KHR 0x31BF
<a name="eglew.h-1230"></a>
<a name="eglew.h-1231"></a>#define EGLEW_KHR_create_context EGLEW_GET_VAR(__EGLEW_KHR_create_context)
<a name="eglew.h-1232"></a>
<a name="eglew.h-1233"></a>#endif /* EGL_KHR_create_context */
<a name="eglew.h-1234"></a>
<a name="eglew.h-1235"></a>/* -------------------- EGL_KHR_create_context_no_error -------------------- */
<a name="eglew.h-1236"></a>
<a name="eglew.h-1237"></a>#ifndef EGL_KHR_create_context_no_error
<a name="eglew.h-1238"></a>#define EGL_KHR_create_context_no_error 1
<a name="eglew.h-1239"></a>
<a name="eglew.h-1240"></a>#define EGL_CONTEXT_OPENGL_NO_ERROR_KHR 0x31B3
<a name="eglew.h-1241"></a>
<a name="eglew.h-1242"></a>#define EGLEW_KHR_create_context_no_error EGLEW_GET_VAR(__EGLEW_KHR_create_context_no_error)
<a name="eglew.h-1243"></a>
<a name="eglew.h-1244"></a>#endif /* EGL_KHR_create_context_no_error */
<a name="eglew.h-1245"></a>
<a name="eglew.h-1246"></a>/* ----------------------------- EGL_KHR_debug ----------------------------- */
<a name="eglew.h-1247"></a>
<a name="eglew.h-1248"></a>#ifndef EGL_KHR_debug
<a name="eglew.h-1249"></a>#define EGL_KHR_debug 1
<a name="eglew.h-1250"></a>
<a name="eglew.h-1251"></a>#define EGL_OBJECT_THREAD_KHR 0x33B0
<a name="eglew.h-1252"></a>#define EGL_OBJECT_DISPLAY_KHR 0x33B1
<a name="eglew.h-1253"></a>#define EGL_OBJECT_CONTEXT_KHR 0x33B2
<a name="eglew.h-1254"></a>#define EGL_OBJECT_SURFACE_KHR 0x33B3
<a name="eglew.h-1255"></a>#define EGL_OBJECT_IMAGE_KHR 0x33B4
<a name="eglew.h-1256"></a>#define EGL_OBJECT_SYNC_KHR 0x33B5
<a name="eglew.h-1257"></a>#define EGL_OBJECT_STREAM_KHR 0x33B6
<a name="eglew.h-1258"></a>#define EGL_DEBUG_CALLBACK_KHR 0x33B8
<a name="eglew.h-1259"></a>#define EGL_DEBUG_MSG_CRITICAL_KHR 0x33B9
<a name="eglew.h-1260"></a>#define EGL_DEBUG_MSG_ERROR_KHR 0x33BA
<a name="eglew.h-1261"></a>#define EGL_DEBUG_MSG_WARN_KHR 0x33BB
<a name="eglew.h-1262"></a>#define EGL_DEBUG_MSG_INFO_KHR 0x33BC
<a name="eglew.h-1263"></a>
<a name="eglew.h-1264"></a>typedef EGLint  ( * PFNEGLDEBUGMESSAGECONTROLKHRPROC) (EGLDEBUGPROCKHR  callback, const EGLAttrib * attrib_list);
<a name="eglew.h-1265"></a>typedef EGLint  ( * PFNEGLLABELOBJECTKHRPROC) (EGLDisplay  display, EGLenum  objectType, EGLObjectKHR  object, EGLLabelKHR  label);
<a name="eglew.h-1266"></a>typedef EGLBoolean  ( * PFNEGLQUERYDEBUGKHRPROC) (EGLint  attribute, EGLAttrib * value);
<a name="eglew.h-1267"></a>
<a name="eglew.h-1268"></a>#define eglDebugMessageControlKHR EGLEW_GET_FUN(__eglewDebugMessageControlKHR)
<a name="eglew.h-1269"></a>#define eglLabelObjectKHR EGLEW_GET_FUN(__eglewLabelObjectKHR)
<a name="eglew.h-1270"></a>#define eglQueryDebugKHR EGLEW_GET_FUN(__eglewQueryDebugKHR)
<a name="eglew.h-1271"></a>
<a name="eglew.h-1272"></a>#define EGLEW_KHR_debug EGLEW_GET_VAR(__EGLEW_KHR_debug)
<a name="eglew.h-1273"></a>
<a name="eglew.h-1274"></a>#endif /* EGL_KHR_debug */
<a name="eglew.h-1275"></a>
<a name="eglew.h-1276"></a>/* --------------------------- EGL_KHR_fence_sync -------------------------- */
<a name="eglew.h-1277"></a>
<a name="eglew.h-1278"></a>#ifndef EGL_KHR_fence_sync
<a name="eglew.h-1279"></a>#define EGL_KHR_fence_sync 1
<a name="eglew.h-1280"></a>
<a name="eglew.h-1281"></a>#define EGL_SYNC_PRIOR_COMMANDS_COMPLETE_KHR 0x30F0
<a name="eglew.h-1282"></a>#define EGL_SYNC_CONDITION_KHR 0x30F8
<a name="eglew.h-1283"></a>#define EGL_SYNC_FENCE_KHR 0x30F9
<a name="eglew.h-1284"></a>
<a name="eglew.h-1285"></a>#define EGLEW_KHR_fence_sync EGLEW_GET_VAR(__EGLEW_KHR_fence_sync)
<a name="eglew.h-1286"></a>
<a name="eglew.h-1287"></a>#endif /* EGL_KHR_fence_sync */
<a name="eglew.h-1288"></a>
<a name="eglew.h-1289"></a>/* --------------------- EGL_KHR_get_all_proc_addresses -------------------- */
<a name="eglew.h-1290"></a>
<a name="eglew.h-1291"></a>#ifndef EGL_KHR_get_all_proc_addresses
<a name="eglew.h-1292"></a>#define EGL_KHR_get_all_proc_addresses 1
<a name="eglew.h-1293"></a>
<a name="eglew.h-1294"></a>#define EGLEW_KHR_get_all_proc_addresses EGLEW_GET_VAR(__EGLEW_KHR_get_all_proc_addresses)
<a name="eglew.h-1295"></a>
<a name="eglew.h-1296"></a>#endif /* EGL_KHR_get_all_proc_addresses */
<a name="eglew.h-1297"></a>
<a name="eglew.h-1298"></a>/* ------------------------- EGL_KHR_gl_colorspace ------------------------- */
<a name="eglew.h-1299"></a>
<a name="eglew.h-1300"></a>#ifndef EGL_KHR_gl_colorspace
<a name="eglew.h-1301"></a>#define EGL_KHR_gl_colorspace 1
<a name="eglew.h-1302"></a>
<a name="eglew.h-1303"></a>#define EGL_GL_COLORSPACE_SRGB_KHR 0x3089
<a name="eglew.h-1304"></a>#define EGL_GL_COLORSPACE_LINEAR_KHR 0x308A
<a name="eglew.h-1305"></a>#define EGL_GL_COLORSPACE_KHR 0x309D
<a name="eglew.h-1306"></a>
<a name="eglew.h-1307"></a>#define EGLEW_KHR_gl_colorspace EGLEW_GET_VAR(__EGLEW_KHR_gl_colorspace)
<a name="eglew.h-1308"></a>
<a name="eglew.h-1309"></a>#endif /* EGL_KHR_gl_colorspace */
<a name="eglew.h-1310"></a>
<a name="eglew.h-1311"></a>/* --------------------- EGL_KHR_gl_renderbuffer_image --------------------- */
<a name="eglew.h-1312"></a>
<a name="eglew.h-1313"></a>#ifndef EGL_KHR_gl_renderbuffer_image
<a name="eglew.h-1314"></a>#define EGL_KHR_gl_renderbuffer_image 1
<a name="eglew.h-1315"></a>
<a name="eglew.h-1316"></a>#define EGL_GL_RENDERBUFFER_KHR 0x30B9
<a name="eglew.h-1317"></a>
<a name="eglew.h-1318"></a>#define EGLEW_KHR_gl_renderbuffer_image EGLEW_GET_VAR(__EGLEW_KHR_gl_renderbuffer_image)
<a name="eglew.h-1319"></a>
<a name="eglew.h-1320"></a>#endif /* EGL_KHR_gl_renderbuffer_image */
<a name="eglew.h-1321"></a>
<a name="eglew.h-1322"></a>/* ---------------------- EGL_KHR_gl_texture_2D_image ---------------------- */
<a name="eglew.h-1323"></a>
<a name="eglew.h-1324"></a>#ifndef EGL_KHR_gl_texture_2D_image
<a name="eglew.h-1325"></a>#define EGL_KHR_gl_texture_2D_image 1
<a name="eglew.h-1326"></a>
<a name="eglew.h-1327"></a>#define EGL_GL_TEXTURE_2D_KHR 0x30B1
<a name="eglew.h-1328"></a>#define EGL_GL_TEXTURE_LEVEL_KHR 0x30BC
<a name="eglew.h-1329"></a>
<a name="eglew.h-1330"></a>#define EGLEW_KHR_gl_texture_2D_image EGLEW_GET_VAR(__EGLEW_KHR_gl_texture_2D_image)
<a name="eglew.h-1331"></a>
<a name="eglew.h-1332"></a>#endif /* EGL_KHR_gl_texture_2D_image */
<a name="eglew.h-1333"></a>
<a name="eglew.h-1334"></a>/* ---------------------- EGL_KHR_gl_texture_3D_image ---------------------- */
<a name="eglew.h-1335"></a>
<a name="eglew.h-1336"></a>#ifndef EGL_KHR_gl_texture_3D_image
<a name="eglew.h-1337"></a>#define EGL_KHR_gl_texture_3D_image 1
<a name="eglew.h-1338"></a>
<a name="eglew.h-1339"></a>#define EGL_GL_TEXTURE_3D_KHR 0x30B2
<a name="eglew.h-1340"></a>#define EGL_GL_TEXTURE_ZOFFSET_KHR 0x30BD
<a name="eglew.h-1341"></a>
<a name="eglew.h-1342"></a>#define EGLEW_KHR_gl_texture_3D_image EGLEW_GET_VAR(__EGLEW_KHR_gl_texture_3D_image)
<a name="eglew.h-1343"></a>
<a name="eglew.h-1344"></a>#endif /* EGL_KHR_gl_texture_3D_image */
<a name="eglew.h-1345"></a>
<a name="eglew.h-1346"></a>/* -------------------- EGL_KHR_gl_texture_cubemap_image ------------------- */
<a name="eglew.h-1347"></a>
<a name="eglew.h-1348"></a>#ifndef EGL_KHR_gl_texture_cubemap_image
<a name="eglew.h-1349"></a>#define EGL_KHR_gl_texture_cubemap_image 1
<a name="eglew.h-1350"></a>
<a name="eglew.h-1351"></a>#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X_KHR 0x30B3
<a name="eglew.h-1352"></a>#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR 0x30B4
<a name="eglew.h-1353"></a>#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR 0x30B5
<a name="eglew.h-1354"></a>#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR 0x30B6
<a name="eglew.h-1355"></a>#define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR 0x30B7
<a name="eglew.h-1356"></a>#define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR 0x30B8
<a name="eglew.h-1357"></a>
<a name="eglew.h-1358"></a>#define EGLEW_KHR_gl_texture_cubemap_image EGLEW_GET_VAR(__EGLEW_KHR_gl_texture_cubemap_image)
<a name="eglew.h-1359"></a>
<a name="eglew.h-1360"></a>#endif /* EGL_KHR_gl_texture_cubemap_image */
<a name="eglew.h-1361"></a>
<a name="eglew.h-1362"></a>/* ----------------------------- EGL_KHR_image ----------------------------- */
<a name="eglew.h-1363"></a>
<a name="eglew.h-1364"></a>#ifndef EGL_KHR_image
<a name="eglew.h-1365"></a>#define EGL_KHR_image 1
<a name="eglew.h-1366"></a>
<a name="eglew.h-1367"></a>#define EGL_NATIVE_PIXMAP_KHR 0x30B0
<a name="eglew.h-1368"></a>
<a name="eglew.h-1369"></a>typedef EGLImageKHR  ( * PFNEGLCREATEIMAGEKHRPROC) (EGLDisplay  dpy, EGLContext  ctx, EGLenum  target, EGLClientBuffer  buffer, const EGLint * attrib_list);
<a name="eglew.h-1370"></a>typedef EGLBoolean  ( * PFNEGLDESTROYIMAGEKHRPROC) (EGLDisplay  dpy, EGLImageKHR  image);
<a name="eglew.h-1371"></a>
<a name="eglew.h-1372"></a>#define eglCreateImageKHR EGLEW_GET_FUN(__eglewCreateImageKHR)
<a name="eglew.h-1373"></a>#define eglDestroyImageKHR EGLEW_GET_FUN(__eglewDestroyImageKHR)
<a name="eglew.h-1374"></a>
<a name="eglew.h-1375"></a>#define EGLEW_KHR_image EGLEW_GET_VAR(__EGLEW_KHR_image)
<a name="eglew.h-1376"></a>
<a name="eglew.h-1377"></a>#endif /* EGL_KHR_image */
<a name="eglew.h-1378"></a>
<a name="eglew.h-1379"></a>/* --------------------------- EGL_KHR_image_base -------------------------- */
<a name="eglew.h-1380"></a>
<a name="eglew.h-1381"></a>#ifndef EGL_KHR_image_base
<a name="eglew.h-1382"></a>#define EGL_KHR_image_base 1
<a name="eglew.h-1383"></a>
<a name="eglew.h-1384"></a>#define EGL_IMAGE_PRESERVED_KHR 0x30D2
<a name="eglew.h-1385"></a>
<a name="eglew.h-1386"></a>#define EGLEW_KHR_image_base EGLEW_GET_VAR(__EGLEW_KHR_image_base)
<a name="eglew.h-1387"></a>
<a name="eglew.h-1388"></a>#endif /* EGL_KHR_image_base */
<a name="eglew.h-1389"></a>
<a name="eglew.h-1390"></a>/* -------------------------- EGL_KHR_image_pixmap ------------------------- */
<a name="eglew.h-1391"></a>
<a name="eglew.h-1392"></a>#ifndef EGL_KHR_image_pixmap
<a name="eglew.h-1393"></a>#define EGL_KHR_image_pixmap 1
<a name="eglew.h-1394"></a>
<a name="eglew.h-1395"></a>#define EGL_NATIVE_PIXMAP_KHR 0x30B0
<a name="eglew.h-1396"></a>
<a name="eglew.h-1397"></a>#define EGLEW_KHR_image_pixmap EGLEW_GET_VAR(__EGLEW_KHR_image_pixmap)
<a name="eglew.h-1398"></a>
<a name="eglew.h-1399"></a>#endif /* EGL_KHR_image_pixmap */
<a name="eglew.h-1400"></a>
<a name="eglew.h-1401"></a>/* -------------------------- EGL_KHR_lock_surface ------------------------- */
<a name="eglew.h-1402"></a>
<a name="eglew.h-1403"></a>#ifndef EGL_KHR_lock_surface
<a name="eglew.h-1404"></a>#define EGL_KHR_lock_surface 1
<a name="eglew.h-1405"></a>
<a name="eglew.h-1406"></a>#define EGL_READ_SURFACE_BIT_KHR 0x0001
<a name="eglew.h-1407"></a>#define EGL_WRITE_SURFACE_BIT_KHR 0x0002
<a name="eglew.h-1408"></a>#define EGL_LOCK_SURFACE_BIT_KHR 0x0080
<a name="eglew.h-1409"></a>#define EGL_OPTIMAL_FORMAT_BIT_KHR 0x0100
<a name="eglew.h-1410"></a>#define EGL_MATCH_FORMAT_KHR 0x3043
<a name="eglew.h-1411"></a>#define EGL_FORMAT_RGB_565_EXACT_KHR 0x30C0
<a name="eglew.h-1412"></a>#define EGL_FORMAT_RGB_565_KHR 0x30C1
<a name="eglew.h-1413"></a>#define EGL_FORMAT_RGBA_8888_EXACT_KHR 0x30C2
<a name="eglew.h-1414"></a>#define EGL_FORMAT_RGBA_8888_KHR 0x30C3
<a name="eglew.h-1415"></a>#define EGL_MAP_PRESERVE_PIXELS_KHR 0x30C4
<a name="eglew.h-1416"></a>#define EGL_LOCK_USAGE_HINT_KHR 0x30C5
<a name="eglew.h-1417"></a>#define EGL_BITMAP_POINTER_KHR 0x30C6
<a name="eglew.h-1418"></a>#define EGL_BITMAP_PITCH_KHR 0x30C7
<a name="eglew.h-1419"></a>#define EGL_BITMAP_ORIGIN_KHR 0x30C8
<a name="eglew.h-1420"></a>#define EGL_BITMAP_PIXEL_RED_OFFSET_KHR 0x30C9
<a name="eglew.h-1421"></a>#define EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR 0x30CA
<a name="eglew.h-1422"></a>#define EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR 0x30CB
<a name="eglew.h-1423"></a>#define EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR 0x30CC
<a name="eglew.h-1424"></a>#define EGL_BITMAP_PIXEL_LUMINANCE_OFFSET_KHR 0x30CD
<a name="eglew.h-1425"></a>#define EGL_LOWER_LEFT_KHR 0x30CE
<a name="eglew.h-1426"></a>#define EGL_UPPER_LEFT_KHR 0x30CF
<a name="eglew.h-1427"></a>
<a name="eglew.h-1428"></a>typedef EGLBoolean  ( * PFNEGLLOCKSURFACEKHRPROC) (EGLDisplay  dpy, EGLSurface  surface, const EGLint * attrib_list);
<a name="eglew.h-1429"></a>typedef EGLBoolean  ( * PFNEGLUNLOCKSURFACEKHRPROC) (EGLDisplay  dpy, EGLSurface  surface);
<a name="eglew.h-1430"></a>
<a name="eglew.h-1431"></a>#define eglLockSurfaceKHR EGLEW_GET_FUN(__eglewLockSurfaceKHR)
<a name="eglew.h-1432"></a>#define eglUnlockSurfaceKHR EGLEW_GET_FUN(__eglewUnlockSurfaceKHR)
<a name="eglew.h-1433"></a>
<a name="eglew.h-1434"></a>#define EGLEW_KHR_lock_surface EGLEW_GET_VAR(__EGLEW_KHR_lock_surface)
<a name="eglew.h-1435"></a>
<a name="eglew.h-1436"></a>#endif /* EGL_KHR_lock_surface */
<a name="eglew.h-1437"></a>
<a name="eglew.h-1438"></a>/* ------------------------- EGL_KHR_lock_surface2 ------------------------- */
<a name="eglew.h-1439"></a>
<a name="eglew.h-1440"></a>#ifndef EGL_KHR_lock_surface2
<a name="eglew.h-1441"></a>#define EGL_KHR_lock_surface2 1
<a name="eglew.h-1442"></a>
<a name="eglew.h-1443"></a>#define EGL_BITMAP_PIXEL_SIZE_KHR 0x3110
<a name="eglew.h-1444"></a>
<a name="eglew.h-1445"></a>#define EGLEW_KHR_lock_surface2 EGLEW_GET_VAR(__EGLEW_KHR_lock_surface2)
<a name="eglew.h-1446"></a>
<a name="eglew.h-1447"></a>#endif /* EGL_KHR_lock_surface2 */
<a name="eglew.h-1448"></a>
<a name="eglew.h-1449"></a>/* ------------------------- EGL_KHR_lock_surface3 ------------------------- */
<a name="eglew.h-1450"></a>
<a name="eglew.h-1451"></a>#ifndef EGL_KHR_lock_surface3
<a name="eglew.h-1452"></a>#define EGL_KHR_lock_surface3 1
<a name="eglew.h-1453"></a>
<a name="eglew.h-1454"></a>#define EGL_READ_SURFACE_BIT_KHR 0x0001
<a name="eglew.h-1455"></a>#define EGL_WRITE_SURFACE_BIT_KHR 0x0002
<a name="eglew.h-1456"></a>#define EGL_LOCK_SURFACE_BIT_KHR 0x0080
<a name="eglew.h-1457"></a>#define EGL_OPTIMAL_FORMAT_BIT_KHR 0x0100
<a name="eglew.h-1458"></a>#define EGL_MATCH_FORMAT_KHR 0x3043
<a name="eglew.h-1459"></a>#define EGL_FORMAT_RGB_565_EXACT_KHR 0x30C0
<a name="eglew.h-1460"></a>#define EGL_FORMAT_RGB_565_KHR 0x30C1
<a name="eglew.h-1461"></a>#define EGL_FORMAT_RGBA_8888_EXACT_KHR 0x30C2
<a name="eglew.h-1462"></a>#define EGL_FORMAT_RGBA_8888_KHR 0x30C3
<a name="eglew.h-1463"></a>#define EGL_MAP_PRESERVE_PIXELS_KHR 0x30C4
<a name="eglew.h-1464"></a>#define EGL_LOCK_USAGE_HINT_KHR 0x30C5
<a name="eglew.h-1465"></a>#define EGL_BITMAP_POINTER_KHR 0x30C6
<a name="eglew.h-1466"></a>#define EGL_BITMAP_PITCH_KHR 0x30C7
<a name="eglew.h-1467"></a>#define EGL_BITMAP_ORIGIN_KHR 0x30C8
<a name="eglew.h-1468"></a>#define EGL_BITMAP_PIXEL_RED_OFFSET_KHR 0x30C9
<a name="eglew.h-1469"></a>#define EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR 0x30CA
<a name="eglew.h-1470"></a>#define EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR 0x30CB
<a name="eglew.h-1471"></a>#define EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR 0x30CC
<a name="eglew.h-1472"></a>#define EGL_BITMAP_PIXEL_LUMINANCE_OFFSET_KHR 0x30CD
<a name="eglew.h-1473"></a>#define EGL_LOWER_LEFT_KHR 0x30CE
<a name="eglew.h-1474"></a>#define EGL_UPPER_LEFT_KHR 0x30CF
<a name="eglew.h-1475"></a>#define EGL_BITMAP_PIXEL_SIZE_KHR 0x3110
<a name="eglew.h-1476"></a>
<a name="eglew.h-1477"></a>typedef EGLBoolean  ( * PFNEGLQUERYSURFACE64KHRPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  attribute, EGLAttribKHR * value);
<a name="eglew.h-1478"></a>
<a name="eglew.h-1479"></a>#define eglQuerySurface64KHR EGLEW_GET_FUN(__eglewQuerySurface64KHR)
<a name="eglew.h-1480"></a>
<a name="eglew.h-1481"></a>#define EGLEW_KHR_lock_surface3 EGLEW_GET_VAR(__EGLEW_KHR_lock_surface3)
<a name="eglew.h-1482"></a>
<a name="eglew.h-1483"></a>#endif /* EGL_KHR_lock_surface3 */
<a name="eglew.h-1484"></a>
<a name="eglew.h-1485"></a>/* --------------------- EGL_KHR_mutable_render_buffer --------------------- */
<a name="eglew.h-1486"></a>
<a name="eglew.h-1487"></a>#ifndef EGL_KHR_mutable_render_buffer
<a name="eglew.h-1488"></a>#define EGL_KHR_mutable_render_buffer 1
<a name="eglew.h-1489"></a>
<a name="eglew.h-1490"></a>#define EGL_MUTABLE_RENDER_BUFFER_BIT_KHR 0x1000
<a name="eglew.h-1491"></a>
<a name="eglew.h-1492"></a>#define EGLEW_KHR_mutable_render_buffer EGLEW_GET_VAR(__EGLEW_KHR_mutable_render_buffer)
<a name="eglew.h-1493"></a>
<a name="eglew.h-1494"></a>#endif /* EGL_KHR_mutable_render_buffer */
<a name="eglew.h-1495"></a>
<a name="eglew.h-1496"></a>/* ----------------------- EGL_KHR_no_config_context ----------------------- */
<a name="eglew.h-1497"></a>
<a name="eglew.h-1498"></a>#ifndef EGL_KHR_no_config_context
<a name="eglew.h-1499"></a>#define EGL_KHR_no_config_context 1
<a name="eglew.h-1500"></a>
<a name="eglew.h-1501"></a>#define EGLEW_KHR_no_config_context EGLEW_GET_VAR(__EGLEW_KHR_no_config_context)
<a name="eglew.h-1502"></a>
<a name="eglew.h-1503"></a>#endif /* EGL_KHR_no_config_context */
<a name="eglew.h-1504"></a>
<a name="eglew.h-1505"></a>/* ------------------------- EGL_KHR_partial_update ------------------------ */
<a name="eglew.h-1506"></a>
<a name="eglew.h-1507"></a>#ifndef EGL_KHR_partial_update
<a name="eglew.h-1508"></a>#define EGL_KHR_partial_update 1
<a name="eglew.h-1509"></a>
<a name="eglew.h-1510"></a>#define EGL_BUFFER_AGE_KHR 0x313D
<a name="eglew.h-1511"></a>
<a name="eglew.h-1512"></a>typedef EGLBoolean  ( * PFNEGLSETDAMAGEREGIONKHRPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint * rects, EGLint  n_rects);
<a name="eglew.h-1513"></a>
<a name="eglew.h-1514"></a>#define eglSetDamageRegionKHR EGLEW_GET_FUN(__eglewSetDamageRegionKHR)
<a name="eglew.h-1515"></a>
<a name="eglew.h-1516"></a>#define EGLEW_KHR_partial_update EGLEW_GET_VAR(__EGLEW_KHR_partial_update)
<a name="eglew.h-1517"></a>
<a name="eglew.h-1518"></a>#endif /* EGL_KHR_partial_update */
<a name="eglew.h-1519"></a>
<a name="eglew.h-1520"></a>/* ------------------------ EGL_KHR_platform_android ----------------------- */
<a name="eglew.h-1521"></a>
<a name="eglew.h-1522"></a>#ifndef EGL_KHR_platform_android
<a name="eglew.h-1523"></a>#define EGL_KHR_platform_android 1
<a name="eglew.h-1524"></a>
<a name="eglew.h-1525"></a>#define EGL_PLATFORM_ANDROID_KHR 0x3141
<a name="eglew.h-1526"></a>
<a name="eglew.h-1527"></a>#define EGLEW_KHR_platform_android EGLEW_GET_VAR(__EGLEW_KHR_platform_android)
<a name="eglew.h-1528"></a>
<a name="eglew.h-1529"></a>#endif /* EGL_KHR_platform_android */
<a name="eglew.h-1530"></a>
<a name="eglew.h-1531"></a>/* -------------------------- EGL_KHR_platform_gbm ------------------------- */
<a name="eglew.h-1532"></a>
<a name="eglew.h-1533"></a>#ifndef EGL_KHR_platform_gbm
<a name="eglew.h-1534"></a>#define EGL_KHR_platform_gbm 1
<a name="eglew.h-1535"></a>
<a name="eglew.h-1536"></a>#define EGL_PLATFORM_GBM_KHR 0x31D7
<a name="eglew.h-1537"></a>
<a name="eglew.h-1538"></a>#define EGLEW_KHR_platform_gbm EGLEW_GET_VAR(__EGLEW_KHR_platform_gbm)
<a name="eglew.h-1539"></a>
<a name="eglew.h-1540"></a>#endif /* EGL_KHR_platform_gbm */
<a name="eglew.h-1541"></a>
<a name="eglew.h-1542"></a>/* ------------------------ EGL_KHR_platform_wayland ----------------------- */
<a name="eglew.h-1543"></a>
<a name="eglew.h-1544"></a>#ifndef EGL_KHR_platform_wayland
<a name="eglew.h-1545"></a>#define EGL_KHR_platform_wayland 1
<a name="eglew.h-1546"></a>
<a name="eglew.h-1547"></a>#define EGL_PLATFORM_WAYLAND_KHR 0x31D8
<a name="eglew.h-1548"></a>
<a name="eglew.h-1549"></a>#define EGLEW_KHR_platform_wayland EGLEW_GET_VAR(__EGLEW_KHR_platform_wayland)
<a name="eglew.h-1550"></a>
<a name="eglew.h-1551"></a>#endif /* EGL_KHR_platform_wayland */
<a name="eglew.h-1552"></a>
<a name="eglew.h-1553"></a>/* -------------------------- EGL_KHR_platform_x11 ------------------------- */
<a name="eglew.h-1554"></a>
<a name="eglew.h-1555"></a>#ifndef EGL_KHR_platform_x11
<a name="eglew.h-1556"></a>#define EGL_KHR_platform_x11 1
<a name="eglew.h-1557"></a>
<a name="eglew.h-1558"></a>#define EGL_PLATFORM_X11_KHR 0x31D5
<a name="eglew.h-1559"></a>#define EGL_PLATFORM_X11_SCREEN_KHR 0x31D6
<a name="eglew.h-1560"></a>
<a name="eglew.h-1561"></a>#define EGLEW_KHR_platform_x11 EGLEW_GET_VAR(__EGLEW_KHR_platform_x11)
<a name="eglew.h-1562"></a>
<a name="eglew.h-1563"></a>#endif /* EGL_KHR_platform_x11 */
<a name="eglew.h-1564"></a>
<a name="eglew.h-1565"></a>/* ------------------------- EGL_KHR_reusable_sync ------------------------- */
<a name="eglew.h-1566"></a>
<a name="eglew.h-1567"></a>#ifndef EGL_KHR_reusable_sync
<a name="eglew.h-1568"></a>#define EGL_KHR_reusable_sync 1
<a name="eglew.h-1569"></a>
<a name="eglew.h-1570"></a>#define EGL_SYNC_FLUSH_COMMANDS_BIT_KHR 0x0001
<a name="eglew.h-1571"></a>#define EGL_SYNC_STATUS_KHR 0x30F1
<a name="eglew.h-1572"></a>#define EGL_SIGNALED_KHR 0x30F2
<a name="eglew.h-1573"></a>#define EGL_UNSIGNALED_KHR 0x30F3
<a name="eglew.h-1574"></a>#define EGL_TIMEOUT_EXPIRED_KHR 0x30F5
<a name="eglew.h-1575"></a>#define EGL_CONDITION_SATISFIED_KHR 0x30F6
<a name="eglew.h-1576"></a>#define EGL_SYNC_TYPE_KHR 0x30F7
<a name="eglew.h-1577"></a>#define EGL_SYNC_REUSABLE_KHR 0x30FA
<a name="eglew.h-1578"></a>#define EGL_FOREVER_KHR 0xFFFFFFFFFFFFFFFF
<a name="eglew.h-1579"></a>
<a name="eglew.h-1580"></a>typedef EGLint  ( * PFNEGLCLIENTWAITSYNCKHRPROC) (EGLDisplay  dpy, EGLSyncKHR  sync, EGLint  flags, EGLTimeKHR  timeout);
<a name="eglew.h-1581"></a>typedef EGLSyncKHR  ( * PFNEGLCREATESYNCKHRPROC) (EGLDisplay  dpy, EGLenum  type, const EGLint * attrib_list);
<a name="eglew.h-1582"></a>typedef EGLBoolean  ( * PFNEGLDESTROYSYNCKHRPROC) (EGLDisplay  dpy, EGLSyncKHR  sync);
<a name="eglew.h-1583"></a>typedef EGLBoolean  ( * PFNEGLGETSYNCATTRIBKHRPROC) (EGLDisplay  dpy, EGLSyncKHR  sync, EGLint  attribute, EGLint * value);
<a name="eglew.h-1584"></a>typedef EGLBoolean  ( * PFNEGLSIGNALSYNCKHRPROC) (EGLDisplay  dpy, EGLSyncKHR  sync, EGLenum  mode);
<a name="eglew.h-1585"></a>
<a name="eglew.h-1586"></a>#define eglClientWaitSyncKHR EGLEW_GET_FUN(__eglewClientWaitSyncKHR)
<a name="eglew.h-1587"></a>#define eglCreateSyncKHR EGLEW_GET_FUN(__eglewCreateSyncKHR)
<a name="eglew.h-1588"></a>#define eglDestroySyncKHR EGLEW_GET_FUN(__eglewDestroySyncKHR)
<a name="eglew.h-1589"></a>#define eglGetSyncAttribKHR EGLEW_GET_FUN(__eglewGetSyncAttribKHR)
<a name="eglew.h-1590"></a>#define eglSignalSyncKHR EGLEW_GET_FUN(__eglewSignalSyncKHR)
<a name="eglew.h-1591"></a>
<a name="eglew.h-1592"></a>#define EGLEW_KHR_reusable_sync EGLEW_GET_VAR(__EGLEW_KHR_reusable_sync)
<a name="eglew.h-1593"></a>
<a name="eglew.h-1594"></a>#endif /* EGL_KHR_reusable_sync */
<a name="eglew.h-1595"></a>
<a name="eglew.h-1596"></a>/* ----------------------------- EGL_KHR_stream ---------------------------- */
<a name="eglew.h-1597"></a>
<a name="eglew.h-1598"></a>#ifndef EGL_KHR_stream
<a name="eglew.h-1599"></a>#define EGL_KHR_stream 1
<a name="eglew.h-1600"></a>
<a name="eglew.h-1601"></a>#define EGL_CONSUMER_LATENCY_USEC_KHR 0x3210
<a name="eglew.h-1602"></a>#define EGL_PRODUCER_FRAME_KHR 0x3212
<a name="eglew.h-1603"></a>#define EGL_CONSUMER_FRAME_KHR 0x3213
<a name="eglew.h-1604"></a>#define EGL_STREAM_STATE_KHR 0x3214
<a name="eglew.h-1605"></a>#define EGL_STREAM_STATE_CREATED_KHR 0x3215
<a name="eglew.h-1606"></a>#define EGL_STREAM_STATE_CONNECTING_KHR 0x3216
<a name="eglew.h-1607"></a>#define EGL_STREAM_STATE_EMPTY_KHR 0x3217
<a name="eglew.h-1608"></a>#define EGL_STREAM_STATE_NEW_FRAME_AVAILABLE_KHR 0x3218
<a name="eglew.h-1609"></a>#define EGL_STREAM_STATE_OLD_FRAME_AVAILABLE_KHR 0x3219
<a name="eglew.h-1610"></a>#define EGL_STREAM_STATE_DISCONNECTED_KHR 0x321A
<a name="eglew.h-1611"></a>#define EGL_BAD_STREAM_KHR 0x321B
<a name="eglew.h-1612"></a>#define EGL_BAD_STATE_KHR 0x321C
<a name="eglew.h-1613"></a>
<a name="eglew.h-1614"></a>typedef EGLStreamKHR  ( * PFNEGLCREATESTREAMKHRPROC) (EGLDisplay  dpy, const EGLint * attrib_list);
<a name="eglew.h-1615"></a>typedef EGLBoolean  ( * PFNEGLDESTROYSTREAMKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream);
<a name="eglew.h-1616"></a>typedef EGLBoolean  ( * PFNEGLQUERYSTREAMKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  attribute, EGLint * value);
<a name="eglew.h-1617"></a>typedef EGLBoolean  ( * PFNEGLQUERYSTREAMU64KHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  attribute, EGLuint64KHR * value);
<a name="eglew.h-1618"></a>typedef EGLBoolean  ( * PFNEGLSTREAMATTRIBKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  attribute, EGLint  value);
<a name="eglew.h-1619"></a>
<a name="eglew.h-1620"></a>#define eglCreateStreamKHR EGLEW_GET_FUN(__eglewCreateStreamKHR)
<a name="eglew.h-1621"></a>#define eglDestroyStreamKHR EGLEW_GET_FUN(__eglewDestroyStreamKHR)
<a name="eglew.h-1622"></a>#define eglQueryStreamKHR EGLEW_GET_FUN(__eglewQueryStreamKHR)
<a name="eglew.h-1623"></a>#define eglQueryStreamu64KHR EGLEW_GET_FUN(__eglewQueryStreamu64KHR)
<a name="eglew.h-1624"></a>#define eglStreamAttribKHR EGLEW_GET_FUN(__eglewStreamAttribKHR)
<a name="eglew.h-1625"></a>
<a name="eglew.h-1626"></a>#define EGLEW_KHR_stream EGLEW_GET_VAR(__EGLEW_KHR_stream)
<a name="eglew.h-1627"></a>
<a name="eglew.h-1628"></a>#endif /* EGL_KHR_stream */
<a name="eglew.h-1629"></a>
<a name="eglew.h-1630"></a>/* ------------------------- EGL_KHR_stream_attrib ------------------------- */
<a name="eglew.h-1631"></a>
<a name="eglew.h-1632"></a>#ifndef EGL_KHR_stream_attrib
<a name="eglew.h-1633"></a>#define EGL_KHR_stream_attrib 1
<a name="eglew.h-1634"></a>
<a name="eglew.h-1635"></a>#define EGL_CONSUMER_LATENCY_USEC_KHR 0x3210
<a name="eglew.h-1636"></a>#define EGL_STREAM_STATE_KHR 0x3214
<a name="eglew.h-1637"></a>#define EGL_STREAM_STATE_CREATED_KHR 0x3215
<a name="eglew.h-1638"></a>#define EGL_STREAM_STATE_CONNECTING_KHR 0x3216
<a name="eglew.h-1639"></a>
<a name="eglew.h-1640"></a>typedef EGLStreamKHR  ( * PFNEGLCREATESTREAMATTRIBKHRPROC) (EGLDisplay  dpy, const EGLAttrib * attrib_list);
<a name="eglew.h-1641"></a>typedef EGLBoolean  ( * PFNEGLQUERYSTREAMATTRIBKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  attribute, EGLAttrib * value);
<a name="eglew.h-1642"></a>typedef EGLBoolean  ( * PFNEGLSETSTREAMATTRIBKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  attribute, EGLAttrib  value);
<a name="eglew.h-1643"></a>typedef EGLBoolean  ( * PFNEGLSTREAMCONSUMERACQUIREATTRIBKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, const EGLAttrib * attrib_list);
<a name="eglew.h-1644"></a>typedef EGLBoolean  ( * PFNEGLSTREAMCONSUMERRELEASEATTRIBKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, const EGLAttrib * attrib_list);
<a name="eglew.h-1645"></a>
<a name="eglew.h-1646"></a>#define eglCreateStreamAttribKHR EGLEW_GET_FUN(__eglewCreateStreamAttribKHR)
<a name="eglew.h-1647"></a>#define eglQueryStreamAttribKHR EGLEW_GET_FUN(__eglewQueryStreamAttribKHR)
<a name="eglew.h-1648"></a>#define eglSetStreamAttribKHR EGLEW_GET_FUN(__eglewSetStreamAttribKHR)
<a name="eglew.h-1649"></a>#define eglStreamConsumerAcquireAttribKHR EGLEW_GET_FUN(__eglewStreamConsumerAcquireAttribKHR)
<a name="eglew.h-1650"></a>#define eglStreamConsumerReleaseAttribKHR EGLEW_GET_FUN(__eglewStreamConsumerReleaseAttribKHR)
<a name="eglew.h-1651"></a>
<a name="eglew.h-1652"></a>#define EGLEW_KHR_stream_attrib EGLEW_GET_VAR(__EGLEW_KHR_stream_attrib)
<a name="eglew.h-1653"></a>
<a name="eglew.h-1654"></a>#endif /* EGL_KHR_stream_attrib */
<a name="eglew.h-1655"></a>
<a name="eglew.h-1656"></a>/* ------------------- EGL_KHR_stream_consumer_gltexture ------------------- */
<a name="eglew.h-1657"></a>
<a name="eglew.h-1658"></a>#ifndef EGL_KHR_stream_consumer_gltexture
<a name="eglew.h-1659"></a>#define EGL_KHR_stream_consumer_gltexture 1
<a name="eglew.h-1660"></a>
<a name="eglew.h-1661"></a>#define EGL_CONSUMER_ACQUIRE_TIMEOUT_USEC_KHR 0x321E
<a name="eglew.h-1662"></a>
<a name="eglew.h-1663"></a>typedef EGLBoolean  ( * PFNEGLSTREAMCONSUMERACQUIREKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream);
<a name="eglew.h-1664"></a>typedef EGLBoolean  ( * PFNEGLSTREAMCONSUMERGLTEXTUREEXTERNALKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream);
<a name="eglew.h-1665"></a>typedef EGLBoolean  ( * PFNEGLSTREAMCONSUMERRELEASEKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream);
<a name="eglew.h-1666"></a>
<a name="eglew.h-1667"></a>#define eglStreamConsumerAcquireKHR EGLEW_GET_FUN(__eglewStreamConsumerAcquireKHR)
<a name="eglew.h-1668"></a>#define eglStreamConsumerGLTextureExternalKHR EGLEW_GET_FUN(__eglewStreamConsumerGLTextureExternalKHR)
<a name="eglew.h-1669"></a>#define eglStreamConsumerReleaseKHR EGLEW_GET_FUN(__eglewStreamConsumerReleaseKHR)
<a name="eglew.h-1670"></a>
<a name="eglew.h-1671"></a>#define EGLEW_KHR_stream_consumer_gltexture EGLEW_GET_VAR(__EGLEW_KHR_stream_consumer_gltexture)
<a name="eglew.h-1672"></a>
<a name="eglew.h-1673"></a>#endif /* EGL_KHR_stream_consumer_gltexture */
<a name="eglew.h-1674"></a>
<a name="eglew.h-1675"></a>/* -------------------- EGL_KHR_stream_cross_process_fd -------------------- */
<a name="eglew.h-1676"></a>
<a name="eglew.h-1677"></a>#ifndef EGL_KHR_stream_cross_process_fd
<a name="eglew.h-1678"></a>#define EGL_KHR_stream_cross_process_fd 1
<a name="eglew.h-1679"></a>
<a name="eglew.h-1680"></a>typedef EGLStreamKHR  ( * PFNEGLCREATESTREAMFROMFILEDESCRIPTORKHRPROC) (EGLDisplay  dpy, EGLNativeFileDescriptorKHR  file_descriptor);
<a name="eglew.h-1681"></a>typedef EGLNativeFileDescriptorKHR  ( * PFNEGLGETSTREAMFILEDESCRIPTORKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream);
<a name="eglew.h-1682"></a>
<a name="eglew.h-1683"></a>#define eglCreateStreamFromFileDescriptorKHR EGLEW_GET_FUN(__eglewCreateStreamFromFileDescriptorKHR)
<a name="eglew.h-1684"></a>#define eglGetStreamFileDescriptorKHR EGLEW_GET_FUN(__eglewGetStreamFileDescriptorKHR)
<a name="eglew.h-1685"></a>
<a name="eglew.h-1686"></a>#define EGLEW_KHR_stream_cross_process_fd EGLEW_GET_VAR(__EGLEW_KHR_stream_cross_process_fd)
<a name="eglew.h-1687"></a>
<a name="eglew.h-1688"></a>#endif /* EGL_KHR_stream_cross_process_fd */
<a name="eglew.h-1689"></a>
<a name="eglew.h-1690"></a>/* -------------------------- EGL_KHR_stream_fifo -------------------------- */
<a name="eglew.h-1691"></a>
<a name="eglew.h-1692"></a>#ifndef EGL_KHR_stream_fifo
<a name="eglew.h-1693"></a>#define EGL_KHR_stream_fifo 1
<a name="eglew.h-1694"></a>
<a name="eglew.h-1695"></a>#define EGL_STREAM_FIFO_LENGTH_KHR 0x31FC
<a name="eglew.h-1696"></a>#define EGL_STREAM_TIME_NOW_KHR 0x31FD
<a name="eglew.h-1697"></a>#define EGL_STREAM_TIME_CONSUMER_KHR 0x31FE
<a name="eglew.h-1698"></a>#define EGL_STREAM_TIME_PRODUCER_KHR 0x31FF
<a name="eglew.h-1699"></a>
<a name="eglew.h-1700"></a>typedef EGLBoolean  ( * PFNEGLQUERYSTREAMTIMEKHRPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  attribute, EGLTimeKHR * value);
<a name="eglew.h-1701"></a>
<a name="eglew.h-1702"></a>#define eglQueryStreamTimeKHR EGLEW_GET_FUN(__eglewQueryStreamTimeKHR)
<a name="eglew.h-1703"></a>
<a name="eglew.h-1704"></a>#define EGLEW_KHR_stream_fifo EGLEW_GET_VAR(__EGLEW_KHR_stream_fifo)
<a name="eglew.h-1705"></a>
<a name="eglew.h-1706"></a>#endif /* EGL_KHR_stream_fifo */
<a name="eglew.h-1707"></a>
<a name="eglew.h-1708"></a>/* ----------------- EGL_KHR_stream_producer_aldatalocator ----------------- */
<a name="eglew.h-1709"></a>
<a name="eglew.h-1710"></a>#ifndef EGL_KHR_stream_producer_aldatalocator
<a name="eglew.h-1711"></a>#define EGL_KHR_stream_producer_aldatalocator 1
<a name="eglew.h-1712"></a>
<a name="eglew.h-1713"></a>#define EGLEW_KHR_stream_producer_aldatalocator EGLEW_GET_VAR(__EGLEW_KHR_stream_producer_aldatalocator)
<a name="eglew.h-1714"></a>
<a name="eglew.h-1715"></a>#endif /* EGL_KHR_stream_producer_aldatalocator */
<a name="eglew.h-1716"></a>
<a name="eglew.h-1717"></a>/* ------------------- EGL_KHR_stream_producer_eglsurface ------------------ */
<a name="eglew.h-1718"></a>
<a name="eglew.h-1719"></a>#ifndef EGL_KHR_stream_producer_eglsurface
<a name="eglew.h-1720"></a>#define EGL_KHR_stream_producer_eglsurface 1
<a name="eglew.h-1721"></a>
<a name="eglew.h-1722"></a>#define EGL_STREAM_BIT_KHR 0x0800
<a name="eglew.h-1723"></a>
<a name="eglew.h-1724"></a>typedef EGLSurface  ( * PFNEGLCREATESTREAMPRODUCERSURFACEKHRPROC) (EGLDisplay  dpy, EGLConfig  config, EGLStreamKHR  stream, const EGLint * attrib_list);
<a name="eglew.h-1725"></a>
<a name="eglew.h-1726"></a>#define eglCreateStreamProducerSurfaceKHR EGLEW_GET_FUN(__eglewCreateStreamProducerSurfaceKHR)
<a name="eglew.h-1727"></a>
<a name="eglew.h-1728"></a>#define EGLEW_KHR_stream_producer_eglsurface EGLEW_GET_VAR(__EGLEW_KHR_stream_producer_eglsurface)
<a name="eglew.h-1729"></a>
<a name="eglew.h-1730"></a>#endif /* EGL_KHR_stream_producer_eglsurface */
<a name="eglew.h-1731"></a>
<a name="eglew.h-1732"></a>/* ---------------------- EGL_KHR_surfaceless_context ---------------------- */
<a name="eglew.h-1733"></a>
<a name="eglew.h-1734"></a>#ifndef EGL_KHR_surfaceless_context
<a name="eglew.h-1735"></a>#define EGL_KHR_surfaceless_context 1
<a name="eglew.h-1736"></a>
<a name="eglew.h-1737"></a>#define EGLEW_KHR_surfaceless_context EGLEW_GET_VAR(__EGLEW_KHR_surfaceless_context)
<a name="eglew.h-1738"></a>
<a name="eglew.h-1739"></a>#endif /* EGL_KHR_surfaceless_context */
<a name="eglew.h-1740"></a>
<a name="eglew.h-1741"></a>/* -------------------- EGL_KHR_swap_buffers_with_damage ------------------- */
<a name="eglew.h-1742"></a>
<a name="eglew.h-1743"></a>#ifndef EGL_KHR_swap_buffers_with_damage
<a name="eglew.h-1744"></a>#define EGL_KHR_swap_buffers_with_damage 1
<a name="eglew.h-1745"></a>
<a name="eglew.h-1746"></a>typedef EGLBoolean  ( * PFNEGLSWAPBUFFERSWITHDAMAGEKHRPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint * rects, EGLint  n_rects);
<a name="eglew.h-1747"></a>
<a name="eglew.h-1748"></a>#define eglSwapBuffersWithDamageKHR EGLEW_GET_FUN(__eglewSwapBuffersWithDamageKHR)
<a name="eglew.h-1749"></a>
<a name="eglew.h-1750"></a>#define EGLEW_KHR_swap_buffers_with_damage EGLEW_GET_VAR(__EGLEW_KHR_swap_buffers_with_damage)
<a name="eglew.h-1751"></a>
<a name="eglew.h-1752"></a>#endif /* EGL_KHR_swap_buffers_with_damage */
<a name="eglew.h-1753"></a>
<a name="eglew.h-1754"></a>/* ------------------------ EGL_KHR_vg_parent_image ------------------------ */
<a name="eglew.h-1755"></a>
<a name="eglew.h-1756"></a>#ifndef EGL_KHR_vg_parent_image
<a name="eglew.h-1757"></a>#define EGL_KHR_vg_parent_image 1
<a name="eglew.h-1758"></a>
<a name="eglew.h-1759"></a>#define EGL_VG_PARENT_IMAGE_KHR 0x30BA
<a name="eglew.h-1760"></a>
<a name="eglew.h-1761"></a>#define EGLEW_KHR_vg_parent_image EGLEW_GET_VAR(__EGLEW_KHR_vg_parent_image)
<a name="eglew.h-1762"></a>
<a name="eglew.h-1763"></a>#endif /* EGL_KHR_vg_parent_image */
<a name="eglew.h-1764"></a>
<a name="eglew.h-1765"></a>/* --------------------------- EGL_KHR_wait_sync --------------------------- */
<a name="eglew.h-1766"></a>
<a name="eglew.h-1767"></a>#ifndef EGL_KHR_wait_sync
<a name="eglew.h-1768"></a>#define EGL_KHR_wait_sync 1
<a name="eglew.h-1769"></a>
<a name="eglew.h-1770"></a>typedef EGLint  ( * PFNEGLWAITSYNCKHRPROC) (EGLDisplay  dpy, EGLSyncKHR  sync, EGLint  flags);
<a name="eglew.h-1771"></a>
<a name="eglew.h-1772"></a>#define eglWaitSyncKHR EGLEW_GET_FUN(__eglewWaitSyncKHR)
<a name="eglew.h-1773"></a>
<a name="eglew.h-1774"></a>#define EGLEW_KHR_wait_sync EGLEW_GET_VAR(__EGLEW_KHR_wait_sync)
<a name="eglew.h-1775"></a>
<a name="eglew.h-1776"></a>#endif /* EGL_KHR_wait_sync */
<a name="eglew.h-1777"></a>
<a name="eglew.h-1778"></a>/* --------------------------- EGL_MESA_drm_image -------------------------- */
<a name="eglew.h-1779"></a>
<a name="eglew.h-1780"></a>#ifndef EGL_MESA_drm_image
<a name="eglew.h-1781"></a>#define EGL_MESA_drm_image 1
<a name="eglew.h-1782"></a>
<a name="eglew.h-1783"></a>#define EGL_DRM_BUFFER_USE_SCANOUT_MESA 0x00000001
<a name="eglew.h-1784"></a>#define EGL_DRM_BUFFER_USE_SHARE_MESA 0x00000002
<a name="eglew.h-1785"></a>#define EGL_DRM_BUFFER_FORMAT_MESA 0x31D0
<a name="eglew.h-1786"></a>#define EGL_DRM_BUFFER_USE_MESA 0x31D1
<a name="eglew.h-1787"></a>#define EGL_DRM_BUFFER_FORMAT_ARGB32_MESA 0x31D2
<a name="eglew.h-1788"></a>#define EGL_DRM_BUFFER_MESA 0x31D3
<a name="eglew.h-1789"></a>#define EGL_DRM_BUFFER_STRIDE_MESA 0x31D4
<a name="eglew.h-1790"></a>
<a name="eglew.h-1791"></a>typedef EGLImageKHR  ( * PFNEGLCREATEDRMIMAGEMESAPROC) (EGLDisplay  dpy, const EGLint * attrib_list);
<a name="eglew.h-1792"></a>typedef EGLBoolean  ( * PFNEGLEXPORTDRMIMAGEMESAPROC) (EGLDisplay  dpy, EGLImageKHR  image, EGLint * name, EGLint * handle, EGLint * stride);
<a name="eglew.h-1793"></a>
<a name="eglew.h-1794"></a>#define eglCreateDRMImageMESA EGLEW_GET_FUN(__eglewCreateDRMImageMESA)
<a name="eglew.h-1795"></a>#define eglExportDRMImageMESA EGLEW_GET_FUN(__eglewExportDRMImageMESA)
<a name="eglew.h-1796"></a>
<a name="eglew.h-1797"></a>#define EGLEW_MESA_drm_image EGLEW_GET_VAR(__EGLEW_MESA_drm_image)
<a name="eglew.h-1798"></a>
<a name="eglew.h-1799"></a>#endif /* EGL_MESA_drm_image */
<a name="eglew.h-1800"></a>
<a name="eglew.h-1801"></a>/* --------------------- EGL_MESA_image_dma_buf_export --------------------- */
<a name="eglew.h-1802"></a>
<a name="eglew.h-1803"></a>#ifndef EGL_MESA_image_dma_buf_export
<a name="eglew.h-1804"></a>#define EGL_MESA_image_dma_buf_export 1
<a name="eglew.h-1805"></a>
<a name="eglew.h-1806"></a>typedef EGLBoolean  ( * PFNEGLEXPORTDMABUFIMAGEMESAPROC) (EGLDisplay  dpy, EGLImageKHR  image, int * fds, EGLint * strides, EGLint * offsets);
<a name="eglew.h-1807"></a>typedef EGLBoolean  ( * PFNEGLEXPORTDMABUFIMAGEQUERYMESAPROC) (EGLDisplay  dpy, EGLImageKHR  image, int * fourcc, int * num_planes, EGLuint64KHR * modifiers);
<a name="eglew.h-1808"></a>
<a name="eglew.h-1809"></a>#define eglExportDMABUFImageMESA EGLEW_GET_FUN(__eglewExportDMABUFImageMESA)
<a name="eglew.h-1810"></a>#define eglExportDMABUFImageQueryMESA EGLEW_GET_FUN(__eglewExportDMABUFImageQueryMESA)
<a name="eglew.h-1811"></a>
<a name="eglew.h-1812"></a>#define EGLEW_MESA_image_dma_buf_export EGLEW_GET_VAR(__EGLEW_MESA_image_dma_buf_export)
<a name="eglew.h-1813"></a>
<a name="eglew.h-1814"></a>#endif /* EGL_MESA_image_dma_buf_export */
<a name="eglew.h-1815"></a>
<a name="eglew.h-1816"></a>/* ------------------------- EGL_MESA_platform_gbm ------------------------- */
<a name="eglew.h-1817"></a>
<a name="eglew.h-1818"></a>#ifndef EGL_MESA_platform_gbm
<a name="eglew.h-1819"></a>#define EGL_MESA_platform_gbm 1
<a name="eglew.h-1820"></a>
<a name="eglew.h-1821"></a>#define EGL_PLATFORM_GBM_MESA 0x31D7
<a name="eglew.h-1822"></a>
<a name="eglew.h-1823"></a>#define EGLEW_MESA_platform_gbm EGLEW_GET_VAR(__EGLEW_MESA_platform_gbm)
<a name="eglew.h-1824"></a>
<a name="eglew.h-1825"></a>#endif /* EGL_MESA_platform_gbm */
<a name="eglew.h-1826"></a>
<a name="eglew.h-1827"></a>/* --------------------- EGL_MESA_platform_surfaceless --------------------- */
<a name="eglew.h-1828"></a>
<a name="eglew.h-1829"></a>#ifndef EGL_MESA_platform_surfaceless
<a name="eglew.h-1830"></a>#define EGL_MESA_platform_surfaceless 1
<a name="eglew.h-1831"></a>
<a name="eglew.h-1832"></a>#define EGL_PLATFORM_SURFACELESS_MESA 0x31DD
<a name="eglew.h-1833"></a>
<a name="eglew.h-1834"></a>#define EGLEW_MESA_platform_surfaceless EGLEW_GET_VAR(__EGLEW_MESA_platform_surfaceless)
<a name="eglew.h-1835"></a>
<a name="eglew.h-1836"></a>#endif /* EGL_MESA_platform_surfaceless */
<a name="eglew.h-1837"></a>
<a name="eglew.h-1838"></a>/* -------------------------- EGL_NOK_swap_region -------------------------- */
<a name="eglew.h-1839"></a>
<a name="eglew.h-1840"></a>#ifndef EGL_NOK_swap_region
<a name="eglew.h-1841"></a>#define EGL_NOK_swap_region 1
<a name="eglew.h-1842"></a>
<a name="eglew.h-1843"></a>typedef EGLBoolean  ( * PFNEGLSWAPBUFFERSREGIONNOKPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  numRects, const EGLint * rects);
<a name="eglew.h-1844"></a>
<a name="eglew.h-1845"></a>#define eglSwapBuffersRegionNOK EGLEW_GET_FUN(__eglewSwapBuffersRegionNOK)
<a name="eglew.h-1846"></a>
<a name="eglew.h-1847"></a>#define EGLEW_NOK_swap_region EGLEW_GET_VAR(__EGLEW_NOK_swap_region)
<a name="eglew.h-1848"></a>
<a name="eglew.h-1849"></a>#endif /* EGL_NOK_swap_region */
<a name="eglew.h-1850"></a>
<a name="eglew.h-1851"></a>/* -------------------------- EGL_NOK_swap_region2 ------------------------- */
<a name="eglew.h-1852"></a>
<a name="eglew.h-1853"></a>#ifndef EGL_NOK_swap_region2
<a name="eglew.h-1854"></a>#define EGL_NOK_swap_region2 1
<a name="eglew.h-1855"></a>
<a name="eglew.h-1856"></a>typedef EGLBoolean  ( * PFNEGLSWAPBUFFERSREGION2NOKPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  numRects, const EGLint * rects);
<a name="eglew.h-1857"></a>
<a name="eglew.h-1858"></a>#define eglSwapBuffersRegion2NOK EGLEW_GET_FUN(__eglewSwapBuffersRegion2NOK)
<a name="eglew.h-1859"></a>
<a name="eglew.h-1860"></a>#define EGLEW_NOK_swap_region2 EGLEW_GET_VAR(__EGLEW_NOK_swap_region2)
<a name="eglew.h-1861"></a>
<a name="eglew.h-1862"></a>#endif /* EGL_NOK_swap_region2 */
<a name="eglew.h-1863"></a>
<a name="eglew.h-1864"></a>/* ---------------------- EGL_NOK_texture_from_pixmap ---------------------- */
<a name="eglew.h-1865"></a>
<a name="eglew.h-1866"></a>#ifndef EGL_NOK_texture_from_pixmap
<a name="eglew.h-1867"></a>#define EGL_NOK_texture_from_pixmap 1
<a name="eglew.h-1868"></a>
<a name="eglew.h-1869"></a>#define EGL_Y_INVERTED_NOK 0x307F
<a name="eglew.h-1870"></a>
<a name="eglew.h-1871"></a>#define EGLEW_NOK_texture_from_pixmap EGLEW_GET_VAR(__EGLEW_NOK_texture_from_pixmap)
<a name="eglew.h-1872"></a>
<a name="eglew.h-1873"></a>#endif /* EGL_NOK_texture_from_pixmap */
<a name="eglew.h-1874"></a>
<a name="eglew.h-1875"></a>/* ------------------------ EGL_NV_3dvision_surface ------------------------ */
<a name="eglew.h-1876"></a>
<a name="eglew.h-1877"></a>#ifndef EGL_NV_3dvision_surface
<a name="eglew.h-1878"></a>#define EGL_NV_3dvision_surface 1
<a name="eglew.h-1879"></a>
<a name="eglew.h-1880"></a>#define EGL_AUTO_STEREO_NV 0x3136
<a name="eglew.h-1881"></a>
<a name="eglew.h-1882"></a>#define EGLEW_NV_3dvision_surface EGLEW_GET_VAR(__EGLEW_NV_3dvision_surface)
<a name="eglew.h-1883"></a>
<a name="eglew.h-1884"></a>#endif /* EGL_NV_3dvision_surface */
<a name="eglew.h-1885"></a>
<a name="eglew.h-1886"></a>/* ------------------------- EGL_NV_coverage_sample ------------------------ */
<a name="eglew.h-1887"></a>
<a name="eglew.h-1888"></a>#ifndef EGL_NV_coverage_sample
<a name="eglew.h-1889"></a>#define EGL_NV_coverage_sample 1
<a name="eglew.h-1890"></a>
<a name="eglew.h-1891"></a>#define EGL_COVERAGE_BUFFERS_NV 0x30E0
<a name="eglew.h-1892"></a>#define EGL_COVERAGE_SAMPLES_NV 0x30E1
<a name="eglew.h-1893"></a>
<a name="eglew.h-1894"></a>#define EGLEW_NV_coverage_sample EGLEW_GET_VAR(__EGLEW_NV_coverage_sample)
<a name="eglew.h-1895"></a>
<a name="eglew.h-1896"></a>#endif /* EGL_NV_coverage_sample */
<a name="eglew.h-1897"></a>
<a name="eglew.h-1898"></a>/* --------------------- EGL_NV_coverage_sample_resolve -------------------- */
<a name="eglew.h-1899"></a>
<a name="eglew.h-1900"></a>#ifndef EGL_NV_coverage_sample_resolve
<a name="eglew.h-1901"></a>#define EGL_NV_coverage_sample_resolve 1
<a name="eglew.h-1902"></a>
<a name="eglew.h-1903"></a>#define EGL_COVERAGE_SAMPLE_RESOLVE_NV 0x3131
<a name="eglew.h-1904"></a>#define EGL_COVERAGE_SAMPLE_RESOLVE_DEFAULT_NV 0x3132
<a name="eglew.h-1905"></a>#define EGL_COVERAGE_SAMPLE_RESOLVE_NONE_NV 0x3133
<a name="eglew.h-1906"></a>
<a name="eglew.h-1907"></a>#define EGLEW_NV_coverage_sample_resolve EGLEW_GET_VAR(__EGLEW_NV_coverage_sample_resolve)
<a name="eglew.h-1908"></a>
<a name="eglew.h-1909"></a>#endif /* EGL_NV_coverage_sample_resolve */
<a name="eglew.h-1910"></a>
<a name="eglew.h-1911"></a>/* --------------------------- EGL_NV_cuda_event --------------------------- */
<a name="eglew.h-1912"></a>
<a name="eglew.h-1913"></a>#ifndef EGL_NV_cuda_event
<a name="eglew.h-1914"></a>#define EGL_NV_cuda_event 1
<a name="eglew.h-1915"></a>
<a name="eglew.h-1916"></a>#define EGL_CUDA_EVENT_HANDLE_NV 0x323B
<a name="eglew.h-1917"></a>#define EGL_SYNC_CUDA_EVENT_NV 0x323C
<a name="eglew.h-1918"></a>#define EGL_SYNC_CUDA_EVENT_COMPLETE_NV 0x323D
<a name="eglew.h-1919"></a>
<a name="eglew.h-1920"></a>#define EGLEW_NV_cuda_event EGLEW_GET_VAR(__EGLEW_NV_cuda_event)
<a name="eglew.h-1921"></a>
<a name="eglew.h-1922"></a>#endif /* EGL_NV_cuda_event */
<a name="eglew.h-1923"></a>
<a name="eglew.h-1924"></a>/* ------------------------- EGL_NV_depth_nonlinear ------------------------ */
<a name="eglew.h-1925"></a>
<a name="eglew.h-1926"></a>#ifndef EGL_NV_depth_nonlinear
<a name="eglew.h-1927"></a>#define EGL_NV_depth_nonlinear 1
<a name="eglew.h-1928"></a>
<a name="eglew.h-1929"></a>#define EGL_DEPTH_ENCODING_NONE_NV 0
<a name="eglew.h-1930"></a>#define EGL_DEPTH_ENCODING_NV 0x30E2
<a name="eglew.h-1931"></a>#define EGL_DEPTH_ENCODING_NONLINEAR_NV 0x30E3
<a name="eglew.h-1932"></a>
<a name="eglew.h-1933"></a>#define EGLEW_NV_depth_nonlinear EGLEW_GET_VAR(__EGLEW_NV_depth_nonlinear)
<a name="eglew.h-1934"></a>
<a name="eglew.h-1935"></a>#endif /* EGL_NV_depth_nonlinear */
<a name="eglew.h-1936"></a>
<a name="eglew.h-1937"></a>/* --------------------------- EGL_NV_device_cuda -------------------------- */
<a name="eglew.h-1938"></a>
<a name="eglew.h-1939"></a>#ifndef EGL_NV_device_cuda
<a name="eglew.h-1940"></a>#define EGL_NV_device_cuda 1
<a name="eglew.h-1941"></a>
<a name="eglew.h-1942"></a>#define EGL_CUDA_DEVICE_NV 0x323A
<a name="eglew.h-1943"></a>
<a name="eglew.h-1944"></a>#define EGLEW_NV_device_cuda EGLEW_GET_VAR(__EGLEW_NV_device_cuda)
<a name="eglew.h-1945"></a>
<a name="eglew.h-1946"></a>#endif /* EGL_NV_device_cuda */
<a name="eglew.h-1947"></a>
<a name="eglew.h-1948"></a>/* -------------------------- EGL_NV_native_query -------------------------- */
<a name="eglew.h-1949"></a>
<a name="eglew.h-1950"></a>#ifndef EGL_NV_native_query
<a name="eglew.h-1951"></a>#define EGL_NV_native_query 1
<a name="eglew.h-1952"></a>
<a name="eglew.h-1953"></a>typedef EGLBoolean  ( * PFNEGLQUERYNATIVEDISPLAYNVPROC) (EGLDisplay  dpy, EGLNativeDisplayType * display_id);
<a name="eglew.h-1954"></a>typedef EGLBoolean  ( * PFNEGLQUERYNATIVEPIXMAPNVPROC) (EGLDisplay  dpy, EGLSurface  surf, EGLNativePixmapType * pixmap);
<a name="eglew.h-1955"></a>typedef EGLBoolean  ( * PFNEGLQUERYNATIVEWINDOWNVPROC) (EGLDisplay  dpy, EGLSurface  surf, EGLNativeWindowType * window);
<a name="eglew.h-1956"></a>
<a name="eglew.h-1957"></a>#define eglQueryNativeDisplayNV EGLEW_GET_FUN(__eglewQueryNativeDisplayNV)
<a name="eglew.h-1958"></a>#define eglQueryNativePixmapNV EGLEW_GET_FUN(__eglewQueryNativePixmapNV)
<a name="eglew.h-1959"></a>#define eglQueryNativeWindowNV EGLEW_GET_FUN(__eglewQueryNativeWindowNV)
<a name="eglew.h-1960"></a>
<a name="eglew.h-1961"></a>#define EGLEW_NV_native_query EGLEW_GET_VAR(__EGLEW_NV_native_query)
<a name="eglew.h-1962"></a>
<a name="eglew.h-1963"></a>#endif /* EGL_NV_native_query */
<a name="eglew.h-1964"></a>
<a name="eglew.h-1965"></a>/* ---------------------- EGL_NV_post_convert_rounding --------------------- */
<a name="eglew.h-1966"></a>
<a name="eglew.h-1967"></a>#ifndef EGL_NV_post_convert_rounding
<a name="eglew.h-1968"></a>#define EGL_NV_post_convert_rounding 1
<a name="eglew.h-1969"></a>
<a name="eglew.h-1970"></a>#define EGLEW_NV_post_convert_rounding EGLEW_GET_VAR(__EGLEW_NV_post_convert_rounding)
<a name="eglew.h-1971"></a>
<a name="eglew.h-1972"></a>#endif /* EGL_NV_post_convert_rounding */
<a name="eglew.h-1973"></a>
<a name="eglew.h-1974"></a>/* ------------------------- EGL_NV_post_sub_buffer ------------------------ */
<a name="eglew.h-1975"></a>
<a name="eglew.h-1976"></a>#ifndef EGL_NV_post_sub_buffer
<a name="eglew.h-1977"></a>#define EGL_NV_post_sub_buffer 1
<a name="eglew.h-1978"></a>
<a name="eglew.h-1979"></a>#define EGL_POST_SUB_BUFFER_SUPPORTED_NV 0x30BE
<a name="eglew.h-1980"></a>
<a name="eglew.h-1981"></a>typedef EGLBoolean  ( * PFNEGLPOSTSUBBUFFERNVPROC) (EGLDisplay  dpy, EGLSurface  surface, EGLint  x, EGLint  y, EGLint  width, EGLint  height);
<a name="eglew.h-1982"></a>
<a name="eglew.h-1983"></a>#define eglPostSubBufferNV EGLEW_GET_FUN(__eglewPostSubBufferNV)
<a name="eglew.h-1984"></a>
<a name="eglew.h-1985"></a>#define EGLEW_NV_post_sub_buffer EGLEW_GET_VAR(__EGLEW_NV_post_sub_buffer)
<a name="eglew.h-1986"></a>
<a name="eglew.h-1987"></a>#endif /* EGL_NV_post_sub_buffer */
<a name="eglew.h-1988"></a>
<a name="eglew.h-1989"></a>/* ------------------ EGL_NV_robustness_video_memory_purge ----------------- */
<a name="eglew.h-1990"></a>
<a name="eglew.h-1991"></a>#ifndef EGL_NV_robustness_video_memory_purge
<a name="eglew.h-1992"></a>#define EGL_NV_robustness_video_memory_purge 1
<a name="eglew.h-1993"></a>
<a name="eglew.h-1994"></a>#define EGL_GENERATE_RESET_ON_VIDEO_MEMORY_PURGE_NV 0x334C
<a name="eglew.h-1995"></a>
<a name="eglew.h-1996"></a>#define EGLEW_NV_robustness_video_memory_purge EGLEW_GET_VAR(__EGLEW_NV_robustness_video_memory_purge)
<a name="eglew.h-1997"></a>
<a name="eglew.h-1998"></a>#endif /* EGL_NV_robustness_video_memory_purge */
<a name="eglew.h-1999"></a>
<a name="eglew.h-2000"></a>/* ------------------ EGL_NV_stream_consumer_gltexture_yuv ----------------- */
<a name="eglew.h-2001"></a>
<a name="eglew.h-2002"></a>#ifndef EGL_NV_stream_consumer_gltexture_yuv
<a name="eglew.h-2003"></a>#define EGL_NV_stream_consumer_gltexture_yuv 1
<a name="eglew.h-2004"></a>
<a name="eglew.h-2005"></a>#define EGL_YUV_BUFFER_EXT 0x3300
<a name="eglew.h-2006"></a>#define EGL_YUV_NUMBER_OF_PLANES_EXT 0x3311
<a name="eglew.h-2007"></a>#define EGL_YUV_PLANE0_TEXTURE_UNIT_NV 0x332C
<a name="eglew.h-2008"></a>#define EGL_YUV_PLANE1_TEXTURE_UNIT_NV 0x332D
<a name="eglew.h-2009"></a>#define EGL_YUV_PLANE2_TEXTURE_UNIT_NV 0x332E
<a name="eglew.h-2010"></a>
<a name="eglew.h-2011"></a>typedef EGLBoolean  ( * PFNEGLSTREAMCONSUMERGLTEXTUREEXTERNALATTRIBSNVPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLAttrib  *attrib_list);
<a name="eglew.h-2012"></a>
<a name="eglew.h-2013"></a>#define eglStreamConsumerGLTextureExternalAttribsNV EGLEW_GET_FUN(__eglewStreamConsumerGLTextureExternalAttribsNV)
<a name="eglew.h-2014"></a>
<a name="eglew.h-2015"></a>#define EGLEW_NV_stream_consumer_gltexture_yuv EGLEW_GET_VAR(__EGLEW_NV_stream_consumer_gltexture_yuv)
<a name="eglew.h-2016"></a>
<a name="eglew.h-2017"></a>#endif /* EGL_NV_stream_consumer_gltexture_yuv */
<a name="eglew.h-2018"></a>
<a name="eglew.h-2019"></a>/* ---------------------- EGL_NV_stream_cross_display ---------------------- */
<a name="eglew.h-2020"></a>
<a name="eglew.h-2021"></a>#ifndef EGL_NV_stream_cross_display
<a name="eglew.h-2022"></a>#define EGL_NV_stream_cross_display 1
<a name="eglew.h-2023"></a>
<a name="eglew.h-2024"></a>#define EGL_STREAM_CROSS_DISPLAY_NV 0x334E
<a name="eglew.h-2025"></a>
<a name="eglew.h-2026"></a>#define EGLEW_NV_stream_cross_display EGLEW_GET_VAR(__EGLEW_NV_stream_cross_display)
<a name="eglew.h-2027"></a>
<a name="eglew.h-2028"></a>#endif /* EGL_NV_stream_cross_display */
<a name="eglew.h-2029"></a>
<a name="eglew.h-2030"></a>/* ----------------------- EGL_NV_stream_cross_object ---------------------- */
<a name="eglew.h-2031"></a>
<a name="eglew.h-2032"></a>#ifndef EGL_NV_stream_cross_object
<a name="eglew.h-2033"></a>#define EGL_NV_stream_cross_object 1
<a name="eglew.h-2034"></a>
<a name="eglew.h-2035"></a>#define EGL_STREAM_CROSS_OBJECT_NV 0x334D
<a name="eglew.h-2036"></a>
<a name="eglew.h-2037"></a>#define EGLEW_NV_stream_cross_object EGLEW_GET_VAR(__EGLEW_NV_stream_cross_object)
<a name="eglew.h-2038"></a>
<a name="eglew.h-2039"></a>#endif /* EGL_NV_stream_cross_object */
<a name="eglew.h-2040"></a>
<a name="eglew.h-2041"></a>/* --------------------- EGL_NV_stream_cross_partition --------------------- */
<a name="eglew.h-2042"></a>
<a name="eglew.h-2043"></a>#ifndef EGL_NV_stream_cross_partition
<a name="eglew.h-2044"></a>#define EGL_NV_stream_cross_partition 1
<a name="eglew.h-2045"></a>
<a name="eglew.h-2046"></a>#define EGL_STREAM_CROSS_PARTITION_NV 0x323F
<a name="eglew.h-2047"></a>
<a name="eglew.h-2048"></a>#define EGLEW_NV_stream_cross_partition EGLEW_GET_VAR(__EGLEW_NV_stream_cross_partition)
<a name="eglew.h-2049"></a>
<a name="eglew.h-2050"></a>#endif /* EGL_NV_stream_cross_partition */
<a name="eglew.h-2051"></a>
<a name="eglew.h-2052"></a>/* ---------------------- EGL_NV_stream_cross_process ---------------------- */
<a name="eglew.h-2053"></a>
<a name="eglew.h-2054"></a>#ifndef EGL_NV_stream_cross_process
<a name="eglew.h-2055"></a>#define EGL_NV_stream_cross_process 1
<a name="eglew.h-2056"></a>
<a name="eglew.h-2057"></a>#define EGL_STREAM_CROSS_PROCESS_NV 0x3245
<a name="eglew.h-2058"></a>
<a name="eglew.h-2059"></a>#define EGLEW_NV_stream_cross_process EGLEW_GET_VAR(__EGLEW_NV_stream_cross_process)
<a name="eglew.h-2060"></a>
<a name="eglew.h-2061"></a>#endif /* EGL_NV_stream_cross_process */
<a name="eglew.h-2062"></a>
<a name="eglew.h-2063"></a>/* ----------------------- EGL_NV_stream_cross_system ---------------------- */
<a name="eglew.h-2064"></a>
<a name="eglew.h-2065"></a>#ifndef EGL_NV_stream_cross_system
<a name="eglew.h-2066"></a>#define EGL_NV_stream_cross_system 1
<a name="eglew.h-2067"></a>
<a name="eglew.h-2068"></a>#define EGL_STREAM_CROSS_SYSTEM_NV 0x334F
<a name="eglew.h-2069"></a>
<a name="eglew.h-2070"></a>#define EGLEW_NV_stream_cross_system EGLEW_GET_VAR(__EGLEW_NV_stream_cross_system)
<a name="eglew.h-2071"></a>
<a name="eglew.h-2072"></a>#endif /* EGL_NV_stream_cross_system */
<a name="eglew.h-2073"></a>
<a name="eglew.h-2074"></a>/* ------------------------ EGL_NV_stream_fifo_next ------------------------ */
<a name="eglew.h-2075"></a>
<a name="eglew.h-2076"></a>#ifndef EGL_NV_stream_fifo_next
<a name="eglew.h-2077"></a>#define EGL_NV_stream_fifo_next 1
<a name="eglew.h-2078"></a>
<a name="eglew.h-2079"></a>#define EGL_PENDING_FRAME_NV 0x3329
<a name="eglew.h-2080"></a>#define EGL_STREAM_TIME_PENDING_NV 0x332A
<a name="eglew.h-2081"></a>
<a name="eglew.h-2082"></a>#define EGLEW_NV_stream_fifo_next EGLEW_GET_VAR(__EGLEW_NV_stream_fifo_next)
<a name="eglew.h-2083"></a>
<a name="eglew.h-2084"></a>#endif /* EGL_NV_stream_fifo_next */
<a name="eglew.h-2085"></a>
<a name="eglew.h-2086"></a>/* --------------------- EGL_NV_stream_fifo_synchronous -------------------- */
<a name="eglew.h-2087"></a>
<a name="eglew.h-2088"></a>#ifndef EGL_NV_stream_fifo_synchronous
<a name="eglew.h-2089"></a>#define EGL_NV_stream_fifo_synchronous 1
<a name="eglew.h-2090"></a>
<a name="eglew.h-2091"></a>#define EGL_STREAM_FIFO_SYNCHRONOUS_NV 0x3336
<a name="eglew.h-2092"></a>
<a name="eglew.h-2093"></a>#define EGLEW_NV_stream_fifo_synchronous EGLEW_GET_VAR(__EGLEW_NV_stream_fifo_synchronous)
<a name="eglew.h-2094"></a>
<a name="eglew.h-2095"></a>#endif /* EGL_NV_stream_fifo_synchronous */
<a name="eglew.h-2096"></a>
<a name="eglew.h-2097"></a>/* ----------------------- EGL_NV_stream_frame_limits ---------------------- */
<a name="eglew.h-2098"></a>
<a name="eglew.h-2099"></a>#ifndef EGL_NV_stream_frame_limits
<a name="eglew.h-2100"></a>#define EGL_NV_stream_frame_limits 1
<a name="eglew.h-2101"></a>
<a name="eglew.h-2102"></a>#define EGL_PRODUCER_MAX_FRAME_HINT_NV 0x3337
<a name="eglew.h-2103"></a>#define EGL_CONSUMER_MAX_FRAME_HINT_NV 0x3338
<a name="eglew.h-2104"></a>
<a name="eglew.h-2105"></a>#define EGLEW_NV_stream_frame_limits EGLEW_GET_VAR(__EGLEW_NV_stream_frame_limits)
<a name="eglew.h-2106"></a>
<a name="eglew.h-2107"></a>#endif /* EGL_NV_stream_frame_limits */
<a name="eglew.h-2108"></a>
<a name="eglew.h-2109"></a>/* ------------------------- EGL_NV_stream_metadata ------------------------ */
<a name="eglew.h-2110"></a>
<a name="eglew.h-2111"></a>#ifndef EGL_NV_stream_metadata
<a name="eglew.h-2112"></a>#define EGL_NV_stream_metadata 1
<a name="eglew.h-2113"></a>
<a name="eglew.h-2114"></a>#define EGL_MAX_STREAM_METADATA_BLOCKS_NV 0x3250
<a name="eglew.h-2115"></a>#define EGL_MAX_STREAM_METADATA_BLOCK_SIZE_NV 0x3251
<a name="eglew.h-2116"></a>#define EGL_MAX_STREAM_METADATA_TOTAL_SIZE_NV 0x3252
<a name="eglew.h-2117"></a>#define EGL_PRODUCER_METADATA_NV 0x3253
<a name="eglew.h-2118"></a>#define EGL_CONSUMER_METADATA_NV 0x3254
<a name="eglew.h-2119"></a>#define EGL_METADATA0_SIZE_NV 0x3255
<a name="eglew.h-2120"></a>#define EGL_METADATA1_SIZE_NV 0x3256
<a name="eglew.h-2121"></a>#define EGL_METADATA2_SIZE_NV 0x3257
<a name="eglew.h-2122"></a>#define EGL_METADATA3_SIZE_NV 0x3258
<a name="eglew.h-2123"></a>#define EGL_METADATA0_TYPE_NV 0x3259
<a name="eglew.h-2124"></a>#define EGL_METADATA1_TYPE_NV 0x325A
<a name="eglew.h-2125"></a>#define EGL_METADATA2_TYPE_NV 0x325B
<a name="eglew.h-2126"></a>#define EGL_METADATA3_TYPE_NV 0x325C
<a name="eglew.h-2127"></a>#define EGL_PENDING_METADATA_NV 0x3328
<a name="eglew.h-2128"></a>
<a name="eglew.h-2129"></a>typedef EGLBoolean  ( * PFNEGLQUERYDISPLAYATTRIBNVPROC) (EGLDisplay  dpy, EGLint  attribute, EGLAttrib * value);
<a name="eglew.h-2130"></a>typedef EGLBoolean  ( * PFNEGLQUERYSTREAMMETADATANVPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  name, EGLint  n, EGLint  offset, EGLint  size, void * data);
<a name="eglew.h-2131"></a>typedef EGLBoolean  ( * PFNEGLSETSTREAMMETADATANVPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLint  n, EGLint  offset, EGLint  size, const void * data);
<a name="eglew.h-2132"></a>
<a name="eglew.h-2133"></a>#define eglQueryDisplayAttribNV EGLEW_GET_FUN(__eglewQueryDisplayAttribNV)
<a name="eglew.h-2134"></a>#define eglQueryStreamMetadataNV EGLEW_GET_FUN(__eglewQueryStreamMetadataNV)
<a name="eglew.h-2135"></a>#define eglSetStreamMetadataNV EGLEW_GET_FUN(__eglewSetStreamMetadataNV)
<a name="eglew.h-2136"></a>
<a name="eglew.h-2137"></a>#define EGLEW_NV_stream_metadata EGLEW_GET_VAR(__EGLEW_NV_stream_metadata)
<a name="eglew.h-2138"></a>
<a name="eglew.h-2139"></a>#endif /* EGL_NV_stream_metadata */
<a name="eglew.h-2140"></a>
<a name="eglew.h-2141"></a>/* -------------------------- EGL_NV_stream_remote ------------------------- */
<a name="eglew.h-2142"></a>
<a name="eglew.h-2143"></a>#ifndef EGL_NV_stream_remote
<a name="eglew.h-2144"></a>#define EGL_NV_stream_remote 1
<a name="eglew.h-2145"></a>
<a name="eglew.h-2146"></a>#define EGL_STREAM_STATE_INITIALIZING_NV 0x3240
<a name="eglew.h-2147"></a>#define EGL_STREAM_TYPE_NV 0x3241
<a name="eglew.h-2148"></a>#define EGL_STREAM_PROTOCOL_NV 0x3242
<a name="eglew.h-2149"></a>#define EGL_STREAM_ENDPOINT_NV 0x3243
<a name="eglew.h-2150"></a>#define EGL_STREAM_LOCAL_NV 0x3244
<a name="eglew.h-2151"></a>#define EGL_STREAM_PROTOCOL_FD_NV 0x3246
<a name="eglew.h-2152"></a>#define EGL_STREAM_PRODUCER_NV 0x3247
<a name="eglew.h-2153"></a>#define EGL_STREAM_CONSUMER_NV 0x3248
<a name="eglew.h-2154"></a>
<a name="eglew.h-2155"></a>#define EGLEW_NV_stream_remote EGLEW_GET_VAR(__EGLEW_NV_stream_remote)
<a name="eglew.h-2156"></a>
<a name="eglew.h-2157"></a>#endif /* EGL_NV_stream_remote */
<a name="eglew.h-2158"></a>
<a name="eglew.h-2159"></a>/* -------------------------- EGL_NV_stream_reset -------------------------- */
<a name="eglew.h-2160"></a>
<a name="eglew.h-2161"></a>#ifndef EGL_NV_stream_reset
<a name="eglew.h-2162"></a>#define EGL_NV_stream_reset 1
<a name="eglew.h-2163"></a>
<a name="eglew.h-2164"></a>#define EGL_SUPPORT_RESET_NV 0x3334
<a name="eglew.h-2165"></a>#define EGL_SUPPORT_REUSE_NV 0x3335
<a name="eglew.h-2166"></a>
<a name="eglew.h-2167"></a>typedef EGLBoolean  ( * PFNEGLRESETSTREAMNVPROC) (EGLDisplay  dpy, EGLStreamKHR  stream);
<a name="eglew.h-2168"></a>
<a name="eglew.h-2169"></a>#define eglResetStreamNV EGLEW_GET_FUN(__eglewResetStreamNV)
<a name="eglew.h-2170"></a>
<a name="eglew.h-2171"></a>#define EGLEW_NV_stream_reset EGLEW_GET_VAR(__EGLEW_NV_stream_reset)
<a name="eglew.h-2172"></a>
<a name="eglew.h-2173"></a>#endif /* EGL_NV_stream_reset */
<a name="eglew.h-2174"></a>
<a name="eglew.h-2175"></a>/* -------------------------- EGL_NV_stream_socket ------------------------- */
<a name="eglew.h-2176"></a>
<a name="eglew.h-2177"></a>#ifndef EGL_NV_stream_socket
<a name="eglew.h-2178"></a>#define EGL_NV_stream_socket 1
<a name="eglew.h-2179"></a>
<a name="eglew.h-2180"></a>#define EGL_STREAM_PROTOCOL_SOCKET_NV 0x324B
<a name="eglew.h-2181"></a>#define EGL_SOCKET_HANDLE_NV 0x324C
<a name="eglew.h-2182"></a>#define EGL_SOCKET_TYPE_NV 0x324D
<a name="eglew.h-2183"></a>
<a name="eglew.h-2184"></a>#define EGLEW_NV_stream_socket EGLEW_GET_VAR(__EGLEW_NV_stream_socket)
<a name="eglew.h-2185"></a>
<a name="eglew.h-2186"></a>#endif /* EGL_NV_stream_socket */
<a name="eglew.h-2187"></a>
<a name="eglew.h-2188"></a>/* ----------------------- EGL_NV_stream_socket_inet ----------------------- */
<a name="eglew.h-2189"></a>
<a name="eglew.h-2190"></a>#ifndef EGL_NV_stream_socket_inet
<a name="eglew.h-2191"></a>#define EGL_NV_stream_socket_inet 1
<a name="eglew.h-2192"></a>
<a name="eglew.h-2193"></a>#define EGL_SOCKET_TYPE_INET_NV 0x324F
<a name="eglew.h-2194"></a>
<a name="eglew.h-2195"></a>#define EGLEW_NV_stream_socket_inet EGLEW_GET_VAR(__EGLEW_NV_stream_socket_inet)
<a name="eglew.h-2196"></a>
<a name="eglew.h-2197"></a>#endif /* EGL_NV_stream_socket_inet */
<a name="eglew.h-2198"></a>
<a name="eglew.h-2199"></a>/* ----------------------- EGL_NV_stream_socket_unix ----------------------- */
<a name="eglew.h-2200"></a>
<a name="eglew.h-2201"></a>#ifndef EGL_NV_stream_socket_unix
<a name="eglew.h-2202"></a>#define EGL_NV_stream_socket_unix 1
<a name="eglew.h-2203"></a>
<a name="eglew.h-2204"></a>#define EGL_SOCKET_TYPE_UNIX_NV 0x324E
<a name="eglew.h-2205"></a>
<a name="eglew.h-2206"></a>#define EGLEW_NV_stream_socket_unix EGLEW_GET_VAR(__EGLEW_NV_stream_socket_unix)
<a name="eglew.h-2207"></a>
<a name="eglew.h-2208"></a>#endif /* EGL_NV_stream_socket_unix */
<a name="eglew.h-2209"></a>
<a name="eglew.h-2210"></a>/* --------------------------- EGL_NV_stream_sync -------------------------- */
<a name="eglew.h-2211"></a>
<a name="eglew.h-2212"></a>#ifndef EGL_NV_stream_sync
<a name="eglew.h-2213"></a>#define EGL_NV_stream_sync 1
<a name="eglew.h-2214"></a>
<a name="eglew.h-2215"></a>#define EGL_SYNC_TYPE_KHR 0x30F7
<a name="eglew.h-2216"></a>#define EGL_SYNC_NEW_FRAME_NV 0x321F
<a name="eglew.h-2217"></a>
<a name="eglew.h-2218"></a>typedef EGLSyncKHR  ( * PFNEGLCREATESTREAMSYNCNVPROC) (EGLDisplay  dpy, EGLStreamKHR  stream, EGLenum  type, const EGLint * attrib_list);
<a name="eglew.h-2219"></a>
<a name="eglew.h-2220"></a>#define eglCreateStreamSyncNV EGLEW_GET_FUN(__eglewCreateStreamSyncNV)
<a name="eglew.h-2221"></a>
<a name="eglew.h-2222"></a>#define EGLEW_NV_stream_sync EGLEW_GET_VAR(__EGLEW_NV_stream_sync)
<a name="eglew.h-2223"></a>
<a name="eglew.h-2224"></a>#endif /* EGL_NV_stream_sync */
<a name="eglew.h-2225"></a>
<a name="eglew.h-2226"></a>/* ------------------------------ EGL_NV_sync ------------------------------ */
<a name="eglew.h-2227"></a>
<a name="eglew.h-2228"></a>#ifndef EGL_NV_sync
<a name="eglew.h-2229"></a>#define EGL_NV_sync 1
<a name="eglew.h-2230"></a>
<a name="eglew.h-2231"></a>#define EGL_SYNC_FLUSH_COMMANDS_BIT_NV 0x0001
<a name="eglew.h-2232"></a>#define EGL_SYNC_PRIOR_COMMANDS_COMPLETE_NV 0x30E6
<a name="eglew.h-2233"></a>#define EGL_SYNC_STATUS_NV 0x30E7
<a name="eglew.h-2234"></a>#define EGL_SIGNALED_NV 0x30E8
<a name="eglew.h-2235"></a>#define EGL_UNSIGNALED_NV 0x30E9
<a name="eglew.h-2236"></a>#define EGL_ALREADY_SIGNALED_NV 0x30EA
<a name="eglew.h-2237"></a>#define EGL_TIMEOUT_EXPIRED_NV 0x30EB
<a name="eglew.h-2238"></a>#define EGL_CONDITION_SATISFIED_NV 0x30EC
<a name="eglew.h-2239"></a>#define EGL_SYNC_TYPE_NV 0x30ED
<a name="eglew.h-2240"></a>#define EGL_SYNC_CONDITION_NV 0x30EE
<a name="eglew.h-2241"></a>#define EGL_SYNC_FENCE_NV 0x30EF
<a name="eglew.h-2242"></a>#define EGL_FOREVER_NV 0xFFFFFFFFFFFFFFFF
<a name="eglew.h-2243"></a>
<a name="eglew.h-2244"></a>typedef EGLint  ( * PFNEGLCLIENTWAITSYNCNVPROC) (EGLSyncNV  sync, EGLint  flags, EGLTimeNV  timeout);
<a name="eglew.h-2245"></a>typedef EGLSyncNV  ( * PFNEGLCREATEFENCESYNCNVPROC) (EGLDisplay  dpy, EGLenum  condition, const EGLint * attrib_list);
<a name="eglew.h-2246"></a>typedef EGLBoolean  ( * PFNEGLDESTROYSYNCNVPROC) (EGLSyncNV  sync);
<a name="eglew.h-2247"></a>typedef EGLBoolean  ( * PFNEGLFENCENVPROC) (EGLSyncNV  sync);
<a name="eglew.h-2248"></a>typedef EGLBoolean  ( * PFNEGLGETSYNCATTRIBNVPROC) (EGLSyncNV  sync, EGLint  attribute, EGLint * value);
<a name="eglew.h-2249"></a>typedef EGLBoolean  ( * PFNEGLSIGNALSYNCNVPROC) (EGLSyncNV  sync, EGLenum  mode);
<a name="eglew.h-2250"></a>
<a name="eglew.h-2251"></a>#define eglClientWaitSyncNV EGLEW_GET_FUN(__eglewClientWaitSyncNV)
<a name="eglew.h-2252"></a>#define eglCreateFenceSyncNV EGLEW_GET_FUN(__eglewCreateFenceSyncNV)
<a name="eglew.h-2253"></a>#define eglDestroySyncNV EGLEW_GET_FUN(__eglewDestroySyncNV)
<a name="eglew.h-2254"></a>#define eglFenceNV EGLEW_GET_FUN(__eglewFenceNV)
<a name="eglew.h-2255"></a>#define eglGetSyncAttribNV EGLEW_GET_FUN(__eglewGetSyncAttribNV)
<a name="eglew.h-2256"></a>#define eglSignalSyncNV EGLEW_GET_FUN(__eglewSignalSyncNV)
<a name="eglew.h-2257"></a>
<a name="eglew.h-2258"></a>#define EGLEW_NV_sync EGLEW_GET_VAR(__EGLEW_NV_sync)
<a name="eglew.h-2259"></a>
<a name="eglew.h-2260"></a>#endif /* EGL_NV_sync */
<a name="eglew.h-2261"></a>
<a name="eglew.h-2262"></a>/* --------------------------- EGL_NV_system_time -------------------------- */
<a name="eglew.h-2263"></a>
<a name="eglew.h-2264"></a>#ifndef EGL_NV_system_time
<a name="eglew.h-2265"></a>#define EGL_NV_system_time 1
<a name="eglew.h-2266"></a>
<a name="eglew.h-2267"></a>typedef EGLuint64NV  ( * PFNEGLGETSYSTEMTIMEFREQUENCYNVPROC) ( void );
<a name="eglew.h-2268"></a>typedef EGLuint64NV  ( * PFNEGLGETSYSTEMTIMENVPROC) ( void );
<a name="eglew.h-2269"></a>
<a name="eglew.h-2270"></a>#define eglGetSystemTimeFrequencyNV EGLEW_GET_FUN(__eglewGetSystemTimeFrequencyNV)
<a name="eglew.h-2271"></a>#define eglGetSystemTimeNV EGLEW_GET_FUN(__eglewGetSystemTimeNV)
<a name="eglew.h-2272"></a>
<a name="eglew.h-2273"></a>#define EGLEW_NV_system_time EGLEW_GET_VAR(__EGLEW_NV_system_time)
<a name="eglew.h-2274"></a>
<a name="eglew.h-2275"></a>#endif /* EGL_NV_system_time */
<a name="eglew.h-2276"></a>
<a name="eglew.h-2277"></a>/* --------------------- EGL_TIZEN_image_native_buffer --------------------- */
<a name="eglew.h-2278"></a>
<a name="eglew.h-2279"></a>#ifndef EGL_TIZEN_image_native_buffer
<a name="eglew.h-2280"></a>#define EGL_TIZEN_image_native_buffer 1
<a name="eglew.h-2281"></a>
<a name="eglew.h-2282"></a>#define EGL_NATIVE_BUFFER_TIZEN 0x32A0
<a name="eglew.h-2283"></a>
<a name="eglew.h-2284"></a>#define EGLEW_TIZEN_image_native_buffer EGLEW_GET_VAR(__EGLEW_TIZEN_image_native_buffer)
<a name="eglew.h-2285"></a>
<a name="eglew.h-2286"></a>#endif /* EGL_TIZEN_image_native_buffer */
<a name="eglew.h-2287"></a>
<a name="eglew.h-2288"></a>/* --------------------- EGL_TIZEN_image_native_surface -------------------- */
<a name="eglew.h-2289"></a>
<a name="eglew.h-2290"></a>#ifndef EGL_TIZEN_image_native_surface
<a name="eglew.h-2291"></a>#define EGL_TIZEN_image_native_surface 1
<a name="eglew.h-2292"></a>
<a name="eglew.h-2293"></a>#define EGL_NATIVE_SURFACE_TIZEN 0x32A1
<a name="eglew.h-2294"></a>
<a name="eglew.h-2295"></a>#define EGLEW_TIZEN_image_native_surface EGLEW_GET_VAR(__EGLEW_TIZEN_image_native_surface)
<a name="eglew.h-2296"></a>
<a name="eglew.h-2297"></a>#endif /* EGL_TIZEN_image_native_surface */
<a name="eglew.h-2298"></a>
<a name="eglew.h-2299"></a>/* ------------------------------------------------------------------------- */
<a name="eglew.h-2300"></a>
<a name="eglew.h-2301"></a>#define EGLEW_FUN_EXPORT GLEW_FUN_EXPORT
<a name="eglew.h-2302"></a>#define EGLEW_VAR_EXPORT GLEW_VAR_EXPORT
<a name="eglew.h-2303"></a>
<a name="eglew.h-2304"></a>EGLEW_FUN_EXPORT PFNEGLCHOOSECONFIGPROC __eglewChooseConfig;
<a name="eglew.h-2305"></a>EGLEW_FUN_EXPORT PFNEGLCOPYBUFFERSPROC __eglewCopyBuffers;
<a name="eglew.h-2306"></a>EGLEW_FUN_EXPORT PFNEGLCREATECONTEXTPROC __eglewCreateContext;
<a name="eglew.h-2307"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPBUFFERSURFACEPROC __eglewCreatePbufferSurface;
<a name="eglew.h-2308"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPIXMAPSURFACEPROC __eglewCreatePixmapSurface;
<a name="eglew.h-2309"></a>EGLEW_FUN_EXPORT PFNEGLCREATEWINDOWSURFACEPROC __eglewCreateWindowSurface;
<a name="eglew.h-2310"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYCONTEXTPROC __eglewDestroyContext;
<a name="eglew.h-2311"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYSURFACEPROC __eglewDestroySurface;
<a name="eglew.h-2312"></a>EGLEW_FUN_EXPORT PFNEGLGETCONFIGATTRIBPROC __eglewGetConfigAttrib;
<a name="eglew.h-2313"></a>EGLEW_FUN_EXPORT PFNEGLGETCONFIGSPROC __eglewGetConfigs;
<a name="eglew.h-2314"></a>EGLEW_FUN_EXPORT PFNEGLGETCURRENTDISPLAYPROC __eglewGetCurrentDisplay;
<a name="eglew.h-2315"></a>EGLEW_FUN_EXPORT PFNEGLGETCURRENTSURFACEPROC __eglewGetCurrentSurface;
<a name="eglew.h-2316"></a>EGLEW_FUN_EXPORT PFNEGLGETDISPLAYPROC __eglewGetDisplay;
<a name="eglew.h-2317"></a>EGLEW_FUN_EXPORT PFNEGLGETERRORPROC __eglewGetError;
<a name="eglew.h-2318"></a>EGLEW_FUN_EXPORT PFNEGLINITIALIZEPROC __eglewInitialize;
<a name="eglew.h-2319"></a>EGLEW_FUN_EXPORT PFNEGLMAKECURRENTPROC __eglewMakeCurrent;
<a name="eglew.h-2320"></a>EGLEW_FUN_EXPORT PFNEGLQUERYCONTEXTPROC __eglewQueryContext;
<a name="eglew.h-2321"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSTRINGPROC __eglewQueryString;
<a name="eglew.h-2322"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSURFACEPROC __eglewQuerySurface;
<a name="eglew.h-2323"></a>EGLEW_FUN_EXPORT PFNEGLSWAPBUFFERSPROC __eglewSwapBuffers;
<a name="eglew.h-2324"></a>EGLEW_FUN_EXPORT PFNEGLTERMINATEPROC __eglewTerminate;
<a name="eglew.h-2325"></a>EGLEW_FUN_EXPORT PFNEGLWAITGLPROC __eglewWaitGL;
<a name="eglew.h-2326"></a>EGLEW_FUN_EXPORT PFNEGLWAITNATIVEPROC __eglewWaitNative;
<a name="eglew.h-2327"></a>
<a name="eglew.h-2328"></a>EGLEW_FUN_EXPORT PFNEGLBINDTEXIMAGEPROC __eglewBindTexImage;
<a name="eglew.h-2329"></a>EGLEW_FUN_EXPORT PFNEGLRELEASETEXIMAGEPROC __eglewReleaseTexImage;
<a name="eglew.h-2330"></a>EGLEW_FUN_EXPORT PFNEGLSURFACEATTRIBPROC __eglewSurfaceAttrib;
<a name="eglew.h-2331"></a>EGLEW_FUN_EXPORT PFNEGLSWAPINTERVALPROC __eglewSwapInterval;
<a name="eglew.h-2332"></a>
<a name="eglew.h-2333"></a>EGLEW_FUN_EXPORT PFNEGLBINDAPIPROC __eglewBindAPI;
<a name="eglew.h-2334"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPBUFFERFROMCLIENTBUFFERPROC __eglewCreatePbufferFromClientBuffer;
<a name="eglew.h-2335"></a>EGLEW_FUN_EXPORT PFNEGLQUERYAPIPROC __eglewQueryAPI;
<a name="eglew.h-2336"></a>EGLEW_FUN_EXPORT PFNEGLRELEASETHREADPROC __eglewReleaseThread;
<a name="eglew.h-2337"></a>EGLEW_FUN_EXPORT PFNEGLWAITCLIENTPROC __eglewWaitClient;
<a name="eglew.h-2338"></a>
<a name="eglew.h-2339"></a>EGLEW_FUN_EXPORT PFNEGLGETCURRENTCONTEXTPROC __eglewGetCurrentContext;
<a name="eglew.h-2340"></a>
<a name="eglew.h-2341"></a>EGLEW_FUN_EXPORT PFNEGLCLIENTWAITSYNCPROC __eglewClientWaitSync;
<a name="eglew.h-2342"></a>EGLEW_FUN_EXPORT PFNEGLCREATEIMAGEPROC __eglewCreateImage;
<a name="eglew.h-2343"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPLATFORMPIXMAPSURFACEPROC __eglewCreatePlatformPixmapSurface;
<a name="eglew.h-2344"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPLATFORMWINDOWSURFACEPROC __eglewCreatePlatformWindowSurface;
<a name="eglew.h-2345"></a>EGLEW_FUN_EXPORT PFNEGLCREATESYNCPROC __eglewCreateSync;
<a name="eglew.h-2346"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYIMAGEPROC __eglewDestroyImage;
<a name="eglew.h-2347"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYSYNCPROC __eglewDestroySync;
<a name="eglew.h-2348"></a>EGLEW_FUN_EXPORT PFNEGLGETPLATFORMDISPLAYPROC __eglewGetPlatformDisplay;
<a name="eglew.h-2349"></a>EGLEW_FUN_EXPORT PFNEGLGETSYNCATTRIBPROC __eglewGetSyncAttrib;
<a name="eglew.h-2350"></a>EGLEW_FUN_EXPORT PFNEGLWAITSYNCPROC __eglewWaitSync;
<a name="eglew.h-2351"></a>
<a name="eglew.h-2352"></a>EGLEW_FUN_EXPORT PFNEGLSETBLOBCACHEFUNCSANDROIDPROC __eglewSetBlobCacheFuncsANDROID;
<a name="eglew.h-2353"></a>
<a name="eglew.h-2354"></a>EGLEW_FUN_EXPORT PFNEGLCREATENATIVECLIENTBUFFERANDROIDPROC __eglewCreateNativeClientBufferANDROID;
<a name="eglew.h-2355"></a>
<a name="eglew.h-2356"></a>EGLEW_FUN_EXPORT PFNEGLDUPNATIVEFENCEFDANDROIDPROC __eglewDupNativeFenceFDANDROID;
<a name="eglew.h-2357"></a>
<a name="eglew.h-2358"></a>EGLEW_FUN_EXPORT PFNEGLPRESENTATIONTIMEANDROIDPROC __eglewPresentationTimeANDROID;
<a name="eglew.h-2359"></a>
<a name="eglew.h-2360"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSURFACEPOINTERANGLEPROC __eglewQuerySurfacePointerANGLE;
<a name="eglew.h-2361"></a>
<a name="eglew.h-2362"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDEVICESEXTPROC __eglewQueryDevicesEXT;
<a name="eglew.h-2363"></a>
<a name="eglew.h-2364"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDEVICEATTRIBEXTPROC __eglewQueryDeviceAttribEXT;
<a name="eglew.h-2365"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDEVICESTRINGEXTPROC __eglewQueryDeviceStringEXT;
<a name="eglew.h-2366"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDISPLAYATTRIBEXTPROC __eglewQueryDisplayAttribEXT;
<a name="eglew.h-2367"></a>
<a name="eglew.h-2368"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDMABUFFORMATSEXTPROC __eglewQueryDmaBufFormatsEXT;
<a name="eglew.h-2369"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDMABUFMODIFIERSEXTPROC __eglewQueryDmaBufModifiersEXT;
<a name="eglew.h-2370"></a>
<a name="eglew.h-2371"></a>EGLEW_FUN_EXPORT PFNEGLGETOUTPUTLAYERSEXTPROC __eglewGetOutputLayersEXT;
<a name="eglew.h-2372"></a>EGLEW_FUN_EXPORT PFNEGLGETOUTPUTPORTSEXTPROC __eglewGetOutputPortsEXT;
<a name="eglew.h-2373"></a>EGLEW_FUN_EXPORT PFNEGLOUTPUTLAYERATTRIBEXTPROC __eglewOutputLayerAttribEXT;
<a name="eglew.h-2374"></a>EGLEW_FUN_EXPORT PFNEGLOUTPUTPORTATTRIBEXTPROC __eglewOutputPortAttribEXT;
<a name="eglew.h-2375"></a>EGLEW_FUN_EXPORT PFNEGLQUERYOUTPUTLAYERATTRIBEXTPROC __eglewQueryOutputLayerAttribEXT;
<a name="eglew.h-2376"></a>EGLEW_FUN_EXPORT PFNEGLQUERYOUTPUTLAYERSTRINGEXTPROC __eglewQueryOutputLayerStringEXT;
<a name="eglew.h-2377"></a>EGLEW_FUN_EXPORT PFNEGLQUERYOUTPUTPORTATTRIBEXTPROC __eglewQueryOutputPortAttribEXT;
<a name="eglew.h-2378"></a>EGLEW_FUN_EXPORT PFNEGLQUERYOUTPUTPORTSTRINGEXTPROC __eglewQueryOutputPortStringEXT;
<a name="eglew.h-2379"></a>
<a name="eglew.h-2380"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPLATFORMPIXMAPSURFACEEXTPROC __eglewCreatePlatformPixmapSurfaceEXT;
<a name="eglew.h-2381"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC __eglewCreatePlatformWindowSurfaceEXT;
<a name="eglew.h-2382"></a>EGLEW_FUN_EXPORT PFNEGLGETPLATFORMDISPLAYEXTPROC __eglewGetPlatformDisplayEXT;
<a name="eglew.h-2383"></a>
<a name="eglew.h-2384"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMCONSUMEROUTPUTEXTPROC __eglewStreamConsumerOutputEXT;
<a name="eglew.h-2385"></a>
<a name="eglew.h-2386"></a>EGLEW_FUN_EXPORT PFNEGLSWAPBUFFERSWITHDAMAGEEXTPROC __eglewSwapBuffersWithDamageEXT;
<a name="eglew.h-2387"></a>
<a name="eglew.h-2388"></a>EGLEW_FUN_EXPORT PFNEGLCREATEPIXMAPSURFACEHIPROC __eglewCreatePixmapSurfaceHI;
<a name="eglew.h-2389"></a>
<a name="eglew.h-2390"></a>EGLEW_FUN_EXPORT PFNEGLCREATESYNC64KHRPROC __eglewCreateSync64KHR;
<a name="eglew.h-2391"></a>
<a name="eglew.h-2392"></a>EGLEW_FUN_EXPORT PFNEGLDEBUGMESSAGECONTROLKHRPROC __eglewDebugMessageControlKHR;
<a name="eglew.h-2393"></a>EGLEW_FUN_EXPORT PFNEGLLABELOBJECTKHRPROC __eglewLabelObjectKHR;
<a name="eglew.h-2394"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDEBUGKHRPROC __eglewQueryDebugKHR;
<a name="eglew.h-2395"></a>
<a name="eglew.h-2396"></a>EGLEW_FUN_EXPORT PFNEGLCREATEIMAGEKHRPROC __eglewCreateImageKHR;
<a name="eglew.h-2397"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYIMAGEKHRPROC __eglewDestroyImageKHR;
<a name="eglew.h-2398"></a>
<a name="eglew.h-2399"></a>EGLEW_FUN_EXPORT PFNEGLLOCKSURFACEKHRPROC __eglewLockSurfaceKHR;
<a name="eglew.h-2400"></a>EGLEW_FUN_EXPORT PFNEGLUNLOCKSURFACEKHRPROC __eglewUnlockSurfaceKHR;
<a name="eglew.h-2401"></a>
<a name="eglew.h-2402"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSURFACE64KHRPROC __eglewQuerySurface64KHR;
<a name="eglew.h-2403"></a>
<a name="eglew.h-2404"></a>EGLEW_FUN_EXPORT PFNEGLSETDAMAGEREGIONKHRPROC __eglewSetDamageRegionKHR;
<a name="eglew.h-2405"></a>
<a name="eglew.h-2406"></a>EGLEW_FUN_EXPORT PFNEGLCLIENTWAITSYNCKHRPROC __eglewClientWaitSyncKHR;
<a name="eglew.h-2407"></a>EGLEW_FUN_EXPORT PFNEGLCREATESYNCKHRPROC __eglewCreateSyncKHR;
<a name="eglew.h-2408"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYSYNCKHRPROC __eglewDestroySyncKHR;
<a name="eglew.h-2409"></a>EGLEW_FUN_EXPORT PFNEGLGETSYNCATTRIBKHRPROC __eglewGetSyncAttribKHR;
<a name="eglew.h-2410"></a>EGLEW_FUN_EXPORT PFNEGLSIGNALSYNCKHRPROC __eglewSignalSyncKHR;
<a name="eglew.h-2411"></a>
<a name="eglew.h-2412"></a>EGLEW_FUN_EXPORT PFNEGLCREATESTREAMKHRPROC __eglewCreateStreamKHR;
<a name="eglew.h-2413"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYSTREAMKHRPROC __eglewDestroyStreamKHR;
<a name="eglew.h-2414"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSTREAMKHRPROC __eglewQueryStreamKHR;
<a name="eglew.h-2415"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSTREAMU64KHRPROC __eglewQueryStreamu64KHR;
<a name="eglew.h-2416"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMATTRIBKHRPROC __eglewStreamAttribKHR;
<a name="eglew.h-2417"></a>
<a name="eglew.h-2418"></a>EGLEW_FUN_EXPORT PFNEGLCREATESTREAMATTRIBKHRPROC __eglewCreateStreamAttribKHR;
<a name="eglew.h-2419"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSTREAMATTRIBKHRPROC __eglewQueryStreamAttribKHR;
<a name="eglew.h-2420"></a>EGLEW_FUN_EXPORT PFNEGLSETSTREAMATTRIBKHRPROC __eglewSetStreamAttribKHR;
<a name="eglew.h-2421"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMCONSUMERACQUIREATTRIBKHRPROC __eglewStreamConsumerAcquireAttribKHR;
<a name="eglew.h-2422"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMCONSUMERRELEASEATTRIBKHRPROC __eglewStreamConsumerReleaseAttribKHR;
<a name="eglew.h-2423"></a>
<a name="eglew.h-2424"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMCONSUMERACQUIREKHRPROC __eglewStreamConsumerAcquireKHR;
<a name="eglew.h-2425"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMCONSUMERGLTEXTUREEXTERNALKHRPROC __eglewStreamConsumerGLTextureExternalKHR;
<a name="eglew.h-2426"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMCONSUMERRELEASEKHRPROC __eglewStreamConsumerReleaseKHR;
<a name="eglew.h-2427"></a>
<a name="eglew.h-2428"></a>EGLEW_FUN_EXPORT PFNEGLCREATESTREAMFROMFILEDESCRIPTORKHRPROC __eglewCreateStreamFromFileDescriptorKHR;
<a name="eglew.h-2429"></a>EGLEW_FUN_EXPORT PFNEGLGETSTREAMFILEDESCRIPTORKHRPROC __eglewGetStreamFileDescriptorKHR;
<a name="eglew.h-2430"></a>
<a name="eglew.h-2431"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSTREAMTIMEKHRPROC __eglewQueryStreamTimeKHR;
<a name="eglew.h-2432"></a>
<a name="eglew.h-2433"></a>EGLEW_FUN_EXPORT PFNEGLCREATESTREAMPRODUCERSURFACEKHRPROC __eglewCreateStreamProducerSurfaceKHR;
<a name="eglew.h-2434"></a>
<a name="eglew.h-2435"></a>EGLEW_FUN_EXPORT PFNEGLSWAPBUFFERSWITHDAMAGEKHRPROC __eglewSwapBuffersWithDamageKHR;
<a name="eglew.h-2436"></a>
<a name="eglew.h-2437"></a>EGLEW_FUN_EXPORT PFNEGLWAITSYNCKHRPROC __eglewWaitSyncKHR;
<a name="eglew.h-2438"></a>
<a name="eglew.h-2439"></a>EGLEW_FUN_EXPORT PFNEGLCREATEDRMIMAGEMESAPROC __eglewCreateDRMImageMESA;
<a name="eglew.h-2440"></a>EGLEW_FUN_EXPORT PFNEGLEXPORTDRMIMAGEMESAPROC __eglewExportDRMImageMESA;
<a name="eglew.h-2441"></a>
<a name="eglew.h-2442"></a>EGLEW_FUN_EXPORT PFNEGLEXPORTDMABUFIMAGEMESAPROC __eglewExportDMABUFImageMESA;
<a name="eglew.h-2443"></a>EGLEW_FUN_EXPORT PFNEGLEXPORTDMABUFIMAGEQUERYMESAPROC __eglewExportDMABUFImageQueryMESA;
<a name="eglew.h-2444"></a>
<a name="eglew.h-2445"></a>EGLEW_FUN_EXPORT PFNEGLSWAPBUFFERSREGIONNOKPROC __eglewSwapBuffersRegionNOK;
<a name="eglew.h-2446"></a>
<a name="eglew.h-2447"></a>EGLEW_FUN_EXPORT PFNEGLSWAPBUFFERSREGION2NOKPROC __eglewSwapBuffersRegion2NOK;
<a name="eglew.h-2448"></a>
<a name="eglew.h-2449"></a>EGLEW_FUN_EXPORT PFNEGLQUERYNATIVEDISPLAYNVPROC __eglewQueryNativeDisplayNV;
<a name="eglew.h-2450"></a>EGLEW_FUN_EXPORT PFNEGLQUERYNATIVEPIXMAPNVPROC __eglewQueryNativePixmapNV;
<a name="eglew.h-2451"></a>EGLEW_FUN_EXPORT PFNEGLQUERYNATIVEWINDOWNVPROC __eglewQueryNativeWindowNV;
<a name="eglew.h-2452"></a>
<a name="eglew.h-2453"></a>EGLEW_FUN_EXPORT PFNEGLPOSTSUBBUFFERNVPROC __eglewPostSubBufferNV;
<a name="eglew.h-2454"></a>
<a name="eglew.h-2455"></a>EGLEW_FUN_EXPORT PFNEGLSTREAMCONSUMERGLTEXTUREEXTERNALATTRIBSNVPROC __eglewStreamConsumerGLTextureExternalAttribsNV;
<a name="eglew.h-2456"></a>
<a name="eglew.h-2457"></a>EGLEW_FUN_EXPORT PFNEGLQUERYDISPLAYATTRIBNVPROC __eglewQueryDisplayAttribNV;
<a name="eglew.h-2458"></a>EGLEW_FUN_EXPORT PFNEGLQUERYSTREAMMETADATANVPROC __eglewQueryStreamMetadataNV;
<a name="eglew.h-2459"></a>EGLEW_FUN_EXPORT PFNEGLSETSTREAMMETADATANVPROC __eglewSetStreamMetadataNV;
<a name="eglew.h-2460"></a>
<a name="eglew.h-2461"></a>EGLEW_FUN_EXPORT PFNEGLRESETSTREAMNVPROC __eglewResetStreamNV;
<a name="eglew.h-2462"></a>
<a name="eglew.h-2463"></a>EGLEW_FUN_EXPORT PFNEGLCREATESTREAMSYNCNVPROC __eglewCreateStreamSyncNV;
<a name="eglew.h-2464"></a>
<a name="eglew.h-2465"></a>EGLEW_FUN_EXPORT PFNEGLCLIENTWAITSYNCNVPROC __eglewClientWaitSyncNV;
<a name="eglew.h-2466"></a>EGLEW_FUN_EXPORT PFNEGLCREATEFENCESYNCNVPROC __eglewCreateFenceSyncNV;
<a name="eglew.h-2467"></a>EGLEW_FUN_EXPORT PFNEGLDESTROYSYNCNVPROC __eglewDestroySyncNV;
<a name="eglew.h-2468"></a>EGLEW_FUN_EXPORT PFNEGLFENCENVPROC __eglewFenceNV;
<a name="eglew.h-2469"></a>EGLEW_FUN_EXPORT PFNEGLGETSYNCATTRIBNVPROC __eglewGetSyncAttribNV;
<a name="eglew.h-2470"></a>EGLEW_FUN_EXPORT PFNEGLSIGNALSYNCNVPROC __eglewSignalSyncNV;
<a name="eglew.h-2471"></a>
<a name="eglew.h-2472"></a>EGLEW_FUN_EXPORT PFNEGLGETSYSTEMTIMEFREQUENCYNVPROC __eglewGetSystemTimeFrequencyNV;
<a name="eglew.h-2473"></a>EGLEW_FUN_EXPORT PFNEGLGETSYSTEMTIMENVPROC __eglewGetSystemTimeNV;
<a name="eglew.h-2474"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_VERSION_1_0;
<a name="eglew.h-2475"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_VERSION_1_1;
<a name="eglew.h-2476"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_VERSION_1_2;
<a name="eglew.h-2477"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_VERSION_1_3;
<a name="eglew.h-2478"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_VERSION_1_4;
<a name="eglew.h-2479"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_VERSION_1_5;
<a name="eglew.h-2480"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_blob_cache;
<a name="eglew.h-2481"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_create_native_client_buffer;
<a name="eglew.h-2482"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_framebuffer_target;
<a name="eglew.h-2483"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_front_buffer_auto_refresh;
<a name="eglew.h-2484"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_image_native_buffer;
<a name="eglew.h-2485"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_native_fence_sync;
<a name="eglew.h-2486"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_presentation_time;
<a name="eglew.h-2487"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANDROID_recordable;
<a name="eglew.h-2488"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANGLE_d3d_share_handle_client_buffer;
<a name="eglew.h-2489"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANGLE_device_d3d;
<a name="eglew.h-2490"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANGLE_query_surface_pointer;
<a name="eglew.h-2491"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANGLE_surface_d3d_texture_2d_share_handle;
<a name="eglew.h-2492"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ANGLE_window_fixed_size;
<a name="eglew.h-2493"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ARM_implicit_external_sync;
<a name="eglew.h-2494"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_ARM_pixmap_multisample_discard;
<a name="eglew.h-2495"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_buffer_age;
<a name="eglew.h-2496"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_client_extensions;
<a name="eglew.h-2497"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_create_context_robustness;
<a name="eglew.h-2498"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_device_base;
<a name="eglew.h-2499"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_device_drm;
<a name="eglew.h-2500"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_device_enumeration;
<a name="eglew.h-2501"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_device_openwf;
<a name="eglew.h-2502"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_device_query;
<a name="eglew.h-2503"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_gl_colorspace_bt2020_linear;
<a name="eglew.h-2504"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_gl_colorspace_bt2020_pq;
<a name="eglew.h-2505"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_gl_colorspace_scrgb_linear;
<a name="eglew.h-2506"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_image_dma_buf_import;
<a name="eglew.h-2507"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_image_dma_buf_import_modifiers;
<a name="eglew.h-2508"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_multiview_window;
<a name="eglew.h-2509"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_output_base;
<a name="eglew.h-2510"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_output_drm;
<a name="eglew.h-2511"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_output_openwf;
<a name="eglew.h-2512"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_pixel_format_float;
<a name="eglew.h-2513"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_platform_base;
<a name="eglew.h-2514"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_platform_device;
<a name="eglew.h-2515"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_platform_wayland;
<a name="eglew.h-2516"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_platform_x11;
<a name="eglew.h-2517"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_protected_content;
<a name="eglew.h-2518"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_protected_surface;
<a name="eglew.h-2519"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_stream_consumer_egloutput;
<a name="eglew.h-2520"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_surface_SMPTE2086_metadata;
<a name="eglew.h-2521"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_swap_buffers_with_damage;
<a name="eglew.h-2522"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_EXT_yuv_surface;
<a name="eglew.h-2523"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_HI_clientpixmap;
<a name="eglew.h-2524"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_HI_colorformats;
<a name="eglew.h-2525"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_IMG_context_priority;
<a name="eglew.h-2526"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_IMG_image_plane_attribs;
<a name="eglew.h-2527"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_cl_event;
<a name="eglew.h-2528"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_cl_event2;
<a name="eglew.h-2529"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_client_get_all_proc_addresses;
<a name="eglew.h-2530"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_config_attribs;
<a name="eglew.h-2531"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_context_flush_control;
<a name="eglew.h-2532"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_create_context;
<a name="eglew.h-2533"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_create_context_no_error;
<a name="eglew.h-2534"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_debug;
<a name="eglew.h-2535"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_fence_sync;
<a name="eglew.h-2536"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_get_all_proc_addresses;
<a name="eglew.h-2537"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_gl_colorspace;
<a name="eglew.h-2538"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_gl_renderbuffer_image;
<a name="eglew.h-2539"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_gl_texture_2D_image;
<a name="eglew.h-2540"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_gl_texture_3D_image;
<a name="eglew.h-2541"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_gl_texture_cubemap_image;
<a name="eglew.h-2542"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_image;
<a name="eglew.h-2543"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_image_base;
<a name="eglew.h-2544"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_image_pixmap;
<a name="eglew.h-2545"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_lock_surface;
<a name="eglew.h-2546"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_lock_surface2;
<a name="eglew.h-2547"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_lock_surface3;
<a name="eglew.h-2548"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_mutable_render_buffer;
<a name="eglew.h-2549"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_no_config_context;
<a name="eglew.h-2550"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_partial_update;
<a name="eglew.h-2551"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_platform_android;
<a name="eglew.h-2552"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_platform_gbm;
<a name="eglew.h-2553"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_platform_wayland;
<a name="eglew.h-2554"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_platform_x11;
<a name="eglew.h-2555"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_reusable_sync;
<a name="eglew.h-2556"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_stream;
<a name="eglew.h-2557"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_stream_attrib;
<a name="eglew.h-2558"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_stream_consumer_gltexture;
<a name="eglew.h-2559"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_stream_cross_process_fd;
<a name="eglew.h-2560"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_stream_fifo;
<a name="eglew.h-2561"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_stream_producer_aldatalocator;
<a name="eglew.h-2562"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_stream_producer_eglsurface;
<a name="eglew.h-2563"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_surfaceless_context;
<a name="eglew.h-2564"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_swap_buffers_with_damage;
<a name="eglew.h-2565"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_vg_parent_image;
<a name="eglew.h-2566"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_KHR_wait_sync;
<a name="eglew.h-2567"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_MESA_drm_image;
<a name="eglew.h-2568"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_MESA_image_dma_buf_export;
<a name="eglew.h-2569"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_MESA_platform_gbm;
<a name="eglew.h-2570"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_MESA_platform_surfaceless;
<a name="eglew.h-2571"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NOK_swap_region;
<a name="eglew.h-2572"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NOK_swap_region2;
<a name="eglew.h-2573"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NOK_texture_from_pixmap;
<a name="eglew.h-2574"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_3dvision_surface;
<a name="eglew.h-2575"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_coverage_sample;
<a name="eglew.h-2576"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_coverage_sample_resolve;
<a name="eglew.h-2577"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_cuda_event;
<a name="eglew.h-2578"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_depth_nonlinear;
<a name="eglew.h-2579"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_device_cuda;
<a name="eglew.h-2580"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_native_query;
<a name="eglew.h-2581"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_post_convert_rounding;
<a name="eglew.h-2582"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_post_sub_buffer;
<a name="eglew.h-2583"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_robustness_video_memory_purge;
<a name="eglew.h-2584"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_consumer_gltexture_yuv;
<a name="eglew.h-2585"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_cross_display;
<a name="eglew.h-2586"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_cross_object;
<a name="eglew.h-2587"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_cross_partition;
<a name="eglew.h-2588"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_cross_process;
<a name="eglew.h-2589"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_cross_system;
<a name="eglew.h-2590"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_fifo_next;
<a name="eglew.h-2591"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_fifo_synchronous;
<a name="eglew.h-2592"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_frame_limits;
<a name="eglew.h-2593"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_metadata;
<a name="eglew.h-2594"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_remote;
<a name="eglew.h-2595"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_reset;
<a name="eglew.h-2596"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_socket;
<a name="eglew.h-2597"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_socket_inet;
<a name="eglew.h-2598"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_socket_unix;
<a name="eglew.h-2599"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_stream_sync;
<a name="eglew.h-2600"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_sync;
<a name="eglew.h-2601"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_NV_system_time;
<a name="eglew.h-2602"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_TIZEN_image_native_buffer;
<a name="eglew.h-2603"></a>EGLEW_VAR_EXPORT GLboolean __EGLEW_TIZEN_image_native_surface;
<a name="eglew.h-2604"></a>/* ------------------------------------------------------------------------ */
<a name="eglew.h-2605"></a>
<a name="eglew.h-2606"></a>GLEWAPI GLenum GLEWAPIENTRY eglewInit (EGLDisplay display);
<a name="eglew.h-2607"></a>GLEWAPI GLboolean GLEWAPIENTRY eglewIsSupported (const char *name);
<a name="eglew.h-2608"></a>
<a name="eglew.h-2609"></a>#define EGLEW_GET_VAR(x) (*(const GLboolean*)&amp;x)
<a name="eglew.h-2610"></a>#define EGLEW_GET_FUN(x) x
<a name="eglew.h-2611"></a>
<a name="eglew.h-2612"></a>GLEWAPI GLboolean GLEWAPIENTRY eglewGetExtension (const char *name);
<a name="eglew.h-2613"></a>
<a name="eglew.h-2614"></a>#ifdef __cplusplus
<a name="eglew.h-2615"></a>}
<a name="eglew.h-2616"></a>#endif
<a name="eglew.h-2617"></a>
<a name="eglew.h-2618"></a>#endif /* __eglew_h__ */
</pre></div></td></tr></table>

    </div>
  


        </div>
        
      </div>
    </div>
  </div>
  
  <div data-module="source/set-changeset" data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"></div>



  
    
    
    
  
  

  </div>

        
        
        
          
    
    
  
        
      </div>
    </div>
    <div id="code-search-cta"></div>
  </div>

      </div>
    </div>
  
</div>

<div id="adg3-dialog"></div>


  

<div data-module="components/mentions/index">
  
    
    
  
  
    
    
  
  
    
    
  
</div>
<div data-module="components/typeahead/emoji/index">
  
    
    
  
</div>

<div data-module="components/repo-typeahead/index">
  
    
    
  
</div>

    
    
  

    
    
  

    
    
  

    
    
  


  


    
    
  

    
    
  


  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  


  
  
  <aui-inline-dialog
    id="help-menu-dialog"
    data-aui-alignment="bottom right"

    
    data-aui-alignment-static="true"
    data-module="header/help-menu"
    responds-to="toggle"
    aria-hidden="true">

  <div id="help-menu-section">
    <h1 class="help-menu-heading">Help</h1>

    <form id="help-menu-search-form" class="aui" target="_blank" method="get"
        action="https://support.atlassian.com/customer/search">
      <span id="help-menu-search-icon" class="aui-icon aui-icon-large aui-iconfont-search"></span>
      <input id="help-menu-search-form-input" name="q" class="text" type="text" placeholder="Ask a question">
    </form>

    <ul id="help-menu-links">
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationHome"
            href="https://confluence.atlassian.com/x/bgozDQ" target="_blank">
          Online help
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="GitTutorials"
            href="https://www.atlassian.com/git?utm_source=bitbucket&amp;utm_medium=link&amp;utm_campaign=help_dropdown&amp;utm_content=learn_git"
            target="_blank">
          Learn Git
        </a>
      </li>
      <li>
        <a id="keyboard-shortcuts-link"
           href="#">Keyboard shortcuts</a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationTutorials"
            href="https://confluence.atlassian.com/x/Q4sFLQ" target="_blank">
          Bitbucket tutorials
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="SiteStatus"
            href="https://status.bitbucket.org/" target="_blank">
          Site status
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="Home"
            href="https://support.atlassian.com/bitbucket-cloud/" target="_blank">
          Support
        </a>
      </li>
    </ul>
  </div>
</aui-inline-dialog>
  


  <div class="omnibar" data-module="components/omnibar/index">
    <form class="omnibar-form aui"></form>
  </div>
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  





  

  <div class="_mustache-templates">
    
      <script id="branch-checkout-template" type="text/html">
        

<div id="checkout-branch-contents">
  <div class="command-line">
    <p>
      Check out this branch on your local machine to begin working on it.
    </p>
    <input type="text" class="checkout-command" readonly="readonly"
        
           value="git fetch && git checkout [[branchName]]"
        
        >
  </div>
  
    <div class="sourcetree-callout clone-in-sourcetree"
  data-module="components/clone/clone-in-sourcetree"
  data-https-url="https://Yanosik@bitbucket.org/Yanosik/zapart.git"
  data-ssh-url="git@bitbucket.org:Yanosik/zapart.git">

  <div>
    <button class="aui-button aui-button-primary">
      
        Check out in Sourcetree
      
    </button>
  </div>

  <p class="windows-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_win" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Windows.
    
  </p>
  <p class="mac-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_mac" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Mac.
    
  </p>
</div>
  
</div>

      </script>
    
      <script id="branch-dialog-template" type="text/html">
        

<div class="tabbed-filter-widget branch-dialog">
  <div class="tabbed-filter">
    <input placeholder="Filter branches" class="filter-box" autosave="branch-dropdown-29632644" type="text">
    [[^ignoreTags]]
      <div class="aui-tabs horizontal-tabs aui-tabs-disabled filter-tabs">
        <ul class="tabs-menu">
          <li class="menu-item active-tab"><a href="#branches">Branches</a></li>
          <li class="menu-item"><a href="#tags">Tags</a></li>
        </ul>
      </div>
    [[/ignoreTags]]
  </div>
  
    <div class="tab-pane active-pane" id="branches" data-filter-placeholder="Filter branches">
      <ol class="filter-list">
        <li class="empty-msg">No matching branches</li>
        [[#branches]]
          
            [[#hasMultipleHeads]]
              [[#heads]]
                <li class="comprev filter-item">
                  <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/eglew.h?at=[[safeName]]"
                     title="[[name]]">
                    [[name]] ([[shortChangeset]])
                  </a>
                </li>
              [[/heads]]
            [[/hasMultipleHeads]]
            [[^hasMultipleHeads]]
              <li class="comprev filter-item">
                <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/eglew.h?at=[[safeName]]" title="[[name]]">
                  [[name]]
                </a>
              </li>
            [[/hasMultipleHeads]]
          
        [[/branches]]
      </ol>
    </div>
    <div class="tab-pane" id="tags" data-filter-placeholder="Filter tags">
      <ol class="filter-list">
        <li class="empty-msg">No matching tags</li>
        [[#tags]]
          <li class="comprev filter-item">
            <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/eglew.h?at=[[safeName]]" title="[[name]]">
              [[name]]
            </a>
          </li>
        [[/tags]]
      </ol>
    </div>
  
</div>

      </script>
    
      <script id="image-upload-template" type="text/html">
        

<form id="upload-image" method="POST"
    
      action="/xhr/Yanosik/zapart/image-upload/"
    >
  <input type='hidden' name='csrfmiddlewaretoken' value='6T1lmoXrypiVbmSLk0Ji8BQmn7lyyqWrecXVE8nKmkbG2KZAWJsp4qLZpajaIRSa' />

  <div class="drop-target">
    <p class="centered">Drag image here</p>
  </div>

  
  <div>
    <button class="aui-button click-target">Select an image</button>
    <input name="file" type="file" class="hidden file-target"
           accept="image/jpeg, image/gif, image/png" />
    <input type="submit" class="hidden">
  </div>
</form>


      </script>
    
      <script id="mention-result" type="text/html">
        
<span class="mention-result">
  <span class="aui-avatar aui-avatar-small mention-result--avatar">
    <span class="aui-avatar-inner">
      <img src="[[avatar_url]]">
    </span>
  </span>
  [[#display_name]]
    <span class="display-name mention-result--display-name">[[&display_name]]</span> <small class="username mention-result--username">[[&username]]</small>
  [[/display_name]]
  [[^display_name]]
    <span class="username mention-result--username">[[&username]]</span>
  [[/display_name]]
  [[#is_teammate]][[^is_team]]
    <span class="aui-lozenge aui-lozenge-complete aui-lozenge-subtle mention-result--lozenge">teammate</span>
  [[/is_team]][[/is_teammate]]
</span>
      </script>
    
      <script id="mention-call-to-action" type="text/html">
        
[[^query]]
<li class="bb-typeahead-item">Begin typing to search for a user</li>
[[/query]]
[[#query]]
<li class="bb-typeahead-item">Continue typing to search for a user</li>
[[/query]]

      </script>
    
      <script id="mention-no-results" type="text/html">
        
[[^searching]]
<li class="bb-typeahead-item">Found no matching users for <em>[[query]]</em>.</li>
[[/searching]]
[[#searching]]
<li class="bb-typeahead-item bb-typeahead-searching">Searching for <em>[[query]]</em>.</li>
[[/searching]]

      </script>
    
      <script id="emoji-result" type="text/html">
        
<span class="emoji-result">
  <span class="emoji-result--avatar">
    <img class="emoji" src="[[src]]">
  </span>
  <span class="name emoji-result--name">[[&name]]</span>
</span>

      </script>
    
      <script id="repo-typeahead-result" type="text/html">
        <span class="aui-avatar aui-avatar-project aui-avatar-xsmall">
  <span class="aui-avatar-inner">
    <img src="[[avatar]]">
  </span>
</span>
<span class="owner">[[&owner]]</span>/<span class="slug">[[&slug]]</span>

      </script>
    
      <script id="share-form-template" type="text/html">
        

<div class="error aui-message hidden">
  <span class="aui-icon icon-error"></span>
  <div class="message"></div>
</div>
<form class="aui">
  <table class="widget bb-list aui">
    <thead>
    <tr class="assistive">
      <th class="user">User</th>
      <th class="role">Role</th>
      <th class="actions">Actions</th>
    </tr>
    </thead>
    <tbody>
      <tr class="form">
        <td colspan="2">
          <input type="text" class="text bb-user-typeahead user-or-email"
                 placeholder="Username or email address"
                 autocomplete="off"
                 data-bb-typeahead-focus="false"
                 [[#disabled]]disabled[[/disabled]]>
        </td>
        <td class="actions">
          <button type="submit" class="aui-button aui-button-light" disabled>Add</button>
        </td>
      </tr>
    </tbody>
  </table>
</form>

      </script>
    
      <script id="share-detail-template" type="text/html">
        

[[#username]]
<td class="user
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[displayName]]">
    <a href="/[[username]]/" class="user">
      <span class="aui-avatar aui-avatar-xsmall">
        <span class="aui-avatar-inner">
          <img src="[[avatar]]">
        </span>
      </span>
      <span>[[displayName]]</span>
    </a>
  </div>
</td>
[[/username]]
[[^username]]
<td class="email
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[email]]">
    <span class="aui-icon aui-icon-small aui-iconfont-email"></span>
    [[email]]
  </div>
</td>
[[/username]]
<td class="role
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    [[#group]]
      [[#hasCustomGroups]]
        <select class="group [[#noGroupChoices]]hidden[[/noGroupChoices]]">
          [[#groups]]
            <option value="[[slug]]"
                [[#isSelected]]selected[[/isSelected]]>
              [[name]]
            </option>
          [[/groups]]
        </select>
      [[/hasCustomGroups]]
      [[^hasCustomGroups]]
      <label>
        <input type="checkbox" class="admin"
            [[#isAdmin]]checked[[/isAdmin]]>
        Administrator
      </label>
      [[/hasCustomGroups]]
    [[/group]]
    [[^group]]
      <ul>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^read]]aui-lozenge-subtle[[/read]]"
            data-permission="read">
          read
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^write]]aui-lozenge-subtle[[/write]]"
            data-permission="write">
          write
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^admin]]aui-lozenge-subtle[[/admin]]"
            data-permission="admin">
          admin
        </li>
      </ul>
    [[/group]]
  </div>
</td>
<td class="actions
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    <a href="#" class="delete">
      <span class="aui-icon aui-icon-small aui-iconfont-remove">Delete</span>
    </a>
  </div>
</td>

      </script>
    
      <script id="share-team-template" type="text/html">
        

<div class="clearfix">
  <span class="team-avatar-container">
    <span class="aui-avatar aui-avatar-medium">
      <span class="aui-avatar-inner">
        <img src="[[avatar]]">
      </span>
    </span>
  </span>
  <span class="team-name-container">
    [[display_name]]
  </span>
</div>
<p class="helptext">
  
    Existing users are granted access to this team immediately.
    New users will be sent an invitation.
  
</p>
<div class="share-form"></div>

      </script>
    
      <script id="scope-list-template" type="text/html">
        <ul class="scope-list">
  [[#scopes]]
    <li class="scope-list--item">
      <span class="scope-list--icon aui-icon aui-icon-small [[icon]]"></span>
      <span class="scope-list--description">[[description]]</span>
    </li>
  [[/scopes]]
</ul>

      </script>
    
      <script id="source-changeset" type="text/html">
        

<a href="/Yanosik/zapart/src/[[raw_node]]/[[path]]?at=master"
    class="[[#selected]]highlight[[/selected]]"
    data-hash="[[node]]">
  [[#author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="[[author.avatar]]">
      </span>
    </span>
    <span class="author" title="[[raw_author]]">[[author.display_name]]</span>
  [[/author.username]]
  [[^author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg">
      </span>
    </span>
    <span class="author unmapped" title="[[raw_author]]">[[author]]</span>
  [[/author.username]]
  <time datetime="[[utctimestamp]]" data-title="true">[[utctimestamp]]</time>
  <span class="message">[[message]]</span>
</a>

      </script>
    
      <script id="embed-template" type="text/html">
        

<form class="aui inline-dialog-embed-dialog">
  <label for="embed-code-[[dialogId]]">Embed this source in another page:</label>
  <input type="text" readonly="true" value="&lt;script src=&quot;[[url]]&quot;&gt;&lt;/script&gt;" id="embed-code-[[dialogId]]" class="embed-code">
</form>

      </script>
    
      <script id="edit-form-template" type="text/html">
        


<form class="bb-content-container online-edit-form aui"
      data-repository="[[owner]]/[[slug]]"
      data-destination-repository="[[destinationOwner]]/[[destinationSlug]]"
      data-local-id="[[localID]]"
      [[#isWriter]]data-is-writer="true"[[/isWriter]]
      [[#hasPushAccess]]data-has-push-access="true"[[/hasPushAccess]]
      [[#isPullRequest]]data-is-pull-request="true"[[/isPullRequest]]
      [[#hideCreatePullRequest]]data-hide-create-pull-request="true"[[/hideCreatePullRequest]]
      data-hash="[[hash]]"
      data-branch="[[branch]]"
      data-path="[[path]]"
      data-is-create="[[isCreate]]"
      data-preview-url="/xhr/[[owner]]/[[slug]]/preview/[[hash]]/[[encodedPath]]"
      data-preview-error="We had trouble generating your preview."
      data-unsaved-changes-error="Your changes will be lost. Are you sure you want to leave?">
  <div class="bb-content-container-header">
    <div class="bb-content-container-header-primary">
      <span class="bb-content-container-heading">
        [[#isCreate]]
          [[#branch]]
            
              Creating <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
            
          [[/branch]]
          [[^branch]]
            [[#path]]
              
                Creating <span class="edit-path">[[path]]</span>
              
            [[/path]]
            [[^path]]
              
                Creating <span class="edit-path">unnamed file</span>
              
            [[/path]]
          [[/branch]]
        [[/isCreate]]
        [[^isCreate]]
          
            Editing <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
          
        [[/isCreate]]
      </span>
    </div>
    <div class="bb-content-container-header-secondary">
      <div class="hunk-nav aui-buttons">
        <button class="prev-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Previous change">
          <span class="aui-icon aui-icon-small aui-iconfont-up">Previous change</span>
        </button>
        <button class="next-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Next change">
          <span class="aui-icon aui-icon-small aui-iconfont-down">Next change</span>
        </button>
      </div>
    </div>
  </div>
  <div class="bb-content-container-body has-header has-footer file-editor">
    <textarea id="id_source"></textarea>
  </div>
  <div class="preview-pane"></div>
  <div class="bb-content-container-footer">
    <div class="bb-content-container-footer-primary">
      <div id="syntax-mode" class="bb-content-container-item field">
        <label for="id_syntax-mode" class="online-edit-form--label">Syntax mode:</label>
        <select id="id_syntax-mode">
          [[#syntaxes]]
            <option value="[[#mime]][[mime]][[/mime]][[^mime]][[mode]][[/mime]]">[[name]]</option>
          [[/syntaxes]]
        </select>
      </div>
      <div id="indent-mode" class="bb-content-container-item field">
        <label for="id_indent-mode" class="online-edit-form--label">Indent mode:</label>
        <select id="id_indent-mode">
          <option value="tabs">Tabs</option>
          <option value="spaces">Spaces</option>
        </select>
      </div>
      <div id="indent-size" class="bb-content-container-item field">
        <label for="id_indent-size" class="online-edit-form--label">Indent size:</label>
        <select id="id_indent-size">
          <option value="2">2</option>
          <option value="4">4</option>
          <option value="8">8</option>
        </select>
      </div>
      <div id="wrap-mode" class="bb-content-container-item field">
        <label for="id_wrap-mode" class="online-edit-form--label">Line wrap:</label>
        <select id="id_wrap-mode">
          <option value="">Off</option>
          <option value="soft">On</option>
        </select>
      </div>
    </div>
    <div class="bb-content-container-footer-secondary">
      [[^isCreate]]
        <button class="preview-button aui-button aui-button-light"
                disabled="disabled" aria-disabled="true"
                data-preview-label="View diff"
                data-edit-label="Edit file">View diff</button>
      [[/isCreate]]
      <button class="save-button aui-button aui-button-primary"
              disabled="disabled" aria-disabled="true">Commit</button>
      [[^isCreate]]
        <a class="aui-button aui-button-link cancel-link" href="#">Cancel</a>
      [[/isCreate]]
    </div>
  </div>
</form>

      </script>
    
      <script id="commit-form-template" type="text/html">
        

<form class="aui commit-form"
      data-title="Commit changes"
      [[#isDelete]]
        data-default-message="[[filename]] deleted online with Bitbucket"
      [[/isDelete]]
      [[#isCreate]]
        data-default-message="[[filename]] created online with Bitbucket"
      [[/isCreate]]
      [[^isDelete]]
        [[^isCreate]]
          data-default-message="[[filename]] edited online with Bitbucket"
        [[/isCreate]]
      [[/isDelete]]
      data-fork-error="We had trouble creating your fork."
      data-commit-error="We had trouble committing your changes."
      data-pull-request-error="We had trouble creating your pull request."
      data-update-error="We had trouble updating your pull request."
      data-branch-conflict-error="A branch with that name already exists."
      data-forking-message="Forking repository"
      data-committing-message="Committing changes"
      data-merging-message="Branching and merging changes"
      data-creating-pr-message="Creating pull request"
      data-updating-pr-message="Updating pull request"
      data-cta-label="Commit"
      data-cancel-label="Cancel">
  [[#isDelete]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <span class="message">
        
          Committing this change will delete [[filename]] from your repository.
        
      </span>
    </div>
  [[/isDelete]]
  <div class="aui-message error hidden">
    <span class="aui-icon icon-error"></span>
    <span class="message"></span>
  </div>
  [[^isWriter]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <p class="title">
        
          You don't have write access to this repository.
        
      </p>
      <span class="message">
        
          We'll create a fork for your changes and submit a
          pull request back to this repository.
        
      </span>
    </div>
  [[/isWriter]]
  [[#isRename]]
    <div class="field-group">
      <label for="id_path">New path</label>
      <input type="text" id="id_path" class="text" value="[[path]]"/>
    </div>
  [[/isRename]]
  <div class="field-group">
    <label for="id_message">Commit message</label>
    <textarea id="id_message" class="long-field textarea"></textarea>
  </div>
  [[^isPullRequest]]
    [[#isWriter]]
      <fieldset class="group">
        <div class="checkbox">
          [[#hasPushAccess]]
            [[^hideCreatePullRequest]]
              <input id="id_create-pullrequest" class="checkbox" type="checkbox">
              <label for="id_create-pullrequest">Create a pull request for this change</label>
            [[/hideCreatePullRequest]]
          [[/hasPushAccess]]
          [[^hasPushAccess]]
            <input id="id_create-pullrequest" class="checkbox" type="checkbox" checked="checked" aria-disabled="true" disabled="true">
            <label for="id_create-pullrequest" title="Branch restrictions do not allow you to update this branch.">Create a pull request for this change</label>
          [[/hasPushAccess]]
        </div>
      </fieldset>
      <div id="pr-fields">
        <div id="branch-name-group" class="field-group">
          <label for="id_branch-name">Branch name</label>
          <input type="text" id="id_branch-name" class="text long-field">
        </div>
        

<div class="field-group" id="id_reviewers_group">
  <label for="reviewers">Reviewers</label>

  
  <input id="reviewers" name="reviewers" type="hidden"
          value=""
          data-mention-url="/xhr/mentions/repositories/:dest_username/:dest_slug"
          data-reviewers="[]"
          data-suggested="[]"
          data-locked="[]">

  <div class="error"></div>
  <div class="suggested-reviewers"></div>

</div>

      </div>
    [[/isWriter]]
  [[/isPullRequest]]
  <button type="submit" id="id_submit">Commit</button>
</form>

      </script>
    
      <script id="merge-message-template" type="text/html">
        Merged [[hash]] into [[branch]]

[[message]]

      </script>
    
      <script id="commit-merge-error-template" type="text/html">
        



  We had trouble merging your changes. We stored them on the <strong>[[branch]]</strong> branch, so feel free to
  <a href="/[[owner]]/[[slug]]/full-commit/[[hash]]/[[path]]?at=[[encodedBranch]]">view them</a> or
  <a href="#" class="create-pull-request-link">create a pull request</a>.


      </script>
    
      <script id="selected-reviewer-template" type="text/html">
        <div class="aui-avatar aui-avatar-xsmall">
  <div class="aui-avatar-inner">
    <img src="[[avatar_url]]">
  </div>
</div>
[[display_name]]

      </script>
    
      <script id="suggested-reviewer-template" type="text/html">
        <button class="aui-button aui-button-link" type="button" tabindex="-1">[[display_name]]</button>

      </script>
    
      <script id="suggested-reviewers-template" type="text/html">
        

<span class="suggested-reviewer-list-label">Recent:</span>
<ul class="suggested-reviewer-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-form-template" type="text/html">
        <div class="omnibar-input-container">
  <input class="omnibar-input" type="text" [[#placeholder]]placeholder="[[placeholder]]"[[/placeholder]]>
</div>
<ul class="omnibar-result-group-list"></ul>

      </script>
    
      <script id="omnibar-blank-slate-template" type="text/html">
        

<div class="omnibar-blank-slate">No results found</div>

      </script>
    
      <script id="omnibar-result-group-list-item-template" type="text/html">
        <div class="omnibar-result-group-header clearfix">
  <h2 class="omnibar-result-group-label" title="[[label]]">[[label]]</h2>
  <span class="omnibar-result-group-context" title="[[context]]">[[context]]</span>
</div>
<ul class="omnibar-result-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-result-list-item-template" type="text/html">
        [[#url]]
  <a href="[[&url]]" class="omnibar-result-label">[[&label]]</a>
[[/url]]
[[^url]]
  <span class="omnibar-result-label">[[&label]]</span>
[[/url]]
[[#context]]
  <span class="omnibar-result-context">[[context]]</span>
[[/context]]

      </script>
    
  </div>




  
  


<script nonce="QT0q4c6MTmDGzd3X">
  window.__initial_state__ = {"global": {"features": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "locale": "en", "geoip_country": "PL", "targetFeatures": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "isFocusedTask": false, "teams": [], "bitbucketActions": [{"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 100, "url": "/repo/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Repository<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repository-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 110, "url": "/account/create-team/", "tab_name": null, "can_display": true, "label": "<strong>Team<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "team-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 120, "url": "/account/projects/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Project<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "project-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 130, "url": "/snippets/new?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Snippet<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "snippet-create-drawer-item", "icon": ""}], "targetUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "isNavigationOpen": true, "path": "/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/eglew.h", "focusedTaskBackButtonUrl": "https://bitbucket.org/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/?at=master", "currentUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}}, "connect": {}, "repository": {"section": {"connectActions": [], "cloneProtocol": "https", "currentRepository": {"scm": "git", "website": "", "name": "Zapart", "language": "", "links": {"clone": [{"href": "https://Yanosik@bitbucket.org/Yanosik/zapart.git", "name": "https"}, {"href": "git@bitbucket.org:Yanosik/zapart.git", "name": "ssh"}], "self": {"href": "https://bitbucket.org/!api/2.0/repositories/Yanosik/zapart"}, "html": {"href": "https://bitbucket.org/Yanosik/zapart"}, "avatar": {"href": "https://bitbucket.org/Yanosik/zapart/avatar/32/"}}, "full_name": "Yanosik/zapart", "owner": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "type": "repository", "slug": "zapart", "is_private": true, "uuid": "{8f59423b-9494-4138-af6f-56e97d727ae4}"}, "menuItems": [{"analytics_label": "repository.overview", "icon_class": "icon-overview", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/overview", "tab_name": "overview", "can_display": true, "label": "Overview", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-overview-link", "icon": "icon-overview"}, {"analytics_label": "repository.source", "icon_class": "icon-source", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/src", "tab_name": "source", "can_display": true, "label": "Source", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-source-link", "icon": "icon-source"}, {"analytics_label": "repository.commits", "icon_class": "icon-commits", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/commits/", "tab_name": "commits", "can_display": true, "label": "Commits", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-commits-link", "icon": "icon-commits"}, {"analytics_label": "repository.branches", "icon_class": "icon-branches", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/", "tab_name": "branches", "can_display": true, "label": "Branches", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-branches-link", "icon": "icon-branches"}, {"analytics_label": "repository.pullrequests", "icon_class": "icon-pull-requests", "badge_label": "0 open pull requests", "weight": 500, "url": "/Yanosik/zapart/pull-requests/", "tab_name": "pullrequests", "can_display": true, "label": "Pull requests", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-pullrequests-link", "icon": "icon-pull-requests"}, {"analytics_label": "site.addon", "icon_class": "aui-iconfont-build", "badge_label": null, "weight": 550, "url": "/Yanosik/zapart/addon/pipelines-installer/home", "tab_name": "repopage-kbbqoq-add-on-link", "can_display": true, "label": "Pipelines", "anchor": true, "analytics_payload": {}, "icon_url": null, "type": "connect_menu_item", "id": "repopage-kbbqoq-add-on-link", "target": "_self"}, {"analytics_label": "repository.downloads", "icon_class": "icon-downloads", "badge_label": null, "weight": 800, "url": "/Yanosik/zapart/downloads/", "tab_name": "downloads", "can_display": true, "label": "Downloads", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-downloads-link", "icon": "icon-downloads"}, {"analytics_label": "user.addon", "icon_class": "aui-iconfont-unfocus", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/addon/bitbucket-trello-addon/trello-board", "tab_name": "repopage-5ABRne-add-on-link", "can_display": true, "label": "Boards", "anchor": false, "analytics_payload": {}, "icon_url": "https://bitbucket-assetroot.s3.amazonaws.com/add-on/icons/35ceae0c-17b1-443c-a6e8-d9de1d7cccdb.svg?Signature=ytKAOdigBlxCGIvP2%2FYLzPrckok%3D&Expires=1511782776&AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&versionId=3oqdrZZjT.HijZ3kHTPsXE6IcSjhCG.P", "type": "connect_menu_item", "id": "repopage-5ABRne-add-on-link", "target": "_self"}, {"analytics_label": "repository.settings", "icon_class": "icon-settings", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/admin", "tab_name": "admin", "can_display": true, "label": "Settings", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-settings-link", "icon": "icon-settings"}], "bitbucketActions": [{"analytics_label": "repository.clone", "icon_class": "icon-clone", "badge_label": null, "weight": 100, "url": "#clone", "tab_name": "clone", "can_display": true, "label": "<strong>Clone<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-clone-button", "icon": "icon-clone"}, {"analytics_label": "repository.create_branch", "icon_class": "icon-create-branch", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/branch", "tab_name": "create-branch", "can_display": true, "label": "Create a <strong>branch<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-branch-link", "icon": "icon-create-branch"}, {"analytics_label": "create_pullrequest", "icon_class": "icon-create-pull-request", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/pull-requests/new", "tab_name": "create-pullreqs", "can_display": true, "label": "Create a <strong>pull request<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-pull-request-link", "icon": "icon-create-pull-request"}, {"analytics_label": "repository.compare", "icon_class": "aui-icon-small aui-iconfont-devtools-compare", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/compare", "tab_name": "compare", "can_display": true, "label": "<strong>Compare<\/strong> branches or tags", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-compare-link", "icon": "aui-icon-small aui-iconfont-devtools-compare"}, {"analytics_label": "repository.fork", "icon_class": "icon-fork", "badge_label": null, "weight": 500, "url": "/Yanosik/zapart/fork", "tab_name": "fork", "can_display": true, "label": "<strong>Fork<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-fork-link", "icon": "icon-fork"}], "activeMenuItem": "source"}}};
  window.__settings__ = {"SOCIAL_AUTH_ATLASSIANID_LOGOUT_URL": "https://id.atlassian.com/logout", "CANON_URL": "https://bitbucket.org", "API_CANON_URL": "https://api.bitbucket.org"};
</script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/jsi18n/en/djangojs.js" nonce="QT0q4c6MTmDGzd3X"></script>

  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/locales/en.js"></script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/vendor.js" nonce="QT0q4c6MTmDGzd3X"></script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/app.js" nonce="QT0q4c6MTmDGzd3X"></script>


<script async src="https://www.google-analytics.com/analytics.js" nonce="QT0q4c6MTmDGzd3X"></script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"licenseKey":"a2cef8c3d3","agent":"","transactionName":"Z11RZxdWW0cEVkYLDV4XdUYLVEFdClsdAAtEWkZQDlJBGgRFQhFMQl1DXFcZQ10AQkFYBFlUVlEXWEJHAA==","applicationID":"1841284","errorBeacon":"bam.nr-data.net","applicationTime":361}</script>
</body>
</html>