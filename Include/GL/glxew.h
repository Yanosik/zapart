<!DOCTYPE html>
<html lang="en">
<head>
  <meta id="bb-bootstrap" data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="5a097ca786e7c03ff34850dd" />
  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta charset="utf-8">
  <title>
  Yanosik / Zapart 
  / source  / Include / GL / glxew.h
 &mdash; Bitbucket
</title>
  <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
  


<meta id="bb-canon-url" name="bb-canon-url" content="https://bitbucket.org">
<meta name="bb-api-canon-url" content="https://api.bitbucket.org">
<meta name="apitoken" content="{&quot;token&quot;: &quot;sAboLq23I0q2_gq_Yw8mC1w4XRcKEaUWbsDQWhDaX-MCJX0dwFFyddLG6qih65ePKfP6_gT1kuxfucOeIKsptIVKyw-FKJhd1hx3k0xcZEgB2B8=&quot;, &quot;expiration&quot;: 1511781314.595188}">

<meta name="bb-commit-hash" content="370568ebc84f">
<meta name="bb-app-node" content="app-172">
<meta name="bb-view-name" content="bitbucket.apps.repo2.views.filebrowse">
<meta name="ignore-whitespace" content="False">
<meta name="tab-size" content="None">
<meta name="locale" content="en">

<meta name="application-name" content="Bitbucket">
<meta name="apple-mobile-web-app-title" content="Bitbucket">


<meta name="theme-color" content="#0049B0">
<meta name="msapplication-TileColor" content="#0052CC">
<meta name="msapplication-TileImage" content="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/mstile-150x150.png">
<link rel="apple-touch-icon" sizes="180x180" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/apple-touch-icon.png">
<link rel="icon" sizes="192x192" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/android-chrome-192x192.png">

<link rel="icon" sizes="16x16 24x24 32x32 64x64" type="image/x-icon" href="/favicon.ico?v=2">
<link rel="mask-icon" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/logos/bitbucket/safari-pinned-tab.svg" color="#0052CC">

<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Bitbucket">

  <meta name="description" content="">
  
  
    
  



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/vendor.css" />
<link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/app.css" />



  <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/css/entry/adg3.css">
  
  <script nonce="pudKVFKPFVGF87mQ">
  window.__sentry__ = {"dsn": "https://ea49358f525d4019945839a3d7a8292a@sentry.io/159509", "release": "370568ebc84f (production)", "tags": {"project": "bitbucket-core"}, "environment": "production"};
</script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/sentry.js" nonce="pudKVFKPFVGF87mQ"></script>
  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/early.js" nonce="pudKVFKPFVGF87mQ"></script>
  
  
    <link href="/Yanosik/zapart/rss?token=a07b0067199174cef445f1e8fa79b35f" rel="alternate nofollow" type="application/rss+xml" title="RSS feed for Zapart" />

</head>
<body class="production adg3  "
    data-static-url="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/"
data-base-url="https://bitbucket.org"
data-no-avatar-image="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg"
data-current-user="{&quot;username&quot;: &quot;Yanosik&quot;, &quot;displayName&quot;: &quot;Jan&quot;, &quot;uuid&quot;: &quot;{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}&quot;, &quot;firstName&quot;: &quot;Jan&quot;, &quot;hasPremium&quot;: false, &quot;lastName&quot;: &quot;&quot;, &quot;avatarUrl&quot;: &quot;https://bitbucket.org/account/Yanosik/avatar/32/?ts=1511780245&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: false, &quot;isKbdShortcutsEnabled&quot;: true, &quot;id&quot;: 10451130, &quot;isAuthenticated&quot;: true}"
data-atlassian-id="{&quot;loginStatusUrl&quot;: &quot;https://id.atlassian.com/profile/rest/profile&quot;}"
data-settings="{&quot;MENTIONS_MIN_QUERY_LENGTH&quot;: 3}"

data-current-repo="{&quot;scm&quot;: &quot;git&quot;, &quot;readOnly&quot;: false, &quot;mainbranch&quot;: {&quot;name&quot;: &quot;master&quot;}, &quot;uuid&quot;: &quot;8f59423b-9494-4138-af6f-56e97d727ae4&quot;, &quot;language&quot;: &quot;&quot;, &quot;owner&quot;: {&quot;username&quot;: &quot;Yanosik&quot;, &quot;uuid&quot;: &quot;8aa0a974-0d88-41f1-b96a-c1ebd6c01af9&quot;, &quot;isTeam&quot;: false}, &quot;fullslug&quot;: &quot;Yanosik/zapart&quot;, &quot;slug&quot;: &quot;zapart&quot;, &quot;id&quot;: 29632644, &quot;pygmentsLanguage&quot;: null}"
data-current-cset="f71033cf7729d442b43ee6f67d340a04470f80ae"





data-browser-monitoring="true"
data-switch-create-pullrequest-commit-status="true"




>
<div id="page">
  
    <div id="adg3-navigation"
  
  
  >
  <nav class="skeleton-nav">
    <div class="skeleton-nav--left">
      <div class="skeleton-nav--left-top">
        <ul class="skeleton-nav--items">
          <li></li>
          <li></li>
          <li></li>
          <li class="skeleton--icon"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
          <li class="skeleton--icon-sub"></li>
        </ul>
      </div>
      <div class="skeleton-nav--left-bottom">
        <div class="skeleton-nav--left-bottom-wrapper">
          <div class="skeleton-nav--item-help"></div>
          <div class="skeleton-nav--item-profile"></div>
        </div>
      </div>
    </div>
    <div class="skeleton-nav--right">
      <ul class="skeleton-nav--items-wide">
        <li class="skeleton--icon-logo-container">
          <div class="skeleton--icon-container"></div>
          <div class="skeleton--icon-description"></div>
          <div class="skeleton--icon-logo"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
        <li>
          <div class="skeleton--icon-small"></div>
          <div class="skeleton-nav--item-wide-content"></div>
        </li>
      </ul>
    </div>
  </nav>
</div>

    <div id="wrapper">
      
  


      
  <div id="nps-survey-container"></div>

 

      
  

<div id="account-warning" data-module="header/account-warning"
  data-unconfirmed-addresses="false"
  data-no-addresses="false"
  
></div>



      
  
<header id="aui-message-bar">
  
</header>


      <div id="content" role="main">

        
          <header class="app-header">
            <div class="app-header--primary">
              
                <div class="app-header--context">
                  <div class="app-header--breadcrumbs">
                    
  <ol class="aui-nav aui-nav-breadcrumbs">
    <li>
  <a href="/Yanosik/">Jan</a>
</li>

<li>
  <a href="/Yanosik/zapart">Zapart</a>
</li>
    
  <li>
    <a href="/Yanosik/zapart/src">
      Source
    </a>
  </li>

  </ol>

                  </div>
                  <h1 class="app-header--heading">
                    <span class="file-path">glxew.h</span>
                  </h1>
                </div>
              
            </div>

            <div class="app-header--secondary">
              
                
              
            </div>
          </header>
        

        
        
  <div class="aui-page-panel ">
    <div class="hidden">
  
  </div>
    <div class="aui-page-panel-inner">

      <div
        id="repo-content"
        class="aui-page-panel-content forks-enabled can-create"
        data-module="repo/index"
        
      >
        
        
  <div id="source-container" class="maskable" data-module="repo/source/index">
    



<header id="source-path">
  
    <div class="labels labels-csv">
      <div class="aui-buttons">
        <button data-branches-tags-url="/api/1.0/repositories/Yanosik/zapart/branches-tags"
                data-module="components/branch-dialog"
                
                class="aui-button branch-dialog-trigger" title="master">
          
            
              <span class="aui-icon aui-icon-small aui-iconfont-devtools-branch">Branch</span>
            
            <span class="name">master</span>
          
          <span class="aui-icon-dropdown"></span>
        </button>
        <button class="aui-button" id="checkout-branch-button"
                title="Check out this branch">
          <span class="aui-icon aui-icon-small aui-iconfont-devtools-clone">Check out branch</span>
          <span class="aui-icon-dropdown"></span>
        </button>
      </div>
      
    
    
  
    </div>
  
  
    <div class="secondary-actions">
      <div class="aui-buttons">
        
          <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/glxew.h?at=master"
            class="aui-button pjax-trigger source-toggle" aria-pressed="true">
            Source
          </a>
          <a href="/Yanosik/zapart/diff/Include/GL/glxew.h?diff2=f71033cf7729&at=master"
            class="aui-button pjax-trigger diff-toggle"
            title="Diff to previous change">
            Diff
          </a>
          <a href="/Yanosik/zapart/history-node/f71033cf7729/Include/GL/glxew.h?at=master"
            class="aui-button pjax-trigger history-toggle">
            History
          </a>
        
      </div>

      
        
        
      

    </div>
  
  <h1>
    
      
        <a href="/Yanosik/zapart/src/f71033cf7729?at=master"
          class="pjax-trigger root" title="Yanosik/zapart at f71033cf7729">Zapart</a> /
      
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/?at=master"
              class="pjax-trigger directory-name">Include</a> /
          
        
      
        
          
            <a href="/Yanosik/zapart/src/f71033cf7729/Include/GL/?at=master"
              class="pjax-trigger directory-name">GL</a> /
          
        
      
        
          
            <span class="file-name">glxew.h</span>
          
        
      
    
  </h1>
  
    
    
  
  <div class="clearfix"></div>
</header>


  
    
  

  <div id="editor-container" class="maskable"
       data-module="repo/source/editor"
       data-owner="Yanosik"
       data-slug="zapart"
       data-is-writer="true"
       data-has-push-access="true"
       data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"
       data-branch="master"
       data-path="Include/GL/glxew.h"
       data-source-url="/api/internal/repositories/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/glxew.h">
    <div id="source-view" class="file-source-container" data-module="repo/source/view-file" data-is-lfs="false">
      <div class="toolbar">
        <div class="primary">
          <div class="aui-buttons">
            
              <button id="file-history-trigger" class="aui-button aui-button-light changeset-info"
                      data-changeset="f71033cf7729d442b43ee6f67d340a04470f80ae"
                      data-path="Include/GL/glxew.h"
                      data-current="f71033cf7729d442b43ee6f67d340a04470f80ae">
                 

  <div class="aui-avatar aui-avatar-xsmall">
    <div class="aui-avatar-inner">
      <img src="https://bitbucket.org/account/Yanosik/avatar/16/?ts=1511779772">
    </div>
  </div>
  <span class="changeset-hash">f71033c</span>
  <time datetime="2017-11-13T12:06:29+00:00" class="timestamp"></time>
  <span class="aui-icon-dropdown"></span>

              </button>
            
          </div>
          
          <a href="/Yanosik/zapart/full-commit/f71033cf7729/Include/GL/glxew.h" id="full-commit-link"
             title="View full commit f71033c">Full commit</a>
        </div>
        <div class="secondary">
          
          <div class="aui-buttons">
            
              <a href="/Yanosik/zapart/annotate/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/glxew.h?at=master"
                 class="aui-button aui-button-light pjax-trigger blame-link">Blame</a>
              
            
            <a href="/Yanosik/zapart/raw/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/glxew.h" class="aui-button aui-button-light raw-link">Raw</a>
          </div>
          
            

            <div class="aui-buttons">
              
              <button id="file-edit-button" class="edit-button aui-button aui-button-light aui-button-split-main"
                  

                  >
                Edit
                
              </button>
              <button id="file-more-actions-button" class="aui-button aui-button-light aui-dropdown2-trigger aui-button-split-more" aria-owns="split-container-dropdown" aria-haspopup="true"
                  >
                More file actions
              </button>
            </div>
            <div id="split-container-dropdown" class="aui-dropdown2 aui-style-default" data-container="#editor-container">
              <ul class="aui-list-truncate">
                <li><a href="#" data-module="repo/source/rename-file" class="rename-link">Rename</a></li>
                <li><a href="#" data-module="repo/source/delete-file" class="delete-link">Delete</a></li>
              </ul>
            </div>
          
        </div>

        <div id="fileview-dropdown"
            class="aui-dropdown2 aui-style-default"
            data-fileview-container="#fileview-container"
            
            
            data-fileview-button="#fileview-trigger"
            data-module="connect/fileview">
          <div class="aui-dropdown2-section">
            <ul>
              <li>
                <a class="aui-dropdown2-radio aui-dropdown2-checked"
                    data-fileview-id="-1"
                    data-fileview-loaded="true"
                    data-fileview-target="#fileview-original"
                    data-fileview-connection-key=""
                    data-fileview-module-key="file-view-default">
                  Default File Viewer
                </a>
              </li>
              
            </ul>
          </div>
        </div>

        <div class="clearfix"></div>
      </div>
      <div id="fileview-container">
        <div id="fileview-original"
            class="fileview"
            data-module="repo/source/highlight-lines"
            data-fileview-loaded="true">
          


  
    <div class="file-source">
      <table class="codehilite highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre><a href="#glxew.h-1">   1</a>
<a href="#glxew.h-2">   2</a>
<a href="#glxew.h-3">   3</a>
<a href="#glxew.h-4">   4</a>
<a href="#glxew.h-5">   5</a>
<a href="#glxew.h-6">   6</a>
<a href="#glxew.h-7">   7</a>
<a href="#glxew.h-8">   8</a>
<a href="#glxew.h-9">   9</a>
<a href="#glxew.h-10">  10</a>
<a href="#glxew.h-11">  11</a>
<a href="#glxew.h-12">  12</a>
<a href="#glxew.h-13">  13</a>
<a href="#glxew.h-14">  14</a>
<a href="#glxew.h-15">  15</a>
<a href="#glxew.h-16">  16</a>
<a href="#glxew.h-17">  17</a>
<a href="#glxew.h-18">  18</a>
<a href="#glxew.h-19">  19</a>
<a href="#glxew.h-20">  20</a>
<a href="#glxew.h-21">  21</a>
<a href="#glxew.h-22">  22</a>
<a href="#glxew.h-23">  23</a>
<a href="#glxew.h-24">  24</a>
<a href="#glxew.h-25">  25</a>
<a href="#glxew.h-26">  26</a>
<a href="#glxew.h-27">  27</a>
<a href="#glxew.h-28">  28</a>
<a href="#glxew.h-29">  29</a>
<a href="#glxew.h-30">  30</a>
<a href="#glxew.h-31">  31</a>
<a href="#glxew.h-32">  32</a>
<a href="#glxew.h-33">  33</a>
<a href="#glxew.h-34">  34</a>
<a href="#glxew.h-35">  35</a>
<a href="#glxew.h-36">  36</a>
<a href="#glxew.h-37">  37</a>
<a href="#glxew.h-38">  38</a>
<a href="#glxew.h-39">  39</a>
<a href="#glxew.h-40">  40</a>
<a href="#glxew.h-41">  41</a>
<a href="#glxew.h-42">  42</a>
<a href="#glxew.h-43">  43</a>
<a href="#glxew.h-44">  44</a>
<a href="#glxew.h-45">  45</a>
<a href="#glxew.h-46">  46</a>
<a href="#glxew.h-47">  47</a>
<a href="#glxew.h-48">  48</a>
<a href="#glxew.h-49">  49</a>
<a href="#glxew.h-50">  50</a>
<a href="#glxew.h-51">  51</a>
<a href="#glxew.h-52">  52</a>
<a href="#glxew.h-53">  53</a>
<a href="#glxew.h-54">  54</a>
<a href="#glxew.h-55">  55</a>
<a href="#glxew.h-56">  56</a>
<a href="#glxew.h-57">  57</a>
<a href="#glxew.h-58">  58</a>
<a href="#glxew.h-59">  59</a>
<a href="#glxew.h-60">  60</a>
<a href="#glxew.h-61">  61</a>
<a href="#glxew.h-62">  62</a>
<a href="#glxew.h-63">  63</a>
<a href="#glxew.h-64">  64</a>
<a href="#glxew.h-65">  65</a>
<a href="#glxew.h-66">  66</a>
<a href="#glxew.h-67">  67</a>
<a href="#glxew.h-68">  68</a>
<a href="#glxew.h-69">  69</a>
<a href="#glxew.h-70">  70</a>
<a href="#glxew.h-71">  71</a>
<a href="#glxew.h-72">  72</a>
<a href="#glxew.h-73">  73</a>
<a href="#glxew.h-74">  74</a>
<a href="#glxew.h-75">  75</a>
<a href="#glxew.h-76">  76</a>
<a href="#glxew.h-77">  77</a>
<a href="#glxew.h-78">  78</a>
<a href="#glxew.h-79">  79</a>
<a href="#glxew.h-80">  80</a>
<a href="#glxew.h-81">  81</a>
<a href="#glxew.h-82">  82</a>
<a href="#glxew.h-83">  83</a>
<a href="#glxew.h-84">  84</a>
<a href="#glxew.h-85">  85</a>
<a href="#glxew.h-86">  86</a>
<a href="#glxew.h-87">  87</a>
<a href="#glxew.h-88">  88</a>
<a href="#glxew.h-89">  89</a>
<a href="#glxew.h-90">  90</a>
<a href="#glxew.h-91">  91</a>
<a href="#glxew.h-92">  92</a>
<a href="#glxew.h-93">  93</a>
<a href="#glxew.h-94">  94</a>
<a href="#glxew.h-95">  95</a>
<a href="#glxew.h-96">  96</a>
<a href="#glxew.h-97">  97</a>
<a href="#glxew.h-98">  98</a>
<a href="#glxew.h-99">  99</a>
<a href="#glxew.h-100"> 100</a>
<a href="#glxew.h-101"> 101</a>
<a href="#glxew.h-102"> 102</a>
<a href="#glxew.h-103"> 103</a>
<a href="#glxew.h-104"> 104</a>
<a href="#glxew.h-105"> 105</a>
<a href="#glxew.h-106"> 106</a>
<a href="#glxew.h-107"> 107</a>
<a href="#glxew.h-108"> 108</a>
<a href="#glxew.h-109"> 109</a>
<a href="#glxew.h-110"> 110</a>
<a href="#glxew.h-111"> 111</a>
<a href="#glxew.h-112"> 112</a>
<a href="#glxew.h-113"> 113</a>
<a href="#glxew.h-114"> 114</a>
<a href="#glxew.h-115"> 115</a>
<a href="#glxew.h-116"> 116</a>
<a href="#glxew.h-117"> 117</a>
<a href="#glxew.h-118"> 118</a>
<a href="#glxew.h-119"> 119</a>
<a href="#glxew.h-120"> 120</a>
<a href="#glxew.h-121"> 121</a>
<a href="#glxew.h-122"> 122</a>
<a href="#glxew.h-123"> 123</a>
<a href="#glxew.h-124"> 124</a>
<a href="#glxew.h-125"> 125</a>
<a href="#glxew.h-126"> 126</a>
<a href="#glxew.h-127"> 127</a>
<a href="#glxew.h-128"> 128</a>
<a href="#glxew.h-129"> 129</a>
<a href="#glxew.h-130"> 130</a>
<a href="#glxew.h-131"> 131</a>
<a href="#glxew.h-132"> 132</a>
<a href="#glxew.h-133"> 133</a>
<a href="#glxew.h-134"> 134</a>
<a href="#glxew.h-135"> 135</a>
<a href="#glxew.h-136"> 136</a>
<a href="#glxew.h-137"> 137</a>
<a href="#glxew.h-138"> 138</a>
<a href="#glxew.h-139"> 139</a>
<a href="#glxew.h-140"> 140</a>
<a href="#glxew.h-141"> 141</a>
<a href="#glxew.h-142"> 142</a>
<a href="#glxew.h-143"> 143</a>
<a href="#glxew.h-144"> 144</a>
<a href="#glxew.h-145"> 145</a>
<a href="#glxew.h-146"> 146</a>
<a href="#glxew.h-147"> 147</a>
<a href="#glxew.h-148"> 148</a>
<a href="#glxew.h-149"> 149</a>
<a href="#glxew.h-150"> 150</a>
<a href="#glxew.h-151"> 151</a>
<a href="#glxew.h-152"> 152</a>
<a href="#glxew.h-153"> 153</a>
<a href="#glxew.h-154"> 154</a>
<a href="#glxew.h-155"> 155</a>
<a href="#glxew.h-156"> 156</a>
<a href="#glxew.h-157"> 157</a>
<a href="#glxew.h-158"> 158</a>
<a href="#glxew.h-159"> 159</a>
<a href="#glxew.h-160"> 160</a>
<a href="#glxew.h-161"> 161</a>
<a href="#glxew.h-162"> 162</a>
<a href="#glxew.h-163"> 163</a>
<a href="#glxew.h-164"> 164</a>
<a href="#glxew.h-165"> 165</a>
<a href="#glxew.h-166"> 166</a>
<a href="#glxew.h-167"> 167</a>
<a href="#glxew.h-168"> 168</a>
<a href="#glxew.h-169"> 169</a>
<a href="#glxew.h-170"> 170</a>
<a href="#glxew.h-171"> 171</a>
<a href="#glxew.h-172"> 172</a>
<a href="#glxew.h-173"> 173</a>
<a href="#glxew.h-174"> 174</a>
<a href="#glxew.h-175"> 175</a>
<a href="#glxew.h-176"> 176</a>
<a href="#glxew.h-177"> 177</a>
<a href="#glxew.h-178"> 178</a>
<a href="#glxew.h-179"> 179</a>
<a href="#glxew.h-180"> 180</a>
<a href="#glxew.h-181"> 181</a>
<a href="#glxew.h-182"> 182</a>
<a href="#glxew.h-183"> 183</a>
<a href="#glxew.h-184"> 184</a>
<a href="#glxew.h-185"> 185</a>
<a href="#glxew.h-186"> 186</a>
<a href="#glxew.h-187"> 187</a>
<a href="#glxew.h-188"> 188</a>
<a href="#glxew.h-189"> 189</a>
<a href="#glxew.h-190"> 190</a>
<a href="#glxew.h-191"> 191</a>
<a href="#glxew.h-192"> 192</a>
<a href="#glxew.h-193"> 193</a>
<a href="#glxew.h-194"> 194</a>
<a href="#glxew.h-195"> 195</a>
<a href="#glxew.h-196"> 196</a>
<a href="#glxew.h-197"> 197</a>
<a href="#glxew.h-198"> 198</a>
<a href="#glxew.h-199"> 199</a>
<a href="#glxew.h-200"> 200</a>
<a href="#glxew.h-201"> 201</a>
<a href="#glxew.h-202"> 202</a>
<a href="#glxew.h-203"> 203</a>
<a href="#glxew.h-204"> 204</a>
<a href="#glxew.h-205"> 205</a>
<a href="#glxew.h-206"> 206</a>
<a href="#glxew.h-207"> 207</a>
<a href="#glxew.h-208"> 208</a>
<a href="#glxew.h-209"> 209</a>
<a href="#glxew.h-210"> 210</a>
<a href="#glxew.h-211"> 211</a>
<a href="#glxew.h-212"> 212</a>
<a href="#glxew.h-213"> 213</a>
<a href="#glxew.h-214"> 214</a>
<a href="#glxew.h-215"> 215</a>
<a href="#glxew.h-216"> 216</a>
<a href="#glxew.h-217"> 217</a>
<a href="#glxew.h-218"> 218</a>
<a href="#glxew.h-219"> 219</a>
<a href="#glxew.h-220"> 220</a>
<a href="#glxew.h-221"> 221</a>
<a href="#glxew.h-222"> 222</a>
<a href="#glxew.h-223"> 223</a>
<a href="#glxew.h-224"> 224</a>
<a href="#glxew.h-225"> 225</a>
<a href="#glxew.h-226"> 226</a>
<a href="#glxew.h-227"> 227</a>
<a href="#glxew.h-228"> 228</a>
<a href="#glxew.h-229"> 229</a>
<a href="#glxew.h-230"> 230</a>
<a href="#glxew.h-231"> 231</a>
<a href="#glxew.h-232"> 232</a>
<a href="#glxew.h-233"> 233</a>
<a href="#glxew.h-234"> 234</a>
<a href="#glxew.h-235"> 235</a>
<a href="#glxew.h-236"> 236</a>
<a href="#glxew.h-237"> 237</a>
<a href="#glxew.h-238"> 238</a>
<a href="#glxew.h-239"> 239</a>
<a href="#glxew.h-240"> 240</a>
<a href="#glxew.h-241"> 241</a>
<a href="#glxew.h-242"> 242</a>
<a href="#glxew.h-243"> 243</a>
<a href="#glxew.h-244"> 244</a>
<a href="#glxew.h-245"> 245</a>
<a href="#glxew.h-246"> 246</a>
<a href="#glxew.h-247"> 247</a>
<a href="#glxew.h-248"> 248</a>
<a href="#glxew.h-249"> 249</a>
<a href="#glxew.h-250"> 250</a>
<a href="#glxew.h-251"> 251</a>
<a href="#glxew.h-252"> 252</a>
<a href="#glxew.h-253"> 253</a>
<a href="#glxew.h-254"> 254</a>
<a href="#glxew.h-255"> 255</a>
<a href="#glxew.h-256"> 256</a>
<a href="#glxew.h-257"> 257</a>
<a href="#glxew.h-258"> 258</a>
<a href="#glxew.h-259"> 259</a>
<a href="#glxew.h-260"> 260</a>
<a href="#glxew.h-261"> 261</a>
<a href="#glxew.h-262"> 262</a>
<a href="#glxew.h-263"> 263</a>
<a href="#glxew.h-264"> 264</a>
<a href="#glxew.h-265"> 265</a>
<a href="#glxew.h-266"> 266</a>
<a href="#glxew.h-267"> 267</a>
<a href="#glxew.h-268"> 268</a>
<a href="#glxew.h-269"> 269</a>
<a href="#glxew.h-270"> 270</a>
<a href="#glxew.h-271"> 271</a>
<a href="#glxew.h-272"> 272</a>
<a href="#glxew.h-273"> 273</a>
<a href="#glxew.h-274"> 274</a>
<a href="#glxew.h-275"> 275</a>
<a href="#glxew.h-276"> 276</a>
<a href="#glxew.h-277"> 277</a>
<a href="#glxew.h-278"> 278</a>
<a href="#glxew.h-279"> 279</a>
<a href="#glxew.h-280"> 280</a>
<a href="#glxew.h-281"> 281</a>
<a href="#glxew.h-282"> 282</a>
<a href="#glxew.h-283"> 283</a>
<a href="#glxew.h-284"> 284</a>
<a href="#glxew.h-285"> 285</a>
<a href="#glxew.h-286"> 286</a>
<a href="#glxew.h-287"> 287</a>
<a href="#glxew.h-288"> 288</a>
<a href="#glxew.h-289"> 289</a>
<a href="#glxew.h-290"> 290</a>
<a href="#glxew.h-291"> 291</a>
<a href="#glxew.h-292"> 292</a>
<a href="#glxew.h-293"> 293</a>
<a href="#glxew.h-294"> 294</a>
<a href="#glxew.h-295"> 295</a>
<a href="#glxew.h-296"> 296</a>
<a href="#glxew.h-297"> 297</a>
<a href="#glxew.h-298"> 298</a>
<a href="#glxew.h-299"> 299</a>
<a href="#glxew.h-300"> 300</a>
<a href="#glxew.h-301"> 301</a>
<a href="#glxew.h-302"> 302</a>
<a href="#glxew.h-303"> 303</a>
<a href="#glxew.h-304"> 304</a>
<a href="#glxew.h-305"> 305</a>
<a href="#glxew.h-306"> 306</a>
<a href="#glxew.h-307"> 307</a>
<a href="#glxew.h-308"> 308</a>
<a href="#glxew.h-309"> 309</a>
<a href="#glxew.h-310"> 310</a>
<a href="#glxew.h-311"> 311</a>
<a href="#glxew.h-312"> 312</a>
<a href="#glxew.h-313"> 313</a>
<a href="#glxew.h-314"> 314</a>
<a href="#glxew.h-315"> 315</a>
<a href="#glxew.h-316"> 316</a>
<a href="#glxew.h-317"> 317</a>
<a href="#glxew.h-318"> 318</a>
<a href="#glxew.h-319"> 319</a>
<a href="#glxew.h-320"> 320</a>
<a href="#glxew.h-321"> 321</a>
<a href="#glxew.h-322"> 322</a>
<a href="#glxew.h-323"> 323</a>
<a href="#glxew.h-324"> 324</a>
<a href="#glxew.h-325"> 325</a>
<a href="#glxew.h-326"> 326</a>
<a href="#glxew.h-327"> 327</a>
<a href="#glxew.h-328"> 328</a>
<a href="#glxew.h-329"> 329</a>
<a href="#glxew.h-330"> 330</a>
<a href="#glxew.h-331"> 331</a>
<a href="#glxew.h-332"> 332</a>
<a href="#glxew.h-333"> 333</a>
<a href="#glxew.h-334"> 334</a>
<a href="#glxew.h-335"> 335</a>
<a href="#glxew.h-336"> 336</a>
<a href="#glxew.h-337"> 337</a>
<a href="#glxew.h-338"> 338</a>
<a href="#glxew.h-339"> 339</a>
<a href="#glxew.h-340"> 340</a>
<a href="#glxew.h-341"> 341</a>
<a href="#glxew.h-342"> 342</a>
<a href="#glxew.h-343"> 343</a>
<a href="#glxew.h-344"> 344</a>
<a href="#glxew.h-345"> 345</a>
<a href="#glxew.h-346"> 346</a>
<a href="#glxew.h-347"> 347</a>
<a href="#glxew.h-348"> 348</a>
<a href="#glxew.h-349"> 349</a>
<a href="#glxew.h-350"> 350</a>
<a href="#glxew.h-351"> 351</a>
<a href="#glxew.h-352"> 352</a>
<a href="#glxew.h-353"> 353</a>
<a href="#glxew.h-354"> 354</a>
<a href="#glxew.h-355"> 355</a>
<a href="#glxew.h-356"> 356</a>
<a href="#glxew.h-357"> 357</a>
<a href="#glxew.h-358"> 358</a>
<a href="#glxew.h-359"> 359</a>
<a href="#glxew.h-360"> 360</a>
<a href="#glxew.h-361"> 361</a>
<a href="#glxew.h-362"> 362</a>
<a href="#glxew.h-363"> 363</a>
<a href="#glxew.h-364"> 364</a>
<a href="#glxew.h-365"> 365</a>
<a href="#glxew.h-366"> 366</a>
<a href="#glxew.h-367"> 367</a>
<a href="#glxew.h-368"> 368</a>
<a href="#glxew.h-369"> 369</a>
<a href="#glxew.h-370"> 370</a>
<a href="#glxew.h-371"> 371</a>
<a href="#glxew.h-372"> 372</a>
<a href="#glxew.h-373"> 373</a>
<a href="#glxew.h-374"> 374</a>
<a href="#glxew.h-375"> 375</a>
<a href="#glxew.h-376"> 376</a>
<a href="#glxew.h-377"> 377</a>
<a href="#glxew.h-378"> 378</a>
<a href="#glxew.h-379"> 379</a>
<a href="#glxew.h-380"> 380</a>
<a href="#glxew.h-381"> 381</a>
<a href="#glxew.h-382"> 382</a>
<a href="#glxew.h-383"> 383</a>
<a href="#glxew.h-384"> 384</a>
<a href="#glxew.h-385"> 385</a>
<a href="#glxew.h-386"> 386</a>
<a href="#glxew.h-387"> 387</a>
<a href="#glxew.h-388"> 388</a>
<a href="#glxew.h-389"> 389</a>
<a href="#glxew.h-390"> 390</a>
<a href="#glxew.h-391"> 391</a>
<a href="#glxew.h-392"> 392</a>
<a href="#glxew.h-393"> 393</a>
<a href="#glxew.h-394"> 394</a>
<a href="#glxew.h-395"> 395</a>
<a href="#glxew.h-396"> 396</a>
<a href="#glxew.h-397"> 397</a>
<a href="#glxew.h-398"> 398</a>
<a href="#glxew.h-399"> 399</a>
<a href="#glxew.h-400"> 400</a>
<a href="#glxew.h-401"> 401</a>
<a href="#glxew.h-402"> 402</a>
<a href="#glxew.h-403"> 403</a>
<a href="#glxew.h-404"> 404</a>
<a href="#glxew.h-405"> 405</a>
<a href="#glxew.h-406"> 406</a>
<a href="#glxew.h-407"> 407</a>
<a href="#glxew.h-408"> 408</a>
<a href="#glxew.h-409"> 409</a>
<a href="#glxew.h-410"> 410</a>
<a href="#glxew.h-411"> 411</a>
<a href="#glxew.h-412"> 412</a>
<a href="#glxew.h-413"> 413</a>
<a href="#glxew.h-414"> 414</a>
<a href="#glxew.h-415"> 415</a>
<a href="#glxew.h-416"> 416</a>
<a href="#glxew.h-417"> 417</a>
<a href="#glxew.h-418"> 418</a>
<a href="#glxew.h-419"> 419</a>
<a href="#glxew.h-420"> 420</a>
<a href="#glxew.h-421"> 421</a>
<a href="#glxew.h-422"> 422</a>
<a href="#glxew.h-423"> 423</a>
<a href="#glxew.h-424"> 424</a>
<a href="#glxew.h-425"> 425</a>
<a href="#glxew.h-426"> 426</a>
<a href="#glxew.h-427"> 427</a>
<a href="#glxew.h-428"> 428</a>
<a href="#glxew.h-429"> 429</a>
<a href="#glxew.h-430"> 430</a>
<a href="#glxew.h-431"> 431</a>
<a href="#glxew.h-432"> 432</a>
<a href="#glxew.h-433"> 433</a>
<a href="#glxew.h-434"> 434</a>
<a href="#glxew.h-435"> 435</a>
<a href="#glxew.h-436"> 436</a>
<a href="#glxew.h-437"> 437</a>
<a href="#glxew.h-438"> 438</a>
<a href="#glxew.h-439"> 439</a>
<a href="#glxew.h-440"> 440</a>
<a href="#glxew.h-441"> 441</a>
<a href="#glxew.h-442"> 442</a>
<a href="#glxew.h-443"> 443</a>
<a href="#glxew.h-444"> 444</a>
<a href="#glxew.h-445"> 445</a>
<a href="#glxew.h-446"> 446</a>
<a href="#glxew.h-447"> 447</a>
<a href="#glxew.h-448"> 448</a>
<a href="#glxew.h-449"> 449</a>
<a href="#glxew.h-450"> 450</a>
<a href="#glxew.h-451"> 451</a>
<a href="#glxew.h-452"> 452</a>
<a href="#glxew.h-453"> 453</a>
<a href="#glxew.h-454"> 454</a>
<a href="#glxew.h-455"> 455</a>
<a href="#glxew.h-456"> 456</a>
<a href="#glxew.h-457"> 457</a>
<a href="#glxew.h-458"> 458</a>
<a href="#glxew.h-459"> 459</a>
<a href="#glxew.h-460"> 460</a>
<a href="#glxew.h-461"> 461</a>
<a href="#glxew.h-462"> 462</a>
<a href="#glxew.h-463"> 463</a>
<a href="#glxew.h-464"> 464</a>
<a href="#glxew.h-465"> 465</a>
<a href="#glxew.h-466"> 466</a>
<a href="#glxew.h-467"> 467</a>
<a href="#glxew.h-468"> 468</a>
<a href="#glxew.h-469"> 469</a>
<a href="#glxew.h-470"> 470</a>
<a href="#glxew.h-471"> 471</a>
<a href="#glxew.h-472"> 472</a>
<a href="#glxew.h-473"> 473</a>
<a href="#glxew.h-474"> 474</a>
<a href="#glxew.h-475"> 475</a>
<a href="#glxew.h-476"> 476</a>
<a href="#glxew.h-477"> 477</a>
<a href="#glxew.h-478"> 478</a>
<a href="#glxew.h-479"> 479</a>
<a href="#glxew.h-480"> 480</a>
<a href="#glxew.h-481"> 481</a>
<a href="#glxew.h-482"> 482</a>
<a href="#glxew.h-483"> 483</a>
<a href="#glxew.h-484"> 484</a>
<a href="#glxew.h-485"> 485</a>
<a href="#glxew.h-486"> 486</a>
<a href="#glxew.h-487"> 487</a>
<a href="#glxew.h-488"> 488</a>
<a href="#glxew.h-489"> 489</a>
<a href="#glxew.h-490"> 490</a>
<a href="#glxew.h-491"> 491</a>
<a href="#glxew.h-492"> 492</a>
<a href="#glxew.h-493"> 493</a>
<a href="#glxew.h-494"> 494</a>
<a href="#glxew.h-495"> 495</a>
<a href="#glxew.h-496"> 496</a>
<a href="#glxew.h-497"> 497</a>
<a href="#glxew.h-498"> 498</a>
<a href="#glxew.h-499"> 499</a>
<a href="#glxew.h-500"> 500</a>
<a href="#glxew.h-501"> 501</a>
<a href="#glxew.h-502"> 502</a>
<a href="#glxew.h-503"> 503</a>
<a href="#glxew.h-504"> 504</a>
<a href="#glxew.h-505"> 505</a>
<a href="#glxew.h-506"> 506</a>
<a href="#glxew.h-507"> 507</a>
<a href="#glxew.h-508"> 508</a>
<a href="#glxew.h-509"> 509</a>
<a href="#glxew.h-510"> 510</a>
<a href="#glxew.h-511"> 511</a>
<a href="#glxew.h-512"> 512</a>
<a href="#glxew.h-513"> 513</a>
<a href="#glxew.h-514"> 514</a>
<a href="#glxew.h-515"> 515</a>
<a href="#glxew.h-516"> 516</a>
<a href="#glxew.h-517"> 517</a>
<a href="#glxew.h-518"> 518</a>
<a href="#glxew.h-519"> 519</a>
<a href="#glxew.h-520"> 520</a>
<a href="#glxew.h-521"> 521</a>
<a href="#glxew.h-522"> 522</a>
<a href="#glxew.h-523"> 523</a>
<a href="#glxew.h-524"> 524</a>
<a href="#glxew.h-525"> 525</a>
<a href="#glxew.h-526"> 526</a>
<a href="#glxew.h-527"> 527</a>
<a href="#glxew.h-528"> 528</a>
<a href="#glxew.h-529"> 529</a>
<a href="#glxew.h-530"> 530</a>
<a href="#glxew.h-531"> 531</a>
<a href="#glxew.h-532"> 532</a>
<a href="#glxew.h-533"> 533</a>
<a href="#glxew.h-534"> 534</a>
<a href="#glxew.h-535"> 535</a>
<a href="#glxew.h-536"> 536</a>
<a href="#glxew.h-537"> 537</a>
<a href="#glxew.h-538"> 538</a>
<a href="#glxew.h-539"> 539</a>
<a href="#glxew.h-540"> 540</a>
<a href="#glxew.h-541"> 541</a>
<a href="#glxew.h-542"> 542</a>
<a href="#glxew.h-543"> 543</a>
<a href="#glxew.h-544"> 544</a>
<a href="#glxew.h-545"> 545</a>
<a href="#glxew.h-546"> 546</a>
<a href="#glxew.h-547"> 547</a>
<a href="#glxew.h-548"> 548</a>
<a href="#glxew.h-549"> 549</a>
<a href="#glxew.h-550"> 550</a>
<a href="#glxew.h-551"> 551</a>
<a href="#glxew.h-552"> 552</a>
<a href="#glxew.h-553"> 553</a>
<a href="#glxew.h-554"> 554</a>
<a href="#glxew.h-555"> 555</a>
<a href="#glxew.h-556"> 556</a>
<a href="#glxew.h-557"> 557</a>
<a href="#glxew.h-558"> 558</a>
<a href="#glxew.h-559"> 559</a>
<a href="#glxew.h-560"> 560</a>
<a href="#glxew.h-561"> 561</a>
<a href="#glxew.h-562"> 562</a>
<a href="#glxew.h-563"> 563</a>
<a href="#glxew.h-564"> 564</a>
<a href="#glxew.h-565"> 565</a>
<a href="#glxew.h-566"> 566</a>
<a href="#glxew.h-567"> 567</a>
<a href="#glxew.h-568"> 568</a>
<a href="#glxew.h-569"> 569</a>
<a href="#glxew.h-570"> 570</a>
<a href="#glxew.h-571"> 571</a>
<a href="#glxew.h-572"> 572</a>
<a href="#glxew.h-573"> 573</a>
<a href="#glxew.h-574"> 574</a>
<a href="#glxew.h-575"> 575</a>
<a href="#glxew.h-576"> 576</a>
<a href="#glxew.h-577"> 577</a>
<a href="#glxew.h-578"> 578</a>
<a href="#glxew.h-579"> 579</a>
<a href="#glxew.h-580"> 580</a>
<a href="#glxew.h-581"> 581</a>
<a href="#glxew.h-582"> 582</a>
<a href="#glxew.h-583"> 583</a>
<a href="#glxew.h-584"> 584</a>
<a href="#glxew.h-585"> 585</a>
<a href="#glxew.h-586"> 586</a>
<a href="#glxew.h-587"> 587</a>
<a href="#glxew.h-588"> 588</a>
<a href="#glxew.h-589"> 589</a>
<a href="#glxew.h-590"> 590</a>
<a href="#glxew.h-591"> 591</a>
<a href="#glxew.h-592"> 592</a>
<a href="#glxew.h-593"> 593</a>
<a href="#glxew.h-594"> 594</a>
<a href="#glxew.h-595"> 595</a>
<a href="#glxew.h-596"> 596</a>
<a href="#glxew.h-597"> 597</a>
<a href="#glxew.h-598"> 598</a>
<a href="#glxew.h-599"> 599</a>
<a href="#glxew.h-600"> 600</a>
<a href="#glxew.h-601"> 601</a>
<a href="#glxew.h-602"> 602</a>
<a href="#glxew.h-603"> 603</a>
<a href="#glxew.h-604"> 604</a>
<a href="#glxew.h-605"> 605</a>
<a href="#glxew.h-606"> 606</a>
<a href="#glxew.h-607"> 607</a>
<a href="#glxew.h-608"> 608</a>
<a href="#glxew.h-609"> 609</a>
<a href="#glxew.h-610"> 610</a>
<a href="#glxew.h-611"> 611</a>
<a href="#glxew.h-612"> 612</a>
<a href="#glxew.h-613"> 613</a>
<a href="#glxew.h-614"> 614</a>
<a href="#glxew.h-615"> 615</a>
<a href="#glxew.h-616"> 616</a>
<a href="#glxew.h-617"> 617</a>
<a href="#glxew.h-618"> 618</a>
<a href="#glxew.h-619"> 619</a>
<a href="#glxew.h-620"> 620</a>
<a href="#glxew.h-621"> 621</a>
<a href="#glxew.h-622"> 622</a>
<a href="#glxew.h-623"> 623</a>
<a href="#glxew.h-624"> 624</a>
<a href="#glxew.h-625"> 625</a>
<a href="#glxew.h-626"> 626</a>
<a href="#glxew.h-627"> 627</a>
<a href="#glxew.h-628"> 628</a>
<a href="#glxew.h-629"> 629</a>
<a href="#glxew.h-630"> 630</a>
<a href="#glxew.h-631"> 631</a>
<a href="#glxew.h-632"> 632</a>
<a href="#glxew.h-633"> 633</a>
<a href="#glxew.h-634"> 634</a>
<a href="#glxew.h-635"> 635</a>
<a href="#glxew.h-636"> 636</a>
<a href="#glxew.h-637"> 637</a>
<a href="#glxew.h-638"> 638</a>
<a href="#glxew.h-639"> 639</a>
<a href="#glxew.h-640"> 640</a>
<a href="#glxew.h-641"> 641</a>
<a href="#glxew.h-642"> 642</a>
<a href="#glxew.h-643"> 643</a>
<a href="#glxew.h-644"> 644</a>
<a href="#glxew.h-645"> 645</a>
<a href="#glxew.h-646"> 646</a>
<a href="#glxew.h-647"> 647</a>
<a href="#glxew.h-648"> 648</a>
<a href="#glxew.h-649"> 649</a>
<a href="#glxew.h-650"> 650</a>
<a href="#glxew.h-651"> 651</a>
<a href="#glxew.h-652"> 652</a>
<a href="#glxew.h-653"> 653</a>
<a href="#glxew.h-654"> 654</a>
<a href="#glxew.h-655"> 655</a>
<a href="#glxew.h-656"> 656</a>
<a href="#glxew.h-657"> 657</a>
<a href="#glxew.h-658"> 658</a>
<a href="#glxew.h-659"> 659</a>
<a href="#glxew.h-660"> 660</a>
<a href="#glxew.h-661"> 661</a>
<a href="#glxew.h-662"> 662</a>
<a href="#glxew.h-663"> 663</a>
<a href="#glxew.h-664"> 664</a>
<a href="#glxew.h-665"> 665</a>
<a href="#glxew.h-666"> 666</a>
<a href="#glxew.h-667"> 667</a>
<a href="#glxew.h-668"> 668</a>
<a href="#glxew.h-669"> 669</a>
<a href="#glxew.h-670"> 670</a>
<a href="#glxew.h-671"> 671</a>
<a href="#glxew.h-672"> 672</a>
<a href="#glxew.h-673"> 673</a>
<a href="#glxew.h-674"> 674</a>
<a href="#glxew.h-675"> 675</a>
<a href="#glxew.h-676"> 676</a>
<a href="#glxew.h-677"> 677</a>
<a href="#glxew.h-678"> 678</a>
<a href="#glxew.h-679"> 679</a>
<a href="#glxew.h-680"> 680</a>
<a href="#glxew.h-681"> 681</a>
<a href="#glxew.h-682"> 682</a>
<a href="#glxew.h-683"> 683</a>
<a href="#glxew.h-684"> 684</a>
<a href="#glxew.h-685"> 685</a>
<a href="#glxew.h-686"> 686</a>
<a href="#glxew.h-687"> 687</a>
<a href="#glxew.h-688"> 688</a>
<a href="#glxew.h-689"> 689</a>
<a href="#glxew.h-690"> 690</a>
<a href="#glxew.h-691"> 691</a>
<a href="#glxew.h-692"> 692</a>
<a href="#glxew.h-693"> 693</a>
<a href="#glxew.h-694"> 694</a>
<a href="#glxew.h-695"> 695</a>
<a href="#glxew.h-696"> 696</a>
<a href="#glxew.h-697"> 697</a>
<a href="#glxew.h-698"> 698</a>
<a href="#glxew.h-699"> 699</a>
<a href="#glxew.h-700"> 700</a>
<a href="#glxew.h-701"> 701</a>
<a href="#glxew.h-702"> 702</a>
<a href="#glxew.h-703"> 703</a>
<a href="#glxew.h-704"> 704</a>
<a href="#glxew.h-705"> 705</a>
<a href="#glxew.h-706"> 706</a>
<a href="#glxew.h-707"> 707</a>
<a href="#glxew.h-708"> 708</a>
<a href="#glxew.h-709"> 709</a>
<a href="#glxew.h-710"> 710</a>
<a href="#glxew.h-711"> 711</a>
<a href="#glxew.h-712"> 712</a>
<a href="#glxew.h-713"> 713</a>
<a href="#glxew.h-714"> 714</a>
<a href="#glxew.h-715"> 715</a>
<a href="#glxew.h-716"> 716</a>
<a href="#glxew.h-717"> 717</a>
<a href="#glxew.h-718"> 718</a>
<a href="#glxew.h-719"> 719</a>
<a href="#glxew.h-720"> 720</a>
<a href="#glxew.h-721"> 721</a>
<a href="#glxew.h-722"> 722</a>
<a href="#glxew.h-723"> 723</a>
<a href="#glxew.h-724"> 724</a>
<a href="#glxew.h-725"> 725</a>
<a href="#glxew.h-726"> 726</a>
<a href="#glxew.h-727"> 727</a>
<a href="#glxew.h-728"> 728</a>
<a href="#glxew.h-729"> 729</a>
<a href="#glxew.h-730"> 730</a>
<a href="#glxew.h-731"> 731</a>
<a href="#glxew.h-732"> 732</a>
<a href="#glxew.h-733"> 733</a>
<a href="#glxew.h-734"> 734</a>
<a href="#glxew.h-735"> 735</a>
<a href="#glxew.h-736"> 736</a>
<a href="#glxew.h-737"> 737</a>
<a href="#glxew.h-738"> 738</a>
<a href="#glxew.h-739"> 739</a>
<a href="#glxew.h-740"> 740</a>
<a href="#glxew.h-741"> 741</a>
<a href="#glxew.h-742"> 742</a>
<a href="#glxew.h-743"> 743</a>
<a href="#glxew.h-744"> 744</a>
<a href="#glxew.h-745"> 745</a>
<a href="#glxew.h-746"> 746</a>
<a href="#glxew.h-747"> 747</a>
<a href="#glxew.h-748"> 748</a>
<a href="#glxew.h-749"> 749</a>
<a href="#glxew.h-750"> 750</a>
<a href="#glxew.h-751"> 751</a>
<a href="#glxew.h-752"> 752</a>
<a href="#glxew.h-753"> 753</a>
<a href="#glxew.h-754"> 754</a>
<a href="#glxew.h-755"> 755</a>
<a href="#glxew.h-756"> 756</a>
<a href="#glxew.h-757"> 757</a>
<a href="#glxew.h-758"> 758</a>
<a href="#glxew.h-759"> 759</a>
<a href="#glxew.h-760"> 760</a>
<a href="#glxew.h-761"> 761</a>
<a href="#glxew.h-762"> 762</a>
<a href="#glxew.h-763"> 763</a>
<a href="#glxew.h-764"> 764</a>
<a href="#glxew.h-765"> 765</a>
<a href="#glxew.h-766"> 766</a>
<a href="#glxew.h-767"> 767</a>
<a href="#glxew.h-768"> 768</a>
<a href="#glxew.h-769"> 769</a>
<a href="#glxew.h-770"> 770</a>
<a href="#glxew.h-771"> 771</a>
<a href="#glxew.h-772"> 772</a>
<a href="#glxew.h-773"> 773</a>
<a href="#glxew.h-774"> 774</a>
<a href="#glxew.h-775"> 775</a>
<a href="#glxew.h-776"> 776</a>
<a href="#glxew.h-777"> 777</a>
<a href="#glxew.h-778"> 778</a>
<a href="#glxew.h-779"> 779</a>
<a href="#glxew.h-780"> 780</a>
<a href="#glxew.h-781"> 781</a>
<a href="#glxew.h-782"> 782</a>
<a href="#glxew.h-783"> 783</a>
<a href="#glxew.h-784"> 784</a>
<a href="#glxew.h-785"> 785</a>
<a href="#glxew.h-786"> 786</a>
<a href="#glxew.h-787"> 787</a>
<a href="#glxew.h-788"> 788</a>
<a href="#glxew.h-789"> 789</a>
<a href="#glxew.h-790"> 790</a>
<a href="#glxew.h-791"> 791</a>
<a href="#glxew.h-792"> 792</a>
<a href="#glxew.h-793"> 793</a>
<a href="#glxew.h-794"> 794</a>
<a href="#glxew.h-795"> 795</a>
<a href="#glxew.h-796"> 796</a>
<a href="#glxew.h-797"> 797</a>
<a href="#glxew.h-798"> 798</a>
<a href="#glxew.h-799"> 799</a>
<a href="#glxew.h-800"> 800</a>
<a href="#glxew.h-801"> 801</a>
<a href="#glxew.h-802"> 802</a>
<a href="#glxew.h-803"> 803</a>
<a href="#glxew.h-804"> 804</a>
<a href="#glxew.h-805"> 805</a>
<a href="#glxew.h-806"> 806</a>
<a href="#glxew.h-807"> 807</a>
<a href="#glxew.h-808"> 808</a>
<a href="#glxew.h-809"> 809</a>
<a href="#glxew.h-810"> 810</a>
<a href="#glxew.h-811"> 811</a>
<a href="#glxew.h-812"> 812</a>
<a href="#glxew.h-813"> 813</a>
<a href="#glxew.h-814"> 814</a>
<a href="#glxew.h-815"> 815</a>
<a href="#glxew.h-816"> 816</a>
<a href="#glxew.h-817"> 817</a>
<a href="#glxew.h-818"> 818</a>
<a href="#glxew.h-819"> 819</a>
<a href="#glxew.h-820"> 820</a>
<a href="#glxew.h-821"> 821</a>
<a href="#glxew.h-822"> 822</a>
<a href="#glxew.h-823"> 823</a>
<a href="#glxew.h-824"> 824</a>
<a href="#glxew.h-825"> 825</a>
<a href="#glxew.h-826"> 826</a>
<a href="#glxew.h-827"> 827</a>
<a href="#glxew.h-828"> 828</a>
<a href="#glxew.h-829"> 829</a>
<a href="#glxew.h-830"> 830</a>
<a href="#glxew.h-831"> 831</a>
<a href="#glxew.h-832"> 832</a>
<a href="#glxew.h-833"> 833</a>
<a href="#glxew.h-834"> 834</a>
<a href="#glxew.h-835"> 835</a>
<a href="#glxew.h-836"> 836</a>
<a href="#glxew.h-837"> 837</a>
<a href="#glxew.h-838"> 838</a>
<a href="#glxew.h-839"> 839</a>
<a href="#glxew.h-840"> 840</a>
<a href="#glxew.h-841"> 841</a>
<a href="#glxew.h-842"> 842</a>
<a href="#glxew.h-843"> 843</a>
<a href="#glxew.h-844"> 844</a>
<a href="#glxew.h-845"> 845</a>
<a href="#glxew.h-846"> 846</a>
<a href="#glxew.h-847"> 847</a>
<a href="#glxew.h-848"> 848</a>
<a href="#glxew.h-849"> 849</a>
<a href="#glxew.h-850"> 850</a>
<a href="#glxew.h-851"> 851</a>
<a href="#glxew.h-852"> 852</a>
<a href="#glxew.h-853"> 853</a>
<a href="#glxew.h-854"> 854</a>
<a href="#glxew.h-855"> 855</a>
<a href="#glxew.h-856"> 856</a>
<a href="#glxew.h-857"> 857</a>
<a href="#glxew.h-858"> 858</a>
<a href="#glxew.h-859"> 859</a>
<a href="#glxew.h-860"> 860</a>
<a href="#glxew.h-861"> 861</a>
<a href="#glxew.h-862"> 862</a>
<a href="#glxew.h-863"> 863</a>
<a href="#glxew.h-864"> 864</a>
<a href="#glxew.h-865"> 865</a>
<a href="#glxew.h-866"> 866</a>
<a href="#glxew.h-867"> 867</a>
<a href="#glxew.h-868"> 868</a>
<a href="#glxew.h-869"> 869</a>
<a href="#glxew.h-870"> 870</a>
<a href="#glxew.h-871"> 871</a>
<a href="#glxew.h-872"> 872</a>
<a href="#glxew.h-873"> 873</a>
<a href="#glxew.h-874"> 874</a>
<a href="#glxew.h-875"> 875</a>
<a href="#glxew.h-876"> 876</a>
<a href="#glxew.h-877"> 877</a>
<a href="#glxew.h-878"> 878</a>
<a href="#glxew.h-879"> 879</a>
<a href="#glxew.h-880"> 880</a>
<a href="#glxew.h-881"> 881</a>
<a href="#glxew.h-882"> 882</a>
<a href="#glxew.h-883"> 883</a>
<a href="#glxew.h-884"> 884</a>
<a href="#glxew.h-885"> 885</a>
<a href="#glxew.h-886"> 886</a>
<a href="#glxew.h-887"> 887</a>
<a href="#glxew.h-888"> 888</a>
<a href="#glxew.h-889"> 889</a>
<a href="#glxew.h-890"> 890</a>
<a href="#glxew.h-891"> 891</a>
<a href="#glxew.h-892"> 892</a>
<a href="#glxew.h-893"> 893</a>
<a href="#glxew.h-894"> 894</a>
<a href="#glxew.h-895"> 895</a>
<a href="#glxew.h-896"> 896</a>
<a href="#glxew.h-897"> 897</a>
<a href="#glxew.h-898"> 898</a>
<a href="#glxew.h-899"> 899</a>
<a href="#glxew.h-900"> 900</a>
<a href="#glxew.h-901"> 901</a>
<a href="#glxew.h-902"> 902</a>
<a href="#glxew.h-903"> 903</a>
<a href="#glxew.h-904"> 904</a>
<a href="#glxew.h-905"> 905</a>
<a href="#glxew.h-906"> 906</a>
<a href="#glxew.h-907"> 907</a>
<a href="#glxew.h-908"> 908</a>
<a href="#glxew.h-909"> 909</a>
<a href="#glxew.h-910"> 910</a>
<a href="#glxew.h-911"> 911</a>
<a href="#glxew.h-912"> 912</a>
<a href="#glxew.h-913"> 913</a>
<a href="#glxew.h-914"> 914</a>
<a href="#glxew.h-915"> 915</a>
<a href="#glxew.h-916"> 916</a>
<a href="#glxew.h-917"> 917</a>
<a href="#glxew.h-918"> 918</a>
<a href="#glxew.h-919"> 919</a>
<a href="#glxew.h-920"> 920</a>
<a href="#glxew.h-921"> 921</a>
<a href="#glxew.h-922"> 922</a>
<a href="#glxew.h-923"> 923</a>
<a href="#glxew.h-924"> 924</a>
<a href="#glxew.h-925"> 925</a>
<a href="#glxew.h-926"> 926</a>
<a href="#glxew.h-927"> 927</a>
<a href="#glxew.h-928"> 928</a>
<a href="#glxew.h-929"> 929</a>
<a href="#glxew.h-930"> 930</a>
<a href="#glxew.h-931"> 931</a>
<a href="#glxew.h-932"> 932</a>
<a href="#glxew.h-933"> 933</a>
<a href="#glxew.h-934"> 934</a>
<a href="#glxew.h-935"> 935</a>
<a href="#glxew.h-936"> 936</a>
<a href="#glxew.h-937"> 937</a>
<a href="#glxew.h-938"> 938</a>
<a href="#glxew.h-939"> 939</a>
<a href="#glxew.h-940"> 940</a>
<a href="#glxew.h-941"> 941</a>
<a href="#glxew.h-942"> 942</a>
<a href="#glxew.h-943"> 943</a>
<a href="#glxew.h-944"> 944</a>
<a href="#glxew.h-945"> 945</a>
<a href="#glxew.h-946"> 946</a>
<a href="#glxew.h-947"> 947</a>
<a href="#glxew.h-948"> 948</a>
<a href="#glxew.h-949"> 949</a>
<a href="#glxew.h-950"> 950</a>
<a href="#glxew.h-951"> 951</a>
<a href="#glxew.h-952"> 952</a>
<a href="#glxew.h-953"> 953</a>
<a href="#glxew.h-954"> 954</a>
<a href="#glxew.h-955"> 955</a>
<a href="#glxew.h-956"> 956</a>
<a href="#glxew.h-957"> 957</a>
<a href="#glxew.h-958"> 958</a>
<a href="#glxew.h-959"> 959</a>
<a href="#glxew.h-960"> 960</a>
<a href="#glxew.h-961"> 961</a>
<a href="#glxew.h-962"> 962</a>
<a href="#glxew.h-963"> 963</a>
<a href="#glxew.h-964"> 964</a>
<a href="#glxew.h-965"> 965</a>
<a href="#glxew.h-966"> 966</a>
<a href="#glxew.h-967"> 967</a>
<a href="#glxew.h-968"> 968</a>
<a href="#glxew.h-969"> 969</a>
<a href="#glxew.h-970"> 970</a>
<a href="#glxew.h-971"> 971</a>
<a href="#glxew.h-972"> 972</a>
<a href="#glxew.h-973"> 973</a>
<a href="#glxew.h-974"> 974</a>
<a href="#glxew.h-975"> 975</a>
<a href="#glxew.h-976"> 976</a>
<a href="#glxew.h-977"> 977</a>
<a href="#glxew.h-978"> 978</a>
<a href="#glxew.h-979"> 979</a>
<a href="#glxew.h-980"> 980</a>
<a href="#glxew.h-981"> 981</a>
<a href="#glxew.h-982"> 982</a>
<a href="#glxew.h-983"> 983</a>
<a href="#glxew.h-984"> 984</a>
<a href="#glxew.h-985"> 985</a>
<a href="#glxew.h-986"> 986</a>
<a href="#glxew.h-987"> 987</a>
<a href="#glxew.h-988"> 988</a>
<a href="#glxew.h-989"> 989</a>
<a href="#glxew.h-990"> 990</a>
<a href="#glxew.h-991"> 991</a>
<a href="#glxew.h-992"> 992</a>
<a href="#glxew.h-993"> 993</a>
<a href="#glxew.h-994"> 994</a>
<a href="#glxew.h-995"> 995</a>
<a href="#glxew.h-996"> 996</a>
<a href="#glxew.h-997"> 997</a>
<a href="#glxew.h-998"> 998</a>
<a href="#glxew.h-999"> 999</a>
<a href="#glxew.h-1000">1000</a>
<a href="#glxew.h-1001">1001</a>
<a href="#glxew.h-1002">1002</a>
<a href="#glxew.h-1003">1003</a>
<a href="#glxew.h-1004">1004</a>
<a href="#glxew.h-1005">1005</a>
<a href="#glxew.h-1006">1006</a>
<a href="#glxew.h-1007">1007</a>
<a href="#glxew.h-1008">1008</a>
<a href="#glxew.h-1009">1009</a>
<a href="#glxew.h-1010">1010</a>
<a href="#glxew.h-1011">1011</a>
<a href="#glxew.h-1012">1012</a>
<a href="#glxew.h-1013">1013</a>
<a href="#glxew.h-1014">1014</a>
<a href="#glxew.h-1015">1015</a>
<a href="#glxew.h-1016">1016</a>
<a href="#glxew.h-1017">1017</a>
<a href="#glxew.h-1018">1018</a>
<a href="#glxew.h-1019">1019</a>
<a href="#glxew.h-1020">1020</a>
<a href="#glxew.h-1021">1021</a>
<a href="#glxew.h-1022">1022</a>
<a href="#glxew.h-1023">1023</a>
<a href="#glxew.h-1024">1024</a>
<a href="#glxew.h-1025">1025</a>
<a href="#glxew.h-1026">1026</a>
<a href="#glxew.h-1027">1027</a>
<a href="#glxew.h-1028">1028</a>
<a href="#glxew.h-1029">1029</a>
<a href="#glxew.h-1030">1030</a>
<a href="#glxew.h-1031">1031</a>
<a href="#glxew.h-1032">1032</a>
<a href="#glxew.h-1033">1033</a>
<a href="#glxew.h-1034">1034</a>
<a href="#glxew.h-1035">1035</a>
<a href="#glxew.h-1036">1036</a>
<a href="#glxew.h-1037">1037</a>
<a href="#glxew.h-1038">1038</a>
<a href="#glxew.h-1039">1039</a>
<a href="#glxew.h-1040">1040</a>
<a href="#glxew.h-1041">1041</a>
<a href="#glxew.h-1042">1042</a>
<a href="#glxew.h-1043">1043</a>
<a href="#glxew.h-1044">1044</a>
<a href="#glxew.h-1045">1045</a>
<a href="#glxew.h-1046">1046</a>
<a href="#glxew.h-1047">1047</a>
<a href="#glxew.h-1048">1048</a>
<a href="#glxew.h-1049">1049</a>
<a href="#glxew.h-1050">1050</a>
<a href="#glxew.h-1051">1051</a>
<a href="#glxew.h-1052">1052</a>
<a href="#glxew.h-1053">1053</a>
<a href="#glxew.h-1054">1054</a>
<a href="#glxew.h-1055">1055</a>
<a href="#glxew.h-1056">1056</a>
<a href="#glxew.h-1057">1057</a>
<a href="#glxew.h-1058">1058</a>
<a href="#glxew.h-1059">1059</a>
<a href="#glxew.h-1060">1060</a>
<a href="#glxew.h-1061">1061</a>
<a href="#glxew.h-1062">1062</a>
<a href="#glxew.h-1063">1063</a>
<a href="#glxew.h-1064">1064</a>
<a href="#glxew.h-1065">1065</a>
<a href="#glxew.h-1066">1066</a>
<a href="#glxew.h-1067">1067</a>
<a href="#glxew.h-1068">1068</a>
<a href="#glxew.h-1069">1069</a>
<a href="#glxew.h-1070">1070</a>
<a href="#glxew.h-1071">1071</a>
<a href="#glxew.h-1072">1072</a>
<a href="#glxew.h-1073">1073</a>
<a href="#glxew.h-1074">1074</a>
<a href="#glxew.h-1075">1075</a>
<a href="#glxew.h-1076">1076</a>
<a href="#glxew.h-1077">1077</a>
<a href="#glxew.h-1078">1078</a>
<a href="#glxew.h-1079">1079</a>
<a href="#glxew.h-1080">1080</a>
<a href="#glxew.h-1081">1081</a>
<a href="#glxew.h-1082">1082</a>
<a href="#glxew.h-1083">1083</a>
<a href="#glxew.h-1084">1084</a>
<a href="#glxew.h-1085">1085</a>
<a href="#glxew.h-1086">1086</a>
<a href="#glxew.h-1087">1087</a>
<a href="#glxew.h-1088">1088</a>
<a href="#glxew.h-1089">1089</a>
<a href="#glxew.h-1090">1090</a>
<a href="#glxew.h-1091">1091</a>
<a href="#glxew.h-1092">1092</a>
<a href="#glxew.h-1093">1093</a>
<a href="#glxew.h-1094">1094</a>
<a href="#glxew.h-1095">1095</a>
<a href="#glxew.h-1096">1096</a>
<a href="#glxew.h-1097">1097</a>
<a href="#glxew.h-1098">1098</a>
<a href="#glxew.h-1099">1099</a>
<a href="#glxew.h-1100">1100</a>
<a href="#glxew.h-1101">1101</a>
<a href="#glxew.h-1102">1102</a>
<a href="#glxew.h-1103">1103</a>
<a href="#glxew.h-1104">1104</a>
<a href="#glxew.h-1105">1105</a>
<a href="#glxew.h-1106">1106</a>
<a href="#glxew.h-1107">1107</a>
<a href="#glxew.h-1108">1108</a>
<a href="#glxew.h-1109">1109</a>
<a href="#glxew.h-1110">1110</a>
<a href="#glxew.h-1111">1111</a>
<a href="#glxew.h-1112">1112</a>
<a href="#glxew.h-1113">1113</a>
<a href="#glxew.h-1114">1114</a>
<a href="#glxew.h-1115">1115</a>
<a href="#glxew.h-1116">1116</a>
<a href="#glxew.h-1117">1117</a>
<a href="#glxew.h-1118">1118</a>
<a href="#glxew.h-1119">1119</a>
<a href="#glxew.h-1120">1120</a>
<a href="#glxew.h-1121">1121</a>
<a href="#glxew.h-1122">1122</a>
<a href="#glxew.h-1123">1123</a>
<a href="#glxew.h-1124">1124</a>
<a href="#glxew.h-1125">1125</a>
<a href="#glxew.h-1126">1126</a>
<a href="#glxew.h-1127">1127</a>
<a href="#glxew.h-1128">1128</a>
<a href="#glxew.h-1129">1129</a>
<a href="#glxew.h-1130">1130</a>
<a href="#glxew.h-1131">1131</a>
<a href="#glxew.h-1132">1132</a>
<a href="#glxew.h-1133">1133</a>
<a href="#glxew.h-1134">1134</a>
<a href="#glxew.h-1135">1135</a>
<a href="#glxew.h-1136">1136</a>
<a href="#glxew.h-1137">1137</a>
<a href="#glxew.h-1138">1138</a>
<a href="#glxew.h-1139">1139</a>
<a href="#glxew.h-1140">1140</a>
<a href="#glxew.h-1141">1141</a>
<a href="#glxew.h-1142">1142</a>
<a href="#glxew.h-1143">1143</a>
<a href="#glxew.h-1144">1144</a>
<a href="#glxew.h-1145">1145</a>
<a href="#glxew.h-1146">1146</a>
<a href="#glxew.h-1147">1147</a>
<a href="#glxew.h-1148">1148</a>
<a href="#glxew.h-1149">1149</a>
<a href="#glxew.h-1150">1150</a>
<a href="#glxew.h-1151">1151</a>
<a href="#glxew.h-1152">1152</a>
<a href="#glxew.h-1153">1153</a>
<a href="#glxew.h-1154">1154</a>
<a href="#glxew.h-1155">1155</a>
<a href="#glxew.h-1156">1156</a>
<a href="#glxew.h-1157">1157</a>
<a href="#glxew.h-1158">1158</a>
<a href="#glxew.h-1159">1159</a>
<a href="#glxew.h-1160">1160</a>
<a href="#glxew.h-1161">1161</a>
<a href="#glxew.h-1162">1162</a>
<a href="#glxew.h-1163">1163</a>
<a href="#glxew.h-1164">1164</a>
<a href="#glxew.h-1165">1165</a>
<a href="#glxew.h-1166">1166</a>
<a href="#glxew.h-1167">1167</a>
<a href="#glxew.h-1168">1168</a>
<a href="#glxew.h-1169">1169</a>
<a href="#glxew.h-1170">1170</a>
<a href="#glxew.h-1171">1171</a>
<a href="#glxew.h-1172">1172</a>
<a href="#glxew.h-1173">1173</a>
<a href="#glxew.h-1174">1174</a>
<a href="#glxew.h-1175">1175</a>
<a href="#glxew.h-1176">1176</a>
<a href="#glxew.h-1177">1177</a>
<a href="#glxew.h-1178">1178</a>
<a href="#glxew.h-1179">1179</a>
<a href="#glxew.h-1180">1180</a>
<a href="#glxew.h-1181">1181</a>
<a href="#glxew.h-1182">1182</a>
<a href="#glxew.h-1183">1183</a>
<a href="#glxew.h-1184">1184</a>
<a href="#glxew.h-1185">1185</a>
<a href="#glxew.h-1186">1186</a>
<a href="#glxew.h-1187">1187</a>
<a href="#glxew.h-1188">1188</a>
<a href="#glxew.h-1189">1189</a>
<a href="#glxew.h-1190">1190</a>
<a href="#glxew.h-1191">1191</a>
<a href="#glxew.h-1192">1192</a>
<a href="#glxew.h-1193">1193</a>
<a href="#glxew.h-1194">1194</a>
<a href="#glxew.h-1195">1195</a>
<a href="#glxew.h-1196">1196</a>
<a href="#glxew.h-1197">1197</a>
<a href="#glxew.h-1198">1198</a>
<a href="#glxew.h-1199">1199</a>
<a href="#glxew.h-1200">1200</a>
<a href="#glxew.h-1201">1201</a>
<a href="#glxew.h-1202">1202</a>
<a href="#glxew.h-1203">1203</a>
<a href="#glxew.h-1204">1204</a>
<a href="#glxew.h-1205">1205</a>
<a href="#glxew.h-1206">1206</a>
<a href="#glxew.h-1207">1207</a>
<a href="#glxew.h-1208">1208</a>
<a href="#glxew.h-1209">1209</a>
<a href="#glxew.h-1210">1210</a>
<a href="#glxew.h-1211">1211</a>
<a href="#glxew.h-1212">1212</a>
<a href="#glxew.h-1213">1213</a>
<a href="#glxew.h-1214">1214</a>
<a href="#glxew.h-1215">1215</a>
<a href="#glxew.h-1216">1216</a>
<a href="#glxew.h-1217">1217</a>
<a href="#glxew.h-1218">1218</a>
<a href="#glxew.h-1219">1219</a>
<a href="#glxew.h-1220">1220</a>
<a href="#glxew.h-1221">1221</a>
<a href="#glxew.h-1222">1222</a>
<a href="#glxew.h-1223">1223</a>
<a href="#glxew.h-1224">1224</a>
<a href="#glxew.h-1225">1225</a>
<a href="#glxew.h-1226">1226</a>
<a href="#glxew.h-1227">1227</a>
<a href="#glxew.h-1228">1228</a>
<a href="#glxew.h-1229">1229</a>
<a href="#glxew.h-1230">1230</a>
<a href="#glxew.h-1231">1231</a>
<a href="#glxew.h-1232">1232</a>
<a href="#glxew.h-1233">1233</a>
<a href="#glxew.h-1234">1234</a>
<a href="#glxew.h-1235">1235</a>
<a href="#glxew.h-1236">1236</a>
<a href="#glxew.h-1237">1237</a>
<a href="#glxew.h-1238">1238</a>
<a href="#glxew.h-1239">1239</a>
<a href="#glxew.h-1240">1240</a>
<a href="#glxew.h-1241">1241</a>
<a href="#glxew.h-1242">1242</a>
<a href="#glxew.h-1243">1243</a>
<a href="#glxew.h-1244">1244</a>
<a href="#glxew.h-1245">1245</a>
<a href="#glxew.h-1246">1246</a>
<a href="#glxew.h-1247">1247</a>
<a href="#glxew.h-1248">1248</a>
<a href="#glxew.h-1249">1249</a>
<a href="#glxew.h-1250">1250</a>
<a href="#glxew.h-1251">1251</a>
<a href="#glxew.h-1252">1252</a>
<a href="#glxew.h-1253">1253</a>
<a href="#glxew.h-1254">1254</a>
<a href="#glxew.h-1255">1255</a>
<a href="#glxew.h-1256">1256</a>
<a href="#glxew.h-1257">1257</a>
<a href="#glxew.h-1258">1258</a>
<a href="#glxew.h-1259">1259</a>
<a href="#glxew.h-1260">1260</a>
<a href="#glxew.h-1261">1261</a>
<a href="#glxew.h-1262">1262</a>
<a href="#glxew.h-1263">1263</a>
<a href="#glxew.h-1264">1264</a>
<a href="#glxew.h-1265">1265</a>
<a href="#glxew.h-1266">1266</a>
<a href="#glxew.h-1267">1267</a>
<a href="#glxew.h-1268">1268</a>
<a href="#glxew.h-1269">1269</a>
<a href="#glxew.h-1270">1270</a>
<a href="#glxew.h-1271">1271</a>
<a href="#glxew.h-1272">1272</a>
<a href="#glxew.h-1273">1273</a>
<a href="#glxew.h-1274">1274</a>
<a href="#glxew.h-1275">1275</a>
<a href="#glxew.h-1276">1276</a>
<a href="#glxew.h-1277">1277</a>
<a href="#glxew.h-1278">1278</a>
<a href="#glxew.h-1279">1279</a>
<a href="#glxew.h-1280">1280</a>
<a href="#glxew.h-1281">1281</a>
<a href="#glxew.h-1282">1282</a>
<a href="#glxew.h-1283">1283</a>
<a href="#glxew.h-1284">1284</a>
<a href="#glxew.h-1285">1285</a>
<a href="#glxew.h-1286">1286</a>
<a href="#glxew.h-1287">1287</a>
<a href="#glxew.h-1288">1288</a>
<a href="#glxew.h-1289">1289</a>
<a href="#glxew.h-1290">1290</a>
<a href="#glxew.h-1291">1291</a>
<a href="#glxew.h-1292">1292</a>
<a href="#glxew.h-1293">1293</a>
<a href="#glxew.h-1294">1294</a>
<a href="#glxew.h-1295">1295</a>
<a href="#glxew.h-1296">1296</a>
<a href="#glxew.h-1297">1297</a>
<a href="#glxew.h-1298">1298</a>
<a href="#glxew.h-1299">1299</a>
<a href="#glxew.h-1300">1300</a>
<a href="#glxew.h-1301">1301</a>
<a href="#glxew.h-1302">1302</a>
<a href="#glxew.h-1303">1303</a>
<a href="#glxew.h-1304">1304</a>
<a href="#glxew.h-1305">1305</a>
<a href="#glxew.h-1306">1306</a>
<a href="#glxew.h-1307">1307</a>
<a href="#glxew.h-1308">1308</a>
<a href="#glxew.h-1309">1309</a>
<a href="#glxew.h-1310">1310</a>
<a href="#glxew.h-1311">1311</a>
<a href="#glxew.h-1312">1312</a>
<a href="#glxew.h-1313">1313</a>
<a href="#glxew.h-1314">1314</a>
<a href="#glxew.h-1315">1315</a>
<a href="#glxew.h-1316">1316</a>
<a href="#glxew.h-1317">1317</a>
<a href="#glxew.h-1318">1318</a>
<a href="#glxew.h-1319">1319</a>
<a href="#glxew.h-1320">1320</a>
<a href="#glxew.h-1321">1321</a>
<a href="#glxew.h-1322">1322</a>
<a href="#glxew.h-1323">1323</a>
<a href="#glxew.h-1324">1324</a>
<a href="#glxew.h-1325">1325</a>
<a href="#glxew.h-1326">1326</a>
<a href="#glxew.h-1327">1327</a>
<a href="#glxew.h-1328">1328</a>
<a href="#glxew.h-1329">1329</a>
<a href="#glxew.h-1330">1330</a>
<a href="#glxew.h-1331">1331</a>
<a href="#glxew.h-1332">1332</a>
<a href="#glxew.h-1333">1333</a>
<a href="#glxew.h-1334">1334</a>
<a href="#glxew.h-1335">1335</a>
<a href="#glxew.h-1336">1336</a>
<a href="#glxew.h-1337">1337</a>
<a href="#glxew.h-1338">1338</a>
<a href="#glxew.h-1339">1339</a>
<a href="#glxew.h-1340">1340</a>
<a href="#glxew.h-1341">1341</a>
<a href="#glxew.h-1342">1342</a>
<a href="#glxew.h-1343">1343</a>
<a href="#glxew.h-1344">1344</a>
<a href="#glxew.h-1345">1345</a>
<a href="#glxew.h-1346">1346</a>
<a href="#glxew.h-1347">1347</a>
<a href="#glxew.h-1348">1348</a>
<a href="#glxew.h-1349">1349</a>
<a href="#glxew.h-1350">1350</a>
<a href="#glxew.h-1351">1351</a>
<a href="#glxew.h-1352">1352</a>
<a href="#glxew.h-1353">1353</a>
<a href="#glxew.h-1354">1354</a>
<a href="#glxew.h-1355">1355</a>
<a href="#glxew.h-1356">1356</a>
<a href="#glxew.h-1357">1357</a>
<a href="#glxew.h-1358">1358</a>
<a href="#glxew.h-1359">1359</a>
<a href="#glxew.h-1360">1360</a>
<a href="#glxew.h-1361">1361</a>
<a href="#glxew.h-1362">1362</a>
<a href="#glxew.h-1363">1363</a>
<a href="#glxew.h-1364">1364</a>
<a href="#glxew.h-1365">1365</a>
<a href="#glxew.h-1366">1366</a>
<a href="#glxew.h-1367">1367</a>
<a href="#glxew.h-1368">1368</a>
<a href="#glxew.h-1369">1369</a>
<a href="#glxew.h-1370">1370</a>
<a href="#glxew.h-1371">1371</a>
<a href="#glxew.h-1372">1372</a>
<a href="#glxew.h-1373">1373</a>
<a href="#glxew.h-1374">1374</a>
<a href="#glxew.h-1375">1375</a>
<a href="#glxew.h-1376">1376</a>
<a href="#glxew.h-1377">1377</a>
<a href="#glxew.h-1378">1378</a>
<a href="#glxew.h-1379">1379</a>
<a href="#glxew.h-1380">1380</a>
<a href="#glxew.h-1381">1381</a>
<a href="#glxew.h-1382">1382</a>
<a href="#glxew.h-1383">1383</a>
<a href="#glxew.h-1384">1384</a>
<a href="#glxew.h-1385">1385</a>
<a href="#glxew.h-1386">1386</a>
<a href="#glxew.h-1387">1387</a>
<a href="#glxew.h-1388">1388</a>
<a href="#glxew.h-1389">1389</a>
<a href="#glxew.h-1390">1390</a>
<a href="#glxew.h-1391">1391</a>
<a href="#glxew.h-1392">1392</a>
<a href="#glxew.h-1393">1393</a>
<a href="#glxew.h-1394">1394</a>
<a href="#glxew.h-1395">1395</a>
<a href="#glxew.h-1396">1396</a>
<a href="#glxew.h-1397">1397</a>
<a href="#glxew.h-1398">1398</a>
<a href="#glxew.h-1399">1399</a>
<a href="#glxew.h-1400">1400</a>
<a href="#glxew.h-1401">1401</a>
<a href="#glxew.h-1402">1402</a>
<a href="#glxew.h-1403">1403</a>
<a href="#glxew.h-1404">1404</a>
<a href="#glxew.h-1405">1405</a>
<a href="#glxew.h-1406">1406</a>
<a href="#glxew.h-1407">1407</a>
<a href="#glxew.h-1408">1408</a>
<a href="#glxew.h-1409">1409</a>
<a href="#glxew.h-1410">1410</a>
<a href="#glxew.h-1411">1411</a>
<a href="#glxew.h-1412">1412</a>
<a href="#glxew.h-1413">1413</a>
<a href="#glxew.h-1414">1414</a>
<a href="#glxew.h-1415">1415</a>
<a href="#glxew.h-1416">1416</a>
<a href="#glxew.h-1417">1417</a>
<a href="#glxew.h-1418">1418</a>
<a href="#glxew.h-1419">1419</a>
<a href="#glxew.h-1420">1420</a>
<a href="#glxew.h-1421">1421</a>
<a href="#glxew.h-1422">1422</a>
<a href="#glxew.h-1423">1423</a>
<a href="#glxew.h-1424">1424</a>
<a href="#glxew.h-1425">1425</a>
<a href="#glxew.h-1426">1426</a>
<a href="#glxew.h-1427">1427</a>
<a href="#glxew.h-1428">1428</a>
<a href="#glxew.h-1429">1429</a>
<a href="#glxew.h-1430">1430</a>
<a href="#glxew.h-1431">1431</a>
<a href="#glxew.h-1432">1432</a>
<a href="#glxew.h-1433">1433</a>
<a href="#glxew.h-1434">1434</a>
<a href="#glxew.h-1435">1435</a>
<a href="#glxew.h-1436">1436</a>
<a href="#glxew.h-1437">1437</a>
<a href="#glxew.h-1438">1438</a>
<a href="#glxew.h-1439">1439</a>
<a href="#glxew.h-1440">1440</a>
<a href="#glxew.h-1441">1441</a>
<a href="#glxew.h-1442">1442</a>
<a href="#glxew.h-1443">1443</a>
<a href="#glxew.h-1444">1444</a>
<a href="#glxew.h-1445">1445</a>
<a href="#glxew.h-1446">1446</a>
<a href="#glxew.h-1447">1447</a>
<a href="#glxew.h-1448">1448</a>
<a href="#glxew.h-1449">1449</a>
<a href="#glxew.h-1450">1450</a>
<a href="#glxew.h-1451">1451</a>
<a href="#glxew.h-1452">1452</a>
<a href="#glxew.h-1453">1453</a>
<a href="#glxew.h-1454">1454</a>
<a href="#glxew.h-1455">1455</a>
<a href="#glxew.h-1456">1456</a>
<a href="#glxew.h-1457">1457</a>
<a href="#glxew.h-1458">1458</a>
<a href="#glxew.h-1459">1459</a>
<a href="#glxew.h-1460">1460</a>
<a href="#glxew.h-1461">1461</a>
<a href="#glxew.h-1462">1462</a>
<a href="#glxew.h-1463">1463</a>
<a href="#glxew.h-1464">1464</a>
<a href="#glxew.h-1465">1465</a>
<a href="#glxew.h-1466">1466</a>
<a href="#glxew.h-1467">1467</a>
<a href="#glxew.h-1468">1468</a>
<a href="#glxew.h-1469">1469</a>
<a href="#glxew.h-1470">1470</a>
<a href="#glxew.h-1471">1471</a>
<a href="#glxew.h-1472">1472</a>
<a href="#glxew.h-1473">1473</a>
<a href="#glxew.h-1474">1474</a>
<a href="#glxew.h-1475">1475</a>
<a href="#glxew.h-1476">1476</a>
<a href="#glxew.h-1477">1477</a>
<a href="#glxew.h-1478">1478</a>
<a href="#glxew.h-1479">1479</a>
<a href="#glxew.h-1480">1480</a>
<a href="#glxew.h-1481">1481</a>
<a href="#glxew.h-1482">1482</a>
<a href="#glxew.h-1483">1483</a>
<a href="#glxew.h-1484">1484</a>
<a href="#glxew.h-1485">1485</a>
<a href="#glxew.h-1486">1486</a>
<a href="#glxew.h-1487">1487</a>
<a href="#glxew.h-1488">1488</a>
<a href="#glxew.h-1489">1489</a>
<a href="#glxew.h-1490">1490</a>
<a href="#glxew.h-1491">1491</a>
<a href="#glxew.h-1492">1492</a>
<a href="#glxew.h-1493">1493</a>
<a href="#glxew.h-1494">1494</a>
<a href="#glxew.h-1495">1495</a>
<a href="#glxew.h-1496">1496</a>
<a href="#glxew.h-1497">1497</a>
<a href="#glxew.h-1498">1498</a>
<a href="#glxew.h-1499">1499</a>
<a href="#glxew.h-1500">1500</a>
<a href="#glxew.h-1501">1501</a>
<a href="#glxew.h-1502">1502</a>
<a href="#glxew.h-1503">1503</a>
<a href="#glxew.h-1504">1504</a>
<a href="#glxew.h-1505">1505</a>
<a href="#glxew.h-1506">1506</a>
<a href="#glxew.h-1507">1507</a>
<a href="#glxew.h-1508">1508</a>
<a href="#glxew.h-1509">1509</a>
<a href="#glxew.h-1510">1510</a>
<a href="#glxew.h-1511">1511</a>
<a href="#glxew.h-1512">1512</a>
<a href="#glxew.h-1513">1513</a>
<a href="#glxew.h-1514">1514</a>
<a href="#glxew.h-1515">1515</a>
<a href="#glxew.h-1516">1516</a>
<a href="#glxew.h-1517">1517</a>
<a href="#glxew.h-1518">1518</a>
<a href="#glxew.h-1519">1519</a>
<a href="#glxew.h-1520">1520</a>
<a href="#glxew.h-1521">1521</a>
<a href="#glxew.h-1522">1522</a>
<a href="#glxew.h-1523">1523</a>
<a href="#glxew.h-1524">1524</a>
<a href="#glxew.h-1525">1525</a>
<a href="#glxew.h-1526">1526</a>
<a href="#glxew.h-1527">1527</a>
<a href="#glxew.h-1528">1528</a>
<a href="#glxew.h-1529">1529</a>
<a href="#glxew.h-1530">1530</a>
<a href="#glxew.h-1531">1531</a>
<a href="#glxew.h-1532">1532</a>
<a href="#glxew.h-1533">1533</a>
<a href="#glxew.h-1534">1534</a>
<a href="#glxew.h-1535">1535</a>
<a href="#glxew.h-1536">1536</a>
<a href="#glxew.h-1537">1537</a>
<a href="#glxew.h-1538">1538</a>
<a href="#glxew.h-1539">1539</a>
<a href="#glxew.h-1540">1540</a>
<a href="#glxew.h-1541">1541</a>
<a href="#glxew.h-1542">1542</a>
<a href="#glxew.h-1543">1543</a>
<a href="#glxew.h-1544">1544</a>
<a href="#glxew.h-1545">1545</a>
<a href="#glxew.h-1546">1546</a>
<a href="#glxew.h-1547">1547</a>
<a href="#glxew.h-1548">1548</a>
<a href="#glxew.h-1549">1549</a>
<a href="#glxew.h-1550">1550</a>
<a href="#glxew.h-1551">1551</a>
<a href="#glxew.h-1552">1552</a>
<a href="#glxew.h-1553">1553</a>
<a href="#glxew.h-1554">1554</a>
<a href="#glxew.h-1555">1555</a>
<a href="#glxew.h-1556">1556</a>
<a href="#glxew.h-1557">1557</a>
<a href="#glxew.h-1558">1558</a>
<a href="#glxew.h-1559">1559</a>
<a href="#glxew.h-1560">1560</a>
<a href="#glxew.h-1561">1561</a>
<a href="#glxew.h-1562">1562</a>
<a href="#glxew.h-1563">1563</a>
<a href="#glxew.h-1564">1564</a>
<a href="#glxew.h-1565">1565</a>
<a href="#glxew.h-1566">1566</a>
<a href="#glxew.h-1567">1567</a>
<a href="#glxew.h-1568">1568</a>
<a href="#glxew.h-1569">1569</a>
<a href="#glxew.h-1570">1570</a>
<a href="#glxew.h-1571">1571</a>
<a href="#glxew.h-1572">1572</a>
<a href="#glxew.h-1573">1573</a>
<a href="#glxew.h-1574">1574</a>
<a href="#glxew.h-1575">1575</a>
<a href="#glxew.h-1576">1576</a>
<a href="#glxew.h-1577">1577</a>
<a href="#glxew.h-1578">1578</a>
<a href="#glxew.h-1579">1579</a>
<a href="#glxew.h-1580">1580</a>
<a href="#glxew.h-1581">1581</a>
<a href="#glxew.h-1582">1582</a>
<a href="#glxew.h-1583">1583</a>
<a href="#glxew.h-1584">1584</a>
<a href="#glxew.h-1585">1585</a>
<a href="#glxew.h-1586">1586</a>
<a href="#glxew.h-1587">1587</a>
<a href="#glxew.h-1588">1588</a>
<a href="#glxew.h-1589">1589</a>
<a href="#glxew.h-1590">1590</a>
<a href="#glxew.h-1591">1591</a>
<a href="#glxew.h-1592">1592</a>
<a href="#glxew.h-1593">1593</a>
<a href="#glxew.h-1594">1594</a>
<a href="#glxew.h-1595">1595</a>
<a href="#glxew.h-1596">1596</a>
<a href="#glxew.h-1597">1597</a>
<a href="#glxew.h-1598">1598</a>
<a href="#glxew.h-1599">1599</a>
<a href="#glxew.h-1600">1600</a>
<a href="#glxew.h-1601">1601</a>
<a href="#glxew.h-1602">1602</a>
<a href="#glxew.h-1603">1603</a>
<a href="#glxew.h-1604">1604</a>
<a href="#glxew.h-1605">1605</a>
<a href="#glxew.h-1606">1606</a>
<a href="#glxew.h-1607">1607</a>
<a href="#glxew.h-1608">1608</a>
<a href="#glxew.h-1609">1609</a>
<a href="#glxew.h-1610">1610</a>
<a href="#glxew.h-1611">1611</a>
<a href="#glxew.h-1612">1612</a>
<a href="#glxew.h-1613">1613</a>
<a href="#glxew.h-1614">1614</a>
<a href="#glxew.h-1615">1615</a>
<a href="#glxew.h-1616">1616</a>
<a href="#glxew.h-1617">1617</a>
<a href="#glxew.h-1618">1618</a>
<a href="#glxew.h-1619">1619</a>
<a href="#glxew.h-1620">1620</a>
<a href="#glxew.h-1621">1621</a>
<a href="#glxew.h-1622">1622</a>
<a href="#glxew.h-1623">1623</a>
<a href="#glxew.h-1624">1624</a>
<a href="#glxew.h-1625">1625</a>
<a href="#glxew.h-1626">1626</a>
<a href="#glxew.h-1627">1627</a>
<a href="#glxew.h-1628">1628</a>
<a href="#glxew.h-1629">1629</a>
<a href="#glxew.h-1630">1630</a>
<a href="#glxew.h-1631">1631</a>
<a href="#glxew.h-1632">1632</a>
<a href="#glxew.h-1633">1633</a>
<a href="#glxew.h-1634">1634</a>
<a href="#glxew.h-1635">1635</a>
<a href="#glxew.h-1636">1636</a>
<a href="#glxew.h-1637">1637</a>
<a href="#glxew.h-1638">1638</a>
<a href="#glxew.h-1639">1639</a>
<a href="#glxew.h-1640">1640</a>
<a href="#glxew.h-1641">1641</a>
<a href="#glxew.h-1642">1642</a>
<a href="#glxew.h-1643">1643</a>
<a href="#glxew.h-1644">1644</a>
<a href="#glxew.h-1645">1645</a>
<a href="#glxew.h-1646">1646</a>
<a href="#glxew.h-1647">1647</a>
<a href="#glxew.h-1648">1648</a>
<a href="#glxew.h-1649">1649</a>
<a href="#glxew.h-1650">1650</a>
<a href="#glxew.h-1651">1651</a>
<a href="#glxew.h-1652">1652</a>
<a href="#glxew.h-1653">1653</a>
<a href="#glxew.h-1654">1654</a>
<a href="#glxew.h-1655">1655</a>
<a href="#glxew.h-1656">1656</a>
<a href="#glxew.h-1657">1657</a>
<a href="#glxew.h-1658">1658</a>
<a href="#glxew.h-1659">1659</a>
<a href="#glxew.h-1660">1660</a>
<a href="#glxew.h-1661">1661</a>
<a href="#glxew.h-1662">1662</a>
<a href="#glxew.h-1663">1663</a>
<a href="#glxew.h-1664">1664</a>
<a href="#glxew.h-1665">1665</a>
<a href="#glxew.h-1666">1666</a>
<a href="#glxew.h-1667">1667</a>
<a href="#glxew.h-1668">1668</a>
<a href="#glxew.h-1669">1669</a>
<a href="#glxew.h-1670">1670</a>
<a href="#glxew.h-1671">1671</a>
<a href="#glxew.h-1672">1672</a>
<a href="#glxew.h-1673">1673</a>
<a href="#glxew.h-1674">1674</a>
<a href="#glxew.h-1675">1675</a>
<a href="#glxew.h-1676">1676</a>
<a href="#glxew.h-1677">1677</a>
<a href="#glxew.h-1678">1678</a>
<a href="#glxew.h-1679">1679</a>
<a href="#glxew.h-1680">1680</a>
<a href="#glxew.h-1681">1681</a>
<a href="#glxew.h-1682">1682</a>
<a href="#glxew.h-1683">1683</a>
<a href="#glxew.h-1684">1684</a>
<a href="#glxew.h-1685">1685</a>
<a href="#glxew.h-1686">1686</a>
<a href="#glxew.h-1687">1687</a>
<a href="#glxew.h-1688">1688</a>
<a href="#glxew.h-1689">1689</a>
<a href="#glxew.h-1690">1690</a>
<a href="#glxew.h-1691">1691</a>
<a href="#glxew.h-1692">1692</a>
<a href="#glxew.h-1693">1693</a>
<a href="#glxew.h-1694">1694</a>
<a href="#glxew.h-1695">1695</a>
<a href="#glxew.h-1696">1696</a>
<a href="#glxew.h-1697">1697</a>
<a href="#glxew.h-1698">1698</a>
<a href="#glxew.h-1699">1699</a>
<a href="#glxew.h-1700">1700</a>
<a href="#glxew.h-1701">1701</a>
<a href="#glxew.h-1702">1702</a>
<a href="#glxew.h-1703">1703</a>
<a href="#glxew.h-1704">1704</a>
<a href="#glxew.h-1705">1705</a>
<a href="#glxew.h-1706">1706</a>
<a href="#glxew.h-1707">1707</a>
<a href="#glxew.h-1708">1708</a>
<a href="#glxew.h-1709">1709</a>
<a href="#glxew.h-1710">1710</a>
<a href="#glxew.h-1711">1711</a>
<a href="#glxew.h-1712">1712</a>
<a href="#glxew.h-1713">1713</a>
<a href="#glxew.h-1714">1714</a>
<a href="#glxew.h-1715">1715</a>
<a href="#glxew.h-1716">1716</a>
<a href="#glxew.h-1717">1717</a>
<a href="#glxew.h-1718">1718</a>
<a href="#glxew.h-1719">1719</a>
<a href="#glxew.h-1720">1720</a>
<a href="#glxew.h-1721">1721</a>
<a href="#glxew.h-1722">1722</a>
<a href="#glxew.h-1723">1723</a>
<a href="#glxew.h-1724">1724</a>
<a href="#glxew.h-1725">1725</a>
<a href="#glxew.h-1726">1726</a>
<a href="#glxew.h-1727">1727</a>
<a href="#glxew.h-1728">1728</a>
<a href="#glxew.h-1729">1729</a>
<a href="#glxew.h-1730">1730</a>
<a href="#glxew.h-1731">1731</a>
<a href="#glxew.h-1732">1732</a>
<a href="#glxew.h-1733">1733</a>
<a href="#glxew.h-1734">1734</a>
<a href="#glxew.h-1735">1735</a>
<a href="#glxew.h-1736">1736</a>
<a href="#glxew.h-1737">1737</a>
<a href="#glxew.h-1738">1738</a>
<a href="#glxew.h-1739">1739</a>
<a href="#glxew.h-1740">1740</a>
<a href="#glxew.h-1741">1741</a>
<a href="#glxew.h-1742">1742</a>
<a href="#glxew.h-1743">1743</a>
<a href="#glxew.h-1744">1744</a>
<a href="#glxew.h-1745">1745</a>
<a href="#glxew.h-1746">1746</a>
<a href="#glxew.h-1747">1747</a>
<a href="#glxew.h-1748">1748</a>
<a href="#glxew.h-1749">1749</a>
<a href="#glxew.h-1750">1750</a>
<a href="#glxew.h-1751">1751</a>
<a href="#glxew.h-1752">1752</a>
<a href="#glxew.h-1753">1753</a>
<a href="#glxew.h-1754">1754</a>
<a href="#glxew.h-1755">1755</a>
<a href="#glxew.h-1756">1756</a>
<a href="#glxew.h-1757">1757</a>
<a href="#glxew.h-1758">1758</a>
<a href="#glxew.h-1759">1759</a>
<a href="#glxew.h-1760">1760</a>
<a href="#glxew.h-1761">1761</a>
<a href="#glxew.h-1762">1762</a>
<a href="#glxew.h-1763">1763</a>
<a href="#glxew.h-1764">1764</a>
<a href="#glxew.h-1765">1765</a>
<a href="#glxew.h-1766">1766</a>
<a href="#glxew.h-1767">1767</a>
<a href="#glxew.h-1768">1768</a>
<a href="#glxew.h-1769">1769</a>
<a href="#glxew.h-1770">1770</a>
<a href="#glxew.h-1771">1771</a>
<a href="#glxew.h-1772">1772</a>
<a href="#glxew.h-1773">1773</a>
<a href="#glxew.h-1774">1774</a>
<a href="#glxew.h-1775">1775</a></pre></div></td><td class="code"><div class="codehilite highlight"><pre><span></span><a name="glxew.h-1"></a><span class="cm">/*</span>
<a name="glxew.h-2"></a><span class="cm">** The OpenGL Extension Wrangler Library</span>
<a name="glxew.h-3"></a><span class="cm">** Copyright (C) 2008-2017, Nigel Stewart &lt;nigels[]users sourceforge net&gt;</span>
<a name="glxew.h-4"></a><span class="cm">** Copyright (C) 2002-2008, Milan Ikits &lt;milan ikits[]ieee org&gt;</span>
<a name="glxew.h-5"></a><span class="cm">** Copyright (C) 2002-2008, Marcelo E. Magallon &lt;mmagallo[]debian org&gt;</span>
<a name="glxew.h-6"></a><span class="cm">** Copyright (C) 2002, Lev Povalahev</span>
<a name="glxew.h-7"></a><span class="cm">** All rights reserved.</span>
<a name="glxew.h-8"></a><span class="cm">** </span>
<a name="glxew.h-9"></a><span class="cm">** Redistribution and use in source and binary forms, with or without </span>
<a name="glxew.h-10"></a><span class="cm">** modification, are permitted provided that the following conditions are met:</span>
<a name="glxew.h-11"></a><span class="cm">** </span>
<a name="glxew.h-12"></a><span class="cm">** * Redistributions of source code must retain the above copyright notice, </span>
<a name="glxew.h-13"></a><span class="cm">**   this list of conditions and the following disclaimer.</span>
<a name="glxew.h-14"></a><span class="cm">** * Redistributions in binary form must reproduce the above copyright notice, </span>
<a name="glxew.h-15"></a><span class="cm">**   this list of conditions and the following disclaimer in the documentation </span>
<a name="glxew.h-16"></a><span class="cm">**   and/or other materials provided with the distribution.</span>
<a name="glxew.h-17"></a><span class="cm">** * The name of the author may be used to endorse or promote products </span>
<a name="glxew.h-18"></a><span class="cm">**   derived from this software without specific prior written permission.</span>
<a name="glxew.h-19"></a><span class="cm">**</span>
<a name="glxew.h-20"></a><span class="cm">** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; </span>
<a name="glxew.h-21"></a><span class="cm">** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE </span>
<a name="glxew.h-22"></a><span class="cm">** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE</span>
<a name="glxew.h-23"></a><span class="cm">** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE </span>
<a name="glxew.h-24"></a><span class="cm">** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR </span>
<a name="glxew.h-25"></a><span class="cm">** CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF </span>
<a name="glxew.h-26"></a><span class="cm">** SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS</span>
<a name="glxew.h-27"></a><span class="cm">** INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN</span>
<a name="glxew.h-28"></a><span class="cm">** CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)</span>
<a name="glxew.h-29"></a><span class="cm">** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF</span>
<a name="glxew.h-30"></a><span class="cm">** THE POSSIBILITY OF SUCH DAMAGE.</span>
<a name="glxew.h-31"></a><span class="cm">*/</span>
<a name="glxew.h-32"></a>
<a name="glxew.h-33"></a><span class="cm">/*</span>
<a name="glxew.h-34"></a><span class="cm"> * Mesa 3-D graphics library</span>
<a name="glxew.h-35"></a><span class="cm"> * Version:  7.0</span>
<a name="glxew.h-36"></a><span class="cm"> *</span>
<a name="glxew.h-37"></a><span class="cm"> * Copyright (C) 1999-2007  Brian Paul   All Rights Reserved.</span>
<a name="glxew.h-38"></a><span class="cm"> *</span>
<a name="glxew.h-39"></a><span class="cm"> * Permission is hereby granted, free of charge, to any person obtaining a</span>
<a name="glxew.h-40"></a><span class="cm"> * copy of this software and associated documentation files (the &quot;Software&quot;),</span>
<a name="glxew.h-41"></a><span class="cm"> * to deal in the Software without restriction, including without limitation</span>
<a name="glxew.h-42"></a><span class="cm"> * the rights to use, copy, modify, merge, publish, distribute, sublicense,</span>
<a name="glxew.h-43"></a><span class="cm"> * and/or sell copies of the Software, and to permit persons to whom the</span>
<a name="glxew.h-44"></a><span class="cm"> * Software is furnished to do so, subject to the following conditions:</span>
<a name="glxew.h-45"></a><span class="cm"> *</span>
<a name="glxew.h-46"></a><span class="cm"> * The above copyright notice and this permission notice shall be included</span>
<a name="glxew.h-47"></a><span class="cm"> * in all copies or substantial portions of the Software.</span>
<a name="glxew.h-48"></a><span class="cm"> *</span>
<a name="glxew.h-49"></a><span class="cm"> * THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS</span>
<a name="glxew.h-50"></a><span class="cm"> * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,</span>
<a name="glxew.h-51"></a><span class="cm"> * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL</span>
<a name="glxew.h-52"></a><span class="cm"> * BRIAN PAUL BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN</span>
<a name="glxew.h-53"></a><span class="cm"> * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN</span>
<a name="glxew.h-54"></a><span class="cm"> * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</span>
<a name="glxew.h-55"></a><span class="cm"> */</span>
<a name="glxew.h-56"></a>
<a name="glxew.h-57"></a><span class="cm">/*</span>
<a name="glxew.h-58"></a><span class="cm">** Copyright (c) 2007 The Khronos Group Inc.</span>
<a name="glxew.h-59"></a><span class="cm">** </span>
<a name="glxew.h-60"></a><span class="cm">** Permission is hereby granted, free of charge, to any person obtaining a</span>
<a name="glxew.h-61"></a><span class="cm">** copy of this software and/or associated documentation files (the</span>
<a name="glxew.h-62"></a><span class="cm">** &quot;Materials&quot;), to deal in the Materials without restriction, including</span>
<a name="glxew.h-63"></a><span class="cm">** without limitation the rights to use, copy, modify, merge, publish,</span>
<a name="glxew.h-64"></a><span class="cm">** distribute, sublicense, and/or sell copies of the Materials, and to</span>
<a name="glxew.h-65"></a><span class="cm">** permit persons to whom the Materials are furnished to do so, subject to</span>
<a name="glxew.h-66"></a><span class="cm">** the following conditions:</span>
<a name="glxew.h-67"></a><span class="cm">** </span>
<a name="glxew.h-68"></a><span class="cm">** The above copyright notice and this permission notice shall be included</span>
<a name="glxew.h-69"></a><span class="cm">** in all copies or substantial portions of the Materials.</span>
<a name="glxew.h-70"></a><span class="cm">** </span>
<a name="glxew.h-71"></a><span class="cm">** THE MATERIALS ARE PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND,</span>
<a name="glxew.h-72"></a><span class="cm">** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF</span>
<a name="glxew.h-73"></a><span class="cm">** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.</span>
<a name="glxew.h-74"></a><span class="cm">** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY</span>
<a name="glxew.h-75"></a><span class="cm">** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,</span>
<a name="glxew.h-76"></a><span class="cm">** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE</span>
<a name="glxew.h-77"></a><span class="cm">** MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.</span>
<a name="glxew.h-78"></a><span class="cm">*/</span>
<a name="glxew.h-79"></a>
<a name="glxew.h-80"></a><span class="cp">#ifndef __glxew_h__</span>
<a name="glxew.h-81"></a><span class="cp">#define __glxew_h__</span>
<a name="glxew.h-82"></a><span class="cp">#define __GLXEW_H__</span>
<a name="glxew.h-83"></a>
<a name="glxew.h-84"></a><span class="cp">#ifdef __glxext_h_</span>
<a name="glxew.h-85"></a><span class="cp">#error glxext.h included before glxew.h</span>
<a name="glxew.h-86"></a><span class="cp">#endif</span>
<a name="glxew.h-87"></a>
<a name="glxew.h-88"></a><span class="cp">#if defined(GLX_H) || defined(__GLX_glx_h__) || defined(__glx_h__)</span>
<a name="glxew.h-89"></a><span class="cp">#error glx.h included before glxew.h</span>
<a name="glxew.h-90"></a><span class="cp">#endif</span>
<a name="glxew.h-91"></a>
<a name="glxew.h-92"></a><span class="cp">#define __glxext_h_</span>
<a name="glxew.h-93"></a>
<a name="glxew.h-94"></a><span class="cp">#define GLX_H</span>
<a name="glxew.h-95"></a><span class="cp">#define __GLX_glx_h__</span>
<a name="glxew.h-96"></a><span class="cp">#define __glx_h__</span>
<a name="glxew.h-97"></a>
<a name="glxew.h-98"></a><span class="cp">#include</span> <span class="cpf">&lt;X11/Xlib.h&gt;</span><span class="cp"></span>
<a name="glxew.h-99"></a><span class="cp">#include</span> <span class="cpf">&lt;X11/Xutil.h&gt;</span><span class="cp"></span>
<a name="glxew.h-100"></a><span class="cp">#include</span> <span class="cpf">&lt;X11/Xmd.h&gt;</span><span class="cp"></span>
<a name="glxew.h-101"></a><span class="cp">#include</span> <span class="cpf">&lt;GL/glew.h&gt;</span><span class="cp"></span>
<a name="glxew.h-102"></a>
<a name="glxew.h-103"></a><span class="cp">#ifdef __cplusplus</span>
<a name="glxew.h-104"></a><span class="k">extern</span> <span class="s">&quot;C&quot;</span> <span class="p">{</span>
<a name="glxew.h-105"></a><span class="cp">#endif</span>
<a name="glxew.h-106"></a>
<a name="glxew.h-107"></a><span class="cm">/* ---------------------------- GLX_VERSION_1_0 --------------------------- */</span>
<a name="glxew.h-108"></a>
<a name="glxew.h-109"></a><span class="cp">#ifndef GLX_VERSION_1_0</span>
<a name="glxew.h-110"></a><span class="cp">#define GLX_VERSION_1_0 1</span>
<a name="glxew.h-111"></a>
<a name="glxew.h-112"></a><span class="cp">#define GLX_USE_GL 1</span>
<a name="glxew.h-113"></a><span class="cp">#define GLX_BUFFER_SIZE 2</span>
<a name="glxew.h-114"></a><span class="cp">#define GLX_LEVEL 3</span>
<a name="glxew.h-115"></a><span class="cp">#define GLX_RGBA 4</span>
<a name="glxew.h-116"></a><span class="cp">#define GLX_DOUBLEBUFFER 5</span>
<a name="glxew.h-117"></a><span class="cp">#define GLX_STEREO 6</span>
<a name="glxew.h-118"></a><span class="cp">#define GLX_AUX_BUFFERS 7</span>
<a name="glxew.h-119"></a><span class="cp">#define GLX_RED_SIZE 8</span>
<a name="glxew.h-120"></a><span class="cp">#define GLX_GREEN_SIZE 9</span>
<a name="glxew.h-121"></a><span class="cp">#define GLX_BLUE_SIZE 10</span>
<a name="glxew.h-122"></a><span class="cp">#define GLX_ALPHA_SIZE 11</span>
<a name="glxew.h-123"></a><span class="cp">#define GLX_DEPTH_SIZE 12</span>
<a name="glxew.h-124"></a><span class="cp">#define GLX_STENCIL_SIZE 13</span>
<a name="glxew.h-125"></a><span class="cp">#define GLX_ACCUM_RED_SIZE 14</span>
<a name="glxew.h-126"></a><span class="cp">#define GLX_ACCUM_GREEN_SIZE 15</span>
<a name="glxew.h-127"></a><span class="cp">#define GLX_ACCUM_BLUE_SIZE 16</span>
<a name="glxew.h-128"></a><span class="cp">#define GLX_ACCUM_ALPHA_SIZE 17</span>
<a name="glxew.h-129"></a><span class="cp">#define GLX_BAD_SCREEN 1</span>
<a name="glxew.h-130"></a><span class="cp">#define GLX_BAD_ATTRIBUTE 2</span>
<a name="glxew.h-131"></a><span class="cp">#define GLX_NO_EXTENSION 3</span>
<a name="glxew.h-132"></a><span class="cp">#define GLX_BAD_VISUAL 4</span>
<a name="glxew.h-133"></a><span class="cp">#define GLX_BAD_CONTEXT 5</span>
<a name="glxew.h-134"></a><span class="cp">#define GLX_BAD_VALUE 6</span>
<a name="glxew.h-135"></a><span class="cp">#define GLX_BAD_ENUM 7</span>
<a name="glxew.h-136"></a>
<a name="glxew.h-137"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXDrawable</span><span class="p">;</span>
<a name="glxew.h-138"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXPixmap</span><span class="p">;</span>
<a name="glxew.h-139"></a><span class="cp">#ifdef __sun</span>
<a name="glxew.h-140"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="n">__glXContextRec</span> <span class="o">*</span><span class="n">GLXContext</span><span class="p">;</span>
<a name="glxew.h-141"></a><span class="cp">#else</span>
<a name="glxew.h-142"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="n">__GLXcontextRec</span> <span class="o">*</span><span class="n">GLXContext</span><span class="p">;</span>
<a name="glxew.h-143"></a><span class="cp">#endif</span>
<a name="glxew.h-144"></a>
<a name="glxew.h-145"></a><span class="k">typedef</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">GLXVideoDeviceNV</span><span class="p">;</span> 
<a name="glxew.h-146"></a>
<a name="glxew.h-147"></a><span class="k">extern</span> <span class="n">Bool</span> <span class="nf">glXQueryExtension</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">errorBase</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">eventBase</span><span class="p">);</span>
<a name="glxew.h-148"></a><span class="k">extern</span> <span class="n">Bool</span> <span class="nf">glXQueryVersion</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">major</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">minor</span><span class="p">);</span>
<a name="glxew.h-149"></a><span class="k">extern</span> <span class="kt">int</span> <span class="nf">glXGetConfig</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">XVisualInfo</span> <span class="o">*</span><span class="n">vis</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attrib</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-150"></a><span class="k">extern</span> <span class="n">XVisualInfo</span><span class="o">*</span> <span class="nf">glXChooseVisual</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attribList</span><span class="p">);</span>
<a name="glxew.h-151"></a><span class="k">extern</span> <span class="n">GLXPixmap</span> <span class="nf">glXCreateGLXPixmap</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">XVisualInfo</span> <span class="o">*</span><span class="n">vis</span><span class="p">,</span> <span class="n">Pixmap</span> <span class="n">pixmap</span><span class="p">);</span>
<a name="glxew.h-152"></a><span class="k">extern</span> <span class="kt">void</span> <span class="nf">glXDestroyGLXPixmap</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPixmap</span> <span class="n">pix</span><span class="p">);</span>
<a name="glxew.h-153"></a><span class="k">extern</span> <span class="n">GLXContext</span> <span class="nf">glXCreateContext</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">XVisualInfo</span> <span class="o">*</span><span class="n">vis</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">shareList</span><span class="p">,</span> <span class="n">Bool</span> <span class="n">direct</span><span class="p">);</span>
<a name="glxew.h-154"></a><span class="k">extern</span> <span class="kt">void</span> <span class="nf">glXDestroyContext</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-155"></a><span class="k">extern</span> <span class="n">Bool</span> <span class="nf">glXIsDirect</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-156"></a><span class="k">extern</span> <span class="kt">void</span> <span class="nf">glXCopyContext</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">src</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">dst</span><span class="p">,</span> <span class="n">GLulong</span> <span class="n">mask</span><span class="p">);</span>
<a name="glxew.h-157"></a><span class="k">extern</span> <span class="n">Bool</span> <span class="nf">glXMakeCurrent</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-158"></a><span class="k">extern</span> <span class="n">GLXContext</span> <span class="nf">glXGetCurrentContext</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-159"></a><span class="k">extern</span> <span class="n">GLXDrawable</span> <span class="nf">glXGetCurrentDrawable</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-160"></a><span class="k">extern</span> <span class="kt">void</span> <span class="nf">glXWaitGL</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-161"></a><span class="k">extern</span> <span class="kt">void</span> <span class="nf">glXWaitX</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-162"></a><span class="k">extern</span> <span class="kt">void</span> <span class="nf">glXSwapBuffers</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">);</span>
<a name="glxew.h-163"></a><span class="k">extern</span> <span class="kt">void</span> <span class="nf">glXUseXFont</span> <span class="p">(</span><span class="n">Font</span> <span class="n">font</span><span class="p">,</span> <span class="kt">int</span> <span class="n">first</span><span class="p">,</span> <span class="kt">int</span> <span class="n">count</span><span class="p">,</span> <span class="kt">int</span> <span class="n">listBase</span><span class="p">);</span>
<a name="glxew.h-164"></a>
<a name="glxew.h-165"></a><span class="cp">#define GLXEW_VERSION_1_0 GLXEW_GET_VAR(__GLXEW_VERSION_1_0)</span>
<a name="glxew.h-166"></a>
<a name="glxew.h-167"></a><span class="cp">#endif </span><span class="cm">/* GLX_VERSION_1_0 */</span><span class="cp"></span>
<a name="glxew.h-168"></a>
<a name="glxew.h-169"></a><span class="cm">/* ---------------------------- GLX_VERSION_1_1 --------------------------- */</span>
<a name="glxew.h-170"></a>
<a name="glxew.h-171"></a><span class="cp">#ifndef GLX_VERSION_1_1</span>
<a name="glxew.h-172"></a><span class="cp">#define GLX_VERSION_1_1</span>
<a name="glxew.h-173"></a>
<a name="glxew.h-174"></a><span class="cp">#define GLX_VENDOR 0x1</span>
<a name="glxew.h-175"></a><span class="cp">#define GLX_VERSION 0x2</span>
<a name="glxew.h-176"></a><span class="cp">#define GLX_EXTENSIONS 0x3</span>
<a name="glxew.h-177"></a>
<a name="glxew.h-178"></a><span class="k">extern</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="nf">glXQueryExtensionsString</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">);</span>
<a name="glxew.h-179"></a><span class="k">extern</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="nf">glXGetClientString</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">name</span><span class="p">);</span>
<a name="glxew.h-180"></a><span class="k">extern</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="nf">glXQueryServerString</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">name</span><span class="p">);</span>
<a name="glxew.h-181"></a>
<a name="glxew.h-182"></a><span class="cp">#define GLXEW_VERSION_1_1 GLXEW_GET_VAR(__GLXEW_VERSION_1_1)</span>
<a name="glxew.h-183"></a>
<a name="glxew.h-184"></a><span class="cp">#endif </span><span class="cm">/* GLX_VERSION_1_1 */</span><span class="cp"></span>
<a name="glxew.h-185"></a>
<a name="glxew.h-186"></a><span class="cm">/* ---------------------------- GLX_VERSION_1_2 ---------------------------- */</span>
<a name="glxew.h-187"></a>
<a name="glxew.h-188"></a><span class="cp">#ifndef GLX_VERSION_1_2</span>
<a name="glxew.h-189"></a><span class="cp">#define GLX_VERSION_1_2 1</span>
<a name="glxew.h-190"></a>
<a name="glxew.h-191"></a><span class="k">typedef</span> <span class="n">Display</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETCURRENTDISPLAYPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-192"></a>
<a name="glxew.h-193"></a><span class="cp">#define glXGetCurrentDisplay GLXEW_GET_FUN(__glewXGetCurrentDisplay)</span>
<a name="glxew.h-194"></a>
<a name="glxew.h-195"></a><span class="cp">#define GLXEW_VERSION_1_2 GLXEW_GET_VAR(__GLXEW_VERSION_1_2)</span>
<a name="glxew.h-196"></a>
<a name="glxew.h-197"></a><span class="cp">#endif </span><span class="cm">/* GLX_VERSION_1_2 */</span><span class="cp"></span>
<a name="glxew.h-198"></a>
<a name="glxew.h-199"></a><span class="cm">/* ---------------------------- GLX_VERSION_1_3 ---------------------------- */</span>
<a name="glxew.h-200"></a>
<a name="glxew.h-201"></a><span class="cp">#ifndef GLX_VERSION_1_3</span>
<a name="glxew.h-202"></a><span class="cp">#define GLX_VERSION_1_3 1</span>
<a name="glxew.h-203"></a>
<a name="glxew.h-204"></a><span class="cp">#define GLX_FRONT_LEFT_BUFFER_BIT 0x00000001</span>
<a name="glxew.h-205"></a><span class="cp">#define GLX_RGBA_BIT 0x00000001</span>
<a name="glxew.h-206"></a><span class="cp">#define GLX_WINDOW_BIT 0x00000001</span>
<a name="glxew.h-207"></a><span class="cp">#define GLX_COLOR_INDEX_BIT 0x00000002</span>
<a name="glxew.h-208"></a><span class="cp">#define GLX_FRONT_RIGHT_BUFFER_BIT 0x00000002</span>
<a name="glxew.h-209"></a><span class="cp">#define GLX_PIXMAP_BIT 0x00000002</span>
<a name="glxew.h-210"></a><span class="cp">#define GLX_BACK_LEFT_BUFFER_BIT 0x00000004</span>
<a name="glxew.h-211"></a><span class="cp">#define GLX_PBUFFER_BIT 0x00000004</span>
<a name="glxew.h-212"></a><span class="cp">#define GLX_BACK_RIGHT_BUFFER_BIT 0x00000008</span>
<a name="glxew.h-213"></a><span class="cp">#define GLX_AUX_BUFFERS_BIT 0x00000010</span>
<a name="glxew.h-214"></a><span class="cp">#define GLX_CONFIG_CAVEAT 0x20</span>
<a name="glxew.h-215"></a><span class="cp">#define GLX_DEPTH_BUFFER_BIT 0x00000020</span>
<a name="glxew.h-216"></a><span class="cp">#define GLX_X_VISUAL_TYPE 0x22</span>
<a name="glxew.h-217"></a><span class="cp">#define GLX_TRANSPARENT_TYPE 0x23</span>
<a name="glxew.h-218"></a><span class="cp">#define GLX_TRANSPARENT_INDEX_VALUE 0x24</span>
<a name="glxew.h-219"></a><span class="cp">#define GLX_TRANSPARENT_RED_VALUE 0x25</span>
<a name="glxew.h-220"></a><span class="cp">#define GLX_TRANSPARENT_GREEN_VALUE 0x26</span>
<a name="glxew.h-221"></a><span class="cp">#define GLX_TRANSPARENT_BLUE_VALUE 0x27</span>
<a name="glxew.h-222"></a><span class="cp">#define GLX_TRANSPARENT_ALPHA_VALUE 0x28</span>
<a name="glxew.h-223"></a><span class="cp">#define GLX_STENCIL_BUFFER_BIT 0x00000040</span>
<a name="glxew.h-224"></a><span class="cp">#define GLX_ACCUM_BUFFER_BIT 0x00000080</span>
<a name="glxew.h-225"></a><span class="cp">#define GLX_NONE 0x8000</span>
<a name="glxew.h-226"></a><span class="cp">#define GLX_SLOW_CONFIG 0x8001</span>
<a name="glxew.h-227"></a><span class="cp">#define GLX_TRUE_COLOR 0x8002</span>
<a name="glxew.h-228"></a><span class="cp">#define GLX_DIRECT_COLOR 0x8003</span>
<a name="glxew.h-229"></a><span class="cp">#define GLX_PSEUDO_COLOR 0x8004</span>
<a name="glxew.h-230"></a><span class="cp">#define GLX_STATIC_COLOR 0x8005</span>
<a name="glxew.h-231"></a><span class="cp">#define GLX_GRAY_SCALE 0x8006</span>
<a name="glxew.h-232"></a><span class="cp">#define GLX_STATIC_GRAY 0x8007</span>
<a name="glxew.h-233"></a><span class="cp">#define GLX_TRANSPARENT_RGB 0x8008</span>
<a name="glxew.h-234"></a><span class="cp">#define GLX_TRANSPARENT_INDEX 0x8009</span>
<a name="glxew.h-235"></a><span class="cp">#define GLX_VISUAL_ID 0x800B</span>
<a name="glxew.h-236"></a><span class="cp">#define GLX_SCREEN 0x800C</span>
<a name="glxew.h-237"></a><span class="cp">#define GLX_NON_CONFORMANT_CONFIG 0x800D</span>
<a name="glxew.h-238"></a><span class="cp">#define GLX_DRAWABLE_TYPE 0x8010</span>
<a name="glxew.h-239"></a><span class="cp">#define GLX_RENDER_TYPE 0x8011</span>
<a name="glxew.h-240"></a><span class="cp">#define GLX_X_RENDERABLE 0x8012</span>
<a name="glxew.h-241"></a><span class="cp">#define GLX_FBCONFIG_ID 0x8013</span>
<a name="glxew.h-242"></a><span class="cp">#define GLX_RGBA_TYPE 0x8014</span>
<a name="glxew.h-243"></a><span class="cp">#define GLX_COLOR_INDEX_TYPE 0x8015</span>
<a name="glxew.h-244"></a><span class="cp">#define GLX_MAX_PBUFFER_WIDTH 0x8016</span>
<a name="glxew.h-245"></a><span class="cp">#define GLX_MAX_PBUFFER_HEIGHT 0x8017</span>
<a name="glxew.h-246"></a><span class="cp">#define GLX_MAX_PBUFFER_PIXELS 0x8018</span>
<a name="glxew.h-247"></a><span class="cp">#define GLX_PRESERVED_CONTENTS 0x801B</span>
<a name="glxew.h-248"></a><span class="cp">#define GLX_LARGEST_PBUFFER 0x801C</span>
<a name="glxew.h-249"></a><span class="cp">#define GLX_WIDTH 0x801D</span>
<a name="glxew.h-250"></a><span class="cp">#define GLX_HEIGHT 0x801E</span>
<a name="glxew.h-251"></a><span class="cp">#define GLX_EVENT_MASK 0x801F</span>
<a name="glxew.h-252"></a><span class="cp">#define GLX_DAMAGED 0x8020</span>
<a name="glxew.h-253"></a><span class="cp">#define GLX_SAVED 0x8021</span>
<a name="glxew.h-254"></a><span class="cp">#define GLX_WINDOW 0x8022</span>
<a name="glxew.h-255"></a><span class="cp">#define GLX_PBUFFER 0x8023</span>
<a name="glxew.h-256"></a><span class="cp">#define GLX_PBUFFER_HEIGHT 0x8040</span>
<a name="glxew.h-257"></a><span class="cp">#define GLX_PBUFFER_WIDTH 0x8041</span>
<a name="glxew.h-258"></a><span class="cp">#define GLX_PBUFFER_CLOBBER_MASK 0x08000000</span>
<a name="glxew.h-259"></a><span class="cp">#define GLX_DONT_CARE 0xFFFFFFFF</span>
<a name="glxew.h-260"></a>
<a name="glxew.h-261"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXFBConfigID</span><span class="p">;</span>
<a name="glxew.h-262"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXPbuffer</span><span class="p">;</span>
<a name="glxew.h-263"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXWindow</span><span class="p">;</span>
<a name="glxew.h-264"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="n">__GLXFBConfigRec</span> <span class="o">*</span><span class="n">GLXFBConfig</span><span class="p">;</span>
<a name="glxew.h-265"></a>
<a name="glxew.h-266"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="p">{</span>
<a name="glxew.h-267"></a>  <span class="kt">int</span> <span class="n">event_type</span><span class="p">;</span> 
<a name="glxew.h-268"></a>  <span class="kt">int</span> <span class="n">draw_type</span><span class="p">;</span> 
<a name="glxew.h-269"></a>  <span class="kt">unsigned</span> <span class="kt">long</span> <span class="n">serial</span><span class="p">;</span> 
<a name="glxew.h-270"></a>  <span class="n">Bool</span> <span class="n">send_event</span><span class="p">;</span> 
<a name="glxew.h-271"></a>  <span class="n">Display</span> <span class="o">*</span><span class="n">display</span><span class="p">;</span> 
<a name="glxew.h-272"></a>  <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">;</span> 
<a name="glxew.h-273"></a>  <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">buffer_mask</span><span class="p">;</span> 
<a name="glxew.h-274"></a>  <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">aux_buffer</span><span class="p">;</span> 
<a name="glxew.h-275"></a>  <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="n">y</span><span class="p">;</span> 
<a name="glxew.h-276"></a>  <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="n">height</span><span class="p">;</span> 
<a name="glxew.h-277"></a>  <span class="kt">int</span> <span class="n">count</span><span class="p">;</span> 
<a name="glxew.h-278"></a><span class="p">}</span> <span class="n">GLXPbufferClobberEvent</span><span class="p">;</span>
<a name="glxew.h-279"></a><span class="k">typedef</span> <span class="k">union</span> <span class="n">__GLXEvent</span> <span class="p">{</span>
<a name="glxew.h-280"></a>  <span class="n">GLXPbufferClobberEvent</span> <span class="n">glxpbufferclobber</span><span class="p">;</span> 
<a name="glxew.h-281"></a>  <span class="kt">long</span> <span class="n">pad</span><span class="p">[</span><span class="mi">24</span><span class="p">];</span> 
<a name="glxew.h-282"></a><span class="p">}</span> <span class="n">GLXEvent</span><span class="p">;</span>
<a name="glxew.h-283"></a>
<a name="glxew.h-284"></a><span class="k">typedef</span> <span class="n">GLXFBConfig</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCHOOSEFBCONFIGPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">nelements</span><span class="p">);</span>
<a name="glxew.h-285"></a><span class="k">typedef</span> <span class="nf">GLXContext</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATENEWCONTEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="kt">int</span> <span class="n">render_type</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">share_list</span><span class="p">,</span> <span class="n">Bool</span> <span class="n">direct</span><span class="p">);</span>
<a name="glxew.h-286"></a><span class="k">typedef</span> <span class="nf">GLXPbuffer</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEPBUFFERPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-287"></a><span class="k">typedef</span> <span class="nf">GLXPixmap</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEPIXMAPPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="n">Pixmap</span> <span class="n">pixmap</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-288"></a><span class="k">typedef</span> <span class="nf">GLXWindow</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEWINDOWPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="n">Window</span> <span class="n">win</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-289"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDESTROYPBUFFERPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">);</span>
<a name="glxew.h-290"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDESTROYPIXMAPPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPixmap</span> <span class="n">pixmap</span><span class="p">);</span>
<a name="glxew.h-291"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDESTROYWINDOWPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXWindow</span> <span class="n">win</span><span class="p">);</span>
<a name="glxew.h-292"></a><span class="k">typedef</span> <span class="nf">GLXDrawable</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETCURRENTREADDRAWABLEPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-293"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETFBCONFIGATTRIBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-294"></a><span class="k">typedef</span> <span class="n">GLXFBConfig</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETFBCONFIGSPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">nelements</span><span class="p">);</span>
<a name="glxew.h-295"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETSELECTEDEVENTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">draw</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="o">*</span><span class="n">event_mask</span><span class="p">);</span>
<a name="glxew.h-296"></a><span class="k">typedef</span> <span class="n">XVisualInfo</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETVISUALFROMFBCONFIGPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">);</span>
<a name="glxew.h-297"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXMAKECONTEXTCURRENTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">display</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">draw</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">read</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-298"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYCONTEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-299"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYDRAWABLEPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">draw</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-300"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSELECTEVENTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">draw</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="n">event_mask</span><span class="p">);</span>
<a name="glxew.h-301"></a>
<a name="glxew.h-302"></a><span class="cp">#define glXChooseFBConfig GLXEW_GET_FUN(__glewXChooseFBConfig)</span>
<a name="glxew.h-303"></a><span class="cp">#define glXCreateNewContext GLXEW_GET_FUN(__glewXCreateNewContext)</span>
<a name="glxew.h-304"></a><span class="cp">#define glXCreatePbuffer GLXEW_GET_FUN(__glewXCreatePbuffer)</span>
<a name="glxew.h-305"></a><span class="cp">#define glXCreatePixmap GLXEW_GET_FUN(__glewXCreatePixmap)</span>
<a name="glxew.h-306"></a><span class="cp">#define glXCreateWindow GLXEW_GET_FUN(__glewXCreateWindow)</span>
<a name="glxew.h-307"></a><span class="cp">#define glXDestroyPbuffer GLXEW_GET_FUN(__glewXDestroyPbuffer)</span>
<a name="glxew.h-308"></a><span class="cp">#define glXDestroyPixmap GLXEW_GET_FUN(__glewXDestroyPixmap)</span>
<a name="glxew.h-309"></a><span class="cp">#define glXDestroyWindow GLXEW_GET_FUN(__glewXDestroyWindow)</span>
<a name="glxew.h-310"></a><span class="cp">#define glXGetCurrentReadDrawable GLXEW_GET_FUN(__glewXGetCurrentReadDrawable)</span>
<a name="glxew.h-311"></a><span class="cp">#define glXGetFBConfigAttrib GLXEW_GET_FUN(__glewXGetFBConfigAttrib)</span>
<a name="glxew.h-312"></a><span class="cp">#define glXGetFBConfigs GLXEW_GET_FUN(__glewXGetFBConfigs)</span>
<a name="glxew.h-313"></a><span class="cp">#define glXGetSelectedEvent GLXEW_GET_FUN(__glewXGetSelectedEvent)</span>
<a name="glxew.h-314"></a><span class="cp">#define glXGetVisualFromFBConfig GLXEW_GET_FUN(__glewXGetVisualFromFBConfig)</span>
<a name="glxew.h-315"></a><span class="cp">#define glXMakeContextCurrent GLXEW_GET_FUN(__glewXMakeContextCurrent)</span>
<a name="glxew.h-316"></a><span class="cp">#define glXQueryContext GLXEW_GET_FUN(__glewXQueryContext)</span>
<a name="glxew.h-317"></a><span class="cp">#define glXQueryDrawable GLXEW_GET_FUN(__glewXQueryDrawable)</span>
<a name="glxew.h-318"></a><span class="cp">#define glXSelectEvent GLXEW_GET_FUN(__glewXSelectEvent)</span>
<a name="glxew.h-319"></a>
<a name="glxew.h-320"></a><span class="cp">#define GLXEW_VERSION_1_3 GLXEW_GET_VAR(__GLXEW_VERSION_1_3)</span>
<a name="glxew.h-321"></a>
<a name="glxew.h-322"></a><span class="cp">#endif </span><span class="cm">/* GLX_VERSION_1_3 */</span><span class="cp"></span>
<a name="glxew.h-323"></a>
<a name="glxew.h-324"></a><span class="cm">/* ---------------------------- GLX_VERSION_1_4 ---------------------------- */</span>
<a name="glxew.h-325"></a>
<a name="glxew.h-326"></a><span class="cp">#ifndef GLX_VERSION_1_4</span>
<a name="glxew.h-327"></a><span class="cp">#define GLX_VERSION_1_4 1</span>
<a name="glxew.h-328"></a>
<a name="glxew.h-329"></a><span class="cp">#define GLX_SAMPLE_BUFFERS 100000</span>
<a name="glxew.h-330"></a><span class="cp">#define GLX_SAMPLES 100001</span>
<a name="glxew.h-331"></a>
<a name="glxew.h-332"></a><span class="k">extern</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">glXGetProcAddress</span> <span class="p">(</span><span class="k">const</span> <span class="n">GLubyte</span> <span class="o">*</span><span class="n">procName</span><span class="p">))</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-333"></a>
<a name="glxew.h-334"></a><span class="cp">#define GLXEW_VERSION_1_4 GLXEW_GET_VAR(__GLXEW_VERSION_1_4)</span>
<a name="glxew.h-335"></a>
<a name="glxew.h-336"></a><span class="cp">#endif </span><span class="cm">/* GLX_VERSION_1_4 */</span><span class="cp"></span>
<a name="glxew.h-337"></a>
<a name="glxew.h-338"></a><span class="cm">/* -------------------------- GLX_3DFX_multisample ------------------------- */</span>
<a name="glxew.h-339"></a>
<a name="glxew.h-340"></a><span class="cp">#ifndef GLX_3DFX_multisample</span>
<a name="glxew.h-341"></a><span class="cp">#define GLX_3DFX_multisample 1</span>
<a name="glxew.h-342"></a>
<a name="glxew.h-343"></a><span class="cp">#define GLX_SAMPLE_BUFFERS_3DFX 0x8050</span>
<a name="glxew.h-344"></a><span class="cp">#define GLX_SAMPLES_3DFX 0x8051</span>
<a name="glxew.h-345"></a>
<a name="glxew.h-346"></a><span class="cp">#define GLXEW_3DFX_multisample GLXEW_GET_VAR(__GLXEW_3DFX_multisample)</span>
<a name="glxew.h-347"></a>
<a name="glxew.h-348"></a><span class="cp">#endif </span><span class="cm">/* GLX_3DFX_multisample */</span><span class="cp"></span>
<a name="glxew.h-349"></a>
<a name="glxew.h-350"></a><span class="cm">/* ------------------------ GLX_AMD_gpu_association ------------------------ */</span>
<a name="glxew.h-351"></a>
<a name="glxew.h-352"></a><span class="cp">#ifndef GLX_AMD_gpu_association</span>
<a name="glxew.h-353"></a><span class="cp">#define GLX_AMD_gpu_association 1</span>
<a name="glxew.h-354"></a>
<a name="glxew.h-355"></a><span class="cp">#define GLX_GPU_VENDOR_AMD 0x1F00</span>
<a name="glxew.h-356"></a><span class="cp">#define GLX_GPU_RENDERER_STRING_AMD 0x1F01</span>
<a name="glxew.h-357"></a><span class="cp">#define GLX_GPU_OPENGL_VERSION_STRING_AMD 0x1F02</span>
<a name="glxew.h-358"></a><span class="cp">#define GLX_GPU_FASTEST_TARGET_GPUS_AMD 0x21A2</span>
<a name="glxew.h-359"></a><span class="cp">#define GLX_GPU_RAM_AMD 0x21A3</span>
<a name="glxew.h-360"></a><span class="cp">#define GLX_GPU_CLOCK_AMD 0x21A4</span>
<a name="glxew.h-361"></a><span class="cp">#define GLX_GPU_NUM_PIPES_AMD 0x21A5</span>
<a name="glxew.h-362"></a><span class="cp">#define GLX_GPU_NUM_SIMD_AMD 0x21A6</span>
<a name="glxew.h-363"></a><span class="cp">#define GLX_GPU_NUM_RB_AMD 0x21A7</span>
<a name="glxew.h-364"></a><span class="cp">#define GLX_GPU_NUM_SPI_AMD 0x21A8</span>
<a name="glxew.h-365"></a>
<a name="glxew.h-366"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBLITCONTEXTFRAMEBUFFERAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLXContext</span> <span class="n">dstCtx</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcX0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcY0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcX1</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcY1</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstX0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstY0</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstX1</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstY1</span><span class="p">,</span> <span class="n">GLbitfield</span> <span class="n">mask</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">filter</span><span class="p">);</span>
<a name="glxew.h-367"></a><span class="k">typedef</span> <span class="nf">GLXContext</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEASSOCIATEDCONTEXTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">id</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">share_list</span><span class="p">);</span>
<a name="glxew.h-368"></a><span class="k">typedef</span> <span class="nf">GLXContext</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEASSOCIATEDCONTEXTATTRIBSAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">id</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">share_context</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span><span class="o">*</span> <span class="n">attribList</span><span class="p">);</span>
<a name="glxew.h-369"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDELETEASSOCIATEDCONTEXTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-370"></a><span class="k">typedef</span> <span class="kt">unsigned</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETCONTEXTGPUIDAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-371"></a><span class="k">typedef</span> <span class="nf">GLXContext</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETCURRENTASSOCIATEDCONTEXTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-372"></a><span class="k">typedef</span> <span class="kt">unsigned</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETGPUIDSAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">maxCount</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span><span class="o">*</span> <span class="n">ids</span><span class="p">);</span>
<a name="glxew.h-373"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETGPUINFOAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">id</span><span class="p">,</span> <span class="kt">int</span> <span class="n">property</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">dataType</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">size</span><span class="p">,</span> <span class="kt">void</span><span class="o">*</span> <span class="n">data</span><span class="p">);</span>
<a name="glxew.h-374"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXMAKEASSOCIATEDCONTEXTCURRENTAMDPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-375"></a>
<a name="glxew.h-376"></a><span class="cp">#define glXBlitContextFramebufferAMD GLXEW_GET_FUN(__glewXBlitContextFramebufferAMD)</span>
<a name="glxew.h-377"></a><span class="cp">#define glXCreateAssociatedContextAMD GLXEW_GET_FUN(__glewXCreateAssociatedContextAMD)</span>
<a name="glxew.h-378"></a><span class="cp">#define glXCreateAssociatedContextAttribsAMD GLXEW_GET_FUN(__glewXCreateAssociatedContextAttribsAMD)</span>
<a name="glxew.h-379"></a><span class="cp">#define glXDeleteAssociatedContextAMD GLXEW_GET_FUN(__glewXDeleteAssociatedContextAMD)</span>
<a name="glxew.h-380"></a><span class="cp">#define glXGetContextGPUIDAMD GLXEW_GET_FUN(__glewXGetContextGPUIDAMD)</span>
<a name="glxew.h-381"></a><span class="cp">#define glXGetCurrentAssociatedContextAMD GLXEW_GET_FUN(__glewXGetCurrentAssociatedContextAMD)</span>
<a name="glxew.h-382"></a><span class="cp">#define glXGetGPUIDsAMD GLXEW_GET_FUN(__glewXGetGPUIDsAMD)</span>
<a name="glxew.h-383"></a><span class="cp">#define glXGetGPUInfoAMD GLXEW_GET_FUN(__glewXGetGPUInfoAMD)</span>
<a name="glxew.h-384"></a><span class="cp">#define glXMakeAssociatedContextCurrentAMD GLXEW_GET_FUN(__glewXMakeAssociatedContextCurrentAMD)</span>
<a name="glxew.h-385"></a>
<a name="glxew.h-386"></a><span class="cp">#define GLXEW_AMD_gpu_association GLXEW_GET_VAR(__GLXEW_AMD_gpu_association)</span>
<a name="glxew.h-387"></a>
<a name="glxew.h-388"></a><span class="cp">#endif </span><span class="cm">/* GLX_AMD_gpu_association */</span><span class="cp"></span>
<a name="glxew.h-389"></a>
<a name="glxew.h-390"></a><span class="cm">/* --------------------- GLX_ARB_context_flush_control --------------------- */</span>
<a name="glxew.h-391"></a>
<a name="glxew.h-392"></a><span class="cp">#ifndef GLX_ARB_context_flush_control</span>
<a name="glxew.h-393"></a><span class="cp">#define GLX_ARB_context_flush_control 1</span>
<a name="glxew.h-394"></a>
<a name="glxew.h-395"></a><span class="cp">#define GLXEW_ARB_context_flush_control GLXEW_GET_VAR(__GLXEW_ARB_context_flush_control)</span>
<a name="glxew.h-396"></a>
<a name="glxew.h-397"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_context_flush_control */</span><span class="cp"></span>
<a name="glxew.h-398"></a>
<a name="glxew.h-399"></a><span class="cm">/* ------------------------- GLX_ARB_create_context ------------------------ */</span>
<a name="glxew.h-400"></a>
<a name="glxew.h-401"></a><span class="cp">#ifndef GLX_ARB_create_context</span>
<a name="glxew.h-402"></a><span class="cp">#define GLX_ARB_create_context 1</span>
<a name="glxew.h-403"></a>
<a name="glxew.h-404"></a><span class="cp">#define GLX_CONTEXT_DEBUG_BIT_ARB 0x0001</span>
<a name="glxew.h-405"></a><span class="cp">#define GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x0002</span>
<a name="glxew.h-406"></a><span class="cp">#define GLX_CONTEXT_MAJOR_VERSION_ARB 0x2091</span>
<a name="glxew.h-407"></a><span class="cp">#define GLX_CONTEXT_MINOR_VERSION_ARB 0x2092</span>
<a name="glxew.h-408"></a><span class="cp">#define GLX_CONTEXT_FLAGS_ARB 0x2094</span>
<a name="glxew.h-409"></a>
<a name="glxew.h-410"></a><span class="k">typedef</span> <span class="nf">GLXContext</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATECONTEXTATTRIBSARBPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">share_context</span><span class="p">,</span> <span class="n">Bool</span> <span class="n">direct</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-411"></a>
<a name="glxew.h-412"></a><span class="cp">#define glXCreateContextAttribsARB GLXEW_GET_FUN(__glewXCreateContextAttribsARB)</span>
<a name="glxew.h-413"></a>
<a name="glxew.h-414"></a><span class="cp">#define GLXEW_ARB_create_context GLXEW_GET_VAR(__GLXEW_ARB_create_context)</span>
<a name="glxew.h-415"></a>
<a name="glxew.h-416"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_create_context */</span><span class="cp"></span>
<a name="glxew.h-417"></a>
<a name="glxew.h-418"></a><span class="cm">/* -------------------- GLX_ARB_create_context_no_error -------------------- */</span>
<a name="glxew.h-419"></a>
<a name="glxew.h-420"></a><span class="cp">#ifndef GLX_ARB_create_context_no_error</span>
<a name="glxew.h-421"></a><span class="cp">#define GLX_ARB_create_context_no_error 1</span>
<a name="glxew.h-422"></a>
<a name="glxew.h-423"></a><span class="cp">#define GLXEW_ARB_create_context_no_error GLXEW_GET_VAR(__GLXEW_ARB_create_context_no_error)</span>
<a name="glxew.h-424"></a>
<a name="glxew.h-425"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_create_context_no_error */</span><span class="cp"></span>
<a name="glxew.h-426"></a>
<a name="glxew.h-427"></a><span class="cm">/* --------------------- GLX_ARB_create_context_profile -------------------- */</span>
<a name="glxew.h-428"></a>
<a name="glxew.h-429"></a><span class="cp">#ifndef GLX_ARB_create_context_profile</span>
<a name="glxew.h-430"></a><span class="cp">#define GLX_ARB_create_context_profile 1</span>
<a name="glxew.h-431"></a>
<a name="glxew.h-432"></a><span class="cp">#define GLX_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001</span>
<a name="glxew.h-433"></a><span class="cp">#define GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002</span>
<a name="glxew.h-434"></a><span class="cp">#define GLX_CONTEXT_PROFILE_MASK_ARB 0x9126</span>
<a name="glxew.h-435"></a>
<a name="glxew.h-436"></a><span class="cp">#define GLXEW_ARB_create_context_profile GLXEW_GET_VAR(__GLXEW_ARB_create_context_profile)</span>
<a name="glxew.h-437"></a>
<a name="glxew.h-438"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_create_context_profile */</span><span class="cp"></span>
<a name="glxew.h-439"></a>
<a name="glxew.h-440"></a><span class="cm">/* ------------------- GLX_ARB_create_context_robustness ------------------- */</span>
<a name="glxew.h-441"></a>
<a name="glxew.h-442"></a><span class="cp">#ifndef GLX_ARB_create_context_robustness</span>
<a name="glxew.h-443"></a><span class="cp">#define GLX_ARB_create_context_robustness 1</span>
<a name="glxew.h-444"></a>
<a name="glxew.h-445"></a><span class="cp">#define GLX_CONTEXT_ROBUST_ACCESS_BIT_ARB 0x00000004</span>
<a name="glxew.h-446"></a><span class="cp">#define GLX_LOSE_CONTEXT_ON_RESET_ARB 0x8252</span>
<a name="glxew.h-447"></a><span class="cp">#define GLX_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB 0x8256</span>
<a name="glxew.h-448"></a><span class="cp">#define GLX_NO_RESET_NOTIFICATION_ARB 0x8261</span>
<a name="glxew.h-449"></a>
<a name="glxew.h-450"></a><span class="cp">#define GLXEW_ARB_create_context_robustness GLXEW_GET_VAR(__GLXEW_ARB_create_context_robustness)</span>
<a name="glxew.h-451"></a>
<a name="glxew.h-452"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_create_context_robustness */</span><span class="cp"></span>
<a name="glxew.h-453"></a>
<a name="glxew.h-454"></a><span class="cm">/* ------------------------- GLX_ARB_fbconfig_float ------------------------ */</span>
<a name="glxew.h-455"></a>
<a name="glxew.h-456"></a><span class="cp">#ifndef GLX_ARB_fbconfig_float</span>
<a name="glxew.h-457"></a><span class="cp">#define GLX_ARB_fbconfig_float 1</span>
<a name="glxew.h-458"></a>
<a name="glxew.h-459"></a><span class="cp">#define GLX_RGBA_FLOAT_BIT_ARB 0x00000004</span>
<a name="glxew.h-460"></a><span class="cp">#define GLX_RGBA_FLOAT_TYPE_ARB 0x20B9</span>
<a name="glxew.h-461"></a>
<a name="glxew.h-462"></a><span class="cp">#define GLXEW_ARB_fbconfig_float GLXEW_GET_VAR(__GLXEW_ARB_fbconfig_float)</span>
<a name="glxew.h-463"></a>
<a name="glxew.h-464"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_fbconfig_float */</span><span class="cp"></span>
<a name="glxew.h-465"></a>
<a name="glxew.h-466"></a><span class="cm">/* ------------------------ GLX_ARB_framebuffer_sRGB ----------------------- */</span>
<a name="glxew.h-467"></a>
<a name="glxew.h-468"></a><span class="cp">#ifndef GLX_ARB_framebuffer_sRGB</span>
<a name="glxew.h-469"></a><span class="cp">#define GLX_ARB_framebuffer_sRGB 1</span>
<a name="glxew.h-470"></a>
<a name="glxew.h-471"></a><span class="cp">#define GLX_FRAMEBUFFER_SRGB_CAPABLE_ARB 0x20B2</span>
<a name="glxew.h-472"></a>
<a name="glxew.h-473"></a><span class="cp">#define GLXEW_ARB_framebuffer_sRGB GLXEW_GET_VAR(__GLXEW_ARB_framebuffer_sRGB)</span>
<a name="glxew.h-474"></a>
<a name="glxew.h-475"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_framebuffer_sRGB */</span><span class="cp"></span>
<a name="glxew.h-476"></a>
<a name="glxew.h-477"></a><span class="cm">/* ------------------------ GLX_ARB_get_proc_address ----------------------- */</span>
<a name="glxew.h-478"></a>
<a name="glxew.h-479"></a><span class="cp">#ifndef GLX_ARB_get_proc_address</span>
<a name="glxew.h-480"></a><span class="cp">#define GLX_ARB_get_proc_address 1</span>
<a name="glxew.h-481"></a>
<a name="glxew.h-482"></a><span class="k">extern</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">glXGetProcAddressARB</span> <span class="p">(</span><span class="k">const</span> <span class="n">GLubyte</span> <span class="o">*</span><span class="n">procName</span><span class="p">))</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-483"></a>
<a name="glxew.h-484"></a><span class="cp">#define GLXEW_ARB_get_proc_address GLXEW_GET_VAR(__GLXEW_ARB_get_proc_address)</span>
<a name="glxew.h-485"></a>
<a name="glxew.h-486"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_get_proc_address */</span><span class="cp"></span>
<a name="glxew.h-487"></a>
<a name="glxew.h-488"></a><span class="cm">/* -------------------------- GLX_ARB_multisample -------------------------- */</span>
<a name="glxew.h-489"></a>
<a name="glxew.h-490"></a><span class="cp">#ifndef GLX_ARB_multisample</span>
<a name="glxew.h-491"></a><span class="cp">#define GLX_ARB_multisample 1</span>
<a name="glxew.h-492"></a>
<a name="glxew.h-493"></a><span class="cp">#define GLX_SAMPLE_BUFFERS_ARB 100000</span>
<a name="glxew.h-494"></a><span class="cp">#define GLX_SAMPLES_ARB 100001</span>
<a name="glxew.h-495"></a>
<a name="glxew.h-496"></a><span class="cp">#define GLXEW_ARB_multisample GLXEW_GET_VAR(__GLXEW_ARB_multisample)</span>
<a name="glxew.h-497"></a>
<a name="glxew.h-498"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_multisample */</span><span class="cp"></span>
<a name="glxew.h-499"></a>
<a name="glxew.h-500"></a><span class="cm">/* ---------------- GLX_ARB_robustness_application_isolation --------------- */</span>
<a name="glxew.h-501"></a>
<a name="glxew.h-502"></a><span class="cp">#ifndef GLX_ARB_robustness_application_isolation</span>
<a name="glxew.h-503"></a><span class="cp">#define GLX_ARB_robustness_application_isolation 1</span>
<a name="glxew.h-504"></a>
<a name="glxew.h-505"></a><span class="cp">#define GLX_CONTEXT_RESET_ISOLATION_BIT_ARB 0x00000008</span>
<a name="glxew.h-506"></a>
<a name="glxew.h-507"></a><span class="cp">#define GLXEW_ARB_robustness_application_isolation GLXEW_GET_VAR(__GLXEW_ARB_robustness_application_isolation)</span>
<a name="glxew.h-508"></a>
<a name="glxew.h-509"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_robustness_application_isolation */</span><span class="cp"></span>
<a name="glxew.h-510"></a>
<a name="glxew.h-511"></a><span class="cm">/* ---------------- GLX_ARB_robustness_share_group_isolation --------------- */</span>
<a name="glxew.h-512"></a>
<a name="glxew.h-513"></a><span class="cp">#ifndef GLX_ARB_robustness_share_group_isolation</span>
<a name="glxew.h-514"></a><span class="cp">#define GLX_ARB_robustness_share_group_isolation 1</span>
<a name="glxew.h-515"></a>
<a name="glxew.h-516"></a><span class="cp">#define GLX_CONTEXT_RESET_ISOLATION_BIT_ARB 0x00000008</span>
<a name="glxew.h-517"></a>
<a name="glxew.h-518"></a><span class="cp">#define GLXEW_ARB_robustness_share_group_isolation GLXEW_GET_VAR(__GLXEW_ARB_robustness_share_group_isolation)</span>
<a name="glxew.h-519"></a>
<a name="glxew.h-520"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_robustness_share_group_isolation */</span><span class="cp"></span>
<a name="glxew.h-521"></a>
<a name="glxew.h-522"></a><span class="cm">/* ---------------------- GLX_ARB_vertex_buffer_object --------------------- */</span>
<a name="glxew.h-523"></a>
<a name="glxew.h-524"></a><span class="cp">#ifndef GLX_ARB_vertex_buffer_object</span>
<a name="glxew.h-525"></a><span class="cp">#define GLX_ARB_vertex_buffer_object 1</span>
<a name="glxew.h-526"></a>
<a name="glxew.h-527"></a><span class="cp">#define GLX_CONTEXT_ALLOW_BUFFER_BYTE_ORDER_MISMATCH_ARB 0x2095</span>
<a name="glxew.h-528"></a>
<a name="glxew.h-529"></a><span class="cp">#define GLXEW_ARB_vertex_buffer_object GLXEW_GET_VAR(__GLXEW_ARB_vertex_buffer_object)</span>
<a name="glxew.h-530"></a>
<a name="glxew.h-531"></a><span class="cp">#endif </span><span class="cm">/* GLX_ARB_vertex_buffer_object */</span><span class="cp"></span>
<a name="glxew.h-532"></a>
<a name="glxew.h-533"></a><span class="cm">/* ----------------------- GLX_ATI_pixel_format_float ---------------------- */</span>
<a name="glxew.h-534"></a>
<a name="glxew.h-535"></a><span class="cp">#ifndef GLX_ATI_pixel_format_float</span>
<a name="glxew.h-536"></a><span class="cp">#define GLX_ATI_pixel_format_float 1</span>
<a name="glxew.h-537"></a>
<a name="glxew.h-538"></a><span class="cp">#define GLX_RGBA_FLOAT_ATI_BIT 0x00000100</span>
<a name="glxew.h-539"></a>
<a name="glxew.h-540"></a><span class="cp">#define GLXEW_ATI_pixel_format_float GLXEW_GET_VAR(__GLXEW_ATI_pixel_format_float)</span>
<a name="glxew.h-541"></a>
<a name="glxew.h-542"></a><span class="cp">#endif </span><span class="cm">/* GLX_ATI_pixel_format_float */</span><span class="cp"></span>
<a name="glxew.h-543"></a>
<a name="glxew.h-544"></a><span class="cm">/* ------------------------- GLX_ATI_render_texture ------------------------ */</span>
<a name="glxew.h-545"></a>
<a name="glxew.h-546"></a><span class="cp">#ifndef GLX_ATI_render_texture</span>
<a name="glxew.h-547"></a><span class="cp">#define GLX_ATI_render_texture 1</span>
<a name="glxew.h-548"></a>
<a name="glxew.h-549"></a><span class="cp">#define GLX_BIND_TO_TEXTURE_RGB_ATI 0x9800</span>
<a name="glxew.h-550"></a><span class="cp">#define GLX_BIND_TO_TEXTURE_RGBA_ATI 0x9801</span>
<a name="glxew.h-551"></a><span class="cp">#define GLX_TEXTURE_FORMAT_ATI 0x9802</span>
<a name="glxew.h-552"></a><span class="cp">#define GLX_TEXTURE_TARGET_ATI 0x9803</span>
<a name="glxew.h-553"></a><span class="cp">#define GLX_MIPMAP_TEXTURE_ATI 0x9804</span>
<a name="glxew.h-554"></a><span class="cp">#define GLX_TEXTURE_RGB_ATI 0x9805</span>
<a name="glxew.h-555"></a><span class="cp">#define GLX_TEXTURE_RGBA_ATI 0x9806</span>
<a name="glxew.h-556"></a><span class="cp">#define GLX_NO_TEXTURE_ATI 0x9807</span>
<a name="glxew.h-557"></a><span class="cp">#define GLX_TEXTURE_CUBE_MAP_ATI 0x9808</span>
<a name="glxew.h-558"></a><span class="cp">#define GLX_TEXTURE_1D_ATI 0x9809</span>
<a name="glxew.h-559"></a><span class="cp">#define GLX_TEXTURE_2D_ATI 0x980A</span>
<a name="glxew.h-560"></a><span class="cp">#define GLX_MIPMAP_LEVEL_ATI 0x980B</span>
<a name="glxew.h-561"></a><span class="cp">#define GLX_CUBE_MAP_FACE_ATI 0x980C</span>
<a name="glxew.h-562"></a><span class="cp">#define GLX_TEXTURE_CUBE_MAP_POSITIVE_X_ATI 0x980D</span>
<a name="glxew.h-563"></a><span class="cp">#define GLX_TEXTURE_CUBE_MAP_NEGATIVE_X_ATI 0x980E</span>
<a name="glxew.h-564"></a><span class="cp">#define GLX_TEXTURE_CUBE_MAP_POSITIVE_Y_ATI 0x980F</span>
<a name="glxew.h-565"></a><span class="cp">#define GLX_TEXTURE_CUBE_MAP_NEGATIVE_Y_ATI 0x9810</span>
<a name="glxew.h-566"></a><span class="cp">#define GLX_TEXTURE_CUBE_MAP_POSITIVE_Z_ATI 0x9811</span>
<a name="glxew.h-567"></a><span class="cp">#define GLX_TEXTURE_CUBE_MAP_NEGATIVE_Z_ATI 0x9812</span>
<a name="glxew.h-568"></a><span class="cp">#define GLX_FRONT_LEFT_ATI 0x9813</span>
<a name="glxew.h-569"></a><span class="cp">#define GLX_FRONT_RIGHT_ATI 0x9814</span>
<a name="glxew.h-570"></a><span class="cp">#define GLX_BACK_LEFT_ATI 0x9815</span>
<a name="glxew.h-571"></a><span class="cp">#define GLX_BACK_RIGHT_ATI 0x9816</span>
<a name="glxew.h-572"></a><span class="cp">#define GLX_AUX0_ATI 0x9817</span>
<a name="glxew.h-573"></a><span class="cp">#define GLX_AUX1_ATI 0x9818</span>
<a name="glxew.h-574"></a><span class="cp">#define GLX_AUX2_ATI 0x9819</span>
<a name="glxew.h-575"></a><span class="cp">#define GLX_AUX3_ATI 0x981A</span>
<a name="glxew.h-576"></a><span class="cp">#define GLX_AUX4_ATI 0x981B</span>
<a name="glxew.h-577"></a><span class="cp">#define GLX_AUX5_ATI 0x981C</span>
<a name="glxew.h-578"></a><span class="cp">#define GLX_AUX6_ATI 0x981D</span>
<a name="glxew.h-579"></a><span class="cp">#define GLX_AUX7_ATI 0x981E</span>
<a name="glxew.h-580"></a><span class="cp">#define GLX_AUX8_ATI 0x981F</span>
<a name="glxew.h-581"></a><span class="cp">#define GLX_AUX9_ATI 0x9820</span>
<a name="glxew.h-582"></a><span class="cp">#define GLX_BIND_TO_TEXTURE_LUMINANCE_ATI 0x9821</span>
<a name="glxew.h-583"></a><span class="cp">#define GLX_BIND_TO_TEXTURE_INTENSITY_ATI 0x9822</span>
<a name="glxew.h-584"></a>
<a name="glxew.h-585"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDTEXIMAGEATIPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">,</span> <span class="kt">int</span> <span class="n">buffer</span><span class="p">);</span>
<a name="glxew.h-586"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDRAWABLEATTRIBATIPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">draw</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-587"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXRELEASETEXIMAGEATIPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">,</span> <span class="kt">int</span> <span class="n">buffer</span><span class="p">);</span>
<a name="glxew.h-588"></a>
<a name="glxew.h-589"></a><span class="cp">#define glXBindTexImageATI GLXEW_GET_FUN(__glewXBindTexImageATI)</span>
<a name="glxew.h-590"></a><span class="cp">#define glXDrawableAttribATI GLXEW_GET_FUN(__glewXDrawableAttribATI)</span>
<a name="glxew.h-591"></a><span class="cp">#define glXReleaseTexImageATI GLXEW_GET_FUN(__glewXReleaseTexImageATI)</span>
<a name="glxew.h-592"></a>
<a name="glxew.h-593"></a><span class="cp">#define GLXEW_ATI_render_texture GLXEW_GET_VAR(__GLXEW_ATI_render_texture)</span>
<a name="glxew.h-594"></a>
<a name="glxew.h-595"></a><span class="cp">#endif </span><span class="cm">/* GLX_ATI_render_texture */</span><span class="cp"></span>
<a name="glxew.h-596"></a>
<a name="glxew.h-597"></a><span class="cm">/* --------------------------- GLX_EXT_buffer_age -------------------------- */</span>
<a name="glxew.h-598"></a>
<a name="glxew.h-599"></a><span class="cp">#ifndef GLX_EXT_buffer_age</span>
<a name="glxew.h-600"></a><span class="cp">#define GLX_EXT_buffer_age 1</span>
<a name="glxew.h-601"></a>
<a name="glxew.h-602"></a><span class="cp">#define GLX_BACK_BUFFER_AGE_EXT 0x20F4</span>
<a name="glxew.h-603"></a>
<a name="glxew.h-604"></a><span class="cp">#define GLXEW_EXT_buffer_age GLXEW_GET_VAR(__GLXEW_EXT_buffer_age)</span>
<a name="glxew.h-605"></a>
<a name="glxew.h-606"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_buffer_age */</span><span class="cp"></span>
<a name="glxew.h-607"></a>
<a name="glxew.h-608"></a><span class="cm">/* ------------------- GLX_EXT_create_context_es2_profile ------------------ */</span>
<a name="glxew.h-609"></a>
<a name="glxew.h-610"></a><span class="cp">#ifndef GLX_EXT_create_context_es2_profile</span>
<a name="glxew.h-611"></a><span class="cp">#define GLX_EXT_create_context_es2_profile 1</span>
<a name="glxew.h-612"></a>
<a name="glxew.h-613"></a><span class="cp">#define GLX_CONTEXT_ES2_PROFILE_BIT_EXT 0x00000004</span>
<a name="glxew.h-614"></a>
<a name="glxew.h-615"></a><span class="cp">#define GLXEW_EXT_create_context_es2_profile GLXEW_GET_VAR(__GLXEW_EXT_create_context_es2_profile)</span>
<a name="glxew.h-616"></a>
<a name="glxew.h-617"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_create_context_es2_profile */</span><span class="cp"></span>
<a name="glxew.h-618"></a>
<a name="glxew.h-619"></a><span class="cm">/* ------------------- GLX_EXT_create_context_es_profile ------------------- */</span>
<a name="glxew.h-620"></a>
<a name="glxew.h-621"></a><span class="cp">#ifndef GLX_EXT_create_context_es_profile</span>
<a name="glxew.h-622"></a><span class="cp">#define GLX_EXT_create_context_es_profile 1</span>
<a name="glxew.h-623"></a>
<a name="glxew.h-624"></a><span class="cp">#define GLX_CONTEXT_ES_PROFILE_BIT_EXT 0x00000004</span>
<a name="glxew.h-625"></a>
<a name="glxew.h-626"></a><span class="cp">#define GLXEW_EXT_create_context_es_profile GLXEW_GET_VAR(__GLXEW_EXT_create_context_es_profile)</span>
<a name="glxew.h-627"></a>
<a name="glxew.h-628"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_create_context_es_profile */</span><span class="cp"></span>
<a name="glxew.h-629"></a>
<a name="glxew.h-630"></a><span class="cm">/* --------------------- GLX_EXT_fbconfig_packed_float --------------------- */</span>
<a name="glxew.h-631"></a>
<a name="glxew.h-632"></a><span class="cp">#ifndef GLX_EXT_fbconfig_packed_float</span>
<a name="glxew.h-633"></a><span class="cp">#define GLX_EXT_fbconfig_packed_float 1</span>
<a name="glxew.h-634"></a>
<a name="glxew.h-635"></a><span class="cp">#define GLX_RGBA_UNSIGNED_FLOAT_BIT_EXT 0x00000008</span>
<a name="glxew.h-636"></a><span class="cp">#define GLX_RGBA_UNSIGNED_FLOAT_TYPE_EXT 0x20B1</span>
<a name="glxew.h-637"></a>
<a name="glxew.h-638"></a><span class="cp">#define GLXEW_EXT_fbconfig_packed_float GLXEW_GET_VAR(__GLXEW_EXT_fbconfig_packed_float)</span>
<a name="glxew.h-639"></a>
<a name="glxew.h-640"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_fbconfig_packed_float */</span><span class="cp"></span>
<a name="glxew.h-641"></a>
<a name="glxew.h-642"></a><span class="cm">/* ------------------------ GLX_EXT_framebuffer_sRGB ----------------------- */</span>
<a name="glxew.h-643"></a>
<a name="glxew.h-644"></a><span class="cp">#ifndef GLX_EXT_framebuffer_sRGB</span>
<a name="glxew.h-645"></a><span class="cp">#define GLX_EXT_framebuffer_sRGB 1</span>
<a name="glxew.h-646"></a>
<a name="glxew.h-647"></a><span class="cp">#define GLX_FRAMEBUFFER_SRGB_CAPABLE_EXT 0x20B2</span>
<a name="glxew.h-648"></a>
<a name="glxew.h-649"></a><span class="cp">#define GLXEW_EXT_framebuffer_sRGB GLXEW_GET_VAR(__GLXEW_EXT_framebuffer_sRGB)</span>
<a name="glxew.h-650"></a>
<a name="glxew.h-651"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_framebuffer_sRGB */</span><span class="cp"></span>
<a name="glxew.h-652"></a>
<a name="glxew.h-653"></a><span class="cm">/* ------------------------- GLX_EXT_import_context ------------------------ */</span>
<a name="glxew.h-654"></a>
<a name="glxew.h-655"></a><span class="cp">#ifndef GLX_EXT_import_context</span>
<a name="glxew.h-656"></a><span class="cp">#define GLX_EXT_import_context 1</span>
<a name="glxew.h-657"></a>
<a name="glxew.h-658"></a><span class="cp">#define GLX_SHARE_CONTEXT_EXT 0x800A</span>
<a name="glxew.h-659"></a><span class="cp">#define GLX_VISUAL_ID_EXT 0x800B</span>
<a name="glxew.h-660"></a><span class="cp">#define GLX_SCREEN_EXT 0x800C</span>
<a name="glxew.h-661"></a>
<a name="glxew.h-662"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXContextID</span><span class="p">;</span>
<a name="glxew.h-663"></a>
<a name="glxew.h-664"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXFREECONTEXTEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">context</span><span class="p">);</span>
<a name="glxew.h-665"></a><span class="k">typedef</span> <span class="nf">GLXContextID</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETCONTEXTIDEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="k">const</span> <span class="n">GLXContext</span> <span class="n">context</span><span class="p">);</span>
<a name="glxew.h-666"></a><span class="k">typedef</span> <span class="nf">GLXContext</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXIMPORTCONTEXTEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContextID</span> <span class="n">contextID</span><span class="p">);</span>
<a name="glxew.h-667"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYCONTEXTINFOEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">context</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span><span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-668"></a>
<a name="glxew.h-669"></a><span class="cp">#define glXFreeContextEXT GLXEW_GET_FUN(__glewXFreeContextEXT)</span>
<a name="glxew.h-670"></a><span class="cp">#define glXGetContextIDEXT GLXEW_GET_FUN(__glewXGetContextIDEXT)</span>
<a name="glxew.h-671"></a><span class="cp">#define glXImportContextEXT GLXEW_GET_FUN(__glewXImportContextEXT)</span>
<a name="glxew.h-672"></a><span class="cp">#define glXQueryContextInfoEXT GLXEW_GET_FUN(__glewXQueryContextInfoEXT)</span>
<a name="glxew.h-673"></a>
<a name="glxew.h-674"></a><span class="cp">#define GLXEW_EXT_import_context GLXEW_GET_VAR(__GLXEW_EXT_import_context)</span>
<a name="glxew.h-675"></a>
<a name="glxew.h-676"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_import_context */</span><span class="cp"></span>
<a name="glxew.h-677"></a>
<a name="glxew.h-678"></a><span class="cm">/* ---------------------------- GLX_EXT_libglvnd --------------------------- */</span>
<a name="glxew.h-679"></a>
<a name="glxew.h-680"></a><span class="cp">#ifndef GLX_EXT_libglvnd</span>
<a name="glxew.h-681"></a><span class="cp">#define GLX_EXT_libglvnd 1</span>
<a name="glxew.h-682"></a>
<a name="glxew.h-683"></a><span class="cp">#define GLX_VENDOR_NAMES_EXT 0x20F6</span>
<a name="glxew.h-684"></a>
<a name="glxew.h-685"></a><span class="cp">#define GLXEW_EXT_libglvnd GLXEW_GET_VAR(__GLXEW_EXT_libglvnd)</span>
<a name="glxew.h-686"></a>
<a name="glxew.h-687"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_libglvnd */</span><span class="cp"></span>
<a name="glxew.h-688"></a>
<a name="glxew.h-689"></a><span class="cm">/* -------------------------- GLX_EXT_scene_marker ------------------------- */</span>
<a name="glxew.h-690"></a>
<a name="glxew.h-691"></a><span class="cp">#ifndef GLX_EXT_scene_marker</span>
<a name="glxew.h-692"></a><span class="cp">#define GLX_EXT_scene_marker 1</span>
<a name="glxew.h-693"></a>
<a name="glxew.h-694"></a><span class="cp">#define GLXEW_EXT_scene_marker GLXEW_GET_VAR(__GLXEW_EXT_scene_marker)</span>
<a name="glxew.h-695"></a>
<a name="glxew.h-696"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_scene_marker */</span><span class="cp"></span>
<a name="glxew.h-697"></a>
<a name="glxew.h-698"></a><span class="cm">/* -------------------------- GLX_EXT_stereo_tree -------------------------- */</span>
<a name="glxew.h-699"></a>
<a name="glxew.h-700"></a><span class="cp">#ifndef GLX_EXT_stereo_tree</span>
<a name="glxew.h-701"></a><span class="cp">#define GLX_EXT_stereo_tree 1</span>
<a name="glxew.h-702"></a>
<a name="glxew.h-703"></a><span class="cp">#define GLX_STEREO_NOTIFY_EXT 0x00000000</span>
<a name="glxew.h-704"></a><span class="cp">#define GLX_STEREO_NOTIFY_MASK_EXT 0x00000001</span>
<a name="glxew.h-705"></a><span class="cp">#define GLX_STEREO_TREE_EXT 0x20F5</span>
<a name="glxew.h-706"></a>
<a name="glxew.h-707"></a><span class="cp">#define GLXEW_EXT_stereo_tree GLXEW_GET_VAR(__GLXEW_EXT_stereo_tree)</span>
<a name="glxew.h-708"></a>
<a name="glxew.h-709"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_stereo_tree */</span><span class="cp"></span>
<a name="glxew.h-710"></a>
<a name="glxew.h-711"></a><span class="cm">/* -------------------------- GLX_EXT_swap_control ------------------------- */</span>
<a name="glxew.h-712"></a>
<a name="glxew.h-713"></a><span class="cp">#ifndef GLX_EXT_swap_control</span>
<a name="glxew.h-714"></a><span class="cp">#define GLX_EXT_swap_control 1</span>
<a name="glxew.h-715"></a>
<a name="glxew.h-716"></a><span class="cp">#define GLX_SWAP_INTERVAL_EXT 0x20F1</span>
<a name="glxew.h-717"></a><span class="cp">#define GLX_MAX_SWAP_INTERVAL_EXT 0x20F2</span>
<a name="glxew.h-718"></a>
<a name="glxew.h-719"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSWAPINTERVALEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int</span> <span class="n">interval</span><span class="p">);</span>
<a name="glxew.h-720"></a>
<a name="glxew.h-721"></a><span class="cp">#define glXSwapIntervalEXT GLXEW_GET_FUN(__glewXSwapIntervalEXT)</span>
<a name="glxew.h-722"></a>
<a name="glxew.h-723"></a><span class="cp">#define GLXEW_EXT_swap_control GLXEW_GET_VAR(__GLXEW_EXT_swap_control)</span>
<a name="glxew.h-724"></a>
<a name="glxew.h-725"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_swap_control */</span><span class="cp"></span>
<a name="glxew.h-726"></a>
<a name="glxew.h-727"></a><span class="cm">/* ----------------------- GLX_EXT_swap_control_tear ----------------------- */</span>
<a name="glxew.h-728"></a>
<a name="glxew.h-729"></a><span class="cp">#ifndef GLX_EXT_swap_control_tear</span>
<a name="glxew.h-730"></a><span class="cp">#define GLX_EXT_swap_control_tear 1</span>
<a name="glxew.h-731"></a>
<a name="glxew.h-732"></a><span class="cp">#define GLX_LATE_SWAPS_TEAR_EXT 0x20F3</span>
<a name="glxew.h-733"></a>
<a name="glxew.h-734"></a><span class="cp">#define GLXEW_EXT_swap_control_tear GLXEW_GET_VAR(__GLXEW_EXT_swap_control_tear)</span>
<a name="glxew.h-735"></a>
<a name="glxew.h-736"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_swap_control_tear */</span><span class="cp"></span>
<a name="glxew.h-737"></a>
<a name="glxew.h-738"></a><span class="cm">/* ---------------------- GLX_EXT_texture_from_pixmap ---------------------- */</span>
<a name="glxew.h-739"></a>
<a name="glxew.h-740"></a><span class="cp">#ifndef GLX_EXT_texture_from_pixmap</span>
<a name="glxew.h-741"></a><span class="cp">#define GLX_EXT_texture_from_pixmap 1</span>
<a name="glxew.h-742"></a>
<a name="glxew.h-743"></a><span class="cp">#define GLX_TEXTURE_1D_BIT_EXT 0x00000001</span>
<a name="glxew.h-744"></a><span class="cp">#define GLX_TEXTURE_2D_BIT_EXT 0x00000002</span>
<a name="glxew.h-745"></a><span class="cp">#define GLX_TEXTURE_RECTANGLE_BIT_EXT 0x00000004</span>
<a name="glxew.h-746"></a><span class="cp">#define GLX_BIND_TO_TEXTURE_RGB_EXT 0x20D0</span>
<a name="glxew.h-747"></a><span class="cp">#define GLX_BIND_TO_TEXTURE_RGBA_EXT 0x20D1</span>
<a name="glxew.h-748"></a><span class="cp">#define GLX_BIND_TO_MIPMAP_TEXTURE_EXT 0x20D2</span>
<a name="glxew.h-749"></a><span class="cp">#define GLX_BIND_TO_TEXTURE_TARGETS_EXT 0x20D3</span>
<a name="glxew.h-750"></a><span class="cp">#define GLX_Y_INVERTED_EXT 0x20D4</span>
<a name="glxew.h-751"></a><span class="cp">#define GLX_TEXTURE_FORMAT_EXT 0x20D5</span>
<a name="glxew.h-752"></a><span class="cp">#define GLX_TEXTURE_TARGET_EXT 0x20D6</span>
<a name="glxew.h-753"></a><span class="cp">#define GLX_MIPMAP_TEXTURE_EXT 0x20D7</span>
<a name="glxew.h-754"></a><span class="cp">#define GLX_TEXTURE_FORMAT_NONE_EXT 0x20D8</span>
<a name="glxew.h-755"></a><span class="cp">#define GLX_TEXTURE_FORMAT_RGB_EXT 0x20D9</span>
<a name="glxew.h-756"></a><span class="cp">#define GLX_TEXTURE_FORMAT_RGBA_EXT 0x20DA</span>
<a name="glxew.h-757"></a><span class="cp">#define GLX_TEXTURE_1D_EXT 0x20DB</span>
<a name="glxew.h-758"></a><span class="cp">#define GLX_TEXTURE_2D_EXT 0x20DC</span>
<a name="glxew.h-759"></a><span class="cp">#define GLX_TEXTURE_RECTANGLE_EXT 0x20DD</span>
<a name="glxew.h-760"></a><span class="cp">#define GLX_FRONT_LEFT_EXT 0x20DE</span>
<a name="glxew.h-761"></a><span class="cp">#define GLX_FRONT_RIGHT_EXT 0x20DF</span>
<a name="glxew.h-762"></a><span class="cp">#define GLX_BACK_LEFT_EXT 0x20E0</span>
<a name="glxew.h-763"></a><span class="cp">#define GLX_BACK_RIGHT_EXT 0x20E1</span>
<a name="glxew.h-764"></a><span class="cp">#define GLX_AUX0_EXT 0x20E2</span>
<a name="glxew.h-765"></a><span class="cp">#define GLX_AUX1_EXT 0x20E3</span>
<a name="glxew.h-766"></a><span class="cp">#define GLX_AUX2_EXT 0x20E4</span>
<a name="glxew.h-767"></a><span class="cp">#define GLX_AUX3_EXT 0x20E5</span>
<a name="glxew.h-768"></a><span class="cp">#define GLX_AUX4_EXT 0x20E6</span>
<a name="glxew.h-769"></a><span class="cp">#define GLX_AUX5_EXT 0x20E7</span>
<a name="glxew.h-770"></a><span class="cp">#define GLX_AUX6_EXT 0x20E8</span>
<a name="glxew.h-771"></a><span class="cp">#define GLX_AUX7_EXT 0x20E9</span>
<a name="glxew.h-772"></a><span class="cp">#define GLX_AUX8_EXT 0x20EA</span>
<a name="glxew.h-773"></a><span class="cp">#define GLX_AUX9_EXT 0x20EB</span>
<a name="glxew.h-774"></a>
<a name="glxew.h-775"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDTEXIMAGEEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int</span> <span class="n">buffer</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-776"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXRELEASETEXIMAGEEXTPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int</span> <span class="n">buffer</span><span class="p">);</span>
<a name="glxew.h-777"></a>
<a name="glxew.h-778"></a><span class="cp">#define glXBindTexImageEXT GLXEW_GET_FUN(__glewXBindTexImageEXT)</span>
<a name="glxew.h-779"></a><span class="cp">#define glXReleaseTexImageEXT GLXEW_GET_FUN(__glewXReleaseTexImageEXT)</span>
<a name="glxew.h-780"></a>
<a name="glxew.h-781"></a><span class="cp">#define GLXEW_EXT_texture_from_pixmap GLXEW_GET_VAR(__GLXEW_EXT_texture_from_pixmap)</span>
<a name="glxew.h-782"></a>
<a name="glxew.h-783"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_texture_from_pixmap */</span><span class="cp"></span>
<a name="glxew.h-784"></a>
<a name="glxew.h-785"></a><span class="cm">/* -------------------------- GLX_EXT_visual_info -------------------------- */</span>
<a name="glxew.h-786"></a>
<a name="glxew.h-787"></a><span class="cp">#ifndef GLX_EXT_visual_info</span>
<a name="glxew.h-788"></a><span class="cp">#define GLX_EXT_visual_info 1</span>
<a name="glxew.h-789"></a>
<a name="glxew.h-790"></a><span class="cp">#define GLX_X_VISUAL_TYPE_EXT 0x22</span>
<a name="glxew.h-791"></a><span class="cp">#define GLX_TRANSPARENT_TYPE_EXT 0x23</span>
<a name="glxew.h-792"></a><span class="cp">#define GLX_TRANSPARENT_INDEX_VALUE_EXT 0x24</span>
<a name="glxew.h-793"></a><span class="cp">#define GLX_TRANSPARENT_RED_VALUE_EXT 0x25</span>
<a name="glxew.h-794"></a><span class="cp">#define GLX_TRANSPARENT_GREEN_VALUE_EXT 0x26</span>
<a name="glxew.h-795"></a><span class="cp">#define GLX_TRANSPARENT_BLUE_VALUE_EXT 0x27</span>
<a name="glxew.h-796"></a><span class="cp">#define GLX_TRANSPARENT_ALPHA_VALUE_EXT 0x28</span>
<a name="glxew.h-797"></a><span class="cp">#define GLX_NONE_EXT 0x8000</span>
<a name="glxew.h-798"></a><span class="cp">#define GLX_TRUE_COLOR_EXT 0x8002</span>
<a name="glxew.h-799"></a><span class="cp">#define GLX_DIRECT_COLOR_EXT 0x8003</span>
<a name="glxew.h-800"></a><span class="cp">#define GLX_PSEUDO_COLOR_EXT 0x8004</span>
<a name="glxew.h-801"></a><span class="cp">#define GLX_STATIC_COLOR_EXT 0x8005</span>
<a name="glxew.h-802"></a><span class="cp">#define GLX_GRAY_SCALE_EXT 0x8006</span>
<a name="glxew.h-803"></a><span class="cp">#define GLX_STATIC_GRAY_EXT 0x8007</span>
<a name="glxew.h-804"></a><span class="cp">#define GLX_TRANSPARENT_RGB_EXT 0x8008</span>
<a name="glxew.h-805"></a><span class="cp">#define GLX_TRANSPARENT_INDEX_EXT 0x8009</span>
<a name="glxew.h-806"></a>
<a name="glxew.h-807"></a><span class="cp">#define GLXEW_EXT_visual_info GLXEW_GET_VAR(__GLXEW_EXT_visual_info)</span>
<a name="glxew.h-808"></a>
<a name="glxew.h-809"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_visual_info */</span><span class="cp"></span>
<a name="glxew.h-810"></a>
<a name="glxew.h-811"></a><span class="cm">/* ------------------------- GLX_EXT_visual_rating ------------------------- */</span>
<a name="glxew.h-812"></a>
<a name="glxew.h-813"></a><span class="cp">#ifndef GLX_EXT_visual_rating</span>
<a name="glxew.h-814"></a><span class="cp">#define GLX_EXT_visual_rating 1</span>
<a name="glxew.h-815"></a>
<a name="glxew.h-816"></a><span class="cp">#define GLX_VISUAL_CAVEAT_EXT 0x20</span>
<a name="glxew.h-817"></a><span class="cp">#define GLX_SLOW_VISUAL_EXT 0x8001</span>
<a name="glxew.h-818"></a><span class="cp">#define GLX_NON_CONFORMANT_VISUAL_EXT 0x800D</span>
<a name="glxew.h-819"></a>
<a name="glxew.h-820"></a><span class="cp">#define GLXEW_EXT_visual_rating GLXEW_GET_VAR(__GLXEW_EXT_visual_rating)</span>
<a name="glxew.h-821"></a>
<a name="glxew.h-822"></a><span class="cp">#endif </span><span class="cm">/* GLX_EXT_visual_rating */</span><span class="cp"></span>
<a name="glxew.h-823"></a>
<a name="glxew.h-824"></a><span class="cm">/* -------------------------- GLX_INTEL_swap_event ------------------------- */</span>
<a name="glxew.h-825"></a>
<a name="glxew.h-826"></a><span class="cp">#ifndef GLX_INTEL_swap_event</span>
<a name="glxew.h-827"></a><span class="cp">#define GLX_INTEL_swap_event 1</span>
<a name="glxew.h-828"></a>
<a name="glxew.h-829"></a><span class="cp">#define GLX_EXCHANGE_COMPLETE_INTEL 0x8180</span>
<a name="glxew.h-830"></a><span class="cp">#define GLX_COPY_COMPLETE_INTEL 0x8181</span>
<a name="glxew.h-831"></a><span class="cp">#define GLX_FLIP_COMPLETE_INTEL 0x8182</span>
<a name="glxew.h-832"></a><span class="cp">#define GLX_BUFFER_SWAP_COMPLETE_INTEL_MASK 0x04000000</span>
<a name="glxew.h-833"></a>
<a name="glxew.h-834"></a><span class="cp">#define GLXEW_INTEL_swap_event GLXEW_GET_VAR(__GLXEW_INTEL_swap_event)</span>
<a name="glxew.h-835"></a>
<a name="glxew.h-836"></a><span class="cp">#endif </span><span class="cm">/* GLX_INTEL_swap_event */</span><span class="cp"></span>
<a name="glxew.h-837"></a>
<a name="glxew.h-838"></a><span class="cm">/* -------------------------- GLX_MESA_agp_offset -------------------------- */</span>
<a name="glxew.h-839"></a>
<a name="glxew.h-840"></a><span class="cp">#ifndef GLX_MESA_agp_offset</span>
<a name="glxew.h-841"></a><span class="cp">#define GLX_MESA_agp_offset 1</span>
<a name="glxew.h-842"></a>
<a name="glxew.h-843"></a><span class="k">typedef</span> <span class="kt">unsigned</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETAGPOFFSETMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="k">const</span> <span class="kt">void</span><span class="o">*</span> <span class="n">pointer</span><span class="p">);</span>
<a name="glxew.h-844"></a>
<a name="glxew.h-845"></a><span class="cp">#define glXGetAGPOffsetMESA GLXEW_GET_FUN(__glewXGetAGPOffsetMESA)</span>
<a name="glxew.h-846"></a>
<a name="glxew.h-847"></a><span class="cp">#define GLXEW_MESA_agp_offset GLXEW_GET_VAR(__GLXEW_MESA_agp_offset)</span>
<a name="glxew.h-848"></a>
<a name="glxew.h-849"></a><span class="cp">#endif </span><span class="cm">/* GLX_MESA_agp_offset */</span><span class="cp"></span>
<a name="glxew.h-850"></a>
<a name="glxew.h-851"></a><span class="cm">/* ------------------------ GLX_MESA_copy_sub_buffer ----------------------- */</span>
<a name="glxew.h-852"></a>
<a name="glxew.h-853"></a><span class="cp">#ifndef GLX_MESA_copy_sub_buffer</span>
<a name="glxew.h-854"></a><span class="cp">#define GLX_MESA_copy_sub_buffer 1</span>
<a name="glxew.h-855"></a>
<a name="glxew.h-856"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCOPYSUBBUFFERMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">int</span> <span class="n">height</span><span class="p">);</span>
<a name="glxew.h-857"></a>
<a name="glxew.h-858"></a><span class="cp">#define glXCopySubBufferMESA GLXEW_GET_FUN(__glewXCopySubBufferMESA)</span>
<a name="glxew.h-859"></a>
<a name="glxew.h-860"></a><span class="cp">#define GLXEW_MESA_copy_sub_buffer GLXEW_GET_VAR(__GLXEW_MESA_copy_sub_buffer)</span>
<a name="glxew.h-861"></a>
<a name="glxew.h-862"></a><span class="cp">#endif </span><span class="cm">/* GLX_MESA_copy_sub_buffer */</span><span class="cp"></span>
<a name="glxew.h-863"></a>
<a name="glxew.h-864"></a><span class="cm">/* ------------------------ GLX_MESA_pixmap_colormap ----------------------- */</span>
<a name="glxew.h-865"></a>
<a name="glxew.h-866"></a><span class="cp">#ifndef GLX_MESA_pixmap_colormap</span>
<a name="glxew.h-867"></a><span class="cp">#define GLX_MESA_pixmap_colormap 1</span>
<a name="glxew.h-868"></a>
<a name="glxew.h-869"></a><span class="k">typedef</span> <span class="nf">GLXPixmap</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEGLXPIXMAPMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">XVisualInfo</span> <span class="o">*</span><span class="n">visual</span><span class="p">,</span> <span class="n">Pixmap</span> <span class="n">pixmap</span><span class="p">,</span> <span class="n">Colormap</span> <span class="n">cmap</span><span class="p">);</span>
<a name="glxew.h-870"></a>
<a name="glxew.h-871"></a><span class="cp">#define glXCreateGLXPixmapMESA GLXEW_GET_FUN(__glewXCreateGLXPixmapMESA)</span>
<a name="glxew.h-872"></a>
<a name="glxew.h-873"></a><span class="cp">#define GLXEW_MESA_pixmap_colormap GLXEW_GET_VAR(__GLXEW_MESA_pixmap_colormap)</span>
<a name="glxew.h-874"></a>
<a name="glxew.h-875"></a><span class="cp">#endif </span><span class="cm">/* GLX_MESA_pixmap_colormap */</span><span class="cp"></span>
<a name="glxew.h-876"></a>
<a name="glxew.h-877"></a><span class="cm">/* ------------------------ GLX_MESA_query_renderer ------------------------ */</span>
<a name="glxew.h-878"></a>
<a name="glxew.h-879"></a><span class="cp">#ifndef GLX_MESA_query_renderer</span>
<a name="glxew.h-880"></a><span class="cp">#define GLX_MESA_query_renderer 1</span>
<a name="glxew.h-881"></a>
<a name="glxew.h-882"></a><span class="cp">#define GLX_RENDERER_VENDOR_ID_MESA 0x8183</span>
<a name="glxew.h-883"></a><span class="cp">#define GLX_RENDERER_DEVICE_ID_MESA 0x8184</span>
<a name="glxew.h-884"></a><span class="cp">#define GLX_RENDERER_VERSION_MESA 0x8185</span>
<a name="glxew.h-885"></a><span class="cp">#define GLX_RENDERER_ACCELERATED_MESA 0x8186</span>
<a name="glxew.h-886"></a><span class="cp">#define GLX_RENDERER_VIDEO_MEMORY_MESA 0x8187</span>
<a name="glxew.h-887"></a><span class="cp">#define GLX_RENDERER_UNIFIED_MEMORY_ARCHITECTURE_MESA 0x8188</span>
<a name="glxew.h-888"></a><span class="cp">#define GLX_RENDERER_PREFERRED_PROFILE_MESA 0x8189</span>
<a name="glxew.h-889"></a><span class="cp">#define GLX_RENDERER_OPENGL_CORE_PROFILE_VERSION_MESA 0x818A</span>
<a name="glxew.h-890"></a><span class="cp">#define GLX_RENDERER_OPENGL_COMPATIBILITY_PROFILE_VERSION_MESA 0x818B</span>
<a name="glxew.h-891"></a><span class="cp">#define GLX_RENDERER_OPENGL_ES_PROFILE_VERSION_MESA 0x818C</span>
<a name="glxew.h-892"></a><span class="cp">#define GLX_RENDERER_OPENGL_ES2_PROFILE_VERSION_MESA 0x818D</span>
<a name="glxew.h-893"></a><span class="cp">#define GLX_RENDERER_ID_MESA 0x818E</span>
<a name="glxew.h-894"></a>
<a name="glxew.h-895"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYCURRENTRENDERERINTEGERMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span><span class="o">*</span> <span class="n">value</span><span class="p">);</span>
<a name="glxew.h-896"></a><span class="k">typedef</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYCURRENTRENDERERSTRINGMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">int</span> <span class="n">attribute</span><span class="p">);</span>
<a name="glxew.h-897"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYRENDERERINTEGERMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">renderer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-898"></a><span class="k">typedef</span> <span class="k">const</span> <span class="kt">char</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYRENDERERSTRINGMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">renderer</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">);</span>
<a name="glxew.h-899"></a>
<a name="glxew.h-900"></a><span class="cp">#define glXQueryCurrentRendererIntegerMESA GLXEW_GET_FUN(__glewXQueryCurrentRendererIntegerMESA)</span>
<a name="glxew.h-901"></a><span class="cp">#define glXQueryCurrentRendererStringMESA GLXEW_GET_FUN(__glewXQueryCurrentRendererStringMESA)</span>
<a name="glxew.h-902"></a><span class="cp">#define glXQueryRendererIntegerMESA GLXEW_GET_FUN(__glewXQueryRendererIntegerMESA)</span>
<a name="glxew.h-903"></a><span class="cp">#define glXQueryRendererStringMESA GLXEW_GET_FUN(__glewXQueryRendererStringMESA)</span>
<a name="glxew.h-904"></a>
<a name="glxew.h-905"></a><span class="cp">#define GLXEW_MESA_query_renderer GLXEW_GET_VAR(__GLXEW_MESA_query_renderer)</span>
<a name="glxew.h-906"></a>
<a name="glxew.h-907"></a><span class="cp">#endif </span><span class="cm">/* GLX_MESA_query_renderer */</span><span class="cp"></span>
<a name="glxew.h-908"></a>
<a name="glxew.h-909"></a><span class="cm">/* ------------------------ GLX_MESA_release_buffers ----------------------- */</span>
<a name="glxew.h-910"></a>
<a name="glxew.h-911"></a><span class="cp">#ifndef GLX_MESA_release_buffers</span>
<a name="glxew.h-912"></a><span class="cp">#define GLX_MESA_release_buffers 1</span>
<a name="glxew.h-913"></a>
<a name="glxew.h-914"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXRELEASEBUFFERSMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">d</span><span class="p">);</span>
<a name="glxew.h-915"></a>
<a name="glxew.h-916"></a><span class="cp">#define glXReleaseBuffersMESA GLXEW_GET_FUN(__glewXReleaseBuffersMESA)</span>
<a name="glxew.h-917"></a>
<a name="glxew.h-918"></a><span class="cp">#define GLXEW_MESA_release_buffers GLXEW_GET_VAR(__GLXEW_MESA_release_buffers)</span>
<a name="glxew.h-919"></a>
<a name="glxew.h-920"></a><span class="cp">#endif </span><span class="cm">/* GLX_MESA_release_buffers */</span><span class="cp"></span>
<a name="glxew.h-921"></a>
<a name="glxew.h-922"></a><span class="cm">/* ------------------------- GLX_MESA_set_3dfx_mode ------------------------ */</span>
<a name="glxew.h-923"></a>
<a name="glxew.h-924"></a><span class="cp">#ifndef GLX_MESA_set_3dfx_mode</span>
<a name="glxew.h-925"></a><span class="cp">#define GLX_MESA_set_3dfx_mode 1</span>
<a name="glxew.h-926"></a>
<a name="glxew.h-927"></a><span class="cp">#define GLX_3DFX_WINDOW_MODE_MESA 0x1</span>
<a name="glxew.h-928"></a><span class="cp">#define GLX_3DFX_FULLSCREEN_MODE_MESA 0x2</span>
<a name="glxew.h-929"></a>
<a name="glxew.h-930"></a><span class="k">typedef</span> <span class="nf">GLboolean</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSET3DFXMODEMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLint</span> <span class="n">mode</span><span class="p">);</span>
<a name="glxew.h-931"></a>
<a name="glxew.h-932"></a><span class="cp">#define glXSet3DfxModeMESA GLXEW_GET_FUN(__glewXSet3DfxModeMESA)</span>
<a name="glxew.h-933"></a>
<a name="glxew.h-934"></a><span class="cp">#define GLXEW_MESA_set_3dfx_mode GLXEW_GET_VAR(__GLXEW_MESA_set_3dfx_mode)</span>
<a name="glxew.h-935"></a>
<a name="glxew.h-936"></a><span class="cp">#endif </span><span class="cm">/* GLX_MESA_set_3dfx_mode */</span><span class="cp"></span>
<a name="glxew.h-937"></a>
<a name="glxew.h-938"></a><span class="cm">/* ------------------------- GLX_MESA_swap_control ------------------------- */</span>
<a name="glxew.h-939"></a>
<a name="glxew.h-940"></a><span class="cp">#ifndef GLX_MESA_swap_control</span>
<a name="glxew.h-941"></a><span class="cp">#define GLX_MESA_swap_control 1</span>
<a name="glxew.h-942"></a>
<a name="glxew.h-943"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETSWAPINTERVALMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-944"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSWAPINTERVALMESAPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">interval</span><span class="p">);</span>
<a name="glxew.h-945"></a>
<a name="glxew.h-946"></a><span class="cp">#define glXGetSwapIntervalMESA GLXEW_GET_FUN(__glewXGetSwapIntervalMESA)</span>
<a name="glxew.h-947"></a><span class="cp">#define glXSwapIntervalMESA GLXEW_GET_FUN(__glewXSwapIntervalMESA)</span>
<a name="glxew.h-948"></a>
<a name="glxew.h-949"></a><span class="cp">#define GLXEW_MESA_swap_control GLXEW_GET_VAR(__GLXEW_MESA_swap_control)</span>
<a name="glxew.h-950"></a>
<a name="glxew.h-951"></a><span class="cp">#endif </span><span class="cm">/* GLX_MESA_swap_control */</span><span class="cp"></span>
<a name="glxew.h-952"></a>
<a name="glxew.h-953"></a><span class="cm">/* --------------------------- GLX_NV_copy_buffer -------------------------- */</span>
<a name="glxew.h-954"></a>
<a name="glxew.h-955"></a><span class="cp">#ifndef GLX_NV_copy_buffer</span>
<a name="glxew.h-956"></a><span class="cp">#define GLX_NV_copy_buffer 1</span>
<a name="glxew.h-957"></a>
<a name="glxew.h-958"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCOPYBUFFERSUBDATANVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">readCtx</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">writeCtx</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">readTarget</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">writeTarget</span><span class="p">,</span> <span class="n">GLintptr</span> <span class="n">readOffset</span><span class="p">,</span> <span class="n">GLintptr</span> <span class="n">writeOffset</span><span class="p">,</span> <span class="n">GLsizeiptr</span> <span class="n">size</span><span class="p">);</span>
<a name="glxew.h-959"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXNAMEDCOPYBUFFERSUBDATANVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">readCtx</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">writeCtx</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">readBuffer</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">writeBuffer</span><span class="p">,</span> <span class="n">GLintptr</span> <span class="n">readOffset</span><span class="p">,</span> <span class="n">GLintptr</span> <span class="n">writeOffset</span><span class="p">,</span> <span class="n">GLsizeiptr</span> <span class="n">size</span><span class="p">);</span>
<a name="glxew.h-960"></a>
<a name="glxew.h-961"></a><span class="cp">#define glXCopyBufferSubDataNV GLXEW_GET_FUN(__glewXCopyBufferSubDataNV)</span>
<a name="glxew.h-962"></a><span class="cp">#define glXNamedCopyBufferSubDataNV GLXEW_GET_FUN(__glewXNamedCopyBufferSubDataNV)</span>
<a name="glxew.h-963"></a>
<a name="glxew.h-964"></a><span class="cp">#define GLXEW_NV_copy_buffer GLXEW_GET_VAR(__GLXEW_NV_copy_buffer)</span>
<a name="glxew.h-965"></a>
<a name="glxew.h-966"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_copy_buffer */</span><span class="cp"></span>
<a name="glxew.h-967"></a>
<a name="glxew.h-968"></a><span class="cm">/* --------------------------- GLX_NV_copy_image --------------------------- */</span>
<a name="glxew.h-969"></a>
<a name="glxew.h-970"></a><span class="cp">#ifndef GLX_NV_copy_image</span>
<a name="glxew.h-971"></a><span class="cp">#define GLX_NV_copy_image 1</span>
<a name="glxew.h-972"></a>
<a name="glxew.h-973"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCOPYIMAGESUBDATANVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">srcCtx</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">srcName</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">srcTarget</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcLevel</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcX</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcY</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">srcZ</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">dstCtx</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">dstName</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">dstTarget</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstLevel</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstX</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstY</span><span class="p">,</span> <span class="n">GLint</span> <span class="n">dstZ</span><span class="p">,</span> <span class="n">GLsizei</span> <span class="n">width</span><span class="p">,</span> <span class="n">GLsizei</span> <span class="n">height</span><span class="p">,</span> <span class="n">GLsizei</span> <span class="n">depth</span><span class="p">);</span>
<a name="glxew.h-974"></a>
<a name="glxew.h-975"></a><span class="cp">#define glXCopyImageSubDataNV GLXEW_GET_FUN(__glewXCopyImageSubDataNV)</span>
<a name="glxew.h-976"></a>
<a name="glxew.h-977"></a><span class="cp">#define GLXEW_NV_copy_image GLXEW_GET_VAR(__GLXEW_NV_copy_image)</span>
<a name="glxew.h-978"></a>
<a name="glxew.h-979"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_copy_image */</span><span class="cp"></span>
<a name="glxew.h-980"></a>
<a name="glxew.h-981"></a><span class="cm">/* ------------------------ GLX_NV_delay_before_swap ----------------------- */</span>
<a name="glxew.h-982"></a>
<a name="glxew.h-983"></a><span class="cp">#ifndef GLX_NV_delay_before_swap</span>
<a name="glxew.h-984"></a><span class="cp">#define GLX_NV_delay_before_swap 1</span>
<a name="glxew.h-985"></a>
<a name="glxew.h-986"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDELAYBEFORESWAPNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">seconds</span><span class="p">);</span>
<a name="glxew.h-987"></a>
<a name="glxew.h-988"></a><span class="cp">#define glXDelayBeforeSwapNV GLXEW_GET_FUN(__glewXDelayBeforeSwapNV)</span>
<a name="glxew.h-989"></a>
<a name="glxew.h-990"></a><span class="cp">#define GLXEW_NV_delay_before_swap GLXEW_GET_VAR(__GLXEW_NV_delay_before_swap)</span>
<a name="glxew.h-991"></a>
<a name="glxew.h-992"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_delay_before_swap */</span><span class="cp"></span>
<a name="glxew.h-993"></a>
<a name="glxew.h-994"></a><span class="cm">/* -------------------------- GLX_NV_float_buffer -------------------------- */</span>
<a name="glxew.h-995"></a>
<a name="glxew.h-996"></a><span class="cp">#ifndef GLX_NV_float_buffer</span>
<a name="glxew.h-997"></a><span class="cp">#define GLX_NV_float_buffer 1</span>
<a name="glxew.h-998"></a>
<a name="glxew.h-999"></a><span class="cp">#define GLX_FLOAT_COMPONENTS_NV 0x20B0</span>
<a name="glxew.h-1000"></a>
<a name="glxew.h-1001"></a><span class="cp">#define GLXEW_NV_float_buffer GLXEW_GET_VAR(__GLXEW_NV_float_buffer)</span>
<a name="glxew.h-1002"></a>
<a name="glxew.h-1003"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_float_buffer */</span><span class="cp"></span>
<a name="glxew.h-1004"></a>
<a name="glxew.h-1005"></a><span class="cm">/* ---------------------- GLX_NV_multisample_coverage ---------------------- */</span>
<a name="glxew.h-1006"></a>
<a name="glxew.h-1007"></a><span class="cp">#ifndef GLX_NV_multisample_coverage</span>
<a name="glxew.h-1008"></a><span class="cp">#define GLX_NV_multisample_coverage 1</span>
<a name="glxew.h-1009"></a>
<a name="glxew.h-1010"></a><span class="cp">#define GLX_COLOR_SAMPLES_NV 0x20B3</span>
<a name="glxew.h-1011"></a><span class="cp">#define GLX_COVERAGE_SAMPLES_NV 100001</span>
<a name="glxew.h-1012"></a>
<a name="glxew.h-1013"></a><span class="cp">#define GLXEW_NV_multisample_coverage GLXEW_GET_VAR(__GLXEW_NV_multisample_coverage)</span>
<a name="glxew.h-1014"></a>
<a name="glxew.h-1015"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_multisample_coverage */</span><span class="cp"></span>
<a name="glxew.h-1016"></a>
<a name="glxew.h-1017"></a><span class="cm">/* -------------------------- GLX_NV_present_video ------------------------- */</span>
<a name="glxew.h-1018"></a>
<a name="glxew.h-1019"></a><span class="cp">#ifndef GLX_NV_present_video</span>
<a name="glxew.h-1020"></a><span class="cp">#define GLX_NV_present_video 1</span>
<a name="glxew.h-1021"></a>
<a name="glxew.h-1022"></a><span class="cp">#define GLX_NUM_VIDEO_SLOTS_NV 0x20F0</span>
<a name="glxew.h-1023"></a>
<a name="glxew.h-1024"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDVIDEODEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">video_slot</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">video_device</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-1025"></a><span class="k">typedef</span> <span class="kt">unsigned</span> <span class="kt">int</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXENUMERATEVIDEODEVICESNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">nelements</span><span class="p">);</span>
<a name="glxew.h-1026"></a>
<a name="glxew.h-1027"></a><span class="cp">#define glXBindVideoDeviceNV GLXEW_GET_FUN(__glewXBindVideoDeviceNV)</span>
<a name="glxew.h-1028"></a><span class="cp">#define glXEnumerateVideoDevicesNV GLXEW_GET_FUN(__glewXEnumerateVideoDevicesNV)</span>
<a name="glxew.h-1029"></a>
<a name="glxew.h-1030"></a><span class="cp">#define GLXEW_NV_present_video GLXEW_GET_VAR(__GLXEW_NV_present_video)</span>
<a name="glxew.h-1031"></a>
<a name="glxew.h-1032"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_present_video */</span><span class="cp"></span>
<a name="glxew.h-1033"></a>
<a name="glxew.h-1034"></a><span class="cm">/* ------------------ GLX_NV_robustness_video_memory_purge ----------------- */</span>
<a name="glxew.h-1035"></a>
<a name="glxew.h-1036"></a><span class="cp">#ifndef GLX_NV_robustness_video_memory_purge</span>
<a name="glxew.h-1037"></a><span class="cp">#define GLX_NV_robustness_video_memory_purge 1</span>
<a name="glxew.h-1038"></a>
<a name="glxew.h-1039"></a><span class="cp">#define GLX_GENERATE_RESET_ON_VIDEO_MEMORY_PURGE_NV 0x20F7</span>
<a name="glxew.h-1040"></a>
<a name="glxew.h-1041"></a><span class="cp">#define GLXEW_NV_robustness_video_memory_purge GLXEW_GET_VAR(__GLXEW_NV_robustness_video_memory_purge)</span>
<a name="glxew.h-1042"></a>
<a name="glxew.h-1043"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_robustness_video_memory_purge */</span><span class="cp"></span>
<a name="glxew.h-1044"></a>
<a name="glxew.h-1045"></a><span class="cm">/* --------------------------- GLX_NV_swap_group --------------------------- */</span>
<a name="glxew.h-1046"></a>
<a name="glxew.h-1047"></a><span class="cp">#ifndef GLX_NV_swap_group</span>
<a name="glxew.h-1048"></a><span class="cp">#define GLX_NV_swap_group 1</span>
<a name="glxew.h-1049"></a>
<a name="glxew.h-1050"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDSWAPBARRIERNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">group</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">barrier</span><span class="p">);</span>
<a name="glxew.h-1051"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXJOINSWAPGROUPNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="n">GLuint</span> <span class="n">group</span><span class="p">);</span>
<a name="glxew.h-1052"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYFRAMECOUNTNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="n">GLuint</span> <span class="o">*</span><span class="n">count</span><span class="p">);</span>
<a name="glxew.h-1053"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYMAXSWAPGROUPSNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="n">GLuint</span> <span class="o">*</span><span class="n">maxGroups</span><span class="p">,</span> <span class="n">GLuint</span> <span class="o">*</span><span class="n">maxBarriers</span><span class="p">);</span>
<a name="glxew.h-1054"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYSWAPGROUPNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="n">GLuint</span> <span class="o">*</span><span class="n">group</span><span class="p">,</span> <span class="n">GLuint</span> <span class="o">*</span><span class="n">barrier</span><span class="p">);</span>
<a name="glxew.h-1055"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXRESETFRAMECOUNTNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">);</span>
<a name="glxew.h-1056"></a>
<a name="glxew.h-1057"></a><span class="cp">#define glXBindSwapBarrierNV GLXEW_GET_FUN(__glewXBindSwapBarrierNV)</span>
<a name="glxew.h-1058"></a><span class="cp">#define glXJoinSwapGroupNV GLXEW_GET_FUN(__glewXJoinSwapGroupNV)</span>
<a name="glxew.h-1059"></a><span class="cp">#define glXQueryFrameCountNV GLXEW_GET_FUN(__glewXQueryFrameCountNV)</span>
<a name="glxew.h-1060"></a><span class="cp">#define glXQueryMaxSwapGroupsNV GLXEW_GET_FUN(__glewXQueryMaxSwapGroupsNV)</span>
<a name="glxew.h-1061"></a><span class="cp">#define glXQuerySwapGroupNV GLXEW_GET_FUN(__glewXQuerySwapGroupNV)</span>
<a name="glxew.h-1062"></a><span class="cp">#define glXResetFrameCountNV GLXEW_GET_FUN(__glewXResetFrameCountNV)</span>
<a name="glxew.h-1063"></a>
<a name="glxew.h-1064"></a><span class="cp">#define GLXEW_NV_swap_group GLXEW_GET_VAR(__GLXEW_NV_swap_group)</span>
<a name="glxew.h-1065"></a>
<a name="glxew.h-1066"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_swap_group */</span><span class="cp"></span>
<a name="glxew.h-1067"></a>
<a name="glxew.h-1068"></a><span class="cm">/* ----------------------- GLX_NV_vertex_array_range ----------------------- */</span>
<a name="glxew.h-1069"></a>
<a name="glxew.h-1070"></a><span class="cp">#ifndef GLX_NV_vertex_array_range</span>
<a name="glxew.h-1071"></a><span class="cp">#define GLX_NV_vertex_array_range 1</span>
<a name="glxew.h-1072"></a>
<a name="glxew.h-1073"></a><span class="k">typedef</span> <span class="kt">void</span> <span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXALLOCATEMEMORYNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">GLsizei</span> <span class="n">size</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">readFrequency</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">writeFrequency</span><span class="p">,</span> <span class="n">GLfloat</span> <span class="n">priority</span><span class="p">);</span>
<a name="glxew.h-1074"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXFREEMEMORYNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span> <span class="o">*</span><span class="n">pointer</span><span class="p">);</span>
<a name="glxew.h-1075"></a>
<a name="glxew.h-1076"></a><span class="cp">#define glXAllocateMemoryNV GLXEW_GET_FUN(__glewXAllocateMemoryNV)</span>
<a name="glxew.h-1077"></a><span class="cp">#define glXFreeMemoryNV GLXEW_GET_FUN(__glewXFreeMemoryNV)</span>
<a name="glxew.h-1078"></a>
<a name="glxew.h-1079"></a><span class="cp">#define GLXEW_NV_vertex_array_range GLXEW_GET_VAR(__GLXEW_NV_vertex_array_range)</span>
<a name="glxew.h-1080"></a>
<a name="glxew.h-1081"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_vertex_array_range */</span><span class="cp"></span>
<a name="glxew.h-1082"></a>
<a name="glxew.h-1083"></a><span class="cm">/* -------------------------- GLX_NV_video_capture ------------------------- */</span>
<a name="glxew.h-1084"></a>
<a name="glxew.h-1085"></a><span class="cp">#ifndef GLX_NV_video_capture</span>
<a name="glxew.h-1086"></a><span class="cp">#define GLX_NV_video_capture 1</span>
<a name="glxew.h-1087"></a>
<a name="glxew.h-1088"></a><span class="cp">#define GLX_DEVICE_ID_NV 0x20CD</span>
<a name="glxew.h-1089"></a><span class="cp">#define GLX_UNIQUE_ID_NV 0x20CE</span>
<a name="glxew.h-1090"></a><span class="cp">#define GLX_NUM_VIDEO_CAPTURE_SLOTS_NV 0x20CF</span>
<a name="glxew.h-1091"></a>
<a name="glxew.h-1092"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1093"></a>
<a name="glxew.h-1094"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">video_capture_slot</span><span class="p">,</span> <span class="n">GLXVideoCaptureDeviceNV</span> <span class="n">device</span><span class="p">);</span>
<a name="glxew.h-1095"></a><span class="k">typedef</span> <span class="n">GLXVideoCaptureDeviceNV</span> <span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXENUMERATEVIDEOCAPTUREDEVICESNVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">nelements</span><span class="p">);</span>
<a name="glxew.h-1096"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXLOCKVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXVideoCaptureDeviceNV</span> <span class="n">device</span><span class="p">);</span>
<a name="glxew.h-1097"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXVideoCaptureDeviceNV</span> <span class="n">device</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-1098"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXRELEASEVIDEOCAPTUREDEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXVideoCaptureDeviceNV</span> <span class="n">device</span><span class="p">);</span>
<a name="glxew.h-1099"></a>
<a name="glxew.h-1100"></a><span class="cp">#define glXBindVideoCaptureDeviceNV GLXEW_GET_FUN(__glewXBindVideoCaptureDeviceNV)</span>
<a name="glxew.h-1101"></a><span class="cp">#define glXEnumerateVideoCaptureDevicesNV GLXEW_GET_FUN(__glewXEnumerateVideoCaptureDevicesNV)</span>
<a name="glxew.h-1102"></a><span class="cp">#define glXLockVideoCaptureDeviceNV GLXEW_GET_FUN(__glewXLockVideoCaptureDeviceNV)</span>
<a name="glxew.h-1103"></a><span class="cp">#define glXQueryVideoCaptureDeviceNV GLXEW_GET_FUN(__glewXQueryVideoCaptureDeviceNV)</span>
<a name="glxew.h-1104"></a><span class="cp">#define glXReleaseVideoCaptureDeviceNV GLXEW_GET_FUN(__glewXReleaseVideoCaptureDeviceNV)</span>
<a name="glxew.h-1105"></a>
<a name="glxew.h-1106"></a><span class="cp">#define GLXEW_NV_video_capture GLXEW_GET_VAR(__GLXEW_NV_video_capture)</span>
<a name="glxew.h-1107"></a>
<a name="glxew.h-1108"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_video_capture */</span><span class="cp"></span>
<a name="glxew.h-1109"></a>
<a name="glxew.h-1110"></a><span class="cm">/* ---------------------------- GLX_NV_video_out --------------------------- */</span>
<a name="glxew.h-1111"></a>
<a name="glxew.h-1112"></a><span class="cp">#ifndef GLX_NV_video_out</span>
<a name="glxew.h-1113"></a><span class="cp">#define GLX_NV_video_out 1</span>
<a name="glxew.h-1114"></a>
<a name="glxew.h-1115"></a><span class="cp">#define GLX_VIDEO_OUT_COLOR_NV 0x20C3</span>
<a name="glxew.h-1116"></a><span class="cp">#define GLX_VIDEO_OUT_ALPHA_NV 0x20C4</span>
<a name="glxew.h-1117"></a><span class="cp">#define GLX_VIDEO_OUT_DEPTH_NV 0x20C5</span>
<a name="glxew.h-1118"></a><span class="cp">#define GLX_VIDEO_OUT_COLOR_AND_ALPHA_NV 0x20C6</span>
<a name="glxew.h-1119"></a><span class="cp">#define GLX_VIDEO_OUT_COLOR_AND_DEPTH_NV 0x20C7</span>
<a name="glxew.h-1120"></a><span class="cp">#define GLX_VIDEO_OUT_FRAME_NV 0x20C8</span>
<a name="glxew.h-1121"></a><span class="cp">#define GLX_VIDEO_OUT_FIELD_1_NV 0x20C9</span>
<a name="glxew.h-1122"></a><span class="cp">#define GLX_VIDEO_OUT_FIELD_2_NV 0x20CA</span>
<a name="glxew.h-1123"></a><span class="cp">#define GLX_VIDEO_OUT_STACKED_FIELDS_1_2_NV 0x20CB</span>
<a name="glxew.h-1124"></a><span class="cp">#define GLX_VIDEO_OUT_STACKED_FIELDS_2_1_NV 0x20CC</span>
<a name="glxew.h-1125"></a>
<a name="glxew.h-1126"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDVIDEOIMAGENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXVideoDeviceNV</span> <span class="n">VideoDevice</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iVideoBuffer</span><span class="p">);</span>
<a name="glxew.h-1127"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETVIDEODEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">numVideoDevices</span><span class="p">,</span> <span class="n">GLXVideoDeviceNV</span> <span class="o">*</span><span class="n">pVideoDevice</span><span class="p">);</span>
<a name="glxew.h-1128"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETVIDEOINFONVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="n">GLXVideoDeviceNV</span> <span class="n">VideoDevice</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="o">*</span><span class="n">pulCounterOutputPbuffer</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="o">*</span><span class="n">pulCounterOutputVideo</span><span class="p">);</span>
<a name="glxew.h-1129"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXRELEASEVIDEODEVICENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="n">GLXVideoDeviceNV</span> <span class="n">VideoDevice</span><span class="p">);</span>
<a name="glxew.h-1130"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXRELEASEVIDEOIMAGENVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">);</span>
<a name="glxew.h-1131"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSENDPBUFFERTOVIDEONVPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">,</span> <span class="kt">int</span> <span class="n">iBufferType</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="o">*</span><span class="n">pulCounterPbuffer</span><span class="p">,</span> <span class="n">GLboolean</span> <span class="n">bBlock</span><span class="p">);</span>
<a name="glxew.h-1132"></a>
<a name="glxew.h-1133"></a><span class="cp">#define glXBindVideoImageNV GLXEW_GET_FUN(__glewXBindVideoImageNV)</span>
<a name="glxew.h-1134"></a><span class="cp">#define glXGetVideoDeviceNV GLXEW_GET_FUN(__glewXGetVideoDeviceNV)</span>
<a name="glxew.h-1135"></a><span class="cp">#define glXGetVideoInfoNV GLXEW_GET_FUN(__glewXGetVideoInfoNV)</span>
<a name="glxew.h-1136"></a><span class="cp">#define glXReleaseVideoDeviceNV GLXEW_GET_FUN(__glewXReleaseVideoDeviceNV)</span>
<a name="glxew.h-1137"></a><span class="cp">#define glXReleaseVideoImageNV GLXEW_GET_FUN(__glewXReleaseVideoImageNV)</span>
<a name="glxew.h-1138"></a><span class="cp">#define glXSendPbufferToVideoNV GLXEW_GET_FUN(__glewXSendPbufferToVideoNV)</span>
<a name="glxew.h-1139"></a>
<a name="glxew.h-1140"></a><span class="cp">#define GLXEW_NV_video_out GLXEW_GET_VAR(__GLXEW_NV_video_out)</span>
<a name="glxew.h-1141"></a>
<a name="glxew.h-1142"></a><span class="cp">#endif </span><span class="cm">/* GLX_NV_video_out */</span><span class="cp"></span>
<a name="glxew.h-1143"></a>
<a name="glxew.h-1144"></a><span class="cm">/* -------------------------- GLX_OML_swap_method -------------------------- */</span>
<a name="glxew.h-1145"></a>
<a name="glxew.h-1146"></a><span class="cp">#ifndef GLX_OML_swap_method</span>
<a name="glxew.h-1147"></a><span class="cp">#define GLX_OML_swap_method 1</span>
<a name="glxew.h-1148"></a>
<a name="glxew.h-1149"></a><span class="cp">#define GLX_SWAP_METHOD_OML 0x8060</span>
<a name="glxew.h-1150"></a><span class="cp">#define GLX_SWAP_EXCHANGE_OML 0x8061</span>
<a name="glxew.h-1151"></a><span class="cp">#define GLX_SWAP_COPY_OML 0x8062</span>
<a name="glxew.h-1152"></a><span class="cp">#define GLX_SWAP_UNDEFINED_OML 0x8063</span>
<a name="glxew.h-1153"></a>
<a name="glxew.h-1154"></a><span class="cp">#define GLXEW_OML_swap_method GLXEW_GET_VAR(__GLXEW_OML_swap_method)</span>
<a name="glxew.h-1155"></a>
<a name="glxew.h-1156"></a><span class="cp">#endif </span><span class="cm">/* GLX_OML_swap_method */</span><span class="cp"></span>
<a name="glxew.h-1157"></a>
<a name="glxew.h-1158"></a><span class="cm">/* -------------------------- GLX_OML_sync_control ------------------------- */</span>
<a name="glxew.h-1159"></a>
<a name="glxew.h-1160"></a><span class="cp">#ifndef GLX_OML_sync_control</span>
<a name="glxew.h-1161"></a><span class="cp">#define GLX_OML_sync_control 1</span>
<a name="glxew.h-1162"></a>
<a name="glxew.h-1163"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETMSCRATEOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int32_t</span><span class="o">*</span> <span class="n">numerator</span><span class="p">,</span> <span class="kt">int32_t</span><span class="o">*</span> <span class="n">denominator</span><span class="p">);</span>
<a name="glxew.h-1164"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETSYNCVALUESOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">ust</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">msc</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">sbc</span><span class="p">);</span>
<a name="glxew.h-1165"></a><span class="k">typedef</span> <span class="nf">int64_t</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSWAPBUFFERSMSCOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int64_t</span> <span class="n">target_msc</span><span class="p">,</span> <span class="kt">int64_t</span> <span class="n">divisor</span><span class="p">,</span> <span class="kt">int64_t</span> <span class="n">remainder</span><span class="p">);</span>
<a name="glxew.h-1166"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXWAITFORMSCOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int64_t</span> <span class="n">target_msc</span><span class="p">,</span> <span class="kt">int64_t</span> <span class="n">divisor</span><span class="p">,</span> <span class="kt">int64_t</span> <span class="n">remainder</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">ust</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">msc</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">sbc</span><span class="p">);</span>
<a name="glxew.h-1167"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXWAITFORSBCOMLPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int64_t</span> <span class="n">target_sbc</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">ust</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">msc</span><span class="p">,</span> <span class="kt">int64_t</span><span class="o">*</span> <span class="n">sbc</span><span class="p">);</span>
<a name="glxew.h-1168"></a>
<a name="glxew.h-1169"></a><span class="cp">#define glXGetMscRateOML GLXEW_GET_FUN(__glewXGetMscRateOML)</span>
<a name="glxew.h-1170"></a><span class="cp">#define glXGetSyncValuesOML GLXEW_GET_FUN(__glewXGetSyncValuesOML)</span>
<a name="glxew.h-1171"></a><span class="cp">#define glXSwapBuffersMscOML GLXEW_GET_FUN(__glewXSwapBuffersMscOML)</span>
<a name="glxew.h-1172"></a><span class="cp">#define glXWaitForMscOML GLXEW_GET_FUN(__glewXWaitForMscOML)</span>
<a name="glxew.h-1173"></a><span class="cp">#define glXWaitForSbcOML GLXEW_GET_FUN(__glewXWaitForSbcOML)</span>
<a name="glxew.h-1174"></a>
<a name="glxew.h-1175"></a><span class="cp">#define GLXEW_OML_sync_control GLXEW_GET_VAR(__GLXEW_OML_sync_control)</span>
<a name="glxew.h-1176"></a>
<a name="glxew.h-1177"></a><span class="cp">#endif </span><span class="cm">/* GLX_OML_sync_control */</span><span class="cp"></span>
<a name="glxew.h-1178"></a>
<a name="glxew.h-1179"></a><span class="cm">/* ------------------------ GLX_SGIS_blended_overlay ----------------------- */</span>
<a name="glxew.h-1180"></a>
<a name="glxew.h-1181"></a><span class="cp">#ifndef GLX_SGIS_blended_overlay</span>
<a name="glxew.h-1182"></a><span class="cp">#define GLX_SGIS_blended_overlay 1</span>
<a name="glxew.h-1183"></a>
<a name="glxew.h-1184"></a><span class="cp">#define GLX_BLENDED_RGBA_SGIS 0x8025</span>
<a name="glxew.h-1185"></a>
<a name="glxew.h-1186"></a><span class="cp">#define GLXEW_SGIS_blended_overlay GLXEW_GET_VAR(__GLXEW_SGIS_blended_overlay)</span>
<a name="glxew.h-1187"></a>
<a name="glxew.h-1188"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIS_blended_overlay */</span><span class="cp"></span>
<a name="glxew.h-1189"></a>
<a name="glxew.h-1190"></a><span class="cm">/* -------------------------- GLX_SGIS_color_range ------------------------- */</span>
<a name="glxew.h-1191"></a>
<a name="glxew.h-1192"></a><span class="cp">#ifndef GLX_SGIS_color_range</span>
<a name="glxew.h-1193"></a><span class="cp">#define GLX_SGIS_color_range 1</span>
<a name="glxew.h-1194"></a>
<a name="glxew.h-1195"></a><span class="cp">#define GLXEW_SGIS_color_range GLXEW_GET_VAR(__GLXEW_SGIS_color_range)</span>
<a name="glxew.h-1196"></a>
<a name="glxew.h-1197"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIS_color_range */</span><span class="cp"></span>
<a name="glxew.h-1198"></a>
<a name="glxew.h-1199"></a><span class="cm">/* -------------------------- GLX_SGIS_multisample ------------------------- */</span>
<a name="glxew.h-1200"></a>
<a name="glxew.h-1201"></a><span class="cp">#ifndef GLX_SGIS_multisample</span>
<a name="glxew.h-1202"></a><span class="cp">#define GLX_SGIS_multisample 1</span>
<a name="glxew.h-1203"></a>
<a name="glxew.h-1204"></a><span class="cp">#define GLX_SAMPLE_BUFFERS_SGIS 100000</span>
<a name="glxew.h-1205"></a><span class="cp">#define GLX_SAMPLES_SGIS 100001</span>
<a name="glxew.h-1206"></a>
<a name="glxew.h-1207"></a><span class="cp">#define GLXEW_SGIS_multisample GLXEW_GET_VAR(__GLXEW_SGIS_multisample)</span>
<a name="glxew.h-1208"></a>
<a name="glxew.h-1209"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIS_multisample */</span><span class="cp"></span>
<a name="glxew.h-1210"></a>
<a name="glxew.h-1211"></a><span class="cm">/* ---------------------- GLX_SGIS_shared_multisample ---------------------- */</span>
<a name="glxew.h-1212"></a>
<a name="glxew.h-1213"></a><span class="cp">#ifndef GLX_SGIS_shared_multisample</span>
<a name="glxew.h-1214"></a><span class="cp">#define GLX_SGIS_shared_multisample 1</span>
<a name="glxew.h-1215"></a>
<a name="glxew.h-1216"></a><span class="cp">#define GLX_MULTISAMPLE_SUB_RECT_WIDTH_SGIS 0x8026</span>
<a name="glxew.h-1217"></a><span class="cp">#define GLX_MULTISAMPLE_SUB_RECT_HEIGHT_SGIS 0x8027</span>
<a name="glxew.h-1218"></a>
<a name="glxew.h-1219"></a><span class="cp">#define GLXEW_SGIS_shared_multisample GLXEW_GET_VAR(__GLXEW_SGIS_shared_multisample)</span>
<a name="glxew.h-1220"></a>
<a name="glxew.h-1221"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIS_shared_multisample */</span><span class="cp"></span>
<a name="glxew.h-1222"></a>
<a name="glxew.h-1223"></a><span class="cm">/* --------------------------- GLX_SGIX_fbconfig --------------------------- */</span>
<a name="glxew.h-1224"></a>
<a name="glxew.h-1225"></a><span class="cp">#ifndef GLX_SGIX_fbconfig</span>
<a name="glxew.h-1226"></a><span class="cp">#define GLX_SGIX_fbconfig 1</span>
<a name="glxew.h-1227"></a>
<a name="glxew.h-1228"></a><span class="cp">#define GLX_RGBA_BIT_SGIX 0x00000001</span>
<a name="glxew.h-1229"></a><span class="cp">#define GLX_WINDOW_BIT_SGIX 0x00000001</span>
<a name="glxew.h-1230"></a><span class="cp">#define GLX_COLOR_INDEX_BIT_SGIX 0x00000002</span>
<a name="glxew.h-1231"></a><span class="cp">#define GLX_PIXMAP_BIT_SGIX 0x00000002</span>
<a name="glxew.h-1232"></a><span class="cp">#define GLX_SCREEN_EXT 0x800C</span>
<a name="glxew.h-1233"></a><span class="cp">#define GLX_DRAWABLE_TYPE_SGIX 0x8010</span>
<a name="glxew.h-1234"></a><span class="cp">#define GLX_RENDER_TYPE_SGIX 0x8011</span>
<a name="glxew.h-1235"></a><span class="cp">#define GLX_X_RENDERABLE_SGIX 0x8012</span>
<a name="glxew.h-1236"></a><span class="cp">#define GLX_FBCONFIG_ID_SGIX 0x8013</span>
<a name="glxew.h-1237"></a><span class="cp">#define GLX_RGBA_TYPE_SGIX 0x8014</span>
<a name="glxew.h-1238"></a><span class="cp">#define GLX_COLOR_INDEX_TYPE_SGIX 0x8015</span>
<a name="glxew.h-1239"></a>
<a name="glxew.h-1240"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXFBConfigIDSGIX</span><span class="p">;</span>
<a name="glxew.h-1241"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="n">__GLXFBConfigRec</span> <span class="o">*</span><span class="n">GLXFBConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1242"></a>
<a name="glxew.h-1243"></a><span class="k">typedef</span> <span class="n">GLXFBConfigSGIX</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCHOOSEFBCONFIGSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="k">const</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">nelements</span><span class="p">);</span>
<a name="glxew.h-1244"></a><span class="k">typedef</span> <span class="nf">GLXContext</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATECONTEXTWITHCONFIGSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="kt">int</span> <span class="n">render_type</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">share_list</span><span class="p">,</span> <span class="n">Bool</span> <span class="n">direct</span><span class="p">);</span>
<a name="glxew.h-1245"></a><span class="k">typedef</span> <span class="nf">GLXPixmap</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEGLXPIXMAPWITHCONFIGSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="n">Pixmap</span> <span class="n">pixmap</span><span class="p">);</span>
<a name="glxew.h-1246"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETFBCONFIGATTRIBSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfigSGIX</span> <span class="n">config</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-1247"></a><span class="k">typedef</span> <span class="nf">GLXFBConfigSGIX</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETFBCONFIGFROMVISUALSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">XVisualInfo</span> <span class="o">*</span><span class="n">vis</span><span class="p">);</span>
<a name="glxew.h-1248"></a><span class="k">typedef</span> <span class="n">XVisualInfo</span><span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETVISUALFROMFBCONFIGSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">);</span>
<a name="glxew.h-1249"></a>
<a name="glxew.h-1250"></a><span class="cp">#define glXChooseFBConfigSGIX GLXEW_GET_FUN(__glewXChooseFBConfigSGIX)</span>
<a name="glxew.h-1251"></a><span class="cp">#define glXCreateContextWithConfigSGIX GLXEW_GET_FUN(__glewXCreateContextWithConfigSGIX)</span>
<a name="glxew.h-1252"></a><span class="cp">#define glXCreateGLXPixmapWithConfigSGIX GLXEW_GET_FUN(__glewXCreateGLXPixmapWithConfigSGIX)</span>
<a name="glxew.h-1253"></a><span class="cp">#define glXGetFBConfigAttribSGIX GLXEW_GET_FUN(__glewXGetFBConfigAttribSGIX)</span>
<a name="glxew.h-1254"></a><span class="cp">#define glXGetFBConfigFromVisualSGIX GLXEW_GET_FUN(__glewXGetFBConfigFromVisualSGIX)</span>
<a name="glxew.h-1255"></a><span class="cp">#define glXGetVisualFromFBConfigSGIX GLXEW_GET_FUN(__glewXGetVisualFromFBConfigSGIX)</span>
<a name="glxew.h-1256"></a>
<a name="glxew.h-1257"></a><span class="cp">#define GLXEW_SGIX_fbconfig GLXEW_GET_VAR(__GLXEW_SGIX_fbconfig)</span>
<a name="glxew.h-1258"></a>
<a name="glxew.h-1259"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIX_fbconfig */</span><span class="cp"></span>
<a name="glxew.h-1260"></a>
<a name="glxew.h-1261"></a><span class="cm">/* --------------------------- GLX_SGIX_hyperpipe -------------------------- */</span>
<a name="glxew.h-1262"></a>
<a name="glxew.h-1263"></a><span class="cp">#ifndef GLX_SGIX_hyperpipe</span>
<a name="glxew.h-1264"></a><span class="cp">#define GLX_SGIX_hyperpipe 1</span>
<a name="glxew.h-1265"></a>
<a name="glxew.h-1266"></a><span class="cp">#define GLX_HYPERPIPE_DISPLAY_PIPE_SGIX 0x00000001</span>
<a name="glxew.h-1267"></a><span class="cp">#define GLX_PIPE_RECT_SGIX 0x00000001</span>
<a name="glxew.h-1268"></a><span class="cp">#define GLX_HYPERPIPE_RENDER_PIPE_SGIX 0x00000002</span>
<a name="glxew.h-1269"></a><span class="cp">#define GLX_PIPE_RECT_LIMITS_SGIX 0x00000002</span>
<a name="glxew.h-1270"></a><span class="cp">#define GLX_HYPERPIPE_STEREO_SGIX 0x00000003</span>
<a name="glxew.h-1271"></a><span class="cp">#define GLX_HYPERPIPE_PIXEL_AVERAGE_SGIX 0x00000004</span>
<a name="glxew.h-1272"></a><span class="cp">#define GLX_HYPERPIPE_PIPE_NAME_LENGTH_SGIX 80</span>
<a name="glxew.h-1273"></a><span class="cp">#define GLX_BAD_HYPERPIPE_CONFIG_SGIX 91</span>
<a name="glxew.h-1274"></a><span class="cp">#define GLX_BAD_HYPERPIPE_SGIX 92</span>
<a name="glxew.h-1275"></a><span class="cp">#define GLX_HYPERPIPE_ID_SGIX 0x8030</span>
<a name="glxew.h-1276"></a>
<a name="glxew.h-1277"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="p">{</span>
<a name="glxew.h-1278"></a>  <span class="kt">char</span> <span class="n">pipeName</span><span class="p">[</span><span class="n">GLX_HYPERPIPE_PIPE_NAME_LENGTH_SGIX</span><span class="p">];</span> 
<a name="glxew.h-1279"></a>  <span class="kt">int</span>  <span class="n">networkId</span><span class="p">;</span> 
<a name="glxew.h-1280"></a><span class="p">}</span> <span class="n">GLXHyperpipeNetworkSGIX</span><span class="p">;</span>
<a name="glxew.h-1281"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="p">{</span>
<a name="glxew.h-1282"></a>  <span class="kt">char</span> <span class="n">pipeName</span><span class="p">[</span><span class="n">GLX_HYPERPIPE_PIPE_NAME_LENGTH_SGIX</span><span class="p">];</span> 
<a name="glxew.h-1283"></a>  <span class="kt">int</span> <span class="n">XOrigin</span><span class="p">;</span> 
<a name="glxew.h-1284"></a>  <span class="kt">int</span> <span class="n">YOrigin</span><span class="p">;</span> 
<a name="glxew.h-1285"></a>  <span class="kt">int</span> <span class="n">maxHeight</span><span class="p">;</span> 
<a name="glxew.h-1286"></a>  <span class="kt">int</span> <span class="n">maxWidth</span><span class="p">;</span> 
<a name="glxew.h-1287"></a><span class="p">}</span> <span class="n">GLXPipeRectLimits</span><span class="p">;</span>
<a name="glxew.h-1288"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="p">{</span>
<a name="glxew.h-1289"></a>  <span class="kt">char</span> <span class="n">pipeName</span><span class="p">[</span><span class="n">GLX_HYPERPIPE_PIPE_NAME_LENGTH_SGIX</span><span class="p">];</span> 
<a name="glxew.h-1290"></a>  <span class="kt">int</span> <span class="n">channel</span><span class="p">;</span> 
<a name="glxew.h-1291"></a>  <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">participationType</span><span class="p">;</span> 
<a name="glxew.h-1292"></a>  <span class="kt">int</span> <span class="n">timeSlice</span><span class="p">;</span> 
<a name="glxew.h-1293"></a><span class="p">}</span> <span class="n">GLXHyperpipeConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1294"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="p">{</span>
<a name="glxew.h-1295"></a>  <span class="kt">char</span> <span class="n">pipeName</span><span class="p">[</span><span class="n">GLX_HYPERPIPE_PIPE_NAME_LENGTH_SGIX</span><span class="p">];</span> 
<a name="glxew.h-1296"></a>  <span class="kt">int</span> <span class="n">srcXOrigin</span><span class="p">;</span> 
<a name="glxew.h-1297"></a>  <span class="kt">int</span> <span class="n">srcYOrigin</span><span class="p">;</span> 
<a name="glxew.h-1298"></a>  <span class="kt">int</span> <span class="n">srcWidth</span><span class="p">;</span> 
<a name="glxew.h-1299"></a>  <span class="kt">int</span> <span class="n">srcHeight</span><span class="p">;</span> 
<a name="glxew.h-1300"></a>  <span class="kt">int</span> <span class="n">destXOrigin</span><span class="p">;</span> 
<a name="glxew.h-1301"></a>  <span class="kt">int</span> <span class="n">destYOrigin</span><span class="p">;</span> 
<a name="glxew.h-1302"></a>  <span class="kt">int</span> <span class="n">destWidth</span><span class="p">;</span> 
<a name="glxew.h-1303"></a>  <span class="kt">int</span> <span class="n">destHeight</span><span class="p">;</span> 
<a name="glxew.h-1304"></a><span class="p">}</span> <span class="n">GLXPipeRect</span><span class="p">;</span>
<a name="glxew.h-1305"></a>
<a name="glxew.h-1306"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDHYPERPIPESGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">hpId</span><span class="p">);</span>
<a name="glxew.h-1307"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDESTROYHYPERPIPECONFIGSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">hpId</span><span class="p">);</span>
<a name="glxew.h-1308"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXHYPERPIPEATTRIBSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">timeSlice</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attrib</span><span class="p">,</span> <span class="kt">int</span> <span class="n">size</span><span class="p">,</span> <span class="kt">void</span> <span class="o">*</span><span class="n">attribList</span><span class="p">);</span>
<a name="glxew.h-1309"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXHYPERPIPECONFIGSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">networkId</span><span class="p">,</span> <span class="kt">int</span> <span class="n">npipes</span><span class="p">,</span> <span class="n">GLXHyperpipeConfigSGIX</span> <span class="o">*</span><span class="n">cfg</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">hpId</span><span class="p">);</span>
<a name="glxew.h-1310"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYHYPERPIPEATTRIBSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">timeSlice</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attrib</span><span class="p">,</span> <span class="kt">int</span> <span class="n">size</span><span class="p">,</span> <span class="kt">void</span> <span class="o">*</span><span class="n">returnAttribList</span><span class="p">);</span>
<a name="glxew.h-1311"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYHYPERPIPEBESTATTRIBSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">timeSlice</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attrib</span><span class="p">,</span> <span class="kt">int</span> <span class="n">size</span><span class="p">,</span> <span class="kt">void</span> <span class="o">*</span><span class="n">attribList</span><span class="p">,</span> <span class="kt">void</span> <span class="o">*</span><span class="n">returnAttribList</span><span class="p">);</span>
<a name="glxew.h-1312"></a><span class="k">typedef</span> <span class="n">GLXHyperpipeConfigSGIX</span> <span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYHYPERPIPECONFIGSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">hpId</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">npipes</span><span class="p">);</span>
<a name="glxew.h-1313"></a><span class="k">typedef</span> <span class="n">GLXHyperpipeNetworkSGIX</span> <span class="o">*</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYHYPERPIPENETWORKSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">npipes</span><span class="p">);</span>
<a name="glxew.h-1314"></a>
<a name="glxew.h-1315"></a><span class="cp">#define glXBindHyperpipeSGIX GLXEW_GET_FUN(__glewXBindHyperpipeSGIX)</span>
<a name="glxew.h-1316"></a><span class="cp">#define glXDestroyHyperpipeConfigSGIX GLXEW_GET_FUN(__glewXDestroyHyperpipeConfigSGIX)</span>
<a name="glxew.h-1317"></a><span class="cp">#define glXHyperpipeAttribSGIX GLXEW_GET_FUN(__glewXHyperpipeAttribSGIX)</span>
<a name="glxew.h-1318"></a><span class="cp">#define glXHyperpipeConfigSGIX GLXEW_GET_FUN(__glewXHyperpipeConfigSGIX)</span>
<a name="glxew.h-1319"></a><span class="cp">#define glXQueryHyperpipeAttribSGIX GLXEW_GET_FUN(__glewXQueryHyperpipeAttribSGIX)</span>
<a name="glxew.h-1320"></a><span class="cp">#define glXQueryHyperpipeBestAttribSGIX GLXEW_GET_FUN(__glewXQueryHyperpipeBestAttribSGIX)</span>
<a name="glxew.h-1321"></a><span class="cp">#define glXQueryHyperpipeConfigSGIX GLXEW_GET_FUN(__glewXQueryHyperpipeConfigSGIX)</span>
<a name="glxew.h-1322"></a><span class="cp">#define glXQueryHyperpipeNetworkSGIX GLXEW_GET_FUN(__glewXQueryHyperpipeNetworkSGIX)</span>
<a name="glxew.h-1323"></a>
<a name="glxew.h-1324"></a><span class="cp">#define GLXEW_SGIX_hyperpipe GLXEW_GET_VAR(__GLXEW_SGIX_hyperpipe)</span>
<a name="glxew.h-1325"></a>
<a name="glxew.h-1326"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIX_hyperpipe */</span><span class="cp"></span>
<a name="glxew.h-1327"></a>
<a name="glxew.h-1328"></a><span class="cm">/* ---------------------------- GLX_SGIX_pbuffer --------------------------- */</span>
<a name="glxew.h-1329"></a>
<a name="glxew.h-1330"></a><span class="cp">#ifndef GLX_SGIX_pbuffer</span>
<a name="glxew.h-1331"></a><span class="cp">#define GLX_SGIX_pbuffer 1</span>
<a name="glxew.h-1332"></a>
<a name="glxew.h-1333"></a><span class="cp">#define GLX_FRONT_LEFT_BUFFER_BIT_SGIX 0x00000001</span>
<a name="glxew.h-1334"></a><span class="cp">#define GLX_FRONT_RIGHT_BUFFER_BIT_SGIX 0x00000002</span>
<a name="glxew.h-1335"></a><span class="cp">#define GLX_BACK_LEFT_BUFFER_BIT_SGIX 0x00000004</span>
<a name="glxew.h-1336"></a><span class="cp">#define GLX_PBUFFER_BIT_SGIX 0x00000004</span>
<a name="glxew.h-1337"></a><span class="cp">#define GLX_BACK_RIGHT_BUFFER_BIT_SGIX 0x00000008</span>
<a name="glxew.h-1338"></a><span class="cp">#define GLX_AUX_BUFFERS_BIT_SGIX 0x00000010</span>
<a name="glxew.h-1339"></a><span class="cp">#define GLX_DEPTH_BUFFER_BIT_SGIX 0x00000020</span>
<a name="glxew.h-1340"></a><span class="cp">#define GLX_STENCIL_BUFFER_BIT_SGIX 0x00000040</span>
<a name="glxew.h-1341"></a><span class="cp">#define GLX_ACCUM_BUFFER_BIT_SGIX 0x00000080</span>
<a name="glxew.h-1342"></a><span class="cp">#define GLX_SAMPLE_BUFFERS_BIT_SGIX 0x00000100</span>
<a name="glxew.h-1343"></a><span class="cp">#define GLX_MAX_PBUFFER_WIDTH_SGIX 0x8016</span>
<a name="glxew.h-1344"></a><span class="cp">#define GLX_MAX_PBUFFER_HEIGHT_SGIX 0x8017</span>
<a name="glxew.h-1345"></a><span class="cp">#define GLX_MAX_PBUFFER_PIXELS_SGIX 0x8018</span>
<a name="glxew.h-1346"></a><span class="cp">#define GLX_OPTIMAL_PBUFFER_WIDTH_SGIX 0x8019</span>
<a name="glxew.h-1347"></a><span class="cp">#define GLX_OPTIMAL_PBUFFER_HEIGHT_SGIX 0x801A</span>
<a name="glxew.h-1348"></a><span class="cp">#define GLX_PRESERVED_CONTENTS_SGIX 0x801B</span>
<a name="glxew.h-1349"></a><span class="cp">#define GLX_LARGEST_PBUFFER_SGIX 0x801C</span>
<a name="glxew.h-1350"></a><span class="cp">#define GLX_WIDTH_SGIX 0x801D</span>
<a name="glxew.h-1351"></a><span class="cp">#define GLX_HEIGHT_SGIX 0x801E</span>
<a name="glxew.h-1352"></a><span class="cp">#define GLX_EVENT_MASK_SGIX 0x801F</span>
<a name="glxew.h-1353"></a><span class="cp">#define GLX_DAMAGED_SGIX 0x8020</span>
<a name="glxew.h-1354"></a><span class="cp">#define GLX_SAVED_SGIX 0x8021</span>
<a name="glxew.h-1355"></a><span class="cp">#define GLX_WINDOW_SGIX 0x8022</span>
<a name="glxew.h-1356"></a><span class="cp">#define GLX_PBUFFER_SGIX 0x8023</span>
<a name="glxew.h-1357"></a><span class="cp">#define GLX_BUFFER_CLOBBER_MASK_SGIX 0x08000000</span>
<a name="glxew.h-1358"></a>
<a name="glxew.h-1359"></a><span class="k">typedef</span> <span class="n">XID</span> <span class="n">GLXPbufferSGIX</span><span class="p">;</span>
<a name="glxew.h-1360"></a><span class="k">typedef</span> <span class="k">struct</span> <span class="p">{</span> <span class="kt">int</span> <span class="n">type</span><span class="p">;</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="n">serial</span><span class="p">;</span> <span class="n">Bool</span> <span class="n">send_event</span><span class="p">;</span> <span class="n">Display</span> <span class="o">*</span><span class="n">display</span><span class="p">;</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">;</span> <span class="kt">int</span> <span class="n">event_type</span><span class="p">;</span> <span class="kt">int</span> <span class="n">draw_type</span><span class="p">;</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">mask</span><span class="p">;</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="n">y</span><span class="p">;</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="n">height</span><span class="p">;</span> <span class="kt">int</span> <span class="n">count</span><span class="p">;</span> <span class="p">}</span> <span class="n">GLXBufferClobberEventSGIX</span><span class="p">;</span>
<a name="glxew.h-1361"></a>
<a name="glxew.h-1362"></a><span class="k">typedef</span> <span class="nf">GLXPbuffer</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCREATEGLXPBUFFERSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXFBConfig</span> <span class="n">config</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">width</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="n">height</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">attrib_list</span><span class="p">);</span>
<a name="glxew.h-1363"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXDESTROYGLXPBUFFERSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">);</span>
<a name="glxew.h-1364"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETSELECTEDEVENTSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="o">*</span><span class="n">mask</span><span class="p">);</span>
<a name="glxew.h-1365"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYGLXPBUFFERSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXPbuffer</span> <span class="n">pbuf</span><span class="p">,</span> <span class="kt">int</span> <span class="n">attribute</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span> <span class="o">*</span><span class="n">value</span><span class="p">);</span>
<a name="glxew.h-1366"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSELECTEVENTSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="n">mask</span><span class="p">);</span>
<a name="glxew.h-1367"></a>
<a name="glxew.h-1368"></a><span class="cp">#define glXCreateGLXPbufferSGIX GLXEW_GET_FUN(__glewXCreateGLXPbufferSGIX)</span>
<a name="glxew.h-1369"></a><span class="cp">#define glXDestroyGLXPbufferSGIX GLXEW_GET_FUN(__glewXDestroyGLXPbufferSGIX)</span>
<a name="glxew.h-1370"></a><span class="cp">#define glXGetSelectedEventSGIX GLXEW_GET_FUN(__glewXGetSelectedEventSGIX)</span>
<a name="glxew.h-1371"></a><span class="cp">#define glXQueryGLXPbufferSGIX GLXEW_GET_FUN(__glewXQueryGLXPbufferSGIX)</span>
<a name="glxew.h-1372"></a><span class="cp">#define glXSelectEventSGIX GLXEW_GET_FUN(__glewXSelectEventSGIX)</span>
<a name="glxew.h-1373"></a>
<a name="glxew.h-1374"></a><span class="cp">#define GLXEW_SGIX_pbuffer GLXEW_GET_VAR(__GLXEW_SGIX_pbuffer)</span>
<a name="glxew.h-1375"></a>
<a name="glxew.h-1376"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIX_pbuffer */</span><span class="cp"></span>
<a name="glxew.h-1377"></a>
<a name="glxew.h-1378"></a><span class="cm">/* ------------------------- GLX_SGIX_swap_barrier ------------------------- */</span>
<a name="glxew.h-1379"></a>
<a name="glxew.h-1380"></a><span class="cp">#ifndef GLX_SGIX_swap_barrier</span>
<a name="glxew.h-1381"></a><span class="cp">#define GLX_SGIX_swap_barrier 1</span>
<a name="glxew.h-1382"></a>
<a name="glxew.h-1383"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDSWAPBARRIERSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="kt">int</span> <span class="n">barrier</span><span class="p">);</span>
<a name="glxew.h-1384"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYMAXSWAPBARRIERSSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">max</span><span class="p">);</span>
<a name="glxew.h-1385"></a>
<a name="glxew.h-1386"></a><span class="cp">#define glXBindSwapBarrierSGIX GLXEW_GET_FUN(__glewXBindSwapBarrierSGIX)</span>
<a name="glxew.h-1387"></a><span class="cp">#define glXQueryMaxSwapBarriersSGIX GLXEW_GET_FUN(__glewXQueryMaxSwapBarriersSGIX)</span>
<a name="glxew.h-1388"></a>
<a name="glxew.h-1389"></a><span class="cp">#define GLXEW_SGIX_swap_barrier GLXEW_GET_VAR(__GLXEW_SGIX_swap_barrier)</span>
<a name="glxew.h-1390"></a>
<a name="glxew.h-1391"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIX_swap_barrier */</span><span class="cp"></span>
<a name="glxew.h-1392"></a>
<a name="glxew.h-1393"></a><span class="cm">/* -------------------------- GLX_SGIX_swap_group -------------------------- */</span>
<a name="glxew.h-1394"></a>
<a name="glxew.h-1395"></a><span class="cp">#ifndef GLX_SGIX_swap_group</span>
<a name="glxew.h-1396"></a><span class="cp">#define GLX_SGIX_swap_group 1</span>
<a name="glxew.h-1397"></a>
<a name="glxew.h-1398"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXJOINSWAPGROUPSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span> <span class="o">*</span><span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">drawable</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">member</span><span class="p">);</span>
<a name="glxew.h-1399"></a>
<a name="glxew.h-1400"></a><span class="cp">#define glXJoinSwapGroupSGIX GLXEW_GET_FUN(__glewXJoinSwapGroupSGIX)</span>
<a name="glxew.h-1401"></a>
<a name="glxew.h-1402"></a><span class="cp">#define GLXEW_SGIX_swap_group GLXEW_GET_VAR(__GLXEW_SGIX_swap_group)</span>
<a name="glxew.h-1403"></a>
<a name="glxew.h-1404"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIX_swap_group */</span><span class="cp"></span>
<a name="glxew.h-1405"></a>
<a name="glxew.h-1406"></a><span class="cm">/* ------------------------- GLX_SGIX_video_resize ------------------------- */</span>
<a name="glxew.h-1407"></a>
<a name="glxew.h-1408"></a><span class="cp">#ifndef GLX_SGIX_video_resize</span>
<a name="glxew.h-1409"></a><span class="cp">#define GLX_SGIX_video_resize 1</span>
<a name="glxew.h-1410"></a>
<a name="glxew.h-1411"></a><span class="cp">#define GLX_SYNC_FRAME_SGIX 0x00000000</span>
<a name="glxew.h-1412"></a><span class="cp">#define GLX_SYNC_SWAP_SGIX 0x00000001</span>
<a name="glxew.h-1413"></a>
<a name="glxew.h-1414"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXBINDCHANNELTOWINDOWSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">channel</span><span class="p">,</span> <span class="n">Window</span> <span class="n">window</span><span class="p">);</span>
<a name="glxew.h-1415"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCHANNELRECTSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">channel</span><span class="p">,</span> <span class="kt">int</span> <span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="n">w</span><span class="p">,</span> <span class="kt">int</span> <span class="n">h</span><span class="p">);</span>
<a name="glxew.h-1416"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCHANNELRECTSYNCSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">channel</span><span class="p">,</span> <span class="n">GLenum</span> <span class="n">synctype</span><span class="p">);</span>
<a name="glxew.h-1417"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYCHANNELDELTASSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">channel</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">x</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">y</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">w</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">h</span><span class="p">);</span>
<a name="glxew.h-1418"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXQUERYCHANNELRECTSGIXPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="kt">int</span> <span class="n">screen</span><span class="p">,</span> <span class="kt">int</span> <span class="n">channel</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">dx</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">dy</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">dw</span><span class="p">,</span> <span class="kt">int</span> <span class="o">*</span><span class="n">dh</span><span class="p">);</span>
<a name="glxew.h-1419"></a>
<a name="glxew.h-1420"></a><span class="cp">#define glXBindChannelToWindowSGIX GLXEW_GET_FUN(__glewXBindChannelToWindowSGIX)</span>
<a name="glxew.h-1421"></a><span class="cp">#define glXChannelRectSGIX GLXEW_GET_FUN(__glewXChannelRectSGIX)</span>
<a name="glxew.h-1422"></a><span class="cp">#define glXChannelRectSyncSGIX GLXEW_GET_FUN(__glewXChannelRectSyncSGIX)</span>
<a name="glxew.h-1423"></a><span class="cp">#define glXQueryChannelDeltasSGIX GLXEW_GET_FUN(__glewXQueryChannelDeltasSGIX)</span>
<a name="glxew.h-1424"></a><span class="cp">#define glXQueryChannelRectSGIX GLXEW_GET_FUN(__glewXQueryChannelRectSGIX)</span>
<a name="glxew.h-1425"></a>
<a name="glxew.h-1426"></a><span class="cp">#define GLXEW_SGIX_video_resize GLXEW_GET_VAR(__GLXEW_SGIX_video_resize)</span>
<a name="glxew.h-1427"></a>
<a name="glxew.h-1428"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIX_video_resize */</span><span class="cp"></span>
<a name="glxew.h-1429"></a>
<a name="glxew.h-1430"></a><span class="cm">/* ---------------------- GLX_SGIX_visual_select_group --------------------- */</span>
<a name="glxew.h-1431"></a>
<a name="glxew.h-1432"></a><span class="cp">#ifndef GLX_SGIX_visual_select_group</span>
<a name="glxew.h-1433"></a><span class="cp">#define GLX_SGIX_visual_select_group 1</span>
<a name="glxew.h-1434"></a>
<a name="glxew.h-1435"></a><span class="cp">#define GLX_VISUAL_SELECT_GROUP_SGIX 0x8028</span>
<a name="glxew.h-1436"></a>
<a name="glxew.h-1437"></a><span class="cp">#define GLXEW_SGIX_visual_select_group GLXEW_GET_VAR(__GLXEW_SGIX_visual_select_group)</span>
<a name="glxew.h-1438"></a>
<a name="glxew.h-1439"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGIX_visual_select_group */</span><span class="cp"></span>
<a name="glxew.h-1440"></a>
<a name="glxew.h-1441"></a><span class="cm">/* ---------------------------- GLX_SGI_cushion ---------------------------- */</span>
<a name="glxew.h-1442"></a>
<a name="glxew.h-1443"></a><span class="cp">#ifndef GLX_SGI_cushion</span>
<a name="glxew.h-1444"></a><span class="cp">#define GLX_SGI_cushion 1</span>
<a name="glxew.h-1445"></a>
<a name="glxew.h-1446"></a><span class="k">typedef</span> <span class="nf">void</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXCUSHIONSGIPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">Window</span> <span class="n">window</span><span class="p">,</span> <span class="kt">float</span> <span class="n">cushion</span><span class="p">);</span>
<a name="glxew.h-1447"></a>
<a name="glxew.h-1448"></a><span class="cp">#define glXCushionSGI GLXEW_GET_FUN(__glewXCushionSGI)</span>
<a name="glxew.h-1449"></a>
<a name="glxew.h-1450"></a><span class="cp">#define GLXEW_SGI_cushion GLXEW_GET_VAR(__GLXEW_SGI_cushion)</span>
<a name="glxew.h-1451"></a>
<a name="glxew.h-1452"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGI_cushion */</span><span class="cp"></span>
<a name="glxew.h-1453"></a>
<a name="glxew.h-1454"></a><span class="cm">/* ----------------------- GLX_SGI_make_current_read ----------------------- */</span>
<a name="glxew.h-1455"></a>
<a name="glxew.h-1456"></a><span class="cp">#ifndef GLX_SGI_make_current_read</span>
<a name="glxew.h-1457"></a><span class="cp">#define GLX_SGI_make_current_read 1</span>
<a name="glxew.h-1458"></a>
<a name="glxew.h-1459"></a><span class="k">typedef</span> <span class="nf">GLXDrawable</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETCURRENTREADDRAWABLESGIPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">void</span><span class="p">);</span>
<a name="glxew.h-1460"></a><span class="k">typedef</span> <span class="nf">Bool</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXMAKECURRENTREADSGIPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">draw</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">read</span><span class="p">,</span> <span class="n">GLXContext</span> <span class="n">ctx</span><span class="p">);</span>
<a name="glxew.h-1461"></a>
<a name="glxew.h-1462"></a><span class="cp">#define glXGetCurrentReadDrawableSGI GLXEW_GET_FUN(__glewXGetCurrentReadDrawableSGI)</span>
<a name="glxew.h-1463"></a><span class="cp">#define glXMakeCurrentReadSGI GLXEW_GET_FUN(__glewXMakeCurrentReadSGI)</span>
<a name="glxew.h-1464"></a>
<a name="glxew.h-1465"></a><span class="cp">#define GLXEW_SGI_make_current_read GLXEW_GET_VAR(__GLXEW_SGI_make_current_read)</span>
<a name="glxew.h-1466"></a>
<a name="glxew.h-1467"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGI_make_current_read */</span><span class="cp"></span>
<a name="glxew.h-1468"></a>
<a name="glxew.h-1469"></a><span class="cm">/* -------------------------- GLX_SGI_swap_control ------------------------- */</span>
<a name="glxew.h-1470"></a>
<a name="glxew.h-1471"></a><span class="cp">#ifndef GLX_SGI_swap_control</span>
<a name="glxew.h-1472"></a><span class="cp">#define GLX_SGI_swap_control 1</span>
<a name="glxew.h-1473"></a>
<a name="glxew.h-1474"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXSWAPINTERVALSGIPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">int</span> <span class="n">interval</span><span class="p">);</span>
<a name="glxew.h-1475"></a>
<a name="glxew.h-1476"></a><span class="cp">#define glXSwapIntervalSGI GLXEW_GET_FUN(__glewXSwapIntervalSGI)</span>
<a name="glxew.h-1477"></a>
<a name="glxew.h-1478"></a><span class="cp">#define GLXEW_SGI_swap_control GLXEW_GET_VAR(__GLXEW_SGI_swap_control)</span>
<a name="glxew.h-1479"></a>
<a name="glxew.h-1480"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGI_swap_control */</span><span class="cp"></span>
<a name="glxew.h-1481"></a>
<a name="glxew.h-1482"></a><span class="cm">/* --------------------------- GLX_SGI_video_sync -------------------------- */</span>
<a name="glxew.h-1483"></a>
<a name="glxew.h-1484"></a><span class="cp">#ifndef GLX_SGI_video_sync</span>
<a name="glxew.h-1485"></a><span class="cp">#define GLX_SGI_video_sync 1</span>
<a name="glxew.h-1486"></a>
<a name="glxew.h-1487"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETVIDEOSYNCSGIPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">unsigned</span> <span class="kt">int</span><span class="o">*</span> <span class="n">count</span><span class="p">);</span>
<a name="glxew.h-1488"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXWAITVIDEOSYNCSGIPROC</span><span class="p">)</span> <span class="p">(</span><span class="kt">int</span> <span class="n">divisor</span><span class="p">,</span> <span class="kt">int</span> <span class="n">remainder</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">int</span><span class="o">*</span> <span class="n">count</span><span class="p">);</span>
<a name="glxew.h-1489"></a>
<a name="glxew.h-1490"></a><span class="cp">#define glXGetVideoSyncSGI GLXEW_GET_FUN(__glewXGetVideoSyncSGI)</span>
<a name="glxew.h-1491"></a><span class="cp">#define glXWaitVideoSyncSGI GLXEW_GET_FUN(__glewXWaitVideoSyncSGI)</span>
<a name="glxew.h-1492"></a>
<a name="glxew.h-1493"></a><span class="cp">#define GLXEW_SGI_video_sync GLXEW_GET_VAR(__GLXEW_SGI_video_sync)</span>
<a name="glxew.h-1494"></a>
<a name="glxew.h-1495"></a><span class="cp">#endif </span><span class="cm">/* GLX_SGI_video_sync */</span><span class="cp"></span>
<a name="glxew.h-1496"></a>
<a name="glxew.h-1497"></a><span class="cm">/* --------------------- GLX_SUN_get_transparent_index --------------------- */</span>
<a name="glxew.h-1498"></a>
<a name="glxew.h-1499"></a><span class="cp">#ifndef GLX_SUN_get_transparent_index</span>
<a name="glxew.h-1500"></a><span class="cp">#define GLX_SUN_get_transparent_index 1</span>
<a name="glxew.h-1501"></a>
<a name="glxew.h-1502"></a><span class="k">typedef</span> <span class="nf">Status</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETTRANSPARENTINDEXSUNPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">dpy</span><span class="p">,</span> <span class="n">Window</span> <span class="n">overlay</span><span class="p">,</span> <span class="n">Window</span> <span class="n">underlay</span><span class="p">,</span> <span class="kt">unsigned</span> <span class="kt">long</span> <span class="o">*</span><span class="n">pTransparentIndex</span><span class="p">);</span>
<a name="glxew.h-1503"></a>
<a name="glxew.h-1504"></a><span class="cp">#define glXGetTransparentIndexSUN GLXEW_GET_FUN(__glewXGetTransparentIndexSUN)</span>
<a name="glxew.h-1505"></a>
<a name="glxew.h-1506"></a><span class="cp">#define GLXEW_SUN_get_transparent_index GLXEW_GET_VAR(__GLXEW_SUN_get_transparent_index)</span>
<a name="glxew.h-1507"></a>
<a name="glxew.h-1508"></a><span class="cp">#endif </span><span class="cm">/* GLX_SUN_get_transparent_index */</span><span class="cp"></span>
<a name="glxew.h-1509"></a>
<a name="glxew.h-1510"></a><span class="cm">/* -------------------------- GLX_SUN_video_resize ------------------------- */</span>
<a name="glxew.h-1511"></a>
<a name="glxew.h-1512"></a><span class="cp">#ifndef GLX_SUN_video_resize</span>
<a name="glxew.h-1513"></a><span class="cp">#define GLX_SUN_video_resize 1</span>
<a name="glxew.h-1514"></a>
<a name="glxew.h-1515"></a><span class="cp">#define GLX_VIDEO_RESIZE_SUN 0x8171</span>
<a name="glxew.h-1516"></a><span class="cp">#define GL_VIDEO_RESIZE_COMPENSATION_SUN 0x85CD</span>
<a name="glxew.h-1517"></a>
<a name="glxew.h-1518"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXGETVIDEORESIZESUNPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">window</span><span class="p">,</span> <span class="kt">float</span><span class="o">*</span> <span class="n">factor</span><span class="p">);</span>
<a name="glxew.h-1519"></a><span class="k">typedef</span> <span class="nf">int</span> <span class="p">(</span> <span class="o">*</span> <span class="n">PFNGLXVIDEORESIZESUNPROC</span><span class="p">)</span> <span class="p">(</span><span class="n">Display</span><span class="o">*</span> <span class="n">display</span><span class="p">,</span> <span class="n">GLXDrawable</span> <span class="n">window</span><span class="p">,</span> <span class="kt">float</span> <span class="n">factor</span><span class="p">);</span>
<a name="glxew.h-1520"></a>
<a name="glxew.h-1521"></a><span class="cp">#define glXGetVideoResizeSUN GLXEW_GET_FUN(__glewXGetVideoResizeSUN)</span>
<a name="glxew.h-1522"></a><span class="cp">#define glXVideoResizeSUN GLXEW_GET_FUN(__glewXVideoResizeSUN)</span>
<a name="glxew.h-1523"></a>
<a name="glxew.h-1524"></a><span class="cp">#define GLXEW_SUN_video_resize GLXEW_GET_VAR(__GLXEW_SUN_video_resize)</span>
<a name="glxew.h-1525"></a>
<a name="glxew.h-1526"></a><span class="cp">#endif </span><span class="cm">/* GLX_SUN_video_resize */</span><span class="cp"></span>
<a name="glxew.h-1527"></a>
<a name="glxew.h-1528"></a><span class="cm">/* ------------------------------------------------------------------------- */</span>
<a name="glxew.h-1529"></a>
<a name="glxew.h-1530"></a><span class="cp">#define GLXEW_FUN_EXPORT GLEW_FUN_EXPORT</span>
<a name="glxew.h-1531"></a><span class="cp">#define GLXEW_VAR_EXPORT GLEW_VAR_EXPORT</span>
<a name="glxew.h-1532"></a>
<a name="glxew.h-1533"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETCURRENTDISPLAYPROC</span> <span class="n">__glewXGetCurrentDisplay</span><span class="p">;</span>
<a name="glxew.h-1534"></a>
<a name="glxew.h-1535"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCHOOSEFBCONFIGPROC</span> <span class="n">__glewXChooseFBConfig</span><span class="p">;</span>
<a name="glxew.h-1536"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATENEWCONTEXTPROC</span> <span class="n">__glewXCreateNewContext</span><span class="p">;</span>
<a name="glxew.h-1537"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEPBUFFERPROC</span> <span class="n">__glewXCreatePbuffer</span><span class="p">;</span>
<a name="glxew.h-1538"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEPIXMAPPROC</span> <span class="n">__glewXCreatePixmap</span><span class="p">;</span>
<a name="glxew.h-1539"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEWINDOWPROC</span> <span class="n">__glewXCreateWindow</span><span class="p">;</span>
<a name="glxew.h-1540"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDESTROYPBUFFERPROC</span> <span class="n">__glewXDestroyPbuffer</span><span class="p">;</span>
<a name="glxew.h-1541"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDESTROYPIXMAPPROC</span> <span class="n">__glewXDestroyPixmap</span><span class="p">;</span>
<a name="glxew.h-1542"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDESTROYWINDOWPROC</span> <span class="n">__glewXDestroyWindow</span><span class="p">;</span>
<a name="glxew.h-1543"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETCURRENTREADDRAWABLEPROC</span> <span class="n">__glewXGetCurrentReadDrawable</span><span class="p">;</span>
<a name="glxew.h-1544"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETFBCONFIGATTRIBPROC</span> <span class="n">__glewXGetFBConfigAttrib</span><span class="p">;</span>
<a name="glxew.h-1545"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETFBCONFIGSPROC</span> <span class="n">__glewXGetFBConfigs</span><span class="p">;</span>
<a name="glxew.h-1546"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETSELECTEDEVENTPROC</span> <span class="n">__glewXGetSelectedEvent</span><span class="p">;</span>
<a name="glxew.h-1547"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETVISUALFROMFBCONFIGPROC</span> <span class="n">__glewXGetVisualFromFBConfig</span><span class="p">;</span>
<a name="glxew.h-1548"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXMAKECONTEXTCURRENTPROC</span> <span class="n">__glewXMakeContextCurrent</span><span class="p">;</span>
<a name="glxew.h-1549"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYCONTEXTPROC</span> <span class="n">__glewXQueryContext</span><span class="p">;</span>
<a name="glxew.h-1550"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYDRAWABLEPROC</span> <span class="n">__glewXQueryDrawable</span><span class="p">;</span>
<a name="glxew.h-1551"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSELECTEVENTPROC</span> <span class="n">__glewXSelectEvent</span><span class="p">;</span>
<a name="glxew.h-1552"></a>
<a name="glxew.h-1553"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBLITCONTEXTFRAMEBUFFERAMDPROC</span> <span class="n">__glewXBlitContextFramebufferAMD</span><span class="p">;</span>
<a name="glxew.h-1554"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEASSOCIATEDCONTEXTAMDPROC</span> <span class="n">__glewXCreateAssociatedContextAMD</span><span class="p">;</span>
<a name="glxew.h-1555"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEASSOCIATEDCONTEXTATTRIBSAMDPROC</span> <span class="n">__glewXCreateAssociatedContextAttribsAMD</span><span class="p">;</span>
<a name="glxew.h-1556"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDELETEASSOCIATEDCONTEXTAMDPROC</span> <span class="n">__glewXDeleteAssociatedContextAMD</span><span class="p">;</span>
<a name="glxew.h-1557"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETCONTEXTGPUIDAMDPROC</span> <span class="n">__glewXGetContextGPUIDAMD</span><span class="p">;</span>
<a name="glxew.h-1558"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETCURRENTASSOCIATEDCONTEXTAMDPROC</span> <span class="n">__glewXGetCurrentAssociatedContextAMD</span><span class="p">;</span>
<a name="glxew.h-1559"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETGPUIDSAMDPROC</span> <span class="n">__glewXGetGPUIDsAMD</span><span class="p">;</span>
<a name="glxew.h-1560"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETGPUINFOAMDPROC</span> <span class="n">__glewXGetGPUInfoAMD</span><span class="p">;</span>
<a name="glxew.h-1561"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXMAKEASSOCIATEDCONTEXTCURRENTAMDPROC</span> <span class="n">__glewXMakeAssociatedContextCurrentAMD</span><span class="p">;</span>
<a name="glxew.h-1562"></a>
<a name="glxew.h-1563"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATECONTEXTATTRIBSARBPROC</span> <span class="n">__glewXCreateContextAttribsARB</span><span class="p">;</span>
<a name="glxew.h-1564"></a>
<a name="glxew.h-1565"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDTEXIMAGEATIPROC</span> <span class="n">__glewXBindTexImageATI</span><span class="p">;</span>
<a name="glxew.h-1566"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDRAWABLEATTRIBATIPROC</span> <span class="n">__glewXDrawableAttribATI</span><span class="p">;</span>
<a name="glxew.h-1567"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXRELEASETEXIMAGEATIPROC</span> <span class="n">__glewXReleaseTexImageATI</span><span class="p">;</span>
<a name="glxew.h-1568"></a>
<a name="glxew.h-1569"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXFREECONTEXTEXTPROC</span> <span class="n">__glewXFreeContextEXT</span><span class="p">;</span>
<a name="glxew.h-1570"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETCONTEXTIDEXTPROC</span> <span class="n">__glewXGetContextIDEXT</span><span class="p">;</span>
<a name="glxew.h-1571"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXIMPORTCONTEXTEXTPROC</span> <span class="n">__glewXImportContextEXT</span><span class="p">;</span>
<a name="glxew.h-1572"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYCONTEXTINFOEXTPROC</span> <span class="n">__glewXQueryContextInfoEXT</span><span class="p">;</span>
<a name="glxew.h-1573"></a>
<a name="glxew.h-1574"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSWAPINTERVALEXTPROC</span> <span class="n">__glewXSwapIntervalEXT</span><span class="p">;</span>
<a name="glxew.h-1575"></a>
<a name="glxew.h-1576"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDTEXIMAGEEXTPROC</span> <span class="n">__glewXBindTexImageEXT</span><span class="p">;</span>
<a name="glxew.h-1577"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXRELEASETEXIMAGEEXTPROC</span> <span class="n">__glewXReleaseTexImageEXT</span><span class="p">;</span>
<a name="glxew.h-1578"></a>
<a name="glxew.h-1579"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETAGPOFFSETMESAPROC</span> <span class="n">__glewXGetAGPOffsetMESA</span><span class="p">;</span>
<a name="glxew.h-1580"></a>
<a name="glxew.h-1581"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCOPYSUBBUFFERMESAPROC</span> <span class="n">__glewXCopySubBufferMESA</span><span class="p">;</span>
<a name="glxew.h-1582"></a>
<a name="glxew.h-1583"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEGLXPIXMAPMESAPROC</span> <span class="n">__glewXCreateGLXPixmapMESA</span><span class="p">;</span>
<a name="glxew.h-1584"></a>
<a name="glxew.h-1585"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYCURRENTRENDERERINTEGERMESAPROC</span> <span class="n">__glewXQueryCurrentRendererIntegerMESA</span><span class="p">;</span>
<a name="glxew.h-1586"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYCURRENTRENDERERSTRINGMESAPROC</span> <span class="n">__glewXQueryCurrentRendererStringMESA</span><span class="p">;</span>
<a name="glxew.h-1587"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYRENDERERINTEGERMESAPROC</span> <span class="n">__glewXQueryRendererIntegerMESA</span><span class="p">;</span>
<a name="glxew.h-1588"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYRENDERERSTRINGMESAPROC</span> <span class="n">__glewXQueryRendererStringMESA</span><span class="p">;</span>
<a name="glxew.h-1589"></a>
<a name="glxew.h-1590"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXRELEASEBUFFERSMESAPROC</span> <span class="n">__glewXReleaseBuffersMESA</span><span class="p">;</span>
<a name="glxew.h-1591"></a>
<a name="glxew.h-1592"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSET3DFXMODEMESAPROC</span> <span class="n">__glewXSet3DfxModeMESA</span><span class="p">;</span>
<a name="glxew.h-1593"></a>
<a name="glxew.h-1594"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETSWAPINTERVALMESAPROC</span> <span class="n">__glewXGetSwapIntervalMESA</span><span class="p">;</span>
<a name="glxew.h-1595"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSWAPINTERVALMESAPROC</span> <span class="n">__glewXSwapIntervalMESA</span><span class="p">;</span>
<a name="glxew.h-1596"></a>
<a name="glxew.h-1597"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCOPYBUFFERSUBDATANVPROC</span> <span class="n">__glewXCopyBufferSubDataNV</span><span class="p">;</span>
<a name="glxew.h-1598"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXNAMEDCOPYBUFFERSUBDATANVPROC</span> <span class="n">__glewXNamedCopyBufferSubDataNV</span><span class="p">;</span>
<a name="glxew.h-1599"></a>
<a name="glxew.h-1600"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCOPYIMAGESUBDATANVPROC</span> <span class="n">__glewXCopyImageSubDataNV</span><span class="p">;</span>
<a name="glxew.h-1601"></a>
<a name="glxew.h-1602"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDELAYBEFORESWAPNVPROC</span> <span class="n">__glewXDelayBeforeSwapNV</span><span class="p">;</span>
<a name="glxew.h-1603"></a>
<a name="glxew.h-1604"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDVIDEODEVICENVPROC</span> <span class="n">__glewXBindVideoDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1605"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXENUMERATEVIDEODEVICESNVPROC</span> <span class="n">__glewXEnumerateVideoDevicesNV</span><span class="p">;</span>
<a name="glxew.h-1606"></a>
<a name="glxew.h-1607"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDSWAPBARRIERNVPROC</span> <span class="n">__glewXBindSwapBarrierNV</span><span class="p">;</span>
<a name="glxew.h-1608"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXJOINSWAPGROUPNVPROC</span> <span class="n">__glewXJoinSwapGroupNV</span><span class="p">;</span>
<a name="glxew.h-1609"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYFRAMECOUNTNVPROC</span> <span class="n">__glewXQueryFrameCountNV</span><span class="p">;</span>
<a name="glxew.h-1610"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYMAXSWAPGROUPSNVPROC</span> <span class="n">__glewXQueryMaxSwapGroupsNV</span><span class="p">;</span>
<a name="glxew.h-1611"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYSWAPGROUPNVPROC</span> <span class="n">__glewXQuerySwapGroupNV</span><span class="p">;</span>
<a name="glxew.h-1612"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXRESETFRAMECOUNTNVPROC</span> <span class="n">__glewXResetFrameCountNV</span><span class="p">;</span>
<a name="glxew.h-1613"></a>
<a name="glxew.h-1614"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXALLOCATEMEMORYNVPROC</span> <span class="n">__glewXAllocateMemoryNV</span><span class="p">;</span>
<a name="glxew.h-1615"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXFREEMEMORYNVPROC</span> <span class="n">__glewXFreeMemoryNV</span><span class="p">;</span>
<a name="glxew.h-1616"></a>
<a name="glxew.h-1617"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__glewXBindVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1618"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXENUMERATEVIDEOCAPTUREDEVICESNVPROC</span> <span class="n">__glewXEnumerateVideoCaptureDevicesNV</span><span class="p">;</span>
<a name="glxew.h-1619"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXLOCKVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__glewXLockVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1620"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__glewXQueryVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1621"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXRELEASEVIDEOCAPTUREDEVICENVPROC</span> <span class="n">__glewXReleaseVideoCaptureDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1622"></a>
<a name="glxew.h-1623"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDVIDEOIMAGENVPROC</span> <span class="n">__glewXBindVideoImageNV</span><span class="p">;</span>
<a name="glxew.h-1624"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETVIDEODEVICENVPROC</span> <span class="n">__glewXGetVideoDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1625"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETVIDEOINFONVPROC</span> <span class="n">__glewXGetVideoInfoNV</span><span class="p">;</span>
<a name="glxew.h-1626"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXRELEASEVIDEODEVICENVPROC</span> <span class="n">__glewXReleaseVideoDeviceNV</span><span class="p">;</span>
<a name="glxew.h-1627"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXRELEASEVIDEOIMAGENVPROC</span> <span class="n">__glewXReleaseVideoImageNV</span><span class="p">;</span>
<a name="glxew.h-1628"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSENDPBUFFERTOVIDEONVPROC</span> <span class="n">__glewXSendPbufferToVideoNV</span><span class="p">;</span>
<a name="glxew.h-1629"></a>
<a name="glxew.h-1630"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETMSCRATEOMLPROC</span> <span class="n">__glewXGetMscRateOML</span><span class="p">;</span>
<a name="glxew.h-1631"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETSYNCVALUESOMLPROC</span> <span class="n">__glewXGetSyncValuesOML</span><span class="p">;</span>
<a name="glxew.h-1632"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSWAPBUFFERSMSCOMLPROC</span> <span class="n">__glewXSwapBuffersMscOML</span><span class="p">;</span>
<a name="glxew.h-1633"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXWAITFORMSCOMLPROC</span> <span class="n">__glewXWaitForMscOML</span><span class="p">;</span>
<a name="glxew.h-1634"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXWAITFORSBCOMLPROC</span> <span class="n">__glewXWaitForSbcOML</span><span class="p">;</span>
<a name="glxew.h-1635"></a>
<a name="glxew.h-1636"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCHOOSEFBCONFIGSGIXPROC</span> <span class="n">__glewXChooseFBConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1637"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATECONTEXTWITHCONFIGSGIXPROC</span> <span class="n">__glewXCreateContextWithConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1638"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEGLXPIXMAPWITHCONFIGSGIXPROC</span> <span class="n">__glewXCreateGLXPixmapWithConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1639"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETFBCONFIGATTRIBSGIXPROC</span> <span class="n">__glewXGetFBConfigAttribSGIX</span><span class="p">;</span>
<a name="glxew.h-1640"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETFBCONFIGFROMVISUALSGIXPROC</span> <span class="n">__glewXGetFBConfigFromVisualSGIX</span><span class="p">;</span>
<a name="glxew.h-1641"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETVISUALFROMFBCONFIGSGIXPROC</span> <span class="n">__glewXGetVisualFromFBConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1642"></a>
<a name="glxew.h-1643"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDHYPERPIPESGIXPROC</span> <span class="n">__glewXBindHyperpipeSGIX</span><span class="p">;</span>
<a name="glxew.h-1644"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDESTROYHYPERPIPECONFIGSGIXPROC</span> <span class="n">__glewXDestroyHyperpipeConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1645"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXHYPERPIPEATTRIBSGIXPROC</span> <span class="n">__glewXHyperpipeAttribSGIX</span><span class="p">;</span>
<a name="glxew.h-1646"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXHYPERPIPECONFIGSGIXPROC</span> <span class="n">__glewXHyperpipeConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1647"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYHYPERPIPEATTRIBSGIXPROC</span> <span class="n">__glewXQueryHyperpipeAttribSGIX</span><span class="p">;</span>
<a name="glxew.h-1648"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYHYPERPIPEBESTATTRIBSGIXPROC</span> <span class="n">__glewXQueryHyperpipeBestAttribSGIX</span><span class="p">;</span>
<a name="glxew.h-1649"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYHYPERPIPECONFIGSGIXPROC</span> <span class="n">__glewXQueryHyperpipeConfigSGIX</span><span class="p">;</span>
<a name="glxew.h-1650"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYHYPERPIPENETWORKSGIXPROC</span> <span class="n">__glewXQueryHyperpipeNetworkSGIX</span><span class="p">;</span>
<a name="glxew.h-1651"></a>
<a name="glxew.h-1652"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCREATEGLXPBUFFERSGIXPROC</span> <span class="n">__glewXCreateGLXPbufferSGIX</span><span class="p">;</span>
<a name="glxew.h-1653"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXDESTROYGLXPBUFFERSGIXPROC</span> <span class="n">__glewXDestroyGLXPbufferSGIX</span><span class="p">;</span>
<a name="glxew.h-1654"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETSELECTEDEVENTSGIXPROC</span> <span class="n">__glewXGetSelectedEventSGIX</span><span class="p">;</span>
<a name="glxew.h-1655"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYGLXPBUFFERSGIXPROC</span> <span class="n">__glewXQueryGLXPbufferSGIX</span><span class="p">;</span>
<a name="glxew.h-1656"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSELECTEVENTSGIXPROC</span> <span class="n">__glewXSelectEventSGIX</span><span class="p">;</span>
<a name="glxew.h-1657"></a>
<a name="glxew.h-1658"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDSWAPBARRIERSGIXPROC</span> <span class="n">__glewXBindSwapBarrierSGIX</span><span class="p">;</span>
<a name="glxew.h-1659"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYMAXSWAPBARRIERSSGIXPROC</span> <span class="n">__glewXQueryMaxSwapBarriersSGIX</span><span class="p">;</span>
<a name="glxew.h-1660"></a>
<a name="glxew.h-1661"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXJOINSWAPGROUPSGIXPROC</span> <span class="n">__glewXJoinSwapGroupSGIX</span><span class="p">;</span>
<a name="glxew.h-1662"></a>
<a name="glxew.h-1663"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXBINDCHANNELTOWINDOWSGIXPROC</span> <span class="n">__glewXBindChannelToWindowSGIX</span><span class="p">;</span>
<a name="glxew.h-1664"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCHANNELRECTSGIXPROC</span> <span class="n">__glewXChannelRectSGIX</span><span class="p">;</span>
<a name="glxew.h-1665"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCHANNELRECTSYNCSGIXPROC</span> <span class="n">__glewXChannelRectSyncSGIX</span><span class="p">;</span>
<a name="glxew.h-1666"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYCHANNELDELTASSGIXPROC</span> <span class="n">__glewXQueryChannelDeltasSGIX</span><span class="p">;</span>
<a name="glxew.h-1667"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXQUERYCHANNELRECTSGIXPROC</span> <span class="n">__glewXQueryChannelRectSGIX</span><span class="p">;</span>
<a name="glxew.h-1668"></a>
<a name="glxew.h-1669"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXCUSHIONSGIPROC</span> <span class="n">__glewXCushionSGI</span><span class="p">;</span>
<a name="glxew.h-1670"></a>
<a name="glxew.h-1671"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETCURRENTREADDRAWABLESGIPROC</span> <span class="n">__glewXGetCurrentReadDrawableSGI</span><span class="p">;</span>
<a name="glxew.h-1672"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXMAKECURRENTREADSGIPROC</span> <span class="n">__glewXMakeCurrentReadSGI</span><span class="p">;</span>
<a name="glxew.h-1673"></a>
<a name="glxew.h-1674"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXSWAPINTERVALSGIPROC</span> <span class="n">__glewXSwapIntervalSGI</span><span class="p">;</span>
<a name="glxew.h-1675"></a>
<a name="glxew.h-1676"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETVIDEOSYNCSGIPROC</span> <span class="n">__glewXGetVideoSyncSGI</span><span class="p">;</span>
<a name="glxew.h-1677"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXWAITVIDEOSYNCSGIPROC</span> <span class="n">__glewXWaitVideoSyncSGI</span><span class="p">;</span>
<a name="glxew.h-1678"></a>
<a name="glxew.h-1679"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETTRANSPARENTINDEXSUNPROC</span> <span class="n">__glewXGetTransparentIndexSUN</span><span class="p">;</span>
<a name="glxew.h-1680"></a>
<a name="glxew.h-1681"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXGETVIDEORESIZESUNPROC</span> <span class="n">__glewXGetVideoResizeSUN</span><span class="p">;</span>
<a name="glxew.h-1682"></a><span class="n">GLXEW_FUN_EXPORT</span> <span class="n">PFNGLXVIDEORESIZESUNPROC</span> <span class="n">__glewXVideoResizeSUN</span><span class="p">;</span>
<a name="glxew.h-1683"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_VERSION_1_0</span><span class="p">;</span>
<a name="glxew.h-1684"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_VERSION_1_1</span><span class="p">;</span>
<a name="glxew.h-1685"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_VERSION_1_2</span><span class="p">;</span>
<a name="glxew.h-1686"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_VERSION_1_3</span><span class="p">;</span>
<a name="glxew.h-1687"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_VERSION_1_4</span><span class="p">;</span>
<a name="glxew.h-1688"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_3DFX_multisample</span><span class="p">;</span>
<a name="glxew.h-1689"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_AMD_gpu_association</span><span class="p">;</span>
<a name="glxew.h-1690"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_context_flush_control</span><span class="p">;</span>
<a name="glxew.h-1691"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_create_context</span><span class="p">;</span>
<a name="glxew.h-1692"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_create_context_no_error</span><span class="p">;</span>
<a name="glxew.h-1693"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_create_context_profile</span><span class="p">;</span>
<a name="glxew.h-1694"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_create_context_robustness</span><span class="p">;</span>
<a name="glxew.h-1695"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_fbconfig_float</span><span class="p">;</span>
<a name="glxew.h-1696"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_framebuffer_sRGB</span><span class="p">;</span>
<a name="glxew.h-1697"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_get_proc_address</span><span class="p">;</span>
<a name="glxew.h-1698"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_multisample</span><span class="p">;</span>
<a name="glxew.h-1699"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_robustness_application_isolation</span><span class="p">;</span>
<a name="glxew.h-1700"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_robustness_share_group_isolation</span><span class="p">;</span>
<a name="glxew.h-1701"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ARB_vertex_buffer_object</span><span class="p">;</span>
<a name="glxew.h-1702"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ATI_pixel_format_float</span><span class="p">;</span>
<a name="glxew.h-1703"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_ATI_render_texture</span><span class="p">;</span>
<a name="glxew.h-1704"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_buffer_age</span><span class="p">;</span>
<a name="glxew.h-1705"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_create_context_es2_profile</span><span class="p">;</span>
<a name="glxew.h-1706"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_create_context_es_profile</span><span class="p">;</span>
<a name="glxew.h-1707"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_fbconfig_packed_float</span><span class="p">;</span>
<a name="glxew.h-1708"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_framebuffer_sRGB</span><span class="p">;</span>
<a name="glxew.h-1709"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_import_context</span><span class="p">;</span>
<a name="glxew.h-1710"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_libglvnd</span><span class="p">;</span>
<a name="glxew.h-1711"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_scene_marker</span><span class="p">;</span>
<a name="glxew.h-1712"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_stereo_tree</span><span class="p">;</span>
<a name="glxew.h-1713"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_swap_control</span><span class="p">;</span>
<a name="glxew.h-1714"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_swap_control_tear</span><span class="p">;</span>
<a name="glxew.h-1715"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_texture_from_pixmap</span><span class="p">;</span>
<a name="glxew.h-1716"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_visual_info</span><span class="p">;</span>
<a name="glxew.h-1717"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_EXT_visual_rating</span><span class="p">;</span>
<a name="glxew.h-1718"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_INTEL_swap_event</span><span class="p">;</span>
<a name="glxew.h-1719"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_MESA_agp_offset</span><span class="p">;</span>
<a name="glxew.h-1720"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_MESA_copy_sub_buffer</span><span class="p">;</span>
<a name="glxew.h-1721"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_MESA_pixmap_colormap</span><span class="p">;</span>
<a name="glxew.h-1722"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_MESA_query_renderer</span><span class="p">;</span>
<a name="glxew.h-1723"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_MESA_release_buffers</span><span class="p">;</span>
<a name="glxew.h-1724"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_MESA_set_3dfx_mode</span><span class="p">;</span>
<a name="glxew.h-1725"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_MESA_swap_control</span><span class="p">;</span>
<a name="glxew.h-1726"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_copy_buffer</span><span class="p">;</span>
<a name="glxew.h-1727"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_copy_image</span><span class="p">;</span>
<a name="glxew.h-1728"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_delay_before_swap</span><span class="p">;</span>
<a name="glxew.h-1729"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_float_buffer</span><span class="p">;</span>
<a name="glxew.h-1730"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_multisample_coverage</span><span class="p">;</span>
<a name="glxew.h-1731"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_present_video</span><span class="p">;</span>
<a name="glxew.h-1732"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_robustness_video_memory_purge</span><span class="p">;</span>
<a name="glxew.h-1733"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_swap_group</span><span class="p">;</span>
<a name="glxew.h-1734"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_vertex_array_range</span><span class="p">;</span>
<a name="glxew.h-1735"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_video_capture</span><span class="p">;</span>
<a name="glxew.h-1736"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_NV_video_out</span><span class="p">;</span>
<a name="glxew.h-1737"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_OML_swap_method</span><span class="p">;</span>
<a name="glxew.h-1738"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_OML_sync_control</span><span class="p">;</span>
<a name="glxew.h-1739"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIS_blended_overlay</span><span class="p">;</span>
<a name="glxew.h-1740"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIS_color_range</span><span class="p">;</span>
<a name="glxew.h-1741"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIS_multisample</span><span class="p">;</span>
<a name="glxew.h-1742"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIS_shared_multisample</span><span class="p">;</span>
<a name="glxew.h-1743"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIX_fbconfig</span><span class="p">;</span>
<a name="glxew.h-1744"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIX_hyperpipe</span><span class="p">;</span>
<a name="glxew.h-1745"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIX_pbuffer</span><span class="p">;</span>
<a name="glxew.h-1746"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIX_swap_barrier</span><span class="p">;</span>
<a name="glxew.h-1747"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIX_swap_group</span><span class="p">;</span>
<a name="glxew.h-1748"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIX_video_resize</span><span class="p">;</span>
<a name="glxew.h-1749"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGIX_visual_select_group</span><span class="p">;</span>
<a name="glxew.h-1750"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGI_cushion</span><span class="p">;</span>
<a name="glxew.h-1751"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGI_make_current_read</span><span class="p">;</span>
<a name="glxew.h-1752"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGI_swap_control</span><span class="p">;</span>
<a name="glxew.h-1753"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SGI_video_sync</span><span class="p">;</span>
<a name="glxew.h-1754"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SUN_get_transparent_index</span><span class="p">;</span>
<a name="glxew.h-1755"></a><span class="n">GLXEW_VAR_EXPORT</span> <span class="n">GLboolean</span> <span class="n">__GLXEW_SUN_video_resize</span><span class="p">;</span>
<a name="glxew.h-1756"></a><span class="cm">/* ------------------------------------------------------------------------ */</span>
<a name="glxew.h-1757"></a>
<a name="glxew.h-1758"></a><span class="n">GLEWAPI</span> <span class="n">GLenum</span> <span class="n">GLEWAPIENTRY</span> <span class="nf">glxewInit</span> <span class="p">();</span>
<a name="glxew.h-1759"></a><span class="n">GLEWAPI</span> <span class="n">GLboolean</span> <span class="n">GLEWAPIENTRY</span> <span class="nf">glxewIsSupported</span> <span class="p">(</span><span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">name</span><span class="p">);</span>
<a name="glxew.h-1760"></a>
<a name="glxew.h-1761"></a><span class="cp">#ifndef GLXEW_GET_VAR</span>
<a name="glxew.h-1762"></a><span class="cp">#define GLXEW_GET_VAR(x) (*(const GLboolean*)&amp;x)</span>
<a name="glxew.h-1763"></a><span class="cp">#endif</span>
<a name="glxew.h-1764"></a>
<a name="glxew.h-1765"></a><span class="cp">#ifndef GLXEW_GET_FUN</span>
<a name="glxew.h-1766"></a><span class="cp">#define GLXEW_GET_FUN(x) x</span>
<a name="glxew.h-1767"></a><span class="cp">#endif</span>
<a name="glxew.h-1768"></a>
<a name="glxew.h-1769"></a><span class="n">GLEWAPI</span> <span class="n">GLboolean</span> <span class="n">GLEWAPIENTRY</span> <span class="nf">glxewGetExtension</span> <span class="p">(</span><span class="k">const</span> <span class="kt">char</span> <span class="o">*</span><span class="n">name</span><span class="p">);</span>
<a name="glxew.h-1770"></a>
<a name="glxew.h-1771"></a><span class="cp">#ifdef __cplusplus</span>
<a name="glxew.h-1772"></a><span class="p">}</span>
<a name="glxew.h-1773"></a><span class="cp">#endif</span>
<a name="glxew.h-1774"></a>
<a name="glxew.h-1775"></a><span class="cp">#endif </span><span class="cm">/* __glxew_h__ */</span><span class="cp"></span>
</pre></div>
</td></tr></table>
    </div>
  


        </div>
        
      </div>
    </div>
  </div>
  
  <div data-module="source/set-changeset" data-hash="f71033cf7729d442b43ee6f67d340a04470f80ae"></div>



  
    
    
    
  
  

  </div>

        
        
        
          
    
    
  
        
      </div>
    </div>
    <div id="code-search-cta"></div>
  </div>

      </div>
    </div>
  
</div>

<div id="adg3-dialog"></div>


  

<div data-module="components/mentions/index">
  
    
    
  
  
    
    
  
  
    
    
  
</div>
<div data-module="components/typeahead/emoji/index">
  
    
    
  
</div>

<div data-module="components/repo-typeahead/index">
  
    
    
  
</div>

    
    
  

    
    
  

    
    
  

    
    
  


  


    
    
  

    
    
  


  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  


  
  
  <aui-inline-dialog
    id="help-menu-dialog"
    data-aui-alignment="bottom right"

    
    data-aui-alignment-static="true"
    data-module="header/help-menu"
    responds-to="toggle"
    aria-hidden="true">

  <div id="help-menu-section">
    <h1 class="help-menu-heading">Help</h1>

    <form id="help-menu-search-form" class="aui" target="_blank" method="get"
        action="https://support.atlassian.com/customer/search">
      <span id="help-menu-search-icon" class="aui-icon aui-icon-large aui-iconfont-search"></span>
      <input id="help-menu-search-form-input" name="q" class="text" type="text" placeholder="Ask a question">
    </form>

    <ul id="help-menu-links">
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationHome"
            href="https://confluence.atlassian.com/x/bgozDQ" target="_blank">
          Online help
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="GitTutorials"
            href="https://www.atlassian.com/git?utm_source=bitbucket&amp;utm_medium=link&amp;utm_campaign=help_dropdown&amp;utm_content=learn_git"
            target="_blank">
          Learn Git
        </a>
      </li>
      <li>
        <a id="keyboard-shortcuts-link"
           href="#">Keyboard shortcuts</a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="DocumentationTutorials"
            href="https://confluence.atlassian.com/x/Q4sFLQ" target="_blank">
          Bitbucket tutorials
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="SiteStatus"
            href="https://status.bitbucket.org/" target="_blank">
          Site status
        </a>
      </li>
      <li>
        <a class="support-ga" data-support-gaq-page="Home"
            href="https://support.atlassian.com/bitbucket-cloud/" target="_blank">
          Support
        </a>
      </li>
    </ul>
  </div>
</aui-inline-dialog>
  


  <div class="omnibar" data-module="components/omnibar/index">
    <form class="omnibar-form aui"></form>
  </div>
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  





  

  <div class="_mustache-templates">
    
      <script id="branch-checkout-template" type="text/html">
        

<div id="checkout-branch-contents">
  <div class="command-line">
    <p>
      Check out this branch on your local machine to begin working on it.
    </p>
    <input type="text" class="checkout-command" readonly="readonly"
        
           value="git fetch && git checkout [[branchName]]"
        
        >
  </div>
  
    <div class="sourcetree-callout clone-in-sourcetree"
  data-module="components/clone/clone-in-sourcetree"
  data-https-url="https://Yanosik@bitbucket.org/Yanosik/zapart.git"
  data-ssh-url="git@bitbucket.org:Yanosik/zapart.git">

  <div>
    <button class="aui-button aui-button-primary">
      
        Check out in Sourcetree
      
    </button>
  </div>

  <p class="windows-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_win" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Windows.
    
  </p>
  <p class="mac-text">
    
      <a href="http://www.sourcetreeapp.com/?utm_source=internal&amp;utm_medium=link&amp;utm_campaign=clone_repo_mac" target="_blank">Atlassian Sourcetree</a>
      is a free Git and Mercurial client for Mac.
    
  </p>
</div>
  
</div>

      </script>
    
      <script id="branch-dialog-template" type="text/html">
        

<div class="tabbed-filter-widget branch-dialog">
  <div class="tabbed-filter">
    <input placeholder="Filter branches" class="filter-box" autosave="branch-dropdown-29632644" type="text">
    [[^ignoreTags]]
      <div class="aui-tabs horizontal-tabs aui-tabs-disabled filter-tabs">
        <ul class="tabs-menu">
          <li class="menu-item active-tab"><a href="#branches">Branches</a></li>
          <li class="menu-item"><a href="#tags">Tags</a></li>
        </ul>
      </div>
    [[/ignoreTags]]
  </div>
  
    <div class="tab-pane active-pane" id="branches" data-filter-placeholder="Filter branches">
      <ol class="filter-list">
        <li class="empty-msg">No matching branches</li>
        [[#branches]]
          
            [[#hasMultipleHeads]]
              [[#heads]]
                <li class="comprev filter-item">
                  <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/glxew.h?at=[[safeName]]"
                     title="[[name]]">
                    [[name]] ([[shortChangeset]])
                  </a>
                </li>
              [[/heads]]
            [[/hasMultipleHeads]]
            [[^hasMultipleHeads]]
              <li class="comprev filter-item">
                <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/glxew.h?at=[[safeName]]" title="[[name]]">
                  [[name]]
                </a>
              </li>
            [[/hasMultipleHeads]]
          
        [[/branches]]
      </ol>
    </div>
    <div class="tab-pane" id="tags" data-filter-placeholder="Filter tags">
      <ol class="filter-list">
        <li class="empty-msg">No matching tags</li>
        [[#tags]]
          <li class="comprev filter-item">
            <a class="pjax-trigger filter-item-link" href="/Yanosik/zapart/src/[[changeset]]/Include/GL/glxew.h?at=[[safeName]]" title="[[name]]">
              [[name]]
            </a>
          </li>
        [[/tags]]
      </ol>
    </div>
  
</div>

      </script>
    
      <script id="image-upload-template" type="text/html">
        

<form id="upload-image" method="POST"
    
      action="/xhr/Yanosik/zapart/image-upload/"
    >
  <input type='hidden' name='csrfmiddlewaretoken' value='I1E1h3QMCVq9dKJO98LvejY3Ir1OXhFGQkABzNg5qQjU48QDLRuCa8TGKuZq7IBp' />

  <div class="drop-target">
    <p class="centered">Drag image here</p>
  </div>

  
  <div>
    <button class="aui-button click-target">Select an image</button>
    <input name="file" type="file" class="hidden file-target"
           accept="image/jpeg, image/gif, image/png" />
    <input type="submit" class="hidden">
  </div>
</form>


      </script>
    
      <script id="mention-result" type="text/html">
        
<span class="mention-result">
  <span class="aui-avatar aui-avatar-small mention-result--avatar">
    <span class="aui-avatar-inner">
      <img src="[[avatar_url]]">
    </span>
  </span>
  [[#display_name]]
    <span class="display-name mention-result--display-name">[[&display_name]]</span> <small class="username mention-result--username">[[&username]]</small>
  [[/display_name]]
  [[^display_name]]
    <span class="username mention-result--username">[[&username]]</span>
  [[/display_name]]
  [[#is_teammate]][[^is_team]]
    <span class="aui-lozenge aui-lozenge-complete aui-lozenge-subtle mention-result--lozenge">teammate</span>
  [[/is_team]][[/is_teammate]]
</span>
      </script>
    
      <script id="mention-call-to-action" type="text/html">
        
[[^query]]
<li class="bb-typeahead-item">Begin typing to search for a user</li>
[[/query]]
[[#query]]
<li class="bb-typeahead-item">Continue typing to search for a user</li>
[[/query]]

      </script>
    
      <script id="mention-no-results" type="text/html">
        
[[^searching]]
<li class="bb-typeahead-item">Found no matching users for <em>[[query]]</em>.</li>
[[/searching]]
[[#searching]]
<li class="bb-typeahead-item bb-typeahead-searching">Searching for <em>[[query]]</em>.</li>
[[/searching]]

      </script>
    
      <script id="emoji-result" type="text/html">
        
<span class="emoji-result">
  <span class="emoji-result--avatar">
    <img class="emoji" src="[[src]]">
  </span>
  <span class="name emoji-result--name">[[&name]]</span>
</span>

      </script>
    
      <script id="repo-typeahead-result" type="text/html">
        <span class="aui-avatar aui-avatar-project aui-avatar-xsmall">
  <span class="aui-avatar-inner">
    <img src="[[avatar]]">
  </span>
</span>
<span class="owner">[[&owner]]</span>/<span class="slug">[[&slug]]</span>

      </script>
    
      <script id="share-form-template" type="text/html">
        

<div class="error aui-message hidden">
  <span class="aui-icon icon-error"></span>
  <div class="message"></div>
</div>
<form class="aui">
  <table class="widget bb-list aui">
    <thead>
    <tr class="assistive">
      <th class="user">User</th>
      <th class="role">Role</th>
      <th class="actions">Actions</th>
    </tr>
    </thead>
    <tbody>
      <tr class="form">
        <td colspan="2">
          <input type="text" class="text bb-user-typeahead user-or-email"
                 placeholder="Username or email address"
                 autocomplete="off"
                 data-bb-typeahead-focus="false"
                 [[#disabled]]disabled[[/disabled]]>
        </td>
        <td class="actions">
          <button type="submit" class="aui-button aui-button-light" disabled>Add</button>
        </td>
      </tr>
    </tbody>
  </table>
</form>

      </script>
    
      <script id="share-detail-template" type="text/html">
        

[[#username]]
<td class="user
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[displayName]]">
    <a href="/[[username]]/" class="user">
      <span class="aui-avatar aui-avatar-xsmall">
        <span class="aui-avatar-inner">
          <img src="[[avatar]]">
        </span>
      </span>
      <span>[[displayName]]</span>
    </a>
  </div>
</td>
[[/username]]
[[^username]]
<td class="email
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]"
    [[#error]]data-error="[[error]]"[[/error]]>
  <div title="[[email]]">
    <span class="aui-icon aui-icon-small aui-iconfont-email"></span>
    [[email]]
  </div>
</td>
[[/username]]
<td class="role
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    [[#group]]
      [[#hasCustomGroups]]
        <select class="group [[#noGroupChoices]]hidden[[/noGroupChoices]]">
          [[#groups]]
            <option value="[[slug]]"
                [[#isSelected]]selected[[/isSelected]]>
              [[name]]
            </option>
          [[/groups]]
        </select>
      [[/hasCustomGroups]]
      [[^hasCustomGroups]]
      <label>
        <input type="checkbox" class="admin"
            [[#isAdmin]]checked[[/isAdmin]]>
        Administrator
      </label>
      [[/hasCustomGroups]]
    [[/group]]
    [[^group]]
      <ul>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^read]]aui-lozenge-subtle[[/read]]"
            data-permission="read">
          read
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^write]]aui-lozenge-subtle[[/write]]"
            data-permission="write">
          write
        </li>
        <li class="permission aui-lozenge aui-lozenge-complete
            [[^admin]]aui-lozenge-subtle[[/admin]]"
            data-permission="admin">
          admin
        </li>
      </ul>
    [[/group]]
  </div>
</td>
<td class="actions
    [[#hasCustomGroups]]custom-groups[[/hasCustomGroups]]">
  <div>
    <a href="#" class="delete">
      <span class="aui-icon aui-icon-small aui-iconfont-remove">Delete</span>
    </a>
  </div>
</td>

      </script>
    
      <script id="share-team-template" type="text/html">
        

<div class="clearfix">
  <span class="team-avatar-container">
    <span class="aui-avatar aui-avatar-medium">
      <span class="aui-avatar-inner">
        <img src="[[avatar]]">
      </span>
    </span>
  </span>
  <span class="team-name-container">
    [[display_name]]
  </span>
</div>
<p class="helptext">
  
    Existing users are granted access to this team immediately.
    New users will be sent an invitation.
  
</p>
<div class="share-form"></div>

      </script>
    
      <script id="scope-list-template" type="text/html">
        <ul class="scope-list">
  [[#scopes]]
    <li class="scope-list--item">
      <span class="scope-list--icon aui-icon aui-icon-small [[icon]]"></span>
      <span class="scope-list--description">[[description]]</span>
    </li>
  [[/scopes]]
</ul>

      </script>
    
      <script id="source-changeset" type="text/html">
        

<a href="/Yanosik/zapart/src/[[raw_node]]/[[path]]?at=master"
    class="[[#selected]]highlight[[/selected]]"
    data-hash="[[node]]">
  [[#author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="[[author.avatar]]">
      </span>
    </span>
    <span class="author" title="[[raw_author]]">[[author.display_name]]</span>
  [[/author.username]]
  [[^author.username]]
    <span class="aui-avatar aui-avatar-xsmall">
      <span class="aui-avatar-inner">
        <img src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/img/default_avatar/user_blue.svg">
      </span>
    </span>
    <span class="author unmapped" title="[[raw_author]]">[[author]]</span>
  [[/author.username]]
  <time datetime="[[utctimestamp]]" data-title="true">[[utctimestamp]]</time>
  <span class="message">[[message]]</span>
</a>

      </script>
    
      <script id="embed-template" type="text/html">
        

<form class="aui inline-dialog-embed-dialog">
  <label for="embed-code-[[dialogId]]">Embed this source in another page:</label>
  <input type="text" readonly="true" value="&lt;script src=&quot;[[url]]&quot;&gt;&lt;/script&gt;" id="embed-code-[[dialogId]]" class="embed-code">
</form>

      </script>
    
      <script id="edit-form-template" type="text/html">
        


<form class="bb-content-container online-edit-form aui"
      data-repository="[[owner]]/[[slug]]"
      data-destination-repository="[[destinationOwner]]/[[destinationSlug]]"
      data-local-id="[[localID]]"
      [[#isWriter]]data-is-writer="true"[[/isWriter]]
      [[#hasPushAccess]]data-has-push-access="true"[[/hasPushAccess]]
      [[#isPullRequest]]data-is-pull-request="true"[[/isPullRequest]]
      [[#hideCreatePullRequest]]data-hide-create-pull-request="true"[[/hideCreatePullRequest]]
      data-hash="[[hash]]"
      data-branch="[[branch]]"
      data-path="[[path]]"
      data-is-create="[[isCreate]]"
      data-preview-url="/xhr/[[owner]]/[[slug]]/preview/[[hash]]/[[encodedPath]]"
      data-preview-error="We had trouble generating your preview."
      data-unsaved-changes-error="Your changes will be lost. Are you sure you want to leave?">
  <div class="bb-content-container-header">
    <div class="bb-content-container-header-primary">
      <span class="bb-content-container-heading">
        [[#isCreate]]
          [[#branch]]
            
              Creating <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
            
          [[/branch]]
          [[^branch]]
            [[#path]]
              
                Creating <span class="edit-path">[[path]]</span>
              
            [[/path]]
            [[^path]]
              
                Creating <span class="edit-path">unnamed file</span>
              
            [[/path]]
          [[/branch]]
        [[/isCreate]]
        [[^isCreate]]
          
            Editing <span class="edit-path">[[path]]</span> on branch: <strong>[[branch]]</strong>
          
        [[/isCreate]]
      </span>
    </div>
    <div class="bb-content-container-header-secondary">
      <div class="hunk-nav aui-buttons">
        <button class="prev-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Previous change">
          <span class="aui-icon aui-icon-small aui-iconfont-up">Previous change</span>
        </button>
        <button class="next-hunk-button aui-button aui-button-light"
            disabled="disabled" aria-disabled="true"
            title="Next change">
          <span class="aui-icon aui-icon-small aui-iconfont-down">Next change</span>
        </button>
      </div>
    </div>
  </div>
  <div class="bb-content-container-body has-header has-footer file-editor">
    <textarea id="id_source"></textarea>
  </div>
  <div class="preview-pane"></div>
  <div class="bb-content-container-footer">
    <div class="bb-content-container-footer-primary">
      <div id="syntax-mode" class="bb-content-container-item field">
        <label for="id_syntax-mode" class="online-edit-form--label">Syntax mode:</label>
        <select id="id_syntax-mode">
          [[#syntaxes]]
            <option value="[[#mime]][[mime]][[/mime]][[^mime]][[mode]][[/mime]]">[[name]]</option>
          [[/syntaxes]]
        </select>
      </div>
      <div id="indent-mode" class="bb-content-container-item field">
        <label for="id_indent-mode" class="online-edit-form--label">Indent mode:</label>
        <select id="id_indent-mode">
          <option value="tabs">Tabs</option>
          <option value="spaces">Spaces</option>
        </select>
      </div>
      <div id="indent-size" class="bb-content-container-item field">
        <label for="id_indent-size" class="online-edit-form--label">Indent size:</label>
        <select id="id_indent-size">
          <option value="2">2</option>
          <option value="4">4</option>
          <option value="8">8</option>
        </select>
      </div>
      <div id="wrap-mode" class="bb-content-container-item field">
        <label for="id_wrap-mode" class="online-edit-form--label">Line wrap:</label>
        <select id="id_wrap-mode">
          <option value="">Off</option>
          <option value="soft">On</option>
        </select>
      </div>
    </div>
    <div class="bb-content-container-footer-secondary">
      [[^isCreate]]
        <button class="preview-button aui-button aui-button-light"
                disabled="disabled" aria-disabled="true"
                data-preview-label="View diff"
                data-edit-label="Edit file">View diff</button>
      [[/isCreate]]
      <button class="save-button aui-button aui-button-primary"
              disabled="disabled" aria-disabled="true">Commit</button>
      [[^isCreate]]
        <a class="aui-button aui-button-link cancel-link" href="#">Cancel</a>
      [[/isCreate]]
    </div>
  </div>
</form>

      </script>
    
      <script id="commit-form-template" type="text/html">
        

<form class="aui commit-form"
      data-title="Commit changes"
      [[#isDelete]]
        data-default-message="[[filename]] deleted online with Bitbucket"
      [[/isDelete]]
      [[#isCreate]]
        data-default-message="[[filename]] created online with Bitbucket"
      [[/isCreate]]
      [[^isDelete]]
        [[^isCreate]]
          data-default-message="[[filename]] edited online with Bitbucket"
        [[/isCreate]]
      [[/isDelete]]
      data-fork-error="We had trouble creating your fork."
      data-commit-error="We had trouble committing your changes."
      data-pull-request-error="We had trouble creating your pull request."
      data-update-error="We had trouble updating your pull request."
      data-branch-conflict-error="A branch with that name already exists."
      data-forking-message="Forking repository"
      data-committing-message="Committing changes"
      data-merging-message="Branching and merging changes"
      data-creating-pr-message="Creating pull request"
      data-updating-pr-message="Updating pull request"
      data-cta-label="Commit"
      data-cancel-label="Cancel">
  [[#isDelete]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <span class="message">
        
          Committing this change will delete [[filename]] from your repository.
        
      </span>
    </div>
  [[/isDelete]]
  <div class="aui-message error hidden">
    <span class="aui-icon icon-error"></span>
    <span class="message"></span>
  </div>
  [[^isWriter]]
    <div class="aui-message info">
      <span class="aui-icon icon-info"></span>
      <p class="title">
        
          You don't have write access to this repository.
        
      </p>
      <span class="message">
        
          We'll create a fork for your changes and submit a
          pull request back to this repository.
        
      </span>
    </div>
  [[/isWriter]]
  [[#isRename]]
    <div class="field-group">
      <label for="id_path">New path</label>
      <input type="text" id="id_path" class="text" value="[[path]]"/>
    </div>
  [[/isRename]]
  <div class="field-group">
    <label for="id_message">Commit message</label>
    <textarea id="id_message" class="long-field textarea"></textarea>
  </div>
  [[^isPullRequest]]
    [[#isWriter]]
      <fieldset class="group">
        <div class="checkbox">
          [[#hasPushAccess]]
            [[^hideCreatePullRequest]]
              <input id="id_create-pullrequest" class="checkbox" type="checkbox">
              <label for="id_create-pullrequest">Create a pull request for this change</label>
            [[/hideCreatePullRequest]]
          [[/hasPushAccess]]
          [[^hasPushAccess]]
            <input id="id_create-pullrequest" class="checkbox" type="checkbox" checked="checked" aria-disabled="true" disabled="true">
            <label for="id_create-pullrequest" title="Branch restrictions do not allow you to update this branch.">Create a pull request for this change</label>
          [[/hasPushAccess]]
        </div>
      </fieldset>
      <div id="pr-fields">
        <div id="branch-name-group" class="field-group">
          <label for="id_branch-name">Branch name</label>
          <input type="text" id="id_branch-name" class="text long-field">
        </div>
        

<div class="field-group" id="id_reviewers_group">
  <label for="reviewers">Reviewers</label>

  
  <input id="reviewers" name="reviewers" type="hidden"
          value=""
          data-mention-url="/xhr/mentions/repositories/:dest_username/:dest_slug"
          data-reviewers="[]"
          data-suggested="[]"
          data-locked="[]">

  <div class="error"></div>
  <div class="suggested-reviewers"></div>

</div>

      </div>
    [[/isWriter]]
  [[/isPullRequest]]
  <button type="submit" id="id_submit">Commit</button>
</form>

      </script>
    
      <script id="merge-message-template" type="text/html">
        Merged [[hash]] into [[branch]]

[[message]]

      </script>
    
      <script id="commit-merge-error-template" type="text/html">
        



  We had trouble merging your changes. We stored them on the <strong>[[branch]]</strong> branch, so feel free to
  <a href="/[[owner]]/[[slug]]/full-commit/[[hash]]/[[path]]?at=[[encodedBranch]]">view them</a> or
  <a href="#" class="create-pull-request-link">create a pull request</a>.


      </script>
    
      <script id="selected-reviewer-template" type="text/html">
        <div class="aui-avatar aui-avatar-xsmall">
  <div class="aui-avatar-inner">
    <img src="[[avatar_url]]">
  </div>
</div>
[[display_name]]

      </script>
    
      <script id="suggested-reviewer-template" type="text/html">
        <button class="aui-button aui-button-link" type="button" tabindex="-1">[[display_name]]</button>

      </script>
    
      <script id="suggested-reviewers-template" type="text/html">
        

<span class="suggested-reviewer-list-label">Recent:</span>
<ul class="suggested-reviewer-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-form-template" type="text/html">
        <div class="omnibar-input-container">
  <input class="omnibar-input" type="text" [[#placeholder]]placeholder="[[placeholder]]"[[/placeholder]]>
</div>
<ul class="omnibar-result-group-list"></ul>

      </script>
    
      <script id="omnibar-blank-slate-template" type="text/html">
        

<div class="omnibar-blank-slate">No results found</div>

      </script>
    
      <script id="omnibar-result-group-list-item-template" type="text/html">
        <div class="omnibar-result-group-header clearfix">
  <h2 class="omnibar-result-group-label" title="[[label]]">[[label]]</h2>
  <span class="omnibar-result-group-context" title="[[context]]">[[context]]</span>
</div>
<ul class="omnibar-result-list unstyled-list"></ul>

      </script>
    
      <script id="omnibar-result-list-item-template" type="text/html">
        [[#url]]
  <a href="[[&url]]" class="omnibar-result-label">[[&label]]</a>
[[/url]]
[[^url]]
  <span class="omnibar-result-label">[[&label]]</span>
[[/url]]
[[#context]]
  <span class="omnibar-result-context">[[context]]</span>
[[/context]]

      </script>
    
  </div>




  
  


<script nonce="pudKVFKPFVGF87mQ">
  window.__initial_state__ = {"global": {"features": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "locale": "en", "geoip_country": "PL", "targetFeatures": {"pr-merge-sign-off": true, "adg3": true, "evolution": false, "clone-mirrors": true, "app-passwords": true, "diff-renames-internal": true, "search-syntax-highlighting": true, "lfs_post_beta": true, "simple-team-creation": true, "code-search-cta-launch": true, "fe_word_diff": true, "trello-boards": true, "clonebundles": true, "use-moneybucket": true, "downgrade-survey": true, "source_webitem": true, "show-guidance-message": true, "diff-renames-public": true, "code-search-cta": true, "new-signup-flow": true, "atlassian-editor": false}, "isFocusedTask": false, "teams": [], "bitbucketActions": [{"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 100, "url": "/repo/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Repository<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repository-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 110, "url": "/account/create-team/", "tab_name": null, "can_display": true, "label": "<strong>Team<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "team-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 120, "url": "/account/projects/create?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Project<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "project-create-drawer-item", "icon": ""}, {"analytics_label": null, "icon_class": "", "badge_label": null, "weight": 130, "url": "/snippets/new?owner=Yanosik", "tab_name": null, "can_display": true, "label": "<strong>Snippet<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "snippet-create-drawer-item", "icon": ""}], "targetUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "isNavigationOpen": true, "path": "/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/glxew.h", "focusedTaskBackButtonUrl": "https://bitbucket.org/Yanosik/zapart/src/f71033cf7729d442b43ee6f67d340a04470f80ae/Include/GL/?at=master", "currentUser": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "extra": {"has_atlassian_account": true}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}}, "connect": {}, "repository": {"section": {"connectActions": [], "cloneProtocol": "https", "currentRepository": {"scm": "git", "website": "", "name": "Zapart", "language": "", "links": {"clone": [{"href": "https://Yanosik@bitbucket.org/Yanosik/zapart.git", "name": "https"}, {"href": "git@bitbucket.org:Yanosik/zapart.git", "name": "ssh"}], "self": {"href": "https://bitbucket.org/!api/2.0/repositories/Yanosik/zapart"}, "html": {"href": "https://bitbucket.org/Yanosik/zapart"}, "avatar": {"href": "https://bitbucket.org/Yanosik/zapart/avatar/32/"}}, "full_name": "Yanosik/zapart", "owner": {"username": "Yanosik", "website": null, "display_name": "Jan", "account_id": "5a097ca786e7c03ff34850dd", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/users/Yanosik"}, "html": {"href": "https://bitbucket.org/Yanosik/"}, "avatar": {"href": "https://bitbucket.org/account/Yanosik/avatar/32/"}}, "created_on": "2017-11-13T11:07:12.212036+00:00", "is_staff": false, "location": null, "type": "user", "uuid": "{8aa0a974-0d88-41f1-b96a-c1ebd6c01af9}"}, "type": "repository", "slug": "zapart", "is_private": true, "uuid": "{8f59423b-9494-4138-af6f-56e97d727ae4}"}, "menuItems": [{"analytics_label": "repository.overview", "icon_class": "icon-overview", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/overview", "tab_name": "overview", "can_display": true, "label": "Overview", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-overview-link", "icon": "icon-overview"}, {"analytics_label": "repository.source", "icon_class": "icon-source", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/src", "tab_name": "source", "can_display": true, "label": "Source", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-source-link", "icon": "icon-source"}, {"analytics_label": "repository.commits", "icon_class": "icon-commits", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/commits/", "tab_name": "commits", "can_display": true, "label": "Commits", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-commits-link", "icon": "icon-commits"}, {"analytics_label": "repository.branches", "icon_class": "icon-branches", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/", "tab_name": "branches", "can_display": true, "label": "Branches", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-branches-link", "icon": "icon-branches"}, {"analytics_label": "repository.pullrequests", "icon_class": "icon-pull-requests", "badge_label": "0 open pull requests", "weight": 500, "url": "/Yanosik/zapart/pull-requests/", "tab_name": "pullrequests", "can_display": true, "label": "Pull requests", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-pullrequests-link", "icon": "icon-pull-requests"}, {"analytics_label": "site.addon", "icon_class": "aui-iconfont-build", "badge_label": null, "weight": 550, "url": "/Yanosik/zapart/addon/pipelines-installer/home", "tab_name": "repopage-kbbqoq-add-on-link", "can_display": true, "label": "Pipelines", "anchor": true, "analytics_payload": {}, "icon_url": null, "type": "connect_menu_item", "id": "repopage-kbbqoq-add-on-link", "target": "_self"}, {"analytics_label": "repository.downloads", "icon_class": "icon-downloads", "badge_label": null, "weight": 800, "url": "/Yanosik/zapart/downloads/", "tab_name": "downloads", "can_display": true, "label": "Downloads", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-downloads-link", "icon": "icon-downloads"}, {"analytics_label": "user.addon", "icon_class": "aui-iconfont-unfocus", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/addon/bitbucket-trello-addon/trello-board", "tab_name": "repopage-5ABRne-add-on-link", "can_display": true, "label": "Boards", "anchor": false, "analytics_payload": {}, "icon_url": "https://bitbucket-assetroot.s3.amazonaws.com/add-on/icons/35ceae0c-17b1-443c-a6e8-d9de1d7cccdb.svg?Signature=hYD4RUHxfLj%2BhuVOD4O9MwrzOI0%3D&Expires=1511782815&AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&versionId=3oqdrZZjT.HijZ3kHTPsXE6IcSjhCG.P", "type": "connect_menu_item", "id": "repopage-5ABRne-add-on-link", "target": "_self"}, {"analytics_label": "repository.settings", "icon_class": "icon-settings", "badge_label": null, "weight": 100, "url": "/Yanosik/zapart/admin", "tab_name": "admin", "can_display": true, "label": "Settings", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-settings-link", "icon": "icon-settings"}], "bitbucketActions": [{"analytics_label": "repository.clone", "icon_class": "icon-clone", "badge_label": null, "weight": 100, "url": "#clone", "tab_name": "clone", "can_display": true, "label": "<strong>Clone<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-clone-button", "icon": "icon-clone"}, {"analytics_label": "repository.create_branch", "icon_class": "icon-create-branch", "badge_label": null, "weight": 200, "url": "/Yanosik/zapart/branch", "tab_name": "create-branch", "can_display": true, "label": "Create a <strong>branch<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-branch-link", "icon": "icon-create-branch"}, {"analytics_label": "create_pullrequest", "icon_class": "icon-create-pull-request", "badge_label": null, "weight": 300, "url": "/Yanosik/zapart/pull-requests/new", "tab_name": "create-pullreqs", "can_display": true, "label": "Create a <strong>pull request<\/strong>", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-create-pull-request-link", "icon": "icon-create-pull-request"}, {"analytics_label": "repository.compare", "icon_class": "aui-icon-small aui-iconfont-devtools-compare", "badge_label": null, "weight": 400, "url": "/Yanosik/zapart/branches/compare", "tab_name": "compare", "can_display": true, "label": "<strong>Compare<\/strong> branches or tags", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-compare-link", "icon": "aui-icon-small aui-iconfont-devtools-compare"}, {"analytics_label": "repository.fork", "icon_class": "icon-fork", "badge_label": null, "weight": 500, "url": "/Yanosik/zapart/fork", "tab_name": "fork", "can_display": true, "label": "<strong>Fork<\/strong> this repository", "anchor": true, "analytics_payload": {}, "target": "_self", "type": "menu_item", "id": "repo-fork-link", "icon": "icon-fork"}], "activeMenuItem": "source"}}};
  window.__settings__ = {"SOCIAL_AUTH_ATLASSIANID_LOGOUT_URL": "https://id.atlassian.com/logout", "CANON_URL": "https://bitbucket.org", "API_CANON_URL": "https://api.bitbucket.org"};
</script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/jsi18n/en/djangojs.js" nonce="pudKVFKPFVGF87mQ"></script>

  <script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/locales/en.js"></script>

<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/vendor.js" nonce="pudKVFKPFVGF87mQ"></script>
<script src="https://d301sr5gafysq2.cloudfront.net/370568ebc84f/dist/webpack/app.js" nonce="pudKVFKPFVGF87mQ"></script>


<script async src="https://www.google-analytics.com/analytics.js" nonce="pudKVFKPFVGF87mQ"></script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"licenseKey":"a2cef8c3d3","agent":"","transactionName":"Z11RZxdWW0cEVkYLDV4XdUYLVEFdClsdAAtEWkZQDlJBGgRFQhFMQl1DXFcZQ10AQkFYBFlUVlEXWEJHAA==","applicationID":"1841284","errorBeacon":"bam.nr-data.net","applicationTime":629}</script>
</body>
</html>