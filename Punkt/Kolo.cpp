#include "kolo.h"

Kolo::Kolo()
{
	wysokoscOkna = 768;
	szerokoscOkna = 1024;
	polozenieOknaX = 100;
	polozenieOknaY = 100;
}

Kolo::Kolo(int wysokoscOkna, int szerokoscOkna, int polozenieOknaX, int polozenieOknaY, GLfloat promien)
{
	this->wysokoscOkna = wysokoscOkna;
	this->szerokoscOkna = szerokoscOkna;
	this->polozenieOknaX = polozenieOknaX;
	this->polozenieOknaY = polozenieOknaY;
	this->promien = promien;
}

Kolo::~Kolo()
{
	//glDeleteBuffers(1, &VBO);
	//glDeleteVertexArrays(1, &VAO);
}

void Kolo::stworzenieOkna(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(szerokoscOkna, wysokoscOkna);
	glutInitWindowPosition(polozenieOknaX, polozenieOknaY);
	glutCreateWindow("trojkat");
}

void Kolo::inicjalizacjaGlew()
{
	GLenum wynik = glewInit();
	if (wynik != GLEW_OK)
	{
		std::cerr << "Nie udalo sie zainicjalizowac GLEW. Blad: " << glewGetErrorString(wynik) << std::endl;
		system("pause");
		exit(1);
	}
}

void  Kolo::wyswietl()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glPointSize(wielkoscPunktu);
	glDrawArrays(GL_TRIANGLE_FAN, 0, ILOSC_BOKOW + 2);

	glDisableVertexAttribArray(0);

	glutSwapBuffers();
}

void Kolo::stworzenieVAO()
{
	glGenVertexArrays(1, &VAO);
	// sprawdzenie nadanego id (nadawane automatycznie)
	std::cout << "VAO: " << VAO << std::endl;
	glBindVertexArray(VAO);
}

void Kolo::stworzenieVBO()
{
	GLfloat Wierzcholki[ILOSC_BOKOW + 21][12];/* = {
		-0.8f, -0.8f, 0.0f,
		0.8f, -0.8f, 0.0f,
		0.0f, 0.8f, 0.0f,
	};*/
	//srodek kola
	Wierzcholki[0][0] = 0.0f;
	Wierzcholki[0][1] = 0.0f;
	Wierzcholki[0][2] = 0.0f;

	GLfloat dwaPi = M_PI * 2.0f;

	for (int i = 1; i < ILOSC_BOKOW + 2; ++i)
	{
		Wierzcholki[i][0] = promien*cos(i*dwaPi / ILOSC_BOKOW);
		Wierzcholki[i][0] = promien*sin(i*dwaPi / ILOSC_BOKOW);
		Wierzcholki[i][2] = 0.0f;
	}

	GLfloat xCCW[3] = { -0.8, 0.8, 0.0 };
	GLfloat yCCW[3] = { -0.8, -0.8, 0.8 };

	GLfloat sumCCW = 0;
	GLfloat sumCW = 0;
	int n = 3;

	for (int i = 0; i < n; ++i){
		sumCCW += xCCW[i] * yCCW[(i + 1) % n] - xCCW[(i + 1) % n] * yCCW[i];
		sumCW += xCCW[i] * yCCW[(i + 1) % n] - xCCW[(i + 1) % n] * yCCW[i];
	}

	GLfloat aCCW = sumCCW / 2;
	GLfloat aCW = sumCW / 2;

	//std::cout<<"aCCW: "....

	glGenBuffers(4, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	// sprawdzenie nadanego id (nadawane automatycznie)
	std::cout << "VBO: " << VBO << std::endl;
	glBufferData(GL_ARRAY_BUFFER, sizeof(Wierzcholki), Wierzcholki, GL_STATIC_DRAW);
}


int main(int argc, char** argv)
{
	//inicjalizacja srodkowego punktu
	Kolo punkt(786, 1024, 100, 100, 0.5f);

	punkt.stworzenieOkna(argc, argv);
	punkt.inicjalizacjaGlew();
	punkt.stworzenieVAO();
	punkt.stworzenieVBO();
	glutDisplayFunc(punkt.wyswietl);



	// po zamknieciu okna kontrola wraca do programu
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	glClearColor(0.2f, 0.1f, 0.0f, 0.0f);

	glutMainLoop();

	//bez glutSetOption() to nie zadziala, nie wejdzie tez program do destruktora
	//system("pause");



	return 0;
}